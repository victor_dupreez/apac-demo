#!/bin/bash

echo "Environment: $1"
echo "Tag: $2"
echo "build script: $3"

REF_SPEC="+refs/tags/$1/*:refs/remotes/origin/tags/$1/*"
TAG_SELECTOR="origin/tags/$2"
URL="https://ci.ps.accedo.tv/ci/job/Les-Mills-Digital-Android/buildWithParameters?token=test"

echo "POSTING: $URL"
curl -w POST --data-urlencode "tag_selector=$TAG_SELECTOR" --data-urlencode "ref_spec=$REF_SPEC" --data-urlencode "gradle_command=clean $3" "$URL"
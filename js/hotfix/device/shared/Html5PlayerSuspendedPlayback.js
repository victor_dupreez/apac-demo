/**
 * Html5PlayerSuspendedPlayback is an implementation of {@link ax/device/interface/Suspendable.SuspendedPlayback}.<br/>
 * It encapsulates information and procedures for playback restoration.
 *
 * @class ax/device/shared/Html5PlayerSuspendedPlayback
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define("ax/device/shared/Html5PlayerSuspendedPlayback", [
    "ax/class",
    "ax/device/interface/Suspendable",
    "ax/device/AbstractPlayer",
    "ax/promise",
    "ax/util",
    "ax/core",
    "ax/exception"
], function (
    klass,
    Suspendable,
    AbstractPlayer,
    promise,
    util,
    core,
    exception
) {
    "use strict";

    return klass.create([Suspendable.SuspendedPlayback], {}, {
        /**
         * Constructs a new Html5PlayerSuspendedPlayback.
         *
         * @method
         * @param {MultiObjectHtml5Player} player The Html5Player instance
         * @param {Video} video The JS Video object
         * @param {Object} metadata The metadata for playback resume
         * @param {String} metadata.url The media url
         * @param {Object} [metadata.opts] The media loading options
         * @param {Number} [metadata.pos=0] The resume position
         * @param {String} [metadata.state] The playback state
         * @memberof ax/device/shared/Html5PlayerSuspendedPlayback
         */
        init: function (player, video, metadata) {
            this.__player = player;
            this.__video = video;
            this.__url = metadata.url;
            this.__opts = metadata.opts || {};
            this.__pos = metadata.pos || 0;
            this.__state = metadata.state;
        },

        /**
         * Restores the suspended playback.
         * The playback states before suspension will retain.
         *
         * @method
         * @returns {Promise} Resolve when finishes
         * @memberof ax/device/shared/Html5PlayerSuspendedPlayback#
         */
        restore: function () {
            var deferred = promise.defer();

            this.__player._swapPlayerObject(this.__video.id);

            if (this.__video.getAttribute("src") !== this.__url) {
                // an one-time event listener
                this.__onLoad = util.bind(function (evt) {
                    this.__restoreTimeAndStates();
                    deferred.resolve();

                    // remove itself
                    evt.target.removeEventListener("loadedmetadata", this.__onLoad);
                }, this);

                this.__video.addEventListener("loadedmetadata", this.__onLoad);

                this.__video.setAttribute("src", this.__url);
                this.__video.load();
            } else {
                this.__restoreTimeAndStates();
                deferred.resolve();
            }

            this.__video.style.visibility = "visible";

            return deferred.promise;
        },

        /**
         * Disposes the suspended playback, destroying he playback states before suspension.<br/>
         * A disposed playback cannot be restored anymore.
         *
         * @method
         * @memberof ax/device/shared/Html5PlayerSuspendedPlayback#
         */
        dispose: function () {
            this.__video.removeEventListener("loadedmetadata", this.__onLoad);
            this.__player._disposeSuspended(this);
            this.__video = null;
            this.__player = null;
        },

        /**
         * Returns the media information.
         *
         * @method
         * @returns {MediaMeta} The media metadata
         * @memberof ax/device/shared/Html5PlayerSuspendedPlayback#
         */
        getMediaMeta: function () {
            return {
                url: this.__url,
                opts: this.__opts
            };
        },

        /**
         * Returns the player instance who is responsible for restoration.
         *
         * @method
         * @returns {ax/device/shared/MultiObjectHtml5Player} The player instance
         * @memberof ax/device/shared/Html5PlayerSuspendedPlayback#
         */
        getPlayer: function () {
            return this.__player;
        },

        /**
         * Restores the playback time and state.
         *
         * @method
         * @private
         * @memberof ax/device/shared/Html5PlayerSuspendedPlayback#
         */
        __restoreTimeAndStates: function () {
            this.__video.currentTime = this.__pos;
            if (this.__state === AbstractPlayer.PLAYBACK_STATE.PLAYING) {
                this.__player.play();
            }
        }
    });
});
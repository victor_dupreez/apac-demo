/**
 * HTML5 player subtitle external strategy
 *
 * Supported type: "webvtt"
 *
 * Please aware of cross domain issue will also happen on the subtitle.
 * @class ax/device/shared/Html5PlayerExtSubtitleStgy
 */
define("ax/device/shared/Html5PlayerExtSubtitleStgy", [
    "ax/class",
    "ax/device/interface/ExtSubtitleStgy",
    "ax/core",
    "ax/exception",
    "ax/console",
    "ax/promise",
    "ax/util",
    "ax/device/Media",
    "ax/device/shared/Html5PlayerPreloadedPlayback"
], function (
    klass,
    IExtSubtitleStgy,
    core,
    exception,
    console,
    promise,
    util,
    Media,
    Html5PlayerPreloadedPlayback
) {
    "use strict";
    return klass.create([IExtSubtitleStgy], {}, {
        /**
         * The player instance
         * @private
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        __player: null,
        /**
         * The preloaded item which contains the player object
         * @private
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        __preloaded: null,
        /**
         * Player object used in the html5 player external subtitle strategy
         * @private
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        __playerObj: null,
        /**
         * Array to hold the subtitle information
         * @private
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        __subtitleArr: [],
        /**
         * The current Id of subtitle track
         * @private
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        __currentId: -1,
        /**
         * Prepare for the subtitle, like download the external source or embedded the external subtitle into the player
         * Doing nothing instead of throwing illegal state exception when it is preloaded object without a unique player object
         * which got via getPlayerObject function.
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         * @method
         * @public
         */
        prepare: function (subtitleOption) {

            //find if any preloaded
            if (this.__preloaded) {
                //use the preloaded getPlayerObject
                if (this.__preloaded instanceof Html5PlayerPreloadedPlayback) {
                    this.__playerObj = this.__preloaded.getPlayerObject();
                } else {
                    //HTML5 Player doesn't support multiple player objects and unable to prepare when preload
                    console.info("Html5 player doesn't support multiple player objects and unable to prepare when preload.");
                    return;
                }

            } else {
                //get the play object via player protected method getPlayerObj
                if (this.__player.getPlayerObject) {
                    this.__playerObj = this.__player.getPlayerObject();
                } else {
                    //backward compatibility for previous player without getPlayerObject function
                    this.__playerObj = this.__player._playerObject;
                }

            }

            //only need to prepare for exist subtitleOption
            if (subtitleOption) {
                this.__prepareObject(this.__playerObj, subtitleOption);
                return;
            }
        },
        /**
         * Prepare for the subtitle track and perform dom handling for the specific playerobj
         * @method
         * @param {HTML5Element} playerObj the html5 video object
         * @param {ax/device/Subtitle.subtitleOption} the option for the external subtitle
         * @private
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        __prepareObject: function (playerObj, subtitleOption) {
            //embedded the subtitle into the video object
            this.__subtitleArr = [];

            if (playerObj) {
                util.each(subtitleOption, util.bind(function (opts, i) {
                    if (this._getCapability().indexOf(opts.type) === -1) {
                        console.info("MultiObjHtml5Player doesn't support " + opts.type + " subtitle format");
                        return;
                    }

                    var trackNode = document.createElement("track");
                    trackNode.kind = "subtitles";
                    trackNode.srclang = opts.lang;
                    trackNode.src = opts.url;
                    //as a record for xdk added track
                    trackNode.xdk = true;
                    this.__playerObj.appendChild(trackNode);
                    //set all the textTrack to be hidden at the beginning
                    this.__playerObj.textTracks[i].mode = "hidden";

                    //prepare for the list
                    this.__subtitleArr.push({
                        id: this.__subtitleArr.length,
                        type: IExtSubtitleStgy.TYPE,
                        lang: opts.lang,
                        tag: opts.tag
                    });
                }, this));
            }
        },
        /**
         * The capable type for the external subtitle
         * @protected
         * @method
         * @returns {String[]} the format of possible subtitle.
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        _getCapability: function () {
            return ["webvtt"];
        },

        /**
         * Set the player and preloadable information
         * @method
         * @param {ax/device/interface/Player} player the player
         * @param {ax/device/interface/Preloaded.PreloadedPlayback} [preloaded] item from the player, it is just for preload to get information for the current preloadable item
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        setPlayer: function (player, preloaded) {
            this.__player = player;
            this.__preloaded = preloaded;
        },
        /**
         * show the substitle.
         * @param {String} id the subtitle id
         * @returns {Promise.<Boolean>} the result of showing substitle. True when successfully loaded.
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} when fail to show non-existing subtitle
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         * @method
         * @public
         */
        showSubtitle: function (id) {
            var media = Media.singleton();

            //hide previous one if exist since only allow one subtitle each time
            if (this.__currentId !== -1) {
                this.__playerObj.textTracks[this.__currentId].mode = "hidden";
            }

            this.__currentId = id;

            if (this.__playerObj.textTracks.length < id) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "Fail to show non existing external subtitle"));
            }
            this.__playerObj.textTracks[id].mode = "showing";

            this.__statusListener = util.bind(function (evt) {
                if (evt.toState === media.STOPPED) {
                    //hide the subtitle
                    this.hideSubtitle();
                    this.__currentId = -1;
                    media.removeEventListener(media.EVT_STATE_CHANGED, this.__statusListener);
                }

            }, this);

            media.addEventListener(media.EVT_STATE_CHANGED, this.__statusListener);


            return promise.resolve(true);
        },
        /**
         * hide the subtitle
         * @method
         * @returns {Promise.<Boolean>} the result of hiding subtitle. True when succesfully hided.
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} No available subtitle
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         * @public
         */
        hideSubtitle: function () {
            if (this.__currentId === -1) {
                console.info("MultiObjHtml5Player no track is showing and do nothing");
                return promise.reject(core.createException(exception.ILLEGAL_STATE, " No available subtitle"));
            }

            this.__playerObj.textTracks[this.__currentId].mode = "hidden";
            return promise.resolve(true);
        },
        /**
         * get the current subtitle
         * @return {Promise.<ax/device/Subtitle.Subtitle>} The current substitle information
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} No available subtitle
         * @method
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         * @public
         */
        getCurrentSubtitle: function () {
            if (this.__currentId === -1) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, " No available subtitle"));
            }

            return promise.resolve(this.__subtitleArr[this.__currentId]);

        },
        /**
         * get all the available subtitle
         * @return {Promise.<ax/device/Subtitle.subsitle[]>} the available substitle array
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         * @method
         * @public
         */
        getSubtitles: function () {
            return promise.resolve(this.__subtitleArr);
        },
        /**
         * deinit the external subtitle
         * @method
         * @protected
         * @memberof ax/device/shared/Html5PlayerExtSubtitleStgy#
         */
        deinit: function () {
            //remove the subtitle track from the dom
            var nodeNo,
                trackNode;

            for (nodeNo = this.__playerObj.childNodes.length - 1; nodeNo > -1; nodeNo--) {
                trackNode = this.__playerObj.childNodes[nodeNo];
                if (trackNode.xdk) {
                    this.__playerObj.removeChild(trackNode);
                }
            }

        }
    });
});
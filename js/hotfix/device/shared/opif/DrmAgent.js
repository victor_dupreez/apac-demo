/**
 * OPIF DRM Agent is a DRM agent that follows the OPIF's Open IPTV specification.
 * @see {@link http://www.oipf.tv/docs/Release1/OIPF-T1-R1-Specification-Volume-5-Declarative-Application-Environment-V1_0-2009-01-06.pdf}  
 * @class ax/device/shared/opif/DrmAgent
 */
define("ax/device/shared/opif/DrmAgent", ["ax/class", "ax/promise", "ax/util", "ax/console", "ax/core", "ax/exception"], function (klass, promise, util, console, core, exception) {
    "use strict";
    var MSG_TYPE = "application/vnd.ms-playready.initiator+xml",
        SYSTEM = {
            UNKNOWN: "Unknown",
            PLAY_READY: "PlayReady",
            MARLIN: "Marlin"
        };

    return klass.create({}, {
        /**
         * The DRM Agent object
         * @protected
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        _drmAgent: undefined,
        /**
         * The DRM systen id
         * @protected
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        _systemId: undefined,
        /**
         * Counter for the DRM request
         * @protected
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        _requestCounter: 0,
        /**
         * The license acquisition defer object
         * @private
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        __laDeferred: undefined,
        /**
         * The last license acquisition promise object in the queue
         * @private
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        __queuedPromise: undefined,
        /**
         * Flag to indicate if an acquisition is processing
         * @private
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        __acquiuringLicense: false,
        /**
         * Process queue
         * @private
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        __queue: [],
        /**
         * Flag to indicate if the queue is being processed
         * @private
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        __processingQueue: false,

        /**
         * Initialize the DrmAgent.
         * @method
         * @param {Object} drmAgent The platform dependent drmAgent
         * @param {String} systemId The system id used to identify the device
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        init: function (drmAgent, systemId) {
            if (!drmAgent) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "No valid native drm agent available");
            }
            if (!systemId) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "systemId is mandatory");
            }

            this._drmAgent = drmAgent;
            this._systemId = systemId;

            drmAgent.onDRMMessageResult = util.bind(this._onDRMMessageResult, this);
            drmAgent.onDRMRightsError = util.bind(this._onDRMRightsError, this);
        },

        /**
         * Send the license request to the native drm agent.
         * @method
         * @param {String} laUrl The url of the license acquisition server
         * @param {String} [customData] The custom data
         * @returns {Promise.<Boolean>} Return true when the license acqiusition success
         * @throws {Promise.<ax/Exception>} Reject if any error occurs
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        sendLicenseRequest: function (laUrl, customData) {
            var deferred = promise.defer(),
                process = util.bind(function (_defer) {
                    var _promise;
                    // reset counter for each process
                    this._requestCounter = 0;

                    this.__laDeferred = _defer;

                    // promise = _promise, but get from _defer to avoid closure issue
                    _promise = _defer.promise;

                    this._sendDRMMessages(laUrl, customData);

                    // the license acquisition resolved, resolve the individual promise
                    return _promise.then(function (msg) {
                        _defer.resolve(msg);
                    }).fail(function (reason) {
                        _defer.reject(reason);
                    });
                }, this, deferred);

            // only process the request if none is working AND the queue is not empty
            // otherwise push to the queue
            if (!this.__acquiuringLicense && this.__queue.length === 0) {
                this.__acquiuringLicense = true;
                process(deferred);
            } else {
                this.__queue.push(process);
            }

            return deferred.promise;
        },

        /**
         * Process the request queue.
         * @protected
         * @method
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        _processQueue: function () {
            if (this.__processingQueue) {
                return;
            }

            this.__processingQueue = true;
            var fn = this.__queue.shift();
            if (fn) {
                fn().then(util.bind(function () {
                    this.__processingQueue = false;
                    this._processQueue();
                }, this));
            } else {
                this.__processingQueue = false;
            }
        },

        /**
         * The actual function to send DRM messages.
         * @protected
         * @method
         * @param {String} laUrl The url of the license acquisition server
         * @param {String} [customData] The custom data
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        _sendDRMMessages: function (laUrl, customData) {
            var xmlLicenseAcquisition;

            try {
                // license URL
                if (util.isString(laUrl)) {
                    this._requestCounter++;

                    xmlLicenseAcquisition = "<?xml version='1.0' encoding='utf-8'?>" +
                            "<PlayReadyInitiator xmlns='http://schemas.microsoft.com/DRM/2007/03/protocols/'>" +
                                "<LicenseServerUriOverride>" +
                                    "<LA_URL>" + laUrl + "</LA_URL>" +
                                "</LicenseServerUriOverride>" +
                            "</PlayReadyInitiator>";
                    this._drmAgent.sendDRMMessage(MSG_TYPE, xmlLicenseAcquisition, this._systemId);
                    console.info("sendDRMMessage (License Acquisition URL): " + laUrl);
                }

                // custom Data
                if (util.isString(customData)) {
                    this._requestCounter++;

                    xmlLicenseAcquisition = "<?xml version='1.0' encoding='utf-8'?>" +
                            "<PlayReadyInitiator xmlns='http://schemas.microsoft.com/DRM/2007/03/protocols/'>" +
                                "<SetCustomData>" +
                                    "<CustomData>" + customData + "</CustomData>" +
                                "</SetCustomData>" +
                            "</PlayReadyInitiator>";
                    this._drmAgent.sendDRMMessage(MSG_TYPE, xmlLicenseAcquisition, this._systemId);
                    console.info("sendDRMMessage (CustomData): " + customData);
                }
            } catch (e) {
                console.warn("[DrmAgent] ERROR: sendDRMMessage (" + e + ")");
                this._requestCounter = 0; // fail, reset counter
                this.__acquiuringLicense = false;
                this.__laDeferred.reject(core.createException(exception.INTERNAL, "sendDRMMessage failed: " + e));
            }
        },

        /**
         * Callback to be called by the native drm agent when sendDRMMessage() is finished.
         * @protected
         * @method
         * @param {String} msgId Identifies the original message which has lead to this resulting message
         * @param {String} resultMsg DRM system specific result message
         * @param {Number} resultCode Integer result code
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        _onDRMMessageResult: function (msgId, resultMsg, resultCode) {
            var description;

            this._requestCounter--;

            switch (resultCode) {
                case 0:
                    description = "Successful";
                    if (this._requestCounter > 0) {
                        description += ", but not yet ready (request counter > 0)";
                    }
                    break;
                case 1:
                    if (resultMsg === "0x8004C600") {
                        description = "Server could not deliver a license (server internal error)";
                    } else if (resultMsg === "0x8004C580") {
                        description = "Acquired license successfully but domain certificate missing on device";
                    }
                    break;
                case 2:
                    description = "Cannot process request";
                    break;
                case 3:
                    description = "Unknown MIME type";
                    break;
                case 4:
                    description = "User consent needed";
                    break;
            }
            console.info("[DrmAgent] onDRMMessageResult: resultCode:" + resultCode + " - " + description);
            console.debug("[DrmAgent] counter down to " + this._requestCounter);
        
            if (this._requestCounter === 0) {
                this._onLicenseAcquired();
            }
        },

        /**
         * Callback to be called when the license acquisition is done.
         * This function will resolve the license acquisition defer.
         * @protected
         * @method
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        _onLicenseAcquired: function () {
            console.info("[DrmAgent] license acquired");
            this.__laDeferred.resolve(true);
            this.__acquiuringLicense = false;
            this._processQueue();
        },

        /**
         * Callback to be called by the native drm agent when license acquisition fails.
         * This function will reject the license acquisition defer.
         * @protected
         * @method
         * @param {Number} errorState Error state code
         * @param {String} contentId 
         * @param {String} drmSystemId The identifier of the drm system
         * @param {String} rightsIssuerUrl The url of the rights issuer
         * @memberof ax/device/shared/opif/DrmAgent#
         */
        _onDRMRightsError: function (errorState, contentId, drmSystemId, rightsIssuerUrl) {
            var systemName = SYSTEM.UNKNOWN,
                description;

            switch(errorState) {
                case 0:
                    description = "No license (0), consumption of the content is blocked";
                    break;
                case 1:
                    description = "Invalid License (1), consumption of the content is blocked";
                    break;
                case 2:
                    description = "Valid License (2), consumption of the content is unblocked";
                    break;
                default:
                    description = "Unknown (" + errorState + ")";
                    break;
            }

            if (drmSystemId) {
                if (drmSystemId.indexOf("19188") !== -1) {
                    systemName = SYSTEM.MARLIN;
                } else if (drmSystemId.indexOf("19219") !== -1) {
                    systemName = SYSTEM.PLAY_READY;
                } else {
                    systemName = drmSystemId;
                }
            }

            console.warn("[DrmAgent] onDRMRightsError (from " + systemName + "): " + description + "; contentId:" + contentId + "; rightsIssuerUrl: " + rightsIssuerUrl);
            this.__laDeferred.reject(core.createException(exception.UNAUTHORIZED, description));
            this.__acquiuringLicense = false;
            this._processQueue();
        }
    });
});

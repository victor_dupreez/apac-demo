/**
 * To provide the html5 localstorage as storage.
 * @class ax/device/shared/LocalStorage
 * @augments ax/device/AbstractStorage
 *
 */
define("ax/device/shared/LocalStorage", ["ax/class", "ax/device/AbstractStorage", "ax/console"], function (klass, AbstractStorage, console) {
    "use strict"; 
    return klass.create(AbstractStorage, {}, {
        /**
         * to set the variable
         * @public
         * @method
         * @name set
         * @param {String} k keys
         * @param {String} v values
         * @memberof ax/device/shared/LocalStorage#
         */
        set: function (k, v) {
            console.log("using Localstorage");
            localStorage.setItem(k, v);
        },
        /**
         * to get the variable
         * @public
         * @method
         * @name get
         * @param {String} k keys
         * @return {String} v values
         * @memberof ax/device/shared/LocalStorage#
         */
        get: function (k) {
            return localStorage.getItem(k);
        },
        /**
         * to unset the variable
         * @public
         * @method
         * @name unset
         * @param {String} k keys
         * @memberof ax/device/shared/LocalStorage#
         */
        unset: function (k) {
            localStorage.removeItem(k);
        },
        /**
         * To do clear the local storage
         * @protected
         * @method
         * @name _doClear
         * @memberof ax/device/shared/LocalStorage#
         */
        _doClear: function () {
            localStorage.clear();
        }
    });
});
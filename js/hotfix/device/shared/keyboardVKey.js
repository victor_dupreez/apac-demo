/**
 * Contains the definition for keyboard virtual keys. Used by device packages who support fixed keyboards. e.g.
 * {@link ax/device/workstation/TvKey|Workstation},{@link ax/ext/device/boxee/TvKey|Boxee}
 *
 * @module ax/device/shared/keyboardVKey
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/device/shared/keyboardVKey", ["ax/device/vKey"], function (vKey) {
    "use strict"; 
    return {
        /**
         *
         * @name A
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:A"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        A: {
            id: "device:kb-vkey:A",
            text: "A"
        },
        /**
         *
         * @name B
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:B"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        B: {
            id: "device:kb-vkey:B",
            text: "B"
        },
        /**
         *
         * @name C
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:C"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        C: {
            id: "device:kb-vkey:C",
            text: "C"
        },
        /**
         *
         * @name D
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:D"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        D: {
            id: "device:kb-vkey:D",
            text: "D"
        },
        /**
         *
         * @name E
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:E"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        E: {
            id: "device:kb-vkey:E",
            text: "E"
        },
        /**
         *
         * @name F
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:F"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        F: {
            id: "device:kb-vkey:F",
            text: "F"
        },
        /**
         *
         * @name G
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:G"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        G: {
            id: "device:kb-vkey:G",
            text: "G"
        },
        /**
         *
         * @name H
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:H"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        H: {
            id: "device:kb-vkey:H",
            text: "H"
        },
        /**
         *
         * @name I
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:I"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        I: {
            id: "device:kb-vkey:I",
            text: "I"
        },
        /**
         *
         * @name J
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:J"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        J: {
            id: "device:kb-vkey:J",
            text: "J"
        },
        /**
         *
         * @name K
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:K"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        K: {
            id: "device:kb-vkey:K",
            text: "K"
        },
        /**
         *
         * @name L
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:L"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        L: {
            id: "device:kb-vkey:L",
            text: "L"
        },
        /**
         *
         * @name M
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:M"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        M: {
            id: "device:kb-vkey:M",
            text: "M"
        },
        /**
         *
         * @name N
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:N"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        N: {
            id: "device:kb-vkey:N",
            text: "N"
        },
        /**
         *
         * @name O
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:O"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        O: {
            id: "device:kb-vkey:O",
            text: "O"
        },
        /**
         *
         * @name P
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:P"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        P: {
            id: "device:kb-vkey:P",
            text: "P"
        },
        /**
         *
         * @name Q
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:Q"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        Q: {
            id: "device:kb-vkey:Q",
            text: "Q"
        },
        /**
         *
         * @name R
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:R"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        R: {
            id: "device:kb-vkey:R",
            text: "R"
        },
        /**
         *
         * @name S
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:S"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        S: {
            id: "device:kb-vkey:S",
            text: "S"
        },
        /**
         *
         * @name T
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:T"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        T: {
            id: "device:kb-vkey:T",
            text: "T"
        },
        /**
         *
         * @name U
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:U"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        U: {
            id: "device:kb-vkey:U",
            text: "U"
        },
        /**
         *
         * @name V
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:V"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        V: {
            id: "device:kb-vkey:V",
            text: "V"
        },
        /**
         *
         * @name W
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:W"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        W: {
            id: "device:kb-vkey:W",
            text: "W"
        },
        /**
         *
         * @name X
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:X"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        X: {
            id: "device:kb-vkey:X",
            text: "X"
        },
        /**
         *
         * @name Y
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:Y"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        Y: {
            id: "device:kb-vkey:Y",
            text: "Y"
        },
        /**
         *
         * @name Z
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:Z"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        Z: {
            id: "device:kb-vkey:Z",
            text: "Z"
        },
        /**
         *
         * @name A_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:a"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        A_LOWER: {
            id: "device:kb-vkey:a",
            text: "a"
        },
        /**
         *
         * @name B_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:b"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        B_LOWER: {
            id: "device:kb-vkey:b",
            text: "b"
        },
        /**
         *
         * @name C_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:c"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        C_LOWER: {
            id: "device:kb-vkey:c",
            text: "c"
        },
        /**
         *
         * @name D_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:d"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        D_LOWER: {
            id: "device:kb-vkey:d",
            text: "d"
        },
        /**
         *
         * @name E_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:e"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        E_LOWER: {
            id: "device:kb-vkey:e",
            text: "e"
        },
        /**
         *
         * @name F_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:f"
         * @property {String} text Text value of keys
         * @memberof module:ax/device/shared/keyboardVKey
         */
        F_LOWER: {
            id: "device:kb-vkey:f",
            text: "f"
        },
        /**
         *
         * @name G_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:g"
         * @property {String} text Text value of keys
         * @memberof module:ax/device/shared/keyboardVKey
         */
        G_LOWER: {
            id: "device:kb-vkey:g",
            text: "g"
        },
        /**
         *
         * @name H_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:h"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        H_LOWER: {
            id: "device:kb-vkey:h",
            text: "h"
        },
        /**
         *
         * @name I_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:i"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        I_LOWER: {
            id: "device:kb-vkey:i",
            text: "i"
        },
        /**
         *
         * @name J_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:j"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        J_LOWER: {
            id: "device:kb-vkey:j",
            text: "j"
        },
        /**
         *
         * @name K_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:k"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        K_LOWER: {
            id: "device:kb-vkey:k",
            text: "k"
        },
        /**
         *
         * @name L_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:l"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        L_LOWER: {
            id: "device:kb-vkey:l",
            text: "l"
        },
        /**
         *
         * @name M_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:m"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        M_LOWER: {
            id: "device:kb-vkey:m",
            text: "m"
        },
        /**
         *
         * @name N_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:n"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        N_LOWER: {
            id: "device:kb-vkey:n",
            text: "n"
        },
        /**
         *
         * @name O_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:o"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        O_LOWER: {
            id: "device:kb-vkey:o",
            text: "o"
        },
        /**
         *
         * @name P_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:p"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        P_LOWER: {
            id: "device:kb-vkey:p",
            text: "p"
        },
        /**
         *
         * @name Q_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:q"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        Q_LOWER: {
            id: "device:kb-vkey:q",
            text: "q"
        },
        /**
         *
         * @name R_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:r"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        R_LOWER: {
            id: "device:kb-vkey:r",
            text: "r"
        },
        /**
         *
         * @name S_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:s"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        S_LOWER: {
            id: "device:kb-vkey:s",
            text: "s"
        },
        /**
         *
         * @name T_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:t"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        T_LOWER: {
            id: "device:kb-vkey:t",
            text: "t"
        },
        /**
         *
         * @name U_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:u"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        U_LOWER: {
            id: "device:kb-vkey:u",
            text: "u"
        },
        /**
         *
         * @name V_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:v"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        V_LOWER: {
            id: "device:kb-vkey:v",
            text: "v"
        },
        /**
         *
         * @name W_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:w"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        W_LOWER: {
            id: "device:kb-vkey:w",
            text: "w"
        },
        /**
         *
         * @name X_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:x"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        X_LOWER: {
            id: "device:kb-vkey:x",
            text: "x"
        },
        /**
         *
         * @name Y_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:y"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        Y_LOWER: {
            id: "device:kb-vkey:y",
            text: "y"
        },
        /**
         *
         * @name Z_LOWER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:z"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        Z_LOWER: {
            id: "device:kb-vkey:z",
            text: "z"
        },
        /**
         *
         * @name SPACE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:space"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        SPACE: {
            id: "device:kb-vkey:space",
            text: " "
        },
        /**
         *
         * @name TILDE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:tilde"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        TILDE: {
            id: "device:kb-vkey:~",
            text: "~"
        },
        /**
         *
         * @name BACK_QUOTE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:`"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        BACK_QUOTE: {
            id: "device:kb-vkey:`",
            text: "`"
        },
        /**
         *
         * @name EXC
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:!"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        EXC: {
            id: "device:kb-vkey:!",
            text: "!"
        },
        /**
         *
         * @name AT
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:@"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        AT: {
            id: "device:kb-vkey:@",
            text: "@"
        },
        /**
         *
         * @name HASH
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:#"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        HASH: {
            id: "device:kb-vkey:#",
            text: "#"
        },
        /**
         *
         * @name DOLLAR
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:$"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        DOLLAR: {
            id: "device:kb-vkey:$",
            text: "$"
        },
        /**
         *
         * @name PERCENT
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:%"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        PERCENT: {
            id: "device:kb-vkey:%",
            text: "%"
        },
        /**
         *
         * @name CARET
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:^"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        CARET: {
            id: "device:kb-vkey:^",
            text: "^"
        },
        /**
         *
         * @name AMP
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:&"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        AMP: {
            id: "device:kb-vkey:&",
            text: "&"
        },
        /**
         *
         * @name STAR
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:*"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        STAR: {
            id: "device:kb-vkey:*",
            text: "*"
        },
        /**
         *
         * @name OPEN_PAREN
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:("
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        OPEN_PAREN: {
            id: "device:kb-vkey:(",
            text: "("
        },
        /**
         *
         * @name CLOSE_PAREN
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:)"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        CLOSE_PAREN: {
            id: "device:kb-vkey:)",
            text: ")"
        },
        /**
         *
         * @name MINUS
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:-"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        MINUS: {
            id: "device:kb-vkey:-",
            text: "-"
        },
        /**
         *
         * @name UNDERSCORE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:_"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        UNDERSCORE: {
            id: "device:kb-vkey:_",
            text: "_"
        },
        /**
         *
         * @name EQUALS
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:="
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        EQUALS: {
            id: "device:kb-vkey:=",
            text: "="
        },
        /**
         *
         * @name PLUS
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:+"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        PLUS: {
            id: "device:kb-vkey:+",
            text: "+"
        },
        /**
         *
         * @name OPEN_BRACKET
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:["
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        OPEN_BRACKET: {
            id: "device:kb-vkey:[",
            text: "["
        },
        /**
         *
         * @name OPEN_BRACE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:{"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        OPEN_BRACE: {
            id: "device:kb-vkey:{",
            text: "{"
        },
        /**
         *
         * @name CLOSE_BRACKET
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:]"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        CLOSE_BRACKET: {
            id: "device:kb-vkey:]",
            text: "]"
        },
        /**
         *
         * @name CLOSE_BRACE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:}"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        CLOSE_BRACE: {
            id: "device:kb-vkey:}",
            text: "}"
        },
        /**
         *
         * @name BACK_SLASH
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:\\"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        BACK_SLASH: {
            id: "device:kb-vkey:\\",
            text: "\\"
        },
        /**
         *
         * @name PIPE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:|"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        PIPE: {
            id: "device:kb-vkey:|",
            text: "|"
        },
        /**
         *
         * @name SEMICOLON
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:;"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        SEMICOLON: {
            id: "device:kb-vkey:;",
            text: ";"
        },
        /**
         *
         * @name COLON
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey::"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        COLON: {
            id: "device:kb-vkey::",
            text: ":"
        },
        /**
         *
         * @name SINGLE_QUOTE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:'"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        SINGLE_QUOTE: {
            id: "device:kb-vkey:'",
            text: "'"
        },
        /**
         *
         * @name QUOTE
         * @type {object}
         * @property {String} id Virtual key Id. 'device:kb-vkey:"'
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        /*jshint quotmark:single */
        QUOTE: {
            id: 'device:kb-vkey:"',
            text: '"'
        },
        /*jshint quotmark:double */
        /**
         *
         * @name COMMA
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:,"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        COMMA: {
            id: "device:kb-vkey:,",
            text: ","
        },
        /**
         *
         * @name LESS
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:<"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        LESS: {
            id: "device:kb-vkey:<",
            text: "<"
        },
        /**
         *
         * @name PERIOD
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:."
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        PERIOD: {
            id: "device:kb-vkey:.",
            text: "."
        },
        /**
         *
         * @name GREATER
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:>"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        GREATER: {
            id: "device:kb-vkey:>",
            text: ">"
        },
        /**
         *
         * @name SLASH
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:/"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        SLASH: {
            id: "device:kb-vkey:/",
            text: "/"
        },
        /**
         *
         * @name QUESTION
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:?"
         * @property {String} text Text value of key
         * @memberof module:ax/device/shared/keyboardVKey
         */
        QUESTION: {
            id: "device:kb-vkey:?",
            text: "?"
        },
        /**
         *
         * @name BACKSPACE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:back-space"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        BACKSPACE: {
            id: "device:kb-vkey:backspace"
        },
        /**
         *
         * @name TAB
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:tab"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        TAB: {
            id: "device:kb-vkey:tab"
        },
        /**
         *
         * @name INSERT
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:insert"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        INSERT: {
            id: "device:kb-vkey:insert"
        },
        /**
         *
         * @name DELETE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:delete"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        DELETE: {
            id: "device:kb-vkey:delete"
        },
        /**
         *
         * @name HOME
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:home"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        HOME: {
            id: "device:kb-vkey:home"
        },
        /**
         *
         * @name END
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:end"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        END: {
            id: "device:kb-vkey:end"
        },
        /**
         *
         * @name PAGE_UP
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:page-up"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        PAGE_UP: {
            id: "device:kb-vkey:page-up"
        },
        /**
         *
         * @name PAGE_DOWN
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:page-down"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        PAGE_DOWN: {
            id: "device:kb-vkey:page-down"
        },
        /**
         *
         * @name ESCAPE
         * @type {object}
         * @property {String} id Virtual key Id. "device:kb-vkey:page-escape"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        ESCAPE: {
            id: "device:kb-vkey:escape"
        },
        /**
         *
         * @name UP
         * @type {object}
         * @see {@link module:ax/device/vKey.UP}
         * @property {String} id Virtual key Id. "device:kb-vkey:up"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        UP: vKey.UP,
        /**
         *
         * @name DOWN
         * @type {object}
         * @see {@link module:ax/device/vKey.DOWN}
         * @property {String} id Virtual key Id. "device:kb-vkey:down"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        DOWN: vKey.DOWN,
        /**
         *
         * @name LEFT
         * @type {object}
         * @see {@link module:ax/device/vKey.LEFT}
         * @property {String} id Virtual key Id. "device:kb-vkey:left"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        LEFT: vKey.LEFT,
        /**
         *
         * @name RIGHT
         * @type {object}
         * @see {@link module:ax/device/vKey.RIGHT}
         * @property {String} id Virtual key Id. "device:kb-vkey:right"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        RIGHT: vKey.RIGHT,
        /**
         *
         * @name PAUSE
         * @type {object}
         * @see {@link module:ax/device/vKey.PAUSE}
         * @property {String} id Virtual key Id. "device:kb-vkey:pause"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        PAUSE: vKey.PAUSE,
        /**
         *
         * @name OK
         * @type {object}
         * @see {@link module:ax/device/vKey.OK}
         * @property {String} id Virtual key Id. "device:kb-vkey:ok"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        OK: vKey.OK,
        /**
         *
         * @name KEY_0
         * @type {object}
         * @see {@link module:ax/device/vKey.KEY_0}
         * @property {String} id Virtual key Id. "device:kb-vkey:0"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_0: vKey.KEY_0,
        /**
         *
         * @name KEY_1
         * @type {object}
         * @see {@link module:ax/device/vKey.KEY_1}
         * @property {String} id Virtual key Id. "device:kb-vkey:1"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_1: vKey.KEY_1,
        /**
         *
         * @name KEY_2
         * @type {object}
         * @see {@link module:ax/device/vKey.KEY_2}
         * @property {String} id Virtual key Id. "device:kb-vkey:2"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_2: vKey.KEY_2,
        /**
         *
         * @name KEY_3
         * @type {object}
         * @see {@link module:ax/device/vKey.KEY_3}
         * @property {String} id Virtual key Id. "device:kb-vkey:3"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_3: vKey.KEY_3,
        /**
         *
         * @name KEY_4
         * @type {object}
         * @see {@link module:ax/device/vKey.KEY_4}
         * @property {String} id Virtual key Id. "device:kb-vkey:4"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_4: vKey.KEY_4,
        /**
         *
         * @name KEY_5
         * @type {object}
         * @see {@link module:ax/device/vKey.OK}
         * @property {String} id Virtual key Id. "device:kb-vkey:5"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_5: vKey.KEY_5,
        /**
         *
         * @name KEY_6
         * @type {object}
         * @see {@link module:ax/device/vKey.PAUSE}
         * @property {String} id Virtual key Id. "device:kb-vkey:6"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_6: vKey.KEY_6,
        /**
         *
         * @name KEY_7
         * @type {object}
         * @see {@link module:ax/device/vKey.OK}
         * @property {String} id Virtual key Id. "device:kb-vkey:7"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_7: vKey.KEY_7,
        /**
         *
         * @name KEY_8
         * @type {object}
         * @see {@link module:ax/device/vKey.PAUSE}
         * @property {String} id Virtual key Id. "device:kb-vkey:8"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_8: vKey.KEY_8,
        /**
         *
         * @name KEY_9
         * @type {object}
         * @see {@link module:ax/device/vKey.OK}
         * @property {String} id Virtual key Id. "device:kb-vkey:9"
         * @memberof module:ax/device/shared/keyboardVKey
         */
        KEY_9: vKey.KEY_9
    };
});
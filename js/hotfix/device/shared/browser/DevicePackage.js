/**
 * The browser device package
 * @class ax/device/shared/browser/DevicePackage
 * @augments ax/device/interface/DevicePackage
 */
define("ax/device/shared/browser/DevicePackage", [
    "ax/class",
    "ax/core",
    "ax/console",
    "ax/device/playerRegistry",
    "ax/device/interface/Player",
    "ax/util",
    "ax/config",
    "ax/promise",
    "require",
    "ax/device/interface/DevicePackage",
    "ax/device/basicInterfaces"
], function (
    klass, 
    core, 
    console, 
    playerRegistry,
    IPlayer,
    util,
    config,
    promise,
    require,
    IDevicePackage,
    basicInterfaces
) {
    "use strict";
    var devicePackage = klass.createAbstract([IDevicePackage], {}, {
        
        setup: function(onDeviceLoaded) {
            this.__onDeviceReady = onDeviceLoaded;
            console.info("set up abstractDevicePackage");
            core.root.onunload = this.deinit;

            return this;
        },
        /**
         * To get the unique indicator for each device package
         * @return {String} the id name e.g workstation
         *
         * @method getId
         * @abstract
         * @memberof ax/device/shared/browser/DevicePackage#
         */
        getId: klass.abstractFn,
        /**
         * To get the default player. Default will be [] array.
         * @method getDefaultPlayer
         * @returns {String[]} Array of the player list
         * @memberof ax/device/shared/browser/DevicePackage#
         */
        getDefaultPlayer: function () {
            return [];
        },
        /**
         * To unlod the playerRegistry and deinit all the player
         * @method deinit
         * @memberof ax/device/shared/browser/DevicePackage#
         */
        deinit: function () {
            var device = require("ax/device");

            console.info("[XDK] DOM Unloaded");

            playerRegistry.deinit();
            
            //deinit device module
            device.deinit();
        }
    });
    return devicePackage;
});
/**
 * The browser keyMap key which is normally used in the tvKey to get the normal remote keys and keyboard keys information
 * @module ax/device/shared/browser/keyMap
 **/
define("ax/device/shared/browser/keyMap", ["ax/device/shared/keyboardVKey", "ax/util"], function (keyboardVKey, util) {
    "use strict";
    var REMOTE, KEYBOARD_CHAR, KEYBOARD_KEY, i, key;
    /*
     * * The REMOTE keycode object, which contain the following keys with keyCode. and all of them are keyCode
     * * UP : 38
     * * DOWN : 40
     * * LEFT : 37
     * * RIGHT : 39
     * * OK : 13
     * * KEY_1 : 48
     * * KEY_2 :49
     * * KEY_3 : 50
     * * KEY_4 : 51
     * * KEY_5 : 52
     * * KEY_6 : 53
     * * KEY_7 : 54
     * * KEY_8 : 55
     * * KEY_9 : 56
     * It provide two mapping keyCode to keyboardVKey/VKey or word to keyCode, please refer to the example
     * @name REMOTE
     * @constant
     * @memberof module:ax/device/shared/browser/keyMap
     * @public
     * @example
     * REMOTE = {
     * VKey:{
     * 38: keyboardVKey.UP,
     * 39: keyboardVKey.RIGHT,
     * 40: keybaordVKey.DOWN,
     * 37: keyboardVKey.LEFT
     * },
     * keyCode:{
     * "UP":38,
     * "DOWN":40,
     * "LEFT":37,
     * "RIGHT:"39
     * }
     * }
     */
    REMOTE = {
        VKey: {},
        keyCode: {
            "UP": 38,
            "DOWN": 40,
            "LEFT": 37,
            "RIGHT": 39,
            "OK": 13
        }
    };
    /*
     * The keyboard key down key code mapping (and char code of keyboard, please refer {@see module:ax/device/shared/browser/keyMap#KEYBOARD_CHAR} ) , it provides 2 key mappings which are keyCode to VKey/KeyboardVKey and word to keyCode.
     * * UP : 38
     * * DOWN : 40
     * * LEFT : 37
     * * RIGHT : 39
     * * OK : 13
     * * PAGE_UP : 34
     * * PAGE_DOWN :35
     * * END : 36
     * * HOME : 19
     * * TAB : 9
     * * INSERT : 45
     * * DELETE : 46
     * * BACKSPACE : 8
     * * ESCAPE : 27
     * It provide two mapping keyCode to keyboardVKey/VKey or word to keyCode, please refer to the example
     * @name KEYBOARD_KEY
     * @constant
     * @memberof module:ax/device/shared/browser/keyMap
     * @public
     * @example
     * KEYBOARD_KEY = {
     * VKey:{
     * 38: keyboardVKey.UP,
     * 39: keyboardVKey.RIGHT,
     * 40: keybaordVKey.DOWN,
     * 37: keyboardVKey.LEFT
     * },
     * keyCode:{
     * "UP":38,
     * "DOWN":40,
     * "LEFT":37,
     * "RIGHT:"39
     * }
     * }
     */
    KEYBOARD_KEY = {
        VKey: {},
        keyCode: {
            "UP": 38,
            "DOWN": 40,
            "LEFT": 37,
            "RIGHT": 39,
            "OK": 13,
            "PAGE_UP": 33,
            "PAGE_DOWN": 34,
            "END": 35,
            "HOME": 36,
            "PAUSE": 19,
            "TAB": 9,
            "INSERT": 45,
            "DELETE": 46,
            "BACKSPACE": 8,
            "ESCAPE": 27
        }
    };
    /*
     *The keyboard key down char code mapping (and key code of keyboard, please refer {@see module:ax/device/shared/browser/keyMap#KEYBOARD_KEY} ) , it provides 2 key mappings which are keyCode to VKey/KeyboardVKey and word to keyCode.
     * * SPACE : 32,
     * * EXC : 33,
     * * HASH : 35,
     * * DOLLAR : 36,
     * * PERCENT : 37,
     * * AMP : 38,
     * * SINGLE_QUOTE : 39,
     * * OPEN_PAREN : 40,
     * * CLOSE_PAREN : 41,
     * * STAR : 42,
     * * PLUS : 43,
     * * COMMA : 44,
     * * MINUS : 45,
     * * PERIOD : 46,
     * * SLASH : 47,
     * * COLON : 58,
     * * SEMICOLON : 59,
     * * LESS : 60,
     * * EQUALS : 61,
     * * GREATER : 62,
     * * QUESTION : 63,
     * * AT : 64,
     * * OPEN_BRACKET : 91,
     * * BACK_SLASH : 92,
     * * CLOSE_BRACKET : 93,
     * * CARET : 94,
     * * UNDERSCORE : 95,
     * * BACK_QUOTE : 96,
     * * OPEN_BRACE : 123,
     * * PIPE : 124,
     * * CLOSE_BRACE : 125,
     * * TILDE : 126
     * Apart from the above it also provides a-z or A-Z key mapping
     * @name KEYBOARD_CHAR
     * @constant
     * @memberof module:ax/device/shared/browser/keyMap
     * @public
     * @example
     * KEYBOARD_CHAR = {
     * VKey:{
     *     32: keyboardVKey.SPACE,
     *     33: keyboardVKey.EXC,
     *     35: keybaordVKey.HASH,
     *     36: keyboardVKey.DOLLAR
     * },
     * keyCode:{
     *    "SPACE": 32,
     *    "EXC": 33,
     *    "HASH": 35,
     *    "DOLLAR": 36,
     *  }
     * }
     */
    KEYBOARD_CHAR = {
        VKey: {},
        keyCode: {
            "SPACE": 32,
            "EXC": 33,
            "HASH": 35,
            "DOLLAR": 36,
            "PERCENT": 37,
            "AMP": 38,
            "SINGLE_QUOTE": 39,
            "OPEN_PAREN": 40,
            "CLOSE_PAREN": 41,
            "STAR": 42,
            "PLUS": 43,
            "COMMA": 44,
            "MINUS": 45,
            "PERIOD": 46,
            "SLASH": 47,
            "COLON": 58,
            "SEMICOLON": 59,
            "LESS": 60,
            "EQUALS": 61,
            "GREATER": 62,
            "QUESTION": 63,
            "AT": 64,
            "OPEN_BRACKET": 91,
            "BACK_SLASH": 92,
            "CLOSE_BRACKET": 93,
            "CARET": 94,
            "UNDERSCORE": 95,
            "BACK_QUOTE": 96,
            "OPEN_BRACE": 123,
            "PIPE": 124,
            "CLOSE_BRACE": 125,
            "TILDE": 126
        }
    };

    util.each(REMOTE.keyCode, function (item) {
        REMOTE.VKey[item.value] = keyboardVKey[item.key];
    });

    util.each(KEYBOARD_CHAR.keyCode, function (item) {
        KEYBOARD_CHAR.VKey[item.value] = keyboardVKey[item.key];
    });

    util.each(KEYBOARD_KEY.keyCode, function (item) {
        KEYBOARD_KEY.VKey[item.value] = keyboardVKey[item.key];
    });
    for (i = 65; i <= 90; i++) { // A-Z
        KEYBOARD_CHAR.VKey[i] = keyboardVKey[String.fromCharCode(i)];
        KEYBOARD_CHAR.keyCode[String.fromCharCode(i)] = i;
    }

    for (i = 97; i <= 122; i++) { // a-z
        KEYBOARD_CHAR.VKey[i] = keyboardVKey[String.fromCharCode(i).toUpperCase() + "_LOWER"];
        KEYBOARD_CHAR.keyCode[String.fromCharCode(i).toUpperCase() + "_LOWER"] = i;
    }

    for (i = 48; i <= 57; i++) { // 1-0
        key = "KEY_" + (i - 48);
        KEYBOARD_CHAR.VKey[i] = keyboardVKey[key];
        KEYBOARD_CHAR.keyCode[key] = i;
        REMOTE.keyCode[key] = i;
        REMOTE.VKey[i] = keyboardVKey[key];
    }



    return {
        "REMOTE": REMOTE,
        "KEYBOARD_KEY": KEYBOARD_KEY,
        "KEYBOARD_CHAR": KEYBOARD_CHAR
    };
});
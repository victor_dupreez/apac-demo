/**
 * Multiple audio track strategy for Panasonic HTML5 player.
 * @class ax/device/shared/Html5PlayerMultiAudioTracksStgy
 * @augments ax/device/interface/MultipleAudioTrackStgy
 */
define("ax/device/shared/Html5PlayerMultiAudioTracksStgy", [
    "ax/class", "ax/device/interface/MultipleAudioTrackStgy", "ax/promise",
    "ax/core", "ax/util", "ax/exception"
], function (klass, MultipleAudioTrackStgy, promise,
    core, util, exception) {

    "use strict";

    var Strategy;

    Strategy = klass.create([MultipleAudioTrackStgy], {}, {
        /**
         * Player instance
         * @name _player
         * @protected
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        _player: null,

        /**
         * The current audio track id
         * @name _currentTrackIndex
         * @protected
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        _currentTrackIndex: null,

        /**
         * Number of seconds between every retry.
         * @name _retryInterval
         * @protected
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        _retryInterval: 1,

        /**
         * Number of trials before getAudioTracks failed.
         * @name _retryLimit
         * @protected
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        _retryLimit: 10,

        /**
         * Initialize the strategy, get called by the _new_ keyword.
         * @method
         * @param {Object} player the player instance that will use this multi audio track strategy
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        init: function (player) {
            this._player = player;
        },

        /**
         * Get ids of Audio Tracks. It will delay 10s when the info is not ready yet.
         * Tested on panasonic 2013 devices only which support playready.
         * @method
         * @returns {Promise.<String[]>} A list of audio track object with id
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        getAudioTracks: function () {
            if (!this._player.isMetadataLoaded()) {
                // metadata (come with loadedmetadata event) is not ready, try again after a while
                return util.delay(this._retryInterval).then(util.bind(this._tryGetAudioTracks, this, 1));
            }

            return this._getAudioTrackIds();
        },

        /**
         * Set audio track by id
         * @method
         * @param {String} id The id of the audio track
         * @returns {Promise.<Boolean>} Return the result after call in a promise
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} if the given parameter is invalid
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} if the player is not ready yet
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        setAudioTrack: function (id) {
            if (!this._player.isMetadataLoaded()) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "The video is not ready yet."));
            }

            var index;

            // parse the input from string to int (to be the array index)
            try {
                index = this._convertIdToIndex(id);
            } catch (e) {
                return promise.reject(e);
            }

            // already using the target track, no further action
            if (this._currentTrackIndex === index) {
                return promise.resolve(true);
            }

            this._enableAudioTrack(index);

            return promise.resolve(true);
        },

        /**
         * Get an attribute from a specific audio track.
         * @method
         * @param {String} id The id of the audio track
         * @param {String} attr The media audio track attribute {@link ax/device/interface/MultipleAudioTrackStgy}
         * @returns {Promise.<String>} Return the result of the property
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} if the given parameter is invalid
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} if the player is not ready yet
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        getAudioTrackAttr: function (id, attr, defaultValue) {
            if (!this._player.isMetadataLoaded()) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "The video is not ready yet."));
            }

            var audioTracks = this._player._playerObject.audioTracks,
                audioTrack,
                index;

            // parse the input from string to int (to be the array index)
            try {
                index = this._convertIdToIndex(id);
            } catch (e) {
                return promise.reject(e);
            }

            // retrieve the audio track
            audioTrack = audioTracks[index];

            switch (attr) {
            case MultipleAudioTrackStgy.LANGCODE:
                return promise.resolve(audioTrack.language);
            default:
                if (!util.isUndefined(defaultValue)) {
                    return promise.resolve(defaultValue);
                }
            }

            // no associated attribute
            return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "No available attribute"));
        },

        /* Get the id of the current audio track.
         * @method
         * @returns {Promise.<String>} The id of the audio track. Some audio track is unable to obtain the current track id, it will return null.
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        getCurrentAudioTrackId: function () {
            return promise.resolve(this._player._playerObject.audioTracks[this._currentTrackIndex].id);
        },

        /**
         * Enable the specific audio track.
         * @method
         * @protected
         * @param {Number} index The idnex of the target audio track
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        _enableAudioTrack: function (index) {
            var audioTracks = this._player._playerObject.audioTracks,
                currentTime;

            // save the current time for later resume after track switch
            currentTime = Math.floor(this._player.getCurTime());

            // Multiple audio tracks cannot be enabled (=true) simultaneously.
            audioTracks[this._currentTrackIndex].enabled = false;
            audioTracks[index].enabled = true;

            // seek to the previous position
            // without the seek, the player will freeze at the last position
            this._player.seek(currentTime);

            // update the index
            this._currentTrackIndex = index;
        },

        /**
         * Retrieve the audio tracks from the player object then return the id of the tracks.
         * This should be access only when the video metadata is ready.
         * @method
         * @protected
         * @returns {Promise.<String[]>} A list of audio track ids
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        _getAudioTrackIds: function () {
            var audioTracks = this._player._playerObject.audioTracks,
                ids = [],
                i,
                length;

            if (audioTracks) {
                for (i = 0, length = audioTracks.length; i < length; i++) {
                    var track = audioTracks[i];

                    if (track.enabled) {
                        this._currentTrackIndex = i;
                    }

                    ids.push(track.id);
                }
            }

            return promise.resolve(ids);
        },

        /**
         * Parse the id from outside world to internal track index.
         * @method
         * @protected
         * @param {String} id The track id
         * @returns {Number} The array index of the track
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} if the id is not valid
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        _convertIdToIndex: function (id) {
            var audioTracks = this._player._playerObject.audioTracks;

            if (!util.isString(id)) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "Track id is invalid.");
            }

            for (var i = audioTracks.length - 1; i >= 0; i--) {
                if (audioTracks[i].id === id) {
                    return i;
                }
            }

            throw core.createException(exception.ILLEGAL_ARGUMENT, "Track id is invalid.");
        },

        /**
         * Retry to get the audio tracks.
         * @method
         * @protected
         * @param {Number} trials The number of attempts that have been tried
         * @returns {Promise.<String[]>} A list of audio track ids
         * @throws {Promise.<module:ax/exception.INTERNAL>} if timeout reached
         * @memberof ax/device/shared/Html5PlayerMultiAudioTracksStgy#
         */
        _tryGetAudioTracks: function (trials) {
            if (!this._player.isMetadataLoaded()) {
                if (trials >= this._retryLimit) {
                    return promise.reject(core.createException(exception.INTERNAL, "Failed to retrieve audio track information."));
                }

                return util.delay(this._retryInterval).then(util.bind(this._tryGetAudioTracks, this, ++trials));
            }

            return this._getAudioTrackIds();
        }
    });

    return Strategy;

});
/**
 * A helper to generate the unique id and store into the device storage.
 * @class ax/device/helper/storageUnqiueId
 */
define("ax/device/helper/storageUniqueId", [
    "require",
    "ax/core",
    "ax/exception"
], function (
    require,
    core,
    exception
) {
    "use strict";
    /**
     * Used as the key to save uuid in {@link ax/device/AbstractStorage|storage}
     * @name UUID_KEY
     * @constant
     * @memberof ax/device/helper/storageUnqiueId
     * @private
     */
    var UUID_KEY = "__UUID__",
        /**
         * To store the unique id
         * @name uuid
         * @memberof ax/device/helper/storageUnqiueId
         * @private
         */
        uuid = null;
    return {
        /**
         * return the unique id and saved to storage, so it will alway return the same id
         * @method getUniqueID
         * @public
         * @returns {String} the device serial number
         * @memberof ax/device/helper/storageUnqiueId
         */
        getUniqueID: function () {
            /*jshint bitwise:false*/
            var device, i, random;
            device = require("ax/device");

            //To ensure the storage and enviornment is ready to use
            if (!device || !device.storage) {
                throw core.createException(exception.ILLEGAL_STATE, "device storage is not ready and fail to generate unique id");
            }

            if (uuid === null) {
                uuid = device.storage.get(UUID_KEY);

                if (!uuid) {
                    uuid = "";
                    for (i = 0; i < 32; i++) {
                        random = Math.random() * 16 | 0;

                        if (i === 8 || i === 12 || i === 16 || i === 20) {
                            uuid += "-";
                        }
                        uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
                            .toString(16);
                    }

                    device.storage.set(UUID_KEY, uuid);
                }
            }

            return uuid;
            /*jshint bitwise:true*/
        }
    };
});
/**
 * The Storage interface. Implement this interfaces to indicate the method to store the data e.g.like cookies, localStorage.
 *
 * @class ax/device/interface/Storage
 * @author Mike Leung <mike.leung@accedo.tv>
 */

define("ax/device/interface/Storage", ["ax/Interface"], function(Interface) {
    "use strict";
    return Interface.create("Storage", {
        /**
         * Sets a value into the storage for the specified key.
         * @name set
         * @function
         * @abstract
         * @param {String} key the key to store the value to
         * @param {mixed} value the value to store
         * @memberof ax/device/interface/Storage
         */
        set: [],
        /**
         * Gets a value from the storage for the specified key.
         * @name get
         * @function
         * @abstract
         * @param {String} key the key to store the value to
         * @returns {mixed} the retrieved value.Return false if key doesn't exist in storage.
         * @memberof ax/device/interface/Storage
         */
        get: ["key"],
        /**
         * Clears a entry from the storage for the specified key.
         * @name unset
         * @function
         * @abstract
         * @param {String} key the key to store the value to
         * @memberof ax/device/interface/Storage
         */
        unset: [],
        /**
         * Clears all entries from the storage.
         * @name clear
         * @function
         * @abstract
         * @memberof ax/device/interface/Storage
         */
        clear: []
    });
});
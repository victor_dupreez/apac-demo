/**
 * The Id interface. Implement this interfaces to identification information of a device.
 *
 * @class ax/device/interface/Id
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("ax/device/interface/Id", ["ax/Interface"], function (Interface) {
    "use strict";
    var Id = Interface.create("Id", {
        /**
         * Get the device type: emulator, tv, bdp, ht (home theatre), stb.
         * @name getDeviceType
         * @function
         * @abstract
         * @deprecated replaced with getHardwareType function which standardize among the different platforms
         * @return {String} device type
         * @memberof ax/device/interface/Id
         * @public
         */
        getDeviceType: [],
        /**
         * Get the hardware type: {@link ax/device/interface/Id.HARDWARE_TYPE}
         * @name getHardwareType
         * @function
         * @abstract
         * @return {ax/device/interface/Id.HARDWARE_TYPE} the hardware type
         * @memberof ax/device/interface/Id
         * @public
         */
        getHardwareType: [],
        /**
         * Get the MAC address.
         * @name getMac
         * @function
         * @abstract
         * @return {String} mac address. null if not available.
         * @memberof ax/device/interface/Id
         * @public
         */
        getMac: [],
        /**
         * Get the system's firmware version.
         * @name getFirmware
         * @function
         * @abstract
         * @return {String} firmware version."dummyFirmware" if not available.
         * @memberof ax/device/interface/Id
         * @public
         */
        getFirmware: [],
        /**
         * Get the year from the system's firmware version.
         * @name getFirmwareYear
         * @function
         * @abstract
         * @memberof ax/device/interface/Id
         * @return {Number} firmware year Return 0 if not available.
         * @public
         */
        getFirmwareYear: [],
        /**
         * Get a uniqueID of the device. UUID is gernerated and stored in localStorage to pretent a
         * unique if device API is not available.
         * @name getUniqueID
         * @function
         * @abstract
         * @return {String} unique ID
         * @memberof ax/device/interface/Id
         * @public
         */
        getUniqueID: [],
        /**
         * Get device's model number.
         * @name getModel
         * @function
         * @abstract
         * @return {String} "dummyModel" if not available.
         * @memberof ax/device/interface/Id
         * @public
         */
        getModel: [],
        /**
         * Get device's internal IP.If there are no related api, it will return 0.0.0.0 and
         * developer may need to send ajax to public api service to get the ip.
         * @name getIP
         * @function
         * @abstract
         * @memberof ax/device/interface/Id
         * @return {String} ip address."0.0.0.0" if it's not available.
         * @public
         */
        getIP: []
    });

    /**
     * Collection of the device hardware type.  
     * To provide the possible hardware type like BD, TV, MONITOR, HOME_THERTRE, EMULATOR, STB, CONSOLE, WORKSTATION, UNKNOWN etc...
     * It gives a information of the hardware information and used in device.id.getHardwareType
     * @enum {String}
     * @memberof ax/device/interface/Id
     */
    Id.HARDWARE_TYPE = {
        /** Bluray player */
        BD: "Bluray",
        /**  TV */
        TV: "TV",
        /**  MONITOR */
        MONITOR: "Monitor",
        /**  HOME THEATRE */
        HOME_THEATRE: "Home Theatre",
        /**  EMULATOR */
        EMULATOR: "Emulator",
        /**  WORKSTATION */
        WORKSTATION: "Workstation",
        /**  Set Top Box */
        STB: "Set Top Box",
        /**  game console like playstation, wiiu */
        CONSOLE: "Console",
        /**  game console monitor like wiiu gamepad monitor*/
        CONSOLE_MONITOR: "Console Monitor",
        /**  Unknown which is unable to determine the device type among various devices*/
        UNKNOWN: "Unknown"
    };

    return Id;
});
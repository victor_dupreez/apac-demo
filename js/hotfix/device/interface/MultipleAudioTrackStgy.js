/**
 * MultipleAudioTrackStgy Interface is to provide multiple audio track function for media like play ready smooth streaming.
 * 
 * ###Configuration Parameters
 *
 *  Attribute | Value
 * --------- | ---------
 * Key:    | device.multiAudioTrackStgy
 * Desc    | To set the multiAudioTrackStgy to the specific video player
 * Type    | String
 * Default  | (NA)
 * Usage | {@link module:ax/config}
 * @class ax/ext/device/samsung/DevicePackage
 * @extends ax/device/AbstractDevicePackage
 * @name MultipleAudioTrackStgy
 * @class ax/device/interface/MultipleAudioTrackStgy
 * @example
 * //in the xdk.config.js
 * "device.multiAudioTrackStgy": {
 *       "ax/ext/device/samsung/SefPlayer": "ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy",
 *       "ax/ext/device/ps3/PSPlayer": "ax/ext/device/ps3/PSPlayerMultiAudioTracksStgy"
 *       "ax/ext/device/playstation/PSPlayer": "ax/ext/device/playstation/PSPlayerMultiAudioTracksStgy"
 * }
 *
 * //in the usage part
 * sMedia.getAudioTracks().then(function(tracks){
 *   if(tracks.length>0){
 *       sMedia.setAudiotrack(tracks[0]); // set the audio track to be first one.
 *       sMedia.getAudioTrackAttr(tracks[0], MultipleAudioTrackStgy.LANGCODE, "eng"); //get the langCode of the first audio track.
 *   }
 * },function(){
 *     //no audio track supports or fail to get the audio tracks
 * });
 *
 * sMedia.getCurrentAudioTrackId().then(function(id){
 *     //get the current audio track id, It may be null since some devices may not able to get the current track id at the first time.
 * })
 *
 */
define("ax/device/interface/MultipleAudioTrackStgy", ["ax/Interface"], function(Interface) {
    "use strict";
    var MultipleAudioTrackStgy = Interface.create("MultipleAudioTrackStgy", {
        /**
         * Get ids of Audio Tracks.
         * @method getAudioTracks
         * @public
         * @returns {Promise.<String[]>} id The id of the track which is mainly for switching the audio track via setAudioTrack
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} unsupported feature
         * @memberof ax/device/interface/MultipleAudioTrackStgy
         */
        getAudioTracks: [],
        /**
         * Set audio track by id
         * @method setAudioTrack
         * @public
         * @param {String} id The id of the audio track
         * @returns {Promise.<Boolean>} Return the result after call in a promise
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} missing parameter.
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} unsupported
         * @memberof ax/device/interface/MultipleAudioTrackStgy
         */
        setAudioTrack: ["id"],
        /* Get audio track attribute
         * @method getAudioTrackAttr
         * @public
         * @param {String} id The id of the audio track
         * @param {String} attr The attribute name {@link ax/device/interface/MultipleAudioTrackStgy.LANGCODE}
         * @param {String} defaultValue The default value of the attribute
         * @returns {Promise.<String>} return the retrieved value or default value if unable to find on the device and given that default value
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} no correct id
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} device is not ready yet
         * @memberof ax/device/interface/MultipleAudioTrackStgy
         */
        getAudioTrackAttr: ["id", "attr", "defaultValue"],
        /* Get current audio track id
         * @method getCurrentAudioTrackId
         * @public
         * @returns {Promise.<String|null>} The id of the audio track. Some audio track is unable to obtain the current track id, it will return null.
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} device is not ready yet
         * @memberof ax/device/interface/MultipleAudioTrackStgy
         */
        getCurrentAudioTrackId: []
    });
    /**
     * The language code according to <http://en.wikipedia.org/wiki/ISO_639> and return the 3 digit code
     * @name LANGCODE
     * @constant
     * @type {String}
     * @memberof ax/device/interface/MultipleAudioTrackStgy
     */
    MultipleAudioTrackStgy.LANGCODE = "LANGCODE";

    return MultipleAudioTrackStgy;

});
/**
 * The drm agent creator interface.
 *
 * @class ax/device/interface/DrmAgentCreator
 */
define(["ax/Interface"], function (Interface) {
    "use strict";
    return Interface.create("DrmAgentCreator", {
        /**
         * Create the drm agent. e.g create the object to handle the drm.
         * @name createDrmAgent
         * @function
         * @abstract
         * @param {String} drm the type of drm
         * @return {Promise.<ax/device/interface/DrmAgent>} the drm agent
         * @memberof ax/device/interface/DrmAgentCreator
         * @public
         */
        createDrmAgent: ["drm"]
    });
});
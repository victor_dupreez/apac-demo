/**
 * The external subtitle strategy interface, which will be awared by subtitle.
 *
 * @class ax/device/interface/ExtSubtitleStgy
 * @extends ax/device/interface/SubtitleStgy
 */
define("ax/device/interface/ExtSubtitleStgy", [
    "ax/Interface",
    "ax/device/interface/SubtitleStgy"
], function (
    Interface,
    SubtitleStgy
) {
    "use strict";
    var ExtSubtitleStgy = Interface.create("ExtSubtitleStgy", [SubtitleStgy], {
        /**
         * Prepare for the subtitle, like download the external source or embedded the external subtitle into the player
         * @name prepare
         * @function
         * @abstract
         * @memberof ax/device/interface/ExtSubtitleStgy
         * @public
         */
        prepare: ["subtitleOption"]
    });

    /**
     * The type constant for external subtitle strategy.
     * @property {String} 
     * @memberof ax/device/interface/ExtSubtitleStgy
     */
    ExtSubtitleStgy.TYPE = "external";

    return ExtSubtitleStgy;
});
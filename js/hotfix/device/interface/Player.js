/**
 * The media player interface
 * Implement this interfaces and register to {@link ax/device/playerRegistry|Player Registry}.
 * @class ax/device/interface/Player
 * @author Mike Leung <mike.leung@accedo.tv>
 */

define("ax/device/interface/Player", ["ax/Interface"], function (Interface) {
    "use strict";
    /**
     * @typedef PlayerCapabilites
     * @inner
     * @desc the player's capabilities
     * @type {object}
     * @property {Array} drms Player supported DRM
     * @property {Array} type Player'supported container format. e.g. ["mp4","hls"]
     * @inner
     * @memberof ax/device/interface/Player
     * @example
     * {
     *      "drms" : ["aes128", "widevine"],
     *      "type" : ["hls", "mp4"]
     * }
     */

    /**
     * @typedef MediaBitrates
     * @inner
     * @desc the media's bitrates
     * @type {object}
     * @property {Array} [currentBitrate] current playback bitrate
     * @property {Array} [availableBitrates] available playback bitrates
     * @inner
     * @memberof ax/device/interface/Player
     *
     */

    var IPlayer = Interface.create("Player", {
        /**
         * Gets the player's capabilities
         * @method
         * @abstract
         * @return {ax/device/interface/Player~PlayerCapabilites} the player's capabilities
         * @memberof ax/device/interface/Player
         */
        getCapabilities: [],
        /**
         * Prepare the player, should be called before using
         * @method
         * @abstract
         * @param {string} type The media type to be played
         * @param {string} [codec] The media codec
         * @param {string} [drm] DRM to be used
         * @memberof ax/device/interface/Player
         */
        prepare: ["opts"],
        /**
         * Reset the player, should be called when it is to replaced by other player
         * @method
         * @abstract
         * @memberof ax/device/interface/Player
         */
        reset: [],
        /**
         * Loads the specified media
         * @method
         * @abstract
         * @param {String} url the URL address of the media
         * @param {Object} [opts] the options for loading this media
         * @param {string} [opts.codec] the media codec
         * @param {string} [opts.type] explicitly specify the media type
         * @param {string} [opts.drm] DRM to be used
         * @memberof ax/device/interface/Player
         */
        load: ["mediaUrl", "opts"],
        /**
         * Start the playback
         * @method
         * @abstract
         * @param {Object} [opts] the options for starting playback
         * @param {Number} [opts.sec] the playback start time
         * @memberof ax/device/interface/Player
         */
        play: ["opts"],
        /**
         * Stop the playback
         * @method
         * @abstract
         * @memberof ax/device/interface/Player
         */
        stop: [],
        /**
         * Pause the playback
         * @method
         * @abstract
         * @memberof ax/device/interface/Player
         */
        pause: [],
        /**
         * Resume the playback
         * @method
         * @abstract
         * @memberof ax/device/interface/Player
         */
        resume: [],
        /**
         * Seek to specifiy position of the video
         * @method
         * @abstract
         * @param {Number} sec the position to seek to in seconds
         * @memberof ax/device/interface/Player
         */
        seek: ["sec"],
        /**
         * Skip the playback forward/backward for certain seconds
         * @method
         * @abstract
         * @param {Number} sec number of seconds to skip (10 by default)
         * @memberof ax/device/interface/Player
         */
        skip: ["sec"],
        /**
         * Speed up/down the media playback
         * @method
         * @abstract
         * @param {Number} speed the playback speed to set
         * @memberof ax/device/interface/Player
         */
        speed: ["speed"],
        /**
         * Get the playback speed
         * @method
         * @abstract
         * @return {Number} the playback speed
         * @memberof ax/device/interface/Player
         */
        getPlaybackSpeed: [],
        /**
         * Sets video window size
         * @method
         * @abstract
         * @param {Object} param window size parameter
         * @param {Integer} param.top window top
         * @param {Integer} param.left window left
         * @param {Integer} param.width window width
         * @param {Integer} param.height window height
         * @memberof ax/device/interface/Player
         */
        setWindowSize: ["param"],
        /**
         * Sets video window size to be fullscreen
         * @method
         * @abstract
         * @memberof ax/device/interface/Player
         */
        setFullscreen: [],
        /**
         * Get the media bitrates
         * @method
         * @abstract
         * @return {ax/device/interface/Player~MediaBitrates} current bitrate and available bitrates
         * @memberof ax/device/interface/Player
         */
        getBitrates: [],
        /**
         * Get the current playback time
         * @method
         * @abstract
         * @return {Number} the current playback time
         * @memberof ax/device/interface/Player
         */
        getCurTime: [],
        /**
         * Get the total playback time
         * @method
         * @abstract
         * @return {Number} the total playback time
         * @memberof ax/device/interface/Player
         */
        getDuration: [],
        /**
         * Get back the path
         * @method
         * @public
         * @return {String} the path of the player
         * @memberof ax/device/interface/Player
         */
        getId: [],
        /**
         * set the id of the player
         * @method
         * @public
         * @param {String} id the path of the player
         * @memberof ax/device/interface/Player
         */
        setId: ["id"]
    });


    /**
     * The playback states of the player.
     * @enum {String}
     * @memberof ax/device/interface/Player
     */
    IPlayer.PLAYBACK_STATE = {
        /**
         * The playback is playing, this also includes skipping, speeding, bufferring.
         */
        PLAYING: "playing",
        /**
         * The playback is paused, this also includes bufferring.
         */
        PAUSED: "paused",
        /**
         * The playback is stopped, or not started yet.
         */
        STOPPED: "stopped"
    };

    var PLAYBACK_ERRORS_GROUPS = {
        RENDER: "onRenderError",
        NETWORK: "onConnectionError",
        DRM: "onDRMError",
        GENERIC: "onUnknownError"
    };
    /**
     * Default player errors.
     * @enum {Object}
     * @memberof ax/device/interface/Player
     */
    IPlayer.PLAYBACK_ERRORS = {

        RENDER: {
            //general
            FAILED: {
                evt: PLAYBACK_ERRORS_GROUPS.RENDER,
                code: 1001,
                id: "player.error.render.failed",
                desc: "Stream Playback Generic Error"
            },
            ABORTED: {
                evt: PLAYBACK_ERRORS_GROUPS.RENDER,
                code: 1002,
                id: "player.error.render.aborted",
                desc: "You aborted the video playback"
            },
            DECODE: {
                evt: PLAYBACK_ERRORS_GROUPS.RENDER,
                code: 1003,
                id: "player.error.render.decode",
                desc: "Decoding error, The video playback was aborted due to a corruption problem or because the video used features your browser did not support."
            },
            CORRUPTED: {
                evt: PLAYBACK_ERRORS_GROUPS.RENDER,
                code: 1004,
                id: "player.error.render.corrupted",
                desc: "Corrupted Stream Error"
            },
            UNSUPPORTED: {
                evt: PLAYBACK_ERRORS_GROUPS.RENDER,
                code: 1005,
                id: "player.error.render.unsupported",
                desc: "Unsupported Video Format, The media resource indicated by the src attribute or assigned media provider object was not suitable."
            },
            //specific
            DEVICE: {
                evt: PLAYBACK_ERRORS_GROUPS.RENDER,
                code: 1006,
                id: "player.error.render.device",
                desc: "Device system Error, out of memory or other reason."
            },
            PLAYLIST: {
                evt: PLAYBACK_ERRORS_GROUPS.RENDER,
                code: 1007,
                id: "player.error.render.playlist",
                desc: "Unrecognized play list or Play list is empty"
            }
        },

        NETWORK: {
            //general
            FAILED: {
                evt: PLAYBACK_ERRORS_GROUPS.NETWORK,
                code: 2001,
                id: "player.error.network.failed",
                desc: "A network error caused the video download to fail part-way"
            },
            FILE: {
                evt: PLAYBACK_ERRORS_GROUPS.NETWORK,
                code: 2002,
                id: "player.error.network.file",
                desc: "File is not found or network error while trying to load the file"
            },
            //specific
            TIMEOUT: {
                evt: PLAYBACK_ERRORS_GROUPS.NETWORK,
                code: 2003,
                id: "player.error.network.timeout",
                desc: "Network Timeout"
            },
            DISCONNECTED: {
                evt: PLAYBACK_ERRORS_GROUPS.NETWORK,
                code: 2004,
                id: "player.error.network.disconnected",
                desc: "Network Disconnected"
            },
            PROTOCOL: {
                evt: PLAYBACK_ERRORS_GROUPS.NETWORK,
                code: 2005,
                id: "player.error.network.protocol",
                desc: "Invalid Protocol"
            }
        },

        DRM: {
            //general
            FAILED: {
                evt: PLAYBACK_ERRORS_GROUPS.DRM,
                code: 3001,
                id: "player.error.drm.failed",
                desc: "Authentication Failed, general message"
            },
            //specific
            NOLICENCE: {
                evt: PLAYBACK_ERRORS_GROUPS.DRM,
                code: 3002,
                id: "player.error.drm.nolicense",
                desc: "No License"
            },
            INVALID: {
                evt: PLAYBACK_ERRORS_GROUPS.DRM,
                code: 3003,
                id: "player.error.drm.invalid",
                desc: "Invalid license"
            }
        },

        GENERIC: {
            //general
            UNKNOWN: {
                evt: PLAYBACK_ERRORS_GROUPS.GENERIC,
                code: 4001,
                id: "player.error.generic.unknown",
                desc: "Unidentified Error on Playback"
            },
            LOAD: {
                evt: PLAYBACK_ERRORS_GROUPS.GENERIC,
                code: 4002,
                id: "player.error.generic.load",
                desc: "Exception caught when trying to load the media"
            }
        }
    };


    return IPlayer;


});
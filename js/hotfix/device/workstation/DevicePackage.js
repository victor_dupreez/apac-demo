/**
 * DevicePack class to handle the packaging for the workstation
 * @class ax/device/workstation/DevicePackage
 * @extends ax/device/AbstractDevicePackage
 */
define("ax/device/workstation/DevicePackage", [
    "ax/device/AbstractDevicePackage",
    "ax/device/shared/LocalStorage",
    "ax/device/workstation/TvKey",
    "ax/device/workstation/Id",
    "ax/device/workstation/System",
    "ax/device/Media",
    "ax/class",
    "ax/util"
], function (
    AbsDevicePackage,
    LocalStorage,
    TvKey,
    Id,
    System,
    Media,
    klass,
    util
) {
    "use strict";
    return klass.create(AbsDevicePackage, {}, {
        /**
         * [Backward Compatible only] To be true when device package version >= 2.3
         * @property {Boolean} shouldSetup Should this setup method in AbstractDevicePackage run
         * @name _shouldSetup
         * @memberof ax/device/workstation/DevicePackage#
         */
        _shouldSetup: true,
        /**
         * To return platform id of this abstraction
         * @method getId
         * @returns {String} the id of the workstation device package
         * @memberof ax/device/workstation/DevicePackage#
         * @public
         */
        getId: function () {
            return "workstation";
        },
        /**
         * To init the workstation device package and packed the item need to be initialize
         * @method setup
         * @param {Function} onDeviceLoaded Device ready callback
         * @returns {ax/device/workstation/DevicePackage} device package itself.
         * @memberof ax/device/workstation/DevicePackage#
         * @public
         */
        setup: function (onDeviceLoaded) {
            var interfaceTypes;

            this._super(onDeviceLoaded);

            interfaceTypes = this.getInterfaceTypes();

            util.each(interfaceTypes, util.bind(function (pair) {
                this.addInterfaceType(pair.key, pair.value.handle, pair.value.interfaceObj);
            }, this));

            this.ready();
        },
        /**
         * To get the default player. Default will be {@link ax/device/shared/Html5Player}
         * @method getDefaultPlayer
         * @returns {String[]} Array of the player list
         * @memberof ax/device/workstation/DevicePackage#
         * @public
         */
        getDefaultPlayer: function () {
            return ["ax/device/shared/Html5Player"];
        },
        /**
         * To init each modules like id, sMedia, system and tvkey
         * @method getInterfaceTypes
         * @memberof ax/device/workstation/DevicePackage#
         * @public
         */
        getInterfaceTypes: function () {
            var ret = {};
            ret[AbsDevicePackage.STORAGE] = {
                handle: AbsDevicePackage.STORAGE_HANDLE,
                interfaceObj: new LocalStorage()
            };

            ret[AbsDevicePackage.MEDIA_PLAYER] = {
                handle: AbsDevicePackage.MEDIA_PLAYER_HANDLE,
                interfaceObj: Media.singleton()
            };

            ret[AbsDevicePackage.TV_KEY] = {
                handle: AbsDevicePackage.TV_KEY_HANDLE,
                interfaceObj: new TvKey()
            };

            ret[AbsDevicePackage.ID] = {
                handle: AbsDevicePackage.ID_HANDLE,
                interfaceObj: new Id()
            };

            ret[AbsDevicePackage.SYSTEM] = {
                handle: AbsDevicePackage.SYSTEM_HANDLE,
                interfaceObj: new System()
            };

            return ret;
        }
    });
});
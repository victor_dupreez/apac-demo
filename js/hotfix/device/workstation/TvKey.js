/**
 * TvKey class to handle the key
 * @class ax/device/workstation/TvKey
 * @extends ax/device/AbstractTvKey
 */
define("ax/device/workstation/TvKey", ["ax/device/shared/browser/TvKey", "ax/class", "ax/device/vKey", "ax/util", "ax/device/shared/browser/keyMap"], function (abstrBrowserTvKey, klass, VKey, util, keyMap) {
    "use strict";
    return klass.create(abstrBrowserTvKey, {}, {
        init: function () {
            //To get the keyboard keyCode since workstation support the keyboard
            var keyMapping = util.clone(keyMap.KEYBOARD_KEY, true);

            //extra device specific keys like color keys and back
            keyMapping.VKey[118] = VKey.RED; //F7
            keyMapping.VKey[119] = VKey.GREEN; //F8
            keyMapping.VKey[120] = VKey.YELLOW; //F9
            keyMapping.VKey[121] = VKey.BLUE; //F10
            keyMapping.VKey[27] = VKey.BACK; //the key esc
            keyMapping.VKey[145] = VKey.EXIT; //The scroll lock key

            //set the keyMap
            this.initKeyMapping(keyMapping, keyMap.KEYBOARD_CHAR);
        }
    });
});
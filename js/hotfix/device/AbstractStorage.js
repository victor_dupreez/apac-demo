/**
 * Storage class to indicate the method to store the data e.g.like cookies, localStorage
 *
 * ###Config Params
 *
 * Attribute | Value
 * --------- | ---------
 * Key      | device:storage:prefix
 * Type     | String
 * Desc     | Unique prefix string for the underlying storage to uniquely map entries to the current App like cookies
 * Default  | "AccedoXDK"
 * Usage    | {@link module:ax/config}
 *
 * @class ax/device/AbstractStorage
 * @augments ax/device/interface/Storage
 */
define("ax/device/AbstractStorage", ["ax/class", "ax/config", "ax/device/interface/Storage", "require"], function (
    klass, config,
    IStorage, require) {
    "use strict";
    return klass.createAbstract([IStorage], {}, {
        /**
         * Unique prefix string for the underlying storage to uniquely map entries to the current App like cookies
         * @name _uniquePrefix
         * @memberof ax/device/AbstractStorage#
         * @protected
         */
        _uniquePrefix: config.get("device.storage.prefix", "AccedoXDK"),
        /**
         * Gets a value from the storage for the specified key.
         * @name get
         * @function
         * @param {String} key the key to store the value to
         * @returns {mixed} the retrieved value.Return false if key doesn't exist in storage.
         * @memberof ax/device/AbstractStorage#
         */
        /*jshint unused:false */
        get: function (key) {
            return false;
        },
        /*jshint unused:true */
        /**
         * Clears all entries from the storage.
         * @name clear
         * @function
         * @memberof ax/device/AbstractStorage#
         */
        clear: function () {

            var device = require("ax/device"),
                uuid, UUID_KEY = device.id.__UUID_KEY;

            uuid = this.get(UUID_KEY);

            this._doClear();

            this.set(UUID_KEY, uuid);
        }
    });
});
/**
 * FallbackSuspendedPlayback is a fallback version of SuspendedPlayback used by {@link ax/device/Media} for the player that has not implemented {@link ax/device/interface/Suspendable.SuspendedPlayback}.
 *
 * @class ax/device/FallbackSuspendedPlayback
 * @memberof ax/deivce/Media
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define("ax/device/FallbackSuspendedPlayback", [
    "ax/class",
    "ax/device/interface/Suspendable",
    "ax/device/AbstractPlayer",
    "ax/promise",
    "ax/util"
], function (
    klass,
    Suspendable,
    AbstractPlayer,
    promise,
    util
) {
    "use strict";

    return klass.create([Suspendable.SuspendedPlayback], {}, {
        /**
         * Constructs a new FallbackSuspendedPlayback.
         * The Media instance is needed such that circular dependency can be avoided.
         *
         * @method
         * @param {ax/device/Media} media The Media instance
         * @param {ax/device/interface/Player} player The player
         * @param {Object} metadata The metadata for playback resume
         * @param {String} metadata.url The media url
         * @param {Object} [metadata.opts] The media loading options
         * @param {Number} [metadata.pos = 0] The resume position
         * @param {String} [metadata.state] The playback state
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        init: function (media, player, metadata) {
            this.__media = media;
            this.__player = player;
            this.__url = metadata.url;
            this.__opts = metadata.opts || {};
            this.__pos = metadata.pos || 0;
            this.__state = metadata.state;
        },

        /**
         * Restores the suspended playback.<br/>
         * The playback states before suspension will retain.
         *
         * @method
         * @returns {Promise} Resolve when finishes
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        restore: function () {
            var deferred = promise.defer();
            var sMedia = this.__media;
            var loadOpts = util.extend(util.extend({}, this.__opts, true), {
                player: this.__player
            });

            // one time event listener to handle the playback state
            var onStateChange = util.bind(function (evt) {
                switch (evt.toState) {
                case sMedia.PLAYING:
                    if (this.__state === AbstractPlayer.PLAYBACK_STATE.PAUSED) {
                        sMedia.pause();
                    }

                    deferred.resolve();
                    sMedia.__stateMachine.removeEventListener(sMedia.EVT_STATE_CHANGED, onStateChange);
                    break;
                }
            }, this);

            sMedia.__stateMachine.addEventListener(sMedia.EVT_STATE_CHANGED, onStateChange);

            sMedia.load(this.__url, loadOpts);

            if (this.__state === AbstractPlayer.PLAYBACK_STATE.STOPPED) {
                return promise.resolve();
            }

            sMedia.play({
                sec: this.__pos
            });

            return deferred.promise;
        },

        /**
         * Disposes the suspended playback, destroying he playback states before suspension.<br/>
         * A disposed playback cannot be restored anymore.
         *
         * @method
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        dispose: function () {
            this.__player = null;
        },

        /**
         * Returns the media information.
         *
         * @method
         * @returns {MediaMeta} The media metadata
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        getMediaMeta: function () {
            return {
                url: this.__url,
                opts: this.__opts
            };
        },

        /**
         * Returns the player instance who is responsible for restoration.
         *
         * @method
         * @returns {ax/device/interface/Player} The player instance
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        getPlayer: function () {
            return this.__player;
        }
    });
});
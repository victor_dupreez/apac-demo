/**
 * Abstract TV key base class. Adds key handling functionality for a device.
 * @class ax/device/AbstractTvKey
 */
define("ax/device/AbstractTvKey", ["ax/class"], function (klass) {
    "use strict";
    var tvKeyKlass = klass.createAbstract({}, {
        /**
         * init function to be called once interface is loaded
         * @name init
         * @function
         * @memberof ax/device/AbstractTvKey#
         * @private
         */
        init: function () {}
    });
    return tvKeyKlass;
});
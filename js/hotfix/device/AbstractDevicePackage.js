/**
 * The abstract Device Package, can be used as a starting point of building any device package.
 * @class ax/device/AbstractDevicePackage
 */
define("ax/device/AbstractDevicePackage", [
    "ax/class",
    "ax/core",
    "ax/console",
    "ax/device/playerRegistry",
    "ax/device/interface/Player",
    "ax/util",
    "ax/config",
    "ax/promise",
    "require",
    "ax/device/interface/DevicePackage"
], function (
    klass,
    core,
    console,
    playerRegistry,
    IPlayer,
    util,
    config,
    promise,
    require,
    IDevicePackage) {
    "use strict";
    var PACKAGE_ENUM = {
            STORAGE: "Storage",
            STORAGE_HANDLE: "storage",
            MEDIA_PLAYER: "Media",
            MEDIA_PLAYER_HANDLE: "media",
            TV_KEY: "TvKey",
            TV_KEY_HANDLE: "tvkey",
            ID: "Id",
            ID_HANDLE: "id",
            CONSOLE: "Console",
            CONSOLE_HANDLE: "console",
            SYSTEM: "System",
            SYSTEM_HANDLE: "system"
        },
        devicePackage = klass.createAbstract([IDevicePackage], {
            /**
             * Name for Storage interface
             * @constant
             * @name STORAGE
             * @memberof ax/device/AbstractDevicePackage
             */
            STORAGE: PACKAGE_ENUM.STORAGE,
            /**
             * Name for Storage Handle
             * @constant
             * @name STORAGE_HANDLE
             * @memberof ax/device/AbstractDevicePackage
             */
            STORAGE_HANDLE: PACKAGE_ENUM.STORAGE_HANDLE,
            /**
             * Name for Media interface
             * @constant
             * @name MEDIA_PLAYER
             * @memberof ax/device/AbstractDevicePackage
             */
            MEDIA_PLAYER: PACKAGE_ENUM.MEDIA_PLAYER,
            /**
             * Name for Media Handle
             * @constant
             * @name MEDIA_PLAYER_HANDLE
             * @memberof ax/device/AbstractDevicePackage
             */
            MEDIA_PLAYER_HANDLE: PACKAGE_ENUM.MEDIA_PLAYER_HANDLE,
            /**
             * Name for TV KEY interface
             * @constant
             * @name TV_KEY
             * @memberof ax/device/AbstractDevicePackage
             */
            TV_KEY: PACKAGE_ENUM.TV_KEY,
            /**
             * Name for TV KEY HANDLE
             * @constant
             * @name TV_KEY_HANDLE
             * @memberof ax/device/AbstractDevicePackage
             */
            TV_KEY_HANDLE: PACKAGE_ENUM.TV_KEY_HANDLE,
            /**
             * Name for ID interface
             * @constant
             * @name ID
             * @memberof ax/device/AbstractDevicePackage
             */
            ID: PACKAGE_ENUM.ID,
            /**
             * Name for ID handle
             * @constant
             * @name ID_HANDLE
             * @memberof ax/device/AbstractDevicePackage
             */
            ID_HANDLE: PACKAGE_ENUM.ID_HANDLE,
            /**
             * Name for Console interface
             * @constant
             * @name CONSOLE
             * @memberof ax/device/AbstractDevicePackage
             */
            CONSOLE: PACKAGE_ENUM.CONSOLE,
            /**
             * Name for Console handle
             * @constant
             * @name CONSOLE_HANDLE
             * @memberof ax/device/AbstractDevicePackage
             */
            CONSOLE_HANDLE: PACKAGE_ENUM.CONSOLE_HANDLE,
            /**
             * Name for System interface
             * @constant
             * @name SYSTEM
             * @memberof ax/device/AbstractDevicePackage
             */
            SYSTEM: PACKAGE_ENUM.SYSTEM,
            /**
             * Name for System interface
             * @constant
             * @name SYSTEM
             * @memberof ax/device/AbstractDevicePackage
             */
            SYSTEM_HANDLE: PACKAGE_ENUM.SYSTEM_HANDLE,
            /**
             * Basic interfaces for a proper device package
             * @constant
             * @name BASIC_INTERFACES
             * @memberof ax/device/AbstractDevicePackage
             */
            BASIC_INTERFACES: [{
                name: PACKAGE_ENUM.STORAGE,
                handle: PACKAGE_ENUM.STORAGE_HANDLE
            }, {
                name: PACKAGE_ENUM.MEDIA_PLAYER,
                handle: PACKAGE_ENUM.MEDIA_PLAYER_HANDLE
            }, {
                name: PACKAGE_ENUM.TV_KEY,
                handle: PACKAGE_ENUM.TV_KEY_HANDLE
            }, {
                name: PACKAGE_ENUM.ID,
                handle: PACKAGE_ENUM.ID_HANDLE
            }, {
                name: PACKAGE_ENUM.SYSTEM,
                handle: PACKAGE_ENUM.SYSTEM_HANDLE
            }],
            /**
             * [Backward Compatible only] To be called when the device package is loaded
             * @deprecated
             * @method _onDeviceReady
             * @memberof ax/device/AbstractDevicePackage
             * @protected
             */
            _onDeviceReady: null,
            /**
             * [Backward Compatible only] To be true when device package version >= 2.3
             * @property {Boolean} Should this setup method in AbstractDevicePackage run
             * @name _shouldSetup
             * @memberof ax/device/AbstractDevicePackage
             * @protected
             */
            _shouldSetup: false,
            /**
             * [Backward Compatible only] To set the callback before init the device package instead of set in the constructor to avoid being overrided.
            
             * @method prepare
             * @param {Function} callback Device ready callback
             * @returns {ax/device/AbstractDevicePackage} abstract device package itself
             * @deprecated
             * @memberof ax/device/AbstractDevicePackage
             * @public
             */
            prepare: function (callback) {
                devicePackage._onDeviceReady = callback;
            }
        }, {
            /**
             * To be called when the device package is loaded
             * @since 2.1 to replace static onDeviceReady variable
             * @name __onDeviceReady
             * @memberof ax/device/AbstractDevicePackage#
             * @private
             */
            __onDeviceReady: null,
            /**
             * To store the interface information like the handle and obj
             * @name __interfaceTypes
             * @memberof ax/device/AbstractDevicePackage#
             * @private
             */
            __interfaceTypes: {},
            /**
             * To store the reference which will be then append to the device
             * @name __device
             * @memberof ax/device/AbstractDevicePackage#
             * @private
             */
            __device: {},
            /**
             * [Backward Compatible only] To set the callback
             * @method init
             * @deprecated constructor should not pass the callback. callback should pass in the set up function.
             * @param {Function} callback Device ready callback
             * @returns {ax/device/AbstractDevicePackage} abstract device package itself
             * @memberof ax/device/AbstractDevicePackage#
             * @public
             */
            init: function (onDeviceLoaded) {
                this.__onDeviceReady = onDeviceLoaded;
                console.info("init of abstractDevicePackage");
                return this;
            },
            /**
             * setup the device package
             * @method setup
             * @param {Function} onDeviceLoaded callback when the device is loaded.
             * @returns {ax/device/AbstractDevicePackage} abstract device package itself
             * @memberof ax/device/AbstractDevicePackage#
             * @public
             */
            setup: function(onDeviceLoaded) {
                if (!this._shouldSetup) {
                    return;
                }

                this.__onDeviceReady = onDeviceLoaded;
                console.info("set up abstractDevicePackage");
                core.root.onunload = this.deinit;

                return this;
            },
            /**
             * To add the interface into the abstraction which later will be appended into the device
             *
             * @method addInterfaceType
             * @param {String} type Interface type
             * @param {Function} handle Interface handle
             * @param {Object} interfaceObj Interface Object
             * @memberof ax/device/AbstractDevicePackage#
             * @public
             */
            addInterfaceType: function (type, handle, interfaceObj) {
                this.__interfaceTypes[type] = {
                    handle: handle,
                    interfaceObj: interfaceObj
                };

                this.__device[handle] = interfaceObj;
            },
            /**
             * Get the interface by type
             * @method getInterface
             * @param {String} type Interface type
             * @return {Object|Null} interfaceObj Interface Object
             * @memberof ax/device/AbstractDevicePackage#
             * @public
             */
            getInterface: function (type) {
                if (this.__interfaceTypes[type]) {
                    return this.__interfaceTypes[type].interfaceObj;
                }
                return null;
            },
            /**
             * To get the unique indicator for each device package
             * @return {String} the id name e.g workstation
             *
             * @method getId
             * @abstract
             * @memberof ax/device/AbstractDevicePackage#
             * @public
             */
            getId: klass.abstractFn,
            /**
             * To go through the checking and ensure the device module is attached to the device
             *
             * @method ready
             * @param {Boolean} [isPaused=false] whether the device load again from pause state
             * @memberof ax/device/AbstractDevicePackage#
             * @public
             */
            ready: function (isPaused) {
                isPaused = isPaused || false;
                if (this.__onDeviceReady && util.isFunction(this.__onDeviceReady)) {
                    this.__onDeviceReady(this.__device, isPaused);
                    return;
                }

                // backward compatible only
                devicePackage._onDeviceReady(this.__device, isPaused);
            },
            /**
             * To load the player from the playlist set in the config
             * @method _preparePlayerList
             * @param {Function} cb callback function to be called after prepared playlist
             * @deprecated it will perform playlist handling when the device package is ready inside the device
             * @memberof ax/device/AbstractDevicePackage#
             * @protected
             */
            _preparePlayerList: function (cb) {
                cb();
            },
            /**
             * To get the default player. Default will be [] array.
             * @method getDefaultPlayer
             * @returns {String[]} Array of the player list
             * @memberof ax/device/AbstractDevicePackage#
             * @public
             */
            getDefaultPlayer: function () {
                return [];
            },
            /**
             * To get the default player. Default will be [] array.
             * @method _getDefaultPlayer
             * @returns {String[]} Array of the player list
             * @deprecated replaced with {@link ax/device/AbstractDevicePackage#getDefaultPlayer}
             * @memberof ax/device/AbstractDevicePackage#
             * @protected
             */
            _getDefaultPlayer: function () {
                return [];
            },
            /**
             * To unlod the playerRegistry and deinit all the player
             *
             * @method deinit
             * @memberof ax/device/AbstractDevicePackage#
             */
            deinit: function () {
                var device = require("ax/device");

                console.info("[XDK] DOM Unloaded");

                playerRegistry.deinit();

                //deinit device module
                device.deinit();
            }
        });
    return devicePackage;
});
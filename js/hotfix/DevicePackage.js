/**
 * DevicePack class to handle the packaging for the tizen.
 * This device package is for the tizen web app and not support for tizen native app or Hybrid app.
 * Since it is based on the real device, it may not be compatible on emulator or simulator. (Refer XDK-2580 for more details)
 *
 * ###Media Player
 * * {@link ax/ext/device/tizen/Html5Player}
 * Support format: MP4, ASF
 *
 * * {@link ax/ext/device/tizen/AVPlayer}
 * Support format: MP4, HLS, ASF, Playready, widevine
 * Default: ax/ext/device/tizen/AVPlayer
 *
 * ###External Files
 * External resources are available in src/res folder
 *
 * 1. config.xml - tizen use config.xml as a configuration file.
 * It can set the application(widget) name, version and privilege..etc.
 * Since the tizen device package access the device api, some privileges are set in the config.xml
 * Usage : Copy to the root folder (e.g src folder). Please make sure the location of the config.xml may conflict with the samsung config.xml
 * Reference : <https://developer.tizen.org/dev-guide/2.2.1/org.tizen.web.appprogramming/html/app_dev_process/set_widget_web.htm>
 *
 * ###Adopt, develop and package the tizen application
 *
 * 1. Obtain the Tizen IDE and set the security profiles (which is the author certificate) in the prefernces>Samsung TV SDK>Security Profiles
 * 2. Create a new Tizen Project with Basic template basic application
 * 3. Import the src folder
 * 4. Build the package to create the .wgt file
 *
 * ###Privilege
 * In order to make application working properly, Tizen device packge will include the following privileges.
 * If missing any privileges in the config.xml, it may fail to initialize the device package properly or block some usages.
 *
 * 1. "http://tizen.org/privilege/tv.inputdevice" To get the input and key event
 * 2. "http://developer.samsung.com/privilege/network.public" To get the network information and status
 * 3. "http://developer.samsung.com/privilege/productinfo" To get the device product information
 * 4. "http://tizen.org/privilege/tv.audio" To control the audio of the TV like mute
 * 5. "http://developer.samsung.com/privilege/drmplay" To support the drm for the AVPlayer, if it is not included, it will fail to play the drm.
 * 6. "http://developer.samsung.com/privilege/drminfo" To get the drm info when playing widevine drm.
 *
 * ###Configuration for the resource
 * XDK default config.xml for tizen won't set any access in the Content Security Policy.  Developers need to manually configure the permission in order  
 * to access the external resources like images, video, ajax, etc.  
 * Without setting those permission, application itself will fail to load external resources and materials.  
 * e.g If you are going to allow the permisson to allow all requests for specific domain, you can simply mention in the properties of config.xml    
 * <access origin="http://www.accedo.tv" subdomains="true"/>  
 * For more specific rules, you may also use `<tizen:content-security-policy>` property.  
 * Reference: <https://developer.tizen.org/dev-guide/2.2.1/org.tizen.web.appprogramming/html/basics_tizen_programming/web_security_privacy.htm#content>
 *
 * ###Resources
 *
 * * <https://accedobroadband.jira.com/wiki/display/XDKDEV/Samsung+Tizen+TV>
 * * <https://accedobroadband.jira.com/wiki/pages/viewpage.action?pageId=68288776>
 * * <https://developer.tizen.org/>
 *
 *
 * @class ax/ext/device/tizen/DevicePackage
 * @extends ax/device/AbstractDevicePackage
 * @author Thomas Lee <thomas.lee@accedo.tv>
 */
define("ax/ext/device/tizen/DevicePackage", [
    "ax/device/shared/browser/DevicePackage",
    "ax/device/shared/LocalStorage",
    "ax/ext/device/tizen/TvKey",
    "ax/ext/device/tizen/Id",
    "ax/ext/device/tizen/System",
    "ax/device/Media",
    "ax/class",
    "ax/config",
    "ax/util",
    "ax/device/basicInterfaces",
    "ax/promise"
], function (
    browserDevicePackage,
    LocalStorage,
    TvKey,
    Id,
    System,
    Media,
    klass,
    config,
    util,
    basicInterfaces,
    promise
) {
    "use strict";

    var system = new System(),
        id = new Id(),
        storage = new LocalStorage();

    return klass.create(browserDevicePackage, {}, {
        /**
         * To return platform id of this device package
         * @method
         * @return {String} the id name
         * @memberof ax/ext/device/tizen/DevicePackage#
         * @protected
         */
        getId: function () {
            return "tizen";
        },
        /**
         * To setup the tizen device package and then call ready to invoke the device module
         * @method setup
         * @param {Function} onDeviceLoaded Device ready callback
         * @returns {ax/device/tizen/DevicePackage} device package itself.
         * @memberof ax/ext/device/tizen/DevicePackage#
         * @protected
         */
        setup: function (onDeviceLoaded) {
            var interfaces, isPaused;

            this._super(onDeviceLoaded);

            system.getInitPromise().then(util.bind(function () {

                interfaces = this.__setupInterfaces();

                isPaused = storage.get(system.LIFE_CYCLE_IS_PAUSED, false);
                if (isPaused) {
                    onDeviceLoaded(interfaces, true);
                    return;
                }

                onDeviceLoaded(interfaces, false);

            }, this)).done();
        },
        /**
         * To get the default player. Default will be {@link ax/ext/device/tizen/Html5Player}
         * @method getDefaultPlayer
         * @returns {String[]} Array of the player list
         * @memberof ax/ext/device/tizen/DevicePackage#
         */
        getDefaultPlayer: function () {
            return ["ax/ext/device/tizen/AVPlayer"];
        },
        /**
         * To init each modules like id, sMedia, system and tvkey
         * @method __setupInterfaces
         * @memberof ax/device/amazon/DevicePackage#
         * @private
         */
        __setupInterfaces: function () {
            var intfs = {};

            intfs[basicInterfaces.STORAGE] = storage;
            intfs[basicInterfaces.MEDIA_PLAYER] = Media.singleton();
            intfs[basicInterfaces.TV_KEY] = new TvKey();
            intfs[basicInterfaces.ID] = id;
            intfs[basicInterfaces.SYSTEM] = system;

            return intfs;
        },
        /**
         * To deinit the device pacakge
         * @method deinit
         * @protected
         * @memberof ax/ext/device/tizen/DevicePackage#
         */
        deinit: function () {

            storage.unset(system.LIFE_CYCLE_IS_PAUSED);

            this._super();
        }
    });
});
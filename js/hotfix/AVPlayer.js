/*globals webapis:true*/
/**
 * The avplayer using webapis.avplay which is the tizen player supported by samsung. It provides video playback
 * and support various drms and encryption.
 * @class ax/ext/device/tizen/AVPlayer
 */
define("ax/ext/device/tizen/AVPlayer", [
    "ax/class",
    "ax/device/interface/Player",
    "ax/exception",
    "ax/core",
    "ax/util",
    "ax/config",
    "ax/device/Media",
    "ax/console",
    "ax/Env"
], function (
    klass,
    IPlayer,
    exception,
    core,
    util,
    config,
    Media,
    console,
    Env
) {
    "use strict";
    var sMedia, avplayer, sEnv,
        resolution = null,
        getResolution = function () {
            resolution = resolution || require("ax/device").system.getDisplayResolution();
            return resolution;
        },
        PLAYBACK_ERRORS = IPlayer.PLAYBACK_ERRORS,
        PLAYREADY_TYPE = "PLAYREADY",
        WIDEVINE_TYPE = "WIDEVINE";

    return klass.create([IPlayer], {}, {
        /**
         * To store the player id
         * @private
         * @name __id
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __id: null,
        /**
         * To store the status whether the object is prepared
         * @private
         * @name __prepared
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __prepared: false,
        /**
         * To determine whether the avplayer is loaded or not
         * @private
         * @name __loaded
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __loaded: false,
        /**
         * To store the status whether the player is connected
         * @private
         * @name __connected
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __connected: false,
        /**
         * Time out(sec) for the player connection
         * @private
         * @name __connectionTimeLimit
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __connectionTimeLimit: config.get("device.player.connection-timeout", 90),
        /**
         * Time out object for the player connection
         * @private
         * @name __connectionTimeOut
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __connectionTimeOut: null,
        /**
         * player current time
         * @private
         * @name __currentTime
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __currentTime: 0,
        /**
         * player playback speed
         * @private
         * @name __playbackSpeed
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __playbackSpeed: 1,
        /**
         * withhold play
         * @private
         * @name __withholdPlay
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __withholdPlay: false,
        /**
         * player seek time
         * @private
         * @name __seekTime
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __seekTime: 0,
        init: function () {
            sMedia = Media.singleton();
            sEnv = Env.singleton();
            if (!webapis || !webapis.avplay) {
                throw core.exception(core.createException(exception.ILLEGAL_STATE, "fail to load the avplayer api"));
            }
            avplayer = webapis.avplay;
        },
        /**
         * To get the capabilities of the player
         * @method getCapabilities
         * @returns {ax/device/interface/Player~PlayerCapabilites}
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        getCapabilities: function () {
            return {
                type: ["mp4", "asf", "hls", "has", "mp3", "dash"],
                drms: ["aes128", "playready", "widevine"]
            };
        },
        /**
         * to prepare the video and create the drm agent if necessary
         * @method prepare
         * @param {Object}  [opts]  extra prarameter needed for the URL
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        prepare: function (opts) {
            if (this.__prepared) {
                return;
            }

            opts = opts || {};

            if (!opts.parentNode) {
                opts.parentNode = document.body;
            }

            this.__parentNode = opts.parentNode;

            //prepare for the player obejct
            this.__playerContainer = document.createElement("div");
            this.__playerContainer.className = "playerContainer";
            this.__playerContainer.innerHTML = "<OBJECT id='av-player' type='application/avplayer' style='position:absolute;width:100%;height:100%;'></OBJECT>";

            this.__parentNode.appendChild(this.__playerContainer);

            this.__playerObject = document.getElementById("av-player");

            //set listener in the idle state

            var operationResult = avplayer.setListener({
                onbufferingstart: util.bind(this.__onBufferingStart, this),
                onbufferingprogress: util.bind(this.__onBufferingProgress, this),
                onbufferingcomplete: util.bind(this.__onBufferingComplete, this),
                oncurrentplaytime: util.bind(this.__onCurrentPlayTime, this),
                oneventcallback: util.bind(this.__onEventCallback, this),
                onsubtitlecallback: util.bind(this.__onSubtitleCallback, this),
                ondrmcallback: util.bind(this.__onDRMCallback, this),
                onstreamcompleted: util.bind(this.__onFinished, this)
            });
            console.info("[avplayer.js][prepare] add listener result " + operationResult);

            this.__prepared = true;

            //handle the player suspend and restore
            sEnv.addEventListener(sEnv.EVT_ONRESUME, this.__handleResume);
            sEnv.addEventListener(sEnv.EVT_ONPAUSE, this.__handlePause);
        },

        /**
         * pause the avplayer when it is suspended.
         * @method __handlePause
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __handlePause: function () {
            if (avplayer.suspend) {
                avplayer.suspend();
            }
        },
        /**
         * restore the avplayer when it is resumed. If video was playing when it is paused, video will play from the last played url and time.
         * @method __handleResume
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __handleResume: function () {
            if (avplayer.restore) {
                avplayer.restore();
            }
        },
        /**
         * resets the video player, to non-playing mode
         * @method
         * @protected
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        reset: function () {
            this.__hide();
            this.__parentNode.removeChild(this.__playerContainer);
            this.__prepared = false;
            this.__connected = false;
            this.__loaded = false;

            sEnv.removeEventListener(sEnv.EVT_ONRESUME, this.__handleResume);
            sEnv.removeEventListener(sEnv.EVT_ONPAUSE, this.__handlePause);
        },
        /**
         * perform the do Load which set the url and possible attributes with respect to the drm.
         * @method
         * @param {String} mediaUrl the media url
         * @param {Object} opts the media options
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __doLoad: function (mediaUrl, opts) {
            //from none state to idle state
            var operationResult = avplayer.open(mediaUrl),
                drmParam = {},
                drmParamString;

            console.info("[AVplayer.js][load] initialize the player result " + operationResult);

            if (opts.drm === "playready") {
                //try to set the configuration for the playready
                if (!util.isUndefined(opts.drmUrl)) {
                    drmParam.LicenseServer = opts.drmUrl;
                }

                if (!util.isUndefined(opts.customData)) {
                    drmParam.CustomData = opts.customData;
                }

                if (!util.isUndefined(opts.httpHeader)) {
                    drmParam.HttpHeader = opts.httpHeader;
                }

                if (!util.isUndefined(opts.soapHeader)) {
                    drmParam.soapHeader = opts.soapHeader;
                }

                if (!util.isUndefined(opts.cookie)) {
                    drmParam.Cookie = opts.cookie;
                }

                if (!util.isUndefined(opts.deleteLicenseAfterUse)) {
                    drmParam.DeleteLicenseAfterUse = opts.deleteLicenseAfterUse;
                }

                drmParamString = util.stringify(drmParam);

                //set the properties when it is not empty
                if (drmParamString !== "{}") {
                    avplayer.setDrm(PLAYREADY_TYPE, "SetProperties", drmParamString);
                    console.info("[AVPlayer.js][load] set the properties " + drmParamString + " for the playready");
                }

            } else if (opts.drm === "widevine") {

                var drmUrl = "",
                    iSeek = "",
                    drmCurTime = "",
                    userData = "",
                    portal = "",
                    deviceType = 60,
                    deviceId = webapis.drminfo.getEsn(WIDEVINE_TYPE);

                if (!util.isUndefined(opts.drmUrl)) {
                    drmUrl = opts.drmUrl;
                }

                if (!util.isUndefined(opts.iSeek)) {
                    iSeek = opts.iSeek;
                }
                if (!util.isUndefined(opts.drmCurTime)) {
                    drmCurTime = opts.drmCurTime;
                }

                if (!util.isUndefined(opts.userData)) {
                    userData = opts.userData;
                }

                if (!util.isUndefined(opts.portal)) {
                    portal = opts.portal;
                }

                if (!util.isUndefined(opts.deviceTypeId)) {
                    deviceType = opts.deviceTypeId;
                }

                drmParamString = "DEVICE_ID=" + deviceId + "|DEVICE_TYPE_ID=" + deviceType + "|STREAM_ID=|IP_ADDR=|DRM_URL=" + drmUrl + "|PORTAL=" + portal + "|I_SEEK=" + iSeek + "|CUR_TIME=" + drmCurTime + "|USER_DATA=" + userData;
                avplayer.setStreamingProperty(WIDEVINE_TYPE, drmParamString);
                console.info("[AVPlayer.js][load] set the properties " + drmParamString + " for the widevine");

            }
        },
        /**
         * to set the url and load the video
         * @method load
         * @param {String}  mediaUrl  url the URL address of the media
         * @param {Object}  [opts]  extra prarameter needed for the URL
         * @param {String} [opts.drm] DRM technology to use
         * @param {String} [opts.type] media container format to use
         * @param {String} [opts.drmUrl] (PlayReady|Widevine) Set the DRM license url
         * @param {String} [opts.cookie] (PlayReady) Set the COOKIE information for PlayReady
         * @param {String} [opts.customData] (PlayReady) to set custom data
         * @param {String} [opts.httpHeader] (PlayReady) add custom http header
         * @param {String} [opts.soapHeader] (PlayReady) add custom soap header
         * @param {String} [opts.deleteLicenseAfterUse] (PlayReady) Enable deletion of license after use.
         * @param {String} [opts.userData] (Widevine) Set the user data
         * @param {String} [opts.deviceTypeId] (Widevine)the device type id when using widevine
         * @param {String} [opts.drmCurTime] (Widevine) cur time param when using widevine
         * @param {String} [opts.iSeek] (Widevine) i-seek param when using widevine
         * @param {String} [opts.portal] (Widevine)portal param when using widevine
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        load: function (mediaUrl, opts) {
            //return when not prepared
            if (!this.__prepared) {
                return false;
            }

            if (this.__url !== mediaUrl) {
                try {
                    avplayer.close();
                } catch (ex) {
                    // AVPlayer state machine changed in firmware 1141
                    // means calling close too early will cause error
                    console.info("[AVplayer.js][load] Error occurred due to close being called too early");
                }
            }

            var operationResult;

            if (opts.parentNode && this.__parentNode !== opts.parentNode) {
                this.__parentNode.removeChild(this.__playerContainer);
                this.__parentNode = opts.parentNode;
                this.__parentNode.appendChild(this.__playerContainer);
            }

            this.__url = mediaUrl;
            this.__mediaOpts = opts;
            this.__connected = false;

            this.__type = opts.type || null;
            this.__drm = opts.drm || null;

            this.__doLoad(mediaUrl, opts);

            operationResult = avplayer.setTimeoutForBuffering(10);
            console.info("[AVplayer.js][load] set timeout for buffering result " + operationResult);

            //hide the object
            this.__hide();
            this.__loaded = true;
        },
        /**
         * to hide the player
         * @method __hide
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @private
         */
        __hide: function () {
            this.__playerContainer.style.visibility = "hidden";
        },
        /**
         * to show the player
         * @method __show
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @private
         */
        __show: function () {
            this.__playerContainer.style.visibility = "visible";
        },
        /**
         * to block the status change and handling when paused or speeding or stopped
         * @method __needStateChangeBlock
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @private
         */
        __needStateChangeBlock: function () {
            if (sMedia.isState(sMedia.PAUSED) || sMedia.isState(sMedia.SPEEDING) || sMedia.isState(sMedia.STOPPED)) {
                return true;
            }
            return false;
        },
        /**
         * on buffering start callback
         * @method __onBufferingStart
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onBufferingStart: function () {
            if (this.__needStateChangeBlock()) {
                return;
            }

            sMedia._onBufferingStart();
            console.info("[AVPlayer.js][__onBufferingStart] buffering start");

            this.__removeConnectionTimeOut();
        },
        /**
         * on buffering progress callback
         * @method __onBufferingProgress
         * @param {Number} percent the percentage of the buffering progress
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onBufferingProgress: function (percent) {
            if (this.__needStateChangeBlock()) {
                return;
            }

            sMedia._onBufferingProgress(percent);
        },
        /**
         * buffering complete callback
         * @method __onBufferingComplete
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onBufferingComplete: function () {
            //delay the buffering complete when it is not yet connected/pr
            this.__delayBufferingComplete = !this.__connected;

            if (this.__needStateChangeBlock() || !this.__connected) {
                return;
            }

            sMedia._onBufferingFinish();
        },
        /**
         * It will be fired the prepareAsync done callback after the first buffering complete
         * @method __prepareSuccess
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __prepareSuccess: function () {
            this.__connected = true;

            console.info("[AVPlayer.js][__prepareSuccess] prepare Success" + this.__seekTime + " - " + this.__withholdPlay);

            if (this.__seekTime > 0) {
                this.__withholdPlay = true;
                //XDK-2616 Fail to seekTo second in aes128, use jumpForward instead of seek.
                if (this.__drm === "aes128") {
                    avplayer.jumpForward(this.__seekTime * 1000);
                } else {
                    this.seek(this.__seekTime);
                }
                this.__seekTime = 0;
            }

            //to change the state into buffering complete which is delayed
            if (this.__delayBufferingComplete) {
                sMedia._onBufferingFinish();
            }

            if (this.__withholdPlay) {
                this.__withholdPlay = false;
                var playResult = avplayer.play();
                console.info("[AVPlayer.js][__prepareSuccess] play result " + playResult);
            }
        },
        /**
         * It will be fired the prepareAsync fail callback
         * @method __prepareFail
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __prepareFail: function (e) {
            this.__stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.RENDER.FAILED, "Fail to prepare the video", e);
        },
        /**
         * the current play time update callback
         * @method __onCurrentPlayTime
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onCurrentPlayTime: function (currentTime) {
            this.__currentTime = currentTime / 1000;
            sMedia._onTimeUpdate(this.getCurTime());
        },
        /**
         * the event callback
         * @method __onEventCallback
         * @param {String} eventType the type of event
         * @param {String} eventData data of the event
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onEventCallback: function (eventType, eventData) {
            console.info("[AVPlayer.js][__onEventCallback] Event type error : " + eventType + ", eventData: " + eventData);
        },
        /**
         * the subtitle event callback
         * @method __onSubtitleCallback
         * @param {Number} duration the time
         * @param {String} text of the subtitle
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onSubtitleCallback: function (duration, text, data3, data4) {
            console.info("[AVPlayer.js][__onSubtitleCallback] Subtitle Changed. Duration" + duration + " text : " + text + " data " + data3 + "; " + data4);
        },
        /**
         * the drm callback
         * @method __onDRMCallback
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onDRMCallback: function (drmEvent, drmData) {
            console.info("[AVPlayer.js][__onDRMCallback] DRM callback: " + drmEvent + ", data: " + drmData);
        },
        /**
         * called when the video finish
         * @method __onFinished
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onFinished: function () {
            console.info("[AVPlayer.js] onFinish");
            this.__stopPlayback();
            sMedia._onFinish();
        },
        /**
         * stop the play
         * @method __stopPlayback
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __stopPlayback: function () {
            //pause the player and avoid the on time update event
            var stopResult = avplayer.stop();
            console.info("[AVPlayer.js][__stopPlayback] call stop" + stopResult);
            this.__hide();

            //reset the witholdPlay to avoid the video to be played when buffering complete.
            this.__withholdPlay = false;

            //remove the connection time out when stop the playback
            this.__removeConnectionTimeOut();

            this.__connected = false;
            this.__loaded = false;
        },
        /**
         * throw error when connection timeout
         * @method __onConnectionTimeout
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __onConnectionTimeout: function () {
            sMedia._onError(PLAYBACK_ERRORS.NETWORK.TIMEOUT);
            console.info("[AVPlayer.js][__onConnectionTimeout] Connection error");
        },
        /**
         * remove the connection timeout
         * @method __removeConnectionTimeOut
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __removeConnectionTimeOut: function () {
            if (this.__connectionTimeOut !== null) {
                util.clearDelay(this.__connectionTimeOut);
                this.__connectionTimeOut = null;
            }
        },
        /**
         * play the video item
         * @method
         * @param {Object} opts object containing the required options
         * @param {Number} opts.sec Play the video at the specified second
         * @public
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        play: function (opts) {
            console.info("[AVPlayer.js][play] play the video from " + (opts.sec ? opts.sec : "start") + " while the media is " + (this.__connected ? "" : "not ") + "connected.");
            var playResult, prepareResult;
            var played = false; // whether avplayer.play() has been called

            this.__withholdPlay = false;
            this.__seekTime = 0;

            this.__show();

            if (!this.__connected) {

                //XDK-2621 it will crash when play again the same widevine url, it needs to load again before play.
                if (!this.__loaded && this.__drm === "widevine") {
                    this.__doLoad(this.__url, this.__mediaOpts);
                }
                //from idle state to ready state
                this.__withholdPlay = true;

                console.info("[AVPlayer.js][play] begin to prepare the video" + avplayer.getState());

                //there are two ways (prepare and prepareAsync), prepare will run synchronous and may block the process.
                //Replaced with prepareAsync, it will undergo the flow asynchronously(buffering start, buffering complete and the prepareAsync success callback)
                //If play in buffering start/complete, it will throw error. So the seeking and withhold play handling will be placed inside the prepareAsync callback.
                prepareResult = avplayer.prepareAsync(util.bind(this.__prepareSuccess, this), util.bind(this.__prepareFail, this));

                console.info("[AVPlayer.js][play] prepare result for the media player playback " + prepareResult);

                sMedia._onConnecting();
                this.__connectionTimeOut = core.getGuid();
                util.delay(this.__connectionTimeLimit, this.__connectionTimeOut)
                    .then(util.bind(this.__onConnectionTimeout, this), function () {
                        // ignore clear delay
                        return;
                    }).done();
            }

            if (opts.sec) {
                //perform directly if paused, playing, speeding
                if (sMedia.isState(sMedia.PAUSED)) {
                    sMedia._onBufferingStart();
                    avplayer.play();
                    played = true;
                    this.seek(opts.sec);
                } else if (sMedia.isState(sMedia.PLAYING)) {
                    this.seek(opts.sec);
                } else {
                    this.__withholdPlay = true;
                    this.__seekTime = opts.sec;
                }
            }

            //reset the speed to 1 when play.
            if (this.__playbackSpeed > 1) {
                avplayer.setSpeed(1);
                this.__playbackSpeed = 1;
            }

            //run only if it is not withhold and not speeding. it will cause error when call play again during speeding
            if (!this.__withholdPlay && !sMedia.isState(sMedia.SPEEDING) && !played) {
                playResult = avplayer.play();
                console.info("[AVPlayer.js][play] play result " + playResult);
            }

            if (this.__connected) {
                sMedia._onPlaying();
            }
        },
        /**
         * stop the video playback
         * @method
         * @public
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        stop: function () {
            this.__stopPlayback();
            sMedia._onStopped();
        },
        /**
         * pause the video item
         * @method
         * @public
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        pause: function () {
            var pauseResult = avplayer.pause();
            console.info("[AVPlayer.js][pause] pause result " + pauseResult);
            sMedia._onPause();
            this.__withholdPlay = false;
            //fail to display the correct time and duration
            this.__updateCurrentTime();
        },
        /**
         * resume playing the video
         * @method
         * @public
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        resume: function () {
            var playResult = avplayer.play();
            console.info("[AVPlayer.js][resume] play result " + playResult);
            sMedia._onPlaying();
        },
        /**
         * Seek to specifiy position of the video
         * @method
         * @param {Number} sec the position to seek to in seconds
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        seek: function (sec) {
            var isSpeeding = this.__playbackSpeed > 1;

            //no response when seekTo 0, so convert to sec from 0s to 1s, which will be further converted into 1000ms.
            //XDK-2612 Fail to seek time to 0 in asf, so try to standardise all media to 0.5s. The device will start at 0 in fact.
            if (sec === 0) {
                sec = 0.5;
            }

            //since it is so strange when seek close to the duration and result in no response,
            //the maximium value for the seek will be a sec before the end
            //and then convert the time to ms format
            sec = Math.min(this.getDuration() - 1, sec) * 1000;

            //fail to seekTo when speeding
            if (isSpeeding) {
                avplayer.setSpeed(1);
            }

            var seekResult = avplayer.seekTo(sec);

            //rollback the speeding status
            if (isSpeeding) {
                avplayer.setSpeed(this.__playbackSpeed);
            }

            //update the current time
            this.__updateCurrentTime();
            console.info("[AVPlayer.js][seek] seek result " + seekResult);
        },
        /**
         * Update the current time automatically.
         * @method
         * @private
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        __updateCurrentTime: function () {
            this.__onCurrentPlayTime(webapis.avplay.getCurrentTime());
        },
        /**
         * Skip the playback forward/backward for certain seconds
         * @method
         * @param {Number} sec number of seconds to skip (10 by default)
         * @public
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        skip: function (sec) {
            var skipResult,
                isSpeeding = this.__playbackSpeed > 1;
            var limit = this.getDuration() - 2;
            var current = this.getCurTime();

            //fail to jumpForward/Backward when speeding
            if (isSpeeding) {
                avplayer.setSpeed(1);
            }

            //XDK-2620 Sometimes, it will stuck when skipping but in fact it skips properly
            //adding try catch make it not blocking to the player
            try {
                if (sec < 0) {
                    skipResult = avplayer.jumpBackward(-sec * 1000);
                } else {
                    sec = current + sec >= limit ? limit - current : sec;
                    skipResult = avplayer.jumpForward(sec * 1000);
                }
            } catch (ex) {
                //fail to run but suppose work properly.
                console.info("The skip operation may have been failed due to the following reason: " + ex);
            }

            //roll back the speeding
            if (isSpeeding) {
                avplayer.setSpeed(this.__playbackSpeed);
            }

            //since it won't update the current time when paused or skipping, so update the time directly
            if (sMedia.isState(sMedia.PAUSED) || sMedia.isState(sMedia.SKIPPING)) {
                this.__updateCurrentTime();
            }
            console.info("[AVPlayer.js][skip] skip " + sec + " result " + skipResult);
        },
        /**
         * Speed up/down the media playback, media with drm aes128 or type hls fail to speed
         * @method
         * @param {Number} speed the playback speed to set
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        speed: function (speed) {

            if (this.__drm === "aes128" || this.__type === "hls") {
                console.warn("[AVPlayer.js][speed] speed is not supported for this type.");
                return;
            }

            var result;
            //from -8x to 8x, it will give "UNKNOWN_ERROR_EVENT_FROM_PLAYER" when calling 16x.
            if (speed > 8) {
                speed = 8;
            }

            if (speed < -8) {
                speed = -8;
            }

            if (sMedia.isState(sMedia.PAUSED)) {
                //first make it play before speeding
                avplayer.play();
            }

            try {
                result = avplayer.setSpeed(speed);
            } catch (ex) {
                console.warn("[AVPlayer.js][speed] Error occur when speeding");
                return;
            }

            this.__playbackSpeed = speed;

            console.info("[AVPlayer.js][speed] set speed " + speed + " result:" + result);

            if (speed === 1) {
                sMedia._onPlaying();
                return;
            }

            sMedia._onSpeeding();
        },
        /**
         * Sets video window size
         * @method
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        getPlaybackSpeed: function () {
            return this.__playbackSpeed;
        },
        /**
         * Sets video window size
         * @method
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        setWindowSize: function (param) {
            this.__playerContainer.style.position = "absolute";
            this.__playerContainer.style.left = param.left + "px";
            this.__playerContainer.style.top = param.top + "px";
            this.__playerContainer.style.width = Math.min(param.width,window.innerWidth) + "px";
            this.__playerContainer.style.height = Math.min(param.height,window.innerHeight) + "px";

            avplayer.setDisplayRect(param.left, param.top, param.width, param.height);
        },
        /**
         * set the video to be full screen
         * @method
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        setFullscreen: function () {
            this.setWindowSize({
                top: 0,
                left: 0,
                height: getResolution().height,
                width: getResolution().width
            });
        },
        /**
         * Get the media bitrates
         * @method
         * @return {ax/device/interface/Player~MediaBitrates} current bitrate and available bitrates
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        getBitrates: function () {
            var info = avplayer.getTotalTrackInfo();
            //0: video, 1: audio, 2:subtitle
            info = util.parse(info[0].extra_info);
            return {
                currentBitrate: info.Bit_rate,
                availableBitrates: avplayer.getStreamingProperty("AVAILABLE_BITRATE")
            };
        },
        /**
         * Get the current playback time
         * @method
         * @return {Number} the current playback time
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        getCurTime: function () {
            return this.__currentTime;
        },
        /**
         * to get the total time of the video
         * @method
         * @return {number} the total time of the video
         * @memberof ax/ext/device/tizen/AVPlayer#
         * @public
         */
        getDuration: function () {
            return avplayer.getDuration() / 1000;
        },
        /**
         * set the id of the player
         * @method
         * @public
         * @param {String} id the path of the player
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        getId: function () {
            return this.__id;
        },
        /**
         * set the id of the player
         * @method
         * @public
         * @param {String} id the path of the player
         * @memberof ax/ext/device/tizen/AVPlayer#
         */
        setId: function (id) {
            this.__id = id;
        }
    });
});
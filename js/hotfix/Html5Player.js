/**
 * Html5 Player for the tizen
 * @class ax/ext/device/tizen/Html5Player
 * @augments ax/device/shared/Html5Player
 */
define("ax/ext/device/tizen/Html5Player", [
    "ax/class",
    "ax/device/shared/Html5Player",
    "ax/util",
    "ax/Env",
    "ax/console"
], function (
    klass,
    Html5Player,
    util,
    Env,
    console
) {
    "use strict";

    var sEnv;

    return klass.create(Html5Player, {}, {

        init: function () {
            this._super();
            sEnv = Env.singleton();
        },

        /**
         * Skip the playback forward/backward for certain seconds.
         *
         * @method
         * @param {Number} [sec] Number of seconds to skip (10 if not specified)
         * @memberof ax/ext/device/tizen/Html5Player#
         */
        skip: function (sec) {
            this._doSeek(this.getCurTime() + sec);
        },

        /**
         * Seek to specifiy position of the video.
         *
         * @method
         * @param {Number} sec The position to seek to in seconds
         * @memberof ax/ext/device/tizen/Html5Player#
         */
        seek: function (sec) {
            this._doSeek(sec);
        },

        /**
         * Perform the actual time setting operation (i.e. seek).
         * This method also limits the range of the time being set, so that the player will not fail.
         *
         * @method
         * @param sec The time to set
         * @memberof ax/ext/device/tizen/Html5Player#
         */
        _doSeek: function (sec) {
            //since it is so strange when seek the duration and no response, try to seek a sec before the end.
            sec = (sec >= this._duration - 1 ? this._duration - 1 : sec);

            this._playerObject.currentTime = sec;
        },

        /**
         * To get the capabilities of the player
         * @method getCapabilities
         * @memberof ax/ext/device/tizen/Html5Player#
         * @return {ax/device/interface/Player~PlayerCapabilites}
         * @function
         * @public
         */
        getCapabilities: function () {
            return {
                type: ["mp4", "asf", "mp3"],
                drms: []
            };
        },
        /**
         * Loads the specified media
         * @method
         * @param {String} url the URL address of the media
         * @param {Object} [opts] the options for loading this media
         * @param {module:ax/device/playerRegistry.DRM} [opts.drm] DRM to be used
         * @memberof ax/ext/device/tizen/Html5Player#
         */
        load: function (mediaUrl, opts) {
            this._super(mediaUrl, opts);

            //XDK-2732 Error when render the another media url, change a little bit on the size in order to force the video to render
            //otherwise, the video size and playback is incorrect
            //it will first set the size a little bit larger and then set back the correct size.
            this.__redrawPlayer(true);
        }

        /**
         * redraw the player, it fails to display the size correctly
         * @method __redrawPlayer
         * @param {Boolean} [defer=false] True if need defer when redraw the
         * @returns {Boolean} True if successfully set the size and redraw
         * @private
         * @memberof  ax/ext/device/tizen/Html5Player#
         */
        __redrawPlayer: function (defer) {
            if (!this._currentWindowSize) {
                return false;
            }

            var originalSize = this._currentWindowSize,
                changeSizeFn = util.bind(function () {

                    this.setWindowSize(util.extend(originalSize, {
                        width: originalSize.width + 1
                    }));

                    this.setWindowSize(originalSize);
                }, this);

            if (defer) {
                util.defer().then(changeSizeFn).done();
            } else {
                changeSizeFn();
            }

            return true;
        },
        /**
         * prepare the player
         * @method prepare
         * @protected
         * @memberof  ax/ext/device/tizen/Html5Player#
         */
        prepare: function (opts) {
            if (this._prepared) {
                return;
            }

            this._super(opts);

            //it fails to resume with right size when app resume. Tried to redraw again to make sure it is on the foreground with right size
            this.__handleResume = util.bind(this.__redrawPlayer, this);
            sEnv.addEventListener(sEnv.EVT_ONRESUME, this.__handleResume);
        },
        /**
         * resets the video player, to non-playing mode
         * @method reset
         * @protected
         * @memberof  ax/ext/device/tizen/Html5Player#
         */
        reset: function () {
            if (!this._prepared) {
                return;
            }

            this._super();

            sEnv.removeEventListener(sEnv.EVT_ONRESUME, this.__handleResume);
            this.__handleResumeRef = null;
        }
    });
});
/**
 * Cross-browser enviroment abstraction module.
 *
 * This class is designed as a singleton, developer should use {@link ax/Env.singleton|singleton} to obtain the instance.
 * Creating instance using the _new_ keyword is prohibited.
 *
 * @class ax/Env
 * @augments ax/EventDispatcher
 * @fires ax/Env#EVT_ON_PLATFORM_DETECTED
 * @fires ax/Env#EVT_ONDETECT
 * @fires ax/Env#EVT_ONUNLOAD
 * @fires ax/Env#EVT_ONLOAD
 * @fires ax/Env#EVT_ONKEY
 * @fires ax/Env#EVT_ONPAUSE
 * @fires ax/Env#EVT_ONRESUME
 * @fires ax/Env#EVT_ON_MOUSE_WHEEL
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/Env", [
    "ax/class", "ax/EventDispatcher", "ax/console", "ax/device/vKey", "ax/util",
    "ax/device", "ax/core", "ax/interface/DeviceHandler"
], function (klass, EventDispatcher, console, vKey, util,
    device, core, IDeviceHandler) {
    "use strict";
    var Env, instance, EVT_ONDETECT = "env:device:ondetect";

    Env = klass.create(EventDispatcher, [IDeviceHandler], {
        /**
         * Get the singleton instance of this class.
         * @method
         * @static
         * @returns {ax/Env} The singleton
         * @memberOf ax/Env
         */
        singleton: function () {
            if (!instance) {
                instance = new Env();
            }

            return instance;
        }
    }, {
        /**
         * Fired when the current platform is detected. i.e. device.platform
         * should be available. Delivering device.platform value as the data.
         * @event EVT_ONDETECT
         * @type {String}
         * @memberof ax/Env#
         */
        EVT_ONDETECT: EVT_ONDETECT,
        /**
         * Fired when the current platform is detected. i.e. device.platform
         * should be available. Delivering device.platform value as the data.
         *
         * @deprecated replaced with {@link ax/Env.EVT_ONDETECT}
         * @event EVT_ON_PLATFORM_DETECTED
         * @type {String}
         * @memberof ax/Env#
         */
        EVT_ON_PLATFORM_DETECTED: EVT_ONDETECT,
        /**
         * Event: onload
         *
         * @event EVT_ONLOAD
         * @memberof ax/Env#
         */
        EVT_ONLOAD: "env:onload",
        /**
         * Event: onunload
         *
         * @event EVT_ONUNLOAD
         * @memberof ax/Env#
         */
        EVT_ONUNLOAD: "env:onunload",
        /**
         * Event: onkey
         *
         * @event EVT_ONKEY
         * @type {object}
         * @property {string} id Virtual key id. e.g. "device:vkey:a"
         * @property {string} value Virtual Key value e.g. "a"
         * @property {string} source Source of the key event e.g. "soft-keyboard"
         * @memberof ax/Env#
         */
        EVT_ONKEY: "env:onkey",
        /**
         * Event: mousewheel
         *
         * @event EVT_ON_MOUSE_WHEEL
         * @type {Object}
         * @property {Number} deltaY The vertical amount of units the wheel has scrolled. Postive number indicates scrolling away from the user, negative means scrolling toward the user.
         * @property {ax/af/Component} target The component that is pointed by the mouse pointer when the wheel event took place
         * @memberof ax/Env#
         */
        EVT_ON_MOUSE_WHEEL: "env:onmousewheel",
        /**
         * Event: onpause
         *
         * @event EVT_ONPAUSE
         * @type {String}
         * @memberof ax/Env#
         */
        EVT_ONPAUSE: "env:onpause",
        /**
         * Event: onresume
         *
         * @event EVT_ONRESUME
         * @type {String}
         * @memberof ax/Env#
         */
        EVT_ONRESUME: "env:onresume",
        /**
         * Whether the env is ready.
         * @memberof ax/Env#
         * @private
         */
        __ready: false,
        /**
         * Whether the deivce is ready.
         * @memberof ax/Env#
         * @protected
         */
        __deviceReady: false,
        /**
         * The screen resolution (will be assigned upon device initialization).
         * @name resolution
         * @deprecated should use device.system.getDisplayResolution
         * @memberof ax/Env#
         * @public
         */
        resolution: null,
        /**
         * Object for storing notBlocked keys
         * @private
         * @memberof ax/Env#
         */
        __notBlockedKeysList: {},
        /**
         * Is currently blocking keys
         * @private
         * @memberof ax/Env#
         */
        __isBlockingKeys: false,
        /**
         * Is currently blocking keys
         * @private
         * @memberof ax/Env#
         */
        __hasMouse: false,
        /**
         * Is currently blocking Mouse
         * @private
         * @memberof ax/Env#
         */
        __isBlockingMouse: false,
        /**
         * To see if the environment is ready or not. Environment is ready when device platform is finished loading
         * @method isLoaded
         * @return {Boolean} True if the env is ready.
         * @public
         * @memberof ax/Env#
         */
        isLoaded: function () {
            return this.__deviceReady;
        },
        /**
         * Start blocking mouse events
         * @method blockMouse
         * @public
         * @memberof ax/Env#
         */
        blockMouse: function () {
            this.__isBlockingMouse = true;
        },
        /**
         * Stop blocking mouse events.
         * @method unblockMouse
         * @public
         * @memberof ax/Env#
         */
        unblockMouse: function () {
            this.__isBlockingMouse = false;
        },
        /**
         * Whether mouse events are blocked
         * @method isBlockingMouse
         * @public
         * @return {Boolean} Whether mouse events are blocked
         * @memberof ax/Env#
         */
        isBlockingMouse: function () {
            return this.__isBlockingMouse;
        },
        /**
         * Start blocking key events. By default, power, exit, home, stop keys are whitelisted.
         * @method blockKeys
         * @public
         * @memberof ax/Env#
         */
        blockKeys: function () {
            this.__isBlockingKeys = true;
        },
        /**
         * Stop blocking key events
         * @name unblockKeys
         * @public
         * @memberof ax/Env#
         * @function
         */
        unblockKeys: function () {
            this.__isBlockingKeys = false;
        },
        /**
         * Is blocking key events
         * @method isBlockingKeys
         * @public
         * @memberof ax/Env#
         * @return {Boolean} Is blocking key events
         */
        isBlockingKeys: function () {
            return this.__isBlockingKeys;
        },
        /**
         * Adds key(s) to be whitelisted when key event blocking is turned on.
         * By default, power, exit, home, stop keys are whitelisted.
         * @name addNotBlockedKey
         * @param {String|Array} key keys(s) to be whitelisted. If you want to block key from the vKey, parameter should be the id of that key
         * @function
         * @public
         * @example
         * sEnv.addNotBlockedKey([vKey.KEY_0.id]);
         * @memberof ax/Env#
         */
        addNotBlockedKey: function (key) {
            var i;
            if (util.isString(key)) {
                this.__notBlockedKeysList[key] = true;
            } else {
                for (i in key) {
                    this.__notBlockedKeysList[key[i]] = true;
                }
            }
        },
        /**
         * Set all the key that exists in an object's members to be key whitelisted (existing settings will get over-written).
         * @name setWhitelist
         * @param {Object} whitelist keys to be whitelisted, in a plain object map
         * @function
         * @public
         * @memberof ax/Env#
         */
        setWhitelist: function (whitelist) {
            this.__notBlockedKeysList = whitelist;
        },
        /**
         * Removes key to be whitelisted when key blocking is turned on.
         * @name removeWhitelistedKey
         * @param {String} key key to be removed from whitelist
         * @function
         * @public
         * @memberof ax/Env#
         */
        removeWhitelistedKey: function (key) {
            delete this.__notBlockedKeysList[key];
        },
        /**
         * Overrides parent method to block key events.
         * @method dispatchEvent
         * @public
         * @memberof ax/Env#
         */
        dispatchEvent: function (type, data) {
            // key event blocking
            if (this.__isBlockingKeys && type === this.EVT_ONKEY && !this.__notBlockedKeysList[data.id]) {
                console.debug("Key blocked by env.");
                return false;
            }
            return this._super(type, data);
        },
        /**
         * Overrides parent method
         * @method addEventListener
         * @param {String} type event type
         * @param {Function} handler event listener function
         * @param {Boolean} [once] set to true if listener will be called only once
         * @returns {ax/Env} current event dispatcher, for easier chain Env.EVT_ONKEYing
         * @public
         * @memberof ax/Env#
         */
        addEventListener: function (type, handler, once) {

            // if EVT_ONLOAD is already fired, just run the handler
            if (type === this.EVT_ONLOAD && this.__deviceReady) {
                handler();
                return this;
            }

            return this._super(type, handler, once);
        },
        /**
         * Returns whether the current platform supports mouse or not.
         * @method hasMouse
         * @memberof ax/Env#
         * @returns {Boolean} whether device has mouse
         * @public
         */
        hasMouse: function () {
            return device.system.hasMouse();
        },
        /**
         * Initializes enviroment
         * @method init
         * @memberof ax/Env#
         * @protected
         */
        init: function () {
            this.addNotBlockedKey([vKey.EXIT.id, vKey.STOP.id]);

            //need to set the handlers at the beginning.
            device.init(this);

            this._super();
        },
        /**
         * Default device initialize callback.
         * @method _onDeviceInited
         * @memberof ax/Env#
         * @private
         */
        _onDeviceInited: function () {
            this.__deviceReady = true;

            //@deprecated init resolution
            this.resolution = device.system.getDisplayResolution();

            console.info("[XDK] env ready");
            //call onload when everything is ready
            this.dispatchEvent(this.EVT_ONLOAD);
        },
        /**
         * Default device initialize callback.
         * @method onDeviceLoad
         * @param {Boolean} [isPaused=false] whether the device load again from pause state
         * @memberof ax/Env#
         * @public
         */
        onDeviceLoad: function (isPaused) {
            isPaused = isPaused || false;

            this.__deviceReady = true;

            //@deprecated init resolution
            this.resolution = device.system.getDisplayResolution();

            console.info("[XDK] env ready from " + isPaused);

            //call onload when everything is ready
            this.dispatchEvent(this.EVT_ONLOAD, isPaused);
        },
        /**
         * The callback when dispatching key event
         * @param {Object} tvKey the key object of {@link module:ax/device/shared/keyboardVKey} or {@link module:ax/device/VKey}
         * @param {String} source the source of the key event. Default will be "device"
         * @method onDeviceKey
         * @memberof ax/Env#
         * @public
         */
        onDeviceKey: function (tvKey, source) {
            var env = Env.singleton();
            source = source || "device";

            if (tvKey) {
                env.dispatchEvent(env.EVT_ONKEY, util.extend({
                    source: source
                }, tvKey));
            }
        },
        /**
         * Default window onload handler.
         * @method _onload
         * @memberof ax/Env#
         * @private
         */
        _onload: function () {
            if (this.__ready) {
                return;
            }

            console.info("[XDK] DOM loaded");

            this.__ready = true;

            if (device._bootstrap) {
                device._bootstrap({
                    inited: util.bind(this._onDeviceInited, this),
                    platformDetecetedCb: util.bind(this._onPlatformDetected, this)
                });
            }
        },
        /**
         * when the application unloads
         * @method onDeviceUnload
         * @memberof ax/Env#
         * @public
         */
        onDeviceUnload: function () {
            console.info("[XDK] Application unload");
            this.dispatchEvent(this.EVT_ONUNLOAD);
        },
        /**
         * Default window onunload handler.
         * @method _onunload
         * @memberof ax/Env#
         * @private
         */
        _onunload: function () {
            this.__ready = false;
            this.__deviceReady = false;
            this.onDeviceUnload();
        },
        /**
         * To dispatch event the platform is detected
         * @method onDeviceDetect
         * @param {String} platform the string of platform
         * @memberof ax/Env#
         * @public
         */
        onDeviceDetect: function (platform) {
            console.info("[XDK] device detected  " + platform);
            this.dispatchEvent(this.EVT_ONDETECT, platform);
        },
        /**
         * To dispatch event the platform is detected
         * @method _onPlatformDetected
         * @param {String} platform the string of platform
         * @memberof ax/Env#
         * @private
         */
        _onPlatformDetected: function (platform) {
            this.onDeviceDetect(platform);
        },
        /**
         * To dispatch event the application pauses
         * @method onPause
         * @memberof ax/Env#
         * @public
         */
        onDevicePause: function () {
            console.info("[XDK] Application pause");
            this.dispatchEvent(this.EVT_ONPAUSE);
        },
        /**
         * To dispatch event the application resume
         * @method onDeviceResume
         * @memberof ax/Env#
         * @public
         */
        onDeviceResume: function () {
            console.info("[XDK] Application resume");
            this.dispatchEvent(this.EVT_ONRESUME);
        }
    });

    // enforce the instance creation before returning, preventing 2 instances being created
    // (2 executions may go into the creation at the same time...)
    Env.singleton();

    return Env;

});
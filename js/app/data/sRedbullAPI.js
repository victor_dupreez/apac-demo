define("app/data/sRedbullAPI",
[
    "ax/class",
    "ax/promise",
    "ax/ajax",
    "ax/util",
    "app/widget/sLoader",
    "app/util/Utils"
],
    function(klass, promise, ajax, util, sLoader, appUtils) {


        var RedbullAPI = klass.create({}, {

            //Staging Redbull API
            // _baseUrl: "https://api-development.redbull.tv/",

            //Prod Redbull API
            _baseUrl: "https://api.redbull.tv/v1/",

            // Redbull API doesnt accept a limit > to 100
            _maxRequestsLimit: 100,

            EMPTY_ENTITY: {
                id: 1,
                message: "No data available."
            },

            SortOption: {
                SORT_BY: "sort_by",
                SORT_DIR: "sort_dir",
                SEARCH: "search",
                STATUS: "status",
                STARTS_AFTER: "starts_after",
                ENDS_BEFORE: "ends_before"
            },

            SortBy: {
                ACTIVATED_ON: "activated_on",
                CREATED_ON: "created_on",
                PUBLISHED_ON: "published_on",
                SCORE: "score",
                EPISODE_NUMBER: "episode_number",
                TITLE: "title"
            },

            SortDir: {
                ASC: "asc",
                DESC: "desc",
                NONE: null
            },

            VideoRendition: {
                64: 64,
                296: 296,
                496: 496,
                856: 856,
                1200: 1200,
                1800: 1800,
                2500: 2500,
                3528: 3528,
                4500: 4500,
                6500: 6500,
                8500: 8500
            },

            StreamStatus: {
                LIVE: "live",
                PAUSE: "pause",
                COMPLETE: "complete",
                REPLAY: "replay",
                PRE_EVENT: "pre-event",
                SOON: "soon",
                LIVE_WAITING: "live-waiting"
            },

            _limitResults: 0,

            init: function() {
            },

            setDataLoaderLimit: function(limitResults) {
                this._limitResults = limitResults > -1 ? limitResults : 0;
            },

            /*
             * for sortBy use the SortBy object
             * for sortyDir use the SortDir object
             * search is a string
             * for status use the StreamStatus object
             * starts_after and ends_before are string dates
             */
            createExtendedOpts: function(sortBy, sortDir, search, status, starts_after, ends_before) {
                return {
                    sort_by: sortBy,
                    sort_dir: sortDir,
                    search: search,
                    status: status,
                    starts_after: starts_after,
                    ends_before: ends_before
                };
            },

            /*
             * Get an image, using the specified options
             */

            setImagesOptimization: function(status) {
                this.__imagesOptimization = !!status;
            },

            getImageUrl: function(src, opts) {
                opts = opts || {};
                var commands = {
                        crop: ["crop=fill", "c_fill"],
                        quality: ["", "q_"],
                        saturation: ["/effect=saturation:", "e_saturation:"],
                        width: ["width=", "w_"],
                        height: ["height=", "h_"],
                        blur: ["/effect=blur", "/e_blur/"],
                        progressive: ["", "fl_progressive"]
                    };

                if (this.__imagesOptimization) {
                    opts.quality = 95;
                    if (opts.width && opts.width < 400) {
                        opts.quality = 85;
                    }
                }

                // if quality is defined, use method 2
                var method = (opts.quality && opts.quality <= 100 && opts.quality >= 0) ? 1 : 0;
                var cmds = "/";

                for (var prop in opts) {
                    if (commands[prop]) {
                        cmds += commands[prop][method] + (opts[prop] === true ? "" : opts[prop]);
                        if(prop !== "quality") {
                            cmds += ",";
                        }
                    }
                }

                if (opts.quality) {
                    var baseUrl = "http://images.redbull.tv/image/fetch";
                    src = src.substring(src.indexOf("/images/") + "/images/".length);
                    baseUrl += cmds;
                    baseUrl += "/" + src;
                    baseUrl = this.removeAnomoliesFromString(baseUrl);
                    return baseUrl;
                }
                else {
                    return src + cmds;
                }
            },

            removeAnomoliesFromString : function(url){
                return url.split(",/").join("/").split("/,").join("/");
            },
            /*
             * Get resources (anything, this method is just an ajax query)
             */

            getResource: function(param, includeBaseUrl, background) {
                var urlPage = includeBaseUrl ? this._baseUrl + param : param,
                    deferred = promise.defer();

                if (!background && appUtils.firstShow) {
                    sLoader.show();
                }

                ajax.request(urlPage, {
                    method: "get"
                }).then(function(transport) {
                    var data = transport.responseJSON;
                    deferred.resolve(data);

                    if (!background && appUtils.firstShow) {
                        sLoader.hide();
                    }

                }, function(rejection) {
                    if (!background && appUtils.firstShow) {
                        sLoader.hide();
                    }

                    deferred.reject("Error while fetching resource: " + rejection);
                });
                return deferred.promise;
            },

            /*
             * renditions is an array of VideoRendition (see up)
             * for instance: renditions = [VideoRendition.64, VideoRendition.296, VideoRendition.1800];
             */
            getVideo: function(videoId, renditions) {
                renditions = renditions || [];
                var i = 0,
                    len = renditions.length,
                    params = "";
                if (len > 0) {
                    params = "?renditions=";
                    for (; i < len; i++) {
                        params += renditions[i] + ",";
                    }
                }
                return this.getResource("videos/" + videoId + params, true);
            },

            getVideoStreamStatus: function(videoId) {
                var deferred = promise.defer();

                this.getResource("videos/" + videoId + "/status", true, true).then(function(data) {
                    deferred.resolve(data.stream);
                });
                return deferred.promise;
            },

            getChannels: function() {
                return this.getResource("channels/", true);
            },

            getChannel: function(channelId) {
                return this.getResource("channels/" + channelId, true);
            },

            getEvents: function(limit) {
                if (!limit || limit < 0) {
                    limit = this._maxRequestsLimit;
                }
                var url = "shows/events?offset=0&limit=" + limit + "&starts_after=" + new Date();
                return this.getResource(url, true);
            },

            getEvent: function(eventId, limit) {
                return this.getResource("shows/" + eventId + "/episodes?offset=0&limit=" + limit, true, true);
            },

            getShow: function(showId) {
                return this.getResource("shows/" + showId, true);
            },

            getShowEpisodes: function(showId, offset, limit, extendedOpts) {
                extendedOpts = extendedOpts || {};
                var options = "?offset=" + offset + "&limit=" + limit;
                if (extendedOpts.sort_by) {
                    options += "&sort_by=" + extendedOpts.sort_by;
                }
                if (extendedOpts.sort_dir) {
                    options += "&sort_dir=" + extendedOpts.sort_dir;
                }
                return this.getResource("shows/" + showId + "/episodes" + options, true);
            },

            getAllShowEpisodes: function(showId, extendedOpts, _i, _objRes) {
                var deferred = promise.defer(),
                    self = this;
                _i = _i || 0;
                this.getShowEpisodes(showId, _i, self._maxRequestsLimit, extendedOpts).then(function(data) {
                    if (!_objRes) {
                        _objRes = data;
                    } else {
                        _objRes.videos.push.apply(_objRes.videos, data.videos);
                    }
                    if (data.meta.total_results < _i + self._maxRequestsLimit) {
                        deferred.resolve(_objRes);
                    } else {
                        deferred.resolve(self.getAllShowEpisodes(showId, extendedOpts, _i + self._maxRequestsLimit, _objRes));
                    }
                });
                return deferred.promise;
            },

            getSearchSuggestions: function(searchString) {
                return this.getResource("search/suggestions?search=" + encodeURIComponent(searchString), true);
            },

            /*
             * Dataloaders
             */

            // get a data loader from an array
            getDataLoaderFromArray: function(objectsArray) {
                return function(from, size) {
                    var dataArr = [],
                        i = from,
                        len = from + size,
                        deferred = promise.defer();


                    for (; i < len && i < objectsArray.length; i++) {
                        dataArr.push({
                            id: i,
                            object: objectsArray[i]
                        });
                    }

                    deferred.resolve({
                        data: dataArr,
                        total: objectsArray.length
                    });

                    return deferred.promise;
                };
            },

            _getDataLoader: function(param, extendedOpts, userFullUrl, filterFct) {
                var self = this,
                    limitResults = this._limitResults,
                    EMPTY_ENTITY = this.EMPTY_ENTITY;
                return function(from, size) {
                    var dataArr = [],
                        deferred = promise.defer(),
                        resource = param.split("/")[0];

                    extendedOpts = extendedOpts || {};
                    limitResults = extendedOpts.maxItems || limitResults;

                    var options = "?offset=" + from + "&limit=" + (extendedOpts.limit ? extendedOpts.limit : (size < 20 ? 20 : size));
                    for (var prop in extendedOpts) {
                        if (extendedOpts[prop]) {
                            options += "&" + prop + "=" + (prop === "search" ? encodeURIComponent(extendedOpts[prop]) : extendedOpts[prop]);
                        }
                    }

                    var urlPage = !userFullUrl ? self._baseUrl + param + options : param + options;

                    ajax.request(urlPage, {
                        method: "get"
                    }).then(function(transport) {
                        var data = transport.responseJSON,
                            i = 0,
                            len = 0;
                        // in case of a search, the resources are under the "search_results" key
                        if (resource === "search") {
                            resource = "search_results";
                        }
                        // if no result with the default resource, try the "videos" one
                        if (!data[resource]) {
                            resource = "videos";
                        }
                        // if still no result, try out with "shows"
                        if (!data[resource]) {
                            resource = "shows";
                        }
                        // try out with "featured_items"
                        if (!data[resource]) {
                            resource = "featured_items";
                        }

                        len = data[resource].length;
                        for (i = 0; i < len; i++) {
                            if (data[resource][i].geo_restricted !== true) {
                                dataArr.push({
                                    id: from + dataArr.length,
                                    object: data[resource][i]
                                });
                            }
                        }

                        var total = data.meta.total_results || len;
                        if (limitResults > 0 && limitResults < total) {
                            total = limitResults;
                        }

                        // fix redbull API bug on the wrong total number of items they may return
                        if (from + len < total && (!data.meta.limit || data.meta.limit > len)) {
                            total = from + len;
                        }

                        //check for empty response. If reponse is empty add  item to array with custom error message.
                        if (total === 0) {
                            dataArr.push(EMPTY_ENTITY);
                            total = 1;
                        }

                        if (dataArr.length > total) {
                            dataArr = dataArr.slice(0, total);
                        }

                        var result = {
                                data: dataArr,
                                total: total
                            };

                        // use the filter fction if provided
                        if(filterFct) {
                            result = filterFct(result);
                        }

                        deferred.resolve(result);

                    }, function(rejection) {
                        deferred.reject("Error while fetching data for DataLoader: ", rejection);
                    });

                    return deferred.promise;
                };

            },

            // general data loader (to use when you know that the result will be a collection)
            getDataLoader: function(url, extendedOpts) {
                extendedOpts = extendedOpts || {};
                return this._getDataLoader(url, extendedOpts, true);
            },

            // Featured related

            getFeaturedDataLoader: function() {
                return this._getDataLoader("featured");
            },

            getChannelFeaturedDataLoader: function(channelId) {
                return this._getDataLoader("channels/" + channelId + "/featured");
            },


            // Search related

            getSearchDataLoader: function(searchString, extendedOpts) {
                extendedOpts = extendedOpts || this.createExtendedOpts(null, null, searchString);
                if (!extendedOpts.search) {
                    extendedOpts.search = searchString;
                }

                var searchChannel = (extendedOpts.channel ? extendedOpts.channel : "search");
                return this._getDataLoader(searchChannel, extendedOpts, undefined, util.bind(function filter(result) {
                    // Remove fallback element
                    if (result.data.length === 1 && result.data[0] === this.EMPTY_ENTITY) {
                        return [];
                    }
                    return result;
                }, this));
            },

            // Live related

            getLiveEventsDataLoader: function(extendedOpts) {
                return this._getDataLoader("channels/live/event_streams", extendedOpts);
            },

            getPastEventsDataLoader: function() {
                var filter = util.bind(function(data) {
                    var i = 0, imax = data.data.length, newData={data:[]};
                    // if no empty data
                    if (imax > 0 && !data.data[0].message) {
                        // we will remove all streams that have a date in the future
                        for (; i<imax; i++) {
                            var statuses = ["live", "pause", "pre-event", "soon", "live-waiting"];

                            if (statuses.indexOf(data.data[i].object.stream.status) >= 0) {
//                                if this is true then the event should be ignored
//                                console.log('IGNORE');
                            }else{
//                                console.log('include');
                                newData.data.push(data.data[i]);
                            }
                        }
                        var total = data.total;
                        data = newData;
                        data.total = total;
                        return data;
                    }
                }, this);
                return this._getDataLoader("channels/live/event_streams", this.createExtendedOpts(null, null, null, null, null, new Date().toISOString()), false, filter);
            },

            // Film related
            getChannelFilmDataLoader: function(){
                return this._getDataLoader("videos/films");

            },

            // Videos related

            getVideosDataLoader: function(extendedOpts) {
                return this._getDataLoader("videos", extendedOpts);
            },

            getRelatedVideosDataLoader: function(videoId) {
                return this._getDataLoader("videos/" + videoId + "/related");
            },

            // @deprecated
            getFeaturedVideosDataLoader: function() {
                return this._getDataLoader("videos/featured");
            },

            getClipsDataLoader: function(extendedOpts) {
                return this._getDataLoader("videos/clips", extendedOpts);
            },

            getEpisodesDataLoader: function(extendedOpts) {
                return this._getDataLoader("videos/episodes", extendedOpts);
            },

            // Home related

            // @deprecated
            getHomeFeaturedVideosDataLoader: function() {
                return this._getDataLoader("videos/featured");
            },

            getHomeFeaturedShowsDataLoader: function() {
                return this._getDataLoader("shows/featured");
            },

            getHomeEpisodesDataLoader: function(extendedOpts) {
                return this._getDataLoader("videos/episodes", extendedOpts);
            },

            getHomeClipsDataLoader: function(extendedOpts) {
                return this._getDataLoader("videos/clips", extendedOpts);
            },

            // Channels related

            getChannelsDataLoader: function() {
                return this._getDataLoader("channels");
            },

            getChannelVideosDataLoader: function(channelId, extendedOpts) {
                return this._getDataLoader("channels/" + channelId + "/videos", extendedOpts);
            },

            // @deprecated
            getChannelFeaturedVideosDataLoader: function(channelId) {
                return this._getDataLoader("channels/" + channelId + "/videos/featured");
            },

            getChannelEpisodesDataLoader: function(channelId, extendedOpts) {
                return this._getDataLoader("channels/" + channelId + "/episodes", extendedOpts);
            },

            getChannelClipsDataLoader: function(channelId, extendedOpts) {
                return this._getDataLoader("channels/" + channelId + "/clips", extendedOpts);
            },

            getChannelShowsDataLoader: function(channelId, extendedOpts) {
                return this._getDataLoader("channels/" + channelId + "/shows", extendedOpts);
            },

            getChannelFeaturedShowsDataLoader: function(channelId) {
                return this._getDataLoader("channels/" + channelId + "/shows/featured");
            },

            // Shows related

            getShowsDataLoader: function(extendedOpts) {
                return this._getDataLoader("shows", extendedOpts);
            },

            getFeaturedShowsDataLoader: function() {
                return this._getDataLoader("shows/featured");
            },

            getShowEpisodesDataLoader: function(showId, extendedOpts) {
                return this._getDataLoader("shows/" + showId + "/episodes", extendedOpts);
            }

        });

        return new RedbullAPI();

    });

define("app/data/RedbullDatasource", [
        "ax/class",
        "ax/af/data/Datasource",
        "ax/core",
        "ax/util",
        "ax/promise"
    ],
    function (klass, Datasource, core, util, promise)
    {
        return klass.create(Datasource, {}, {

            init: function(opts) {
                opts = opts || {};
                this._super(opts);
                this._preloaded = {};
                this._preloadSize = opts.preloadSize > 0 ? opts.preloadSize : 20;
                this._preloadTrigger = opts.preloadTrigger > 0 ? opts.preloadTrigger : 15;
                this._noPreload = !!opts.noPreload;
            },

            getRange: function (from, to) {
                var totalCount = this.getTotalCount(),
                    self = this,
                    newRange = [],
                    i, getRangePromise,
                    //use custom count method to count the length of current data
                    len = this._data.length;

                if (util.isUndefined(to)) {
                    to = this.getTotalCount();
                }

                //if the from is larger than 0, the total count should set at the beginning.
                if (from < 0 || from >= to ) {
                    return promise.reject(core.createException("IncorrectParam", "Incorrect \"from\" or \"to\" parameter!"));
                }

                //if the from is larger than the total count where no more data is available
                if (totalCount !== -1 && from >= totalCount) {
                    return promise.reject(core.createException("IncorrectState", "No more data available!"));
                }

                //the first checking

                newRange = this.__checkFetchedData(from, to);

                //if all the data are existed then return directly
                if (newRange.length === 0) {

                    if (!this._noPreload && len < this._totalCount && len < to + this._preloadTrigger) {
                        // TODO: check data size if problem
                        if (this._preloaded[len] === undefined) {
                            this._preloaded[len] = false;
                            this._load(len, this._preloadSize).then(function() {
                                self._preloaded[len] = true;
                            });
                        }
                    }

                    return promise.resolve(this.__getFetchedData(from, to));
                }

                //create a promise which store for each range, it will fetch range by range
                getRangePromise = promise.resolve();

                for (i = 0; i < newRange.length; i++) {

                    getRangePromise = getRangePromise.then(util.bind(function (fromIndex, toIndex) {
                        var actualFrom, fetchSize;

                        //to prevent the data is already included into previous load
                        if (self.hasData(fromIndex, toIndex)) {
                            return promise.resolve();
                        }

                        actualFrom = fromIndex;
                        fetchSize = toIndex - actualFrom;

                        //to load the data
                        return self._load(actualFrom, fetchSize);

                    }, null, newRange[i].from, newRange[i].to));

                }

                return getRangePromise.then(util.bind(function () {
                    //after loaded all data, get back all the data
                    return self.__getFetchedData(from, to);
                }));

            },

            reset: function () {
                this._super();
                this._preloaded = {};
            }
        });
    });
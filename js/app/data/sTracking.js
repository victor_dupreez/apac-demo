define("app/data/sTracking", [
    "ax/class",
    "ax/EventDispatcher",
    "ax/device",
    "ax/util",
    "ax/Element",
    "ax/console",

    "app/util/Utils"
], function(klass, EventDispatcher, device, util, Element, console, Utils) {
        var Tracking = klass.create(EventDispatcher, {}, {

            //Event names
            VIDEO_PLAY_RESUME: "stracking-video-playresume",
            VIDEO_PAUSE: "stracking-video-pause",
            VIDEO_STOP: "stracking-video-stop",
            VIDEO_EOF: "stracking-video-eof",
            VIDEO_POS: "stracking-video-pos",


            _trackDomain: "https://redbull01.webtrekk.net",
            // Staging
            //_trackId: "184654448869435",
            //Production
            _trackId: "207511680390829",
            _pixelVersion: "323",
            _pixelUrl: "",
            _pageName: "", // This should be current page
            _platform: "unknown",
            _cg1: "Red Bull TV",
            _cg2: "{#_cg1} - int",
            _cg3: "{#_cg2} - RBTVapp {#_platform}",

            _cg4: "", //channel
            _cg5: "", //Subchannel
            _cp1: "en", //lang

            _cp8: "Red Bull TV", // Project category

            _trackingElement: null,

            init: function () {
                this._initialized = false;
            },

            /**
             * Function to do the actual initialization of value.
             * This is needed to do later in the app process since the device package does not have all the correct values.
             */
            _doInit: function() {

                if (this._initialized || !device.platform) {
                    return;
                }

                this._initialized = true;

                // Used for videotracking
                this.video = {};

                this._platform = Utils.capitalizeWord(device.platform) + " TV";
                if(device.platform === "lg"){
                    this._platform = "LG TV";
                }

                this._pixelUrl = this._trackDomain + "/" + this._trackId + "/wt.pl?p=" + this._pixelVersion;

                this._cg2 = this._cg2.replace(/{#_cg1}/, util.bind(this._replacer, this));
                this._cg3 = this._cg3.replace(/{#_cg2}/, util.bind(this._replacer, this));
                this._cg3 = this._cg3.replace(/{#_platform}/, util.bind(this._replacer, this));
                this._cp2 = "Red Bull TV " + this._platform;

                this._trackingElement = new Element("img");
                this._trackingElement.attr("height", "1px");
                this._trackingElement.attr("width", "1px");

                this.addEventListener(this.VIDEO_PLAY_RESUME, util.bind(this.trackPlay, this));
                this.addEventListener(this.VIDEO_PAUSE, util.bind(this.trackPause, this));
                this.addEventListener(this.VIDEO_STOP, util.bind(this.trackStop, this));
                this.addEventListener(this.VIDEO_EOF, util.bind(this.trackEOF, this));
                this.addEventListener(this.VIDEO_POS, util.bind(this.trackPosition, this));
            },

            /**
             * Used as a string replace callback. This function should be used combined with string replace like so: "string {#value}".replace(/{#value}/, util.bind(this._replacer, this)).
             * This function will then replace the matched value and take the property "value" from "this" e.g. this["value"];
             *
             * @return {string} Text to replace the matched text.
             */
            _replacer: function() {

                var args = Array.prototype.slice.call(arguments),
                    string = args.pop(),
                    offset = args.pop(),
                    i = 0,
                    match;

                console.log(string);
                console.log(offset);

                for (; i < args.length; i++) {
                    match = args[i].match(/(\w+)/g);
                    if (match) {
                        match = match[0];
                        return this[match];
                    }
                }
            },

            /**
             * Creates the url to be used for sending tracking information.
             *
             * @return {string}         Url for tracking purposes.
             */
            _buildPagePixelUrl: function() {
                var url = this._pixelUrl + "," + this._pageName,
                    dimensions = device.system.getDisplayResolution();
                url += ",0," + dimensions.width + "x" + dimensions.height + ",0,0,0,0,0,0"; // hardcoded value that we don't change, for now at least.

                url += "&x=" + (new Date()).getTime(); // Timestamp to avoid cache
                url += "&cp1=" + this._cp1;
                url += "&cp2=" + this._cp2;
                url += "&cg1=" + this._cg1;
                url += "&cg2=" + this._cg2;
                url += "&cg3=" + this._cg3;

                if (this._cg4) {
                    url += "&cg4=" + this._cg4;
                }

                if (this._cg5) {
                    url += "&cg5=" + this._cg5;
                }

                return url.replace(/\s+/g, "+");
            },

            /**
             * Returns a formatted pagename containing pagename and additional channel if existing.
             * @return {String} Page name
             */
            getFormattedPageName: function() {

            },

            formatPageName: function(pageName) {
                if (!pageName) {
                    return "";
                }

                pageName = pageName.toLowerCase().replace(/ /g, "-");

                return pageName;
            },

            setPageNameDependingOnVideoType: function(video) {
                switch (video.type) {
                    case "episode":
                        this.setPage(video.subtitle + "." + video.title);
                        break;

                    case "event_stream":
                        this.setPage(video.subtitle + "." + video.title + "." + video.stream.status);
                        break;

                    case "clip":
                        this.setPage(video.title);
                        break;
                }
            },

            /**
             * Stores the current page name. This will be appended to the tracking url.
             * @param {string} pageName Page title/name e.g homescreen.
             */
            setPage: function(pageName) {

                this._pageName = "app.rbtv." + this._platform.toLowerCase().replace(/ /g, "") + "." +  pageName;
                this._pageName = this.formatPageName(this._pageName);
                this._pageName = encodeURIComponent(this._pageName);

                // Reset channel and subchannel
                this._cg4 = "";
                this._cg5 = "";

                // Reset different values that are used to indicate the full page path
                this._showTitle = "";
            },

            /**
             * Stores the channel title
             * @param {string} channel Channel title.
             */
            setChannel: function(channel) {
                this._cg4 = Utils.capitalizeWord(channel);
            },

            /**
             * Stores the subchannel title
             * @param {string} subChannel Sub channel title.
             */
            setSubChannel: function(subChannel) {
                this._cg5 = Utils.capitalizeWord(subChannel);
            },

            /**
             * Sets both channel and subchannel depending on channel id key in params.
             *
             * @param {object} params Containing information about the page.
             * @return {boolean} Returns true or false depending if it did set the channel/subchannel or not.
             */
            setChannelAndSubChannel: function(params) {
                if (params.channelId && params.channelId.indexOf("/") >= 0) { // Assume it's a subchannel
                    var channelAndSubchannel = params.channelId.split("/");
                    this.setChannel(channelAndSubchannel[0]);
                    this.setSubChannel(channelAndSubchannel[1]);
                    return true;
                }
                return false;
            },

            _parseMi: function(mi) {
                mi = mi || "";

                var parsedMi = "";

                parsedMi = "app.rbtv." + this._platform.toLowerCase().replace(/ /g, "") + "." + mi;

                return parsedMi;
            },

            _doTrack: function(url) {

                if (!this._initialized || !device.platform) {
                    console.log("Aborted tracking cause sTracking is not initalized or platform is not defined, (_doTrack function)");
                    return;
                }

                this._trackingElement.getHTMLElement().src = url;

                console.log("tracking url: " + url);

                return url;
            },

            /**
             * Used to send tracking information about a page.
             * This function will also check if the class has been initialized correctly.
             *
             * @param  {object} params containing information about the current page (channelName, channelId, breadCrumbs etc).
             * @return {string}        Tracking url which was used for sending tracking information.
             */
            trackPage: function(params) {

                if (!this._initialized) {
                    this._doInit();
                }

                if (params.breadCrumbs && !params.channelId) {
                    params.channelId = params.breadCrumbs.join("/");
                    params.channelName = params.breadCrumbs[0];
                }

                if (params.screenName === "showscreen" && params.showTitle) {
                    this.setPage(params.showTitle);
                    this.setChannelAndSubChannel(params);
                } else if (params.videoObject) {
                    this.setPageNameDependingOnVideoType(params.videoObject);
                    this.setChannelAndSubChannel(params);
                } else if (params.isMenuItem === true) {
                    this.setPage(params.channelName);
                    this.setChannel(params.channelName);
                } else {
                    if (!this.setChannelAndSubChannel(params)) {
                        this.setChannel(params.channelName);
                    }
                }

                var url = this._buildPagePixelUrl();

                this._doTrack(url);

                return url;

            },

            /**
             * Sets the default values from the video object to be tracked.
             *
             * @param {object} options Video object containing information about the video.
             */
            setVideo: function(options) {

                options = options || {};

                var mi = this._parseMi(options.vin || options.id);

                this.video = {};

                this.video.mg1 = options.title + " - " + options.subtitle;
                this.video.mg2 = this._cg1 + " " + this._cg3.replace(this._cg2, "").replace(/^\s+-\s+/,"");
                this.video.mg3 = this._cg4;
                this.video.mg4 = this._cg5;
                this.video.mg5 = options.subtitle;
                this.video.mg6 = options.season_number;
                this.video.mg7 = options.episode_number;
                this.video.mg8 = mi + (options.stream && options.stream.status ? "." + options.stream.status : "");
                this.video.mg9 = options.type;

                if (options.stream && options.stream.status) {
                    this.video.mg10 = options.stream.status;
                }

                this.video.cg4 = this._cg4;
                this.video.cg5 = this._cg5;

                this.video.cp2 = this._cp2;

                //Action parameters
                this.video.mi = mi;
                this.video.ck1 = this.video.mg2;
                this.video.ck2 = this._pageName;
                //this.video.ck3 = encodeURIComponent("Channel:") + " redbulltv " + this._platform;
                this.video.ck4 = options.vin || options.id;
                //this.video.ck5 = device.platform; // player id, lets just use platform id

                /*if (options.type === "event_stream" && options.stream && options.stream.status) {
                 this.video.ck6 = options.stream.status.replace("-event", "");
                 } else {
                 this.video.ck6 = options.type;
                 }*/

                this._trackedPlayAction = false;
                this._trackedEOF = false;

            },

            /**
             * Creates the pixel url for the video tracking.
             *
             * @param  {object} options Object containing additional parameters to be appended to the url. The key/value will be appended to the url like this: "&key=value".
             * @return {string}         pixel url to be used on the tracking element.
             */
            _buildVideoPixelUrl: function(options) {
                var url = this._pixelUrl + ",st";

                url += "&x=" + (new Date()).getTime(); // Timestamp to avoid cache
                url += "&cg1=" + this._cg1;
                url += "&cg2=" + this._cg2;
                url += "&cg3=" + this._cg3;
                url += "&cp8=" + this._cp8;

                util.each(options, util.bind(function(pair) {
                    if (pair.value) {
                        url += "&" + pair.key + "=" + pair.value;
                    }

                    // Used to determine if resume should be called or not.
                    /*if (pair.key === "mk") {
                     this._lastVideoAction = pair.value;
                     }*/
                }, this));

                return url.replace(/\s+/g,"+");
            },

            /**
             * Track play status from the player.
             *
             * @param  {object} options Additional parameters to be appended to the tracking url.
             * @return {string}         Tracking url used for the tracking element.
             */
            trackPlay: function(options) {
                options = options || {};

                // If we haven't tracked play action yet we do that.
                if (!this._trackedPlayAction) {
                    util.each(this.video, function(pair) {
                        options[pair.key] = pair.value;
                    });
                    options.mk = "play";

                    this._trackedPlayAction = true;

                    this._lastVideoAction = "play";

                    return this._doTrack(this._buildVideoPixelUrl(options));
                    // If the last action was pause we track resume
                } else if (this._lastVideoAction === "pause") {
                    return this.trackResume(options);
                }
            },

            /**
             * Track resume status from the player.
             *
             * @param  {object} options Additional parameters to be appended to the tracking url.
             * @return {string}         Tracking url used for the tracking element.
             */
            trackResume: function(options) {
                options.mi = this.video.mi;
                options.mk = "resume";

                this._lastVideoAction = "resume";

                return this._doTrack(this._buildVideoPixelUrl(options));
            },

            /**
             * Track pause status from the player.
             *
             * @param  {object} options Additional parameters to be appended to the tracking url.
             * @return {string}         Tracking url used for the tracking element.
             */
            trackPause: function(options) {

                // Only continue if we are in "playing" mode to avoid multiple pause tracking events in a row.
                if (this._lastVideoAction !== "resume" && this._lastVideoAction !== "play") {
                    return;
                }

                options.mi = this.video.mi;
                options.mk = "pause";

                this._lastVideoAction = "pause";

                return this._doTrack(this._buildVideoPixelUrl(options));
            },

            /**
             * Track stop status from the player.
             *
             * @param  {object} options Additional parameters to be appended to the tracking url.
             * @return {string}         Tracking url used for the tracking element.
             */
            trackStop: function(options) {

                //Let's ignore stop event if we have already tracked end of file.
                if (this._trackedEOF === true) {
                    return;
                }

                options.mi = this.video.mi;
                options.mk = "stop";

                return this._doTrack(this._buildVideoPixelUrl(options));
            },

            /**
             * Track end of file, this should be called 20 seconds before the video ends according to documentation.
             * This will only be tracked once for the video.
             *
             * @param  {object} options Additional parameters to be appended to the tracking url.
             * @return {string}         Tracking url used for the tracking element.
             */
            trackEOF: function(options) {
                // Already tracked for this video.
                if (this._trackedEOF === true) {
                    return;
                }

                options.mi = this.video.mi;
                options.mk = "eof";

                this._trackedEOF = true;

                return this._doTrack(this._buildVideoPixelUrl(options));
            },

            /**
             * Track the current position for the video.
             * @param  {object} options Additional parameters to be appended to the tracking url.
             * @return {string}         Tracking url used for the tracking element.
             */
            trackPosition: function() {
                //NOOP for now

                /*options.mi = this.video.mi;
                 options.mk = "pos";

                 return this._doTrack(this._buildVideoPixelUrl(options));*/
            }
        });

        return new Tracking({});
    }
);
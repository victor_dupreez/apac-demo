define("app/data/sRedbullEventsTracker", [
        "ax/class",
        "ax/util",
        "ax/console",
        "ax/af/evt/DualEventDispatcher",

        "app/data/sRedbullAPI",
        "app/util/Utils",
        "ax/promise"
    ],
    function(klass, util, console, EventDispatcher, sRedbullAPI, Utils, promise) {

        var RedbullEventsTracker = klass.create(EventDispatcher, {}, {

            // events
            EVT_UPDATED: "app:data:redbullevent:updated",
            EVT_UPDATED_FOR_STREAM: function(evtId) {
                return this.EVT_UPDATED + ":" + evtId;
            },
            EVT_STATUS_UPDATED: "app:data:redbullevent:statusupdated",
            EVT_STATUS_UPDATED_FOR_STREAM: function(evtId) {
                return this.EVT_STATUS_UPDATED + ":" + evtId;
            },
            EVT_START_TIME_UPDATED: "app:data:redbullevent:starttimeupdated",
            EVT_START_TIME_UPDATED_FOR_STREAM: function(evtId) {
                return this.EVT_START_TIME_UPDATED + ":" + evtId;
            },
            EVT_END_TIME_UPDATED: "app:data:redbullevent:endtimeupdated",
            EVT_END_TIME_UPDATED_FOR_STREAM: function(evtId) {
                return this.EVT_END_TIME_UPDATED + ":" + evtId;
            },
            EVT_READY: "app:data:redbullevent:ready",
            EVT_READY_FOR_STREAM: function(evtId) {
                return this.EVT_READY + ":" + evtId;
            },
            EVT_PRIORITY_UPDATED: "app:data:redbullevent:priorityupdated",
            EVT_PRIORITY_UPDATED_FOR_STREAM: function(evtId) {
                return this.EVT_PRIORITY_UPDATED + ":" + evtId;
            },
            EVT_LIVECOUNT_UPDATED: "app:data:redbullevent:livecountupdated",
            EVT_NEW_STREAM_TRACKED: "app:data:redbullevent:newstreamtracked",

            // when should we check an event status
            _changeTrackingPeriod: 10 * 1000,
            // do not track events starting in more than longestTrackingTime ms
            // by default set to 6 months
            _longestTrackingTime: 6 * 30 * 24 * 60 * 60 * 1000,
            // how many events do we want to track ?
            _maxEvents: 100,
            // how many event-stream per event do we want to track ?
            _maxEventStreamsPerEvent: 3,
            // maximum event-streams that we track simultaneously
            _maxEventsTracked: 60,
            // maximum event-streams kept in memory (0 = no limit)
            _maxEventsInMemory: 100,
            // should we include seconds in the component countdown updater
            _countdownsWithSeconds: false,

            _events: [],
            _eventIndexes: {},
            _eventLiveCount: 0,
            _countdowns: [],

            activate: function(opts) {
                this.setOptions(opts);
//                this.trackEvents();
                this.loadup();
            },

            init: function() {
                this._super();
            },

            setOptions: function(opts) {
                opts = opts || {};
                if (opts.longestTrackingTime > 0) {
                    this._longestTrackingTime = opts.longestTrackingTime;
                }
                if (opts.changeTrackingPeriod > 0) {
                    this._changeTrackingPeriod = opts.changeTrackingPeriod;
                }
                if (opts.maxEventsTracked > 0) {
                    this._maxEventsTracked = opts.maxEventsTracked;
                }
                if (opts.maxEventStreamsPerEvent > 0) {
                    this._maxEventStreamsPerEvent = opts.maxEventStreamsPerEvent;
                }
                if (opts.maxEventsInMemory >= 0) {
                    this._maxEventsInMemory = opts.maxEventsInMemory;
                }
                this._countdownsWithSeconds = !!opts.countdownsWithSeconds;
            },

            _pushEvent: function(event) {
                // event already in list
                if (this._eventIndexes[event.id]) {
                    return;
                }
                this._events.push(event);
                this._eventIndexes[event.id] = this._events.length - 1;
                this.__isDataOrdered = false;
            },

            _updateEvent: function(event) {
                // event doesnt exists
                var index = this._eventIndexes[event.id];
                if (index < 0) {
                    return;
                }
                this._events[index] = event;
                this.__isDataOrdered = false;
            },

            _getEvent: function(eventId) {
                return this._events[this._eventIndexes[eventId]];
            },

            _getEventFromIndex: function(index) {
                return this._events[index];
            },

            _deleteEvent: function(event) {
                if (!event || !event.id) {
                    return;
                }
                var index = this._eventIndexes[event.id],
                    length;
                // delete index entry
                delete this._eventIndexes[event.id];
                // delete entry in array
                if (index > -1) {
                    this._events.splice(index, 1);
                }
                // and reindex entries located after deleted item
                for (length = this._events.length; index < length; index++) {
                    this._eventIndexes[this._events[index]] = index;
                }
                this.__isDataOrdered = false;
            },

            _orderEvents: function() {
                // if array already ordered, skip
                if (this.__isDataOrdered === true) {
                    return;
                }

                // order array
                this._events.sort(function(a, b) {
                    // stream priority first
                    if (a.streamPriority !== b.streamPriority) {
                        return b.streamPriority - a.streamPriority;
                    }
                    if (a.streamStatus === sRedbullAPI.StreamStatus.LIVE || b.streamStatus === sRedbullAPI.StreamStatus.LIVE) {
                        if (a.streamStatus !== b.streamStatus) {
                            return a.streamStatus === sRedbullAPI.StreamStatus.LIVE ? -1 : 1;
                        }

                    }
                    // stream start time then
                    return a._msBeforeEvent - b._msBeforeEvent;
                });
                // update indexes
                var i = this._events.length;
                while (--i > -1) {
                    this._eventIndexes[this._events[i].id] = i;
                }
                // we remove extra events if limit is passed
                // we give it some delay in case more event are being added soon
                if (this._maxEventsTracked > 0 && this._events.length > this._maxEventsTracked) {
                    util.clearDelay("_cleanExtraES");
                    util.delay(10, "_cleanExtraES").then(util.bind(function() {
                        var evt;
                        for (i = this._events.length; i > this._maxEventsTracked; i--) {
                            // remove/untrack evt from list
                            if (!this._maxEventsInMemory || i <= this._maxEventsInMemory) {
                                evt = this._events[i-1];
                                this.untrackEventStream(evt);
                            } else {
                                evt = this._events.pop();
                                delete this._eventIndexes[evt.id];
                                this.untrackEventStream(evt);
                            }
                            console.log("[rbe]Event-stream '" + evt.id + "' thrown away (max event-streams number reached)");
                        }
                    }, this));
                }
                this.__isDataOrdered = true;
                console.log("[rbe]Event-streams array reordered");
            },

            loadup: function() {
                var self = this;
//                sRedbullAPI.getResource("https://api-staging.redbull.tv/v1/videos/event_streams?limit=100").then(function(data) {
                sRedbullAPI.getResource(sRedbullAPI._baseUrl + "/videos/event_streams?limit=100").then(function(data) {
                    console.log("[rbe]LIVE EVENTS: ", data);
                    for (var i=0, imax=data.videos.length; i<imax; i++) {
                        self.trackEventStream(data.videos[i]);
                    }
                });
            },

            trackEvents: function() {
                var self = this;
                // FIXME: if maxEvents is > to 100 (cf. sRedbullAPI -> _maxRequestsLimit)
                // we would have to do several requests
                // right now we will consider 100 is already a big enough number
                sRedbullAPI.getEvents(this._maxEvents).then(function(events) {
                    console.log("[rbe]Events: ", events);
                    // for each event we will track the first event-streams
                    // let's chain the calls so we don't kill perfs
                    self._trackEventsInChain(events.shows, 0, self);
                }, function() {
                    console.log("[rbe]Error while fetching the events list!");
                });
            },

            _trackEventsInChain: function(events, curIndex, ctx) {
                if (curIndex > events.length-1) {
                    return;
                }
                // get list of _maxEventStreamsPerEvent event-streams for cur event
                sRedbullAPI.getEvent(events[curIndex].id, ctx._maxEventStreamsPerEvent).then(function(eventStreams) {
                    var streams = eventStreams.videos,
                        j = 0,
                        jmax = streams.length < ctx._maxEventStreamsPerEvent ? streams.length : ctx._maxEventStreamsPerEvent;
                    for (; j<jmax; j++) {
                        // give some randomness to prevent tons of events tracking during the same time
                        util.delay(Math.floor(Math.random()*3) + 1).then((function(streamEvt) {
                            return function() {
                                ctx.trackEventStream(streamEvt);
                            };
                        })(streams[j]));
                    }
                    // and recurse for next event
                    ctx._trackEventsInChain(events, curIndex + 1, ctx);
                }, function() {
                    console.log("[rbe]Error while fetching event-streams!");
                    ctx._trackEventsInChain(events, curIndex + 1, ctx);
                });
            },

            getLiveEventStreamsCount: function() {
                return this._eventLiveCount;
            },

            _incLiveCount: function() {
                this._eventLiveCount++;
                this.dispatchEvent(this.EVT_LIVECOUNT_UPDATED, {
                    newCount: this._eventLiveCount
                });
            },

            _decLiveCount: function() {
                if (this._eventLiveCount > 0) {
                    this._eventLiveCount--;
                    this.dispatchEvent(this.EVT_LIVECOUNT_UPDATED, {
                        newCount: this._eventLiveCount
                    });
                }
            },

            getLiveEventStreams: function() {
                this._orderEvents();
                var i = -1, len = this._events.length, lives = [];
                while (++i < len) {
                    if (this._events[i].streamStatus === sRedbullAPI.StreamStatus.LIVE) {
                        lives.push(this._events[i]);
                    }
                }
                return lives;
            },

            getClosestEventStreams: function(number) {
                this._orderEvents();
                if (!number) {
                    return this._events;
                }
                return this._events.slice(0, number);
            },

            getClosestEventStreamsPerEvent: function(nbEventStreamsPerEvent, totalNbEventStreams, includeLive) {
                this._orderEvents();
                var i = -1, len = this._events.length, curNbEvts = 0, stats = {}, curEs, stat, res = [];
                while (++i < len && curNbEvts < totalNbEventStreams) {
                    curEs = this._events[i];
                    // skip currently live event streams
                    if (curEs.object.stream.status === sRedbullAPI.StreamStatus.LIVE && !includeLive) {
                        continue;
                    }
                    stat = stats[curEs.object.show.id];
                    if (!stat) { stat = 0; }
                    if (stat < nbEventStreamsPerEvent) {
                        stats[curEs.object.show.id] = stat + 1;
                        res[curNbEvts++] = curEs;
                    }
                }
                return res;
            },

            _getSelfUpdatableDataLoader: function(fct, ds, args) {
                var self = this,
                    data = [],
                    dataProviderFct,
                    _id = Math.floor(Math.random() * 10000 + 1);

                dataProviderFct = fct;

                var updateFct = function(evt) {
                    var f = function() {
                        // we need to check if anything changed between the previous and new data
                        // first we can check the arrays sizes: if it differs we can be sure something changed
                        var newdata = dataProviderFct.apply(self, args),
                            differ = newdata.length !== data.length;
                        // if same size, we need to make sure that something changed between the 2 versions
                        if (!differ) {
                            for (var j=0, jmax = data.length; j<jmax; j++) {
                                var jsonOldData = util.stringify(data[j]),
                                    jsonNewData = util.stringify(newdata[j]);
                                if (jsonOldData !== jsonNewData) {
                                    differ = true;
                                    break;
                                }
                            }
                        }
                        // something changed, refresh the ds
                        if (differ) {
                            data = newdata;
                            if (ds) {
                                ds.fetch(0, data.length);
                            }
                        }
                    };

                    if (evt.__delay) {
                        util.clearDelay("_evtDelay_" + _id);
                        util.delay(1.0, "_evtDelay_" + _id).then(f);
                    } else {
                        f(evt);
                    }
                };

                // we're gonna listen to any update in the tracked event, and check if our array needs to be updated
                this.addEventListener(this.EVT_UPDATED, updateFct);
                this.addEventListener(this.EVT_STATUS_UPDATED, updateFct);
                this.addEventListener(this.EVT_NEW_STREAM_TRACKED, updateFct);
                this.addEventListener(this.EVT_START_TIME_UPDATED, updateFct);


                var timer = null, count = 0;
                return function(from, size) {
                    var deferred = promise.defer();

                    var fct = function() {
                        if (data.length > 0) {
                            deferred.resolve({
                                data: data.slice(from, from + size),
                                total: data.length
                            });
                        }
                        else {
                            // the first time we don't resolve until we got data or after 5 secs
                            if (timer) {
                                deferred.resolve({
                                    data: [{id:-1, message:"no data"}],
                                    total: 1
                                });
                            }
                        }
                    };

                    fct();

                    if (!timer) {
                        timer = setInterval(function() {
                            count += 1000;
                            data = dataProviderFct.apply(self, args);
                            // we loop while no result found, for 5 secs
                            if (data.length > 0 || count > 5000) {
                                clearInterval(timer);
                                fct();
                            }
                        }, 1000);
                    }

                    return deferred.promise;
                };
            },

            getClosestEventStreamsPerEventDataLoader: function(ds, nbEventStreamsPerEvent, totalNbEventStreams, includeLive) {
                return this._getSelfUpdatableDataLoader(util.bind(this.getClosestEventStreamsPerEvent, this), ds, [nbEventStreamsPerEvent, totalNbEventStreams, includeLive]);
            },

            getLiveEventStreamsDataLoader: function(ds) {
                return this._getSelfUpdatableDataLoader(util.bind(this.getLiveEventStreams, this), ds);
            },

            _getCountdown: function(date) {
                return date - new Date();
            },

            _createEventChangeTrackingTimer: function(event) {
                var eventChangeTrackingTimerId = setInterval(util.bind(function() {
                    this._eventChangeTrackingHandler(event);
                }, this), this._changeTrackingPeriod);
                return eventChangeTrackingTimerId;
            },

            _eventChangeTrackingHandler: function(event) {
                // contact redbull API to get updated status of the event
                sRedbullAPI.getVideoStreamStatus(event.id).then(util.bind(function(streamUpdate) {
                    if (!streamUpdate) {
                        return;
                    }

                    var oldStatus = event.streamStatus,
                        evtObj;

                    // stream status changed
                    if (streamUpdate.status && event.streamStatus !== streamUpdate.status) {
                        evtObj = {
                                eventType: this.EVT_STATUS_UPDATED,
                                eventId: event.id,
                                oldStatus: oldStatus,
                                newStatus: streamUpdate.status
                            };

                        console.log("stream " + event.id + " changed state from " + oldStatus + " to " + streamUpdate.status);


                        // update evt object
                        event.streamStatus = streamUpdate.status;
                        event.object.stream.status = streamUpdate.status;

                        // dispatch status evt_updated events
                        this.dispatchEvent(this.EVT_STATUS_UPDATED, evtObj);
                        this.dispatchEvent(this.EVT_STATUS_UPDATED_FOR_STREAM(event.id), evtObj);

                        // if event-stream goes live
                        if (streamUpdate.status === sRedbullAPI.StreamStatus.LIVE) {
                            // inc live event counter and ask for a reorder of the list (as live events are first in out ordered list)
                            this._incLiveCount();
                            this.__isDataOrdered = false;
                            // if the event has never been live before, dispatch a ready event
                            if (oldStatus === sRedbullAPI.StreamStatus.PRE_EVENT || oldStatus === sRedbullAPI.StreamStatus.SOON) {
                                console.log("stream " + event.id + " READY EVENT");

                                // dispatch a general event
                                this.dispatchEvent(this.EVT_READY, {
                                    event: event
                                });
                                // display an evt specific for this stream
                                this.dispatchEvent(this.EVT_READY_FOR_STREAM(event.id), {
                                    event: event
                                });
                            }
                        }
                        // if the event-stream was live, dec live evt counter and reorder list
                        else if (oldStatus === sRedbullAPI.StreamStatus.LIVE) {
                            this._decLiveCount();
                            this.__isDataOrdered = false;

                            console.log("stream " + event.id + " was live,  not anymore");

                        }
                    }
                    // stream start time changed
                    if (streamUpdate.starts_at && event.streamStartTime !== streamUpdate.starts_at) {
                        // calculate when is the event
                        var msBeforeEvent = this._getCountdown(Utils.isoDate(streamUpdate.starts_at));
                        evtObj = {
                                eventType: this.EVT_START_TIME_UPDATED,
                                eventId: event.id,
                                oldStartTime: event.streamStartTime,
                                newStartTime: streamUpdate.starts_at,
                                msBeforeEvent: msBeforeEvent
                            };

                        // update evt object
                        event.streamStartTime = streamUpdate.starts_at;
                        event.object.stream.starts_at = streamUpdate.starts_at;
                        event._msBeforeEvent = msBeforeEvent;

                        // then we need to change the wake up timer to a new one, reflecting the change in time
                        clearInterval(event._eventWakeupTimerId);
                        if (msBeforeEvent > 0) {
                            event._eventWakeupTimerId = this._createWakeupTimer(event, msBeforeEvent);
                        }

                        // now that the time changed, we need to reorder our array immediately
                        this.__isDataOrdered = false;
                        this._orderEvents();

                        // dispatch a start time updated event event
                        this.dispatchEvent(this.EVT_START_TIME_UPDATED, evtObj);
                        this.dispatchEvent(this.EVT_START_TIME_UPDATED_FOR_STREAM(event.id), evtObj);
                    }
                    // stream end time changed
                    if (streamUpdate.ends_at && event.streamEndTime !== streamUpdate.ends_at) {
                        evtObj = {
                                eventType: this.EVT_END_TIME_UPDATED,
                                eventId: event.id,
                                oldEndTime: event.streamEndTime,
                                newEndTime: streamUpdate.ends_at
                            };

                        // update evt object
                        event.streamEndTime = streamUpdate.ends_at;
                        event.object.stream.ends_at = streamUpdate.ends_at;

                        // dispatch a end time updated event event
                        this.dispatchEvent(this.EVT_END_TIME_UPDATED, evtObj);
                        this.dispatchEvent(this.EVT_END_TIME_UPDATED_FOR_STREAM(event.id), evtObj);
                    }
                    // stream priority changed
                    if (streamUpdate.priority && event.streamPriority !== streamUpdate.priority) {
                        evtObj = {
                                eventType: this.EVT_PRIORITY_UPDATED,
                                eventId: event.id,
                                oldPriority: event.streamPriority,
                                newPriority: streamUpdate.priority
                            };

                        // update evt object
                        event.streamPriority = streamUpdate.priority;
                        event.object.stream.priority = streamUpdate.priority;

                        // now that the priority changed, we need to reorder our array immediately
                        this.__isDataOrdered = false;
                        this._orderEvents();

                        // dispatch a end time updated event event
                        this.dispatchEvent(this.EVT_PRIORITY_UPDATED, evtObj);
                        this.dispatchEvent(this.EVT_PRIORITY_UPDATED(event.id), evtObj);
                    }

                    // if any update happened
                    if (evtObj) {
                        console.log("stream " + event.id + ": dispatching EVT_UPDATED");

                        // dispatch a general evt_updated event
                        this.dispatchEvent(this.EVT_UPDATED, evtObj);
                        // display a specific event for this stream
                        this.dispatchEvent(this.EVT_UPDATED_FOR_STREAM(event.id), evtObj);
                    }

                }, this));
            },

            _createWakeupTimer: function(event, msBeforeEvent) {
                // bounded with 0x7FFFFFFF as timer doesn't handle a bigger number
                // but it's more than 3 weeks ahead, won't matter...
                var eventWakeupTimerId = setTimeout(util.bind(function() {
                    this._eventWakeupHandler(event);
                }, this), msBeforeEvent > 0x7FFFFFFF ? 0x7FFFFFFF : msBeforeEvent);

                // return this timer id
                return eventWakeupTimerId;
            },

            _eventWakeupHandler: function(evt) {
                var self = this;
                // after some time we will check the event status
                util.delay(10).then(function() {
                    self._eventChangeTrackingHandler(evt);
                });
            },

            trackEventStream: function(eventStream) {
                // wrong data, nothing to do
                if (!eventStream && !eventStream.stream) {
                    return;
                }

                // if the event is already tracked, return
                if (this.getTrackedEventStream(eventStream.id)) {
                    return;
                }

                // if the event is already over, nothing to track
                if (eventStream.stream.status === sRedbullAPI.StreamStatus.COMPLETE || eventStream.stream.status === sRedbullAPI.StreamStatus.REPLAY) {
                    console.log("[rbe]Event-stream '" + eventStream.id + "' already over, skipped");
                    return;
                }

                var countdown = this._getCountdown(Utils.isoDate(eventStream.stream.starts_at));
                // if the event is too far away in time
                if (this._longestTrackingTime > 0 && countdown > this._longestTrackingTime) {
                    console.log("[rbe]Event-stream '" + eventStream.id + "' starting time is too far in time, skipped");
                    return;
                }

                // if event has already started
                if (countdown < 0) {
                    // we check if the event is really over
                    var eventStreamStatus = eventStream.stream.status,
                        apiStreamStatus = sRedbullAPI.StreamStatus,
                        eventStreamEnds = this._getCountdown(Utils.isoDate(eventStream.stream.ends_at)) < 0;

                    if (eventStreamStatus !== apiStreamStatus.LIVE &&
                        eventStreamStatus !== apiStreamStatus.LIVE_WAITING &&
                        eventStreamStatus !== apiStreamStatus.PAUSE && eventStreamEnds)
                    {
                        console.log("[rbe]Event-stream '" + eventStream.id + "' already over (eventhough status has not been set to over -redbull API bug-), skipped");
                        return;
                    }
                }

                // if event is already live, inc live counter
                if (eventStream.stream.status === sRedbullAPI.StreamStatus.LIVE) {
                    this._incLiveCount();
                }

                // create event object
                var evt = {
                        id: eventStream.id,
                        object: eventStream,
                        streamStatus: eventStream.stream.status,
                        streamPriority: eventStream.stream.priority,
                        streamStartTime: eventStream.stream.starts_at,
                        streamEndTime: eventStream.stream.ends_at,
                        _msBeforeEvent: countdown
                    };

                // timer that will regularly check status
                evt._eventChangeTrackingTimerId = this._createEventChangeTrackingTimer(evt);
                // timer waking up when event starts for this event (if event not already started)
                if (countdown > 0) {
                    evt._eventWakeupTimerId = this._createWakeupTimer(evt, evt._msBeforeEvent);
                }

                // save event
                this._pushEvent(evt);

                this.dispatchEvent(this.EVT_NEW_STREAM_TRACKED, {
                    event: evt,
                    __delay: true
                });

                // reorder array after some delay (in case this method would be called in loop or such)
                util.clearDelay("_rdlay");
                util.delay(0.5, "_rdlay").then(util.bind(function() {
                    this._orderEvents();
                }, this));

                console.log("[rbe]Starting tracking event-stream: ", evt);
            },

            untrackEventStream: function(event) {
                var evt = event.id ? event : this._getEvent(event);
                // no event matching, nothing to do
                if (!evt) {
                    return;
                }
                // we are gonna stop the tracking timers
                clearInterval(evt._eventChangeTrackingTimerId);
                clearInterval(evt._eventWakeupTimerId);
                // and if the event was live, dec live counter
                if (evt.streamStatus === sRedbullAPI.StreamStatus.LIVE) {
                    this._decLiveCount();
                }

                console.log("[rbe]Event-stream '" + evt.id + "' untracked");
            },

            getTrackedEventStream: function(eventId) {
                return this._getEvent(eventId);
            },

            clearTrackedEventStreams: function() {
                var len = this._events.length;
                while (--len > -1) {
                    var evt = this._events[len];
                    clearInterval(evt._eventChangeTrackingTimerId);
                    clearInterval(evt._eventWakeupTimerId);
                }
                this._events.length = 0;
                this._eventIndexes = {};
                this._eventLiveCount = 0;
                this.__isDataOrdered = false;
            },

            attachCountdownToComponent: function(parent, component, date) {
                var self = this;
                component.addClass("countdown-text-color");
                // the main refresh function, called by the setInterval, and in charge of updating all specified countdowns
                var fct = function() {
                    var i = self._countdowns.length;
                    if (!i) {
                        clearInterval(self.__timerCd);
                    }
                    while (--i > -1) {
                        var obj = self._countdowns[i], p = obj.parent, c = obj.component, t = obj.time;
                        if (!p) {
                            self._countdowns.splice(i, 1);
                            continue;
                        }
                        if (t <= 0) {
                            if (!!c) {
                                c.setText("Right now");
                                c.removeClass("countdown-text-color");
                            }
                            self._countdowns.splice(i, 1);
                            continue;
                        }
                        if (p._inDOMTree && c) {
                            //c.setText(Utils._replaceSpaces(Utils.countdownDate(t, self._countdownsWithSeconds, true)));

                            c.setText(Utils.prettyDate(t, true));
                        }
                        obj.time -= self._countdownsWithSeconds ? 1000 : 60000;
                    }
                };
                if (!this.__timerCd && this._countdowns.length>0) {
                    this.__timerCd = setInterval(function() {
                        fct();
                    }, this._countdownsWithSeconds ? 1000 : 60000);
                }
                var duplicate = false;
                var q, len = this._countdowns.length;
                for(q=0;q<len;q++){
                    if(this._countdowns[q].parent === parent && this._countdowns[q].component === component){
                        duplicate = true;
                        break;
                    }
                }
                if(!duplicate){
                    this._countdowns.push({
                        parent: parent,
                        component: component,
                        time: date - new Date()
                    });
//                    fct();
                }
            },

            deinit: function() {

            }

        });

        return new RedbullEventsTracker();

    });

define("app/widget/sLoader",
[
    "ax/class",
    "ax/core",
    "ax/Env",
    "ax/device",
    "app/widget/Loader",
    "app/util/Utils"
],function(klass,  core, Env, device, loader, appUtils) {
    var sEnv = Env.singleton();
    var Loader = klass.create(loader, {}, {
        init: function() {
            this._super({
                delayAmount: 2.5,
                blockKeys: true // We want to block keys upon loading
            });
        },

        onHideCallback: function() {
            if (appUtils.firstShow) {
                // fix provided by Samsung.

                //It’s reproduced only, when env onload event is delayed. Due to this delayed env onload event, the TVArea initialization is delayed.
                //When hide function in widget/sSplashscreen.js tries to set the TVArea visible, it is not done as the TVArea has not been instantiated at all.
                //Also while setting visbility to true, the firstShow argument is set to false.
                //The TVArea is set to visible in widget/sLoader.js  only when firstShow is true, which means that now TVArea would never be visible. This cause black screen defect.
                var startApp = function(){
                    try{
                        sEnv.dispatchEvent("FOCUS_PAGE");
                    } catch(e) {
                        window.setTimeout(startApp,1000);
                    }
                };
                if (device.platform === "samsung") {
                    window.setTimeout(startApp, 5000);
                } else {
                    startApp();
                }
            }
        },

        show: function() {
            if (!this._inDOMTree) {
                this.getRoot().appendTo(core.root.document.body);
            }
            this._super();
        }
    });

    return new Loader({});
});

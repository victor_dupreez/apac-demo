/**
 * An image button widget.
 * @class wgt/ImageButton
 * @param {Object} opts The options object
 * @param {String|ax/af/mvc/ModelRef} opts.text The text displayed on this button
 * @param {String} opts.isPureText whether the button text to set is pure text or html
 */
define("app/widget/ImageButton", ["ax/class", "ax/af/Container", "ax/Element", "ax/ext/ui/Image", "ax/ext/ui/Label", "ax/af/mvc/ModelRef", "app/data/MenuItemImage"],
    function(klass, Container, Element, Image, Label, ModelRef, MenuItemImage) {
        return klass.create(Container, {}, {
            /**
             * The internal label widget to show button text
             * @protected
             * @name _label
             * @memberof ax/af/wgt/Button#
             */
            _label: null,
            /**
             * override parent"s init() method
             * @method
             * @memberof ax/af/wgt/Button#
             * @protected
             */
            init: function(opts) {
                opts.focusable = true;
                opts.clickable = true;
                opts.root = opts.root || new Element("div");

                this._super(opts);

                this.getRoot().addClass("image-button");
                if (opts.bg) {
                    this.createImage("image", opts.bg);
                } else if(opts.css) {
                    this.createImageContainer("css", opts.css);

                } else if (opts.icons && opts.icons.icon) {
                    this.createImage("image", opts.icons.icon.uri);
                    this.createImage("image_focused", opts.icons.icon_active.uri);

                    this.image_focused.hide();

                    this._icon = opts.icons.icon.uri;
                    this._focusIcon = opts.icons.icon_active.uri;
                }

                if (opts.text) {
                    this._label = new Label({
                        text: opts.text,
                        css: "image-button-text",
                        parent: this
                    });

                }
            },

            setBackgroundImage: function(src) {
                var objImage = new MenuItemImage({
                    src: src
                });
                this.image.setSrc(new ModelRef(objImage, "src"));

            },

            createImage: function(property, src) {               
                this[property] = new Image({
                    src: src,
                    parent: this
                });         
            },
            
            createImageContainer: function(property, value) {
                this[property] = new Container({
                    css: value,
                    parent: this
                });
            }
        });
    });

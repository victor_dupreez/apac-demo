define("app/widget/Keyboard", [
        "ax/class",
        "ax/Env",
        "ax/af/Container",
        "ax/Element",
        "ax/ext/ui/input/Keyboard",
        "ax/ext/ui/input/Input",
        "ax/device",
        "ax/util",
        "ax/af/mvc/view",
        "ax/af/evt/type",
        "ax/console",
        "ax/core",
        "ax/config",

        "app/widget/ImageButton"
    ],
    function(klass, Env, Container, Element, Keyboard, Input, device, util, view, evtType, console, core, config, ImageButton) {
        return klass.create(Container, {}, {
            init: function(opts) {
                this.Env = Env.singleton();
                opts.css = opts.css || "overlay-keyboard";
                this._super(opts);

                this.opts = opts;

                if (device.platform === "samsung" && device.id.getFirmwareYear() > 2011) {
                    // Require the needed samsung ime parts.
                    amd.require(["$MANAGER_WIDGET/Common/IME_XT9/ime.js", "$MANAGER_WIDGET/Common/IME_XT9/inputCommon/ime_input.js"], util.bind(function() {
                        console.log("REQUIRE SUCCESS");
                        //create samsung needed parts
                        this._setupSamsungKeyboard(opts);
                    }, this), function() {
                        // This should never happen, fingers crossed
                        console.log("REQUIRE FAIL");
                    });
                } else {
                    this._createKeyboard(opts);
                }

                this.hide();
            },

            /**
             * Placeholder for the on close callback.
             */
            _onCloseCallback: function() {
                console.log("on keyboard close");
            },

            /**
             * Placerholder for on text change callback.
             */
            _onTextChange: function() {
                console.log("text changed");
            },

            /**
             * Sets the on text change callback.
             * This will be called upon text change in the keyboard.
             *
             * @param {function} onTextChange The callback function for when the text changes.
             */
            setOnTextChange: function(onTextChange) {
                this._onTextChange = onTextChange;
            },

            /**
             * Sets the callback function for when the keyboard is closed.
             * @param {function} onClose Callback function for when the keyboard closes.
             */
            setOnClose: function(onClose) {
                this._onCloseCallback = onClose;
            },

            /**
             * This function will hide the keyboard.  It will be called when "search" or back key is pressed.
             */
            onClose: function() {
                this.hideKeyboard();
                //this._onCloseCallback();
            },

            /**
             * Creates the on screen keyboard using XDK keyboard widget.
             */
            _createKeyboard: function() {

                var container = view.render({
                    klass: Container,
                    id: "keyboardContainer",
                    css: "keyboard-container",
                    forwardFocus: true,
                    children: [{
                        klass: Input,
                        id: "input",
                        css: "keyboard-input",
                        useNative: false
                    }, {
                        klass: Keyboard,
                        id: "kb",
                        css: "onscreen-keyboard",
                        keyMaps: {
                            "default": [
                                ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", {
                                    display: new ImageButton({
                                        css: "keyboard-delete-key",
                                        id: "#keyDelete"
                                    }),
                                    keyId: "keyboard:delete"
                                }],
                                ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "^", "*"],
                                ["a", "s", "d", "f", "g", "h", "j", "k", "l", "~", "@", "!"],
                                ["z", "x", "c", "v", "b", "n", "m", ",", ".", "?", "-", {
                                    display: new ImageButton({
                                        css: "keyboard-enter-key"
                                    }),
                                    keyId: "keyboard:enter"
                                }],
                                [{
                                    display: "space",
                                    input: " ",
                                    colSpan: 6
                                }, {
                                    display: "search",
                                    keyId: "keyboard:search",
                                    colSpan: 4
                                }, {
                                    display: new ImageButton({
                                        css: "keyboard-left-key"
                                    }),
                                    keyId: "keyboard:left-arrow"
                                }, {
                                    display: new ImageButton({
                                        css: "keyboard-right-key"
                                    }),
                                    keyId: "keyboard:right-arrow"
                                }]
                            ],
                            "capital": [
                                ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", {
                                    display: new ImageButton({
                                        css: "keyboard-delete-key",
                                        id: "#keyDelete"
                                    }),
                                    keyId: "keyboard:delete"
                                }],
                                ["Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P", "^", "*"],
                                ["A", "S", "D", "F", "G", "H", "J", "K", "L", "~", "@", "!"],
                                ["Z", "X", "C", "V", "B", "N", "M", ",", ".", "?", "-", {
                                    display: new ImageButton({
                                        css: "keyboard-enter-key"
                                    }),
                                    keyId: "keyboard:enter"
                                }],
                                [{
                                    display: "abc",
                                    keyId: "keyboard:FromABCToabc"
                                }, {
                                    display: "12#",
                                    keyId: "keyboard:FromABCToNumber"
                                }, {
                                    display: "space",
                                    input: " ",
                                    colSpan: 2
                                }, {
                                    display: "search",
                                    keyId: "keyboard:search",
                                    colSpan: 2
                                }, {
                                    display: new ImageButton({
                                        css: "keyboard-left-key"
                                    }),
                                    keyId: "keyboard:left-arrow"
                                }, {
                                    display: new ImageButton({
                                        css: "keyboard-right-key"
                                    }),
                                    keyId: "keyboard:right-arrow"
                                }]
                            ],
                            "number": [
                                ["1", "2", "3", "4", "5", "6"],
                                ["7", "8", "9", "0", ":", ";"],
                                [".", ",", "?", "!", "'", "\""],
                                ["@", "&", "$", "#", "(", ")"],
                                ["-", "\\", {
                                    display: new ImageButton({
                                        css: "keyboard-delete-key"
                                    }),
                                    keyId: "keyboard:delete"
                                }, {
                                    display: new ImageButton({
                                        css: "keyboard-enter-key"
                                    }),
                                    keyId: "keyboard:enter"
                                }],
                                [{
                                    display: "ABC",
                                    keyId: "keyboard:FromNumberToABC"
                                }, {
                                    display: "abc",
                                    keyId: "keyboard:FromNumberToabc",
                                    defaultFocus: true
                                }, {
                                    display: "space",
                                    input: " ",
                                    colSpan: 2
                                }, {
                                    display: "search",
                                    keyId: "keyboard:search",
                                    colSpan: 2
                                }]
                            ]
                        }
                    }]
                });

                this.attach(container);

                this.keyboard = container.find("kb");
                this.keyboardInputField = container.find("input");

                // This should be tweaked later on
                // This is needed so the focus doesn't automagically jump away from the keyboard.

                this.keyboard.onEdge = function(direction) {
                    console.log(arguments);
                    if (direction === "nextDown") {
                        return true;
                    }
                    return false;
                };

                // Listen to text change on the keyboard input field.
                this.keyboardInputField.addEventListener(evtType.TEXT_CHANGED, util.bind(function(text) {
                    this._onTextChange(text);
                }, this));

                this._setupKeyboardHandling();
            },

            /**
             * Show suggestions for samsung native keyboard.
             * @param  {object} suggestions containing settings for the suggestion to be displayed.
             *                               e.g {request : "searchtext", items : ["search text", "search for this instead"]}
             *                               will display the items as suggestions.
             */
            showSuggestions: function(suggestions) {
                console.log("show suggestions");
                if (device.platform === "samsung") {
                    this.imeBox.showAutoCompletePopup(suggestions);
                }
            },

            /**
             * Shows the keyboard
             */
            showKeyboard: function(opts) {
                if (this.imeBox) {

                    if (this.keyboardInputField && opts.text) {
                        this.keyboardInputField.value = opts.text;
                    }

                    this.imeBox.onShow();
                } else {

                    this.show();

                    if (this.keyboardInputField && opts.text) {
                        this.keyboardInputField.setText(opts.text);
                    }
                }
            },

            /**
             * [hideKeyboard description]
             * @return {[type]} [description]
             */
            hideKeyboard: function() {
                if (this.imeBox) {
                    document.getElementById("samsungTextInputField").value = "";
                    this.imeBox.onClose();
                    this.Env.removeEventListener(this.Env.EVT_ONUNLOAD, util.bind(function() {
                        this.imeBox.onClose();
                    }, this));
                } else {
                    this.keyboardInputField.setText("");
                    this.hide();
                }
            },

            /**
             * Creates a samsung native keyboard.
             * @param  {object} opts Additional settings for the keyboard. e.g inputTitle
             */
            _setupSamsungKeyboard: function() {

                var samsungInputField = document.getElementById("samsungTextInputField");

                // If the samsungTextInputFied was not retrieved we need to create it.
                if (!samsungInputField) {
                    samsungInputField = new Element("input", {
                            id: "samsungTextInputField",
                            css: "samsungTextInputField",
                            type: "text",
                            maxlength: config.get("samsungMaxInputLength", 50)
                        },
                        core.root.document.body
                    );
                    samsungInputField = samsungInputField.getHTMLElement();
                }

                this.imeBox = new IMEShell_Common();

                // This is needed for samsung 2013 to make sure the keyboard is closed if exiting the app by using source / smarthub key.
                this.Env.addEventListener(this.Env.EVT_ONUNLOAD, util.bind(function() {
                    this.imeBox.onClose();
                }, this));

                this.imeBox.inputTitle = this.opts.inputTitle || "Input Title";
                this.imeBox.inputboxID = "samsungTextInputField";
                this.keyboardInputField = samsungInputField;

                this.imeBox.onKeyPressFunc = util.bind(function(key, str) {
                    console.log("ONKEYPRESSFUNC: " + key + ", " + str);
                    switch (key) {
                        case (29443):
                            // Enter Key
                            this._onCloseCallback(str);
                            this.hideKeyboard();
                            break;
                            //might not be needed, will test 2013-10-18, ML
                        case (222):
                            this.hideKeyboard();
                            break;
                        case (88):
                            //return
                        case (45):
                            //exit
                            this.hideKeyboard();
                            this._onCloseCallback(false);
                            break;
                    }
                }, this);

                // It's needed to override this function if you want to handle search suggestion by yourself and disable the default suggestions.
                this.imeBox.onCompleteFunc = function(value) {
                    console.log("onCompleteFunc: " + value);
                };

                this.imeBox.onTextChangeFunc = util.bind(function(value) {
                    console.log("onTextChangeFunc: " + value);
                    if (this._onTextChange) {
                        this._onTextChange(value);
                    }
                }, this);

                this.imeBox.onClose();

            },

            /**
             * Setup event handling for the xdk keyboard
             */
            _setupKeyboardHandling: function() {

                this.keyboardInputField.startInput();

                this.keyboard.onHandler = util.bind(function(id) {
                    var info = id;
                    switch (info) {
                        // Do the actual search here.
                        case "keyboard:enter":
                        case "keyboard:search":
                            //do not close the keyboard if search field is empty
                            if(this.keyboardInputField.getText() === "") {
                                return;
                            }

                            this._onCloseCallback(this.keyboardInputField.getText());
                            this.onClose();
                            break;

                        case "keyboard:delete":
                            this.keyboardInputField.backspace();
                            break;

                        case "keyboard:left-arrow":
                            this.keyboardInputField.moveCursor("left");
                            break;

                        case "keyboard:right-arrow":
                            this.keyboardInputField.moveCursor("right");
                            break;


                        case "keyboard:FromABCToabc":
                            this.keyboard.setFocusPosition("default", [5, 0]);
                            this.keyboard.switchKeyMap("default");
                            break;

                        case "keyboard:FromabcToABC":
                            this.keyboard.setFocusPosition("capital", [5, 0]);
                            this.keyboard.switchKeyMap("capital");
                            break;

                        case "keyboard:FromNumberToabc":
                            this.keyboard.setFocusPosition("default", [5, 1]);
                            this.keyboard.switchKeyMap("default");
                            break;
                        case "keyboard:FromNumberToABC":
                            this.keyboard.setFocusPosition("capital", [5, 0]);
                            this.keyboard.switchKeyMap("capital");
                            break;

                        case "keyboard:FromABCToNumber":
                        case "keyboard:FromabcToNumber":
                            this.keyboard.setFocusPosition("number", [5, 1]);
                            this.keyboard.switchKeyMap("number");
                            break;
                        case "keyboard:ABC":
                            this.keyboard.switchKeyMap("capital");
                            break;

                    }
                    return false;
                }, this);

                this.addEventListener(evtType.KEY, util.bind(function(key) {
                    if (key.id === "device:vkey:back") {
                        this._onCloseCallback(false);
                        this.onClose();
                    }
                }, this));
            }
        });
    });

/*
* Grid item to hold an image and metadata. Use CSS for all styling.
* Usage (all options are optional, mix and match as necessary)
   var item = new GridItem({
       css: "item-normal", // Use this to style a specific type of grid items
       imageUrl: data.img,
       liveNow: true, // Show "Live now" label or not
       tag: "tag", // Category tag
       tagMaxLength: 40, // Truncate the tag if it is longer than 40 characters
       title: "Title",
       titleMaxLength: 20,
       description: "My item description",
       descriptionMaxLength: 100,
       subtitle: "My Subtitle",
       subtitleMaxLength: 30,
       info: "Info label",
       infoMaxLength: 40,
       time: "5d 6h 44m",
       timeMaxLength: 50
   })
*/
define("app/widget/RedBullGridItem", [
        "ax/class",
        "ax/af/Container",
        "ax/ext/ui/Image",
        "ax/ext/ui/Label",
        "ax/ext/ui/Button",
        "ax/util"
    ],
    function(klass, Container, Image, Label, Button, util) {

        return klass.create(Button, {}, {


            /**
             * override's parent's init() method
             * @method
             * @protected
             */
            init: function(opts) {
                opts = opts || {};
                opts.css = opts.css || "";
                opts.css = opts.css + " rb-grid-item";
                this._super(opts);

                this._rootElement = new Container({
                    parent: this,
                    css: "rb-grid-item-container " + (opts.css || "")
                });

                this._backgroundImage = new Image({
                    src: opts.imageUrl,
                    css: "rb-grid-item-image",
                    parent: this._rootElement
                });

                this._overlayContainer = new Container({
                    parent: this._rootElement,
                    css: "rb-grid-item-overlay"
                });

                this._border = new Container({
                    parent: this._rootElement,
                    css: "rb-grid-item-border"
                });

                this._insideContainer = new Container({
                    parent: this._overlayContainer,
                    css: "rb-grid-item-overlay-content"
                });

                this._liveNow = new Label({
                    text: "LIVE NOW",
                    css: "rb-grid-item-live-now",
                    parent: this._insideContainer
                });
                /*if (opts.liveNow) {
                    this.showLiveNowLabel();
                } else {*/
                this.hideLiveNowLabel();
                //}

                var item,
                    containers = [
                        "tag",
                        "title",
                        "description",
                        "subtitle",
                        "info",
                        "time"
                    ],
                    labelCss = "";

                // Remove the badge/tag if it is LIVE.
                // This to not confuse the user that the event is actually live.
                if (opts.tag && opts.tag === "LIVE") {
                    containers.shift();
                }

                for (var i = 0; i < containers.length; i++) {
                    item = containers[i];
                    if (opts[item]) {
                        if (opts[item + "MaxLength"]) {
                            opts[item] = util.truncate(opts[item], opts[item + "MaxLength"]);
                        }

                        labelCss = "rb-grid-item-" + item;

                        // Add class so we can style the right now text to be different than other text such as "Last Week"
                        if (opts[item] === "Right Now") {
                            labelCss += " rb-grid-item-right-now";
                        }

                        this["_" + item] = new Label({
                            text: opts[item],
                            css: labelCss,
                            parent: this._insideContainer
                        });
                    }
                }

            },

            showLiveNowLabel: function() {
                //this._liveNow.show();
            },

            hideLiveNowLabel: function() {
                this._liveNow.hide();
            },

            /**
             */
            deinit: function() {
                this._super();
            }
        });
    });

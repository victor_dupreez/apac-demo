define("app/widget/Footer", [
        "ax/class",
        "ax/af/Container",
        "ax/ext/ui/grid/BoundedGrid",
        "ax/promise"
    ],
    function(klass, Container, BoundedGrid, promise) {
        return klass.create(Container, {}, {
            init: function(opts) {
                this._super(opts);

                this.grid = new BoundedGrid({
                    id: opts.id || "grid-footer",
                    css: "grid-footer",
                    rows: opts.rows || 4,
                    cols: 1,
                    alignment: BoundedGrid.VERTICAL
                });

                this.attach(this.grid);

                this._dataLoader = null;
            },

            clearFooter: function() {
                this.grid.reset();
            },

            getDatasource: function() {
                return this.grid.getDatasource();
            },

            setDisplayStrategy: function(callback) {
                this.grid.setDisplayStgy(callback);
            },

            setDataLoader: function(dataloader) {
                this._dataLoader = dataloader;
            },

            setDatasource: function(datasource, data) {

                if (this._dataLoader) {
                    datasource.setDataLoader(this._dataLoader);
                } else {
                    var footerData = data;
                    datasource.setDataLoader(function(from, to) {

                        var data = footerData || [],
                            returnData = [],
                            len,
                            i = from,
                            deferred = promise.defer();

                        if (data) {
                            len = data.length;
                            for (; (i < to && i < len); i++) {
                                returnData.push({
                                    id: i,
                                    object: data[i]
                                });
                            }

                            deferred.resolve({
                                data: returnData,
                                total: len
                            });
                        } else {
                            deferred.reject("No data");
                        }

                        return deferred.promise;

                    });
                }

                return this.grid.setDatasource(datasource);
            }
        });
    }
);
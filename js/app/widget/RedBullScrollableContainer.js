/**
* Scrollable container that will scroll depending on the focused elements offset position
* RedBullScrollableContainer needs to be initiated like this
*
* klass: RedBullScrollableContainer,
* id: "an id",
* forwardFocus: true,
* children: [{
    * klass: Layout,
    * id: "an id",
    * css: "class name",
    * forwardFocus: true,
    * width: 1,
    * children: [
        focusable children goes here
*
* The Layout child should contain all the elements that can be focused by the app.
* And the Layout child needs to be positioned relative to get the focusable elements to have the it as it's offsetParent.
*/
define("app/widget/RedBullScrollableContainer", [
        "ax/class",
        "ax/device",
        "ax/af/Container",
        "ax/util",
        "ax/af/evt/type",
        "app/util/Utils"
    ],
    function(klass, device, Container, util, evtType, appUtils) {
        return klass.create(Container, {}, {
            init: function(opts) {

                var css = "redbull-scrollable-container";

                opts.css = appUtils.isAnimatedContainers() ? css : css + " disable-animation";

                this._super(opts);

                this._bottomThreshold = opts.bottomThreshold || 45;
                this._topThreshold = opts.topThreshold || 45;


                //this._keepMarginAtTop = opts.keepMarginAtTop || false;

                this.addEventListener(evtType.FOCUS, util.bind(function(focus) {

                    var resolution = device.system.getDisplayResolution();

                    if (focus && focus.target) {
                        var element = focus.target.getRoot().getHTMLElement(),
                            boundingClientRect = element.getBoundingClientRect(),
                            viewMarginTop = element.offsetParent ? element.offsetParent.offsetTop : 0,
                            newPosition;

                        if (boundingClientRect.top <= 0) {
                            newPosition = (viewMarginTop - boundingClientRect.top) + this._topThreshold;
                            newPosition = (newPosition < 0 ? newPosition : 0);
                            if (this.getRoot().getHTMLElement().offsetTop !== newPosition) {
                                this.getRoot().getHTMLElement().style.marginTop = newPosition + "px";
                                this.getRoot().getHTMLElement().style.height = (resolution.height - newPosition) + "px";
                                this.dispatchEvent(evtType.SCROLLED_PERCENTAGE, newPosition);
                            }
                        } else if (element.offsetTop + element.offsetHeight + element.offsetParent.offsetTop > resolution.height - this._bottomThreshold) {
                            newPosition = -element.offsetTop - element.offsetHeight + resolution.height - this._bottomThreshold;
                            if (this.getRoot().getHTMLElement().offsetTop !== newPosition) {
                                this.getRoot().getHTMLElement().style.marginTop = newPosition + "px";
                                this.getRoot().getHTMLElement().style.height = (resolution.height - newPosition) + "px";
                                this.dispatchEvent(evtType.SCROLLED_PERCENTAGE, newPosition);
                            }
                        }
                    }
                }, this));
            }
        });
    }
);
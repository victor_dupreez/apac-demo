define("app/widget/RedBullCountdown", [
        "ax/class",
        "ax/af/Container",
        "ax/ext/ui/Layout",
        "ax/ext/ui/Label",
        "ax/util",
        "app/util/Utils"
    ],
    function(klass, Container, Layout, Label, util, Utils) {

        return klass.create(Container, {}, {

            EVT_COUNTDOWN_OVER: "app:widget:countdown:over",

            init: function(opts) {
                opts = opts || {};
                this._super(opts);

                this.addClass("wgt-countdown");

                this._countdownTitle = new Label({
                    text: opts.title,
                    css: "countdown-title",
                    parent: this
                });

                var countdownLayout = new Layout({
                    alignment: Layout.HORIZONTAL
                });

                var lbls = ["days", "hours", "mins", "secs"],
                    len = lbls.length,
                    i = 0;

                this._labels = {};

                for (; i<len; i++) {
                    var labelCtner = new Container({
                            type: Container,
                            css: "countdown-item"
                        }),
                        labelNr = new Label({
                            id: "label-" + lbls[i],
                            css: "countdown-item-number",
                            text: "00",
                            parent: labelCtner
                        });

                    this._labels[lbls[i]] = labelNr;
                    countdownLayout.attach(labelCtner);
                }

                this.attach(countdownLayout);

                this._countdownDesc = new Label({
                    text: "Going live now...",
                    css: "countdown-desc",
                    parent: this
                });
            },

            deinit: function() {
                clearInterval(this._timer);
                this._super();
            },

            setCountdownTimer: function(msecs) {
                clearInterval(this._timer);
                this.setCountdown(0, 0, 0, 0);

                if (msecs <= 0) {
                    return;
                }
                this._msecs = msecs;
                this._timer = setInterval(util.bind(function() {
                    this._msecs -= 1000;

                    if (this._msecs > 0) {
                        var numbers = Utils.countdownValues(this._msecs, true);
                        this.setCountdown(numbers.days, numbers.hours, numbers.minutes, numbers.seconds);
                    } else {
                        clearInterval(this._timer);
                        this.setCountdown(0, 0, 0, 0);
                        this.dispatchEvent(this.EVT_COUNTDOWN_OVER, {});
                    }
                }, this), 1000);
            },

            stopCountdownTimer: function() {
                clearInterval(this._timer);
            },

            setCountdownDescVisible: function(status) {
                if (status) {
                    this._countdownDesc.show();
                } else {
                    this._countdownDesc.hide();
                }
            },

            setTitle: function(newTitle) {
                this._countdownTitle.setText(newTitle);
            },

            setCountdown: function(days, hours, mins, secs) {
                this.setNumber("days", days);
                this.setNumber("hours", hours);
                this.setNumber("mins", mins);
                this.setNumber("secs", secs);
            },

            setNumber: function(rowId, number) {
                number = number || 0;
                var label = this._labels[rowId];
                if (label) {
                    label.setText(number < 10 ? "0" + number : number);
                }

            }

        });
    });

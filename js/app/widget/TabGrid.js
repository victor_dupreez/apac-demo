define("app/widget/TabGrid", [
        "ax/class",
        "ax/af/Container",
        "ax/util",
        "app/widget/AnimatedGrid",
        "ax/af/data/LocalDatasource",
        "ax/promise",
        "ax/ext/ui/Button",
        "ax/af/evt/type"
    ],
    function(klass, Container, util, AnimatedGrid, Ds, promise, Button, evtType) {

        return klass.create(Container, {}, {
            init: function(opts) {

                opts = opts || {};

                this._super(opts);

                this.createGrid(opts);
            },

            /* To be overridden*/
            onSelect: function() {},

            setOnSelectionChanged: function(callback) {
                this.onSelect = callback;
            },

            changeSelection: function(direction, focus, evt) {
                var dataSource = this.grid.getDatasouce(),
                    data = dataSource._data,
                    selectedIndex = this.grid._selectedDsIndex;

                if (!util.isEmpty(evt))
                {
                    this.grid.select(selectedIndex, selectedIndex);
                    this.onSelect(data[selectedIndex], focus);
                }

                switch (direction) {
                    case "right":
                        if (selectedIndex < data.length - 1) {
                            this.grid.select(selectedIndex + 1, selectedIndex + 1);
                            this.onSelect(data[selectedIndex + 1], focus);
                        }

                        return false;

                    case "left":
                        if (selectedIndex > 0) {
                            this.grid.select(selectedIndex - 1, selectedIndex - 1);
                            this.onSelect(data[selectedIndex - 1], focus);
                            return false;
                        }

                        return true;
                }
            },

            select: function() {
                var dataSource = this.grid.getDatasouce(),
                    data = dataSource._data;

                if (data) {
                    this.grid.select(0, 0);
                    this.onSelect(data[0], false);
                }
            },

            createGrid: function(opts) {
                this.grid = new AnimatedGrid({
                    focusable: opts.focusable,
                    id: "tab-list",
                    rows: opts.rows || 5,
                    cols: opts.cols || 1,
                    /*displayEndBoundary: 1,*/
                    scrollEndBoundary: 1,
                    alignment: AnimatedGrid.VERTICAL
                    //forwardFocus: true
                });

                this.grid.setDisplayStgy(this.displayStrategy);

                this.grid.addEventListener(evtType.KEY, util.bind(function(key) {
                    if (key.id === "device:vkey:left") {
                        return this.changeSelection("left");
                    } else if (key.id === "device:vkey:right") {
                        return this.changeSelection("right");
                    }

                }, this));

                /*this.grid.addEventListener(evtType.SELECTION_CHANGED, util.bind(function(value) {
                    this.onSelect(value.data[value.selectedDsIndex]);
                }, this));*/
                this.attach(this.grid);
            },

            displayStrategy: function(data) {

                var button = new Button({
                    css: "tab-button",
                    id: "tab-button-" + data.id,
                    text: data.object.title
                });

                var highlight = new Container({
                    css: "tab-button-highlight"
                });

                button._indexId = data.id;
                button.attach(highlight);

                return button;
            },

            setTabs: function(tabs) {

                this.tabData = tabs || [];

                var ds = new Ds();

                ds.setDataLoader(util.bind(function(from, size) {
                    var dataArr = [],
                        i = from,
                        len = from + size,
                        deferred = promise.defer();

                    for (; i < len && i < this.tabData.length; i++) {
                        dataArr.push({
                            id: i,
                            object: this.tabData[i]
                        });
                    }

                    deferred.resolve({
                        data: dataArr,
                        total: this.tabData.length
                    });

                    return deferred.promise;
                }, this));

                return this.grid.setDatasource(ds);
            }
        });
    });

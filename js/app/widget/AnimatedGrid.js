define("app/widget/AnimatedGrid", [
        "ax/class",
        "ax/ext/ui/grid/Grid",
        "ax/ext/ui/grid/gridStgy",
        "ax/ext/ui/grid/boundedGridStgy",
        "ax/util",
        "ax/promise",
        "ax/af/Container",
        "ax/af/Component",
        "ax/console",
        "app/util/sSettings"
    ], function(klass, Grid, gridStgy, boundedGridStgy, util, promise, Container, Component, console, sSettings) {

        var transition, transitionend; // TRIAL CSS3

        if ("ontransitionend" in window) {
            transition = "transition";
            transitionend = "transitionend";
        } else if ("onwebkittransitionend" in window) {
            transition = "webkitTransition";
            transitionend = "webkitTransitionEnd";
        } else if ("onotransitionend" in window) {
            transition = "oTransition";
            transitionend = "oTransitionEnd";
        } else {
            // not supporting css3
            transition = false;
            transitionend = false;
        }

        var mod = function(number, divisor) { // modulo operation
            return ((number % divisor) + divisor) % divisor;
        };

        return klass.create(Grid, {}, {

            __EXCESS_CHILDREN_THRESHOLD: 4,

            /**
             * Overrides parent init() function.
             * @method
             * @override
             * @protected
             * @memberof ax/ext/ScrollingGrid#
             */
            init: function(opts) {
                this._super(opts);

                var isVerticalAlign = opts.alignment === Grid.VERTICAL,
                    frontBoundary = opts.scrollFrontBoundary || 0,
                    endBoundary = opts.scrollEndBoundary || 0,
                    displayFrontBoundary = opts.displayFrontBoundary || 0,
                    displayEndBoundary = opts.displayEndBoundary || 0,
                    noAnimation = opts.noAnimation || false;

                this.noAnimation = noAnimation || transition === false;

                // Force animation off?
                if (sSettings.getConfig().animated_lists === false) {
                    this.noAnimation = true;
                }

                this.isVerticalAlign = isVerticalAlign;

                this.transitionStyle = {};

                this.displayFrontBoundary = displayFrontBoundary;
                this.displayEndBoundary = displayEndBoundary;

                this._defaultPosition = 0;
                this._scrolledToEnd = false;

                this.setPrereadyStgy(gridStgy.BASIC_PREREADY_STGY);
                this.setCheckScrollableStgy(gridStgy.BASIC_CHECK_SCROLLABLE_STGY);
                //this.setCheckSelectableStgy(util.bind(boundedGridStgy.SCROLL_BOUNDARY_CHECK_SELECTABLE_STGY, this, frontBoundary, endBoundary));
                this.setCheckSelectableStgy(util.bind(this.CSS_SCROLL_BOUNDARY_CHECK_SELECTABLE_STGY, this, frontBoundary, endBoundary));

                this.setDataMappingStgy(gridStgy.BASIC_DATA_MAPPING_STGY);

                if (this.noAnimation || !transition) {
                    this.setScrollEntranceStgy(util.bind(gridStgy.BASIC_SCROLL_ENTRANCE_STGY, this, isVerticalAlign));

                    this.setScrollShiftStgy(gridStgy.BASIC_SCROLL_SHIFT_STGY);
                    this.setScrollExitStgy(gridStgy.BASIC_SCROLL_EXIT_STGY);
                } else {
                    this.setScrollEntranceStgy(util.bind(this.CSS_SCROLL_ENTRANCE_STGY, this, isVerticalAlign));
                    this.setScrollShiftStgy(this.CSS_SCROLL_SHIFT_STGY);
                    this.setScrollExitStgy(this.CSS_SCROLL_EXIT_STGY);
                }

                this.setOnUpdateStgy(gridStgy.BASIC_ONUPDATE_STGY);


                this.setKeyNavigationStgy(util.bind(boundedGridStgy.SCROLL_BOUNDARY_KEY_NAVIGATION_STGY, this, isVerticalAlign, frontBoundary, endBoundary));

                if (this.isVerticalAlign) {
                    // Safety width to make sure the float don't get pushed to a row below the others.
                    // @todo find a better way to get the same effect
                    // this could also be an issue caused by the floating focus in redbull app.
                    this.getChildren()[0].getRoot().css("width", "200%");
                    this.getRoot().getHTMLElement().style.overflow = "hidden";
                }
            },

            /**
             * Resets the position of the grid to 0px.
             *  It will reset the margin-left style if the grid is "vertical" (alignment: Grid.VERTICAL) aligned otherwise it will reset margin-top.
             */
            resetScrollPosition: function() {

                var targetDom = this.getChildren()[0].getRoot(),
                    style = this.isVerticalAlign ? "marginLeft" : "marginTop";

                targetDom.removeClass("grid-animation");
                targetDom.getHTMLElement().style[style] = "0px";

                this._scrolledToEnd = false;
                this._defaultPosition = 0;
            },

            /**
             * Retrieves the size (width/height) depending if the grid is vertical or not.
             * @param  {component} child xdk component.
             * @return {number}       either width or height depending if the grid is vertical aligned or not.
             */
            getChildSize: function(child) {
                var domElement = child.getRoot().getHTMLElement();

                if (this.isVerticalAlign) {
                    return domElement.getBoundingClientRect().width;
                } else {
                    return domElement.getBoundingClientRect().height;
                }
            },

            /**
            * Figures out the top position compared to it's parent.
            */
            getElementMargin: function(element) {
                var domElement = element.getRoot().getHTMLElement(),
                    parent = element.getRoot().getParent().getHTMLElement(),
                    parentBounding = parent.getBoundingClientRect(),
                    domElementBounding = domElement.getBoundingClientRect();

                if (this.isVerticalAlign) {
                    return (domElementBounding.left - parentBounding.left) || 0;
                } else {
                    return (domElementBounding.top - parentBounding.top) || 0;
                }
            },

            /**
             * Animate the grid container depending on child height/width.
             * @param  {xdk component} child      xdk component, should be a grid child. Used to determine the length of scroll.
             * @param  {number} scrollStep which direction the animation should be done. 1 is for right/down scrolling. 0 is up/left scrolling.
             * @param  {promise} promise    if a promise is defined it will be resolved upon transitionEnd event.
             */
            animate: function(child, scrollStep, promise) {
                var domElement = child.getRoot().getHTMLElement(),
                    container = this.getChildren()[0],
                    gridMargin = 0,
                    scrollPosition;

                if (!this.noAnimation) {
                    container.getRoot().addEventListener(transitionend, function() {

                        container.getRoot().removeClass("grid-animation");
                        container.getRoot().removeEventListener(transitionend);

                        if (promise) {
                            promise.resolve();
                        }

                        return false;
                    });
                }

                var styleProperty;

                if (this.isVerticalAlign) {
                    if (scrollStep <= 0 && !this.noAnimation) {
                        gridMargin = parseInt(container.getRoot().getHTMLElement().style.marginLeft, 10);
                        scrollPosition = -(domElement.getBoundingClientRect().width - this._defaultPosition);
                        container.getRoot().css("marginLeft", (gridMargin !== 0 ? gridMargin : scrollPosition) + "px");
                    }

                    this.transitionStyle = {
                        "marginLeft": ((scrollStep > 0 ? -1 : 0) * domElement.getBoundingClientRect().width)
                    };

                    styleProperty = "marginLeft";

                } else {
                    if (scrollStep <= 0 && !this.noAnimation) {
                        container.getRoot().css("marginTop", -(domElement.getBoundingClientRect().height - this._defaultPosition) + "px");
                    }

                    this.transitionStyle = {
                        "marginTop": ((scrollStep > 0 ? -1 : 0) * domElement.getBoundingClientRect().height)
                    };

                    styleProperty = "marginTop";

                }

                if (scrollStep < 0 && this._defaultPosition < 0) {
                    this.transitionStyle[styleProperty] = this._defaultPosition;
                } else {
                    this.transitionStyle[styleProperty] += this._defaultPosition;
                }

                container.getRoot().addClass("grid-animation");

                for (var style in this.transitionStyle) {
                    if (style) {
                        container.getRoot().css(style, this.transitionStyle[style] + "px");
                    }
                }

                if (this.noAnimation || !transition) {
                    container.getRoot().removeClass("grid-animation");

                    // Ugly hack:
                    // This was needed for samsung 2011 to force reflow/repaint. Otherwise the grid would get stuck in scrolled position instead of the correct position.
                    this.hide();
                    this.show();

                    if (promise) {
                        promise.resolve();
                    }
                }
            },

            // add the new children
            CSS_SCROLL_ENTRANCE_STGY: function(verticalAlign, container, scrollStep, curState, toState) {

                //scrollStep = 2;

                console.log("CSS_SCROLL_ENTRANCE_STGY", arguments);

                var newRowsComponents = toState.gridComponents.slice(toState.newRowsStart, toState.newRowsStart + toState.newRowsCount),
                    newRowsCount = toState.newRowsCount,
                    i, j, rowComponent, rowChildren, placement, marker,
                    children = container.getChildren();

                // Calculate if scrolling up or down

                if (children.length > 0 && scrollStep < 0 && curState.gridComponents && curState.gridComponents[0]) {
                    placement = Component.PLACE_BEFORE;
                    marker = curState.gridComponents[0][0].getParent();

                } else {
                    placement = Component.PLACE_APPEND;
                    marker = null;
                }

                // ADD the new container
                for (i = 0; i < newRowsCount; i++) {
                    rowChildren = [];
                    for (j = 0; j < curState.cols; j++) {
                        if (!newRowsComponents[i] || !newRowsComponents[i][j]) {
                            break;
                        }
                        rowChildren.push(newRowsComponents[i][j]);
                    }
                    rowComponent = new Container({
                        css: verticalAlign ? "wgt-grid-row-v" : "wgt-grid-row-h",
                        children: rowChildren
                    });

                    //just create the row item and add css
                    if (!scrollStep) {
                        rowComponent.getRoot().addClass("row-" + i);
                    }

                    container.attach(rowComponent, placement, marker);
                }

                // calculate step depending on new children length
                // grid.children
                var newMarginOffset = 0,
                    tempOffsetFix,
                    nChildren = container.getChildren().length,
                    maxRows = this._rows,
                    excessChildren;

                if (children.length > 0) {

                    var targetDom = container.getRoot().getHTMLElement(),
                        domElement = container.getChildren()[0].getRoot().getHTMLElement();

                    if (this.isVerticalAlign) {
                        this.transitionStyle = {
                            style: "marginLeft",
                            size: /*((scrollStep > 0 ? -1 : 0) * */domElement.getBoundingClientRect().width//)
                        };
                    } else {
                        this.transitionStyle = {
                            style: "marginTop",
                            size: /*((scrollStep > 0 ? -1 : 0) * */domElement.getBoundingClientRect().height//)
                        };
                    }

                    if( placement === Component.PLACE_BEFORE ){
                        newMarginOffset = (newRowsCount * this.transitionStyle.size) - this.transitionStyle.size;
                        container.getRoot().removeClass("grid-animation");

                        tempOffsetFix = -this.transitionStyle.size;

                        if (this._defaultPosition < 0) {
                            newMarginOffset = this._defaultPosition;
                            tempOffsetFix += this._defaultPosition;
                        }

                        tempOffsetFix += "px";


                        console.log("tempOffsetFix: "+tempOffsetFix);
                        targetDom.style[this.transitionStyle.style] = tempOffsetFix;
                        util.delay(0).then( util.bind(function(){
                            container.getRoot().addClass("grid-animation");

                            targetDom.style[this.transitionStyle.style] = newMarginOffset + "px";
                            console.log("newMarginOffset: "+newMarginOffset);
                        }, this ));
                    }else{
                        nChildren = container.getChildren().length;
                        maxRows = this._rows;
                        excessChildren = nChildren - maxRows;


                        newMarginOffset = ((excessChildren-1)*-this.transitionStyle.size) + (newRowsCount * -this.transitionStyle.size);
                        //util.delay(0).then( function(){
                        container.getRoot().addClass("grid-animation");
                        targetDom.style[this.transitionStyle.style] = (newMarginOffset + this._defaultPosition) + "px";

                        //} );
                    }

                    // check for excess children
                    if (this.removeExcessChildren(container, placement)) {
                        this.onAnimationComplete(container, (placement === Component.PLACE_BEFORE ? 0 : 1));
                    }

                    if (this.noAnimation || !transition) {
                        container.getRoot().removeClass("grid-animation");
                        this.onAnimationComplete(container, (placement === Component.PLACE_BEFORE ? 0 : 1));
                    }
                }
            },

            /**
             * Removes excess children. This is to avoid unwanted grid children to be left in the grid after an animation has been done.
             * This will also be called upon every scroll entrance strategy to avoid too many children being in the DOM during a long scroll.
             * @param  {component} container       Parent of all grid elements.
             * @param  {number} placement       If the children is being placed PREPEND or APPEND.
             * @param  {boolean} removeAllExcess If true it will remove all excess children not just if it exceeds the threshold.
             * @return {boolean}                 Return true if excess children was removed.
             */
            removeExcessChildren: function(container, placement, removeAllExcess) {
                var nChildren = container.getChildren().length,
                    maxRows = this._rows,
                    excessChildren = nChildren - maxRows,
                    children = container.getChildren(),
                    i;

                excessChildren = (excessChildren < 0 ) ? 0 : excessChildren;

                if (children.length > 0 && (excessChildren > this.__EXCESS_CHILDREN_THRESHOLD || removeAllExcess)) {
                    if (placement === Component.PLACE_BEFORE) {

                        for (i = maxRows; i < maxRows+excessChildren && children[i]; i++) {
                            console.log("removing ["+i+"] since "+i+" < "+(nChildren+excessChildren));
                            // throw away child
                            children[i].deinit();
                        }
                    } else {
                        for (i = 0; i < excessChildren && children[i]; i++) {
                            console.log("removing ["+i+"] since "+i+" < "+(excessChildren));
                            // throw away child
                            children[i].deinit();
                        }
                    }
                    //container.getRoot().removeClass("grid-animation");

                    return true;
                }
                return false;
            },

            //add the css and then start css3 animation.
            CSS_SCROLL_SHIFT_STGY: function() {

                var deferred = promise.defer();
                deferred.resolve();
                return deferred.promise;
            },

            //remove the children
            CSS_SCROLL_EXIT_STGY: function(container, scrollStep) {
                var self = this;
                var targetDom = container.getRoot();
                if (!this.noAnimation) {
                    targetDom.removeAllListeners();
                    targetDom.addEventListener(transitionend, function() {
                        self.onAnimationComplete( container, scrollStep );
                    });
                }

                return promise.resolve();
            },

            /**
             * Called when the animation has been complete. It will remove the css animation class ("grid-animation") and reposition the grid to it's default position.
             * @param  {component} container  Parent of all grid elements.
             * @param  {number} scrollStep If the children is being placed PREPEND or APPEND.
             */
            onAnimationComplete: function( container, scrollStep ){
                var targetDom = container.getRoot();
                this.removeExcessChildren(container, (scrollStep > 0 ? Component.PLACE_APPEND : Component.PLACE_BEFORE), true);

                targetDom.removeClass("grid-animation");
                targetDom.getHTMLElement().style[this.transitionStyle.style] = this._defaultPosition + "px";
            },

            CSS_SCROLL_BOUNDARY_CHECK_SELECTABLE_STGY: function(frontBoundary, endBoundary, curState, toState) {
                var toDsIndex = toState.selectedDsIndex,
                    toGridRow = toState.selectedGridRow,
                    dataCount = curState.ds.getTotalCount(),
                    toDsRow, lastDsRow;

                console.log("CSS_SCROLL_BOUNDARY_CHECK_SELECTABLE_STGY");

                if (toDsIndex < 0) {
                    return false;
                }

                toDsRow = Math.floor(toDsIndex / curState.cols);
                lastDsRow = Math.floor((dataCount - 1) / curState.cols);

                // inside the forbidden boundary
                if (toGridRow < frontBoundary || toGridRow >= curState.rows - endBoundary) {
                    // need to be reaching ends to allow selection
                    if (toDsRow > frontBoundary && toDsRow < lastDsRow - endBoundary) {
                        // not allowed
                        return false;
                    }
                }

                if (toDsIndex < dataCount) { // witnin range

                    var child,
                        direction = "down";

                    if (curState.selectedDsIndex > toState.selectedDsIndex) {
                        direction = "up";
                    }

                    if (curState.data && curState.data.length >= curState.rows - 1) {
                        // meeessy code-block
                        // This block will scroll the grid if it has displayFrontBoundary/displayEndBoundary defined.
                        if (this.displayEndBoundary > 0 && toDsRow === lastDsRow && curState.gridComponents && this._scrolledToEnd === false && curState.data.length >= curState.rows) {
                            child = curState.gridComponents[0][0].getParent();
                            this._scrolledToEnd = true;
                            if (this.displayFrontBoundary > 0) {
                                this._defaultPosition = -this.getChildSize(child);
                            }

                            this.animate(curState.gridComponents[curState.gridComponents.length - 1][0].getParent(), 1);

                        } else if (this.displayEndBoundary > 0 && this._scrolledToEnd === true && toState.selectedGridRow === this.displayEndBoundary - 1) {
                            child = curState.gridComponents[1][0].getParent();
                            this._defaultPosition = 0;
                            this.animate(child, 0);

                            this._scrolledToEnd = false;
                        } else if (this.displayFrontBoundary > 0 && this._defaultPosition === 0 && toState.selectedGridRow === (this._rows - this.displayFrontBoundary - this.displayEndBoundary) && toState.selectedGridRow > curState.selectedGridRow) {
                            child = curState.gridComponents[1][0].getParent();
                            this._defaultPosition = 0;
                            this.animate(child, 1);
                            this._defaultPosition = -this.getChildSize(child);
                        } else if (this.displayFrontBoundary > 0 && this._scrolledToEnd === true && toState.selectedGridRow === this.displayFrontBoundary && toState.selectedGridRow < curState.selectedGridRow) {
                            child = curState.gridComponents[1][0].getParent();
                            this._defaultPosition = 0;
                            this.animate(child, 1);
                            this._defaultPosition = -this.getChildSize(child);
                            this._scrolledToEnd = false;
                        } else if (this.displayFrontBoundary > 0 && this._defaultPosition !== 0 && toState.selectedGridRow === this.displayFrontBoundary - 1 && curState.selectedGridRow > toState.selectedGridRow) {
                            child = curState.gridComponents[1][0].getParent();
                            this._defaultPosition = 0;
                            this.animate(child, 0);
                            this._scrolledToEnd = false;
                        }
                    }

                    return true;
                }

                if (toDsRow <= lastDsRow) { // we can still go to that row
                    toState.selectedDsIndex = dataCount - 1;
                    toState.selectedGridCol = mod(dataCount - 1, this._cols);

                    if (endBoundary > 0) {

                    }

                    return true;
                }

                return false;
            }
        });
    });

define("app/widget/Loader", [
        "ax/class",
        "ax/af/Container",
        "ax/Element",
        "ax/Env",
        "ax/util",
        "ax/config",

        "app/util/Utils"
    ], function(klass, Container, Element, Env, util, config, appUtils) {

        return klass.create(Container, {}, {
            init: function(opts) {

                this.Env = Env.singleton();

                opts = opts || {};

                opts.root = new Element("div");
                opts.css = opts.css || "loading-overlay";

                this.opts = opts;

                this._super(opts);

                // Loading indicator, should be used as a static background if any.
                this.loadingIndicator = new Container({});
                this.loadingIndicator.addClass("loading-indicator");

                // Rotating part of loading indicator
                this.spinner = new Container({});
                this.spinner.addClass("loading-indicator");
                this.spinner.addClass("spinner");

                if (opts.center) {
                    this.addClass("center-loading");
                }

                this.attach(this.loadingIndicator);
                this.attach(this.spinner);

                // Used on hide to delay the hiding of the loader.
                this._delayAmount = opts.delayAmount >= 0 ? opts.delayAmount : 2.5;

                // Used as a name for the util.delay so we can cancel it out if another hide has been called.
                this._delayName = "loaderDelay" + (new Date()).getTime();

                // Used to determine if we should hide the loader or not.
                this._showCounter = 0;

                // Used for sprite animation
                this._currentLoop = 0;
                this.spriteFrames = config.get("spriteAnimationFrames", 3);

                if (!window.requestAnimFrame) {
                    window.requestAnimFrame = (function() {
                        return function(callback) {
                            return window.setTimeout(callback, 3000);
                        };
                    })();
                }


                this.hide();
            },

            animationConfig: {
                "samsung": {
                    2011: "sprite",
                    2012: "css",
                    2013: "css"
                },
                "workstation": {
                    0: "css"
                }
            },

            dimensions: {
                w: appUtils.scaleDimension(56),
                h: appUtils.scaleDimension(42)
            },

            // Dummy callback
            onHideCallback: function() {},

            getAnimationType: function() {

                // This check was found in bouncedLabel.js and it might be enough.
                if (!("ontransitionend" in window) && !("onwebkittransitionend" in window) && !("onotransitionend" in window)) {
                    return "sprite";
                }

                return "sprite";

                /*var platform = device.platform,
                    firmwareYear = device.id.getFirmwareYear();

                if (!this.animationConfig[platform] || !this.animationConfig[platform][firmwareYear]) {
                    return "sprite";
                }

                return this.animationConfig[platform][firmwareYear];*/
            },

            spriteAnimation: function() {
                // Cancel the animation if _showCounter <= 0
                if (!this.animated) {
                    return;
                }

                if (this._spriteTimeout) {
                    clearTimeout(this._spriteTimeout);
                }

                this._spriteTimeout = setTimeout(util.bind(function(){

                    this.spinner.getRoot().css("backgroundPosition", (this._currentLoop * -this.dimensions.w) + "px" + " 0");

                    this._currentLoop++;

                    if (this._currentLoop > this.spriteFrames) {
                        this._currentLoop = 0;
                    }

                    this.spriteAnimation();
                }, this), 500);
            },

            startAnimation: function() {

                this.animated = true;

                switch (this.getAnimationType()) {
                    case "css":
                        this.spinner.addClass("rotate");
                        break;

                    case "sprite":
                        this.spriteAnimation();
                        break;
                }
            },

            stopAnimation: function() {

                this.animated = false;

                switch (this.getAnimationType()) {
                    case "css":
                        this.spinner.removeClass("rotate");
                        break;
                    case "sprite":
                        if (this._spriteTimeout) {
                            clearTimeout(this._spriteTimeout);
                        }
                        //console.log("sprite animation");
                        break;
                }
            },

            show: function(forceBlock) {
                this._root.show();

                this.startAnimation();

                this._showCounter++;

                if (this.opts.blockKeys || forceBlock) {
                    this.Env.blockKeys();
                }
            },

            hide: function(forceUnblock) {
                this._showCounter--;
                if (this._showCounter <= 0 || forceUnblock) {
                    util.clearDelay(this._delayName);
                    // Delay the hide to avoid strange flashes of the loadingindicator.
                    util.delay(this._delayAmount, this._delayName).then(util.bind(function() {
                        if (this._showCounter <= 0 || forceUnblock) {
                            this._root.hide();
                            this._showCounter = 0;

                            this.stopAnimation();

                            this.onHideCallback();

                            if (this.opts.blockKeys || forceUnblock) {
                                this.Env.unblockKeys();
                            }
                        }
                    }, this));
                }
            }
        });
    });

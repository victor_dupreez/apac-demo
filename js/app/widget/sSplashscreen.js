define("app/widget/sSplashscreen", [
        "ax/class"
    ], function(klass) {
        var Splashscreen = klass.create({}, {
            init: function() {
                this.firstShow = true;
            },

            show: function() {
                if (this.firstShow) {
                    document.getElementById("TVArea").style.visibility = "hidden";
                }
            },

            hide: function() {
                this.firstShow = false;
                document.getElementById("splashscreen").style.display = "none";
                document.getElementById("TVArea").style.visibility = "visible";
            }
        });

        return new Splashscreen({});
    });

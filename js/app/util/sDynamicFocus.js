define("app/util/sDynamicFocus", [
    "ax/core",
    "ax/class",
    "ax/af/Component",
    "ax/util",
    "ax/af/mvc/AppRoot",
    "ax/af/evt/type",
    "ax/af/evt/DualEventDispatcher",
    "ax/device",
    "ps/util/cssUtil"
], function(core, klass, Component, util, AppRoot, evtType, EventDispatcher, device, cssUtil) {

        var DynamicFocus = klass.create(EventDispatcher, {}, {

            _focus: null,
            _disabled: false,
            _elementName: "dynamicFocusRectangle",
            _trackingTimeout: 2000,
            _trackingInterval: 50,
            _trackingScrollDelay: 250,
            _trackingTries: 10,
            _isFocusActive: 0,
            _lowUpdate: false,
            _delay: 0.0,
            _getOffsetLimits: false,

            init: function() {
                this._super();
                var self = this;

                self.AppRoot = AppRoot.singleton();

                cssUtil.addCss("#" + this._elementName + " {\
                        position: absolute;\
                        z-index: 9999999;\
                    }").then(function(node) {
                        self._cssNode = node;
                    });

                cssUtil.addCss("." + this._elementName + "-anim {\
                        transition: all 0.35s;\
                        -moz-transition: all 0.35s;\
                        -webkit-transition: all 0.35s;\
                    }").then(function(node) {
                        self._cssAnimNode = node;
                    });
            },

            deinit: function() {
                this.disable();
                if (this._focus) {
                    delete this._focus;
                }
                if (this._cssNode) {
                    cssUtil.removeCssNode(this._cssNode);
                }
                if (this._cssAnimNode) {
                    cssUtil.removeCssNode(this._cssAnimNode);
                }
                this.AppRoot.getView().removeEventListener(evtType.BLUR, util.bind(this._blurHandler, this));
                this.AppRoot.getView().removeEventListener(evtType.FOCUS, util.bind(this._focusHandler, this));
                if (this._scrollable) {
                    this._scrollable.removeEventListener(evtType.SCROLLED_PERCENTAGE, util.bind(this._scrollHandler, this));
                }
            },

            activate: function(opts) {
                if (this._focus) {
                    return;
                }

                this.setOptions(opts);

                this.AppRoot.getView().addEventListener(evtType.BLUR, util.bind(this._blurHandler, this));
                this.AppRoot.getView().addEventListener(evtType.FOCUS, util.bind(this._focusHandler, this));

                this._focus = new Component();
                this._focus.setId("#" + this._elementName);
                this._focus.addClass(opts.cssClass);
                this.enable();
            },

            setOptions: function(opts) {
                opts = opts || {};
                if (opts.trackingTimeout > -1) {
                    this._trackingTimeout = opts.trackingTimeout;
                }
                if (opts.trackingInterval > -1) {
                    this._trackingInterval = opts.trackingInterval;
                }
                if (opts.trackingScrollDelay > -1) {
                    this._trackingScrollDelay = opts.trackingScrollDelay;
                }
                if (opts.lowUpdate) {
                    this._lowUpdate = !!opts.lowUpdate;
                }
                if (this._focus) {
                    if (opts.animations) {
                        this._focus.addClass(this._elementName + "-anim");
                    } else {
                        this._focus.removeClass(this._elementName + "-anim");
                    }
                    this._animationsEnabled = opts.animations;
                }
            },

            setAnimationStatus: function(status) {
                this.setOptions({
                    animations: status
                });
            },

            _initFocus: function() {
                if (!this._focus || !this._focus._root || !this._focus._root._dom) {
                    return;
                }
                if (this._lowUpdate) {
                    this._focus._root._dom.style.top = "0px";
                    this._focus._root._dom.style.left = "0px";
                    this._focus._root._dom.style.width = "0px";
                    this._focus._root._dom.style.height = "0px";
                } else {
                    this._focus._root._dom.style.cssText = "top:0px;left:0px;width:0px;height:0px;";
                }
            },

            enable: function(delaySecs) {
                if (!this._disabled || !this._focus) {
                    return;
                }
                this._disabled = false;
                this._delay = delaySecs > 0 ? delaySecs : 0.0;
                this._initFocus();
                if (delaySecs > 0) {
                    this.hide();
                    this.__hasDelay = true;
                    this.__hideTimer = setTimeout(util.bind(function() {
                        this._focus.getRoot().appendTo(core.root.document.body);
                        this.show();
                        this.__hasDelay = false;
                    }, this), delaySecs * 1000 + 100);
                }
                else {
                    this._focus.getRoot().appendTo(core.root.document.body);
                }
            },

            disable: function() {
                if (this._disabled) {
                    return;
                }
                clearInterval(this.__hideTimer);
                this._disabled = true;
                this._stopTimers();
                var elt = core.root.document.getElementById(this._elementName);
                if (elt) {
                    core.root.document.body.removeChild(elt);
                }
            },

            isEnabled: function() {
                return !this._disabled;
            },

            hide: function() {
                if (this._focus) {
                    this._focus.hide();
                }
            },

            show: function() {
                if (this._focus && !this.__forcedHidden) {
                    this._focus.show();
                }
            },

            forceHide: function(duration) {
                this.__forcedHidden = true;

                if (this._focus) {
                    this._focus.hide();
                }

                if (duration > 0) {
                    clearInterval(this._fht);
                    this._fht = setTimeout(util.bind(function() {
                        this.forceShow();
                    }, this), duration * 1000);
                }
            },

            forceShow: function() {
                this.__forcedHidden = false;

                if (this._focus) {
                    this._focus.show();
                }
            },

            isHidden: function() {
                if (this._focus) {
                    return this._focus.isHidden();
                }
            },

            setTrackingScrollDelay: function(delay) {
                this._trackingScrollDelay = delay;
            },

            setTrackingDelay: function(secs) {
                this._delay = secs;
            },

            focusComponent: function(component) {
                this.startTrackFocusEvent();
                this._focusHandler({
                    target: component
                });
            },

            stopTrackFocusEvent: function() {
                this._doNotFocus = true;
                this._stopTimers();
            },

            startTrackFocusEvent: function() {
                this._doNotFocus = false;
            },

            setOffsetLimits: function(limits) {
                this._getOffsetLimits = limits;
            },

            exclude: function(components, include) {
                // exclude components from the dynamic focus
                if (!components && !components.length) {
                    return;
                }
                var i = components.length - 1,
                    cur;
                for (; i>-1; i--) {
                    cur = components[i];
                    if (!cur) {
                        continue;
                    }
                    if (!include) {
                        cur.__noDynamicFocus = true;
                    } else {
                        delete cur.__noDynamicFocus;
                    }
                }
            },

            allow: function(components) {
                this.exclude(components, true);
            },

            listenScrollable: function(scrollable) {
                if (!scrollable || this._scrollable === scrollable) {
                    return;
                }
                this.stopListenScrollable();
                this._scrollable = scrollable;
                this._scrollable.addEventListener(evtType.SCROLLED_PERCENTAGE, util.bind(this._scrollHandler, this));
            },

            stopListenScrollable: function() {
                if (!this._scrollable) {
                    return;
                }
                this._scrollable.removeEventListener(evtType.SCROLLED_PERCENTAGE, util.bind(this._scrollHandler, this));
                this._scrollable = null;
            },

            _scrollHandler: function() {
                this._delay = this._trackingScrollDelay / 1000;
            },

            _getOffsets: function(domElement) {
                var top = domElement.offsetTop,
                    left = domElement.offsetLeft,
                    offsetParent = domElement.offsetParent;
                while (offsetParent) {
                    top += offsetParent.offsetTop;
                    left += offsetParent.offsetLeft;
                    offsetParent = offsetParent.offsetParent;
                }
                return {
                    top: top,
                    left: left
                };
            },

            _updateComponent: function(target, src) {

                var resolution =  device.system.getDisplayResolution();

                if (!src || !src._root) {
                    return false;
                }
                if (!src._root._dom.clientWidth && !src._root._dom.clientHeight) {
                    target.hide();
                    return false;
                }
                if (target.isHidden() && !this.__hasDelay && !this.__forcedHidden) {
                    target.show();
                }
                var updated = 0,
                    style = target.isHidden() ? "display:none;" : "",
                    tgtDom = target._root._dom,
                    srcDom = src._root._dom,
                    offsets = this._getOffsets(srcDom),
                    stlObj = {};

                if (tgtDom.offsetTop !== offsets.top && (this._getOffsetLimits === false || (offsets.top >= this._getOffsetLimits.top))) {
                    if ((offsets.top + srcDom.clientHeighFt) >= resolution.height) {
                        offsets.top = resolution.height - srcDom.clientHeight;
                    }
                    if (offsets.top <= 0) {
                        offsets.top = 0;
                    }
                    stlObj.top = offsets.top;
                    updated++;
                } else {
                    stlObj.top = this._lowUpdate ? null : tgtDom.offsetTop;
                }

                if (tgtDom.offsetLeft !== offsets.left && (this._getOffsetLimits === false || (offsets.left >= this._getOffsetLimits.left))) {
                    if ((offsets.left + srcDom.clientWidth) >= resolution.width) {
                        offsets.left = resolution.width - srcDom.clientWidth;
                    }
                    stlObj.left = offsets.left;
                    updated++;
                } else {
                    stlObj.left = this._lowUpdate ? null : tgtDom.offsetLeft;
                }

                if (tgtDom.clientWidth !== srcDom.clientWidth) {
                    stlObj.width = srcDom.clientWidth;
                    updated++;
                } else {
                    stlObj.width = this._lowUpdate ? null : tgtDom.clientWidth;
                }

                if (tgtDom.clientHeight !== srcDom.clientHeight) {
                    stlObj.height = srcDom.clientHeight;
                    updated++;
                } else {
                    stlObj.height = this._lowUpdate ? null : tgtDom.clientHeight;
                }

                if (updated) {
                    if (this._lowUpdate) {
                        if (stlObj.top !== null) { tgtDom.style.top = stlObj.top + "px"; }
                        if (stlObj.left !== null) { tgtDom.style.left = stlObj.left + "px"; }
                        if (stlObj.width !== null) { tgtDom.style.width = stlObj.width + "px"; }
                        if (stlObj.height !== null) { tgtDom.style.height = stlObj.height + "px"; }
                    } else {
                        tgtDom.style.cssText = style+"top:"+stlObj.top+"px;left:"+stlObj.left+"px;width:"+stlObj.width+"px;height:"+stlObj.height+"px;";
                    }
                }
                return updated;
            },

            _blurHandler: function() {
                if (this._disabled) {
                    return;
                }
                this._stopTimers();
            },

            _focusHandler: function(evt) {
                if (this._disabled || this._doNotFocus) {
                    return;
                }

                this._stopTimers();

                util.clearDelay("focusDelay");
                util.delay(this._delay, "focusDelay").then(util.bind(function() {
                    // the delay is only used once
                    this._delay = 0.0;

                    if (evt.target && this._focus) {
                        var component = evt.target;

                        // we will ignore the components having this special class
                        if (component.__noDynamicFocus) {
                            if (!this._ignoredComp) {
                                // save state
                                this._forcedHiddenState = this.__forcedHidden;
                                this._ignoredComp = true;
                            }
                            // hide the focus and return
                            this.forceHide();
                            return;
                        } else {
                            // if the previously focus comp was an ignored one
                            if (this._ignoredComp) {
                                // disable temporaly animations
                                var animState = this._animationsEnabled;
                                this.setAnimationStatus(false);
                                // we update the focus position while hidden
                                this._updateComponent(this._focus, component);
                                // put back the anim state to previous one
                                this.setAnimationStatus(animState);
                                // and then remove the boolean so the focus will be shown next time
                                if (!this._forcedHiddenState) {
                                    this.forceShow();
                                }
                                this._ignoredComp = false;
                            }
                        }

                        // back to normal behavior here
                        // first we update the component props to the ones of the focused one
                        this._updateComponent(this._focus, component);
                        // then we will check for a while if the focused component moved (anims or component loading)
                        this._trackComponent(component, this._focus);
                    }
                }, this));
            },

            _trackComponent: function(trackedComp, targetComp) {
                var self = this,
                    trackCount = 0;
                // track component to check for any change in position/size
                clearInterval(this._timer);
                this._timer = setInterval(function() {
                    var updated = self._updateComponent(targetComp, trackedComp);
                    if (updated === 0) {
                        // if nothing got updated, we will still continue tracking for a specified nb of tries
                        if (trackCount++ > self._trackingTries) {
                            self._stopTimers();
                        }
                    }
                }, this._trackingInterval);

                // this timer will stop the tracking after a while
                this._timeOut = setTimeout(function() {
                    clearInterval(self._timer);
                }, this._trackingTimeout);
            },

            _stopTimers: function() {
                this._focus._root._dom.style.webkitAnimationPlayState = "paused";
                clearInterval(this._timer);
                clearInterval(this._timeOut);
            }

        });

        return new DynamicFocus();

    });

define("app/util/RedbullGridStgy", [
        "ax/device/vKey"
    ], function (vKey) {

        var strategies = {};

        // ======== key navigation strategies ========
        strategies.FEATURED_GRID_KEY_NAVIGATION_STGY = function (key, curState) {
            switch (key) {

            case vKey.UP.id:
                if (curState.selectedDsIndex === 5) {
                    return {
                        rowChange: -3
                    };
                } else if (curState.selectedDsIndex === 3 || curState.selectedDsIndex === 4) {
                    return {
                        rowChange: -curState.selectedDsIndex
                    };
                }
                return {
                    rowChange: -1
                };
            case vKey.DOWN.id:
                if (curState.selectedDsIndex === 2) {
                    return {
                        rowChange: 3
                    };
                } else if (curState.selectedDsIndex === 0) {
                    return {
                        rowChange: 3
                    };
                } else if (curState.selectedDsIndex === 3 || curState.selectedDsIndex === 4) {
                    return {
                        colChange: 1
                    };
                }
                return {
                    rowChange: 1
                };
            case vKey.LEFT.id:
                if (curState.selectedDsIndex === 2) {
                    return {
                        rowChange: -2
                    };
                } else if (curState.selectedDsIndex === 3) {
                    return {
                        colChange: -1
                    };
                }
                return {
                    rowChange: -1
                };
            case vKey.RIGHT.id:
                return {
                    rowChange: 1
                };
            default:
                return;

            }
        };

        return strategies;
    });

define("app/util/PreloadControllerManager", [
        "ax/class",
        "ax/util",
        "ax/af/Container",
        "ax/af/mvc/ControllerManager",
        "ax/promise",
        "ax/console",
        "ax/device/vKey",
        "ax/af/evt/type"
    ],
    function(klass, util, Container, CtrlMgr, promise, console, vKey, evtType) {

        var sCtrlMgr = CtrlMgr.singleton();

        return klass.create({

            HISTORY_ADD: 0,
            HISTORY_REPLACE: 1,
            HISTORY_SKIP: 2

        }, {

            _parentController: null,
            _view: null,
            _map: {},
            _orderArray: [],
            _history: [],
            _curControllerId: null,
            _containerId: null,
            _preloadWindowSize: 0,
            _checkLevelWhenUnload: false,
            _doNotUnloadOrderedCtrlers: false,

            init: function(parentController, containerId, opts) {
                this._parentController = parentController;
                this._view = parentController.getView();
                this._containerId = containerId;
                opts = opts || {};
                // create the container that will hold the preloaded items
                var c = new Container();
                c.setId("#preloadContainer");
                c.hide();
                this._view.attach(c);

                if (opts.preloadWindowSize > 0) {
                    this._preloadWindowSize = opts.preloadWindowSize;
                }
                this._checkLevelWhenUnload = !!opts.checkLevelWhenUnload;
                this._doNotUnloadOrderedCtrlers = !!opts.doNotUnloadOrderedCtrlers;

                // register back key, used for history management
                parentController.getView().addEventListener(evtType.KEY, util.bind(function(evt) {

                    var aCtrl = this.getController("player-ctrler");
                    var overlay = false;
                    var isPlayerOverlay = false;
                    if(aCtrl && !aCtrl.liveSoonOrOver) {
                        overlay = aCtrl.getOverlay();
                        if(overlay){
                            isPlayerOverlay = (aCtrl && overlay && !overlay.isDisabled());
                        }
                    }
                    if (!isPlayerOverlay && evt.id === vKey.BACK.id) {
                        this.back(true);
                    }
                }, this));

            },

            deinit: function() {
                var keys = Object.keys(this._map);
                // unload all controllers
                for (var k in keys) {
                    if (keys.hasOwnProperty(k)) {
                        this.unloadController(keys[k]);
                    }
                }
                this._view.detach(this._view.find("preloadContainer"));
            },

            setPreloadWindowSize: function(size) {
                if (size >= 0) {
                    this._preloadWindowSize = size;
                }
            },

            mapController: function(controller, controllerId, opts) {
                opts = opts || {};
                // check if controller with this id already entered
                var found = this._map[controllerId];
                if (found) {
                    console.log("controller '" + controllerId + "' already mapped!");
                    return;
                }
                // add an entry into the map
                this._map[controllerId] = {
                    position: -1,
                    controller: controller,
                    params: opts.params,
                    retain: opts.retain,
                    level: opts.level,
                    locked: opts.locked,
                    deleteOnUnload: opts.deleteOnUnload,
                    tol: opts.timeOfLife
                };
                // add the controller in the order map
                if (!opts.skipOrder) {
                    this._orderArray.push(controllerId);
                }
                // if wanted, load the controller
                if (opts.doLoad) {
                    this.preloadController(controllerId);
                }
            },

            preloadController: function(controllerId) {
                var prel = this._view.find("preloadContainer"),
                    found = this._map[controllerId];

                // controller not found
                if (!found) {
                    console.log("no controller with id '" + controllerId + "' found to preload");
                    return;
                }
                // controller already loaded
                if (found.position !== -1) {
                    console.log("controller '" + controllerId + "' already preloaded, nothing to do");
                    return found.position;
                }
                // controller not loaded, lets do it
                else {
                    console.log("controller '" + controllerId + "' not loaded, dynamically preloading it");
                    var c = new Container();
                    c.setId("preload-" + controllerId);
                    prel.attach(c);

                    // show the preloaded container before opening the progressbar
                    if(controllerId === "player-ctrler") {
                        prel.show();
                    }

                    sCtrlMgr.open(found.controller, c, {
                        context: {
                            ctrlerId: controllerId,
                            mainCtrl: this._parentController,
                            preload: true,
                            params: found.params
                        },
                        historySkip: true
                    });

                    prel.hide();

                    found.position = prel.getChildren().length - 1;

                    // set expiration time if specified
                    if (found.tol > 0) {
                        found.expirationTime = new Date().getTime() + (found.tol * 60 * 1000);
                    }

                    return found.position;
                }
            },

            _preloadWindow: function(controllerId) {
                if (!this._preloadWindowSize && this._doNotUnloadOrderedCtrlers) {
                    return;
                }

                var i = 0,
                    pos = -1,
                    len = this._orderArray.length,
                    imax, cur,
                    obj = {};

                // first find the position of the controller in the order array
                for (; i < len; i++) {
                    if (this._orderArray[i] === controllerId) {
                        pos = i;
                        break;
                    }
                }
                // could not find specified controller
                if (pos === -1) {
                    return;
                }

                obj[pos] = controllerId;

                // we look for the ctrlers to preload
                if (this._preloadWindowSize > 0) {
                    i = pos - this._preloadWindowSize;
                    imax = pos + this._preloadWindowSize;
                    for (; i<=imax; i++) {
                        if (i < 0) {
                            cur = len + i;
                        } else if (i >= len) {
                            cur = len - i;
                        } else {
                            cur = i;
                        }
                        obj[cur] = this._orderArray[cur];
                    }
                }

                // now we preload the specified one, and unload the rest if option set
                for (i=0; i<len; i++) {
                    cur = obj[i];
                    if (cur) {
                        this.preloadController(cur);
                    } else {
                        if (!this._doNotUnloadOrderedCtrlers) {
                            this.unloadController(this._orderArray[i]);
                        }
                    }
                }
            },

            displayController: function(controllerId, setFocus, historyOpt, params) {
                var deferred = promise.defer();

                // controller already active, nothing to do
                if (this._curControllerId === controllerId) {
                    return deferred.promise;
                }

                var found = this._map[controllerId];
                // nothing to load
                if (!found) {
                    console.log("no controller with id '" + controllerId + " found to display");
                    return deferred.promise;
                }

                if (this._checkLevelWhenUnload) {
                    this._unloadHigherControllers(controllerId);
                }

                var pos = found.position;

                // controller loaded and has expiration time
                if (pos > -1 && found.expirationTime > 0) {
                    // we check if time is still valid, otherwise we unload it
                    if (new Date().getTime() > found.expirationTime) {
                        console.log("controller '" + controllerId + "' expired, unloading it");
                        found.refresh = true;
                        if (this.unloadController(controllerId)) {
                            pos = -1;
                        }
                    }
                }

                // if controller already loaded
                if (pos > -1) {
                    var unloadCtrler = false;
                    // if ctrler has expiration date and has expired
                    if (found.expirationTime > 0 && new Date().getTime() > found.expirationTime) {
                        console.log("controller '" + controllerId + "' expired, unloading it");
                        unloadCtrler = true;
                    }
                    // if ctrler has to be refresh
                    if (found.refresh) {
                        unloadCtrler = true;
                    }
                    // unload ctrler
                    if (unloadCtrler) {
                        found.refresh = true;
                        if (this.unloadController(controllerId)) {
                            pos = -1;
                        }
                    }

                }

                // controller not loaded yet, lets do it
                if (pos === -1) {
                    pos = this.preloadController(controllerId);
                }

                // if the controller is correctly loaded
                if (pos > -1) {
                    var cont = this._view.find(this._containerId),
                        prel = this._view.find("preloadContainer"),
                        newCtainer = null,
                        curSubController;

                    newCtainer = prel.getChildren()[pos].getChildren()[0];
                    if (newCtainer) {
                        // if no container was already attach, attach the new one
                        if (cont.getChildren().length === 0) {
                            cont.attach(newCtainer);
                        }
                        // othewise switch the old one with the new one
                        else {
                            curSubController = cont.getChildren()[0].getAttachedController();
                            curSubController.hide();
                            cont.replace(cont.getChildren()[0], newCtainer);
                        }
                        this._curControllerId = controllerId;
                        curSubController = cont.getChildren()[0].getAttachedController();
                        curSubController.show(params);
                        if (setFocus) {
                            curSubController.requestFocus();
                        }

                        // remove flag refresh if the ctrler has been refreshed
                        if (found.refresh) {
                            delete found.refresh;
                        }

                        // then manage history if specified
                        if (!historyOpt || historyOpt === this.constructor.HISTORY_ADD) {
                            if (this._history[this._history.length] !== controllerId) {
                                this._history.push(controllerId);
                            }
                        } else if (historyOpt === this.constructor.HISTORY_REPLACE) {
                            if (this._history.length === 0) {
                                this._history.push(controllerId);
                            } else {
                                this._history[this._history.length - 1] = controllerId;
                            }
                        }

                        // preload x controller around selected controller
                        // and unload the rest
                        this._preloadWindow(controllerId);

                        deferred.resolve();
                    }
                } else {
                    console.log("count not preload controller '" + controllerId + "'");
                }
                return deferred.promise;
            },
            getController: function(controllerId) {
                var prel = this._view.find("preloadContainer"),
                    pos = -1,
                    found = this._map[controllerId];

                if (!found) {
                    console.log("no controller with id '" + controllerId + "' found");
                    return;
                }

                if (found.position === -1) {
                    console.log("controller '" + controllerId + "' not loaded, nothing to return");
                    return;
                }

                pos = found.position;
                var ctainer = prel.getChildren()[pos],
                    ctrl = ctainer.getChildren()[0].getAttachedController();

                return ctrl;
            },
            back: function(unloadCtrl) {
                if (this._history.length < 2) {
                    return;
                }
                var lastCtrl = this._history.pop(),
                    iof = this._history.indexOf(lastCtrl);
                if (unloadCtrl) {
                    this.unloadController(lastCtrl, iof === -1);
                }
                var ctrlId = this._history[this._history.length - 1];
                if (ctrlId) {
                    this.displayController(ctrlId, true, this.constructor.HISTORY_SKIP);
                }
            },

            unloadController: function(controllerId, deleteEntry) {
                var prel = this._view.find("preloadContainer"),
                    pos = -1,
                    found = this._map[controllerId];

                if (!found) {
                    console.log("no controller with id '" + controllerId + "' to unload");
                    return;
                }

                if (found.position === -1) {
                    console.log("controller '" + controllerId + "' not loaded, nothing to unload");
                    return true;
                }

                if (found.retain) {
                    return;
                }

                if (this._checkLevelWhenUnload) {
                    this._unloadHigherControllers(controllerId);
                }

                pos = found.position;
                var ctainer = prel.getChildren()[pos],
                    ctrl = ctainer.getChildren()[0].getAttachedController();
                if (ctrl) {
                    prel.detach(ctainer);
                    ctrl.reset();
                    ctrl.deinit();

                    // the controller we're deleting is the one being displayed
                    // so we need to also detach it from the page container
                    if (this._curControllerId === controllerId) {
                        var cont = this._view.find(this._containerId);
                        if (cont && cont.getChildren().length > 0) {
                            cont.detachAll();
                        }
                        this._curControllerId = -1;
                    }

                    // reset params
                    found.position = -1;
                    var keys = Object.keys(this._map), cur;
                    // update pos of the preloaded controllers
                    for (var k in keys) {
                        if (keys.hasOwnProperty(k)) {
                            cur = this._map[keys[k]];
                            if (cur.position > pos) {
                                cur.position--;
                            }
                        }
                    }

                    // if we are not refreshing the controller
                    if (found.refresh !== true) {
                        // if it was on the top of the history, remove it
                        var len = this._history.length;
                        if (len > 0 && this._history[len - 1] === controllerId) {
                            this._history.pop();
                        }
                        // delete its entry in the table if needed
                        if (deleteEntry || found.deleteOnUnload) {
                            this._deleteControllerEntry(controllerId);
                        }
                    }

                    console.log("controller '" + controllerId + "' unloaded");
                    return true;
                }
            },

            _unloadHigherControllers: function(controllerId) {
                var found = this._map[controllerId];
                if (!found || found.level < 0 || this._lockUnload) {
                    return;
                }

                this._lockUnload = true;

                // we go through all controllers and unload those with a higher level number
                var keys = Object.keys(this._map),
                    ctrler = null,
                    deleted = {},
                    key,
                    imax = this._history.length - 1;
                for (var k in keys) {
                    if (keys.hasOwnProperty(k)) {
                        key = keys[k];
                        ctrler = this._map[key];
                        if (ctrler.position !== -1 && ctrler.level > found.level) {
                            this.unloadController(key);
                            deleted[key] = true;
                        }
                    }
                }
                // remove deleted controllers from the history
                while (imax > -1) {
                    ctrler = this._history[imax--];
                    if (deleted[ctrler]) {
                        this._history.pop();
                    }
                }

                this._lockUnload = false;
            },

            _deleteControllerEntry: function(controllerId) {
                var found = this._map[controllerId];
                if (!found || found.locked) {
                    return;
                }

                // delete entry
                delete this._map[controllerId];
                // remove entry from orderArray if found
                var i = 0,
                    len = this._orderArray.length,
                    pos = -1;
                for (; i < len; i++) {
                    if (this._orderArray[i] === controllerId) {
                        pos = i;
                        break;
                    }
                }
                if (pos !== -1) {
                    this._orderArray.slice(pos, 1);
                }
            },

            existsController: function(controllerId) {
                return !!this._map[controllerId];
            },

            setRefreshFlag: function(controllerId) {
                var ctrler = this._map[controllerId];
                if (ctrler) {
                    ctrler.refresh = true;
                }
            },

            getHistory: function() {
                return this._history;
            }
        });
    });

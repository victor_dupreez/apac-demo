define("app/util/Utils",
[
    "ax/class",
    "ax/ext/ui/Layout",
    "ax/device",
    "app/util/sSettings",
    "ps/ui/Notification"
],
function(klass, Layout, device, sSettings, Notification) {

    "use strict";

    var Utils = klass.create({
        notification: new Notification({
            type: {
                QUITPROMPT: {
                    actionable: true,
                    sticky: true,
                    id: "quitPrompt"
                },
                LIVENOTSTARTED: {
                    actionable: true,
                    sticky: true,
                    id: "liveNotStarted"
                },
                NETWORKERROR: {
                    actionable: true,
                    sticky: true,
                    id: "networkError"
                },
                GENERAL: {
                    actionable: true,
                    sticky: true,
                    id: "general"
                },
                LIVESTARTED: {
                    actionable: false,
                    sticky: false,
                    id: "liveStarted"
                }
            },
            alignment: Layout.VERTICAL
        }),

        // date formatted as 2010-12-31
        simpleDate: function(date) {
            var m_names = ["January", "February", "March",
                   "April", "May", "June", "July", "August", "September",
                   "October", "November", "December"
               ];
            var strs = date.split("-"), posMonth = parseInt(strs[1], 10) - 1, day = parseInt(strs[2], 10);
            return m_names[posMonth] + " " + day + ", " + strs[0];
        },

        videoDateFormat: function(video) {
            var api = amd.require("app/data/sRedbullAPI");
            // return the following date format depending on the event time
            if (!video.stream) {
                return video.updated_on ? this.prettyDate(video.updated_on) : this.prettyDate(video.published_on);
            }
            switch (video.stream.status) {
                case api.StreamStatus.LIVE:
                case api.StreamStatus.PAUSE:
                case api.StreamStatus.LIVE_WAITING:
                case api.StreamStatus.PRE_EVENT:
                    return this.futureDate(this.isoDate(video.stream.starts_at), this.isoDate(video.stream.ends_at));
                case api.StreamStatus.SOON:
                    return this.countdownDate(this.isoDate(video.stream.starts_at));
                default:
                    return this.prettyDate(this.isoDate(video.stream.ends_at));
            }
        },

        futureDate: function(date, date2) {
            // two dates, return the range time, like "10am - 12pm" if current date is between the two
            if (date2) {
                var now = new Date();
                if (now > date && now < date2) {
                    var f = date.getHours() > 12 ? date.getHours() - 12 + "pm - " : date.getHours() + "am - ";
                    f += date2.getHours() > 12 ? date2.getHours() - 12 + "pm" : date2.getHours() + "am";
                    return f;
                }
            }
            // single date, return something like "26 Apr @ 05:30 pm"
            var m_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
            var end = date.getHours() > 12 ? " pm" : " am";
            var d = date.getDate() + " " +  m_names[date.getMonth()] + " @ " + hours+ ":" + ((date.getMinutes() < 10)?"0":"") + date.getMinutes() + end;
            return d;
        },

        isoDate: function(string) {
            var D = new Date(string);
            if (isNaN(D)){
                var day, tz,
                rx=/^(\d{4}\-\d\d\-\d\d([tT][\d:\.]*)?)([zZ]|([+\-])(\d\d):(\d\d))?$/,
                p= rx.exec(string) || [];
                if (p[1]) {
                    day = p[1].split(/\D/);
                    for (var i= 0, L= day.length; i<L; i++){
                        day[i]= parseInt(day[i], 10) || 0;
                    }
                    day[1]-= 1;
                    day = new Date(Date.UTC.apply(Date, day));
                    if (!day.getDate()) { return NaN; }
                    if (p[5]){
                        tz = (parseInt(p[5], 10)*60);
                        if(p[6]) { tz+= parseInt(p[6], 10); }
                        if(p[4] === "+") { tz*= -1; }
                        if(tz) { day.setUTCMinutes(day.getUTCMinutes()+ tz); }
                    }
                    return day;
                }
                return NaN;
            }
            return D;
        },

        prettyDate: function(date_str, isAlreadyDateDif) {
            var time_formats = [
                [60, "Just now", 1], // 60
                [120, "1 Minute ago", "Live in 1 minute"], // 60*2
                [3600, "Minutes", 60], // 60*60, 60
                [7200, "1 Hour ago", "Live in 1 hour"], // 60*60*2
                [86400, "Hours", 3600], // 60*60*24, 60*60
                [172800, "Yesterday", "Live in 1 day"], // 60*60*24*2
                [604800, "Days", 86400], // 60*60*24*7, 60*60*24
                [1209600, "Last week", "Live in 1 week"], // 60*60*24*7*4*2
                [2419200, "Weeks", 604800], // 60*60*24*7*4, 60*60*24*7
                [4838400, "Last month", "Live in 1 month"], // 60*60*24*7*4*2
                [29030400, "Months", 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
                [58060800, "Last year", "Next year"], // 60*60*24*7*4*12*2
                [2903040000, "Years", 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
                [5806080000, "Last century", "Next century"], // 60*60*24*7*4*12*100*2
                [58060800000, "Centuries", 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
            ];

            var time = ("" + date_str).replace(/-/g, "/").replace(/[TZ]/g, " ");
            var seconds = isAlreadyDateDif ? -(date_str / 1000) : (new Date() - this.isoDate(date_str)) / 1000;
            var token = "ago",
                list_choice = 1,
                isInFuture = false;
            if (seconds < 0) {
                seconds = Math.abs(seconds);
                token = "Live in";
                list_choice = 2;
                isInFuture = true;
            }
            var i = 0,
                format = time_formats[i];
            while (format) {
                if (seconds < format[0]) {
                    if (typeof format[2] === "string") {
                        return this._replaceSpaces(format[list_choice]);
                    } else {
                        if (isInFuture) {
                            return this._replaceSpaces(token + " " + Math.floor(seconds / format[2]) + " " + format[1]);
                        } else {
                            return this._replaceSpaces(Math.floor(seconds / format[2]) + " " + format[1] + " " + token);
                        }

                    }
                }
                format = time_formats[++i];
            }
            return this._replaceSpaces(time);
        },

        _replaceSpaces: function(str) {
            return str;
            //return str.replace(/ /g, "\u00a0");
        },

        /**
         * Will capitalize the first letter.
         * @param  {string} word The word that should have the first letter capitalized
         * @return {string}      Will return a capitalized representation of the word, if no word was defined an empty string will be returned.
         */
        capitalizeWord: function(word) {
            //return word.replace(/^\w/, function(match){ return match.charAt(0).toUpperCase() + match.substr(1); });
            if (word) {
                return word.charAt(0).toUpperCase() + word.substr(1);
            }
            return "";
        },

        countdownValues: function(timeDifMs, skipMonthsAndWeeks) {
            var _second = 1000,
                _minute = _second * 60,
                _hour = _minute * 60,
                _day = _hour * 24,
                _week = _day * 7,
                _month = _week * 4;

            if (skipMonthsAndWeeks) {
                return {
                    days: Math.floor(timeDifMs / _day),
                    hours: Math.floor((timeDifMs % _day) / _hour),
                    minutes: Math.floor((timeDifMs % _hour) / _minute),
                    seconds: Math.floor((timeDifMs % _minute) / _second)
                };
            }

            return {
                months: Math.floor(timeDifMs / _month),
                weeks: Math.floor((timeDifMs % _month) / _week),
                days: Math.floor((timeDifMs % _week) / _day),
                hours: Math.floor((timeDifMs % _day) / _hour),
                minutes: Math.floor((timeDifMs % _hour) / _minute),
                seconds: Math.floor((timeDifMs % _minute) / _second)
            };
        },

        countdownDate: function(whenDate, includeSeconds, isAlreadyDateDif) {
            var distance = isAlreadyDateDif ? whenDate : whenDate - new Date();

            if (distance < 0) {
                return "passed";
            }

            var values = this.countdownValues(distance),
                res = "";

            if (values.months > 0) {
                res += values.months + "m ";
            }
            if (values.weeks > 0) {
                res += values.weeks + "w ";
            }
            if (values.days > 0) {
                res += values.days + "d ";
            }
            if (values.hours > 0) {
                res += values.hours + "h ";
            }
            if (values.minutes > 0) {
                res += values.minutes + "m ";
            }
            if (includeSeconds && values.seconds > 0) {
                res += values.seconds + "s ";
            }
            return res;
        },

        exit: function(toTV)
        {
            var media = require("ax/device/Media").singleton(),
                params = !!toTV ? { toTV: true } : {};

            try
            {
                media.stop();
                media.deinit();
            }
            catch (e)
            {

            }

            setTimeout(function() { device.system.exit(params); }, 200);
        },

        formatTime: function(secs) {
            var hours = parseInt(secs / 3600, 10) % 24;
            var minutes = parseInt(secs / 60, 10) % 60;
            var seconds = secs % 60;
            var result = (hours === 0 ? "" : hours < 10 ? "0" + hours + ":" : hours + ":") + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
            return result;
        },

        scaleDimension: function(dimension) {

            if(window.innerHeight <= 720) {
                return dimension;
            }

            return Math.ceil(dimension * 1.5);
        },

        getImageRootDir: function() {

            var height = window.innerHeight <= 720 ? "720" : "1080";
            return "img/" + height + "p/";
        },

        isConnected: function() {
            return device.system.getNetworkStatus()
                .then(function(status)
                {
                    return status;
                });
        },

        isAnimatedContainers: function() {
            return sSettings.getConfig().animated_containers;
        },

        isAnimatedMenu: function() {
            return sSettings.getConfig().animated_menu;
        },

        selectPreviousObject: function(list, index) {

            return !list[index] ? index - 1 : index;
        }

    },{});

    return Utils;


});

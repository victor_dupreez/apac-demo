define("app/util/sSettings", [
        "ax/class",
        "ax/promise",
        "ax/console",
        "ax/util"
    ], function(klass, promise, console, util) {

        var Settings = klass.create({}, {

            _rawConf: null,
            _options: {},

            init: function() {},
            deinit: function() {},

            loadJsonConfig: function(src) {
                var self = this,
                    deferred = promise.defer(),
                    rawFile = new XMLHttpRequest();
                rawFile.open("GET", src, true);
                rawFile.onreadystatechange = function () {
                    if (rawFile.readyState === 4) {
                        if ((rawFile.status === 200 || rawFile.status === 0) &&
                                (rawFile.responseText && rawFile.responseText !== "")) {
                            self._rawConf = JSON.parse(rawFile.responseText);
                            console.log("Config file '" + src + "' loaded");
                            deferred.resolve(self._rawConf);
                        } else {
                            var err = "Error loading file '" + src + "'";
                            console.log(err);
                            deferred.reject(err);
                        }
                    }
                };
                rawFile.send(null);
                return deferred.promise;
            },

            loadConfig: function(config) {
                if (!this._rawConf) {
                    console.log("No config file loaded! Default config loaded instead");
                    this._createDefaultConfig();
                }
                this._options = {};
                // first we load the common part
                util.extend(this._options, this._rawConf.common, true);

                // and the specified config
                if (this._rawConf.configs[config]) {
                    util.extend(this._options, this._rawConf.configs[config], true);
                    console.log("Config '" + config + "' loaded: ", this._options);
                    this._configName = config;
                    return true;
                } else {
                    console.log("No config '" + config + "' found");
                }

            },

            getConfigName: function() {
                return this._configName;
            },

            getConfig: function() {
                return this._options;
            },

            existsConfig: function(config) {
                return !!this._rawConf.configs[config];
            },

            // this is just a copy of the config.json file
            _createDefaultConfig: function() {
                this._rawConf = {
                        common: {
                            player_skip_options: {
                                sec: 5,
                                progressive: true,
                                multipleFactor: 1.12,
                                delaySec: 1.5,
                                progressiveCap: 600
                            },
                            menuitems_loading_delay: 0.35,
                            images_optimization: true,
                            events_tracking_opts: {
                                changeTrackingPeriod: 10000
                            },
                            animated_containers: false,
                            renditions: ["496", "1800", "2500", "3528", "4500", "6500", "8500"]
                        },
                        configs: {
                            high: {
                                preload_window_size : 1,
                                load_menuitems_on_focus: true,
                                dynamic_focus: {
                                    animations: true
                                },
                                player_skip_options: {
                                    sec: 6
                                },
                                events_tracking_opts: {
                                    countdownsWithSeconds: true
                                },
                                animated_containers: true
                            },
                            medium: {
                                preload_window_size : 1,
                                load_menuitems_on_focus: true,
                                dynamic_focus: {
                                    lowUpdate: true,
                                    animations: false,
                                    trackingTimeout: 1500,
                                    trackingInterval: 50,
                                    trackingScrollDelay: 0
                                },
                                player_skip_options: {
                                    sec: 6
                                }
                            },
                            low: {
                                preload_window_size: 0,
                                load_menuitems_on_focus: false,
                                dynamic_focus: {
                                    lowUpdate: true,
                                    animations: false,
                                    trackingTimeout: 1000,
                                    trackingInterval: 50,
                                    trackingScrollDelay: 0
                                },
                                renditions: ["496", "1800", "2500", "3528", "4500"],
                                events_tracking_opts: {
                                    maxEventsInMemory: 80
                                }
                            }
                        }
                    };
            }

        });

        return new Settings();

    });

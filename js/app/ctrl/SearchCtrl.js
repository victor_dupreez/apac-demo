define("app/ctrl/SearchCtrl", [

        "ax/class",
        "ax/af/mvc/view",
        "ax/af/focusManager",
        "ax/promise",
        "ax/af/data/LocalDatasource",
        "ax/af/evt/type",
        "ax/console",
        "ax/util",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",
        "ax/device",
        "ax/ext/ui/grid/ScrollingGrid",

        "app/ctrl/AbstractCtrl",
        "app/tmpl/SearchTmpl",
        "app/widget/RedBullGridItem",
        "app/data/sRedbullAPI",
        "app/ctrl/PlayerCtrl",
        "app/ctrl/ShowCtrl",
        "app/widget/AnimatedGrid",
        "app/util/Utils",
        "app/data/sTracking",
        "app/widget/Loader"
    ],
    function(klass, view,  focusMgr, promise, Ds, evtType, console, util,
        Label, Layout, device,
        ScrollingGrid, AbstractCtrl, SearchTmpl,RedBullGridItem, redbullAPI,
        PlayerCtrl, ShowCtrl,  AnimatedGrid,
        Utils, sTracking, Loader) {


        return klass.create(AbstractCtrl, {}, {

            gridCount: 0,

            tabData: [{
                title: "Live",
                tabData: {
                    type: "live",
                    path: "channels/live/event_streams"
                }
            }, {
                title: "Episodes",
                tabData: {
                    type: "episodes",
                    path: "videos/episodes"
                }
            }, {
                title: "Shows",
                tabData: {
                    type: "shows",
                    path: "shows"
                }
            },{
                title: "Clips",
                tabData: {
                    type: "clips",
                    path: "videos/clips"
                }
            },{
                title: "Film",
                tabData: {
                    type: "film",
                    path: "videos/films"
                }
            }],

            init: function() {
                console.log("SearchCtrl init()");
                this.setView(view.render(SearchTmpl, {
                    name: this.ctrlName

                }));
                this._super("SearchCtrl");
            },
            setup: function(context) {

                console.log("SearchCtrl setup()", context);
                this._super(context);

                var view = this.getView(),
                    tabGrid = view.find("tab-grid"),
                    scrollableContainer = view.find("search-view-scrollable"),
                    searchViewIcon = view.find("search-view-icon"),
                    searchViewInfo = view.find("search-view-info"),
                    searchNotFound = view.find("search-not-found").hide();


                this.keyboard = view.find("keyboard");
                this.inputTextField = view.find("input-field-text");
                this.input = view.find("input-field-container");
                this.gridContainer = view.find("grid-container");
                this.searchGridContainer = view.find("search-grid-container");
                this.loader = new Loader({
                    delayAmount: 0.1,
                    center: true
                });

                view.attach(this.loader);

                this._searchMade = false;
                this._keyboardClosed = false;
                this._clearSearchResults = true;
                this.tabGrid = tabGrid;
                this.tabGrid.setTabs(this.tabData);
                this.scrollableContainer = scrollableContainer;
                this.searchViewIcon = searchViewIcon;
                this.searchViewInfo = searchViewInfo;
                this.searchNotFound = searchNotFound;

                if(!Utils.isAnimatedContainers())
                {
                    this.scrollableContainer.addClass("disable-animation");
                }

                this.tabGridEventListener();
                this.addInputEventListener();
                this.addKeyboardEventListener();

                this.createGridComponents(this.tabData);

                focusMgr.focus(this.input);

            },

            addInputEventListener: function() {

                this.input.addEventListener(evtType.CLICK, util.bind(function() {
                    this.keyboard.showKeyboard({
                        text: this._searchMade === true ? this.inputTextField.getText() : ""
                    });

                    focusMgr.focus(this.keyboard);
                }, this));

                this.input.addEventListener(evtType.FOCUS, util.bind(function() {
                    this.scrollPage(0);

                    if (this._keyboardClosed === true && this._searchMade === false && device.platform === "samsung" && device.id.getFirmwareYear() > 2011) {
                        this._keyboardClosed = false;
                        focusMgr.focus(this.getMainController()._menuSelItemId);
                        return;
                    }

                    if (this._searchMade === false) {
                        this.input.dispatchEvent(evtType.CLICK);
                    }
                }, this));

                this.input.addEventListener(evtType.KEY, util.bind(function(key) {
                    console.log(key);
                    if (key && key.id === "device:vkey:up") {
                        return false;
                    }
                    return true;
                }, this));
            },

            addKeyboardEventListener: function() {

                this.keyboard.setOnClose(util.bind(function(text) {
                    console.log("TEXT: " + text);
                    this._keyboardClosed = true;
                    if (text) {
                        this.inputTextField.setText(text);
                        // Do search
                        // This should be used to set the dataloader for grids
                        this.doSearch(text);
                        this._searchMade = true;

                        //getSearchSuggestions
                    }
                    focusMgr.focus(this.input);
                }, this));

                this.keyboard.setOnTextChange(util.bind(function(text) {
                    console.log("ontextchange - " + text);

                    if (text) {
                        this.suggestionText = text;

                        redbullAPI.getSearchSuggestions(text).then(util.bind(function(data) {
                            var dataObj = {
                                request : text, // Characters entered in IME
                                items : []
                            };

                            if (data && data.search_suggestions && this.suggestionText === text) {
                                dataObj.items = data.search_suggestions;
                                this.keyboard.showSuggestions(dataObj);
                            } else {
                                this.keyboard.showSuggestions({
                                    request : "",
                                    items : []
                                });
                            }
                        }, this));
                    } else {
                        this.keyboard.showSuggestions({
                            request : "",
                            items : []
                        });
                    }
                }, this));

            },

            tabGridEventListener: function() {
                this.tabGrid.setOnSelectionChanged(util.bind(function(data, focus) {
                    var type = data.object.tabData.type;
                    this.showGrid(type, focus);
                }, this));

                this.tabGrid.addEventListener(evtType.FOCUS, util.bind(function() {
                    this.scrollPage(0);
                }, this));

                this.tabGrid.addEventListener(evtType.CLICK, util.bind(function(key) {
                    return this.tabGrid.changeSelection("click", true, key);
                }, this));
            },

            doSearch: function(searchText) {
                var searchPromises = [],
                    newtab = [];

                this.loader.show();

                util.each(this.tabData, util.bind(function (tab) {
                    var gridType = tab.tabData.type,
                        ds = new Ds();

                    ds.setDataLoader(redbullAPI.getSearchDataLoader(searchText, {
                        sort_dir: "desc",
                        channel: this.gridComponents[gridType].path,
                        limit: 40
                    }));

                    this.gridComponents[gridType].grid.resetScrollPosition();

                    searchPromises.push(
                        this.gridComponents[gridType].grid.setDatasource(ds).then(util.bind(function (tab) {
                            newtab.push(tab);
                        }, {}, tab), function() {}));
                }, this));

                promise.all(searchPromises).then(util.bind(function() {
                    if (newtab.length) {
                        // sorting
                        newtab.sort(util.bind(function (a, b) {
                            return this.tabData.indexOf(a) - this.tabData.indexOf(b);
                        }, this));

                        this.tabGrid.setTabs(newtab).then(util.bind(function() {
                            this.tabGrid.select(0);
                            this.skipTabGrid(newtab);
                        }, this)).done();
                        this.tabGrid.show();
                        this.tabGrid.addClass("search-made");
                        this.searchNotFound.hide();
                    } else {
                        this.tabGrid.hide();
                        this.searchNotFound.show();
                    }
                    this.loader.hide();
                    this.searchViewIcon.hide();
                    this.searchViewInfo.hide();

                }, this), function() {
                    console.error("Search Error");
                }).done();
            },

            skipTabGrid: function(tab) {
                var nextDown = tab.length === 1 ? this.searchGridContainer : this.tabGrid,
                    nextUp = tab.length === 1 ? this.input : this.tabGrid;

                this.input.setOption("nextDown", nextDown);
                this.searchGridContainer.setOption("nextUp", nextUp);
            },

            clearSearch: function() {
                for (var key in this.gridComponents) {
                    if (this.gridComponents.hasOwnProperty(key)) {
                        var grid = this.gridComponents[key].grid;
                        grid.resetScrollPosition();
                        grid.reset();
                    }
                }
                //remove highlight in the tab
                this.tabGrid.removeClass("search-made");
                //set the text back to default value
                this.inputTextField.setText("Search");
                //show search view icon
                this.searchViewIcon.show();
                //show the search view info
                this.searchViewInfo.show();
                //hide the search not found info
                this.searchNotFound.hide();
                //reset the tabs
                this.tabGrid.setTabs(this.tabData);
                //set the searchmade to false to be able to show keyboard widget when the user comes back to search
                this._searchMade = false;
            },

            createGridComponents: function(tabData) {
                var i, len;
                // Object to hold the three different grids
                this.gridComponents = {};

                // Prepare for a grid for each tabdata type.
                for (i = 0, len = tabData.length; i < len; i++) {
                    if (tabData[i] && tabData[i].tabData && tabData[i].tabData.type) {
                        this.createGrid(tabData[i].tabData);
                    }
                }
            },

            /**
             * Show the grid with correct type and hide the others
             */
            showGrid: function(type, focus) {
                var grid;

                for (grid in this.gridComponents) {
                    if (grid === type) {

                        this.gridComponents[type].grid.show();
                        this.tabGrid.setOption("nextDown", this.gridComponents[type].grid);

                        if (focus) {
                            focusMgr.focus(this.gridComponents[type].grid);
                        }

                    } else {
                        this.gridComponents[grid].grid.hide();
                    }
                }
            },

            scrollPage: function(offset) {

                if (offset > 0 || this._currentScrolledPosition === offset) {
                    return;
                }

                this.scrollableContainer.dispatchEvent(evtType.SCROLLED_PERCENTAGE, offset);

                this.scrollableContainer.getRoot().getHTMLElement().style.top = offset + "px";

                this._currentScrolledPosition = offset;
            },

            createGrid: function(data) {
                var rows = 6,
                    cols = 1,
                    css = "search-grid",
                    scrollEndBoundary = 1,
                    scrollFrontBoundary = 1;

                if (data.type === "shows") {
                    rows = 5;
                    css += " search-shows";
                    scrollEndBoundary = 1;
                    scrollFrontBoundary = 1;
                }

                var grid = new AnimatedGrid({
                    id: "search-grid-" + data.type,
                    css: css,
                    nextLeft: "menu",
                    nextRight: null,
                    rows: rows,
                    cols: cols,
                    scrollFrontBoundary: scrollFrontBoundary,
                    scrollEndBoundary: scrollEndBoundary,
                    displayFrontBoundary: 2,
                    displayEndBoundary: 1,
                    alignment: ScrollingGrid.HORIZONTAL
                });

                grid.addEventListener(evtType.KEY, util.bind(function(key) {
                    // Change selection in the tab
                    if (key.id === "device:vkey:left") {
                        return this.tabGrid.changeSelection("left", true);
                    } else if (key.id === "device:vkey:right") {
                        return this.tabGrid.changeSelection("right", true);
                    }
                }, this));

                grid.addEventListener(evtType.FOCUS, util.bind(function() {
                    // Change selection in the tab
                    this.scrollPage(-110);
                }, this));

                grid.setDisplayStgy(util.bind(this.gridDisplayStrategy, this));

                var view = this.getView(),
                    gridContainer = view.find("search-grid-container");

                gridContainer.attach(grid);

                this.gridComponents[data.type] = {
                    grid: grid,
                    path: data.path
                };

                grid.hide();

                this.gridCount++;
            },

            gridDisplayStrategy: function(data) {
                if (data.message) {

                    var lblError = new Label({
                            text: data.message
                        }),
                        lblCell = new Layout({
                            alignment: Layout.VERTICAL,
                            id: "errorCell" + data.id,
                            css: "no-data-cell",
                            clickable: false,
                            focusable: false,
                            children: [lblError]
                        });

                    return lblCell;
                }

                var img = data.object.images,
                    subtitle = data.object.title === data.object.subtitle ? null : data.object.subtitle,
                    opts;

                if (data.object.type === "series" && img && img.portrait) {
                    opts = {
                        width: Utils.scaleDimension(112),
                        height: Utils.scaleDimension(168)
                    };

                    img = redbullAPI.getImageUrl(img.portrait.uri, opts);
                    subtitle = data.object.short_description;
                } else if (img && img.landscape) {
                    opts = {
                        width: Utils.scaleDimension(177),
                        height: Utils.scaleDimension(100),
                        crop: true
                    };

                    img = redbullAPI.getImageUrl(img.landscape.uri, opts);
                }

                var image = new RedBullGridItem({
                    css: "search-item",
                    imageUrl: img,
                    //tag: tag,
                    tagMaxLength: 0,
                    title: data.object.title,
                    titleMaxLength: 0,
                    //description: desc,
                    descriptionMaxLength: 0,
                    subtitle: subtitle,
                    subtitleMaxLength: 0,
                    //time: prettyDate,
                    timeMaxLength: 0
                });

                image.addEventListener(evtType.CLICK, util.bind(function() {

                    var params = {},
                        nextController,
                        dot = data.object.type,
                        dataObjectType = dot === "episode" ||
                        dot === "clip" ||
                        dot === "event_stream" ||
                        dot === "film";

                    if (dataObjectType) {
                        params = {
                            videoObject: data.object,
                            breadCrumbs: data.object.channel_breadcrumbs,
                            videoId: data.object.id,
                            nextCtrlName: ShowCtrl
                        };

                        nextController = PlayerCtrl;
                    } else {

                        params = {
                            breadCrumbs: data.object.channel_breadcrumbs,
                            showTitle: data.object.title,
                            showId: data.object.id,
                            showBackgroundImage: data.object.images.background.uri,
                            showTitleImage: data.object.images &&  data.object.images.logo ?  data.object.images.logo.uri : ""
                        };

                        nextController = ShowCtrl;
                    }

                    this._clearSearchResults = false;

                    this.getMainController().changeCurrentSubCtrl(nextController, false, params);
                }, this));

                return image;
            },

            hide: function() {
                if (this._clearSearchResults) {
                    this.clearSearch();
                }

            },

            show: function() {
                this._clearSearchResults = true;
                sTracking.trackPage(this.__context.params);
            }
        });
    });

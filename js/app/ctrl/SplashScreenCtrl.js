define("app/ctrl/SplashScreenCtrl",
[
    "ax/af/mvc/AppRoot",
    "ax/af/mvc/Controller",
    "ax/af/mvc/view",
    "ax/class",
    "ax/util",
    "ax/device",

    "app/widget/sLoader",
    "app/tmpl/SplashScreenTmpl",
    "app/ctrl/MainCtrl",

    "app/util/sSettings",
    "app/util/Utils",
    "app/data/sRedbullAPI"

], function(AppRoot, Controller, View, klass, util, device,
    sLoader, SplashScreenTmpl, MainCtrl, sSettings, appUtils, sRedbullAPI)
{

    "use strict";

    var sAppRoot = AppRoot.singleton();
    return klass.create(Controller, {}, {
        init: function() {
            this.setView(View.render(SplashScreenTmpl));
            sLoader.show();
            appUtils.firstShow = true;
        },

        setup: function() {
            // loading the config file before anything else can continue
            sSettings.loadJsonConfig("appconfig.json")
                .complete(util.bind(this.next, this));
        },

        // things to do after the config file has been loaded
        next: function() {

            // load app config depending on device
            if (device.id.getFirmwareYear() === 2011) {
                sSettings.loadConfig("low");
            }
            else {
                sSettings.loadConfig("medium");
            }

            // configure images quality
            sRedbullAPI.setImagesOptimization(sSettings.getConfig().images_optimization);

            //added some delay to show splash screen
            util.clearDelay("delayMainCtrl");
            util.delay(1, "delayMainCtrl").then(util.bind(function() {
                sAppRoot.setMainController(MainCtrl);
            }, this));
        },

        reset: function() {
            appUtils.firstShow  = false;
            sLoader.hide();
        }
    });
});

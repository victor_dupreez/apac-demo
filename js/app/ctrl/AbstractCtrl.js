define("app/ctrl/AbstractCtrl", [
        "ax/class",
        "ax/af/mvc/Controller",
        "ax/af/focusManager",
        "ax/promise",
        "ax/console"
    ],
    function(klass, Controller, focusMgr, promise, console) {

        return klass.create(Controller, {}, {

            // reference to the main controller
            _mainController: null,
            // context given when creating the ctrler
            __context: null,
            // list of invalid grids
            _invalidGrids: [],

            init: function(name) {
                this.ctrlName = name;
                this.subCtrlContainerId = this.ctrlName + "-subctrl-container";
                this._super();
            },

            setup: function(context) {
                this.__context = context;
                if (this._mainController === null && context.mainCtrl !== null) {
                    this._mainController = context.mainCtrl;
                }
                if (!context.preload) {
                    var view = this.getView();
                    focusMgr.focus(view);
                }
            },

            getMainController: function() {
                return this._mainController;
            },

            getControllerId: function() {
                if (this.__context) {
                    return this.__context.ctrlerId;
                }
            },

            requestFocus: function() {
                var view = this.getView();
                focusMgr.focus(view);
            },

            // set a grid to an invalid state, it will be reset on the next show() call
            setInvalidGrid: function(gridComponent) {
                if (gridComponent) {
                    if (this._invalidGrids.indexOf(gridComponent) < 0) {
                        this._invalidGrids.push(gridComponent);
                    }
                    gridComponent.hide();
                    gridComponent.setOption("focusable", false);
                }
            },

            // set a datasource to a grid, and if any error occurs, set the grid as invalid
            setGridDatasource: function(grid, ds) {
                var self = this,
                    deferred = promise.defer();
                grid.setDatasource(ds).then(function() {
                    deferred.resolve();
                }, function(error) {
                    self.setInvalidGrid(grid);
                    deferred.reject(error);
                });
                return deferred.promise;
            },

            // method used to reset the grids that have been flagged as invalid
            resetInvalidGrids: function() {
                var self = this,
                    i = this._invalidGrids.length - 1;
                if (i > -1) {
                    console.log("Reseting invalid grids...");
                    for (; i >= 0; i--) {
                        var ds = this._invalidGrids[i].getDatasource();
                        if (ds) {
                            this._invalidGrids[i].reset();
                            this._invalidGrids[i]._ds = null;
                            this._invalidGrids[i].setDatasource(ds).then(function() {
                                console.log("Grid properly reset");
                                self._invalidGrids.pop().show();
                            }, function() {
                                console.log("Error trying to reset the grid");
                            });
                        }
                    }
                }
            },

            // method called when the controller is set visible by the main controller
            show: function() {
                // let's reset all grids flagged as invalid
                this.resetInvalidGrids();
            },

            // method called when the controller is set hidden by the main controller
            hide: function() {},

            reset: function() {}
        });
    });

define("app/ctrl/ChannelCtrl", [
        "ax/class",
        "ax/af/mvc/view",
        "ax/af/data/LocalDatasource",
        "ax/af/evt/type",
        "ax/console",
        "ax/util",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",
        "ax/af/Container",
        "ax/ext/ui/grid/ScrollingGrid",

        "app/ctrl/AbstractCtrl",
        "app/tmpl/ChannelTmpl",
        "app/widget/RedBullGridItem",
        "app/data/sRedbullAPI",
        "app/ctrl/PlayerCtrl",
        "app/ctrl/SubChannelCtrl",
        "app/util/Utils",
        "app/ctrl/ShowCtrl",
        "app/widget/AnimatedGrid",
        "app/util/RedbullGridStgy",
        "app/data/sTracking"
    ],
    function(klass, view, Ds, evtType, console, util,
        Label, Layout, Container, ScrollingGrid, AbstractCtrl,
        ChannelTmpl, RedBullGridItem, redbullAPI, PlayerCtrl, SubChannelCtrl, Utils,
        ShowCtrl, AnimatedGrid, redbullGridStgy, sTracking) {

        return klass.create(AbstractCtrl, {}, {
            init: function() {
                console.log("ChannelCtrl init()");
                this.setView(view.render(ChannelTmpl, {
                    name: this.ctrlName
                }));
                this._super("ChannelCtrl");
            },

            setup: function(context) {
                console.log("ChannelCtrl setup()", context);
                this._super(context);

                var view = this.getView(),
                    self = this,
                    dataLoader,
                    lbChannels = view.find("lblChannels"),
                    featuredItemsDs = new Ds(),
                    featuredItemsGrid = view.find("featuredgrid"),
                    focusableContainer = view.find("focusableContainer"),
                    loopedgrid2 = view.find("loopedgrid2");

                featuredItemsGrid.addClass("featuredGrid");
                featuredItemsGrid.addClass("borderGradientBig");

                // set a specific key navigation strategy for out featured grid
                featuredItemsGrid.setKeyNavigationStgy(redbullGridStgy.FEATURED_GRID_KEY_NAVIGATION_STGY);

                redbullAPI.setDataLoaderLimit(3);
                dataLoader = redbullAPI.getChannelFeaturedDataLoader(context.params.channelId);
                featuredItemsDs.setDataLoader(dataLoader);
                redbullAPI.setDataLoaderLimit(0);

                featuredItemsGrid.setDisplayStgy(function(data) {
                    //if data contains message that means an empty ds has been assigned to the grid.
                    //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                    //error label has been shown. Error text need improvisation.
                    if (data.message) {

                        if (focusableContainer)
                        {
                            focusableContainer.hide();
                        }

                        loopedgrid2.setOption("nextUp", undefined);

                        var lblError = new Label({
                                text: data.message
                            }),
                            lblCell = new Layout({
                                alignment: Layout.VERTICAL,
                                id: "errorCell" + data.id,
                                clickable: false,
                                focusable: false,
                                children: [lblError]
                            });

                        return lblCell;
                    }

                    var img = data.object.images;

                    var imgOpts = {
                            width: Utils.scaleDimension(400),
                            height: Utils.scaleDimension(220),
                            crop: true
                        };

                    if (data.id === 0) {
                        imgOpts.width = Utils.scaleDimension(800);
                        imgOpts.height = Utils.scaleDimension(450);
                    }

                    if (img && img.landscape) {
                        img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                    }

                    var channelBreadCrumbs = "channel_breadcrumbs",
                        publishedOn = "published_on",
                        shortDescription = "short_description",
                        dataObj = data.object,
                        channelBreadCrumbsObj = dataObj[channelBreadCrumbs],
                        tag = channelBreadCrumbsObj.length > 0 ? channelBreadCrumbsObj[channelBreadCrumbsObj.length - 1].toUpperCase() : "",
                        desc = data.id === 0 ? dataObj[shortDescription] : null,
                        prettyDate = Utils.prettyDate(dataObj[publishedOn]);

                    var image = new RedBullGridItem({
                        css: "item-normal",
                        imageUrl: img,
                        tag: tag,
                        tagMaxLength: 0,
                        title: data.object.title,
                        titleMaxLength: 0,
                        description: desc,
                        descriptionMaxLength: 0,
                        subtitle: data.object.subtitle,
                        subtitleMaxLength: 0,
                        time: prettyDate,
                        timeMaxLength: 0
                    });

                    if (data.id === 0) {
                        image.addClass("bigImage");
                    }

                    image.addEventListener(evtType.CLICK, util.bind(function() {
                        console.log("selected: " + data.object.id);
                        var params = {
                            videoObject: data.object,
                            videoId: data.object.id,
                            nextCtrlName: ShowCtrl
                        };
                        this.getMainController().changeCurrentSubCtrl(PlayerCtrl, false, params);
                    }, self));

                    return image;
                });

                self.setGridDatasource(featuredItemsGrid, featuredItemsDs);

                var subChannelLoopedGrid = view.find("loopedgrid2");
                subChannelLoopedGrid.addClass("subChannelGrid");
                subChannelLoopedGrid.addClass("borderGradient");

                //Fetch Subchannel - subcategory data from RedBullAPI.
                redbullAPI.getChannel(context.params.channelId).then(function(channel) {
                    console.log("SubChannel Information From RedBullAPI");

                    //Load the subchannel list grid only if there is any element.
                    if (channel) {
                        var subchannelDs = new Ds(),
                            subChannels = "sub_channels",
                            subChannelObj = channel[subChannels];

                        dataLoader = redbullAPI.getDataLoaderFromArray(subChannelObj);
                        subchannelDs.setDataLoader(dataLoader);

                        // if no subchannel, remove the focus option
                        if (subChannelObj.length === 0) {
                            lbChannels.hide();
                            subChannelLoopedGrid.setOption("focusable", false);
                        }

                        //Dynamically assigning rows and cols to subchannel grid.
                        subChannelLoopedGrid._rows = 8;//channel.sub_channels.length > 7 ? 7 : channel.sub_channels.length;
                        subChannelLoopedGrid._cols = 1;
                        self.setGridDatasource(subChannelLoopedGrid, subchannelDs);

                        var gridLayout = view.find("makeitworkLayout");

                        var subChannelDetailGrid, subChannelLabel;

                        for (var i = 0; i < subChannelObj.length; i++) {
                            subChannelDetailGrid = new AnimatedGrid({
                                rows: 5,
                                forwardFocus: true,
                                autoNavigation: true,
                                alignment: ScrollingGrid.VERTICAL,
                                scrollEndBoundary: 2,
                                scrollFrontBoundary: 2,
                                displayEndBoundary: 1,
                                displayFrontBoundary: 2,
                                cols: 1,
                                clickable: true,
                                id: "#grid" + i
                            });

                            subChannelDetailGrid.addClass("subChannelDetailGrid");
                            subChannelDetailGrid.addClass("borderGradientBig");

                            subChannelLabel = new Label({
                                text: subChannelObj[i].title
                            });

                            subChannelLabel.addClass("gridTitle");

                            gridLayout.attach(subChannelLabel);
                            gridLayout.attach(subChannelDetailGrid, Container.PLACE_APPEND);

                            var subChannelDetailDs = new Ds();
                            var loaderOptions = redbullAPI.createExtendedOpts(redbullAPI.SortBy.PUBLISHED_ON, redbullAPI.SortDir.DESC);

                            loaderOptions.limit = 10;
                            loaderOptions.maxItems = 10;

                            dataLoader = redbullAPI.getChannelVideosDataLoader(subChannelObj[i].id, loaderOptions);
                            subChannelDetailDs.setDataLoader(dataLoader);

                            //set display startegy for SubChannelLoopedGrid.
                            subChannelDetailGrid.setDisplayStgy(function(data) {
                                //if data contains message that means an empty ds has been assigned to the grid.
                                //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                                //error label has been shown. Error text need improvisation.
                                if (data.message) {
                                    var lblError = new Label({
                                            text: data.message
                                        }),

                                        cell = new Layout({
                                            alignment: Layout.VERTICAL,
                                            id: "errorCell" + data.id,
                                            clickable: false,
                                            focusable: false,
                                            children: [lblError]
                                        });

                                    return cell;
                                }

                                if (data.moreButton) {
                                    var moreArrow = new Container({
                                            text: data.message,
                                            css: "more-arrow"
                                        }),
                                        moreButton = new Layout({
                                            alignment: Layout.VERTICAL,
                                            id: "moreButton",
                                            css: "more-button",
                                            clickable: true,
                                            focusable: true,
                                            children:[moreArrow]
                                        });

                                    moreButton.addEventListener(evtType.CLICK, function() {
                                        var params = {
                                            channelId: data.id,
                                            channelName: data.title
                                        };

                                        self.getMainController().changeCurrentSubCtrl(SubChannelCtrl, false, params);
                                    });

                                    return moreButton;
                                }


                                var img = data.object.images,
                                    imgOpts = {
                                        width: Utils.scaleDimension(370),
                                        height: Utils.scaleDimension(210),
                                        crop: true
                                    };

                                if (img && img.landscape) {
                                    img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                                }
                                var publishedOn = "published_on",
                                    prettyDate = Utils.prettyDate(data.object[publishedOn]),

                                    redbullGridItem = new RedBullGridItem({
                                        css: "item-normal",
                                        imageUrl: img,
                                        tagMaxLength: 0,
                                        title: data.object.title,
                                        titleMaxLength: 0,
                                        subtitle: data.object.subtitle,
                                        subtitleMaxLength: 0,
                                        time: prettyDate,
                                        timeMaxLength: 0
                                    });

                                redbullGridItem.addEventListener(evtType.CLICK, util.bind(function() {
                                    console.log("selected: " + data.object.id);
                                    var params = {
                                        videoObject: data.object,
                                        videoId: data.object.id,
                                        nextCtrlName: ShowCtrl
                                    };
                                    this.getMainController().changeCurrentSubCtrl(PlayerCtrl, false, params);
                                }, self));

                                return redbullGridItem;
                            });

                            subChannelDetailGrid.setDatasource(subChannelDetailDs).then(util.bind(function(){
                                var ds = this.grid.getDatasource();
                                //fix the incorrect length return by getFetchedCount
                                var fetchCount = ds.getFetchedCount();
                                var index = ds._data[fetchCount - 1] ? fetchCount : fetchCount - 1;
                                ds.insert({
                                    moreButton: true,
                                    id: subChannelObj[this.i].id,
                                    title: subChannelObj[this.i].title
                                }, index);

                            }, { grid: subChannelDetailGrid, i: i, scope: this}));
                        }
                    }
                });


                //set display startegy for SubChannelLoopedGrid.
                subChannelLoopedGrid.setDisplayStgy(function(data) {
                    //if data contains message that means an empty ds has been assigned to the grid.
                    //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                    //error label has been shown. Error text need improvisation.
                    if (data.message) {
                        var lblError = new Label({
                                text: data.message
                            }),

                            cell = new Layout({
                                alignment: Layout.VERTICAL,
                                id: "errorCell" + data.id,
                                clickable: false,
                                focusable: false,
                                children: [lblError]
                            });

                        return cell;
                    }

                    var img = data.object.images,
                        imgOpts = {
                            width: Utils.scaleDimension(170),
                            height: Utils.scaleDimension(96),
                            crop: true
                        };

                    if (img && img.landscape) {
                        img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                    }

                    var redbullGridItem = new RedBullGridItem({
                            css: "item-normal",
                            imageUrl: img,
                            liveNow: false,
                            tag: "",
                            tagMaxLength: 0,
                            title: data.object.title,
                            titleMaxLength: 0,
                            description: "",
                            descriptionMaxLength: 0,
                            subtitle: "",
                            subtitleMaxLength: 0,
                            info: "",
                            infoMaxLength: 0,
                            time: "",
                            timeMaxLength: 0
                        });

                    redbullGridItem.addEventListener(evtType.CLICK, util.bind(function() {
                        var params = {
                            channelId: data.object.id,
                            channelName: data.object.title
                        };
                        this.getMainController().changeCurrentSubCtrl(SubChannelCtrl, false, params);
                    }, self));

                    return redbullGridItem;
                });

            },

            show: function() {
                sTracking.trackPage(this.__context.params);
            }
        });
    });

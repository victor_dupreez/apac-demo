define("app/ctrl/SubChannelCtrl",
[
    "ax/class",
    "ax/af/mvc/view",
    "ax/af/focusManager",
    "ax/af/data/LocalDatasource",
    "ax/af/evt/type",
    "ax/device/vKey",
    "ax/console",
    "ax/util",
    "ax/ext/ui/Label",
    "ax/ext/ui/Layout",

    "app/ctrl/AbstractCtrl",
    "app/tmpl/SubChannelTmpl",
    "app/widget/RedBullGridItem",
    "app/data/sRedbullAPI",
    "app/ctrl/PlayerCtrl",
    "app/ctrl/ShowCtrl",
    "app/util/Utils",
    "app/util/RedbullGridStgy",
    "app/data/sTracking"
],
function(klass, view, focusMgr, Ds, evtType, vKey, console, util,
    Label, Layout, AbstractCtrl,
    SubChannelTmpl, RedBullGridItem, redbullAPI, PlayerCtrl, ShowCtrl, Utils,
    redbullGridStgy, sTracking)
{


    return klass.create(AbstractCtrl,
    {
        CALL: 4
    },
    {

        callCtr: 0,
        changeController: null,
        gridsContainer: [],
        _opts: {},

        init: function() {
            console.log("SubChannelCtrl init()");
            this.setView(view.render(SubChannelTmpl, {
                name: this.ctrlName
            }));
            this._super("SubChannelCtrl");
        },

        setup: function(context) {
            this._opts = context || {};
            console.log("SubChannelCtrl setup()", this._opts);
            this._super(context);
            var view = this.getView(),
                dataLoader,
                featuredItemsDs = new Ds(),
                lblSubChannelTitle = view.find("lblSubChannelTitle"),
                subChannelFeaturedGrid = view.find("subChannelFeaturedGrid"),
                focusableContainer = view.find("focusableContainer"),
                showsLoopedGrid = view.find("showsLoopedGrid");


            // set the subchannel title
            lblSubChannelTitle.setText(this._opts.params.channelName);
            if (this._opts.params.isMenuItem === true) {
                lblSubChannelTitle.hide();
            }

            // set a specific key navigation strategy for out featured grid
            subChannelFeaturedGrid.setKeyNavigationStgy(redbullGridStgy.FEATURED_GRID_KEY_NAVIGATION_STGY);

            //call the redbullapi to fetch the first six featured videos.
            redbullAPI.setDataLoaderLimit(6);
            dataLoader = redbullAPI.getChannelFeaturedDataLoader(this._opts.params.channelId);
            featuredItemsDs.setDataLoader(dataLoader);
            redbullAPI.setDataLoaderLimit(0);


            //set the display startegy for subChannelFeaturedGrid. Adding Redbullgriditem and images to subChannelFeaturedGrid.
            subChannelFeaturedGrid.setDisplayStgy(util.bind(function(data) {

                //if data contains message that means an empty ds has been assigned to the grid.
                //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                //error label has been shown. Error text need improvisation.
                if (data.message) {

                    if (focusableContainer)
                    {
                        focusableContainer.hide();
                    }

                    showsLoopedGrid.setOption("nextUp", undefined);

                    var lblError = new Label({
                            text: data.message
                        }),
                        cell = new Layout({
                            alignment: Layout.VERTICAL,
                            id: "errorCell" + data.id,
                            clickable: false,
                            focusable: false,
                            children: [lblError]
                        });

                    return cell;
                }

                var img = data.object.images,
                    imgOpts = {
                        width: Utils.scaleDimension(data.id === 0 ? 800 : 400),
                        height: Utils.scaleDimension(data.id === 0 ? 450 : 220)
                    };

                if (data.id !== 0) {
                    imgOpts.crop = true;
                }

                if (img && img.landscape) {
                    img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                }

                var desc = data.id === 0 ? data.object.short_description : null,
                    prettyDate = Utils.prettyDate(data.object.published_on),
                    image = new RedBullGridItem({
                        css: "item-normal",
                        imageUrl: img,
                        liveNow: false,
                        tag: "",
                        tagMaxLength: 0,
                        title: data.object.title,
                        titleMaxLength: 0,
                        description: desc,
                        descriptionMaxLength: 0,
                        subtitle: data.object.subtitle,
                        subtitleMaxLength: 0,
                        time: prettyDate,
                        timeMaxLength: 0
                    });

                if (data.id === 0) {
                    image.addClass("bigImage");
                } else if (data.id === 3 || data.id === 4) {
                    image.addClass("fixCellSize");
                }

                image.addEventListener(evtType.CLICK, util.bind(function() {
                    var params = {
                        videoObject: data.object,
                        breadCrumbs: data.object.channel_breadcrumbs,
                        videoId: data.object.id,
                        nextCtrlName: ShowCtrl
                    };
                    this.getMainController().changeCurrentSubCtrl(PlayerCtrl, false, params);
                }, this));

                return image;

            }, this));

            // Focus needs to be applied after the grid has it's data.
            this.setGridDatasource(subChannelFeaturedGrid, featuredItemsDs).then(util.bind(function ()
            {
                if (subChannelFeaturedGrid._ds._totalCount < 4) {
                    subChannelFeaturedGrid.addClass("featuredgridHeight");
                }
                if (!this._opts.params || !this._opts.params.isMenuItem) {
                    focusMgr.focus(subChannelFeaturedGrid);
                }

                this.checkPageContent(subChannelFeaturedGrid);

            }, this), function() {
                util.delay(1).then(function() {
                    focusMgr.focus(view);
                });
            });

            var loopedGridItems = ["showsLoopedGrid", "latestClipsLoopedGrid", "latestEpisodesLoopedGrid"];

            util.each(loopedGridItems, function(gridId) {
                this.setupGridDs(gridId, this._opts.params.channelId, this);
            }, this);

        },

        setupGridDs: function(gridId, subChannelNameId, thisController) {
            var view = this.getView(),
                grid = view.find(gridId),
                localDs = new Ds(),
                loaderOptions = redbullAPI.createExtendedOpts(redbullAPI.SortBy.PUBLISHED_ON, redbullAPI.SortDir.DESC),
                targetCtrl,
                gridRows = 5,
                dataLoader;

            switch (gridId) {
                case "showsLoopedGrid":
                    dataLoader = redbullAPI.getChannelShowsDataLoader(subChannelNameId, loaderOptions);
                    targetCtrl = ShowCtrl;
                    gridRows = 9;
                    break;
                case "latestEpisodesLoopedGrid":
                    dataLoader = redbullAPI.getChannelEpisodesDataLoader(subChannelNameId, loaderOptions);
                    targetCtrl = PlayerCtrl;
                    break;
                case "latestClipsLoopedGrid":
                    dataLoader = redbullAPI.getChannelClipsDataLoader(subChannelNameId, loaderOptions);
                    targetCtrl = PlayerCtrl;
                    break;
            }

            localDs.setDataLoader(dataLoader);

            grid._rows = gridRows;
            grid._cols = 1;

            //set the display startegy for showsLoopedGrid. Adding Redbullgriditem and images to showsLoopedGrid.
            grid.setDisplayStgy(function(data) {
                //if data contains message that means an empty ds has been assigned to the grid.
                //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                //error label has been shown. Error text need improvisation.
                if (data.message) {
                    var lblError = new Label({
                            text: data.message
                        }),
                        cell = new Layout({
                            alignment: Layout.VERTICAL,
                            id: "errorCell" + data.id,
                            clickable: false,
                            focusable: true,
                            children: [lblError]
                        });
                    return cell;
                }

                var img = data.object.images,
                    imgOpts = {
                        width: Utils.scaleDimension(370),
                        height: Utils.scaleDimension(210),
                        crop: true
                    },
                    bgImg;

                if (img && img.landscape) {
                    img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                }

                if (gridId === "showsLoopedGrid" && (data.object.images && data.object.images.portrait)) {

                    var opts = {
                        width: Utils.scaleDimension(160),
                        height: Utils.scaleDimension(220)
                    };

                    img = redbullAPI.getImageUrl(data.object.images.portrait.uri, opts);

                    if (data.object.images && data.object.images.background) {
                        bgImg = data.object.images.background.uri;
                    }
                }

                //For showsgrid no title or description is requiered.
                var gridImg;
                if (gridId === "showsLoopedGrid") {
                    gridImg = new RedBullGridItem({
                        css: "item-normal",
                        imageUrl: img,
                        liveNow: false,
                        tag: "",
                        tagMaxLength: 0,
                        title: "",
                        titleMaxLength: 0,
                        description: "",
                        descriptionMaxLength: 0,
                        subtitle: "",
                        subtitleMaxLength: 0,
                        time: "",
                        timeMaxLength: 0
                    });
                } else {
                    var subtitle = data.object.subtitle,
                            prettyDate = Utils.prettyDate(data.object.published_on);
                    gridImg = new RedBullGridItem({
                        css: "item-normal",
                        imageUrl: img,
                        tagMaxLength: 0,
                        title: data.object.title,
                        titleMaxLength: 0,
                        subtitle: subtitle,
                        subtitleMaxLength: 0,
                        time: prettyDate,
                        timeMaxLength: 0
                    });
                }

                //event handler for changing the controller on gridcell click
                thisController.changeController = util.bind(function() {
                    var params = {
                        videoObject: data.object,
                        breadCrumbs: data.object.channel_breadcrumbs,
                        videoId: data.object.id,
                        showTitle: data.object.title,
                        showId: data.object.id,
                        showBackgroundImage: bgImg,
                        nextCtrlName: ShowCtrl,
                        showTitleImage: data.object.images &&  data.object.images.logo ?  data.object.images.logo.uri : ""
                    };
                    thisController.getMainController().changeCurrentSubCtrl(targetCtrl, false, params);
                }, thisController);

                gridImg.addEventListener(evtType.CLICK, thisController.changeController);

                return gridImg;
            });

            this.setGridDatasource(grid, localDs).then(util.bind(function() {
                // if no data, hide grid
                if (!grid._ds._totalCount || (grid._ds._totalCount === 1 && grid._ds._data[0].message)) {
                    grid.getParent().hide();
                }

                this.checkPageContent(grid);
            }, this));
        },

        show: function() {
            this._super();
            sTracking.trackPage(this.__context.params);
        },

        hide: function() {
            this._super();
        },

        checkPageContent: function (grid)
        {
            var goBack = true,
                x;

            this.callCtr++;
            this.gridsContainer.push(grid);

            if (this._opts.mainCtrl._backCpt || this.callCtr < this.constructor.CALL)
            {
                return;
            }

            for (x = 0; x < this.gridsContainer.length; x++)
            {
                var gridObj = this.gridsContainer[x];

                if (gridObj._ds._totalCount > 0 && !gridObj._ds._data[0].message)
                {
                    goBack = false;
                    focusMgr.focus(gridObj);
                    break;
                }
            }

            if (goBack)
            {
                this.getMainController().getView().dispatchEvent(evtType.KEY, vKey.BACK);
            }
        }
    });
});

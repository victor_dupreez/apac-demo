/*global domReady*/

function hasClass(e, c) {
    return e._root._dom.getAttribute("class").indexOf(c) > -1;
}

define("app/ctrl/MainCtrl", [
        "ax/class",
        "ax/util",
        "ax/af/mvc/Controller",
        "ax/af/mvc/view",
        "ax/af/focusManager",
        "ax/af/evt/type",
        "ax/Env",
        "ax/device",
        "ax/ext/ui/Label",
        "ax/af/Component",

        "app/tmpl/MainTmpl",
        "app/ctrl/SubChannelCtrl",
        "app/ctrl/PlayerCtrl",
        "app/util/PreloadControllerManager",
        "app/ctrl/ChannelCtrl",
        "app/ctrl/SearchCtrl",
        "app/ctrl/HomeCtrl",
        "app/ctrl/LiveCtrl",
        "app/data/sRedbullAPI",
        "app/widget/ImageButton",
        "app/widget/sLoader",
        "app/util/sSettings",
        "app/util/Utils",
        "app/data/sRedbullEventsTracker",

        "ps/ui/Notification"
    ],
    function(klass, util, Controller, view, focusMgr, evtType,
        Env, device, Label, Component, mainTmpl, SubChannelCtrl,
        PlayerCtrl, PreloadMgr, ChannelCtrl, SearchCtrl, HomeCtrl,
        LiveCtrl, sRedbullAPI, ImageButton, sLoader, sSettings, appUtils, sRedbullEventsTracker,
        Notification) {

        var sEnv = Env.singleton();

        return klass.create(Controller, {}, {

            _menuSelItemId: null,
            _preloadMgr: null,
            _ctrlId: 0,
            _displayCtrlerOnFocus: true,
            _focusDelay: 0.35,

            init: function() {
                this.setView(view.render(mainTmpl));
                this._initNotification();
                this._super();
            },

            _initNotification: function() {
                domReady(util.bind(function() {
                    // Set up a global Notification instance.
                    // We add support for three new types of notifications:
                    // 1. Quit prompt
                    // 2. Live not yet started
                    // 3. Network error
                    // 4. General

                    // Listen for events from the notification viewer to enable/disable the dynamic focus
                    // but we don't care about the livestarted type, as it's not actionable

                    appUtils.notification.addEventListener(appUtils.notification.EVT_SHOW, function(evt) {
                        if (evt.type === appUtils.notification.options.type.LIVESTARTED.id) {
                            util.defer().then(function() {
                                if (!hasClass(appUtils.notification._rootElement, "live-notif-anim")) {
                                    appUtils.notification._rootElement.addClass("live-notif-anim");
                                }
                            });
                        }
                    });

                    appUtils.notification.addEventListener(appUtils.notification.EVT_HIDE, util.bind(function(evt) {
                        if (evt.type ===appUtils.notification.options.type.LIVESTARTED.id) {
                            appUtils.notification._rootElement.removeClass("live-notif-anim");
                            return;
                        }
                        var main = this.getView().find("menu");
                        if (appUtils.notification._previousFocus &&appUtils.notification._previousFocus.getParent() === main) {
                            return;
                        }
                    }, this));

                }, this));
            },

            getCurrentSubCtrl: function() {
                return this.getView().find("pageContainer").getChildren()[0].getAttachedController();
            },

            changeCurrentSubCtrl: function(newController, skipHistory, params) {
                this._backCpt = false;
                var cId;
                // apparently samsung 2011 doesnt like instanceof, so lets use ===
                if (newController === PlayerCtrl) {
                    cId = "player-ctrler";
                }else {
                    cId = "__subctrl" + this._ctrlId++;
                    this._preloadMgr.mapController(newController, cId, {
                        level: 1,
                        params: params,
                        doLoad: true,
                        skipOrder: true,
                        deleteOnUnload: true
                    });
                }
                if (skipHistory) {
                    this._preloadMgr.displayController(cId, true, PreloadMgr.HISTORY_REPLACE, params);
                } else {
                    this._preloadMgr.displayController(cId, true, PreloadMgr.HISTORY_ADD, params);
                }
            },

            back: function() {
                this._preloadMgr.back(true);
            },

            toggleFullScreen: function() {
                var main = this.getView().find("main-view");
                if (hasClass(main, "fullscreen-min")) {
                    main.removeClass("fullscreen-min");
                } else {
                    main.addClass("fullscreen-min");
                }
            },

            isFullScreen: function() {
                return hasClass(this.getView().find("main-view"), "fullscreen-min");
            },

            _changeActiveMenuImage: function(state, component) {
                component = component || this._menuSelItemId;

                if (component._icon || component._focusIcon) {
                    if (state === "normal") {
                        component.image.show();
                        component.image_focused.hide();
                    } else {
                        component.image.hide();
                        component.image_focused.show();
                    }
                } else {
                    component.removeClass("icon-search").removeClass("icon-search-focused");
                    component.addClass("icon-search" + (state === "normal" ? "" : "-focused"));
                }
            },

            setup: function() {

                this._backCpt = true;

                var view = this.getView(),
                    menu = view.find("menu"),
                    page = view.find("page");

                // create a preload controller manager
                this._preloadMgr = new PreloadMgr(this, "pageContainer", {
                    checkLevelWhenUnload: true,
                    doNotUnloadOrderedCtrlers: true
                });

                // configure app
                this._preloadMgr.setPreloadWindowSize(sSettings.getConfig().preload_window_size);
                this._displayCtrlerOnFocus = sSettings.getConfig().load_menuitems_on_focus;
                this._focusDelay = sSettings.getConfig().menuitems_loading_delay;

                this._preloadMgr.displayController("menu-main");
                this._menuSelItemId = view.find("menu-main");

                if(!appUtils.isAnimatedMenu())
                {
                    menu.addClass("disable-animation");
                    page.addClass("disable-animation");
                }

                this.getChannels();
            },

            // then request the main channels and map the ctrlers accordingly
            getChannels: function() {
                var self = this,
                    view = this.getView(),
                    menu = view.find("menu"),
                    search = view.find("menu-search");

                sRedbullAPI.getChannels().then(util.bind(function(data) {
                    // we're ignoring the channel "main" here (so we start at 1)
                    var i = 0,
                        len = data.channels.length,
                        curChan;
                    for (; i < len; i++) {

                        var opts = {
                            width: appUtils.scaleDimension(80)
                        };

                        curChan = data.channels[i];
                        // Create the menu button
                        util.each(curChan.icons, function(pair) {
                            this[pair.key].uri = sRedbullAPI.getImageUrl(pair.value.uri, opts);
                        }, curChan.icons);

                        // @TODO do this prettier
                        if (curChan.id === "main") {
                            curChan.title = "Red Bull TV";
                        }

                        var menuBtn = new ImageButton({
                            //bg: "./img/icons/icon_" + curChan.id + ".png",
                            icons: curChan.icons,
                            text: curChan.title,
                            id: "menu-" + curChan.id
                        });

                        // if live, create a badge object
                        if (curChan.id === "live") {
                            this._badge = new Label({});
                            this._badge.addClass("livecount-badge");
                            this._badge._count = 0;
                            this._badge.hide();
                            menuBtn.attach(this._badge);
                        }

                        // and attach it to the menu
                        menu.attach(menuBtn, Component.PLACE_BEFORE, search);

                        if (curChan.id === "main") {
                            menuBtn.setOption("nextUp", "menu-search");
                            this._preloadMgr.mapController(HomeCtrl, "menu-" + curChan.id, {
                                doLoad: true,
                                level: 0,
                                params: {
                                    channelId: curChan.id,
                                    channelName: curChan.title,
                                    isMenuItem: true
                                }
                            });
                        } else {
                            // map the controller accordingly to the type of channel
                            this._preloadMgr.mapController(curChan.id === "live" ? LiveCtrl : curChan.sub_channels.length > 0 ? ChannelCtrl : SubChannelCtrl, "menu-" + curChan.id, {
                                level: 0,
                                params: {
                                    channelId: curChan.id,
                                    channelName: curChan.title,
                                    isMenuItem: true
                                }
                            });
                        }
                    }

                    // map last controller
                    this._preloadMgr.mapController(SearchCtrl, "menu-search", {
                        level: 0,
                        retain: true,
                        params: {
                            channelName: "search",
                            isMenuItem: true
                        }
                    });

                    // map player ctrler
                    this._preloadMgr.mapController(PlayerCtrl, "player-ctrler", {
                        level: -1,
                        skipOrder: true,
                        retain: true
                    });

                    // and display the first one

                    // FIXME: start live event tracking
                    sRedbullEventsTracker.activate(sSettings.getConfig().events_tracking_opts);
                    sRedbullEventsTracker.addEventListener(sRedbullEventsTracker.EVT_LIVECOUNT_UPDATED, function(evt) {
                        var liveEvts = evt.newCount;
                        if (self._badge._count !== liveEvts) {
                            self._badge._count = liveEvts;
                            if (liveEvts) {
                                self._badge.setText(liveEvts);
                                if (self._badge.isHidden()) {
                                    self._badge.show();
                                }
                            } else {
                                self._badge.hide();
                            }
                        }
                    });
                    sRedbullEventsTracker.addEventListener(sRedbullEventsTracker.EVT_STATUS_UPDATED, function(evt) {
                        // if event is live, display a notif
                        if (evt.newStatus === sRedbullAPI.StreamStatus.LIVE) {
                            var etitle = sRedbullEventsTracker.getTrackedEventStream(evt.eventId).object.title;
                            appUtils.notification.add({
                                type:appUtils.notification.options.type.LIVESTARTED,
                                text: "<span class=\"bold-font\">" + etitle + "</span> just went live. Message will close in 2 seconds.",
                                hideDelay: 3500
                            });
                        }
                    });

                    // continue the page setup

                    this._postSetup();

                }, this),
                function() {
                    sLoader.hide();
                    appUtils.notification.add({
                        type:appUtils.notification.options.type.NETWORKERROR,
                        text: "We're experiencing network difficulties right now. The application will now close. Please try again later.",
                        buttons: [{
                            label: "EXIT",
                            action: function() {
                                appUtils.exit(true);
                            }
                        }]
                    });
                });
            },

            _postSetup: function() {

                var view = this.getView(),
                    preloadMgr = this._preloadMgr,
                    main = view.find("main-view"),
                    menu = view.find("menu"),
                    page = view.find("page");

                var focusListener = util.bind(function(e) {
                    if (e.relatedTarget && e.relatedTarget.getParent().getId() !== "menu") {
                        return;
                    }
                    if (preloadMgr.existsController(e.target.getId())) {
                        var newSel = menu.find(e.target.getId());
                        this._changeActiveMenuImage("focused", newSel);
                        // add a delay before loading to prevent useless loading when navigating in the menu
                        util.clearDelay("displayCtrler");
                        util.delay(this._focusDelay, "displayCtrler").then(util.bind(function() {
                            // if we have selected a new menu item, load it
                            if (newSel !== this._menuSelItemId) {
                                // and display the controller
                                preloadMgr.displayController(e.target.getId(), false, PreloadMgr.HISTORY_REPLACE).then(function() {
                                    // here we can do stuff after the controller has been rendered
                                });
                                this._menuSelItemId = newSel;
                            }
                        }, this));
                    }
                }, this);

                var blurListener = util.bind(function(e) {
                    if (e.relatedTarget instanceof Notification) {
                        return;
                    }
                    if (!hasClass(main, "menu-min")) {
                        main.addClass("menu-min");
                        // change icon
                        this._changeActiveMenuImage("normal", e.target);
                    }
                }, this);

                // expend menu when focus on it
                menu.addEventListener(evtType.FOCUS, util.bind(function(e) {
                    if (hasClass(main, "menu-min")) {
                        main.removeClass("menu-min");
                    }
                    if (e.relatedTarget && e.relatedTarget.getParent().getId() !== "menu") {
                        if (e.target !== this._menuSelItemId) {
                            focusMgr.focus(this._menuSelItemId);
                        }
                        this._changeActiveMenuImage("focused");
                    }
                }, this));


                menu.addEventListener(evtType[this._displayCtrlerOnFocus ? "FOCUS" : "CLICK"], focusListener);
                menu.addEventListener(evtType.CLICK, util.bind(function(evt) {
                    focusMgr.focus(evt.target);
                    if (this._menuSelItemId && evt.target.getId() === this._menuSelItemId.getId()) {
                        focusMgr.focus(page);
                    }

                }, this));

                menu.addEventListener(evtType.BLUR, blurListener);

                var f = util.bind(function() {
                    if (this._menuSelItemId && !hasClass(this._menuSelItemId, "focused")) {
                        // change icon
                        this._changeActiveMenuImage("active");
                        // add the focused class to the menu item
                        this._menuSelItemId.addClass("focused");
                    }
                }, this);

                page.addEventListener(evtType.FOCUS, f, {}, true);

                // listener for the key event
                view.addEventListener(evtType.KEY, util.bind(this.onKey, this));

                focusMgr.focus(menu);

                sEnv.addEventListener("FOCUS_PAGE", function() {
                    focusMgr.focus(page);
                });

            },

            onKey: function(obj) {

                var view = this.getView(),
                    main = view.find("main-view"),
                    menu = view.find("menu"),
                    backKey = device.vKey.BACK.id,
                    exitKey = device.vKey.EXIT.id;

                var notificationAction = function(action) {
                    if (action) {
                        action();
                    }
                };

                if ((obj.id === backKey || obj.id === exitKey) && focusMgr.getCurFocus() === menu.getActiveChild()) {

                    var toTv = obj.id !== backKey;

                    appUtils.notification.add({
                        type:appUtils.notification.options.type.QUITPROMPT,
                        text: "Do you really want to quit?",
                        buttons: [{
                            label: "YES",
                            focused: true,
                            action: function() {
                                appUtils.exit(toTv);
                            }
                        }, {
                            label: "CANCEL"
                        }]
                    });

                    return false;

                } else if (obj.id === device.vKey.BACK.id && appUtils.notification.isShowing()) {
                    var notification = appUtils.notification._queue[0],
                        i, len, action, buttons;
                    if (notification.buttons && notification.buttons.length > 0 ) {
                        buttons = notification.buttons;
                        for (i = 0, len = buttons.length; i < len; i++) {
                            if (buttons[i] && typeof buttons[i].action === "undefined") {
                                action = null;
                            } else if (buttons[i] && !action) {
                                action = buttons[i].action;
                            }
                        }
                    }
                    notificationAction(action);
                    appUtils.notification.hideCurrent();
                    return false;
                } else if (obj.id === device.vKey.BACK.id) {
                    // Find the best button for back key.
                    // If there is a button without actoin we use that. Otherwise we just use the first / last best one.
                    // key back event
                    var aCtrl = this._preloadMgr.getController("player-ctrler");
                    var overlay;
                    if (aCtrl) {
                        overlay = aCtrl.getOverlay();
                    }
                    // we're on the root of the app, we display and focus the menu if it's minified
                    var doBackOpt = true;
                    //if playerController is showing overlay hide it
                    if (aCtrl && !aCtrl.liveSoonOrOver && overlay && !overlay.isDisabled()) {
                        aCtrl.setOverlayDisabled(false);
                        aCtrl.hideOverlay(0);
                        doBackOpt = false;
                    }else if (this._backCpt === true && hasClass(main, "menu-min")) {
                        main.removeClass("menu-min");
                        focusMgr.focus(this._menuSelItemId);
                    }
                    if(doBackOpt) {
                        this._backCpt = this._preloadMgr.getHistory().length === 1;
                    }
                } else if (obj.id === device.vKey.RIGHT.id) {
                    util.clearDelay("displayCtrler");
                }
            }
        });
    });

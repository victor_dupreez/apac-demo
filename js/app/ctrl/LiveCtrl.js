define("app/ctrl/LiveCtrl", [
        "ax/class",
        "ax/af/mvc/view",
        "ax/af/data/LocalDatasource",
        "ax/af/evt/type",
        "ax/console",
        "ax/util",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",

        "app/ctrl/AbstractCtrl",
        "app/tmpl/LiveTmpl",
        "app/widget/RedBullGridItem",
        "app/data/sRedbullAPI",
        "app/ctrl/PlayerCtrl",
        "app/ctrl/ShowCtrl",
        "app/util/Utils",
        "app/util/RedbullGridStgy",
        "app/data/sTracking",
        "app/data/sRedbullEventsTracker"
    ],
    function(klass, view, Ds, evtType, console, util,
        Label, Layout,
        AbstractCtrl, LiveTmpl, RedBullGridItem, redbullAPI,
        PlayerCtrl, ShowCtrl, Utils,
        redbullGridStgy, sTracking, sRedbullEventsTracker) {

        return klass.create(AbstractCtrl, {}, {

            changeController: null,

            init: function() {
                console.log("LiveTmpl init()");
                this.setView(view.render(LiveTmpl, {
                    name: this.ctrlName
                }));
                this._super("LiveTmpl");
            },

            setup: function(context) {
                console.log("LiveTmpl setup()", context);
                this._super(context);
                var view = this.getView(),
                    self = this,
                    dataLoader,
                    featuredItemsDs = new Ds(),
                    subChannelFeaturedGrid = view.find("featuredgrid");

                // set a specific key navigation strategy for out featured grid
                subChannelFeaturedGrid.setKeyNavigationStgy(redbullGridStgy.FEATURED_GRID_KEY_NAVIGATION_STGY);

                //call the redbullapi to fetch the first six featured videos.
                redbullAPI.setDataLoaderLimit(3);
                dataLoader = redbullAPI.getChannelFeaturedDataLoader(context.params.channelId);
                featuredItemsDs.setDataLoader(dataLoader);
                redbullAPI.setDataLoaderLimit(0);

                //set the display startegy for subChannelFeaturedGrid. Adding Redbullgriditem and images to subChannelFeaturedGrid.
                subChannelFeaturedGrid.setDisplayStgy(function(data) {

                    //if data contains message that means an empty ds has been assigned to the grid.
                    //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                    //error label has been shown. Error text need improvisation.
                    if (data.message) {
//                        view.find("showsLoopedGrid").setOption("nextUp", undefined);
                        var lblError = new Label({
                            text: data.message
                        }),
                            cell = new Layout({
                                alignment: Layout.VERTICAL,
                                id: "errorCell" + data.id,
                                clickable: false,
                                focusable: false,
                                children: [lblError]
                            });
                        return cell;
                    }

                    var img = data.object.images,
                        liveNow = false;

                    var imgOpts = {
                            width: Utils.scaleDimension(400),
                            height: Utils.scaleDimension(220),
                            crop: true
                        };

                    if (data.id === 0) {
                        imgOpts.width = Utils.scaleDimension(800);
                        imgOpts.height = Utils.scaleDimension(450);
                    }

                    if (img && img.landscape) {
                        img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                    }


                    var tag = data.object.channel_breadcrumbs.length > 0 ? data.object.channel_breadcrumbs[data.object.channel_breadcrumbs.length - 1].toUpperCase() : "";
                    var desc = data.id === 0 ? data.object.short_description : null;
                    var prettyDate = Utils.prettyDate(data.object.updated_on ? data.object.updated_on : data.object.published_on);
                    var subtitle = data.object.title;
                    if (data.object.stream && (data.object.stream.status === redbullAPI.StreamStatus.LIVE || data.object.stream.status === redbullAPI.StreamStatus.LIVE_WAITING)) {

                        liveNow = true;
                        prettyDate = "Right Now";
                        /*subtitle = Utils.futureDate(Utils.isoDate(data.object.stream.starts_at),
                                                    Utils.isoDate(data.object.stream.ends_at));*/
                    }

                    var image = new RedBullGridItem({
                        css: "item-normal",
                        imageUrl: img,
                        liveNow: liveNow,
                        tag: tag,
                        tagMaxLength: 0,
                        title: data.object.subtitle,
                        titleMaxLength: 0,
                        description: desc,
                        descriptionMaxLength: 0,
                        subtitle: subtitle,
                        subtitleMaxLength: 0,
                        time: prettyDate,
                        timeMaxLength: 0
                    });

                    if (data.id === 0) {
                        image.addClass("bigImage");
                    } else if (data.id === 3 || data.id === 4) {
                        image.addClass("fixCellSize");
                    }

                    image.addEventListener(evtType.CLICK, util.bind(function() {
                        var field = "channel_breadcrumbs";
                        var params = {
                            videoObject: data.object,
                            breadCrumbs: data.object[field],
                            videoId: data.object.id,
                            nextCtrlName: ShowCtrl
                        };
                        this.getMainController().changeCurrentSubCtrl(PlayerCtrl, false, params);
                    }, self));

                    return image;

                });

                self.setGridDatasource(subChannelFeaturedGrid, featuredItemsDs);

                util.each(["liveEventGrid", "upcomingLiveEventGrid", "pastLiveEventGrid"], function(gridId) {
                    self.setupGridDs(gridId, self);
                }, self);
            },

            setupGridDs: function(gridId, thisController) {
                var view = this.getView(),
                    grid = view.find(gridId),
                    self = this;

                var localDs = new Ds();
                var targetCtrl;
                var gridRows = 5;
                var dataLoader;

                switch (gridId) {
                    case "liveEventGrid":
                        targetCtrl = PlayerCtrl;
                        dataLoader = sRedbullEventsTracker.getLiveEventStreamsDataLoader(localDs);
                        break;
                    case "upcomingLiveEventGrid":
                        dataLoader = sRedbullEventsTracker.getClosestEventStreamsPerEventDataLoader(localDs, 10, 100);
                        targetCtrl = PlayerCtrl;
                        break;
                    case "pastLiveEventGrid":
                        dataLoader = redbullAPI.getPastEventsDataLoader();
                        targetCtrl = PlayerCtrl;
                        break;
                }

                localDs.setDataLoader(dataLoader);

                grid._rows = gridRows;
                grid._cols = 1;

                //set the display startegy for showsLoopedGrid. Adding Redbullgriditem and images to showsLoopedGrid.
                grid.setDisplayStgy(function(data) {

                    //if data contains message that means an empty ds has been assigned to the grid.
                    //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                    //error label has been shown. Error text need improvisation.
                    if (data.message) {
                        var lblError = new Label({
                            text: data.message
                        }),
                        cell = new Layout({
                            alignment: Layout.VERTICAL,
                            id: "errorCell",
                            clickable: false,
                            focusable: false,
                            children: [lblError]
                        });
                        // hide the grid
                        grid.getParent().hide();
                        return cell;
                    }

                    // display the grid if it was previously hidden
                    if (grid.getParent().isHidden()) {
                        grid.getParent().show();
                    }

                    var prettyDate;
                    if (data.object.stream && (data.object.stream.status === redbullAPI.StreamStatus.LIVE || data.object.stream.status === redbullAPI.StreamStatus.LIVE_WAITING)) {
                        prettyDate = " Right now";
                    }else if (grid.getId() === "upcomingLiveEventGrid") {
                        //prettyDate = Utils.countdownDate(Utils.isoDate(data.object.stream.starts_at));
                        prettyDate = Utils.prettyDate(data.object.stream.starts_at);
                    } else if (grid.getId() === "liveEventGrid") {
                        prettyDate = Utils.futureDate(Utils.isoDate(data.object.stream.starts_at),
                                                      Utils.isoDate(data.object.stream.ends_at));
                    } else {
                        prettyDate = Utils.prettyDate((data.object.stream.starts_at)?data.object.stream.starts_at:data.object.published_on);
                    }

                    var opts = {
                            width: Utils.scaleDimension(370),
                            height: Utils.scaleDimension(210),
                            crop: true
                        },
                        gridImg = new RedBullGridItem({
                            css: "item-normal",
                            imageUrl: redbullAPI.getImageUrl(data.object.images.landscape.uri, opts),
                            tagMaxLength: 0,
                            subtitle: data.object.title,
                            titleMaxLength: 0,
                            title: data.object.subtitle,
                            subtitleMaxLength: 0,
                            time: prettyDate,
                            timeMaxLength: 0
                        });

                    // for the upcoming event, we will have a countdown
                    if (grid.getId() === "upcomingLiveEventGrid") {
                        sRedbullEventsTracker.attachCountdownToComponent(gridImg, gridImg._time, Utils.isoDate(data.object.stream.starts_at));
                    }

                    //event handler for changing the controller on gridcell click
                    thisController.changeController = util.bind(function() {
                        var params = {
                            videoObject: data.object,
                            breadCrumbs: data.object.channel_breadcrumbs,
                            videoId: data.object.id,
                            showTitle: data.object.title,
                            showId: data.object.id,
                            nextCtrlName: ShowCtrl
                        };
                        thisController.getMainController().changeCurrentSubCtrl(targetCtrl, false, params);
                    }, thisController);

                    gridImg.addEventListener(evtType.CLICK, thisController.changeController);

                    return gridImg;
                });

                self.setGridDatasource(grid, localDs).then(function() {
                    // if no data, hide grid
                    if (!grid._ds._totalCount || (grid._ds._totalCount === 1 && grid._ds._data[0].message)) {
                        grid.getParent().hide();
                    }
                }, function() {
                    grid.getParent().hide();
                });

            },

            show: function() {
                sTracking.trackPage(this.__context.params);
            }

        });
    });

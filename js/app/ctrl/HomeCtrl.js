define("app/ctrl/HomeCtrl", [
        "ax/class",
        "ax/af/mvc/view",
        "ax/af/data/LocalDatasource",
        "ax/af/evt/type",
        "ax/console",
        "ax/util",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",

        "app/ctrl/AbstractCtrl",
        "app/tmpl/HomeTmpl",
        "app/widget/RedBullGridItem",
        "app/data/sRedbullAPI",
        "app/ctrl/PlayerCtrl",
        "app/ctrl/ShowCtrl",
        "app/util/Utils",
        "app/util/RedbullGridStgy",
        "app/data/sTracking",
        "app/data/sRedbullEventsTracker"
    ],
    function(klass, view, Ds, evtType, console, util,
        Label, Layout, AbstractCtrl,
        HomeTmpl, RedBullGridItem, redbullAPI, PlayerCtrl, ShowCtrl,
        Utils, redbullGridStgy,
        sTracking, sRedbullEventsTracker) {

        return klass.create(AbstractCtrl, {}, {

            changeController: null,

            init: function() {
                console.log("HomeCtrl init()");
                this.setView(view.render(HomeTmpl, {
                    name: this.ctrlName
                }));
                this._super("HomeCtrl");
            },

            setup: function(context) {

                console.log("HomeCtrl setup()", context);
                this._super(context);
                var view = this.getView(),
                    self = this,
                    dataLoader,
                    featuredItemsDs = new Ds(),
                    subChannelFeaturedGrid = view.find("featuredgrid");

                // set a specific key navigation strategy for out featured grid
                subChannelFeaturedGrid.setKeyNavigationStgy(redbullGridStgy.FEATURED_GRID_KEY_NAVIGATION_STGY);

                //call the redbullapi to fetch the first six featured videos.
                redbullAPI.setDataLoaderLimit(3);
                dataLoader = redbullAPI.getFeaturedDataLoader();
                featuredItemsDs.setDataLoader(dataLoader);
                redbullAPI.setDataLoaderLimit(0);

                //set the display startegy for subChannelFeaturedGrid. Adding Redbullgriditem and images to subChannelFeaturedGrid.
                subChannelFeaturedGrid.setDisplayStgy(function(data) {

                    //if data contains message that means an empty ds has been assigned to the grid.
                    //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                    //error label has been shown. Error text need improvisation.
                    if (data.message) {

//                        view.find("focusableContainer").hide();
//                        view.find("showsLoopedGrid").setOption("nextUp", undefined);

                        var lblError = new Label({
                            text: ""
                        }),
                            cell = new Layout({
                                alignment: Layout.VERTICAL,
                                id: "errorCell" + data.id,
                                clickable: false,
                                focusable: false,
                                children: [lblError]
                            });
                        return cell;
                    }

                    var img = data.object.images;
                    var imgOpts = {
                            width: Utils.scaleDimension(400),
                            height: Utils.scaleDimension(220),
                            crop: true
                        };
                    if (data.id === 0) {
                        imgOpts.width = Utils.scaleDimension(800);
                        imgOpts.height = Utils.scaleDimension(450);
                    }

                    if (img && img.landscape) {
                        img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                    }

                    var liveNow = false;
                    var tag = data.object.channel_breadcrumbs.length > 0 ? data.object.channel_breadcrumbs[data.object.channel_breadcrumbs.length - 1].toUpperCase() : "";
                    var desc = data.id === 0 ? data.object.short_description : null;
                    var prettyDate = Utils.prettyDate((data.object.stream && data.object.stream.starts_at)?data.object.stream.starts_at:data.object.published_on);
                    var subtitle = data.object.subtitle;

                    if (data.object.stream && (data.object.stream.status === redbullAPI.StreamStatus.LIVE || data.object.stream.status === redbullAPI.StreamStatus.LIVE_WAITING)) {
                        liveNow = true;
                        prettyDate = "Right Now";
                        /*subtitle = Utils.futureDate(Utils.isoDate(data.object.stream.starts_at),
                                                    Utils.isoDate(data.object.stream.ends_at));*/
                    }

                    var image = new RedBullGridItem({
                        css: "item-normal",
                        imageUrl: img,
                        liveNow: liveNow,
                        tag: tag,
                        tagMaxLength: 0,
                        title: data.object.title,
                        titleMaxLength: 0,
                        description: desc,
                        descriptionMaxLength: 0,
                        subtitle: subtitle,
                        subtitleMaxLength: 0,
                        time: prettyDate,
                        timeMaxLength: 0
                    });

                    if (data.id === 0) {
                        image.addClass("bigImage");
                    } else if (data.id === 3 || data.id === 4) {
                        image.addClass("fixCellSize");
                    }

                    image.addEventListener(evtType.CLICK, util.bind(function() {
                        var params = {
                            videoObject: data.object,
                            breadCrumbs: data.object.channel_breadcrumbs,
                            videoId: data.object.id,
                            nextCtrlName: ShowCtrl
                        };
                        this.getMainController().changeCurrentSubCtrl(PlayerCtrl, false, params);
                    }, self));

                    return image;

                });

                self.setGridDatasource(subChannelFeaturedGrid, featuredItemsDs);

                util.each(["liveEventGrid","showsLoopedGrid", "latestClipsLoopedGrid", "latestEpisodesLoopedGrid","filmEventGrid", "pastLiveEventGrid"], function(gridId) {
                    self.setupGridDs(gridId, self);
                }, self);
            },

            setupGridDs: function(gridId, thisController) {
                var view = this.getView(),
                    grid = view.find(gridId),
                    self = this;

                var localDs = new Ds();
                var loaderOptions = redbullAPI.createExtendedOpts(redbullAPI.SortBy.CREATED_ON, redbullAPI.SortDir.DESC);
                var targetCtrl;
                var gridRows = 5;
                var dataLoader;

                switch (gridId) {
                    case "liveEventGrid":
                        targetCtrl = PlayerCtrl;
                        // we want 1 evt stream per event, and 100 in total max (we include currently live events as well)
                        dataLoader = sRedbullEventsTracker.getClosestEventStreamsPerEventDataLoader(localDs, 10, 100, true);
                        break;
                    case "showsLoopedGrid":
                        dataLoader = redbullAPI.getHomeFeaturedShowsDataLoader(loaderOptions);
                        targetCtrl = ShowCtrl;
                        gridRows = 9;
                        break;
                    case "latestEpisodesLoopedGrid":
                        loaderOptions = redbullAPI.createExtendedOpts(redbullAPI.SortBy.PUBLISHED_ON, redbullAPI.SortDir.DESC);
                        dataLoader = redbullAPI.getHomeEpisodesDataLoader(loaderOptions);
                        targetCtrl = PlayerCtrl;
                        break;
                    case "latestClipsLoopedGrid":
                        loaderOptions = redbullAPI.createExtendedOpts(redbullAPI.SortBy.PUBLISHED_ON, redbullAPI.SortDir.DESC);
                        dataLoader = redbullAPI.getHomeClipsDataLoader(loaderOptions);
                        targetCtrl = PlayerCtrl;
                        break;
                    case "pastLiveEventGrid":
                        dataLoader = redbullAPI.getPastEventsDataLoader();
                        targetCtrl = PlayerCtrl;
                        break;
                    case "filmEventGrid":
                        dataLoader = redbullAPI.getChannelFilmDataLoader();
                        targetCtrl = PlayerCtrl;
                        break;
                }

                localDs.setDataLoader(dataLoader);

                grid._rows = gridRows;
                grid._cols = 1;

                //set the display startegy for showsLoopedGrid. Adding Redbullgriditem and images to showsLoopedGrid.
                grid.setDisplayStgy(function(data) {
                    //if data contains message that means an empty ds has been assigned to the grid.
                    //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                    //error label has been shown. Error text need improvisation.
                    if (data.message) {
                        var lblError = new Label({
                            text: data.message
                        }),
                            cell = new Layout({
                                alignment: Layout.VERTICAL,
                                id: "errorCell" + data.id,
                                clickable: false,
                                focusable: false,
                                children: [lblError]
                            });
                        return cell;
                    }

                    var img = data.object.images,
                        imgOpts = {
                            width: Utils.scaleDimension(370),
                            height: Utils.scaleDimension(210),
                            crop: true
                        },
                        bgImg;

                    if (img && img.landscape) {
                        img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                    }

                    if (gridId === "showsLoopedGrid" && (data.object.images && data.object.images.portrait)) {

                        var opts = {
                            width: Utils.scaleDimension(160),
                            height: Utils.scaleDimension(220)
                        };

                        img = redbullAPI.getImageUrl(data.object.images.portrait.uri, opts);

                        if (data.object.images && data.object.images.background) {
                            bgImg = data.object.images.background.uri;
                        }
                    }

                    //For showsgrid no title or description is requiered.
                    var gridImg,
                        title,
                        subtitle;
                    if (gridId === "showsLoopedGrid") {
                        gridImg = new RedBullGridItem({
                            css: "item-normal",
                            imageUrl: img,
                            liveNow: false,
                            tag: "",
                            tagMaxLength: 0,
                            title: "",
                            titleMaxLength: 0,
                            description: "",
                            descriptionMaxLength: 0,
                            subtitle: "",
                            subtitleMaxLength: 0,
                            time: "",
                            timeMaxLength: 0
                        });
                    }
                    else {

                        subtitle = (gridId !== "latestClipsLoopedGrid") ? data.object.subtitle : null;
                        var prettyDate,
                                tag,
                                liveNow;


                        title = data.object.title;
                        subtitle = data.object.subtitle;

                        if (gridId === "pastLiveEventGrid") {
                            title = data.object.subtitle;
                            subtitle = data.object.title;
                        }

                        if (data.object.stream && (data.object.stream.status === redbullAPI.StreamStatus.LIVE || data.object.stream.status === redbullAPI.StreamStatus.LIVE_WAITING)) {
                            liveNow = true;
                            prettyDate = "Right Now";
                            //subtitle = Utils.futureDate(Utils.isoDate(data.object.stream.starts_at),
                            //                            Utils.isoDate(data.object.stream.ends_at));
                        } else if (grid.getId() === "liveEventGrid") {
                            //subtitle = Utils.futureDate(Utils.isoDate(data.object.stream.starts_at));
                            prettyDate = Utils.prettyDate(data.object.stream.starts_at);
                            //prettyDate = Utils._replaceSpaces(Utils.countdownDate(Utils.isoDate(data.object.stream.starts_at)));
                            tag = null;
                            title = data.object.subtitle;
                            subtitle = data.object.title;
                        } else {
                            prettyDate = Utils.prettyDate((data.object.stream && data.object.stream.starts_at)?data.object.stream.starts_at:data.object.published_on);
                            tag = data.object.channel_breadcrumbs.length > 0 ? data.object.channel_breadcrumbs[data.object.channel_breadcrumbs.length - 1].toUpperCase() : "";
                        }

                        gridImg = new RedBullGridItem({
                            css: "item-normal",
                            imageUrl: img,
                            liveNow: liveNow,
                            tag: tag,
                            tagMaxLength: 0,
                            subtitle: subtitle,
                            titleMaxLength: 0,
                            title: title,
                            subtitleMaxLength: 0,
                            time: prettyDate,
                            timeMaxLength: 0
                        });
                    }

                    if (grid.getId() === "liveEventGrid" && data.object.stream.status !== redbullAPI.StreamStatus.LIVE) {
                        sRedbullEventsTracker.attachCountdownToComponent(gridImg, gridImg._time, Utils.isoDate(data.object.stream.starts_at));
                    }

                    //event handler for changing the controller on gridcell click
                    thisController.changeController = util.bind(function() {
                        var params = {
                            videoObject: data.object,
                            breadCrumbs: data.object.channel_breadcrumbs,
                            videoId: data.object.id,
                            showTitle: data.object.title,
                            showId: data.object.id,
                            showBackgroundImage: bgImg,
                            nextCtrlName: ShowCtrl,
                            showTitleImage: data.object.images &&  data.object.images.logo ?  data.object.images.logo.uri : ""
                        };

                        thisController.getMainController().changeCurrentSubCtrl(targetCtrl, false, params);
                    }, thisController);

                    gridImg.addEventListener(evtType.CLICK, thisController.changeController);

                    return gridImg;
                });

                self.setGridDatasource(grid, localDs).then(function() {
                    console.log("LOADING OK for" + grid.getId());
                }, function(failed) {
                    console.log("LOADING FAILED for " + grid.getId() + " : ", failed);
                });

            },

            show: function() {
                this._super();

                sTracking.trackPage(this.__context.params);
            },

            hide: function() {
                this._super();
            }

        });
    });

define("app/ctrl/PlayerCtrl", [
        "ax/class",
        "ax/console",
        "ax/af/mvc/view",
        "ax/af/data/Datasource",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",
        "ax/af/evt/type",
        "ax/device",
        "ax/device/Media",
        "ax/af/mvc/AppRoot",
        "ax/util",
        "ax/af/focusManager",
        "ax/device/vKey",
        "ax/af/Container",
        "ax/af/Component",

        "app/ctrl/AbstractCtrl",
        "app/tmpl/PlayerTmpl",
        "app/data/sRedbullAPI",
        "app/widget/RedBullGridItem",
        "app/util/Utils",
        "app/widget/Loader",
        "app/data/sTracking",
        "app/util/sSettings",
        "app/util/Utils",
        "app/data/sRedbullEventsTracker",

        "ps/ui/TTMLCaptioning",
        "lib/conviva"
    ],
    function(klass, console, View, Datasource, Label, Layout, evtType, device, Media,
        AppRoot, util, focusMgr, VKey, Container, Component, AbstractCtrl, PlayerTmpl, redbullAPI,
        RedBullGridItem, Utils, Loader, sTracking,
        sSettings, appUtils, sRedbullEventsTracker, TTMLCaptioning, conviva) {

        var sAppRoot = AppRoot.singleton(),
            sMedia = Media.singleton();

        return klass.create(AbstractCtrl, {}, {
            showOverlayInterval: null,
            ignoreShowOverlay: false,
            liveSoonOrOver: false,
            _nextCtrlName: null,
            _videosQueue: [],


            init: function() {
                this.setView(View.render(PlayerTmpl, {
                    name: this.ctrlName
                }));
                this._super("PlayerController");
            },

            deinit: function() {
                this._super();
            },
            getOverlay: function(){
                return this._overlay;
            },
            setup: function(context) {
                this._super(context);
                var view = this.getView();

                this._footerWidthFixed = false;

                this._playerRoot = view.find("player-root");
                this._playerTop = view.find("player-top");
                this._playerLive = view.find("player-live-area").hide();
                this.footer = view.find("player-footer-wrapper");
                this.footer.grid.setOption("nextLeft", "player-footer-wrapper");
                this._overlayOpacity = view.find("overlay-opacity");
                this._overlay = view.find("player-container");
                this._logo = view.find("liveLogo");
                this._captionContainer = view.find("captions-container");
                this._playBtn = view.find("playButton");
                this._stopBtn = view.find("pauseButton");
                this._rewindBtn = view.find("rewindButton");
                this._forwardBtn = view.find("forwardButton");


                this.loader = new Loader({
                    delayAmount: 0,
                    center: true
                });
                view.attach(this.loader);

                this.playerSkipBackConfig = {};
                util.extend(this.playerSkipBackConfig, sSettings.getConfig().player_skip_options);
                this.playerSkipBackConfig.sec *= -1;

                this.loader.show();

                this.playerButtonEventListeners();

                this._updateHandlerFct = util.bind(this._updateHandler, this);
                this._playHandlerFct = util.bind(this._playHandler, this);
                this._finishedHandlerFct = util.bind(this._finishedHandler, this);
                this._bufferingHandlerFct = util.bind(this._bufferingHandler, this);
                this._errorHandlerFct = util.bind(this._errorHandler, this);

                sMedia.addEventListener(sMedia.EVT_TIME_UPDATE, this._updateHandlerFct);
                sMedia.addEventListener(sMedia.EVT_STATE_CHANGED, this._playHandlerFct);
                sMedia.addEventListener(sMedia.EVT_FINISHED, this._finishedHandlerFct);
                sMedia.addEventListener(sMedia.EVT_BUFFERING_PROGRESS, this._bufferingHandlerFct);
                sMedia.addEventListener(sMedia.EVT_ERROR, this._errorHandlerFct);
                this._setFooterDisplayStgy(view);
            },

            playerButtonEventListeners: function()
            {
                this._stopBtn.addEventListener(evtType.CLICK, util.bind(this.stopClick, this));
                this._playBtn.addEventListener(evtType.CLICK, util.bind(this.playClick, this));
                this._rewindBtn.addEventListener(evtType.CLICK, util.bind(this.rwClick, this));
                this._forwardBtn.addEventListener(evtType.CLICK, util.bind(this.ffClick, this));
            },

            /* handlers */
            playClick: function(evt) {
                if (evt && this._overlay.isDisabled()) {
                    return;
                }
                if (sMedia.isState(sMedia.PLAYING)) {
                    sMedia.pause();
                } else {
                    sMedia.play();
                }
            },

            simplePlayClick: function(evt) {
                if (evt && this._overlay.isDisabled()) {
                    return;
                }
                sMedia.play();
            },

            pauseClick: function(evt) {
                if (evt && this._overlay.isDisabled()) {
                    return;
                }
                if (sMedia.isState(sMedia.PLAYING)) {
                    sMedia.pause();
                }
            },

            stopClick: function(evt) {
                if (evt && this._overlay.isDisabled()) {
                    return;
                }

                this.getMainController().back();
            },

            ffClick: function() {
                if (this.loader._showCounter) {
                    return;
                }
                this.hideOverlay();
                sMedia.skip(sSettings.getConfig().player_skip_options);
            },

            rwClick: function() {
                if (this.loader._showCounter) {
                    return;
                }
                this.hideOverlay();
                sMedia.skip(this.playerSkipBackConfig);
            },

            _keyDownEvent: function(evt) {
                var key = device.tvkey.getKeyMapping().keyCode[evt.keyCode];

                if (!key) {
                    return;
                }

                if (this.media && !this.media.ad && !this.closedByNotification && key.id !== VKey.STOP.id) {
                    if(this.ignoreShowOverlay) {
                        this.ignoreShowOverlay = false;
                    }else{
                        this.showOverlay();
                    }
                }

                switch (key.id) {
                    case VKey.PLAY_PAUSE.id:
                        focusMgr.focus(this._playBtn);
                        this.playClick();
                        break;
                    case VKey.PLAY.id:
                        focusMgr.focus(this._playBtn);
                        this.simplePlayClick();
                        break;
                    case VKey.PAUSE.id:
                        focusMgr.focus(this._playBtn);
                        this.pauseClick();
                        break;
                    case VKey.STOP.id:
                        this.stopClick();
                        break;
                    case VKey.FF.id:
                        focusMgr.focus(this._forwardBtn);
                        this.ffClick();
                        break;
                    case VKey.RW.id:
                        focusMgr.focus(this._rewindBtn);
                        this.rwClick();
                        break;
                    case VKey.OK.id:
                        if (!this._overlay.isDisabled()) {
                            var curFoc = focusMgr.getCurFocus(),
                                fct = (curFoc === this._rewindBtn) ? this.rwClick : (curFoc === this._forwardBtn) ? this.ffClick : null;
                            if (!this._actionTimer && fct) {
                                fct = util.bind(fct, this);
                                fct();
                                this._actionTimer = setInterval(fct, 150);
                                return;
                            }
                        } else {
                            this.showOverlay();
                        }
                        break;
                }
            },

            _keyUpEvent: function(evt) {
                var key = device.tvkey.getKeyMapping().keyCode[evt.keyCode];
                if (key && key.id === VKey.OK.id) {
                    clearInterval(this._actionTimer);
                    this._actionTimer = null;
                }
            },

            _setFooterDisplayStgy: function() {

                this.footer.setDisplayStrategy(util.bind(function(item) {
                    var css = item.object.css || "footer-item";
                    if (item.object.extraCss) {
                        css += " " + item.object.extraCss;
                    }
                    var footeritem = View.render({
                        klass: Layout,
                        clickable: true,
                        focusable: true,
                        id: item.object.type || "footer-item",
                        css: css,
                        children: [{
                            klass: Container,
                            css: (item.object.css || "footer-item") + "-image"
                        },
                        {
                            klass: Label,
                            css: (item.object.css || "footer-item") + "-text",
                            text: item.object.text
                        }]
                    });

                    if (item.object.type === "show-overview") {
                        footeritem.addEventListener(evtType.CLICK, util.bind(function() {
                            if (this._overlay.isDisabled()) {
                                return;
                            }
                            // if we come from this show page, just back
                            if (this._params && this._params.callerShowId === item.object.data.showId) {
                                this.getMainController().back();
                            }
                            // otherwise navigate to the video's show page
                            else {
                                this.getMainController().changeCurrentSubCtrl(this._params.nextCtrlName, true, item.object.data);
                            }
                        }, this));
                    }
                    if (item.object.type === "show-live") {
                        footeritem.addEventListener(evtType.CLICK, util.bind(function() {
                            if (this._overlay.isDisabled()) {
                                return;
                            }
                            // TODO: go to "all live events"
                            this.getMainController().back();
                        }, this));
                    }
                    else if (item.object.type === "subtitle-toggle") {
                        footeritem.addEventListener(evtType.CLICK, util.bind(function() {
                            if (this._overlay.isDisabled()) {
                                return;
                            }
                            if (footeritem.getRoot().hasClass("captions-off")) {
                                this._showCaptions = true;
                                footeritem.removeClass("captions-off");
                            } else {
                                this._showCaptions = false;
                                footeritem.addClass("captions-off");
                            }
                        }, this));
                    }

                    return footeritem;
                }, this));

            },

            _updateHandler: function(t) {
                var view = this.getView();
                if (!view) {
                    return;
                }

                var timeline = view.find("timeline"),
                    timePassed = view.find("timePassed"),
                    timeRemaining = view.find("timeRemaining"),
                    remainingTimeInSeconds = parseInt(sMedia.getDuration() - t, 10);

                timePassed.setText(Utils.formatTime(parseInt(t, 10)));
                if(sMedia.getDuration()>0){
                    timeRemaining.setText(Utils.formatTime(remainingTimeInSeconds));
                }else{
                    timeRemaining.setText("--:--");
                }
                console.log("timeline update" + t / sMedia.getDuration() * 100);
                timeline.update(t / sMedia.getDuration() * 100);

                if (remainingTimeInSeconds <= 20) {
                    sTracking.dispatchEvent(sTracking.VIDEO_EOF, this.getPlayerTimes());
                }
            },

            _bufferingHandler: function() {
                this.loader.show();
                this.loader.showCounter = 1;
            },

            _errorHandler: function() {
                this.loader.hide(true);

                // if the video isn't a live event, show the error message popup
                if (!this.media || !this.media.liveEvent) {
                    return this.showVideoErrorPopup();
                }
                // TODO: otherwise what do we do ?
                else {
                    return this.showVideoErrorPopup();
                }
            },

            _playHandler: function(states) {

                var playBtnImage;

                if (states.toState === sMedia.BUFFERING || states.toState === sMedia.CONNECTING) {
                    this.loader.show();
                    this.loader.showCounter = 1;
                } else {
                    this.loader.hide(true);
                }
                if (states.toState === sMedia.PLAYING) {
                    this.hideOverlay();
                    this.setOverlayDisabled(false);
                    sTracking.dispatchEvent(sTracking.VIDEO_PLAY_RESUME, this.getPlayerTimes());
                }

                if (states.toState === sMedia.PAUSED) {
                    sTracking.dispatchEvent(sTracking.VIDEO_PAUSE, this.getPlayerTimes());
                }

                if (states.toState === sMedia.STOPPED) {
                    sTracking.dispatchEvent(sTracking.VIDEO_STOP, this.getPlayerTimes());
                    conviva.cleanUpSession();
                }

                if (!this._playBtn || !this._playBtn._root) {
                    return;
                }

                playBtnImage = states.toState === sMedia.PLAYING ? "pause-video" : "play-video";
                this._playBtn.removeClass("pause-video").removeClass("play-video");
                this._playBtn.addClass(playBtnImage);
            },

            getPlayerTimes: function() {
                return {
                    mt1: sMedia.getCurTime() || "0",
                    mt2: sMedia.getDuration() || "0"
                };
            },

            _finishedHandler: function() {
                // when a video is over lets play the next one from the queue
                if (this._view) {
                    this._playVideo();
                }
            },


            /* end handlers */

            show: function(params) {

                var self = this;

                this.closedByNotification = false;
                params.screenName = "playerscreen";
                this._params = params;

                sTracking.trackPage(params);

                if (!params.videoId) {
                    console.log("No video id specified");
                    return this.showVideoErrorPopup();
                }

                sAppRoot.getView().getRoot().addClass("staticPosition");
                if (!this.getMainController().isFullScreen()) {
                    this.getMainController().toggleFullScreen();
                }

                this._keyDownEventInstance = util.bind(this._keyDownEvent, this);
                this._keyUpEventInstance = util.bind(this._keyUpEvent, this);

                document.addEventListener("keydown", this._keyDownEventInstance, false);
                document.addEventListener("keyup", this._keyUpEventInstance , false);

                focusMgr.focus(this._playBtn);

                this.hideOverlay(0);
                this._overlay.removeClass("soon");
                this.loader.show();

                // load the video
                util.delay(1, "play_delay").then(function() {
                    self.loadVideoData(params.videoId);
                });
            },

            setOverlayDisabled: function(disable) {
                if (disable) {
                    clearTimeout(this._timer);
                }
                this._overlayDisabled = disable;
            },

            showOverlay: function(unlock) {
                if (unlock) {
                    this.setOverlayDisabled(false);
                }
                util.defer().then(util.bind(function() {

                    this._captionContainer.hide();

                    if (this._overlay && this._overlay._root) {
                        this._overlay.enable();

                        // Focus play/pause button if the overlay just been shown.
                        if (this._overlay.isHidden()) {
                            this._overlay.show();
                            focusMgr.focus(this._playBtn);
                        }
                    }
                    if (!this._overlayDisabled) {
                        this.hideOverlay();
                    }
                }, this));
            },
            clearIgnoreShowOverlay: function(scope){
                return function(){
                    clearInterval(scope.showOverlayInterval);
                    scope.ignoreShowOverlay = false;
                };
            },

            hideOverlay: function(time) {
                this.ignoreShowOverlay = true;
                this.showOverlayInterval = setInterval(this.clearIgnoreShowOverlay(this), 500);

                if (!this._showCaptions) {
                    this._captionContainer.hide();
                }

                if (time !== 0) {
                    time = 5000;
                }
                clearTimeout(this._timer);
                this._timer = setTimeout(util.bind(function() {

                    if (this._showCaptions) {
                        this._captionContainer.show();
                    }

                    if (this._overlay && this._overlay._root) {
                        this._overlay.disable();
                        this._overlay.hide();
                    }
                }, this), time);
            },

            loadVideoData: function(videoId) {
                sMedia.stop();

                var self = this,
                    view = this.getView();

                // do not show captions by default
                this._showCaptions = false;
                // stop event tracking if any
                this._untrackEvent();
                // clear videos queue
                this._clearQueue();
                // clear video footer
                this._clearFooter();
                this.isLive = true;

                // retrieve the video data and queue the video
                redbullAPI.getVideo(videoId, sSettings.getConfig().renditions).then(util.bind(function(video) {
                    sTracking.setVideo(video);
                    redbullAPI.setDataLoaderLimit(3);
                    // set video data in view
                    var cat = video.channel_breadcrumbs.length > 0 ? video.channel_breadcrumbs[video.channel_breadcrumbs.length - 1].toUpperCase() : "",
                        videoTitle = video.stream ? video.subtitle : video.title;

                    var dataLoader = redbullAPI.getRelatedVideosDataLoader(videoId);

                    if (cat === "LIVE") {
                        cat = "";
                        dataLoader = redbullAPI.getPastEventsDataLoader();
                    }

                    redbullAPI.setDataLoaderLimit(0);

                    view.find("videoCat").setText(cat);
                    view.find("videoTitle").setText(videoTitle);
                    view.find("videoDesc").setText(video.long_description);
                    view.find("videoDate").setText(Utils.videoDateFormat(video));

                    var grid = view.find("player-related-videos"),
                        controls = view.find("player-controls");

                    var ds = new Datasource();

                    ds.setDataLoader(dataLoader);

                    grid.setDisplayStgy(function(data) {

                        if (data.message) {
                            var lblError = new Label({
                                    text: data.message
                                }),
                                errorcell = new Layout({
                                    alignment: Layout.VERTICAL,
                                    id: "errorCell" + data.id,
                                    clickable: false,
                                    focusable: false,
                                    children: [lblError]
                                });
                            return errorcell;
                        }

                        // picture
                        var img = data.object.images,
                            imgOpts = {
                                width: appUtils.scaleDimension(155),
                                height:appUtils.scaleDimension(88),
                                crop:true
                            };

                        if (img && img.landscape) {
                            img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                        }

                        var redbullGridItem = new RedBullGridItem({
                            css: "item-normal",
                            imageUrl: img,
                            title: data.object.title
                        });

                        redbullGridItem.addEventListener(evtType.CLICK, util.bind(function() {
                            if (this._overlay.isDisabled()) {
                                return;
                            }
                            this.loadVideoData(data.object.id);
                            focusMgr.focus(controls);
                        }, self));

                        return redbullGridItem;
                    });

                    // Bind to you might like grid
                    grid.setDatasource(ds).then(util.bind(function() {
                        if (this.getDatasource().getFetchedCount() === 1 && this.getDatasource().getFetchedData(0).message) {
                            view.find("player-grid").hide();
                        } else {
                            view.find("player-grid").show();
                            if (focusMgr.getCurFocus() === view.find("focus-trap")) {
                                focusMgr.focus(grid);
                                this.getDatasource().setTotalCount(3);
                            }
                        }
                    }, grid), function() {
                        view.find("player-grid").hide();
                    });

                    var cc = null;
                    if (video.closed_captions && video.closed_captions.dfxp) {
                        cc = video.closed_captions.dfxp.uri;
                        this._addFooterItem({
                                type: "subtitle-toggle",
                                css: "subtitle-toggle",
                                extraCss: "captions-off",
                                text: "Subtitles"
                            }
                        );
                    }

                    if (video.show && !video.stream) {
                        var show = video.show || {},
                            images = show.images || {};

                        this._addFooterItem({
                            type: "show-overview",
                            data: {
                                    breadCrumbs: video.channel_breadcrumbs,
                                    showTitle: video.show.title,
                                    showId: video.show.id,
                                    showBackgroundImage: images.background ? images.background.uri : "",
                                    showTitleImage: images.logo ? images.logo.uri : ""
                                },
                                css: "show-overview",
                                text: "Show Overview"
                            }
                        );
                    }

                    // we have an event video
                    if (video.stream) {

                    }

                    if (cc || (video.show && !video.stream)) {
                        this.footer.show();
                    } else {
                        this.footer.hide();
                    }

                    var url = null;
                    if (video.stream) {
                        if (video.videos.live && video.stream.status === redbullAPI.StreamStatus.LIVE) {
                            url = video.videos.live.uri;
                        } else if (video.videos.demand) {
                            url = video.videos.demand.uri;
                            this.isLive = false;
                        } else {
                            url = video.videos.live ? video.videos.live.uri : "";
                        }
                    }
                    else if (video.videos.master) {
                        url = video.videos.master.uri;
                    }

                    this._setFooterDatasource();
                    this._queueVideo({
                        url: url,
                        cc: cc,
                        type: "hls",
                        liveEvent: !!video.stream,
                        ad: false,
                        videoObj: video
                    });

                    this._overlay.removeClass("hidden-visibility");

                }, this), function() {
                    return self.showVideoErrorPopup();
                });
            },

            _clearFooter: function() {
                this.footer.clearFooter();
                this._footerData = [];
            },

            _addFooterItem: function(item) {
                this._footerData.push(item);
            },

            _setFooterDatasource: function() {
                if (!this._footerData.length) {
                    return;
                }
                var ds = new Datasource();
                this.footer.setDatasource(ds, this._footerData).then(util.bind(function(){

                    if (this.footer.getDatasource().getFetchedCount() <= 0) {
                        this.footer.hide();
                    } else {
                        this.footer.show();
                    }
                }, this));
            },

            _fixFooterWidth: function() {
                if (!this._footerWidthFixed && this.footer.getDatasource() && this.footer.getDatasource().getFetchedCount() > 0) {
                    var grid = this.footer.grid,
                        element = grid.getChildren()[0].getRoot().getHTMLElement(),
                        width = element.getBoundingClientRect().width;

                    grid.getRoot().getHTMLElement().style.width = width + "px";

                    this._footerWidthFixed = true;
                }
            },

            setTimelineMode: function(type) {
                var view = this.getView(),
                    playerButtons = view.find("player-buttons"),
                    btn = view.find("playButton"),
                    timelineBlock = view.find("player-timeline");

                // going to live mode (play button on same line as timeline)
                if (type !== "normal") {
                    if (btn.getParent() === playerButtons) {
                        playerButtons.detach(btn);
                        timelineBlock.attach(btn, Component.PLACE_AFTER, view.find("timePassed"));
                        timelineBlock.addClass("liveMode");
                        btn.setOption("nextLeft", btn);
                        btn.show();
                    }
                }
                // going to normal mode
                else {
                    if (btn.getParent() !== playerButtons) {
                        timelineBlock.detach(btn);
                        timelineBlock.removeClass("liveMode");
                        playerButtons.attach(btn, Component.PLACE_AFTER, view.find("rewindButton"));
                        btn.setOption("nextLeft", null);
                    }
                }
            },

            showBlock: function(blockId) {

                var view = this.getView(),
                    playerOverlay = view.find("player-container"),
                    playerTop = view.find("player-top"),
                    playerControls = view.find("player-controls"),
                    playerButtons = view.find("player-buttons"),
                    livePaused = view.find("player-live-area-paused"),
                    liveOver = view.find("player-live-area-thanks"),
                    liveSoon = view.find("player-live-area-countdown"),
                    resolution = device.system.getDisplayResolution(),
                    imgOpts = {
                        width: resolution.width,
                        height: resolution.height,
                        crop: true,
                        blur: true,
                        saturation: -70
                    };


                var status = this.media && this.media.videoObj.stream ? this.media.videoObj.stream.status : "",
                    statuses = redbullAPI.StreamStatus;

                var live = status === statuses.LIVE ||
                    status === statuses.LIVE_WAITING ||
                    status === statuses.PAUSE;

                this.setTimelineMode(live ? "live" : "normal");

                if (blockId !== "soon") {
                    playerOverlay.removeClass("soon");
                }

                this.liveSoonOrOver = false;
                if (blockId === "pause") {
                    playerTop.hide();
                    playerControls.show();
                    liveOver.hide();
                    liveSoon.hide();
                    livePaused.show();
                    this._playerLive.show();
                    this.liveSoonOrOver = true;
                    sMedia.pause();
                }
                else if (blockId === "soon") {
                    playerOverlay.addClass("soon");
                    var dif = Utils.isoDate(this.media.videoObj.stream.starts_at) - new Date();
                    liveSoon.setCountdownTimer(dif);
                    liveSoon.setCountdownDescVisible(this.media.videoObj.stream.status === redbullAPI.StreamStatus.LIVE_WAITING);

                    playerTop.show();
                    playerControls.hide();
                    liveOver.hide();
                    liveSoon.show();
                    livePaused.hide();
                    this._playerLive.show();
                    this.liveSoonOrOver = true;
                }
                else if (blockId === "over") {
                    playerTop.hide();
                    playerControls.hide();
                    liveOver.show();
                    liveSoon.hide();
                    livePaused.hide();

                    this._clearFooter();
                    this._addFooterItem({
                        type: "show-live",
                        data: {},
                        css: "show-overview",
                        text: "Show all live events"
                    });
                    this._setFooterDatasource();

                    this._playerLive.show();
                    this.liveSoonOrOver = true;
                    sMedia.stop();
                }
                else if (blockId === "normal") {
                    playerTop.show();
                    playerControls.show();
                    liveOver.hide();
                    liveSoon.hide();
                    livePaused.hide();
                }

                if (blockId === "normal") {
                    this._overlayOpacity.removeClass("opacity-eighty-img");
                    this._overlayOpacity.addClass("opacity-seventy-cmd");
                    this._overlay.getRoot().getHTMLElement().style.backgroundImage = "";
                    this.setOverlayDisabled(false);
                    focusMgr.focus(this.getView().find("player-controls"));
                }
                else {
                    // change opacity type and put a background picture
                    this._overlayOpacity.removeClass("opacity-seventy-cmd");
                    var ur = redbullAPI.getImageUrl(this.media.videoObj.images.background.uri, imgOpts);
                    this._overlay.getRoot().getHTMLElement().style.backgroundImage = "url('" + ur + "')";

                    this.setOverlayDisabled(true);
                    this.showOverlay();
                    playerButtons.hide();
                    focusMgr.focus(this.getView().find("focus-trap"));
                }
                if (live) {
                    this._logo.addClass("live");
                    playerButtons.hide();
                } else {
                    this._logo.removeClass("live");
                    playerButtons.show();
                }
            },

            showVideoErrorPopup: function() {
                var self = this;
                conviva.cleanUpSession();
                this.setOverlayDisabled(true);
                appUtils.notification.add({
                    type: appUtils.notification.options.type.GENERAL,
                    text: "Sorry, we are having difficulties playing this video right now. Please try again later.",
                    buttons: [{
                        label: "OK",
                        action: function() {
                            self.closedByNotification = true;
                            util.delay(0.5).then(function() {
                                self.getMainController().back();
                            });
                        }
                    }]
                });
            },

            _clearQueue: function() {
                this._videosQueue = [];
            },

            _queueVideo: function(media) {
                // no url, notify and exit
                if (!media.url && !media.liveEvent) {
                    return this.showVideoErrorPopup();
                }
                this._videosQueue.push(media);
                if (media.playDirectly || sMedia.isState(sMedia.STOPPED)) {
                    this._playVideo();
                }
            },

            _showBlockForEventStream: function(evt) {
                if (evt.stream.status === redbullAPI.StreamStatus.SOON || evt.stream.status === redbullAPI.StreamStatus.PRE_EVENT || evt.stream.status === redbullAPI.StreamStatus.LIVE_WAITING) {
                    this.showBlock("soon");
                    return false;
                } else if (evt.stream.status === redbullAPI.StreamStatus.COMPLETE) {
                    this.showBlock("over");
                    this._untrackEvent();
                } else if (evt.stream.status === redbullAPI.StreamStatus.REPLAY) {
                    // @TODO: fixme
                    this.showBlock("over");
                    this._untrackEvent();
                } else if (evt.stream.status === redbullAPI.StreamStatus.PAUSE) {
                    this.showBlock("pause");
                } else if (evt.stream.status === redbullAPI.StreamStatus.LIVE) {
                    // TODO: try to start the stream
                    this.showBlock("normal");
                    this._logo.addClass("live");
                }
                return true;
            },

            _trackEvent: function() {
                this._untrackEvent();
                // if no media, or if media isn't a live event, do nothing
                if (!this.media || !this.media.videoObj || !this.media.videoObj.stream) {
                    return;
                }
                console.log("tracking event ", this.media);

                sRedbullEventsTracker.trackEventStream(this.media.videoObj);
                this._eventChangeHandler = {
                        eventId: this.media.videoObj.id,
                        func: util.bind(function(evt) {
                            if (evt.eventType === sRedbullEventsTracker.EVT_STATUS_UPDATED) {
                                // do something when this happens
                                console.log("video from " + evt.oldStatus + " to " + evt.newStatus);
                                // update event status
                                this.media.videoObj.stream.status = evt.newStatus;
                                // update player view
                                this._showBlockForEventStream(this.media.videoObj);

                                // if going to live, start video
                                if (evt.newStatus === redbullAPI.StreamStatus.LIVE) {
                                    this.loadVideoData(this.media.videoObj.id);
                                }
                            }
                            else if (evt.eventType === sRedbullEventsTracker.EVT_START_TIME_UPDATED) {
                                // we need to refresh the countdown if it is displayed right now
                                if (this.media.videoObj.stream.status === redbullAPI.StreamStatus.PRE_EVENT) {
                                    this.media.videoObj.stream.starts_at = evt.newStartTime;
                                    var view = this.getView(),
                                        liveSoon = view.find("player-live-area-countdown"),
                                        dif = Utils.isoDate(this.media.videoObj.stream.starts_at) - new Date();
                                    liveSoon.setCountdownTimer(dif);
                                }
                            }

                        }, this)
                    };
                sRedbullEventsTracker.addEventListener(sRedbullEventsTracker.EVT_UPDATED_FOR_STREAM(this._eventChangeHandler.eventId), this._eventChangeHandler.func);
            },
            _untrackEvent: function() {
                console.log("untracking event ", this.media);

                if (this._eventChangeHandler && this._eventChangeHandler.eventId) {
                    sRedbullEventsTracker.removeEventListener(sRedbullEventsTracker.EVT_UPDATED_FOR_STREAM(this._eventChangeHandler.eventId), this._eventChangeHandler.func);
                }
            },

            _playVideo: function() {
                // nothing to play, exit the player
                if (!this._videosQueue.length) {
                    return this.getMainController().back();
                }

                // get the next media to play
                this.media = this._videosQueue.shift();
                var playNow = true;
                this._overlay.removeClass("soon");
                // if it's an ad, immediately hide the overlay and remove focus
                if (this.media.ad) {
                    this.hideOverlay(0);
                    focusMgr.focus(this.getView().find("focus-trap"));
                }
                else {
                    // TODO: test code: if live event, display the pause block
                    if (this.media.liveEvent) {
                        // let's start tracking it
                        this._trackEvent();

                        playNow = this._showBlockForEventStream(this.media.videoObj);
                        this.loader.hide();

                        if (playNow === true) {
                            this.showBlock("normal");
                        }

                    }

                    // normal video
                    else {
                        this.showBlock("normal");
                    }
                }

                if (this.media.cc) {
                    this._captioning = new TTMLCaptioning({
                        subtitleFileUrl: this.media.cc,
                        videoPlayer: sMedia,
                        captionsContainer: this._captionContainer
                    });
                }

                sMedia.stop();

                if (this.media.url && playNow) {

                    var mediaOpts = { type: this.media.type };

                    if ((device.platform === "samsung" || device.platform === "tizen") && !this.isLive)
                    {
                        mediaOpts.startBitrate = 2000000;
                    }

                    sMedia.load(this.media.url, mediaOpts);
                    sMedia.setFullscreen();
                    sMedia.play();
                }
                conviva.createSession(this.media);
                console.log("Video shown: ", this.media);
            },

            hide: function() {

                if (this._captioning) {
                    this._captioning.deinit();
                }

                // clear play delay to prevent media from playing when ctrler is hidden
                util.clearDelay("play_delay");
                // stop overlay timer
                clearTimeout(this._timer);
                // stop video
                sMedia.stop();
                // stop tracking live event if any
                this._untrackEvent();
                // remove key events
                sAppRoot.getView().getRoot().removeClass("staticPosition");
                document.removeEventListener("keydown", this._keyDownEventInstance, false);
                document.removeEventListener("keyup", this._keyUpEventInstance, false);
                // remove fullscreen
                if (this.getMainController().isFullScreen()) {
                    this.getMainController().toggleFullScreen();
                }

                var view = this.getView(),
                    timeline = view.find("timeline"),
                    timePassed = view.find("timePassed"),
                    timeRemaining = view.find("timeRemaining");

                timePassed.setText(Utils.formatTime(0));
                timeRemaining.setText(Utils.formatTime(0));
                timeline.update(0);

                // Reset value
                this.closedByNotification = false;
                // Hide overlay
                this.hideOverlay(0);
                // hide loader if present
                this.loader.hide(true);
                // hide footer
                this.footer.hide();
                // hide live div
                this._playerLive.hide();
            },

            reset: function() {
                sMedia.removeEventListener(sMedia.EVT_TIME_UPDATE, this._updateHandlerFct);
                sMedia.removeEventListener(sMedia.EVT_STATE_CHANGED, this._playHandlerFct);
                sMedia.removeEventListener(sMedia.EVT_FINISHED, this._finishedHandlerFct);
                sMedia.removeEventListener(sMedia.EVT_BUFFERING_PROGRESS, this._bufferingHandlerFct);
                sMedia.removeEventListener(sMedia.EVT_ERROR, this._errorHandlerFct);
                conviva.cleanUpSession();
                this.hide();
            }
        });
    });

define("app/ctrl/ShowCtrl", [
        "ax/class",
        "ax/af/mvc/view",
        "ax/af/focusManager",
        "ax/af/data/LocalDatasource",
        "ax/af/evt/type",
        "ax/console",
        "ax/util",
        "ax/ext/ui/Image",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",
        "ax/af/Container",
        "ax/ext/ui/grid/ScrollingGrid",
        "ax/device",

        "app/ctrl/AbstractCtrl",
        "app/tmpl/ShowTmpl",
        "app/widget/RedBullGridItem",
        "app/data/sRedbullAPI",
        "app/ctrl/PlayerCtrl",
        "app/util/Utils",
        "app/widget/AnimatedGrid",
        "app/data/sTracking"
    ],
    function(klass, view, focusMgr, Ds, evtType, console, util,
        Image, Label, Layout, Container,  ScrollingGrid, device, AbstractCtrl, ShowTmpl,
        RedBullGridItem, redbullAPI, PlayerCtrl, Utils, AnimatedGrid,
        sTracking) {

        return klass.create(AbstractCtrl, {}, {
            init: function() {
                console.log("ShowCtrl init()");
                this.setView(view.render(ShowTmpl, {
                    name: this.ctrlName
                }));
                this._super("ShowCtrl");
            },

            setup: function(context) {
                console.log("ShowCtrl setup()", context);
                this._super(context);

                this.__context.params.screenName = "showscreen";

                var view = this.getView(),
                    self = this,
                    mainView = view.find("main-view"),
                    showLayout = view.find("showLayout"),
                    backgroundHolder = view.find("background-holder"),
                    gradientOverlay = view.find("show-gradient-overlay"),
                    resolution = device.system.getDisplayResolution(),
                    opts = {
                        width: (resolution.width - 80),
                        height: resolution.height,
                        crop: true
                    },
                    dataLoader,
                    imgLayoutBackground;


                //set the background image
                imgLayoutBackground = new Image({
                    src: redbullAPI.getImageUrl(context.params.showBackgroundImage, opts),
                    css: "backgroundImage"
                });
                //To make the bg image visible, attach the img to main Container before mainview - scrollable container.
                //mainContainer.attach(imgLayoutBackground, Container.PLACE_BEFORE, mainView);
                backgroundHolder.attach(imgLayoutBackground, Container.PLACE_BEFORE, gradientOverlay);
                mainView.addClass("frontView");

                //set the show title
                var ctnr = new Container();
                ctnr.addClass("centerDiv");

                showLayout.attach(ctnr);

                //Get all the episodes for the showId
                var loaderOptions = redbullAPI.createExtendedOpts(redbullAPI.SortBy.CREATED_ON, redbullAPI.SortDir.ASC);
                redbullAPI.getAllShowEpisodes(context.params.showId, loaderOptions).then(function(episodes) {

                    if (episodes) {
                        // after this next loop, season[i] will contains all episodes (ordered) for the season i
                        // we can then just create a grid for each season[i]
                        var seasons = [],
                            i = 0,
                            len = episodes.videos.length,
                            episode;

                        for (; i < len; i++) {
                            episode = episodes.videos[i];
                            if (!seasons[episode.season_number-1]) {
                                seasons[episode.season_number-1] = [];
                            }
                            seasons[episode.season_number-1].push(episode);
                        }

                        for (i=0; i<seasons.length; i++) {
                            var arr = seasons[i];
                            if (arr) {
                                arr.sort(function(a, b) {
                                    return b.episode_number - a.episode_number;
                                });
                            }
                        }

                        for (var j = 0; j < seasons.length; j++) {
                            if (!seasons[j]) {
                                continue;
                            }
                            var lblSeasonNumber = new Label({
                                text: "Season " + (j+1),
                                css: "gridTitle"
                            });
                            showLayout.attach(lblSeasonNumber);

                            var episodeDs = new Ds();
                            dataLoader = redbullAPI.getDataLoaderFromArray(seasons[j]);
                            episodeDs.setDataLoader(dataLoader);

                            var episodeGrid = new AnimatedGrid({
                                rows: 5,
                                forwardFocus: true,
                                autoNavigation: true,
                                alignment: ScrollingGrid.VERTICAL,
                                scrollEndBoundary: 2,
                                scrollFrontBoundary: 2,
                                displayEndBoundary: 1,
                                displayFrontBoundary: 2,
                                cols: 1,
                                id: "#episodeGrid" + (j+1)
                            });

                            if (j === 0) {
                                episodeGrid.setOption("nextUp", null);
                            }

                            episodeGrid.addClass("episodeGrid borderGradientBig");

                            var lblPlaceHolder = new Label({
                                text: "  "
                            });

                            //IMP: To make a cell of a grid focusable. Otherwise entire grid is selected instead of a cell.
                            //                        episodeGrid._container._opts["forwardFocus"]=true;
                            //                        episodeGrid._container._opts["focusable"]=true;

                            showLayout.attach(episodeGrid);
                            showLayout.attach(lblPlaceHolder);

                            //set display startegy for episodeGrid.
                            episodeGrid.setDisplayStgy(function(data) {
                                //if data contains message that means an empty ds has been assigned to the grid.
                                //To avoid focus issue and uncaught exception cannot access 0 of a null, this small
                                //error label has been shown. Error text need improvisation.
                                if (data.message) {
                                    var lblError = new Label({
                                            text: data.message
                                        }),
                                        lblCell = new Layout({
                                            alignment: Layout.VERTICAL,
                                            id: "errorCell" + data.id,
                                            clickable: false,
                                            focusable: false,
                                            children: [lblError]
                                        });
                                    return lblCell;
                                }

                                var img = data.object.images,
                                    imgOpts = {
                                        width: Utils.scaleDimension(370),
                                        height: Utils.scaleDimension(208),
                                        crop: true
                                    };

                                if (img && img.landscape) {
                                    img = redbullAPI.getImageUrl(img.landscape.uri, imgOpts);
                                }

                                var redbullGridItem = new RedBullGridItem({
                                        css: "item-normal",
                                        imageUrl: img,
                                        liveNow: false,
                                        tag: "",
                                        tagMaxLength: 0,
                                        title: data.object.title,
                                        titleMaxLength: 0,
                                        description: "",
                                        descriptionMaxLength: 0,
                                        subtitle: "Episode " + data.object.episode_number,
                                        subtitleMaxLength: 0,
                                        info: "",
                                        infoMaxLength: 0,
                                        time: "",
                                        timeMaxLength: 0,
                                        id: "cellcat" + data.object.season_number + "-" + data.object.episode_number
                                    }),

                                    lblTelecastedDate = new Label({
                                        text: Utils.simpleDate(data.object.published_on),
                                        css: "whitelabel"
                                    }),

                                    episodeDescription = new Label({
                                        text: util.truncate(data.object.long_description, 110, "..."),
                                        css: "toggleWhiteLabel"
                                    }),

                                    cell = new Layout({
                                        alignment: Layout.VERTICAL,
                                        forwardFocus: true,
                                        children: [redbullGridItem, lblTelecastedDate, episodeDescription]
                                    });

                                cell.addEventListener(evtType.CLICK, util.bind(function() {
                                    var params = {
                                        videoObject: data.object,
                                        breadCrumbs: data.object.channel_breadcrumbs,
                                        videoId: data.object.id,
                                        callerShowId: context.params.showId,
                                        nextCtrlName: self.constructor
                                    };
                                    this.getMainController().changeCurrentSubCtrl(PlayerCtrl, false, params);
                                }, self));

                                return cell;
                            });

                            // if we come from a video, we will try to load from the video episode instead of the first episode
                            var selGrid = "episodeGrid1";
                            if (episodeGrid.getId() === "episodeGrid" + context.params.videoSeason) {
                                episodeGrid.setPrereadyStgy(function(state) {
                                    state.selectedDsIndex = context.params.videoNumber - 1;
                                    state.selectedGridRow = 0;
                                });
                                selGrid = episodeGrid.getId();
                                self.__target = episodeGrid;
                                self.__targetDs = episodeDs;
                            }

                            episodeGrid.setDatasource(episodeDs).then((function(grid) {
                                var g = grid;
                                return function() {
                                    /*
                                    // TODO: if an episode is given when creating the controller, the grid will try to focus it
                                    if (context.params.videoSeason) {
                                        if (g.getId() === "episodeGrid" + context.params.videoSeason) {
                                            var cell = "cellcat" + context.params.videoSeason + "-" + context.params.videoNumber;
                                            g.select(context.params.videoNumber-1, 0).then(function() {
                                                focusMgr.focus(view.find(cell));
                                            });
                                        }
                                    }
                                    // otherwise lets just select the first grid
                                    else {
                                        if (g.getId() === "episodeGrid1") {
                                            focusMgr.focus(g);
                                        }
                                    }
                                    */
                                    if (g.getId() === selGrid) {
                                        focusMgr.focus(g);
                                    }
                                };
                            }(episodeGrid)),
                            // if ds loading failed
                            function() {
                                // we just take care of our special case
                                // eg when loading ds from special index and it fails
                                // this only raise if redbull API is not correct, not it should normally not happens
                                if (!self.__target) {
                                    return;
                                }
                                // we're gonna try to load the ds again, but from the normal index
                                self.__target._ds = null;
                                self.__target.setPrereadyStgy(function(state) {
                                    state.selectedDsIndex = 0;
                                    state.selectedGridRow = 0;
                                });
                                self.__target.setDatasource(self.__targetDs).then(function() {
                                    focusMgr.focus(self.__target);
                                });
                            });
                        }


                    } //if
                }); //redbullapi end
            },

            show: function() {
                sTracking.trackPage(this.__context.params);
            },

            hide: function() {},

            deinit: function() {
                this.hide();
            }

        });
    });

define("app/tmpl/PlayerTmpl", [
        "ax/af/Container",
        "ax/ext/ui/Layout",
        "ax/ext/ui/Label",
        "ax/ext/ui/grid/BoundedGrid",
        "ax/ext/ui/grid/ScrollingGrid",
        "ax/ext/ui/ProgressBar",

        "app/widget/ImageButton",
        "app/widget/Footer",
        "app/widget/RedBullCountdown"
    ],
    function(Container, Layout, Label, BoundedGrid, ScrollingGrid, ProgressBar,
        ImageButton, Footer, RedBullCountdown) {
        return function() {
            return {
                klass: Container,
                id: "#player-root",
                forwardFocus: true,
                autoNavigation: true,
                children: [{
                    klass: Container,
                    id: "#captions-container",
                    children: []
                },{
                    klass: Container,
                    id: "#player-container",
                    forwardFocus: true,
                    autoNavigation: true,
                    children: [{
                        klass: Container,
                        id: "overlay-opacity",
                        css: "overlay-gradient"
                    },{
                        klass: Container,
                        id: "#player-live-area",
                        forwardFocus: true,
                        children: [
                            {
                                klass: Container,
                                id: "#player-live-area-thanks",
                                children: [
                                    {
                                        klass: Container,
                                        css: "live-area-logo"
                                    },{
                                        klass: Label,
                                        text: "Thanks for watching!"
                                    }
                                ]
                            },
                            {
                                klass: RedBullCountdown,
                                id: "#player-live-area-countdown",
                                title: "LIVE IN"
                            },
                            {
                                klass: Container,
                                id: "#player-live-area-paused",
                                children: [
                                    {
                                        klass: Layout,
                                        alignment: Layout.HORIZONTAL,
                                        children: [
                                            {
                                                klass: Container,
                                                css: "icon-live-area-pause"
                                            }, {
                                                klass: Label,
                                                text: "This event is paused."
                                            }
                                        ]
                                    },{
                                        klass: Label,
                                        text: "We're working hard on bringing it back up. Please hold the line for a second..."
                                    }
                                ]
                            }
                        ]
                    },{
                        klass: Layout,
                        id: "#player-top",
                        width: 2,
                        alignment: Layout.HORIZONTAL,
                        forwardFocus: true,
                        nextDown: "player-controls",
                        children: [{
                            klass: Container,
                            id: "#player-meta",
                            forwardFocus: true,
                            children: [{
                                klass: Label,
                                id: "#videoCat",
                                text: ""
                            }, {
                                klass: Label,
                                id: "#videoTitle",
                                text: ""
                            }, {
                                klass: Label,
                                id: "#videoDesc",
                                text: ""
                            }, {
                                klass: Label,
                                id: "#videoDate",
                                text: ""
                            }]
                        }, {
                            klass: Container,
                            id: "#player-grid",
                            forwardFocus: true,
                            children: [{
                                klass: Label,
                                id: "#mightLikeLabel",
                                text: "YOU MIGHT ALSO LIKE"
                            }, {
                                klass: BoundedGrid,
                                id: "#player-related-videos",
                                css: "borderGradient",
                                rows: 3,
                                cols: 1,
                                alignment: ScrollingGrid.VERTICAL,
                                nextLeft: "player-controls",
                                nextUp: null
                            }]
                        }]
                    }, {
                        klass: Container,
                        id: "#player-controls",
                        autoNavigation: true,
                        forwardFocus: true,
                        nextDown: "player-footer-wrapper",
                        children: [{
                            klass: Layout,
                            id: "#player-buttons",
                            width: 4,
                            alignment: Layout.HORIZONTAL,
                            forwardFocus: true,
                            defaultFocus: "playButton",
                            nextTop: "player-related-videos",
                            children: [{
                                klass: ImageButton,
                                id: "rewindButton",
                                css: "rewind-video",
                                nextLeft: "rewindButton",
                                nextUp: "player-grid",
                                nextDown: "player-footer-wrapper"
                            }, {
                                klass: ImageButton,
                                id: "playButton",
                                css: "play-video",
                                nextUp: "player-grid",
                                nextDown: "player-footer-wrapper"
                            }, {
                                klass: ImageButton,
                                id: "pauseButton",
                                css: "stop-video",
                                nextUp: "player-grid",
                                nextDown: "player-footer-wrapper"
                            }, {
                                klass: ImageButton,
                                id: "forwardButton",
                                css: "forward-video",
                                nextRight: "player-related-videos",
                                nextUp: "player-grid",
                                nextDown: "player-footer-wrapper"
                            }]
                        }, {
                            klass: Layout,
                            width: 4,
                            id: "#player-timeline",
                            alignment: Layout.HORIZONTAL,
                            forwardFocus: true,
                            children: [{
                                klass: Label,
                                id: "timePassed",
                                css: "text-align-right",
                                text: "00:00"
                            }, {
                                klass: ProgressBar,
                                id: "#timeline"
                            }, {
                                klass: Label,
                                id: "timeRemaining",
                                css: "text-align-left",
                                text: "00:00"
                            }]
                        }]
                    }, {
                        klass: Footer,
                        id: "player-footer-wrapper",
                        css: "player-footer-wrapper",
                        nextUp: "player-buttons",
                        autoNavigation: true,
                        forwardFocus: true
                    }]
                },
                {
                    klass: Container,
                    id: "#liveLogo"
                },
                {
                    klass: Container,
                    id: "focus-trap",
                    focusable: true,
                    nextLeft: "focus-trap",
                    nextRight: "focus-trap",
                    nextUp: "focus-trap",
                    nextDown: "focus-trap"
                }]
            };
        };
    });

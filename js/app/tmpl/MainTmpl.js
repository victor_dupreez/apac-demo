define("app/tmpl/MainTmpl", [
        "ax/af/Container",
        "ax/ext/ui/Layout",

        "app/widget/ImageButton"
    ],
    function(Container, Layout, ImageButton) {
        return function() {

            return {
                klass: Container,
                forwardFocus: true,
                id: "main-view",
                css: "menu-min",
                children: [{
                    klass: Layout,
                    width: 1,
                    id: "#menu",
                    forwardFocus: true,
                    nextRight: "page",
                    children: [{
                        klass: ImageButton,
                        text: "Search",
                        css: "icon-search",
                        id: "menu-search",
                        nextDown: "menu-main"
                    }]
                }, {
                    klass: Layout,
                    width: 1,
                    id: "#page",
                    alignment: Layout.HORIZONTAL,
                    forwardFocus: true,
                    children: [{
                        klass: Container,
                        id: "#pageContainer",
                        nextUp: "menu",
                        nextLeft: "menu",
                        forwardFocus: true,
                        children: []
                    }]
                }]
            };

        };

    });

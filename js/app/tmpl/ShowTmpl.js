define("app/tmpl/ShowTmpl", ["ax/af/Container", "ax/ext/ui/Layout", "app/widget/RedBullScrollableContainer"],
    function(Container, Layout, RedBullScrollableContainer) {
        return function() {
            return {
                klass: Container,
                id:"#mainContainer",
                children: [{
                    klass: Container,
                    id: "background-holder",
                    children:[{
                        klass: Container,
                        css: "show-gradient-overlay",
                        id: "show-gradient-overlay"
                    }]
                },{
                    klass: RedBullScrollableContainer,
                    id: "main-view",
                    forwardFocus: true,
                    bottomThreshold: 45,
                    topThreshold: 140,
                    children: [{
                        klass: Layout,                        
                        id: "#showLayout",
                        css: "redbull-scroll",
                        forwardFocus: true,
                        width: 1,
                        children: []
                    }]
                }]
            };
        };
    });

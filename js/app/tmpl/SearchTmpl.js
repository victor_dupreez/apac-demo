define("app/tmpl/SearchTmpl", [
        "ax/af/Container",
        "app/widget/Keyboard",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",
        "app/widget/TabGrid"
    ],
    function(Container, Keyboard, Label, Layout, TabGrid) {
        return function() {

            return {
                klass: Container,
                id: "main-view",
                css: "search-view",
                forwardFocus: true,
                children: [{
                        klass: Container,
                        id: "search-view-icon",
                        css: "search-view-icon"
                    }, {
                        klass: Label,
                        id: "search-view-info",
                        css: "search-view-info",
                        text: "What are you looking for?"
                    }, {
                        klass: Container,
                        id: "search-view-scrollable",
                        css: "search-view-scrollable",
                        children:[{
                            klass: Layout,
                            css: "search-view-top-container",
                            children: [{
                                klass: Layout,
                                id: "input-field-container",
                                focusable: true,
                                clickable: true,
                                css: "input-field-container",
                                nextDown: "tab-list",
                                children: [{
                                    klass: Container,
                                    id: "input-field",
                                    css: "search-text-field"
                                }, {
                                    klass: Label,
                                    id: "input-field-text",
                                    css: "search-input-field-text",
                                    text: "Search"
                                }]

                            }]
                        },{
                            klass: Layout,
                            //width: 1,
                            id: "grid-container",
                            css: "search-view-grid-container",
                            forwardFocus: true,
                            children:[{
                                klass: TabGrid,
                                forwardFocus: true,
                                focusable: true,
                                nextUp: "input-field-container",
                                id: "tab-grid",
                                rows: 5,
                                cols:1,
                                css: "search-tabs"
                            }, {
                                klass: Container,
                                id: "search-grid-container",
                                css: "search-grid-container",
                                forwardFocus: true,
                                children:[{
                                    klass: Label,
                                    id: "search-not-found",
                                    css: "search-not-found",
                                    text: "No data available."
                                }]
                            }, {
                                klass: Container,
                                id: "search-grid-gradient",
                                css: "search-grid-gradient"
                            }]
                        }]
                    }, {
                        klass: Keyboard,
                        id: "keyboard"
                    }]
                };
        };
    });

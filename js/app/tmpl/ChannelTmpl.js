define("app/tmpl/ChannelTmpl", [
        "ax/af/Container",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",
        "ax/ext/ui/grid/BoundedGrid",
        "ax/ext/ui/grid/ScrollingGrid",
        "app/widget/AnimatedGrid",
        "app/widget/RedBullScrollableContainer"
    ],
    function(Container, Label, Layout, BoundedGrid, ScrollingGrid, AnimatedGrid, RedBullScrollableContainer) {
        return function() {
            return {
                klass: Container,
                forwardFocus: true,
                children: [{
                    klass: RedBullScrollableContainer,
                    id: "main-view",
                    nextRight: "scrollableContainer-scrollbar",
                    forwardFocus: true,
                    bottomThreshold: 45,
                    topThreshold: 60,
                    children: [{
                        klass: Layout,
                        id: "makeitworkLayout",
                        css: "redbull-scroll",
                        forwardFocus: true,
                        //nextUp:"change-user",
                        width: 1,
                        children: [{
                            klass: BoundedGrid,
                            id: "#featuredgrid",
                            rows: 3,
                            cols: 1,
                            alignment: ScrollingGrid.VERTICAL,
                            nextUp: null,
                            forwardFocus: true,
                            autoNavigation: true
                        }, {
                            klass: Label,
                            id: "#lblChannels",
                            text: "Channels",
                            css: "gridTitle"
                        }, {
                            klass: AnimatedGrid,
                            id: "#loopedgrid2",
                            scrollEndBoundary: 1,
                            displayEndBoundary: 1,
                            alignment: ScrollingGrid.VERTICAL,
                            forwardFocus: true,
                            autoNavigation: true
                        }]
                    }]
                }]
            };
        };
    });

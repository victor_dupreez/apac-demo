define("app/tmpl/SubChannelTmpl", [
        "ax/af/Container",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",
        "ax/ext/ui/grid/BoundedGrid",
        "ax/ext/ui/grid/ScrollingGrid",

        "app/widget/AnimatedGrid",
        "app/widget/RedBullScrollableContainer",
        "app/util/Utils"
    ],
    function(Container, Label, Layout, BoundedGrid, ScrollingGrid, AnimatedGrid, RedBullScrollableContainer, Utils) {
        return function() {
            return {
                klass: Container,
                forwardFocus: true,
                children: [{
                    klass: RedBullScrollableContainer,
                    id: "main-view",
                    nextRight: "scrollableContainer-scrollbar",
                    forwardFocus: true,
                    bottomThreshold: Utils.scaleDimension(45),
                    topThreshold: Utils.scaleDimension(60),
                    children: [{
                        klass: Layout,
                        id: "makeitworkLayout",
                        css: "redbull-scroll",
                        forwardFocus: true,
                        width: 1,
                        children: [{
                            klass: Label,
                            id: "#lblSubChannelTitle",
                            text: "",
                            css: "gridTitle"
                        }, {
                            klass: BoundedGrid,
                            id: "#subChannelFeaturedGrid",
                            css: "featuredGrid borderGradientBig",
                            rows: 6,
                            cols: 1,
                            alignment: ScrollingGrid.VERTICAL,
                            nextUp: null,
                            forwardFocus: true,
                            autoNavigation: true
                        }, {
                            klass: Container,
                            children: [
                                {
                                    klass: Label,
                                    text: "Shows",
                                    css: "gridTitle"
                                }, {
                                    klass: AnimatedGrid,
                                    id: "#showsLoopedGrid",
                                    css: "showGridImage",
                                    alignment: ScrollingGrid.VERTICAL,
                                    scrollEndBoundary: 2,
                                    scrollFrontBoundary: 2,
                                    displayEndBoundary: 1,
                                    displayFrontBoundary: 2,
                                    forwardFocus: true,
                                    autoNavigation: true
                                }
                            ]
                        }, {
                            klass: Container,
                            children: [
                                {
                                    klass: Label,
                                    css: "gridTitle",
                                    text: "Episodes"
                                }, {
                                    klass: AnimatedGrid,
                                    id: "#latestEpisodesLoopedGrid",
                                    css: "latestClipsEpisodeGrid borderGradientBig",
                                    alignment: ScrollingGrid.VERTICAL,
                                    scrollEndBoundary: 2,
                                    scrollFrontBoundary: 2,
                                    displayEndBoundary: 1,
                                    displayFrontBoundary: 2,
                                    forwardFocus: true,
                                    autoNavigation: true
                                }
                            ]
                        }, {
                            klass: Container,
                            children: [
                                {
                                    klass: Label,
                                    css: "gridTitle",
                                    text: "Clips"
                                }, {
                                    klass: AnimatedGrid,
                                    id: "#latestClipsLoopedGrid",
                                    css: "latestClipsEpisodeGrid borderGradientBig",
                                    alignment: ScrollingGrid.VERTICAL,
                                    scrollEndBoundary: 2,
                                    scrollFrontBoundary: 2,
                                    displayEndBoundary: 1,
                                    displayFrontBoundary: 2,
                                    forwardFocus: true,
                                    autoNavigation: true
                                }
                            ]
                        }]
                    }]
                }]
            };
        };
    });

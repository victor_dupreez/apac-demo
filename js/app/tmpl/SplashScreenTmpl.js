define("app/tmpl/SplashScreenTmpl",
[
    "ax/af/Container"
],
function (Container)
{
    "use strict";

    return function()
    {
        return {
            klass: Container,
            id: "splashscreen",
            css: "splashscreen"
        };
    };
});

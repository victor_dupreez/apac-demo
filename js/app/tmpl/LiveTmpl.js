define("app/tmpl/LiveTmpl", [
        "ax/af/Container",
        "ax/ext/ui/Label",
        "ax/ext/ui/Layout",
        "ax/ext/ui/grid/BoundedGrid",
        "ax/ext/ui/grid/ScrollingGrid",
        "app/widget/AnimatedGrid",
        "app/widget/RedBullScrollableContainer"
    ],
    function(Container, Label, Layout, BoundedGrid, ScrollingGrid, AnimatedGrid, RedBullScrollableContainer) {
        return function() {
            return {
                klass: Container,
                forwardFocus: true,
                children: [{
                    klass: RedBullScrollableContainer,
                    id: "main-view",
                    nextRight: "scrollableContainer-scrollbar",
                    forwardFocus: true,
                    bottomThreshold: 45,
                    topThreshold: 60,
                    children: [{
                        klass: Layout,
                        id: "makeitworkLayout",
                        css: "redbull-scroll",
                        forwardFocus: true,
                        width: 1,
                        children: [{
                            klass: BoundedGrid,
                            id: "#featuredgrid",
                            css: "featuredGrid borderGradientBig",
                            rows: 3,
                            cols: 1,
                            alignment: ScrollingGrid.VERTICAL,
                            forwardFocus: true,
                            nextUp: null,
                            autoNavigation: true
                        },{
                            klass: Container,
                            children: [
                                {
                                    klass: Label,
                                    text: "Currently Live",
                                    css: "gridTitle"
                                },{
                                    klass: AnimatedGrid,
                                    id: "#liveEventGrid",
                                    css: "latestClipsEpisodeGrid borderGradientBig",
                                    alignment: ScrollingGrid.VERTICAL,
                                    scrollEndBoundary: 2,
                                    scrollFrontBoundary: 2,
                                    displayEndBoundary: 1,
                                    displayFrontBoundary: 2,
                                    forwardFocus: true,
                                    autoNavigation: true
                                }
                            ]
                        },{
                            klass: Container,
                            children: [
                                {
                                    klass: Label,
                                    text: "Upcoming",
                                    css: "gridTitle"
                                },{
                                    klass: AnimatedGrid,
                                    id: "#upcomingLiveEventGrid",
                                    css: "latestClipsEpisodeGrid borderGradientBig",
                                    alignment: ScrollingGrid.VERTICAL,
                                    scrollEndBoundary: 2,
                                    scrollFrontBoundary: 2,
                                    displayEndBoundary: 1,
                                    displayFrontBoundary: 2,
                                    forwardFocus: true,
                                    autoNavigation: true
                                }
                            ]
                        },{
                            klass: Container,
                            children: [
                                {
                                    klass: Label,
                                    text: "Past Events",
                                    css: "gridTitle"
                                }, {
                                    klass: AnimatedGrid,
                                    id: "#pastLiveEventGrid",
                                    css: "latestClipsEpisodeGrid borderGradientBig",
                                    scrollEndBoundary: 2,
                                    scrollFrontBoundary: 2,
                                    displayEndBoundary: 1,
                                    displayFrontBoundary: 2,
                                    alignment: ScrollingGrid.VERTICAL,
                                    forwardFocus: true,
                                    autoNavigation: true
                                }
                            ]
                        }]
                    }]
                }]
            };
        };
    });

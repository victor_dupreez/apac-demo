define("lib/conviva", ["ax/ax"], function (ax) {
    var conviva = {
        // Prerequisites
        prerequisites:{
            customerId: 'c3.Redbull-Test',
            //customerKey: '92ba176709a3309c876a2443b0ac627de1d1c84a', // testing
            customerKey: '611d530742f809cd6f092653d8f624cd87dd046d', // production
            serviceUrl: 'https://redbull.testonly.conviva.com'
        },

        // References to the current sessionId and streamer
        sessionId: null,
        streamer: null,

        _isSupported: function() {
            switch(ax.platform) {
                case "samsung": return true;
            }
            return false;
        },

        init: function() {
            if (!this._isSupported()) return false;
            // One-time initialization call: must happen before any other Conviva calls
            console.log("*** CONVIVA init ***");
            LoadLivePass();

            var nnavi = document.getElementById("pluginObjectNNavi");

            var samsungApi = new Conviva.SamsungApi(nnavi);

            console.log("*** CONVIVA init *** customerKey=" + this.prerequisites.customerKey);
            console.log("*** CONVIVA init *** samsungApi=" + samsungApi);

            Conviva.LivePass.initWithSettings(
                this.prerequisites.customerKey, { platformApi : samsungApi}
                //this.prerequisites.customerKey, { platformApi : samsungApi, gatewayUrl : this.prerequisites.serviceUrl }
            );

            console.log("*** CONVIVA init done ***");
        },
        // As soon as the user clicks play (even before ads)
        //When a video asset is selected for initial playback (either by the user explicitly or via an auto-play logic)
        // a Conviva monitoring session must be created with its corresponding metadata and a Conviva.SamsungStreamer object.
        createSession: function(videoData) {
            if (!this._isSupported()) return false;
            console.log("*** CONVIVA createSession *** = " + videoData.url);
            // Clean up previous session
            this.cleanUpSession();

            /*
            * {"assetUrl":"http://ais.channel4.com/asset/3757709?client=samsung","assetId":"3757709","drm":"widevine","portalId":"rbmch4tv","url":"http://ak.samsung.c4assets.com/samsung/CH4_32_02_29_57812003001001_001.wvm","licenseUrl":"https://widevine.c4.aws.redbeemedia.com/license","responseTimeEpoch":1412348665909,"userData":"token=NbetqcQaShoLo86Pzf6NEWGPtWpc7k8zYpe6iBXxKPKtK8uCBrgGsLkCNK7GRjI3pzlIpUgY3dcoa3amDtEwSZekWhQh3lars5zqQcvZ","meta":{"brandTitle":"Educating the East End","brandWebSafeTitle":"educating-the-east-end","contractId":"57812","episodeTitle":"Educating the East End","programmeNumber":"3","duration":2804,"ageRating":16,"isCatchUp":true},"freewheel":{"breaks":[0,705,1485,2282],"duration":2804,"brandWebSafeTitle":"educating-the-east-end","fwServer":"http://2a7e9.v.fwmrm.net","fwNetworkId":"174057","fwProfile":"174057:c4_samsung_live","fwAssetId":"57812/003","fwAdManagerLocation":"http://2a7e9.v.fwmrm.net/ad/p/1","fwLogLevel":"QUIET","fwCacheBuster":"1","siteSectionId":"samsung.channel4.com/4od","networkid":"174057","serverurl":"http://2a7e9.v.fwmrm.net/ad/p/1","profile":"174057:c4_samsung_live","sitesectionid":"samsung.channel4.com/4od"}}
            * */

            console.log("---------------------------------------");
            console.log(videoData);
            console.log("---------------------------------------");

            if (videoData) {
                console.log("---------------------------------------sient amic");

                //E.g.: "[MI201405210081] Red Bull Drift Shifters - Auckland, New Zealand"
                var video = videoData.videoObj;
                var assetName = '[' + video.id + '] ' + video.subtitle + ' - ' + video.title;
                var tags = {};
                //E.g.: "[MI201405210081] Red Bull Drift Shifters - Auckland, New Zealand"
                console.log("- - - - -assetname - "+assetName + " ");
                var convivaMetadata = new Conviva.ConvivaContentInfo(assetName, tags);
                //One of the enumerated CDN values where your content is served from
                convivaMetadata.defaultReportingCdnName = Conviva.ConvivaContentInfo.CDN_NAME_OTHER;
                convivaMetadata.streamUrl = videoData.url;
                convivaMetadata.isLive = videoData.liveEvent;

                convivaMetadata.playerName = "Redbull_Samsung_TV_App";
                //convivaMetadata.viewerId = C4.data.getUniqueUserId();// IDK
                console.log(convivaMetadata);
                this.streamer = null;
                console.log("*** CONVIVA createSession *** streamer = " + this.streamer);
                console.log("*** *** convivaMetadata = " + convivaMetadata);
                this.sessionId = Conviva.LivePass.createSession(this.streamer, convivaMetadata);
                console.log("*** *** sessionId = " + this.sessionId);
                this.attachStreamer();
            }
        },

        // call just before main content starts, after ads
        attachStreamer: function() {
            if (!this._isSupported()) return false;
            console.log("*** CONVIVA attachStreamer ***");
            var player = document.getElementById('infolinkPlayer');
            this.streamer  = new Conviva.SamsungStreamer(player);
            console.log("+ + + + + + " +this.streamer+ " - - - - - - "+this.sessionId);
            Conviva.LivePass.attachStreamer(this.sessionId, this.streamer);
        },

        // call this when video is interrupted by midrolls and after midrolls call attachStreamer
        detachStreamer: function() {
            if (!this._isSupported()) return false;
            console.log("*** CONVIVA detachStreamer ***");
            Conviva.LivePass.detachStreamer(this.sessionId);
        },

        // When the stream is done with playback
        cleanUpSession: function() {
            if (!this._isSupported()) return false;
            console.log("*** CONVIVA cleanUpSession ***");
            if (this.sessionId != null) {
                Conviva.LivePass.cleanupSession(this.sessionId);
                this.sessionId = null;
                this.streamer = null;
            }
        },

        reportError: function(errorString) {
            if (!this._isSupported()) return false;
            console.log("*** CONVIVA reportError = " + errorString + " ***");
            Conviva.LivePass.reportError(this.sessionId, errorString);
        },

        // when and ad starts
        adStart: function() {
            if (!this._isSupported()) return false;
            console.log("*** CONVIVA adStart ***");
            if (this.sessionId != null) {
                Conviva.LivePass.adStart(this.sessionId);
            }
        },

        // when an ad ends
        adEnd: function() {
            if (!this._isSupported()) return false;
            console.log("*** CONVIVA adEnd ***");
            if (this.sessionId != null) {
                Conviva.LivePass.adEnd(this.sessionId);
            }
        }
    };
    return conviva;
});
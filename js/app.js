require(["ax/Env",
    "ax/config",
    "ax/af/mvc/AppRoot",
    "ax/console",
    "ax/device",
    "ax/util",
    "ax/promise",
    "ax/device/interface/System",
    "ax/ext/mouse/mouseHandler",
    "app/ctrl/SplashScreenCtrl",
    "app/util/Utils",

    "lib/conviva"
], function(Env, config, AppRoot, console, device, util,
    promise, ISystem, mouseHandler, SplashScreenCtrl, appUtils, conviva) {

    console.log("All module loaded!");

    var sEnv = Env.singleton(),
        sAppRoot = AppRoot.singleton(),
        agPromise = promise.defer();

    // Must be in a timeout because the device package has not always initialized at this point
    // for some reason.
    function __onDevicePackageReady() {
        /**
         * AppGrid
         * - Initialize once sEnv is loaded
         */
        try {
            __initAppGrid().then(function(AppGrid) {
                return promise.all([
                    promise.resolve(AppGrid),
                    AppGrid.applicationStart()
                ]);
            }).then(function success(result) {
                // Appgrid responded and working

                // var response = result[1];

                // Resolve AppGrid promise with AppGrid instance
                agPromise.resolve(result[0]);
            }, function fail(reason) {
                console.error("Could not initialize AppGrid: " + reason);
                agPromise.reject(reason);
            });
        } catch (e) {
            console.error("Exception initiating app grid: " + e);
            agPromise.reject(e);
        }

        // Conviva
        conviva.init();
    }


    /**
     * Initialize AppGrid
     * @private
     * @return {ax/promise~Promise} - To be resolved with AppGrid instance
     */
    function __initAppGrid() {

        var appGridUrl = config.get("appGrid.url"),
            apiKey = config.get("appGrid.apiKey"),
            // device.id is only available after EVT_ONLOAD
            uid = device.id.getUniqueID(),

            appGridPath = "tve/AppGridService",

            deferred = promise.defer();

        if (device.id.getFirmwareYear() === 2011) {
            // At this point, including appgrid on a samsung 2011 TV causes the app to not start.
            // AppGrid will not be used for samsung 2011.
            deferred.reject("AppGrid does not work on 2011");
        } else if (!util.isString(apiKey)) {
            deferred.reject("No apiKey for AppGridService");
        } else if (!util.isString(appGridUrl)) {
            deferred.reject("No appGridUrl for AppGridService");
        } else if (util.isUndefined(uid)) {
            deferred.reject("No uid for AppGridService");
        } else {

            // Include AppGrid here to allow same code to run on 2011
            require([appGridPath], function success(AppGridService) {
                try {
                    // Resolve with AppGrid instance
                    deferred.resolve(new AppGridService(appGridUrl, apiKey, uid));
                } catch (e) {
                    deferred.reject(e);
                }
            }, function fail() {
                deferred.reject("Failed to load " + appGridPath);
            });
        }

        return deferred.promise;
    }


    var dpInterval = setInterval(function() {
        if (device && device.platform) {
            clearInterval(dpInterval);
            __onDevicePackageReady();
        }
    }, 1000);

    var _onNetworkStatusChanged = function(evt){
        console.log("internet connection change");
        if (!evt) {
            console.log("internet connection lost");
            appUtils.notification.add({
                type: appUtils.notification.options.type.NETWORKERROR,
                text: "We're experiencing network difficulties right now. The application will now close. Please try again later. APP",
                buttons: [{
                    label: "EXIT",
                    action: function() {
                        device.system.exit({
                            toTV: true
                        });
                    }
                }]
            });
        } else {
            // Network came back, kill the network error popup
            console.log("internet connection gain");
           // inetWorking = true;
            appUtils.notification.hideCurrent();
        }
    };

    sEnv.addEventListener(sEnv.EVT_ONLOAD, function() {
        device.system.addEventListener(ISystem.EVT_NETWORK_STATUS_CHANGED, _onNetworkStatusChanged);
        mouseHandler.start();
        sAppRoot.setMainController(SplashScreenCtrl);
    });
});
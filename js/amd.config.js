require({
    "baseUrl": "js",
    "paths": {

        "ax/device": "hotfix/device",
        "ax/Env": "hotfix/Env",
        "ax/ext/device/tizen/DevicePackage": "hotfix/DevicePackage",
        "ax/ext/device/tizen/System": "hotfix/System",
        "ax/interface/DeviceHandler": "hotfix/DeviceHandler",

        // Implement multitasking
        "ax/ext/device/tizen/Html5Player": "hotfix/Html5Player",
        "ax/ext/device/tizen/AVPlayer": "hotfix/AVPlayer",

        //RedBull project
		"app/widget/RedBullGridItem": "app/widget/RedBullGridItem",
        "app/widget/ImageButton": "app/widget/ImageButton",
        "app/widget/sLoader": "app/widget/sLoader",
        "app/widget/Loader": "app/widget/Loader",
        "app/widget/Keyboard": "app/widget/Keyboard",
        "app/widget/TabGrid": "app/widget/TabGrid",
        "app/widget/AnimatedGrid": "app/widget/AnimatedGrid",
        "app/widget/RedBullScrollableContainer": "app/widget/RedBullScrollableContainer",

        // PS module stuff
        "ps/util/Polyfill": "../ps/tv.accedo.ps.util-polyfill/js/ps/util/Polyfill",
        "ps/util/cssUtil": "../ps/tv.accedo.ps.util-cssUtil/js/ps/util/cssUtil",
        "ps/ui/Captioning": "../ps/tv.accedo.ps.ui-captioning/js/ps/ui/Captioning",
        "ps/ui/TTMLCaptioning": "../ps/tv.accedo.ps.ui-captioning/js/ps/ui/TTMLCaptioning",
        "ps/ui/Notification": "../ps/tv.accedo.ps.ui-notification/js/ps/ui/Notification",
        "ps/util/ImagePreloader": "../ps/tv.accedo.ps.util-imagepreloader/js/ps/util/ImagePreloader",

        // Conviva module
        "lib/conviva": "lib/conviva/samsung"
    }
});

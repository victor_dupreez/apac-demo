/**
 * LinearContentService Interface
 * @name LinearContentService
 * @memberof tve
 * @class tve/interface/LinearContentService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/interface/LinearContentService", ["ax/Interface"], function(Interface) {
    return Interface.create("LinearContentService", {
        /**
         * Get all Channels
         * @method getAllChannels
         * @public
         * @param {Object} opts The options object
         * @param {String} [opts.sortBy] sort by id/channelNumber/title
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/LinearContentService#
         */
        getAllChannels: ["opts"],
        /**
         * Get Channel by Id
         * @method getChannelById
         * @public
         * @param {String} id ChannelID
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/channelNumber/title
         * @returns {Promise.<tve/model/Channel>} Channel object
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/LinearContentService#
         */
        getChannelById: ["id", "opts"],
        /**
         * Get Channels by Ids
         * @method getChannelsByIds
         * @public
         * @param {String[]} ids ChannelIDs
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/channelNumber/title
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/LinearContentService#
         */
        getChannelsByIds: ["ids", "opts"],
        /**
         * Get Channels by Category
         * @method getChannelsByCategoryId
         * @public
         * @param {String} categoryId CategoryID
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/channelNumber/title
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/LinearContentService#
         */
        getChannelsByCategoryId: ["categoryId", "opts"],
        /**
         * Get all TVListings
         * @method getAllTVListings
         * @public
         * @param {String} startTime startTime
         * @param {String} endTime endTime
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/channelId
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/LinearContentService#
         */
        getAllTVListings: ["startTime", "endTime", "opts"],
        /**
         * Get TVListing by channel Id
         * @method getTVListingByChannelId
         * @public
         * @param {String} channelId ChannelID
         * @param {String} startTime startTime
         * @param {String} endTime endTime
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/channelId
         * @returns {Promise.<tve/model/TVListing>} TVListing object
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/LinearContentService#
         */
        getTVListingByChannelId: ["channelId", "startTime", "endTime", "opts"],
        /**
         * Get TVListing by channel Ids
         * @method getTVListingsByChannelIds
         * @public
         * @param {String[]} channelIds ChannelIDs
         * @param {String} startTime startTime
         * @param {String} endTime endTime
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/channelId
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/LinearContentService#
         */
        getTVListingsByChannelIds: ["channelIds", "startTime", "endTime", "opts"]
    });
});
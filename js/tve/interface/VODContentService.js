/**
 * VODContentService Interface
 * @name VODContentService
 * @memberof tve
 * @class tve/interface/VODContentService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/interface/VODContentService", ["ax/Interface"], function(Interface) {
    return Interface.create("VODContentService", {
        /**
         * Get all Movies
         * @method getAllMovies
         * @public
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/VODContentService#
         */
        getAllMovies: ["opts"],
        /**
         * Get Movie by Id
         * @method getMovieById
         * @public
         * @param {String} id MovieID
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
         * @returns {Promise.<tve/model/Asset>} Asset object
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/VODContentService#
         */
        getMovieById: ["id", "opts"],
        /**
         * Get Movies by Ids
         * @method getMoviesByIds
         * @public
         * @param {String[]} ids MovieIDs
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/VODContentService#
         */
        getMoviesByIds: ["ids", "opts"],
        /**
         * Get Movies by category
         * @method getMoviesByCategoryId
         * @public
         * @param {String} categoryId CategoryID
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/VODContentService#
         */
        getMoviesByCategoryId: ["categoryId", "opts"],
        /**
         * Get all TV Shows
         * @method getAllTVShows
         * @public
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
         * @param {String} [opts.showEpisodes] flag to show episodes in return
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/VODContentService#
         */
        getAllTVShows: ["opts"],
        /**
         * Get TV Show by Id
         * @method getTVShowById
         * @public
         * @param {String} id TVShowID
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
         * @param {String} [opts.showEpisodes] flag to show episodes in return
         * @returns {Promise.<tve/model/Asset>} Asset object
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/VODContentService#
         */
        getTVShowById: ["id", "opts"],
        /**
         * Get TV Shows by Ids
         * @method getTVShowsByIds
         * @public
         * @param {String[]} ids TVShowIDs
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
         * @param {String} [opts.showEpisodes] flag to show episodes in return
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/VODContentService#
         */
        getTVShowsByIds: ["ids", "opts"],
        /**
         * Get TV Shows by category
         * @method getTVShowsByCategoryId
         * @public
         * @param {String} categoryId CategoryID
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
         * @param {String} [opts.showEpisodes] flag to show episodes in return
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/VODContentService#
         */
        getTVShowsByCategoryId: ["categoryId", "opts"],
        /**
         * Get all Episodes
         * @method getAllEpisodes
         * @public
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/VODContentService#
         */
        getAllEpisodes: ["opts"],
        /**
         * Get Episodes by Id
         * @method getEpisodeById
         * @public
         * @param {String} id EpisodeID
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
         * @returns {Promise.<tve/model/Asset>} Asset object
         * @throws {Promise.<tve/TVEError>} Internal error object
         * @memberof tve/interface/VODContentService#
         */
        getEpisodeById: ["id", "opts"],
        /**
         * Get Episode by TVShow Id
         * @method getEpisodeByTVShowId
         * @public
         * @param {String} showId showID
         * @param {Object} [opts] The options object
         * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
         * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
         * @throws {Promise.<tve/TVEError>} Internal error object 
         * @memberof tve/interface/VODContentService#
         */
        getEpisodesByTVShowId: ["showId", "opts"]

    });
});
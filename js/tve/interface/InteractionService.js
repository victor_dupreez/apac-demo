/**
 * InteractionService Interface
 * @name InteractionService
 * @memberof tve
 * @class tve/interface/InteractionService
 * @author Joe Roth <joe.roth@accedo.tv>
 */
/*jslint nomen: true */
/*global define: true, setTimeout: true*/
define("tve/interface/InteractionService", ["ax/Interface"], function (Interface) {
    "use strict";
    var InteractionService,
        UNPAIRED,
        PAIRED_DISCONNECTED,
        PAIRED_CONNECTED;

    InteractionService = Interface.create("InteractionService", {
        /**
         * Set the role of the device.  
         * A device can either be a host or a client. The host hosts the pairing, while the client joins a pairing hosted by other device.
         * @method setRole
         * @public
         * @param {ROLE} role the role of the device
         * @memberOf tve/interface/InteractionService#
         */
        setDeviceRole: ["role"],

        /**
         * Sets callback function which is called everytime the Interaction service state changes
         * @method setStateCallback
         * @public
         * @param {requestCallback} cb callback function which will receive state change information
         * @memberOf tve/interface/InteractionService#
         */
        setStateCallback: ["cb"],

        /**
         * Sets callback function which is called everytime the Interaction service receives a command
         * @method setReceivedCommandCallback
         * @public
         * @param {requestCallback} cb callback function which will receive command information
         * @memberOf tve/interface/InteractionService#
         */
        setReceivedCommandCallback: ["cb"],

        /**
         * Gets current Interaction State (may take up to 2 seconds to process)
         * @method getState
         * @returns {Promise.<State>} one of the state listed in this.state
         * @throws {Promise.<tve/TVEError>} the reason why it cannot get state
         * @memberOf tve/interface/InteractionService#
         * @public
         */
        getState: [],

        /**
         * Creates new pair, pairing code is returned with the resolved promise
         * @method createPair
         * @returns {Promise.<String>} the 4-digit pairing code
         * @throws {Promise.<tve/TVEError>} the reason why it cannot create pair
         * @memberOf tve/interface/InteractionService#
         * @public
         */
        createPair: [],

        /**
         * Pair with the host device using the specified pairing code.  
         * @method pairWithCode
         * @param {String} code the pairing code from host device
         * @returns {Promise} no actual return value
         * @throws {Promise.<tve/TVEError>} the reason why it cannot pair
         * @memberOf tve/interface/InteractionService#
         */
        pairWithCode: ["code"],

        /**
         * Unpairs device, promise is rejected if you are currently unpaired
         * @method unpair
         * @returns {Promise} no actual return value
         * @throws {Promise.<tve/TVEError>} the reason why it cannot unpair
         * @memberOf tve/interface/InteractionService#
         * @public
         */
        unpair: [],

        /**
         * Kick all device, promise is rejected if the library hasn't been initialized.  
         * @method resetPair
         * @returns {Promise} no actual return value
         * @throws {Promise.<tve/TVEError>} the reason why it cannot reset
         * @memberOf tve/AccedoConnectService#
         * @public
         */
        resetPair: [],

        /**
         * Send command to companion device
         *  - must have type attribute
         *  - serialization of command must be less than MAX_MESSAGE_SIZE bytes
         *  - must be in state 3
         * @method sendCommand
         * @public
         * @param {Object} obj command object to send to companion device
         * @param {String} [obj.type] type of command 
         * @returns {Promise} no actual return value
         * @throws {Promise.<tve/TVEError>} the reason why it cannot send command
         * @memberOf tve/interface/InteractionService#
         */
        sendCommand: ["obj"]
    });

    /**
     * Connect state
     * @public
     * @typedef {Object} State
     * @property {String} value the value of the state
     * @property {String} description the description of the state
     * @memberOf tve/interface/InteractionService#
     */
    UNPAIRED = {
        value: 1,
        description: "Unpaired"
    };
    PAIRED_DISCONNECTED = {
        value: 2,
        description: "Paired, but companion device is not online"
    };
    PAIRED_CONNECTED = {
        value: 3,
        description: "Paired and connected to companion device"
    };

    /**
     * Enumeration of states
     * @public
     * @typedef {Object} STATE
     * @property {State} UNPAIRED unpaired state
     * @property {State} PAIRED_DISCONNECTED paired, but the companion isn't online
     * @property {State} PAIRED_CONNECTED paired, and the companion is online
     * @memberof tve/interface/InteractionService#
     */
    InteractionService.STATE = {
        UNPAIRED: UNPAIRED,
        PAIRED_DISCONNECTED: PAIRED_DISCONNECTED,
        PAIRED_CONNECTED: PAIRED_CONNECTED
    };

    /**
     * Enumeration of roles
     * @typedef {Object} ROLE
     * @property {Number} HOST integer value representing host
     * @property {Number} CLIENT integer value representing client
     * @pulibc
     * @memberOf tve/interface/InteractionService#
     */
    InteractionService.ROLE = {
        HOST: 1,
        CLIENT: 2
    };

    return InteractionService;
});

/**
 * AnalyticsService Interface
 * @name LogService
 * @memberof tve
 * @class tve/interface/LogService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/interface/LogService", ["ax/Interface"], function(Interface) {
    return Interface.create("LogService", {
        /**
         * Get the level of the log service.
         * @method getLevel
         * @public
         * @returns {Promise.<String>} Returns log level for application. 'error', 'warn', 'debug' or 'off' if log disabled
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/LogService#
         */
        getLevel: [],
        /**
         * Send a debugging log message to log service
         * @method debug
         * @public
         * @param {String} message the message to log
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/LogService#
         */
        debug: ["message"],
        /**
         * Send a warning log message to log service
         * @method warn
         * @public
         * @param {String} message the message to log
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/LogService#
         */
        warn: ["message"],
        /**
         * Send a error log message to log service
         * @method error
         * @public
         * @param {String} message the message to log
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/LogService#
         */
        error: ["message"]
    });
});
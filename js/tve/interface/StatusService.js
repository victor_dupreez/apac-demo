/**
 * StatusService Interface
 * @name StatusService
 * @memberof tve
 * @class tve/interface/StatusService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/interface/StatusService", ["ax/Interface"], function(Interface) {
    return Interface.create("StatusService", {
        /**
         * Get the status of the app to check if the app is in maintaince mode.
         * @method getStatus
         * @public
         * @returns {Promise.<String>} Status (e.g. Active)
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/StatusService#
         */
        getStatus: [],
        /**
         * Get the message of the app from AppGrid.
         * @method getMessage
         * @public
         * @returns {Promise.<String>} Status Message
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/StatusService#
         */
        getMessage: []
    });
});
/**
 * ResourceService Interface
 * @name ResourceService
 * @memberof tve
 * @class tve/interface/ResourceService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/interface/ResourceService", ["ax/Interface"], function(Interface) {
    return Interface.create("ResourceService", {
        /**
         * Get the resource on AppGrid by the given key.
         * @method getResource
         * @public
         * @param {String} key
         * @returns {Promise.<String>} Resource value by key
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/ResourceService#
         */
        getResource: ["key"],
        /**
         * Get all the resources on AppGrid.
         * @method getAllResources
         * @public
         * @returns {Promise.<Object>} Resource values map by keys
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/ResourceService#
         */
        getAllResources: []
    });
});
/**
 * AnalyticsService Interface
 * @name AnalyticsService
 * @memberof tve
 * @class tve/interface/AnalyticsService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/interface/AnalyticsService", ["ax/Interface"], function(Interface) {
    return Interface.create("AnalyticsService", {
        /**
         * Send the application start event log to analytics service on AppGrid
         * @method applicationStart
         * @public
         * @returns {Promise.<Undefined>} fulfilled with no return value
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/AnalyticsService#
         */
        applicationStart: [],
        /**
         * Send the application stop event log to analytics service on AppGrid
         * @method applicationStop
         * @public
         * @returns {Promise.<Undefined>} fulfilled with no return value
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/AnalyticsService#
         */
        applicationStop: []
    });
});
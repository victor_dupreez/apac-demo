/**
 * ConfigurationService Interface
 * @name ConfigurationService
 * @memberof tve
 * @class tve/interface/ConfigurationService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/interface/ConfigurationService", ["ax/Interface"], function(Interface) {
    return Interface.create("ConfigurationService", {
        /**
         * Get the config in app config file on AppGrid by the given key.
         * @method getConfig
         * @public
         * @param {String} key
         * @param {String} defaultValue (optional) it fails to get the data, it will return the default value
         * @returns {Promise.<String>} Configuration value by key
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/ConfigurationService#
         */
        getConfig: ["key", "defaultValue"],
        /**
         * Get the configs in app config file on AppGrid by the given keys.
         * @method getConfigs
         * @public
         * @param {Array} keys
         * @param {Object} defaultValues (optional) it fails to get the data, it will return the default values
         * If it only gets parts of the data, the new data will merge into default values and return the merged data.
         * @returns {Promise.<Object>} Configuration value map by keys
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/ConfigurationService#
         */
        getConfigs: ["keys", "defaultValues"],
        /**
         * Get all the config in app config file on AppGrid.
         * @method getAllConfigs
         * @public
         * @returns {Promise.<Object>} Configuration value map by keys
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/ConfigurationService#
         */
        getAllConfigs: []
    });
});
/**
 * UserSettingsService Interface
 * @name UserSettingsService
 * @memberof tve
 * @class tve/interface/UserSettingsService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/interface/UserSettingsService", ["ax/Interface"], function(Interface) {
    return Interface.create("UserSettingsService", {
        /**
         * Get the setting of the user in an application scope on AppGrid by the given key.
         * @method getSetting
         * @public
         * @param {String} key the key of the requested object
         * @param {String} [defaultValue] (optional) if it fails to get the data, it will return the default values
         * @returns {Promise.<String>} User setting value by key
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        getSetting: ["key", "defaultValue"],
        /**
         * Get the setting of the user in a group scope on AppGrid by the given key.
         * @method getSharedSetting
         * @public
         * @param {String} key the key of the requested object
         * @param {String} [defaultValue] (optional) if it fails to get the data, it will return the default values
         * @returns {Promise.<String>} User group setting value by key
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        getSharedSetting: ["key", "defaultValue"],
        /**
         * Get more the one settings of the user in an application scope on AppGrid by given keys.
         * @method getSettings
         * @public
         * @param {Array} keys the keys of the requested objects
         * @param {Object} [defaultValues] (optional) if it fails to get the data, it will return the default values.
         * If it only gets parts of the data, the new data will merge into default values and return the merged data.
         * @returns {Promise.<Object>} User settings values map by keys
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        getSettings: ["keys", "defaultValues"],
        /**
         * Get more the one settings of the user in a group scope on AppGrid by given keys.
         * @method getSharedSettings
         * @public
         * @param {Array} keys the keys of the requested objects
         * @param {Object} [defaultValues] (optional) if it fails to get the data, it will return the default values.
         * If it only gets parts of the data, the new data will merge into default values and return the merged data.
         * @returns {Promise.<Object>} User group settings values map by keys
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        getSharedSettings: ["keys", "defaultValues"],
        /**
         * Get all the settings of the user in an application scope on AppGrid.
         * @method getAllSettings
         * @public
         * @returns {Promise.<Object>} User settings values map by keys
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        getAllSettings: [],
        /**
         * Get all the settings of the user in a group scope on AppGrid.
         * @method getAllSharedSettings
         * @public
         * @returns {Promise.<Object>} User group settings values map by keys
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        getAllSharedSettings: [],
        /**
         * Set the setting of the user in an application scope on AppGrid by the given key and value.
         * @method setSetting
         * @public
         * @param {String} key the key of the object to be set
         * @param {String} value the value to set to the key
         * @returns {Promise.<Undefined>} fulfilled with no return value
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        setSetting: ["key", "value"],
        /**
         * Set the setting of the user in a group scope on AppGrid by the given key and value.
         * @method getSharedSetting
         * @public
         * @param {String} key the key of the object to be set
         * @param {String} value the value to set to the key
         * @returns {Promise.<Undefined>} fulfilled with no return value
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        setSharedSetting: ["key", "value"],
        /**
         * Set more than one settings of the user in an application scope on AppGrid by the given keys and their values.
         * @method getAllSettings
         * @public
         * @param {Object} object keys and the value to set to the key
         * @returns {Promise.<Undefined>} fulfilled with no return value
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        setAllSettings: ["data"],
        /**
         * Set the setting of the user in a group scope on AppGrid by the given keys and their values.
         * @method getAllSharedSettings
         * @public
         * @param {Object} object keys and the value to set to the key
         * @returns {Promise.<Undefined>} fulfilled with no return value
         * @throws {Promise.<ax/TVEError>} Internal error object
         * @memberof tve/interface/UserSettingsService#
         */
        setAllSharedSettings: ["data"]
    });
});
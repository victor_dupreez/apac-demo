/**
 * The OVP service layer (mockup), implements all OVP related services
 * which contain LinearContentService VODContentService
 * @name AccedoOvpService
 * @memberof tve
 * @class tve/AccedoOvpService
 * @augments tve/interface/LinearContentService
 * @augments tve/interface/VODContentService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/AccedoOvpService", ["ax/ax", "tve/util/CacheHelper", "tve/model/MediaCategory", "tve/model/Asset", "tve/model/TVChannel", "tve/model/TVListing", "tve/model/TVShow", "tve/model/Resource", "tve/TVEError", "tve/interface/VODContentService", "tve/interface/LinearContentService"],
    function (ax, CacheHelper, Category, Asset, TVChannel, TVListing, TVShow, Resource, TVEError, VODContentService, LinearContentService) {

        var FACILITY = TVEError.FACILITY,
            ERROR = TVEError.ERROR,
            SORTING = {
                DESC: "desc",
                ASC: "asc"
            },

            /**
             * private API functions for OVP related service.
             * @member ovpServiceApi
             * @memberof tve/AccedoOvpService#
             * @private
             */
            ovpServiceApi = {
                cacheHelper: null,
                baseURL: "",
                init: function (url, cacheHelper) {
                    this.baseURL = url;
                    this.cacheHelper = cacheHelper;
                },
                categoryRequest: function (opts) {
                    opts = opts || {};


                    var url = this.baseURL + "/category",
                        ids = opts.id,
                        objType = opts.objType,
                        // to be put as ajax parameters, should not be accessed directly from out of module
                        parameters = parameters || {};

                    // by Ids
                    if (ids) {
                        ids = ids.join ? ids.join() : ids;
                        url += "/" + ids;
                        delete opts.id;
                    }

                    if (objType) {
                        url += "/" + objType;
                        delete opts.objType;
                    }

                    return this.request(url, opts, parameters);
                },

                // ==== ILinearContentService ====
                channelRequest: function (opts) {
                    opts = opts || {};

                    var url = this.baseURL + "/channel",
                        ids = opts.id,
                        // to be put as ajax parameters, should not be accessed directly from out of module
                        parameters = parameters || {};

                    // by Ids
                    if (ids) {
                        ids = ids.join ? ids.join() : ids;
                        url += "/" + ids;
                        delete opts.id;
                    }

                    return this.request(url, opts, parameters);
                },
                // a little bit different handling to standard channel
                channelByCategoryRequest: function (opts) {
                    opts = opts || {};

                    if (!opts.id) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided in OVP API"));
                    }
                    ax.util.extend(opts, {
                        objType: "channel"
                    });
                    return this.categoryRequest(opts);
                },
                tvListingRequest: function (opts) {
                    opts = opts || {};

                    var url = this.baseURL + "/tvlisting",
                        ids = opts.channelId,
                        // to be put as ajax parameters, should not be accessed directly from out of module
                        parameters = parameters || {};

                    if (!opts.startTime || !opts.endTime) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no startTime or endTime is provided in OVP API"));
                    }

                    // put parameters
                    parameters.startTime = opts.startTime;
                    parameters.endTime = opts.endTime;

                    // by Ids
                    if (ids) {
                        ids = ids.join ? ids.join() : ids;
                        url += "/" + ids;
                        delete opts.channelId;
                    }

                    return this.request(url, opts, parameters);
                },
                // ==== IVODContentService ====
                movieRequest: function (opts) {
                    opts = opts || {};

                    var url = this.baseURL + "/movie",
                        ids = opts.id,
                        // to be put as ajax parameters, should not be accessed directly from out of module
                        parameters = parameters || {};

                    // by Ids
                    if (ids) {
                        ids = ids.join ? ids.join() : ids;
                        url += "/" + ids;
                        delete opts.id;
                    }

                    return this.request(url, opts, parameters);
                },
                movieByCategoryRequest: function (opts) {
                    opts = opts || {};
                    if (!opts.id) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided in OVP API"));
                    }

                    ax.util.extend(opts, {
                        objType: "movie"
                    });
                    return this.categoryRequest(opts);
                },
                tvShowRequest: function (opts) {
                    opts = opts || {};
                    var url = this.baseURL + "/tvshow",
                        ids = opts.id,
                        objType = opts.objType,
                        // to be put as ajax parameters, should not be accessed directly from out of module
                        parameters = parameters || {};


                    if (ax.util.isBoolean(opts.showEpisodes)) {
                        // put parameters
                        parameters.episodes = opts.showEpisodes;
                    }

                    // by Ids
                    if (ids) {
                        ids = ids.join ? ids.join() : ids;
                        url += "/" + ids;
                        delete opts.id;
                    }

                    if (objType) {
                        url += "/" + objType;
                        delete opts.objType;
                    }

                    return this.request(url, opts, parameters);
                },
                tvShowByCategoryRequest: function (opts) {
                    opts = opts || {};
                    if (!opts.id) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided in OVP API"));
                    }
                    ax.util.extend(opts, {
                        objType: "tvshow"
                    });
                    return this.categoryRequest(opts);
                },
                episodeRequest: function (opts) {
                    opts = opts || {};
                    var url = this.baseURL + "/episode",
                        ids = opts.id,
                        // to be put as ajax parameters, should not be accessed directly from out of module
                        parameters = parameters || {};

                    // by Ids
                    if (ids) {
                        url += "/" + ids;
                        delete opts.id;
                    }

                    return this.request(url, opts, parameters);
                },
                episodeByTVShowRequest: function (opts) {
                    ax.util.extend(opts, {
                        objType: "episode"
                    });
                    return this.tvShowRequest(opts);
                },
                // search 
                searchRequest: function(opts) {
                    opts = opts || {};
                    var url = this.baseURL + "/search",
                        targets = opts.targets,
                        key = opts.key,
                        // to be put as ajax parameters, should not be accessed directly from out of module
                        parameters = parameters || {};

                    if (!targets || !key) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no target type or key is provided in OVP API"));
                    }

                    url += "/" + targets + "/" + key;

                    return this.request(url, opts, parameters);
                },
                request: function(url, opts, param) {
                    opts = opts || {};
                    param = param || {};

                    var __opts,
                        __cacheHelper = this.cacheHelper,
                        cached_key = JSON.stringify({
                            "url": url,
                            "opts": opts,
                            "param": param
                        }),
                        cached_data = __cacheHelper.getCache(cached_key),
                        cached_since = 0;

                    if (!ax.util.isUndefined(cached_data)) {
                        // if not expired, resolve with cache data
                        if (!__cacheHelper.isExpired(cached_key)) {
                            console.info("From Cache");
                            ax.promise.resolve(cached_data);
                        } else {
                            cached_since = __cacheHelper.getTimestamp(cached_key);
                        }
                    }

                    // put sortBy
                    if (opts.sortingKey) {
                        param.sortBy = opts.sortingKey + "|" + (opts.sortingOrder ? opts.sortingOrder : SORTING.DESC);
                    }

                    // put pagination
                    param.pageSize = opts.pageSize || 100;
                    param.pageNumber = opts.pageNumber || 1;



                    __opts = {
                        method: "get",
                        parameters: param
                    };

                    return ax.ajax.request(url, __opts).then(
                        function (transport) {
                            var responseJSON = null;

                            responseJSON = transport.responseJSON || JSON.parse(transport.responseText);
                            // save to cache
                            __cacheHelper.setObject(responseJSON, cached_key, +new Date());
                            return responseJSON;

                        },
                        function (reason) {
                            throw new TVEError(FACILITY.GENERAL, ERROR.NETWORK, reason.transport ? reason.transport.statusText : "network issue", reason);
                        });
                }
            },
            /**
             * parser functions for OVP related service, parse to model objects.
             * @member ovpServiceParser
             * @memberof tve/AccedoOvpService#
             * @private
             */
            ovpServiceParser = (function () {

                var exportObj = {},
                    IMAGES = {
                        THUMBNAIL: "thumbnail",
                        COVER: "cover",
                        BANNER: "banner",
                        LOGO: "logo"
                    }, modelParseFnMap = null;

                function parseResource(json) {

                    var ret = {
                        id: json.id,
                        url: json.url,
                        mimeType: "",
                        language: json.language,
                        duration: json.duration,
                        geoLock: json.geoLock,
                        width: json.width,
                        height: json.height
                    };

                    if (!ret.mimeType && json.type) {
                        ret.mimeType = json.type;
                    }

                    if (!ret.mimeType && json.format) {
                        ret.mimeType = json.format;
                    }

                    return new Resource(ret);
                }
                exportObj.parseResource = parseResource;


                function parseCategory(json) {

                    var ret = {
                        id: json.id,
                        title: json.title,
                        description: json.description,
                        categories: [] // sub-categories
                    };

                    ax.util.each(json.categories, function (category) {
                        ret.categories.push(new Category(category));
                    });

                    return new Category(ret);
                }
                exportObj.parseCategory = parseCategory;


                function parseAsset(json) {

                    var flagCover, flagThumb, flagBanner,
                        videoContent,
                        ret = {
                            type: "",
                            id: json.id,
                            title: json.title,
                            description: json.description,
                            categories: [],
                            images: [],
                            videos: [],
                            parentalRatings: [],
                            credits: {},
                            metadata: {},
                            publishedDate: json.publishedDate,
                            availableDate: json.availableDate
                        };

                    ret.type = json.type ? Asset.TYPE[json.type.toUpperCase()] : 0;

                    ax.util.each(json.categories, function (category) {
                        ret.categories.push(new Category(category));
                    });

                    flagThumb = false;
                    flagCover = false;
                    flagBanner = false;

                    ax.util.each(json.images, function (image) {

                        if (flagThumb && flagBanner && flagCover) {
                            return ax.util.breaker;
                        }


                        if (!flagThumb && ax.util.indexOf(image.type, IMAGES.THUMBNAIL) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.thumbnail$resId = image.id;
                            flagThumb = true;

                            return;
                        }

                        if (!flagCover && ax.util.indexOf(image.type, IMAGES.COVER) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.cover$resId = image.id;
                            flagCover = true;

                            return;
                        }

                        if (!flagBanner && ax.util.indexOf(image.type, IMAGES.BANNER) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.banner$resId = image.id;
                            flagBanner = true;

                            return;
                        }
                    });

                    // AccedoOvp returns 1 video only, we should handle differently if more than 1 video in json
                    ax.util.each(json.videos, function (video) {

                        if (!videoContent) {
                            videoContent = video;
                            return;
                        }

                        if (videoContent && video.width > videoContent.width) {
                            videoContent = video;
                        }

                    });

                    if (videoContent) {

                        ret.videos.push(parseResource(videoContent));

                        ret.metadata.trailer$resId = videoContent.id;
                        ret.metadata.content$resId = videoContent.id;
                    }

                    ret.credits = __arrayToMapHelper(json.credits, ["role", "name"]);

                    ret.parentalRatings = json.parentalRatings;

                    return new Asset(ret);
                }
                exportObj.parseAsset = parseAsset;


                function parseProgramAsset(json) {

                    var flagCover, flagThumb, flagBanner,
                        ret = {
                            type: "",
                            id: json.id,
                            title: json.title,
                            description: json.description,
                            images: [],
                            metadata: {}
                        };

                    // for a program object, its type should be _program_
                    ret.type = Asset.TYPE.PROGRAM;

                    flagThumb = false;
                    flagCover = false;
                    flagBanner = false;

                    ax.util.each(json.images, function (image) {

                        if (flagThumb && flagBanner && flagCover) {
                            return ax.util.breaker;
                        }


                        if (!flagThumb && ax.util.indexOf(image.type, IMAGES.THUMBNAIL) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.thumbnail$resId = image.id;
                            flagThumb = true;

                            return;
                        }

                        if (!flagCover && ax.util.indexOf(image.type, IMAGES.COVER) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.cover$resId = image.id;
                            flagCover = true;

                            return;
                        }

                        if (!flagBanner && ax.util.indexOf(image.type, IMAGES.BANNER) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.banner$resId = image.id;
                            flagBanner = true;

                            return;
                        }
                    });

                    ret.metadata.program$startTime = json.startTime;
                    ret.metadata.program$endTime = json.endTime;

                    return new Asset(ret);
                }
                exportObj.parseProgramAsset = parseProgramAsset;


                function parseTVChannel(json) {

                    var ret = {
                        id: json.id,
                        title: json.title,
                        channelNumber: json.channelNumber,
                        description: json.description,
                        categories: [],
                        images: [],
                        streams: [],
                        geoLock: json.geoLock,
                        language: json.language,
                        metadata: {}
                    };

                    ax.util.each(json.categories, function (category) {
                        ret.categories.push(new Category(category));
                    });


                    ax.util.each(json.images, function (image) {

                        if (ax.util.indexOf(image.type, IMAGES.LOGO) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.logo$resId = image.id;

                            // only has one logo image in AccedoOVP
                            return ax.util.breaker;
                        }

                    });

                    ax.util.each(json.streams, function (stream) {

                        ret.streams.push(parseResource(stream));
                        ret.metadata.content$resId = stream.id;

                        // only has one stream in AccedoOVP
                        return ax.util.breaker;
                    });

                    return new TVChannel(ret);
                }
                exportObj.parseTVChannel = parseTVChannel;


                function parseTVListing(json) {

                    var ret = {
                        channelId: json.channelId,
                        programs: []
                    };

                    ax.util.each(json.programs, function (program) {
                        ret.programs.push(parseProgramAsset(program));
                    });

                    return new TVListing(ret);
                }
                exportObj.parseTVListing = parseTVListing;


                function parseTVShow(json) {

                    var flagThumb, flagBanner,
                        ret = {
                            id: json.id,
                            title: json.title,
                            description: json.description,
                            categories: [],
                            images: [],
                            episodes: [],
                            metadata: {},
                            publishedDate: json.publishedDate,
                            availableDate: json.availableDate
                        };

                    ax.util.each(json.categories, function (category) {
                        ret.categories.push(new Category(category));
                    });

                    flagThumb = false;
                    flagBanner = false;

                    ax.util.each(json.images, function (image) {

                        if (flagBanner && flagThumb) {
                            return ax.util.breaker;
                        }

                        if (!flagThumb && ax.util.indexOf(image.type, IMAGES.THUMBNAIL) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.thumbnail$resId = image.id;
                            flagThumb = true;

                            return;
                        }

                        if (!flagBanner && ax.util.indexOf(image.type, IMAGES.BANNER) > -1) {
                            ret.images.push(parseResource(image));
                            ret.metadata.banner$resId = image.id;
                            flagBanner = true;

                            return;
                        }

                    });

                    // there is an option to show episode details or not
                    if (json.episodes) {

                        // parse incomplete episode item
                        ax.util.each(json.episodes, function (episode) {

                            var episodeRet = {
                                type: Asset.TYPE.EPISODE,
                                id: episode.id,
                                title: episode.title,
                                description: episode.description,
                                images: [],
                                metadata: {
                                    "thumbnail$resId": episode.images[0].id,
                                    "episodeNumber": episode.episodeNumber
                                }
                            };

                            episodeRet.images.push(parseResource(episode.images[0]));

                            ret.episodes.push(new Asset(episodeRet));
                        });
                    }

                    return new TVShow(ret);
                }
                exportObj.parseTVShow = parseTVShow;


                modelParseFnMap = {
                    "resource": parseResource,
                    "category": parseCategory,
                    "asset": parseAsset,
                    "programAsset": parseProgramAsset,
                    "channel": parseTVChannel,
                    "tvlisting": parseTVListing,
                    "tvshow": parseTVShow
                };

                function parseModelEntries(entries, modelName) {

                    var parseFn = modelParseFnMap[modelName];

                    if (!parseFn) {
                        throw new TVEError(FACILITY.GENERAL, ERROR.INTERNAL, "incorrect parser reference: " + modelName);
                    }

                    var ret = [];
                    ax.util.each(entries, function (entry) {
                        ret.push(parseFn(entry));
                    });

                    return ret;
                }
                exportObj.parseModelEntries = parseModelEntries;


                function __arrayToMapHelper(subObj, parseMap) {
                    var ret = {},
                        NAME = parseMap[0],
                        VALUE = parseMap[1],
                        subObjName, subObjValue, subLen, i;

                    subLen = subObj.length;

                    for (i = 0; i < subLen; i++) {
                        subObjName = subObj[i][NAME];
                        subObjValue = subObj[i][VALUE];
                        if (!subObjName || !subObjValue) {
                            continue;
                        }
                        // parse array to object
                        ret[subObjName] = subObjValue;
                    }

                    return ret;
                }


                return exportObj;


            })(),

            OvpService = ax.klass.create([VODContentService, LinearContentService], {
                /**
                 * Sorting order
                 * @name SORTING
                 * @typedef {Object}
                 * @property {String} ASC - sort ascending
                 * @property {String} DESC - sort descending
                 * @public
                 * @memberof tve/AccedoOvpService
                 * @static
                 */
                SORTING: SORTING
            }, {
                /**
                 * CachingHelper Instance, may be used for any OVP related service.
                 * @member __cache
                 * @memberof tve/AccedoOvpService#
                 * @private
                 */
                __cache: null,

                /**
                 * Init the AccedoOvpService moudle
                 * @method init
                 * @memeberof tve/AccedoOvpService#
                 * @private
                 */
                init: function (opts) {
                    if (opts && opts.baseURL) {
                        this.__cache = new CacheHelper();
                        ovpServiceApi.init(opts.baseURL, this.__cache);
                    } else {
                        throw new TVEError(FACILITY.GENERAL, ERROR.INTERNAL, "no baseURL is provided, cannot init service");
                    }
                },

                // ==== Impliment Interfaces ====

                // Private function

                /**
                 * Get all Categories
                 * @method getCategories
                 * @public
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/name
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */

                getAllCategories: function (opts) {

                    var _json;

                    return ovpServiceApi.categoryRequest(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "category");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get Category By Id
                 * @method getCategoryById
                 * @public
                 * @param {String} id CategoryID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/name
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<tve/model/MediaCategory>} MediaCategory object
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getCategoryById: function (id, opts) {
                    if (!id) {
                        return ax.promise.reject(new TVEError(FACILITY.GENERAL, ERROR.INTERNAL, "no id is provided"));
                    }
                    opts = opts || {};
                    opts.id = id;
                    return ovpServiceApi.categoryRequest(opts).then(function (json) {

                        return ovpServiceParser.parseCategory(json);
                    });
                },

                /**
                 * Get Categories By Ids
                 * @method getCategoriesByIds
                 * @public
                 * @param {String[]} ids CategoryIDs
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/name
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getCategoriesByIds: function (ids, opts) {
                    if (!ids || !ax.util.isArray(ids)) {
                        return ax.promise.reject(new TVEError(FACILITY.GENERAL, ERROR.INTERNAL, "incorrect parameter type"));
                    }
                    opts = opts || {};
                    opts.id = ids;

                    var _json;

                    return ovpServiceApi.categoryRequest(opts).then(function (json) {
                        _json = json;

                        if (ids.length > 1) {
                            return ovpServiceParser.parseModelEntries(json.entries, "category");
                        }
                        return ovpServiceParser.parseCategory(json);

                    }).then(function (entries) {

                        return {
                            // in case of only one object, set all other attribues as 1
                            entries: ax.util.isArray(entries) ? entries : [entries],
                            totalCount: _json.totalCount || 1,
                            pageSize: _json.pageSize || 1,
                            pageNumber: _json.pageNumber || 1
                        };
                    });
                },


                // ==== ILinearContentService ====

                // Private function
                __getChannel: function (opts) {

                    if (opts && opts.categoryId) {
                        opts.id = opts.categoryId;
                        delete opts.categoryId;
                        return ovpServiceApi.channelByCategoryRequest(opts);
                    } else {
                        return ovpServiceApi.channelRequest(opts);
                    }

                },

                /**
                 * Get all Channels
                 * @method getAllChannels
                 * @public
                 * @param {Object} opts The options object
                 * @param {String} [opts.sortingKey] sort by id/channelNumber/title
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getAllChannels: function (opts) {

                    var _json;

                    return this.__getChannel(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "channel");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get Channel by Id
                 * @method getChannelById
                 * @public
                 * @param {String} id ChannelID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/channelNumber/title
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<tve/model/Channel>} Channel object
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getChannelById: function (id, opts) {
                    if (!id) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided"));
                    }
                    opts = opts || {};
                    opts.id = id;
                    return this.__getChannel(opts).then(function (json) {

                        return ovpServiceParser.parseTVChannel(json);
                    });
                },

                /**
                 * Get Channels by Ids
                 * @method getChannelsByIds
                 * @public
                 * @param {String[]} ids ChannelIDs
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/channelNumber/title
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getChannelsByIds: function (ids, opts) {
                    if (!ids || !ax.util.isArray(ids)) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "incorrect parameter type"));
                    }
                    opts = opts || {};
                    opts.id = ids;

                    var _json;

                    return this.__getChannel(opts).then(function (json) {
                        _json = json;

                        if (ids.length > 1) {
                            return ovpServiceParser.parseModelEntries(json.entries, "channel");
                        }
                        return ovpServiceParser.parseTVChannel(json);

                    }).then(function (entries) {

                        return {
                            // in case of only one object, set all other attribues as 1
                            entries: ax.util.isArray(entries) ? entries : [entries],
                            totalCount: _json.totalCount || 1,
                            pageSize: _json.pageSize || 1,
                            pageNumber: _json.pageNumber || 1
                        };
                    });
                },

                /**
                 * Get Channels by Category
                 * @method getChannelsByCategoryId
                 * @public
                 * @param {String} categoryId CategoryID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/channelNumber/title
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getChannelsByCategoryId: function (categoryId, opts) {
                    if (!categoryId) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no categoryId is provided"));
                    }
                    opts = opts || {};
                    opts.categoryId = categoryId;

                    var _json;

                    return this.__getChannel(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "channel");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get all TVListings
                 * @method getAllTVListings
                 * @public
                 * @param {String} startTime startTime
                 * @param {String} endTime endTime
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/channelId
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getAllTVListings: function (startTime, endTime, opts) {

                    if (!startTime || !endTime) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no startTime or endTime is provided"));
                    }
                    opts = opts || {};
                    opts.startTime = startTime;
                    opts.endTime = endTime;

                    var _json;

                    return ovpServiceApi.tvListingRequest(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "tvlisting");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get TVListing by channel Id
                 * @method getTVListingByChannelId
                 * @public
                 * @param {String} channelId ChannelID
                 * @param {String} startTime startTime
                 * @param {String} endTime endTime
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/channelId
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<tve/model/TVListing>} TVListing object
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getTVListingByChannelId: function (channelId, startTime, endTime, opts) {
                    if (!channelId) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no channelId is provided"));
                    }
                    if (!startTime || !endTime) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no startTime or endTime is provided"));
                    }
                    opts = opts || {};
                    opts.channelId = channelId;
                    opts.startTime = startTime;
                    opts.endTime = endTime;

                    return ovpServiceApi.tvListingRequest(opts).then(function (json) {

                        return ovpServiceParser.parseTVListing(json);
                    });
                },

                /**
                 * Get TVListing by channel Ids
                 * @method getTVListingsByChannelIds
                 * @public
                 * @param {String[]} channelIds ChannelIDs
                 * @param {String} startTime startTime
                 * @param {String} endTime endTime
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/channelId
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getTVListingsByChannelIds: function (channelIds, startTime, endTime, opts) {
                    if (!channelIds) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no channelIds are provided"));
                    }
                    if (!startTime || !endTime) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no startTime or endTime is provided"));
                    }
                    opts = opts || {};
                    opts.channelId = channelIds;
                    opts.startTime = startTime;
                    opts.endTime = endTime;

                    var _json;

                    return ovpServiceApi.tvListingRequest(opts).then(function (json) {
                        _json = json;

                        if (channelIds.length > 1) {
                            return ovpServiceParser.parseModelEntries(json.entries, "tvlisting");
                        }
                        return ovpServiceParser.parseTVListing(json);

                    }).then(function (entries) {

                        return {
                            // in case of only one object, set all other attribues as 1
                            entries: ax.util.isArray(entries) ? entries : [entries],
                            totalCount: _json.totalCount || 1,
                            pageSize: _json.pageSize || 1,
                            pageNumber: _json.pageNumber || 1
                        };
                    });
                },

                // ==== IVODContentService ====

                // Private functions
                __getMovie: function (opts) {

                    if (opts && opts.categoryId) {
                        opts.id = opts.categoryId;
                        delete opts.categoryId;
                        return ovpServiceApi.movieByCategoryRequest(opts);
                    } else {
                        return ovpServiceApi.movieRequest(opts);
                    }

                },

                __getTVShow: function (opts) {

                    if (opts && opts.categoryId) {
                        opts.id = opts.categoryId;
                        delete opts.categoryId;
                        return ovpServiceApi.tvShowByCategoryRequest(opts);
                    } else {
                        return ovpServiceApi.tvShowRequest(opts);
                    }

                },

                __getEpisode: function (opts) {

                    if (opts && opts.showId) {
                        opts.id = opts.showId;
                        delete opts.showId;
                        return ovpServiceApi.episodeByTVShowRequest(opts);
                    } else {
                        return ovpServiceApi.episodeRequest(opts);
                    }
                },

                /**
                 * Get all Movies
                 * @method getAllMovies
                 * @public
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getAllMovies: function (opts) {

                    var _json;

                    return this.__getMovie(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "asset");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get Movie by Id
                 * @method getMovieById
                 * @public
                 * @param {String} id MovieID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<tve/model/Asset>} Asset object
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getMovieById: function (id, opts) {
                    if (!id) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided"));
                    }
                    opts = opts || {};
                    opts.id = id;
                    return this.__getMovie(opts).then(function (json) {

                        return ovpServiceParser.parseAsset(json);
                    });
                },

                /**
                 * Get Movies by Ids
                 * @method getMoviesByIds
                 * @public
                 * @param {String[]} ids MovieIDs
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getMoviesByIds: function (ids, opts) {
                    if (!ids || !ax.util.isArray(ids)) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "incorrect parameter type"));
                    }
                    opts = opts || {};
                    opts.id = ids;

                    var _json;

                    return this.__getMovie(opts).then(function (json) {
                        _json = json;

                        if (ids.length > 1) {
                            return ovpServiceParser.parseModelEntries(json.entries, "asset");
                        }
                        return ovpServiceParser.parseAsset(json);

                    }).then(function (entries) {

                        return {
                            // in case of only one object, set all other attribues as 1
                            entries: ax.util.isArray(entries) ? entries : [entries],
                            totalCount: _json.totalCount || 1,
                            pageSize: _json.pageSize || 1,
                            pageNumber: _json.pageNumber || 1
                        };
                    });
                },


                /**
                 * Get Movies by category
                 * @method getMoviesByCategoryId
                 * @public
                 * @param {String} categoryId CategoryID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getMoviesByCategoryId: function (categoryId, opts) {
                    if (!categoryId) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no categoryId is provided"));
                    }
                    opts = opts || {};
                    opts.categoryId = categoryId;

                    var _json;

                    return this.__getMovie(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "asset");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get all TV Shows
                 * @method getAllTVShows
                 * @public
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @param {String} [opts.showEpisodes] flag to show episodes in return
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getAllTVShows: function (opts) {

                    var _json;

                    return this.__getTVShow(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "tvshow");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get TV Show by Id
                 * @method getTVShowById
                 * @public
                 * @param {String} id TVShowID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @param {String} [opts.showEpisodes] flag to show episodes in return
                 * @returns {Promise.<tve/model/Asset>} Asset object
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getTVShowById: function (id, opts) {
                    if (!id) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided"));
                    }
                    opts = opts || {};
                    opts.id = id;
                    return this.__getTVShow(opts).then(function (json) {

                        return ovpServiceParser.parseTVShow(json);
                    });
                },

                /**
                 * Get TV Shows by Ids
                 * @method getTVShowsByIds
                 * @public
                 * @param {String[]} ids TVShowIDs
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @param {String} [opts.showEpisodes] flag to show episodes in return
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getTVShowsByIds: function (ids, opts) {
                    if (!ids || !ax.util.isArray(ids)) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "incorrect parameter type"));
                    }
                    opts = opts || {};
                    opts.id = ids;

                    var _json;

                    return this.__getTVShow(opts).then(function (json) {
                        _json = json;

                        if (ids.length > 1) {
                            return ovpServiceParser.parseModelEntries(json.entries, "tvshow");
                        }
                        return ovpServiceParser.parseTVShow(json);

                    }).then(function (entries) {

                        return {
                            // in case of only one object, set all other attribues as 1
                            entries: ax.util.isArray(entries) ? entries : [entries],
                            totalCount: _json.totalCount || 1,
                            pageSize: _json.pageSize || 1,
                            pageNumber: _json.pageNumber || 1
                        };
                    });
                },

                /**
                 * Get TV Shows by category
                 * @method getTVShowsByCategoryId
                 * @public
                 * @param {String} categoryId CategoryID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @param {String} [opts.showEpisodes] flag to show episodes in return
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getTVShowsByCategoryId: function (categoryId, opts) {
                    if (!categoryId) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no categoryId is provided"));
                    }
                    opts = opts || {};
                    opts.categoryId = categoryId;

                    var _json;

                    return this.__getTVShow(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "tvshow");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get all Episodes
                 * @method getAllEpisodes
                 * @public
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getAllEpisodes: function (opts) {

                    var _json;

                    return this.__getEpisode(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "asset");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                /**
                 * Get Episodes by Id
                 * @method getEpisodeById
                 * @public
                 * @param {String} id EpisodeID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<tve/model/Asset>} Asset object
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getEpisodeById: function (id, opts) {
                    if (!id) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided"));
                    }
                    opts = opts || {};
                    opts.id = id;
                    return this.__getEpisode(opts).then(function (json) {

                        return ovpServiceParser.parseAsset(json);
                    });
                },

                /**
                 * Get Episodes by TVShow Id
                 * @method getEpisodesByTVShowId
                 * @public
                 * @param {String} showId showID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortingKey] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
                 * @param {tve/AccedoOvpService.SORTING} [opts.sortingOrder] sorting order. Default is descending
                 * @param {Integer} [opts.pageSize] page size. Default is 100
                 * @param {Integer} [opts.pageNumber] page number. Default is 1
                 * @returns {Promise.<Object>} Object with 4 properites: entries, totalConut, pageSize, pageNumber
                 * @throws {Promise.<tve/TVEError>} Internal error object
                 * @memberof tve/AccedoOvpService#
                 */
                getEpisodesByTVShowId: function (showId, opts) {
                    if (!showId) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no showId is provided"));
                    }
                    opts = opts || {};
                    opts.showId = showId;

                    var _json;

                    return this.__getEpisode(opts).then(function (json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "asset");

                    }).then(function (entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },


                // Searching
                getSearchMovies: function(key, opts) {
                    if (!key) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no key is provided"));
                    }
                    opts = opts || {};
                    opts.targets = "movie";
                    opts.key = key;

                    var _json;

                    return ovpServiceApi.searchRequest(opts).then(function(json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "asset");

                    }).then(function(entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                getSearchTVShows: function(key, opts) {
                    if (!key) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no key is provided"));
                    }
                    opts = opts || {};
                    opts.targets = "tvshow";
                    opts.key = key;

                    var _json;

                    return ovpServiceApi.searchRequest(opts).then(function(json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "tvshow");

                    }).then(function(entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                },

                getSearchEpisodes: function(key, opts) {
                    if (!key) {
                        return ax.promise.reject(new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no key is provided"));
                    }
                    opts = opts || {};
                    opts.targets = "episode";
                    opts.key = key;

                    var _json;

                    return ovpServiceApi.searchRequest(opts).then(function(json) {
                        _json = json;

                        return ovpServiceParser.parseModelEntries(json.entries, "asset");

                    }).then(function(entries) {

                        return {
                            entries: entries,
                            totalCount: _json.totalCount,
                            pageSize: _json.pageSize,
                            pageNumber: _json.pageNumber
                        };
                    });
                }

            });

        return OvpService;
    });
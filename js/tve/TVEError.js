/**
 * Error handling for TVE Blocks
 * @name TVEError
 * @memberof tve
 * @class tve/TVEError
 * @example
 *      new TVEError(TVEError.FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.NOT_FOUND, 'a custom error message');
 * @augments Error
 * @author Shawn Zeng <shawn.zeng@accedo.tv>
 */
define("tve/TVEError", ["ax/class"], function (klass) {

    var TVEError = klass.create(Error, {
        /**
         * Available facility codes of TVE Error
         * @name FACILITY
         * @type {object}
         * @public
         * @memberof tve/TVEError
         * @static
         */
        FACILITY: {

            /**
             * Configuration service
             * @name CONFIGURATION_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            CONFIGURATION_SERVICE: 11,
            
            /**
             * User settings service
             * @name USER_SETTINGS_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            USER_SETTINGS_SERVICE: 12,

            /**
             * Analytics service
             * @name ANALYTICS_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            ANALYTICS_SERVICE: 13,

            /**
             * Status service
             * @name STATUS_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            STATUS_SERVICE: 14,

            /**
             * Resource service
             * @name RESOURCE_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            RESOURCE_SERVICE: 15,

            /**
             * Log service
             * @name LOG_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            LOG_SERVICE: 16,

            /**
             * Connect
             * @name CONNECT
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            CONNECT: 20,

            /**
             * Ad Mediate
             * @name AD_MEDIATE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            AD_MEDIATE: 21,

            /**
             * VOD content service
             * @name VOD_CONTENT_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            VOD_CONTENT_SERVICE: 40,

            /**
             * Linear content service
             * @name LINEAR_CONTENT_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            LINEAR_CONTENT_SERVICE: 41,

            /**
             * Rating service
             * @name RATING_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            RATING_SERVICE: 42,

            /**
             * Authentication service
             * @name AUTHENTICATION_SERVICE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            AUTHENTICATION_SERVICE: 43,

            /**
             * Linear manager
             * @name LINEAR_MANAGER
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            LINEAR_MANAGER: 50,

            /**
             * VOD manager
             * @name VOD_MANAGER
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            VOD_MANAGER: 51,
        
            /**
             * General
             * @name GENERAL
             * @type {Number}
             * @public
             * @memberof tve/TVEError.FACILITY
             * @static
             */
            GENERAL: 90
        },

        /**
         * Available error codes of TVE Error
         * @name ERROR
         * @type {object}
         * @public
         * @memberof tve/TVEError
         * @static
         */
        ERROR: {

            /**
             * Requested item has not been found
             * @name NOT_FOUND
             * @type {Number}
             * @public
             * @memberof tve/TVEError.ERROR
             * @static
             */
            NOT_FOUND: 1,

            /**
             * Network problem during communication
             * @name NETWORK
             * @type {Number}
             * @public
             * @memberof tve/TVEError.ERROR
             * @static
             */
            NETWORK: 2,

            /**
             * Internal error occurred
             * @name INTERNAL
             * @type {Number}
             * @public
             * @memberof tve/TVEError.ERROR
             * @static
             */
            INTERNAL: 3,

            /**
             * Unauthorized to access the requested resource
             * @name UNAUTHORIZED
             * @type {Number}
             * @public
             * @memberof tve/TVEError.ERROR
             * @static
             */
            UNAUTHORIZED: 4,

            /**
             * Error while parsing response
             * @name INVALID
             * @type {Number}
             * @public
             * @memberof tve/TVEError.ERROR
             * @static
             */
            INVALID: 5,

            /**
             * Storing resource failed
             * @name STORAGE
             * @type {Number}
             * @public
             * @memberof tve/TVEError.ERROR
             * @static
             */
            STORAGE: 6
        }  
    }, 
        
    { 
        /**
         * Init the TVEError moudle
         * @method init
         * @param {Number} facility 2 digital numbers, the facility of the error
         * @param {Number} errorCode 3 digital numbers, the error code of the error
         * @param {String} message the error message of the error, this parameter will be a public memeber 
         * @param {String|Number|Boolean|Object} cause (optional) the error cause of the error
         * @memberof tve/TVEError
         * @private
         */
        init: function(facility, errorCode, message, cause){
            this.__facility = facility;
            this.__errorCode = errorCode;
            this.__cause = cause || null;
            this.__code = this.__facility * 1000 + this.__errorCode;
            
            //message and name are public memebers
            this.message = message;
            this.name = this.__code.toString();
        },

        /**
         * Get the facility of the error object.
         * @method getCode
         * @return {Number} 2 digital numbers 
         * @memberof tve/TVEError
         * @public
         */
        getFacility: function(){
            return this.__facility;
        },

        /**
         * Get the error code of the error object.
         * @method getCode
         * @return {Number} 1-3 digital numbers
         * @memberof tve/TVEError
         * @public
         */
        getErrorCode: function(){
            return this.__errorCode;
        },

        /**
         * Get the combined code (facility and error code) of the error object.
         * @method getCode
         * @return {Number} 5 digital numbers
         * @memberof tve/TVEError
         * @public
         */
        getCode: function(){
            return this.__code;
        },

        /**
         * Get the error message of the error object.
         * @method getMessage
         * @return {String} the error message
         * @memberof tve/TVEError
         * @public
         */
        getMessage: function(){
            return this.message;
        },

        /**
         * Get the error cause of the error object.
         * @method getCause
         * @return {Object} the error cause
         * @memberof tve/TVEError
         * @public
         */
        getCause: function(){
            return this.__cause;
        }
    });

	return TVEError;
});
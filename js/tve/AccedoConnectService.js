/** 
 * The Accedo Conntect Service uses the AccedoConnect Lib to provide an implementation of the Interaction interface.
 * @name AccedoConnectService
 * @memberof tve
 * @class tve/AccedoConnectService
 * @augments tve/interface/InteractionService
 * @author Joe Roth <joe.roth@accedo.tv>
 */
/*jslint nomen: true */
/*global define: true, setTimeout: true*/
define("tve/AccedoConnectService", ["ax/ax", "ax/device", "ax/console", "ax/promise", "tve/TVEError", "tve/interface/InteractionService", "lib/aconnect.js"],
    function (ax, device, console, promise, TVEError, InteractionService, _connect) {
       
        "use strict";

        // Connect actually loaded as a Global, _connect is not used
        CONNECT.config.transport_mode = ax.config.get("tve.connect.transport", "xhr");

        var connect = CONNECT,

            //stateEnum
            STATE = InteractionService.STATE,
            ROLE = InteractionService.ROLE,
            currentLibState, // the library states before abstraction
            currentState,
            currentRole,
            MAX_MESSAGE_SIZE = 1024, // bytes 
            STATE_TIMEOUT = 10 * 1000, // 10 second timeout on getState

            //public method
            ConnectService,

            //private variables used for public interface
            stateCallback,
            receivedCommandCallback,
            getStateCallback,
            createPairOnSuccess,
            createPairOnFailure,
            initConnect,
            
            //private methods
            setCurrentState,
            createPair,
            pairWithCode,
            getPairingCode,
            unpair,
            kickAll,
            shutdownLibrary,
            onConnectStateChange,
            
            //private variables used for AccedoConnectLib interaction
            uuid,
            apiKey,
            connectServer,
            connection,
            connectStateCb = {
                "onStateChange": function (newState) {
                    currentLibState = newState;

                    switch (newState.value) {
                    case connect.statesEnum.INITIALIZED.value:
                        // there is some bug in the lib....
                        // goes to state 01 after unpair instead of state 02
                        // when bug is removed....we can remove this case
                        if (currentState) {
                            setCurrentState(STATE.UNPAIRED);
                        }
                        break;
                    case connect.statesEnum.UNPAIRED_DISCONNECTED.value:
                        setCurrentState(STATE.UNPAIRED);
                        break;
                    case connect.statesEnum.UNPAIRED_CONNECTED_ALONE.value:
                        setCurrentState(STATE.UNPAIRED);
                        break;
                    case connect.statesEnum.PAIRED_CONNECTED_ALONE.value:
                        setCurrentState(STATE.PAIRED_DISCONNECTED);
                        break;
                    case connect.statesEnum.PAIRED_CONNECTED.value:
                        setCurrentState(STATE.PAIRED_CONNECTED);
                        break;
                    case connect.statesEnum.UNPAIRED_CONNECTED.value:
                        setCurrentState(STATE.UNPAIRED);
                        break;
                    default:
                    }
                }
            },
            connectChannelOpts = {
                "onMessage": function (msg) {
                    // a valid command should have a message set
                    // filter the empty message commands that are not sent from peer
                    if (msg.message === undefined || msg.message === null) {
                        return;
                    }

                    receivedCommandCallback(msg.message);
                },
                "onPresence": function (ntf) {
                    //don"t need to do anything here...same functionality is managed by state callback
                },
                "onSuccess": function (_connection) {
                    connection = _connection;
                    
                    if (typeof createPairOnSuccess === "function") {
                        if (connection.pairingCode) {
                            createPairOnSuccess(connection.pairingCode);
                        } else {
                            createPairOnSuccess();
                        }
                    }

                    createPairOnSuccess = null;
                    createPairOnFailure = null;
                },
                "onFail": function (reason) {
                    console.info(reason);

                    if (typeof createPairOnFailure === "function") {
                        createPairOnFailure(reason);
                    }

                    createPairOnSuccess = null;
                    createPairOnFailure = null;
                }
            };

        //this function initializes connect, and returns current state to cb
        initConnect = function (cb) {
            //this part is a bit convoluted (really should get promises into connect.... :/ )
            //it inits, then checks for pair
            //  if no pair
            //    state is UNPAIRED
            //  else if pair
            //    calls pair
            //      if alone (this is implemented in the onSuccess handler)
            //        state is PAIRED_DISCONNECTED
            //      else if people there
            //        state is PAIRED_CONNECTED
            getStateCallback = cb;
            connect.config.connect_server = connectServer;
            connect.init(apiKey, uuid, connectStateCb, function (success, reason) {
                if (success) {
                    connect.hasPair(function (hasPair) {
                        if (!hasPair) {
                            setCurrentState(STATE.UNPAIRED);
                        } else {
                            connect.pair(connectChannelOpts);
                        }
                    });
                } else {
                    cb(success, reason);
                }
            });
        };

        createPair = function (onSuccess, onFailure) {
            createPairOnSuccess = onSuccess;

            createPairOnFailure = onFailure;

            connect.createNewPair(connectChannelOpts);
        };

        pairWithCode = function (code, onSuccess, onFailure) {
            createPairOnSuccess = onSuccess;

            createPairOnFailure = onFailure;

            connect.pair(ax.util.extend({"code": code}, connectChannelOpts, true));
        };

        getPairingCode = function (cb) {
            connect.getPairingCode(cb);
        };

        unpair = function(cb) {
            connect.unpair(cb);
        };

        kickAll = function(cb) {
            connect.kickAll(cb);
        };

        shutdownLibrary = function() {
            connect.shutdownLibrary();
        };

        setCurrentState = function (state) {
            if (state !== currentState) {
                currentState = state;
                if (stateCallback) {
                    stateCallback(state);
                }
                if (getStateCallback) {
                    //getStateCallback is one-time use
                    getStateCallback(true);
                    getStateCallback = null;
                }
            }
        };

        ConnectService = ax.klass.create([InteractionService], {}, {
            /**
             * Init Connect
             * @method init
             * @public
             * @param {String} _uuid unque dentifier
             * @param {String} _apiKey Accedo Connect apiKey
             * @param {String} _connectServer Accedo Connect server
             * @memberof tve/AccedoConnectService#
             */
            init: function (_uuid, _apiKey, _connectServer) {
                uuid = _uuid;
                apiKey = _apiKey;
                connectServer = _connectServer;
            },

            setDeviceRole: function (role) {
                currentRole = role;
            },

            setStateCallback: function (cb) {
                stateCallback = cb;
            },

            setReceivedCommandCallback: function (cb) {
                receivedCommandCallback = cb;
            },

            getState: function () {
                if (!currentRole) {
                    return promise.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "No role is set"));
                }

                var defer = promise.defer();
                setTimeout(function () {
                    defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.NETWORK, "Network timeout"));
                }, STATE_TIMEOUT);

                if (!currentState) {
                    initConnect(function (success, reason) {
                        if (success && currentState) {
                            defer.resolve(currentState);
                        } else {
                            defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INTERNAL, reason));
                        }
                    });
                } else {
                    defer.resolve(currentState);
                }
                return defer.promise;
            },

            createPair: function () {
                if (currentRole !== ROLE.HOST) {
                    return promise.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Only the host can create pair"));
                }

                var defer = promise.defer(),
                    timeoutTimer = setTimeout(function () {
                        defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.NETWORK, "Network timeout"));
                    }, STATE_TIMEOUT),
                    onSuccess = function (code) {
                        clearTimeout(timeoutTimer);
                        defer.resolve(code);
                    },
                    onFailure = function (reason) {
                        clearTimeout(timeoutTimer);
                        defer.reject(reason);
                    };

                //creates the pair, returns a promise with the code
                return this.getState().then(function (myState) {
                    switch (myState) {
                    case STATE.UNPAIRED:
                        if (currentLibState.value === connect.statesEnum.UNPAIRED_CONNECTED_ALONE.value || currentLibState.value === connect.statesEnum.UNPAIRED_CONNECTED.value) {
                            getPairingCode(onSuccess);
                        } else {
                            createPair(onSuccess, onFailure);
                        }
                        break;
                    default:
                        clearTimeout(timeoutTimer);
                        return promise.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Could not pair in [" + myState ? myState.description : myState + "] state"));
                    }

                    return defer.promise;
                });
            },

            pairWithCode: function(code) {
                if (currentRole !== ROLE.CLIENT) {
                    return promise.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Only the client can pair with code"));
                }

                var defer = promise.defer(),
                    timeoutTimer = setTimeout(function () {
                        defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.NETWORK, "Network timeout"));
                    }, STATE_TIMEOUT * 2), // 2 API calls here
                    onSuccess = function (code) {
                        clearTimeout(timeoutTimer);
                        defer.resolve();
                    },
                    onFailure = function (reason) {
                        clearTimeout(timeoutTimer);
                        defer.reject(reason);
                    };

                //creates the pair, returns a promise with the code
                return this.getState().then(function (myState) {
                    switch (myState) {
                    case STATE.UNPAIRED:
                        pairWithCode(code, onSuccess, onFailure);
                        break;
                    default:
                        clearTimeout(timeoutTimer);
                        return promise.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Could not pair in [" + myState ? myState.description : myState + "] state"));
                    }

                    return defer.promise;
                });
            },

            unpair: function () {
                return this.getState().then(function(state) {
                    var defer = promise.defer();

                    switch (state) {
                    case STATE.PAIRED_CONNECTED: // intended non-break case
                    case STATE.PAIRED_DISCONNECTED:
                        unpair(function (response) {
                            defer.resolve();
                        });
                        break;
                    default:
                        defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Could not unpair in [" + state ? state.description : state + "] state"));
                        break;
                    }
                
                    return defer.promise;
                });
            },

            resetPair: function () {
                if (currentRole !== ROLE.HOST) {
                    return promise.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Only host can reset pair"));
                }

                return this.getState().then(function(state) {
                    var defer = promise.defer();

                    switch (state) {
                    case STATE.PAIRED_CONNECTED: // intended non-break case
                    case STATE.PAIRED_DISCONNECTED:
                        if (connect.hasOwnProperty("kickAll")) {
                            kickAll(function (response) {
                                defer.resolve();
                            });
                        } else {
                            defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "The connect library hasn't initialized properly"));
                        }
                        break;
                    default:
                        defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Could not reset pair in [" + state ? state.description : state + "] state"));
                        break;
                    }
                
                    return defer.promise;
                });
            },

            /**
             * Disconnects device.
             *   - if you are in state 3, your companion device will move to state 2
             *   - if you are in state 2, your companion device will start up in state 2
             * Calling 'getState' will put you back into state 2 or 3
             * @method disconnect
             * @memberof tve/AccedoConnectService#
             * @public
             */
            disconnect: function () {
                if (currentState) {
                    currentState = null;
                }

                shutdownLibrary();
            },

            sendCommand:  function (obj) {
                //fails if 
                //  obj doesn't have type attribute
                //  serialization of obj > 1024
                //  we are not in state 3
                //  we don't get a success callback from sendMessage method call
                var defer = promise.defer(),
                    hasType = obj.hasOwnProperty("type"),
                    objStr = JSON.stringify(obj);

                if (!hasType) {
                    defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Command object does not have type attribute"));
                    return defer.promise;
                }
                
                if (objStr.length > MAX_MESSAGE_SIZE) {
                    defer.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "Command object does not have type attribute"));
                    return defer.promise;
                }

                return this.getState().then(function (myState) {
                    if (myState !== STATE.PAIRED_CONNECTED) {
                        return promise.reject(new TVEError(TVEError.FACILITY.CONNECT, TVEError.ERROR.INVALID, "In state: " + myState.description));
                    } else {
                        // this has an optional callback argument, but there is currently a bug and it won't work
                        // the connect.util.ajax success response is always in JSON, but the sendMessage function expects it to be text
                        // calling JSON.parse on JSON data results in an error, and the callback is never called 
                        connection.sendMessage(obj);
                        return promise.resolve();
                    }
                });
            }
        });

        return ConnectService;
    });

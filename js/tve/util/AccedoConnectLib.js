/** 
 * Accedo connect client library.  
 * It is different from the official Accedo Connect client library in the following ways:
 *    - getLibrary function has been rewritten and uses eval() instead of jsonp to do library injection
 *    - connect.util.JSON references global JSON object which is included as XDK util
 *    - connect.util.ajax uses XDK function ax.ajax.request and converts the normal async calls used by library into promises
 *    - connect.util.jsonp has been removed
 *    - connect.config.connect_server has been removed.....make sure this property is set before init of the lib
 *    - some small linting changes
 * @name AccedoConnectLib
 * @memberof tve/util
 * @class tve/util/AccedoConnectLib
 * @author Joe Roth <joe.roth@accedo.tv>
 */
/*jslint nomen: true, evil: true */
/*global define:true */
define("tve/util/AccedoConnectLib", ["ax/console", "ax/ax", "ax/config"], function (console, ax, config) {

    "use strict";
    var CONNECT = {};

    CONNECT.config = {
        "transport_mode"  : config.get("tve.connect.transport", "xhr")
    };

    CONNECT.log = function (str) {
        // uncomment this for debugging purposes
        // console.log(str);
    };

    CONNECT.info = function (str) {
        console.info(str);
    };

    (function (connect) {
        //private variables
        var SDK_VERSION = "2.2",
            MAX_TIMEOUT = 60 * 60 * 10, //time in seconds of an AJAX timeout, needs to be high because we do long polling

        //private functions
            getLibrary,
            processConnectCallbacks;

        //possible states
        connect.statesEnum = {
            NOT_INITIALIZED: {
                value:  "00",
                description: "Connect library not initialized"
            },
            INITIALIZED: {
                value:  "01",
                description:  "Connect library initialized, disconnected"
            },
            UNPAIRED_DISCONNECTED: {
                value:  "02",
                description:  "Device not paired, disconnected"
            },
            UNPAIRED_CONNECTED_ALONE: {
                value:  "03",
                description:  "Unpaired but connected, one device in channel"
            },
            PAIRED_CONNECTED_ALONE: {
                value:  "04",
                description:  "Paired and connected, one device in channel"
            },
            PAIRED_CONNECTED: {
                value:  "05",
                description:  "Paired and connected, multiple devices in channel"
            },
            UNPAIRED_CONNECTED: {
                value:  "06",
                description:  "Unpaired but connected, multiple devices in channel"
            },
            UNKNOWN: {
                value:  "99",
                description: "State unknown. Network lost or App was shutdown"
            }
        };
        connect.connectCallbacks = {};        //saves the callback objects that the client passes in "init"
        connect.currentState = connect.statesEnum.NOT_INITIALIZED; //set first state

        /**
        * Initializes the Accedo Connect library (which includes getting server side component of library)
        * @method init
        * @public
        * @param {String} appKey Application specific Accedo Connect application key
        * @param {String} deviceID unique identifier
        * @param {Object} connectCallbacks_ Object which contains the various connect callback functions
        * @param {requestCallback} [connectCallbacks_.onStateChane] Callback function for state changed
        * @param {requestCallback} cb Returns true boolean to callback function for successful library init
        * @memberof tve/AccedoConnectLib
        */
        connect.init =  function (appKey, deviceID, connectCallbacks_, cb) {
            connect.log("Starting init.");
            connect.deviceID = deviceID;

            //save our callbacks
            if (!processConnectCallbacks(connectCallbacks_)) {
                connect.log("Missing required connect callbacks.  Add them and try to pair again.");
                cb(false);
                return;
            }

            connect.log("Sending authorization request: {appKey:" + appKey + ",deviceID:" + deviceID + "}");
            connect.util.ajax(connect.config.connect_server + "/auth/" + appKey,
                //auth success
                function (resp) {
                    connect.session_key = resp.session.key;
                    connect.log("Authorization success. Key: " + connect.session_key);
                    getLibrary(connect.session_key, cb);
                },
                //auth fail
                function (obj) {
                    //connect.log("Authorization failure. Reason: " + reason);
                    cb.call(this, false, JSON.parse(obj.transport.response));
                    //connect.connectCallbacks.onStateChange(connect.statesEnum.NOT_INITIALIZED);
                }
                );
        };

        processConnectCallbacks = function (connectCallbacks_) {
            if (
                typeof connectCallbacks_ !== "object" ||
                    !(connectCallbacks_.hasOwnProperty("onStateChange"))
            ) {
                return false;
            }

            //save our connect callbacks for later use
            connect.connectCallbacks.onStateChange = connectCallbacks_.onStateChange || function () {};
            return true;
        };

        connect.pair = function () {
            connect.info("Must call 'init' before pairing.");
        };

        connect.unpair = function () {
            connect.info("Must call 'init' before unpairing.");
        };

        connect.createNewPair = function () {
            connect.info("Must call 'init' before creating new pairing.");
        };

        connect.getVersion = function () {
            return SDK_VERSION;
        };

        connect.shutdownLibrary = function () {
            connect.info("Must call 'init' before shutting down library.");
        };

        // one use function which injects the server-side library as a script tag
        // calls onLoadSuccess when library has been successfully loaded
        getLibrary = function (key, cb) {
            var libUrl = connect.config.connect_server + "/lib/?sessionKey=" + key + "&connectVar=connect&version=" + SDK_VERSION;
            connect.log("Making library injection request at url: " + libUrl);

            connect.onLoadSuccess = function () {};

            // use the promise pattern because the connect.util.ajax function parses response as JSON
            ax.ajax.request(libUrl).then(function (resp) {
                eval(resp.responseText);
                cb(true);
            }, function (err) {
                console.log(err);
            });
        };

        // connect util functions are used by both server lib and client lib to do common things (ajax, JSON parsing)
        // These have been formatted specifically for TVE
        connect.util = {};
        connect.util.ajax = function (url, onSuccess, onError) {
            var req = ax.ajax.request(url, {timeOut: MAX_TIMEOUT});
            req.then(function (resp) {
                onSuccess(JSON.parse(resp.response));
            }, function (err) {
                onError(err);
            });
        };
        connect.util.JSON  = JSON;

    }(CONNECT));


    return CONNECT;
});

/**
 * The utility for cache. It can generate a cache from an old cache(if it has) with a size limitation and a purging time for each object.
 * The default setting for the cache is to purge the object after 5mins the object being created/refreshed, and the size is limited to 100.
 * For the details in architecture, please check the wiki page on https://accedobroadband.jira.com/wiki/display/TVE/Caching
 * @name CacheHelper
 * @memberof tve/util
 * @class tve/util/CacheHelper
 * @author Shawn Zeng <shawn.zeng@accedo.tv>
 */
define("tve/util/CacheHelper", ["ax/class", "ax/core", "ax/promise", "ax/console", "ax/util"], function (klass, core, promise, console, util) {
    
    var CacheHelper = klass.create({

        /**
         * Available codes of cache TTL setting
         * @name TTL
         * @type {object}
         * @public
         * @memberof tve/util/CacheHelper
         * @static
         */
        TTL:{
        
            NO_CACHE: -2,
            
            FOREVER: -1,
                
            DEFAULT: 0        
        }

    }, 
    
    {
        
        /**
         * The time of how long will the cached object be valid
         * @member __ttl
         * @memberof tve/util/CacheHelper
         * @private
         */        
        __ttl: null,

        /**
         * The maximum of the objects for the cache   
         * @member __size
         * @memberof tve/util/CacheHelper
         * @private
         */
        __size: null,
        
        /**
         * The live time for the cache objects.
         * @member __cache
         * @memberof tve/util/CacheHelper
         * @private
         */
        __cache: null,        
        
        /**
         * Init the cache helper, set the cache, and time of purging
         * @method init
         * @param {Object} [opts] Object to be set by using the different attributes
         * @param {String} [opts.ttl]  The live time for the cache object. The default time is 5 mins(5*60s).
         * @param {String} [opts.cache] Set the cache based on another one
         * @param {Object} [opts.size]  The maximum of the objects for the cache. The default size is 100.
         * @memberof tve/util/CacheHelper
         * @private
         */        
        init: function(opts){
            opts = opts || {};
            this.__ttl = opts.ttl || 5*60;
            this.__size = opts.size || 100;

            this.__cacheReference = [];

            if(opts.cache){
                this.__cache = util.clone(opts.cache, true);

                var cache = this.__cache,
                    cacheReference = this.__cacheReference,
                    limitedSize = this.__size,
                    importSize,
                    key,
                    i;

                for(key in cache){
                    if(cache.hasOwnProperty(key)){
                        cacheReference.push(key);
                    }
                }
                
                importSize = util.size(cacheReference);

                if(importSize > limitedSize){
                    for(i = limitedSize; i < importSize; i++){
                        delete cache[cacheReference[i]];
                    }
                    util.remove(cacheReference, limitedSize, importSize - 1);
                }
            }else{
                this.__cache = {};
            }            
        },

        /**
         * Save obj to cache with a key, and set the timeout for purging.
         * @method setObject
         * @param {String} key the unique key for this cache
         * @param {Object} obj the actual data to be cached
         * @param {String} timestamp the time of calling sever to get the data or generating the cache object
         * @memberof tve/util/CacheHelper
         * @public
         */
        setObject: function(key, obj, timestamp, customTTL){
            //if the object is set to no cache, then return without saving
            if(customTTL && customTTL === CacheHelper.TTL.NO_CACHE){
                return;
            }

            timestamp = timestamp || (new Date()).getTime();

            var cacheRef = this.__cacheReference,
                currentSize = util.size(cacheRef),
                index = util.indexOf(cacheRef, key),
                ttl = customTTL || this.__ttl;

            if(index > -1){
                util.remove(cacheRef, index);
            }else if(currentSize && currentSize === this.__size){
                delete this.__cache[cacheRef[0]];
                util.remove(cacheRef, 0);
            }
            cacheRef.push(key);

            this.__cache[key] = {
                data: obj,
                timestamp: timestamp,
                expiration: timestamp + ttl * 1000 
            };

            if(customTTL){
                this.__cache[key].customTTL = customTTL;
            }
        },

        /**
         * Set the timestamp of the cached object to the current time
         * @method renewTimestamp
         * @param {String} key the unique key for this cache
         * @memberof tve/util/CacheHelper
         * @public
         */
        renewTimestamp: function(key){
            var obj = this.__cache[key],
                ttl = obj.customTTL || this.__ttl,
                time = (new Date()).getTime();

            obj.timestamp = time;
            obj.expiration = time + ttl;
        },

        /**
         * Check if the cache is expired
         * @method isExpired
         * @param {String} key the unique key for this cache
         * @return {Bool} whether it's expired
         * @memberof tve/util/CacheHelper
         * @public
         */
        isExpired: function(key){
            var obj = this.__cache[key];
            
            //if the cache is set to "forever", then return false directly.
            if(obj.customTTL && obj.customTTL === CacheHelper.TTL.FOREVER){
                return false;
            }
            
            if((new Date()).getTime() > obj.expiration){
                return true;
            }

            return false;
        },

        /**
         * Get cache for key
         * @method getCache
         * @param {String} key the unique key for this cache
         * @return {Object|undefined} the data in the cache object
         * @memberof tve/util/CacheHelper
         * @public
         */
        getCache: function(key){
            if(this.__cache.hasOwnProperty(key)){
                return this.__cache[key].data;
            }

            return undefined;
        },

        /**
         * Get timestamp for key
         * @method getTimestamp
         * @param {String} key the unique key for this cache
         * @return {string} the timestamp in the cache object
         * @memberof tve/util/CacheHelper
         * @public
         */
        getTimestamp: function(key){
            if(this.__cache.hasOwnProperty(key)){
                return this.__cache[key].timestamp;
            }

            return undefined;
        }

    });
    
    return CacheHelper;
});
/**
 * the AppGrid service layer, implements all AppGrid related services
 * which contain StatusService AnalyticsService ConfigurationService LogService
 * @name AppGridService
 * @memberof tve
 * @class tve/AppGridService
 * @augments tve/interface/AnalyticsService
 * @augments tve/interface/ConfigurationService
 * @augments tve/interface/LogService
 * @augments tve/interface/ResourceService
 * @augments tve/interface/StatusService
 * @augments tve/interface/UserSettingsService
 * @author Shawn Zeng <shawn.zeng@accedo.tv>
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/AppGridService", ["ax/class", "ax/core", "ax/util", "ax/config", "ax/promise", "ax/ajax", "ax/device", "tve/util/CacheHelper", "lib/base64", "lib/moment", "tve/TVEError", "tve/interface/AnalyticsService", "tve/interface/ConfigurationService", "tve/interface/LogService", "tve/interface/ResourceService", "tve/interface/StatusService", "tve/interface/UserSettingsService"], function(klass, core, util, config, promise, ajax, device, CacheHelper, base64, moment, TVEError, AnalyticsService, ConfigurationService, LogService, ResourceService, StatusService, UserSettingsService) {

    var FACILITY = TVEError.FACILITY,
        ERROR = TVEError.ERROR,

        AppGridService = klass.create([AnalyticsService, ConfigurationService, LogService, ResourceService, StatusService, UserSettingsService], {}, {

            /**
             * The session key for the app in AppGrid
             * @member __sessionKey
             * @memberof tve/AppGridService#
             * @private
             */
            __sessionKey: null,

            /**
             * The universally unique identifier of the app in AppGrid.
             * @member __uuid
             * @memberof tve/AppGridService#
             * @private
             */
            __uuid: null,

            /**
             * Set the sequence for all types of log levels
             * @member __logLevels
             * @memberof tve/AppGridService#
             * @private
             */
            __logLevels: ["off", "error", "warn", "debug"],


            /**
             * The expiration time of the current session which is retrived from AppGrid
             * @member __sessionExpiration
             * @memberof tve/AppGridService#
             * @private
             */
            __sessionExpiration: null,

            /**
             * AppGridService
             * Initialize a session. On successful initialization, set the sessionKey and sesionExpiration properties and the X-Session header.
             * If the session key failed to initialize, the current service method which depends on session will return reject through promise.
             * @method __getSessionKey
             * @return {Promise.<String>} Session Key
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @private
             */
            __getSessionKey: function() {
                if (this.__isSessionValid()) {
                    //when there is already a valid session, return the promise resolve with session key directly.
                    return promise.resolve(this.__sessionKey);
                }

                if (this.__getSessionPromise) {
                    //when there is already a promise for getting session, return the promise.
                    return this.__getSessionPromise;
                }

                var url = this.__baseURL + "/session",
                    options = {};

                options.requestHeaders = {};
                options.requestHeaders["X-Application-Key"] = this.__appKey;
                options.requestHeaders["X-User-Id"] = this.__uuid;

                this.__getSessionPromise = this.__sendRequest(url, options).then(util.bind(function(transport) {
                    var responseJSON = transport.responseJSON || null;

                    if (responseJSON && responseJSON.sessionKey && responseJSON.expiration) {
                        this.__sessionKey = responseJSON.sessionKey;
                        this.__sessionExpiration = moment(responseJSON.expiration, "YYYYMMDD HH:mm:SS Z").valueOf();

                        if (this.__sessionExpiration < (new Date()).getTime()) {
                            throw new TVEError(TVEError.FACILITY.GENERAL, TVEError.ERROR.INVALID, "GetSession: Unable to get the correct expiration.");
                        }

                        this.__getSessionPromise = null;

                        return this.__sessionKey;
                    }
                    this.__getSessionPromise = null;
                    throw new TVEError(TVEError.FACILITY.GENERAL, TVEError.ERROR.INVALID, "GetSession: Unable to get the session. Cannot get the sessionKey or expiration.");
                }, this), util.bind(function(reason) {
                    this.__getSessionPromise = null;
                    throw new TVEError(TVEError.FACILITY.GENERAL, TVEError.ERROR.NOT_FOUND, "GetSession: Unable to get the session.", reason);
                }, this));

                return this.__getSessionPromise;
            },

            /**
             * AppGridService
             * To check if the session is still valid. The expiration time is get from the last call of getSessionKey.
             * @method __isSessionValid
             * @return {Bool} If the session is valid
             * @memberof tve/AppGridService#
             * @private
             */
            __isSessionValid: function() {
                return (this.__sessionKey && this.__sessionExpiration > (new Date()).getTime() + 300000);
            },

            /**
             * AppGridService
             * Send a get request to AppGrid server, all the GET HTTP request to AppGrid should be sent here.
             * @method __setGetRequest
             * @param {String} url the URL to be sent
             * @param {Object} (optional) options the options passed into request function in ax/ajax
             * @param {Object} (optional) customTTL the custom setting of ttl(time to live) for the cache
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object             
             * @memberof tve/AppGridService#
             * @private
             */
            __sendGetRequest: function(url, options, customTTL) {
                options = options || {};
                options.parameters = options.parameters || ""; //the request parameters

                var key = this.__generateCacheKey(url, options.parameters),
                    timestamp = (new Date()).getTime(),
                    cacheHelper = this.__cacheHelper,
                    cacheObj = cacheHelper.getCache(key),

                    doNoCacheAction = util.bind(function() {

                        return this.__getSessionKey().then(util.bind(function(sessionKey) {
                            return this.__sendRequest(url, options, sessionKey).then(function(transport) {
                                cacheHelper.setObject(key, transport, timestamp, customTTL || CacheHelper.TTL.DEFAULT);
                                return transport;
                            });
                        },this));
                    }, this);

                if (cacheObj !== undefined && !cacheHelper.isExpired(key)) {
                    return promise.resolve(cacheObj);
                } else if (cacheObj !== undefined) {
                    //this "If-Modified-Since" currently always returns a failure -15.11.2013
                    return this.__getSessionKey().then(util.bind(function(sessionKey) {
                        return this.__sendRequest(url, {
                            requestHeaders: {
                                "If-Modified-Since": (new Date(cacheHelper.getTimestamp(key))).toUTCString()
                            }
                        }, sessionKey).then(function(transport) {
                            if (transport.status === 304) {
                                cacheHelper.renewTimestamp(key);
                                return cacheObj;
                            } else if (transport.status === 200) {
                                cacheHelper.setObject(key, transport, timestamp);
                                return transport;
                            }
                        }, function() {
                            //Currently the appgrid doesn"t accept a "If-Modeified-Since" header, which means it will always go to the onFailure block.
                            //A JIRA issue APPGRID-769 has been raised for this.
                            //Once this issue is resolved, the code in this function can be removed.
                            return doNoCacheAction();
                        });
                    }, this));
                } else {
                    return doNoCacheAction();
                }
            },

            /**
             * AppGridService
             * Send a post request to AppGrid server, all the POST HTTP request to AppGrid should be sent here.
             * @method __sendPostRequest
             * @param {String} url the URL to be sent
             * @param {Object} options the options passed into request function in ax/ajax            
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @private
             */
            __sendPostRequest: function(url, options) {
                options = options || {};
                options.method = "post";
                options.requestHeaders = options.requestHeaders || {
                    "content-type": "application/json"
                };

                return this.__getSessionKey().then(util.bind(function(sessionKey) {
                    // return the request through promise
                    return this.__sendRequest(url, options, sessionKey);
                }, this));
            },

            /**
             * Generate a cache key based on request information
             * @method __generateCacheKey
             * @param {String} path the URL for the request
             * @param {Object} parameters Request parameters, object containing key-value pairs that are convertible into key1=value1&key2=value2 format with e.g toQueryString() method
             * @param {String} requestBody Request Body object (usually present only with POST & PUT calls)
             * @return {String} the string for the cache key
             * @memberof tve/AppGridService#
             * @private
             */
            __generateCacheKey: function(path, parameters, requestBody) {
                var body = requestBody ? "__body[" + requestBody.toString() + "]" : "",
                    queryString = util.isString(parameters) ? parameters : util.toQueryString(parameters),
                    str = path + "?" + queryString + "&" + body;

                if (window.btoa) {
                    return window.btoa(str);
                } else {
                    return base64.encode(str);
                }
            },

            /**
             * AppGridService - StatusService
             * Get the status of the app to check if the app is in maintaince mode.
             * @method getStatus
             * @return {Promise.<String>} Status (e.g. Active)
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */         
            getStatus: function() {
                var url = this.__baseURL + "/status";

                return this.__sendGetRequest(url).then(function(transport) {
                    if (transport.responseJSON && transport.responseJSON.status) {
                        return transport.responseJSON.status;
                    }
                    throw new TVEError(FACILITY.STATUS_SERVICE, ERROR.INVALID, "Cannot get the status");
                }, function(reason) {
                    throw new TVEError(FACILITY.STATUS_SERVICE, ERROR.NETWORK, "Cannot get the status", reason);
                });
            },

            /**
             * AppGridService - StatusService
             * Get the message of the app from AppGrid.
             * @method getMessage
             * @returns {Promise.<String>} Status Message
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getMessage: function() {
                var url = this.__baseURL + "/status";

                return this.__sendGetRequest(url).then(function(transport) {
                    if (transport.responseJSON && transport.responseJSON.message) {
                        return transport.responseJSON.message;
                    }
                    throw new TVEError(FACILITY.STATUS_SERVICE, ERROR.INVALID, "Cannot get the message through status service");
                }, function(reason) {
                    throw new TVEError(FACILITY.STATUS_SERVICE, ERROR.NETWORK, "Cannot get the message through status service", reason);
                });
            },

            /**
             * AppGridService - AnalyticsService
             * Send the application start event log to analytics service on AppGrid.
             * @method applicationStart
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object             
             * @memberof tve/AppGridService#
             * @public
             */
            applicationStart: function() {
                return this.__sendApplicationEvent("START").then(function(transport){return;});
            },

            /**
             * AppGridService - AnalyticsService
             * Send the application stop event log to analytics service on AppGrid.
             * @method applicationStop
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object             
             * @memberof tve/AppGridService#
             * @public
             */
            applicationStop: function() {
                return this.__sendApplicationEvent("QUIT").then(function(transport){return;});
            },

            /**
             * AppGridService - ConfigurationService
             * Get the config in app config file on AppGrid by the given key.
             * @method getConfig
             * @param {String} key
             * @param {String} defaultValue (optional) it fails to get the data, it will return the default value
             * @returns {Promise.<String>} Configuration value by key
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getConfig: function(key, defaultValue) {
                var url = this.__baseURL + "/metadata/" + key;

                return this.__sendGetRequest(url).then(function(transport) {
                    if (transport.responseJSON && transport.responseJSON[key]) {
                        return transport.responseJSON[key];
                    } else if (defaultValue) {
                        return defaultValue;
                    }
                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.INVALID, "cannot get the config value");
                }, function(reason) {
                    if (defaultValue) {
                        return promise.resolve(defaultValue);
                    }
                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.NETWORK, "cannot get the config value", reason);
                });
            },

            /**
             * AppGridService - ConfigurationService
             * Get the configs in app config file on AppGrid by the given keys.
             * @method getConfigs
             * @param {Array} keys
             * @param {Object} defaultValues (optional) it fails to get the data, it will return the default values
             * If it only gets parts of the data, the new data will merge into default values and return the merged data.
             * @returns {Promise.<Object>} Configuration value map by keys
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getConfigs: function(keys, defaultValues) {
                var url = this.__baseURL + "/metadata/" + keys.join(",");

                return this.__sendGetRequest(url, null, CacheHelper.TTL.FOREVER).then(function(transport) {
                    if (transport.responseJSON) {
                        if (defaultValues) {
                            return util.extend(defaultValues, transport.responseJSON);
                        }
                        return transport.responseJSON;
                    }

                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.INVALID, "cannot get values of configs");
                }, function(reason) {
                    if (defaultValues) {
                        return promise.resolve(defaultValues);
                    }
                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.NETWORK, "cannot get values of configs", reason);
                });
            },

            /**
             * AppGridService - ConfigurationService
             * Get all the config in app config file on AppGrid.
             * @method getAllConfigs
             * @returns {Promise.<Object>} Configuration value map by keys
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getAllConfigs: function() {
                var url = this.__baseURL + "/metadata";

                return this.__sendGetRequest(url).then(function(transport) {
                    if (transport.responseJSON) {
                        return transport.responseJSON;
                    }
                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.INVALID, "cannot get all configs");
                }, function(reason) {
                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.NETWORK, "cannot get all configs", reason);
                });
            },

            /**
             * AppGridService - ResourceService
             * Get the resource on AppGrid by the given key.
             * @method getResource
             * @param {String} key
             * @returns {Promise.<String>} Resource value by key
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getResource: function(key) {
                return this.getAllResources().then(util.bind(function(transport) {
                    if (transport[key]) {
                        var url = transport[key];

                        if (url.indexOf("/") === 0) {
                            return this.__baseURL + url;
                        }
                        return url;
                    } else {
                        throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.INVALID, "cannot get the requested resource");
                    }
                }, this), function(reason) {
                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.NETWORK, "cannot get the requested resource", reason);
                });
            },

            /**
             * AppGridService - ResourceService
             * Get all the resources on AppGrid.
             * @method getAllResources
             * @returns {Promise.<Object>} Resource values map by keys
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getAllResources: function() {
                if (this.__getAllResourcesPromise) {
                    //when there"s already a promise for getting all resources, return the promise.
                    return this.__getAllResourcesPromise;
                }

                var url = this.__baseURL + "/asset";

                this.__getAllResourcesPromise = this.__sendGetRequest(url).then(function(transport) {
                    this.__getAllResourcesPromise = null;

                    if (transport.responseJSON) {
                        return transport.responseJSON;
                    }
                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.INVALID, "cannot get all resources");
                }, function(reason) {
                    this.__getAllResourcesPromise = null;
                    throw new TVEError(FACILITY.CONFIGURATION_SERVICE, TVEError.ERROR.NETWORK, "cannot get all resources", reason);
                });

                return this.__getAllResourcesPromise;
            },

            /**
             * AppGridService - LogService
             * Get the level of the log service.
             * @method getLevel
             * @returns {Promise.<String>} Returns log level for application. 'error', 'warn', 'debug' or 'off' if log disabled
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getLevel: function() {
                //If there"s already a promise to get the level, then return this promise directly.
                if (this.__getLevelPromise) {
                    return this.__getLevelPromise;
                }

                var url = this.__baseURL + "/application/log/level";

                this.__getLevelPromise = this.__sendGetRequest(url).then(util.bind(function(transport) {
                    if (transport.responseJSON && transport.responseJSON.logLevel) {
                        this.__getLevelPromise = null;
                        return transport.responseJSON.logLevel;
                    }
                    this.__getLevelPromise = null;
                    throw new TVEError(FACILITY.LOG_SERVICE, ERROR.INVALID, "Cannot get the log level");
                }, this), util.bind(function(reason) {
                    this.__getLevelPromise = null;
                    throw new TVEError(FACILITY.LOG_SERVICE, ERROR.NETWORK, "Cannot get the log level", reason);
                }, this));

                return this.__getLevelPromise;
            },

            /**
             * AppGridService - LogService
             * Send a debugging log message to log service
             * @method debug
             * @memberof tve/AppGridService#
             * @param {String} message the message to log
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @public
             */
            debug: function(message) {
                this.__sendLog("debug", message);
            },

            /**
             * AppGridService - LogService
             * Send a warning log message to log service
             * @method warn
             * @memberof tve/AppGridService#
             * @param {String} message the message to log
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @public
             */
            warn: function(message) {
                this.__sendLog("warn", message);
            },

            /**
             * AppGridService - LogService
             * Send a error log message to log service
             * @method error
             * @memberof tve/AppGridService#
             * @param {String} message the message to log
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @public
             */
            error: function(message) {
                this.__sendLog("error", message);
            },

            /**
             * Get the setting of user on AppGrid by the given key or get all settings directly. Helper method for the public functions.
             * @method __getSettingHelper
             * @param {String} scope should be "user" or "group"
             * @param {Array} keys the key(s) of the requested objects
             * @param {Object|String} [defaultValue] (optional) if it fails to get the data, it will return the default values
             * @returns {Promise.<Object>} JSON object
             * @throws {Promise.<tve/TVEError>} Internal error object               
             * @memberof tve/AppGridService#
             * @private
             */
            __getSettingHelper: function(scope, keys, defaultValue) {
                var url = this.__baseURL + "/" + scope + "/" + this.__uuid,
                    length = 0;

                if (keys) {
                    length = keys.length;
                    url += "/" + keys.join(",");
                }

                return this.__sendGetRequest(url, null, CacheHelper.TTL.NO_CACHE).then(function(transport) {
                    if (!length && transport.responseJSON) {
                        return transport.responseJSON;
                    }

                    if (length === 1 && transport.responseJSON) {
                        return transport.responseJSON;
                    }

                    if (length > 1 && transport.responseJSON) {
                        if (defaultValue) {
                            return util.extend(defaultValue, transport.responseJSON);
                        }
                        return transport.responseJSON;
                    }

                    if (defaultValue) {
                        return defaultValue;
                    }

                    throw new TVEError(FACILITY.USER_SETTINGS_SERVICE, ERROR.INVALID, "cannot get requested setting");
                }, function(reason) {
                    if (defaultValue && length) {
                        return promise.resolve(defaultValue);
                    }
                    throw new TVEError(FACILITY.USER_SETTINGS_SERVICE, ERROR.NETWORK, "cannot get requested setting", reason);
                });
            },

            /**
             * Get the setting of the user in an application scope on AppGrid by the given key.
             * @method getSetting
             * @param {String} key the key of the requested object
             * @param {String} [defaultValue] (optional) if it fails to get the data, it will return the default values
             * @returns {Promise.<String>} User setting value by key
             * @throws {Promise.<tve/TVEError>} Internal error object             
             * @memberof tve/AppGridService#
             * @public
             */
            getSetting: function(key, defaultValue) {
                return this.__getSettingHelper("user", [key], defaultValue);
            },

            /**
             * Get the setting of the user in a group scope on AppGrid by the given key.
             * @method getSharedSetting
             * @param {String} key the key of the requested object
             * @param {String} [defaultValue] (optional) if it fails to get the data, it will return the default values
             * @returns {Promise.<String>} User group setting value by key
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getSharedSetting: function(key, defaultValue) {
                return this.__getSettingHelper("group", [key], defaultValue);
            },

            /**
             * Get more the one settings of the user in an application scope on AppGrid by given keys.
             * @method getSettings
             * @param {Array} keys the keys of the requested objects
             * @param {Object} [defaultValues] (optional) if it fails to get the data, it will return the default values.
             * If it only gets parts of the data, the new data will merge into default values and return the merged data.
             * @returns {Promise.<Object>} User settings values map by keys
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getSettings: function(keys, defaultValues) {
                return this.__getSettingHelper("user", keys, defaultValues);
            },

            /**
             * Get more the one settings of the user in a group scope on AppGrid by given keys.
             * @method getSharedSetting
             * @param {Array} keys the keys of the requested objects
             * @param {Object} [defaultValues] (optional) if it fails to get the data, it will return the default values
             * If it only gets parts of the data, the new data will merge into default values and return the merged data.
             * @returns {Promise.<Object>} User group settings values map by keys
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getSharedSettings: function(keys, defaultValues) {
                return this.__getSettingHelper("group", keys, defaultValues);
            },

            /**
             * Get all the settings of the user in an application scope on AppGrid.
             * @method getAllSettings
             * @returns {Promise.<Object>} User settings values map by keys
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getAllSettings: function() {
                return this.__getSettingHelper("user");
            },

            /**
             * Get all the settings of the user in a group scope on AppGrid.
             * @method getAllSharedSettings
             * @returns {Promise.<Object>} User group settings values map by keys
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            getAllSharedSettings: function() {
                return this.__getSettingHelper("group");
            },

            /**
             * Set the setting of the user on AppGrid by the given key and value. Helper method for the public functions.
             * @method __setSettingHelper
             * @param {String} scope should be "user" or "group"
             * @param {Object} data the key and value to be set
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @private
             */
            __setSettingHelper: function(scope, data) {
                var options = {},
                    keys = util.keys(data),
                    count = keys.length,
                    url = this.__baseURL + "/" + scope + "/" + this.__uuid;

                if (count === 1) {
                    url += "/" + keys[0];
                    options.postBody = data[keys[0]];
                } else {
                    options.postBody = JSON.stringify(data);
                }

                return this.__sendPostRequest(url, options).fail(function(reason) {
                    throw new TVEError(FACILITY.USER_SETTINGS_SERVICE, ERROR.NETWORK, "cannot set the setting", reason);
                });
            },

            /**
             * Set the setting of the user in an application scope on AppGrid by the given key and value.
             * @method setSetting
             * @param {String} key the key of the object to be set
             * @param {String} value the value to set to the key
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            setSetting: function(key, value) {
                var data = {};
                data[key] = value;
                return this.__setSettingHelper("user", data).then(function(transport){return;});
            },

            /**
             * Set the setting of the user in a group scope on AppGrid by the given key and value.
             * @method setSharedSetting
             * @param {String} key the key of the object to be set
             * @param {String} value the value to set to the key
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            setSharedSetting: function(key, value) {
                var data = {};
                data[key] = value;
                return this.__setSettingHelper("group", data).then(function(transport){return;});
            },

            /**
             * Set more than one settings of the user in an application scope on AppGrid by the given keys and their values.
             * Please note it will rewrite the settings
             * @method setSettings
             * @param {Object} object keys and the value to set to the key
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            setAllSettings: function(data) {
                return this.__setSettingHelper("user", data).then(function(transport){return;});
            },

            /**
             * Set the setting of the user in a group scope on AppGrid by the given keys and their values.
             * Please note it will rewrite the settings
             * @method setSharedSettings
             * @param {Object} object keys and the value to set to the key
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            setAllSharedSettings: function(data) {
                return this.__setSettingHelper("group", data).then(function(transport){return;});
            },

            /**
             * Init the AppGridService moudle
             * @method init
             * @param {String} api the base URL for the AppGrid server
             * @param {String} appKey the app key for the app
             * @param {String} uuid Universally unique identifier of the device.
             * @memberof tve/AppGridService#
             * @private
             */
            init: function(api, appKey, uuid) {
                this.__baseURL = api;
                this.__appKey = appKey;
                this.__uuid = uuid;
                this.__cacheHelper = new CacheHelper();
            },

            /**
             * Send ajax request, all http requests of AppGridService should be sent here.
             * @method __sendRequest
             * @param {String} url the URL to be sent
             * @param {Object} options the options passed into request function in ax/ajax
             * @param {String} sessionKey "X-Session" in the header
             * @memberof tve/AppGridService#
             * @returns {Promise.<Undefined>} fulfilled with no return value          
             * @private
             */
            __sendRequest: function(url, options, sessionKey) {
                options = options || {};
                options.requestHeaders = options.requestHeaders || {};
                if (sessionKey) {
                    options.requestHeaders["X-Session"] = sessionKey;
                }
                return ajax.request(url, options);
            },

            /**
             * For usage tracking, send the application event to analytics service on AppGrid.
             * @method __sendApplicationEvent
             * @memberof tve/AppGridService#
             * @param {String} message the predefined events have: START, QUIT
             * @returns {Promise.<Undefined>} fulfilled with no return value
             * @throws {Promise.<tve/TVEError>} Internal error object             
             * @private
             */
            __sendApplicationEvent: function(message) {
                var url = this.__baseURL + "/event/log",
                    postBody = {},
                    options = {};

                postBody.eventType = message;
                options.postBody = JSON.stringify(postBody);

                return this.__sendPostRequest(url, options).fail(function(reason) {
                    throw new TVEError(FACILITY.ANALYTICS_SERVICE, ERROR.NETWORK, "Cannot send the application event", reason);
                });
            },

            /**
             * Send log to AppGrid, such as error, warn and debug.
             * If the log level is in a lower priority than the current level get from the AppGrid, it will not sent the request.
             * @method __sendLog
             * @memberof tve/AppGridService#
             * @param {String} logType the log type which should be "debug, "error" or "warn"
             * @param {String} message the message to log
             * @throws {Promise.<tve/TVEError>} Internal error object              
             * @private
             */
            __sendLog: function(logType, message) {
                var levels = this.__logLevels;

                this.getLevel().then(util.bind(function(level) {
                    if (util.indexOf(levels, logType) <= util.indexOf(levels, level)) {
                        var options = {},
                            postBody = {},
                            url = this.__baseURL + "/application/log/" + logType;

                        postBody.message = message;
                        options.postBody = JSON.stringify(postBody);

                        this.__sendPostRequest(url, options);
                    }
                }, this), function(reason) {
                    throw new TVEError(FACILITY.LOG_SERVICE, ERROR.NETWORK, "Cannot send the log", reason);
                });
            },
            


            //functions below are added only for TVE Demo
            
            /**
             * A api wrapper for the SIT get
             * @method sitGet
             * @param {String} the end part of url for SIT
             * @returns {Promise.<XMLHttpRequest>} XMLHttpRequest object
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            sitGet: function(url) {
                var url = this.__baseURL + "/plugin/sit/" + url;

                return this.__sendGetRequest(url, null, CacheHelper.TTL.NO_CACHE);
            },

            /**
             * A api wrapper for the SIT post
             * @method sitPost
             * @param {String} the end part of url for SIT
             * @param {Object} postbody
             * @returns {Promise.<XMLHttpRequest>} XMLHttpRequest object
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            sitPost: function(url, postBody) {
                var url = this.__baseURL + "/plugin/sit/" + url,
                    options = {
                        parameters: postBody,
                        requestHeaders: {
                            "content-type": "application/x-www-form-urlencoded"
                        }
                    };

                return this.__sendPostRequest(url, options);
            },

            /**
             * A api wrapper for the SIT delete
             * @method sitPost
             * @param {String} the end part of url for SIT
             * @returns {Promise.<XMLHttpRequest>} XMLHttpRequest object
             * @throws {Promise.<tve/TVEError>} Internal error object
             * @memberof tve/AppGridService#
             * @public
             */
            sitDelete: function(url) {
                var url = this.__baseURL + "/plugin/sit/" + url,
                    options = {
                        method: "delete"
                    };

                return this.__getSessionKey().then(util.bind(function(sessionKey) {
                    // return the request through promise
                    return this.__sendRequest(url, options, sessionKey);
                }, this));
            },            
            
            /**
             * Set the UUID. FOR DEMO ONLY.
             * @method setUUID
             * @param {String} uuid the uuid
             * @memberof tve/AppGridService#
             * @public
             */
            setUUID: function(uuid) {
                if (uuid !== this.__uuid) {
                    this.__uuid = uuid;
                    this.__sessionExpiration = 0;
                }
            }            
        });

    return AppGridService;
});
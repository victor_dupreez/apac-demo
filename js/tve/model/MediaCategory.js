/**
 * Category Model
 * @name MediaCategory
 * @memberof tve/model
 * @class tve/model/MediaCategory
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/model/MediaCategory", ["ax/class", "ax/af/mvc/Model"], function(klass, Model) {
    return klass.create(Model, {
        defaults: {
            /**
             * @member id
             * @memberof tve/model/MediaCategory#
             * @public
             * @type {String}
             */
            "id": "",
            /**
             * @member title
             * @memberof tve/model/MediaCategory#
             * @public
             * @type {String}
             */
            "title": "",
            /**
             * @member description
             * @memberof tve/model/MediaCategory#
             * @public
             * @type {String}
             */
            "description": "",
            /**
             * @member categories
             * @memberof tve/model/MediaCategory#
             * @public
             * @type {tve/model/MediaCategory}
             */
            "categories": [ // a list of sub-categories, without further elaboration on sub-categories
                // {
                //     "id": "",
                //     "name": "",
                //     "description": ""
                // }
            ]
        }
    }, {});
});
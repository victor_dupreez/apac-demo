/**
 * TVListing Model
 * @name TVListing
 * @memberof tve/model
 * @class tve/model/TVListing
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/model/TVListing", ["ax/class", "ax/af/mvc/Model"], function(klass, Model) {
    return klass.create(Model, {
        defaults: {
            /**
             * @member channelId
             * @memberof tve/model/TVListing#
             * @public
             * @type {String}
             */
            "channelId": "",
            /**
             * @member id
             * @memberof tve/model/TVListing#
             * @public
             * @type {tve/model/Asset[]}
             */
            "programs": [
                // {program object}
            ]
        }
    }, {});
});
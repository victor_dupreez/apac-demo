/**
 * TVShow Model
 * @name TVShow
 * @memberof tve/model
 * @class tve/model/TVShow
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/model/TVShow", ["ax/class", "ax/af/mvc/Model"], function(klass, Model) {
    return klass.create(Model, {
        defaults: {
            /**
             * @member id
             * @memberof tve/model/TVShow#
             * @public
             * @type {String}
             */
            "id": "",
            /**
             * @member title
             * @memberof tve/model/TVShow#
             * @public
             * @type {String}
             */
            "title": "",
            /**
             * @member description
             * @memberof tve/model/TVShow#
             * @public
             * @type {String}
             */
            "description": "",
            /**
             * @member categories
             * @memberof tve/model/TVShow#
             * @public
             * @type {tve/model/Category[]}
             */
            "categories": [
                // {category object}
            ],
            /**
             * @member images
             * @memberof tve/model/TVShow#
             * @public
             * @type {tve/model/Asset[]}
             */
            "images": [
                // {image object}
            ],
            /**
             * @member videos
             * @memberof tve/model/TVShow#
             * @public
             * @type {tve/model/Asset[]}
             */
            "videos": [
                // {video object}
            ],
            /**
             * @member episodes
             * @memberof tve/model/TVShow#
             * @public
             * @type {tve/model/Episode[]}
             */
            "episodes": [
                // {episode object}
            ],
            /**
             * @member publishedDate
             * @memberof tve/model/TVShow#
             * @public
             * @type {Number}
             */
            "publishedDate": 0, // timestamp
            /**
             * @member availableDate
             * @memberof tve/model/TVShow#
             * @public
             * @type {Number}
             */
            "availableDate": 0 // timestamp
        }
    }, {});
});
/**
 * Asset Model
 * @name Asset
 * @memberof tve/model
 * @class tve/model/Asset
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/model/Asset", ["ax/class", "ax/af/mvc/Model"], function(klass, Model) {
    return klass.create(Model, {
        /**
         * Available TYPE enum for Asset Model
         * @typedef {Object} TYPE
         * @public
         * @memberof tve/model/Asset
         */
        TYPE: {
            EPISODE: 1,
            MOVIE: 2,
            PROGRAM: 3
        },
        defaults: {
            /**
             * @member type
             * @memberof tve/model/Asset#
             * @public
             * @type {tve/model/Asset#TYPE}
             */
            "type": 0, // Asset.TYPE.EPISODE, Asset.TYPE.MOVIE, Asset.TYPE.PROGRAM
            /**
             * @member id
             * @memberof tve/model/Asset#
             * @public
             * @type {String}
             */
            "id": "",
            /**
             * @member title
             * @memberof tve/model/Asset#
             * @public
             * @type {String}
             */
            "title": "",
            /**
             * @member description
             * @memberof tve/model/Asset#
             * @public
             * @type {String}
             */
            "description": "",
            /**
             * @member publishedDate
             * @memberof tve/model/Asset#
             * @public
             * @type {Number}
             */
            "publishedDate": 0, // timestamp
            /**
             * @member availableDate
             * @memberof tve/model/Asset#
             * @public
             * @type {Number}
             */
            "availableDate": 0, // timestamp
            /**
             * @member credits
             * @memberof tve/model/Asset#
             * @public
             * @type {Object}
             */
            "credits": {
                // Map
            },
            /**
             * @member categories
             * @memberof tve/model/Asset#
             * @public
             * @type {tve/model/Category[]}
             */
            "categories": [
                // {Category object}
            ],
            /**
             * @member images
             * @memberof tve/model/Asset#
             * @public
             * @type {tve/model/Resource[]}
             */
            "images": [
                // {image object}
            ],
            /**
             * @member videos
             * @memberof tve/model/Asset#
             * @public
             * @type {tve/model/Resource[]}
             */
            "videos": [
                // {video object}
            ],
            /**
             * @member parentalRatings
             * @memberof tve/model/Asset#
             * @public
             * @type {Array}
             */
            "parentalRatings": [ // "PG", "MA", etc
                ""
            ],
            /**
             * @member metadata
             * @memberof tve/model/Asset#
             * @public
             * @type {Array}
             */
            "metadata": { // additional metadata
                // 
                // "tvShowId": "9",
                // "tvShowTitle": "Game of Thrones",
                // "language": "en" // ISO 639-1 lang code
                // 
            }
        }

    }, {});
});
/**
 * Resource Model
 * @name Resource
 * @memberof tve/model
 * @class tve/model/Resource
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/model/Resource", ["ax/class", "ax/af/mvc/Model"], function(klass, Model) {
    return klass.create(Model, {
        defaults: {
            /**
             * @member id
             * @memberof tve/model/Resource#
             * @public
             * @type {String}
             */
            "id": "",
            /**
             * @member url
             * @memberof tve/model/Resource#
             * @public
             * @type {String}
             */
            "url": "",
            /**
             * @member mimeType
             * @memberof tve/model/Resource#
             * @public
             * @type {String}
             */
            "mimeType": "",
            /**
             * @member language
             * @memberof tve/model/Resource#
             * @public
             * @type {String}
             */
            "language": "", // ISO-639-1 lang code
            /**
             * @member metadata
             * @memberof tve/model/Resource#
             * @public
             * @type {Array}
             */
            "metadata": { // additional metadata
                // 
                // "tvShowId": "9",
                // "tvShowTitle": "Game of Thrones",
                // "language": "en" // ISO 639-1 lang code
                // 
            },
            /**
             * @member duration
             * @memberof tve/model/Resource#
             * @public
             * @type {Integer}
             */
            "duration": 0,
            /**
             * @member geoLock
             * @memberof tve/model/Resource#
             * @public
             * @type {Boolean}
             */
            "geoLock": false,
            /**
             * @member width
             * @memberof tve/model/Resource#
             * @public
             * @type {Integer}
             */
            "width": 0,
            /**
             * @member height
             * @memberof tve/model/Resource#
             * @public
             * @type {Integer}
             */
            "height": 0
        }
    }, {});
});
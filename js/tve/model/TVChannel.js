/**
 * TVChannel Model
 * @name TVChannel
 * @memberof tve/model
 * @class tve/model/TVChannel
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/model/TVChannel", ["ax/class", "ax/af/mvc/Model"], function(klass, Model) {
    return klass.create(Model, {
        defaults: {
            /**
             * @member id
             * @memberof tve/model/TVChannel#
             * @public
             * @type {String}
             */
            "id": "",
            /**
             * @member title
             * @memberof tve/model/TVChannel#
             * @public
             * @type {String}
             */
            "title": "",
            /**
             * @member channelNumber
             * @memberof tve/model/TVChannel#
             * @public
             * @type {String}
             */
            "channelNumber": "",
            /**
             * @member description
             * @memberof tve/model/TVChannel#
             * @public
             * @type {String}
             */
            "description": "",
            /**
             * @member categories
             * @memberof tve/model/TVChannel#
             * @public
             * @type {tve/model/Category[]}
             */
            "categories": [
                // {category object}
            ],
            /**
             * @member images
             * @memberof tve/model/TVChannel#
             * @public
             * @type {tve/model/Asset[]}
             */
            "images": [
                // {image object}
            ],
            /**
             * @member videos
             * @memberof tve/model/TVChannel#
             * @public
             * @type {tve/model/Asset[]}
             */
            "videos": [
                // {video object}
            ],
            /**
             * @member streams
             * @memberof tve/model/TVChannel#
             * @public
             * @type {tve/model/Asset[]}
             */
            "streams": [
                // {video object}
            ],
            /**
             * @member listings
             * @memberof tve/model/TVChannel#
             * @public
             * @type {tve/model/TVListing[]}
             */
            "listings": [
                // {tvlisting objects}
            ],
            /**
             * @member geoLock
             * @memberof tve/model/TVChannel#
             * @public
             * @type {Boolean}
             */
            "geoLock": false,
            /**
             * @member language
             * @memberof tve/model/TVChannel#
             * @public
             * @type {String}
             */
            "language": "", // ISO 639-1 lang code
            /**
             * @member metadata
             * @memberof tve/model/TVChannel#
             * @public
             * @type {Array}
             */
            "metadata": {} // additional metadata, in array of key-value pair
        }
    }, {});
});
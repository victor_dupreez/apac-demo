/**
 * The OVP service layer (thePlatform), implements all OVP related services
 * which contain LinearContentService VODContentService
 * @name ThePlatformOvpService
 * @memberof tve
 * @class tve/ThePlatformOvpService
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("tve/ThePlatformOvpService", ["ax/ax", "ax/config", "tve/util/CacheHelper", "tve/model/MediaCategory", "tve/model/Asset", "tve/model/TVChannel", "tve/model/TVListing", "tve/model/TVShow", "tve/model/Resource", "tve/TVEError", "tve/interface/VODContentService", "tve/interface/LinearContentService", "lib/moment"],
    function(ax, config, CacheHelper, Category, Asset, TVChannel, TVListing, TVShow, Resource, TVEError, VODContentService, LinearContentService, moment) {

        var FACILITY = TVEError.FACILITY,
            ERROR = TVEError.ERROR,

            TWO_DAYS_TIME = 1000 * 60 * 60 * 24 * 2,

            MEDIA_URL = "http://data.media.theplatform.com/media/data",



            //DEMO MOCK FEEDS
            MOVIE_CAT_ID = "9768515931",

            TVSHOW_CAT_ID = "9768515932",
            EPISODE_CAT_ID = "11240515733",

            MOVIE_CAT_FID = MEDIA_URL + "/Category/" + MOVIE_CAT_ID,
            TVSHOW_CAT_FID = MEDIA_URL + "/Category/" + TVSHOW_CAT_ID,
            EPISODE_CAT_FID = MEDIA_URL + "/Category/" + EPISODE_CAT_ID,


            //FEEDS
            FEED_BASE = "http://feed.theplatform.com/f/xBrLJC",

            MEDIA_FEED = FEED_BASE + "/DwE21wX1pGqZ",

            MOVIE_FEED = FEED_BASE + "/nmCd_vOgHt_8",

            TVSHOW_FEED = FEED_BASE + "/46XsbNAyxahn",

            EPISODE_FEED = FEED_BASE + "/DwE21wX1pGqZ",

            //SORT BY
            SORT_BY = {
                MOST_VIEWED: "default",
                MOST_RATED: ":starRating|desc",
                LATEST: "pubDate|desc",
                A_TO_Z: "title|asc",
            },



            //
            LINEAR_URL = "http://data.entertainment.tv.theplatform.com/entertainment/data",

            CHANNEL_ID_BASE = LINEAR_URL + "/ChannelSchedule",


            //LINEAR feed

            LINEAR_FEED_BASE = "http://feed.entertainment.tv.theplatform.com/f/xBrLJC",

            LINEAR_FEED = LINEAR_FEED_BASE + "/accedo-grid",

            LINEAR_FEED_PARAM = {
                byLocationId: LINEAR_URL + "/Location/14952486465",
                fields: "id,title,guid,channelNumber,listings,stations.title,stations.shortTitle,listings.startTime,listings.endTime,listings.program,listings.contentRatings,listings.programId,listings.seriesId,listings.guid,listings.program.description,listings.program.title,listings.program.guid,listings.program.programType,listings.program.secondaryTitle,stations.tuningInstruction,stations.thumbnails",
                range: "1-30"
            },

            ALL_CHANNELS_START_TIME = moment().subtract('days', 1).format("YYYY-MM-DD"),
            ALL_CHANNELS_END_TIME = moment().format("YYYY-MM-DD"),



            /**
             * private API functions for OVP related service.
             * @member ovpServiceApi
             * @memberof tve/ThePlatformOvpService#
             * @private
             */
            ovpServiceApi = {
                cacheHelper: null,
                baseURL: "",
                movieURL: "",
                tvshowURL: "",
                episodesURL: "",
                categoryURL: "",
                linearURL: "",
                mediaURL: "",

                tvListingStandardParms: "",
                channelStandardParams: "",

                init: function(feeds, cacheHelper) {

                    this.mediaFeed = feeds.media;



                    // this.movieURL = this.baseURL + endpoints.movie;
                    // this.tvshowURL = this.baseURL + endpoints.tvshow;
                    // this.episodeURL = this.baseURL + endpoints.episode;


                    // this.categoryURL = this.baseURL + "/S5aJeXR8giur/categories?form=cjson&pretty=true"
                    // MEDIA_URL = "http://data.media.theplatform.com/media/data";
                    // this.linearURL = ovpConfig.linearURL;

                    this.tvListingStandardParms = "form=cjson&range=1-50&byLocationId=http%3A%2F%2Fdata.entertainment.tv.theplatform.com%2Fentertainment%2Fdata%2FLocation%2F14952486465&fields=id%2Ctitle%2Cguid%2CchannelNumber%2Clistings%2Ctitle%2Cstations.title%2Cstations.shortTitle%2Clistings.startTime%2Clistings.endTime%2Clistings.program%2Clistings.contentRatings%2Clistings.programId%2Clistings.seriesId%2Clistings.guid%2Clistings.program.description%2Clistings.program.title%2Clistings.program.guid%2Clistings.program.programType%2Clistings.program.secondaryTitle%2Cstations.tuningInstruction,stations.thumbnails&pretty=true";
                    // this.channelStandardParams = "form=cjson&range=1-50&byLocationId=http%3A%2F%2Fdata.entertainment.tv.theplatform.com%2Fentertainment%2Fdata%2FLocation%2F14618150680&byListingTime=2013-12-19T19%3A00%3A00Z~2013-12-20T19%3A00%3A00Z&fields=id,title,stations,stations.shortTitle,stations.tuningInstruction,stations.title";

                    this.cacheHelper = cacheHelper;
                },



                // ==== IVODContentService ====

                categoryRequest: function(opts) {

                    var url = MEDIA_FEED + "/categories",
                        ids = opts.id,
                        parentIds = opts.parentId,
                        param = param || {},
                        i;


                    if (parentIds) {

                        if (ax.util.isArray(parentIds)) {
                            for (i = 0; i < parentIds.length; i++) {
                                parentIds[i] = parentIds[i].replace(MEDIA_URL + "/Category/", '');
                            }
                            parentIds = parentIds.join ? parentIds.join("|") : ids;
                        } else if (ax.util.isString(parentIds)) {
                            parentIds = parentIds.replace(MEDIA_URL + "/Category/", '');
                        }

                        param.byParentId = parentIds;
                    }

                    // by Ids
                    if (ids) {

                        if (ax.util.isArray(ids)) {
                            for (i = 0; i < ids.length; i++) {
                                ids[i] = ids[i].replace(MEDIA_URL + "/Category/", '');
                            }
                            ids = ids.join ? ids.join(",") : ids;
                        } else if (ax.util.isString(ids)) {
                            ids = ids.replace(MEDIA_URL + "/Category/", '');
                        }

                        // put parameters
                        param.byId = ids;

                    }

                    return this.request(url, opts, param);
                },

                mediaRequest: function(opts, parameters) {
                    opts = opts || {};

                    parameters = parameters || {};
                    var url = opts.feed,
                        ids = opts.id,
                        categoryIds = opts.categoryId,
                        i;

                    // by Ids
                    if (ids) {
                        if (ax.util.isArray(ids)) {
                            for (i = 0; i < ids.length; i++) {
                                ids[i] = ids[i].replace(MEDIA_URL + "/Media/", '');
                            }
                            ids = ids.join("|");
                        } else {
                            ids = ids.replace(MEDIA_URL + "/Media/", '');
                        }
                        // put parameters
                        parameters.byId = ids;
                    }


                    // by categories handling
                    if (categoryIds) {
                        if (ax.util.isArray(categoryIds)) {
                            for (i = 0; i < categoryIds.length; i++) {
                                categoryIds[i] = categoryIds[i].replace(MEDIA_URL + "/Category/", '');
                            }

                            categoryIds = categoryIds.join("|");
                        } else {
                            categoryIds = categoryIds.replace(MEDIA_URL + "/Category/", '');
                        }

                        parameters.byCategoryIds = categoryIds;
                    }

                    // sorting and ignore default 
                    if (opts.sortBy && SORT_BY[opts.sortBy] != "default") {

                        parameters.sort = SORT_BY[opts.sortBy];

                    }

                    if (opts.keywords) {
                        parameters.q = opts.keywords.split(',').join('|');
                    }


                    return this.request(url, opts, parameters);
                },

                episodeRequest: function(opts) {

                    opts = opts || {};
                    var seriesRef = opts.seriesRef,
                        parameters = parameters || {};

                    if (seriesRef) {
                        parameters.byCustomValue = "{seriesRef}{" + seriesRef + "}";
                    }

                    return this.mediaRequest(opts, parameters);
                },
                // ==== ILinearContentService ====
                tvListingRequest: function(opts) {
                    opts = opts || {};
                    var url = LINEAR_FEED,
                        ids = opts.channelId,
                        param = ax.util.clone(LINEAR_FEED_PARAM);

                    if (!opts.startTime || !opts.endTime) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no startime or endtime is provided in OVP API"));
                    }

                    if (opts.endTime - opts.startTime > TWO_DAYS_TIME) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "the time range for thePlatform's tvListing must not exceed 2 days time"));
                    }

                    //convert timestamps to ISO Strings
                    var isoStartTime = new Date(opts.startTime).toISOString(),
                        isoEndTime = new Date(opts.endTime).toISOString();


                    isoStartTime = new Date("2013-12-23").toISOString();
                    isoEndTime = new Date("2013-12-24").toISOString();

                    // put listing time putarameters
                    param.byListingTime = isoStartTime + "~" + isoEndTime;

                    // by Ids
                    if (ids) {
                        ids = ids.join ? ids.join() : ids;
                        url += "/" + ids;
                    }

                    return this.request(url, opts, param);
                },


                request: function(url, opts, param) {

                    var deferred = ax.promise.defer(),
                        __opts,
                        __cacheHelper = this.cacheHelper,
                        cached_key = JSON.stringify({
                            "url": url,
                            "opts": opts
                        }),
                        cached_data = __cacheHelper.getCache(cached_key);


                    if (!ax.util.isUndefined(cached_data) && !__cacheHelper.isExpired(cached_key)) {
                        // if not expired, resolve with cache data
                        console.info("From Cache");
                        return deferred.resolve(cached_data);

                    }

                    param.form = "cjson";

                    __opts = {
                        method: "get",
                        parameters: param,
                    };


                    return ax.ajax.request(url, __opts).then(
                        function(transport) {

                            var responseJSON = transport.responseJSON || JSON.parse(transport.responseText);
                            // save to cache
                            __cacheHelper.setObject(responseJSON, cached_key, +new Date());
                            return responseJSON;

                        },
                        function(reason) {
                            throw new TVEError(FACILITY.GENERAL, ERROR.NETWORK, reason.transport ? reason.transport.statusText : "network issue", reason);
                        });
                }
            },
            /**
             * parser functions for OVP related service, parse to model objects.
             * @member ovpServiceParser
             * @memberof tve/ThePlatformOvpService#
             * @private
             */
            ovpServiceParser = (function() {


                var exportObj = {},
                    MOVIE_VIDEO = "iPad",

                    MOVIE_IMAGES = {
                        THUMBNAIL: "thumbnail",
                        COVER: "cover",
                        BANNER: "banner"
                    },
                    TVSHOW_IMAGES = {
                        THUMBNAIL: "thumbnail",
                        COVER: "cover",
                        BANNER: "banner"
                    };



                //-- service generic models --

                function parseCategory(json) {

                    return new Category(json);

                }

                exportObj.parseCategory = parseCategory;

                function parseCategoryEntries(entries) {



                    var categories = [];
                    ax.util.each(entries, function(entry) {
                        categories.push(parseCategory(entry));
                    });

                    return categories;
                }

                exportObj.parseCategoryEntries = parseCategoryEntries;


                function parseResource(json) {

                    return new Resource({
                        mimeType: json.contentType + "/" + json.format,
                        url: json.url,
                        id: json.id,
                        duration: json.duration,
                        width: json.width,
                        height: json.height
                    });
                }
                exportObj.parseResource = parseResource;



                // -- movie on demand -- 

                function parseMovieAsset(json) {

                    var key, i, subObj, subLen, parserFn,
                        promiseStack = [],
                        ret,
                        //image parsing flags
                        flagDefault, flagCover, flagThumb, flagBanner, resId,
                        //video parsing flags
                        videoContent;

                    //init model structure
                    ret = {
                        id: json.id,
                        title: json.title,
                        description: json.description,
                        publishedDate: json.pubDate,
                        availableDate: json.availableDate,
                        credits: json.credits,
                        metadata: {},
                        parentalRatings: []
                    };

                    //parse rating
                    ax.util.each(json.ratings, function(item) {
                        ret.parentalRatings.push(item.rating);
                    });



                    //parse images 
                    ret.images = [];

                    flagCover = false;
                    flagThumb = false;
                    flagBanner = false;
                    flagDefault = false;



                    ax.util.each(json.thumbnails, function(thumbnail) {

                        if (flagDefault && flagThumb && flagBanner && flagCover) return ax.util.breaker;

                        resId = null;

                        if (!flagDefault && thumbnail.isDefault) {


                            ret.images.push(parseResource(thumbnail));

                            ret.metadata["defaultimg$resId"] = thumbnail.id;

                            flagDefault = true;

                            if (!resId) {
                                resId = thumbnail.id;
                            }
                        }

                        //parse thumbnail

                        if (!flagThumb && ax.util.indexOf(thumbnail.assetTypes, MOVIE_IMAGES.THUMBNAIL) > -1) {

                            if (resId) {

                                ret.metadata["thumbnail$resId"] = resId;

                                flagThumb = true;

                                return;
                            }

                            ret.images.push(parseResource(thumbnail));

                            ret.metadata["thumbnail$resId"] = thumbnail.id;

                            flagThumb = true;
                            return;
                        }

                        if (!flagCover && ax.util.indexOf(thumbnail.assetTypes, MOVIE_IMAGES.COVER) > -1) {


                            if (resId) {

                                ret.metadata["cover$resId"] = resId;

                                flagCover = true;

                                return;
                            }

                            ret.images.push(parseResource(thumbnail));

                            ret.metadata["cover$resId"] = thumbnail.id;

                            flagCover = true;
                            return;
                        }

                        if (!flagBanner && ax.util.indexOf(thumbnail.assetTypes, MOVIE_IMAGES.BANNER) > -1) {


                            if (resId) {

                                ret.metadata["banner$resId"] = resId;

                                flagBanner = true;

                                return;
                            }


                            ret.images.push(parseResource(thumbnail));

                            ret.metadata["banner$resId"] = thumbnail.id;

                            flagBanner = true;
                            return;
                        }

                    });


                    if (!flagDefault) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "Thumbnail missing from the feed... ");
                    }

                    // use thumnail as fallback
                    if (!flagThumb) {
                        ret.metadata["thumbnail$resId"] = ret.metadata["defaultimg$resId"];
                    }

                    // use thumnail as fallback
                    if (!flagCover) {
                        ret.metadata["cover$resId"] = ret.metadata["thumbnail$resId"];
                    }

                    //use thumbnail as fallback

                    if (!flagBanner) {
                        ret.metadata["banner$resId"] = ret.metadata["thumbnail$resId"];
                    }



                    //parse videos
                    ret.videos = [];


                    ax.util.each(json.content, function(video) {

                        if (video.format.toUpperCase() !== "MPEG4") {
                            return;
                        }

                        if (!videoContent && video.format === "MPEG4") {
                            videoContent = video;
                            return;
                        }

                        if (videoContent && video.width > videoContent.width) {
                            videoContent = video;
                        }


                    });

                    if (videoContent) {

                        ret.videos.push(parseResource(videoContent));

                        ret.metadata["trailer$resId"] = videoContent.id;
                        ret.metadata["content$resId"] = videoContent.id;
                    }

                    //parse credits
                    ret.credits = __arrayToMapHelper(json.credits, ["role", "name"]);

                    //parse star rating
                    ret.metadata["starRating"] = json["pl1$starRating"];

                    //parse keywords
                    ret.metadata["keywords"] = json["keywords"];

                    //parse pricing
                    ret.metadata["rental$period"] = json["pl1$rentalPeriod"];
                    ret.metadata["rental$price"] = json["pl1$price"];


                    //set the type enum
                    ret.type = Asset.TYPE.MOVIE;

                    //leaving categories to be populated in interface layer, since it has to call API again
                    ret.categories = [];

                    return new Asset(ret);

                }

                exportObj.parseMovieAsset = parseMovieAsset;

                function parseMovieEntries(entries) {

                    var movies = [];
                    ax.util.each(entries, function(entry) {
                        movies.push(parseMovieAsset(entry));
                    });

                    return movies;
                }

                exportObj.parseMovieEntries = parseMovieEntries;


                function parseTVShow(json) {


                    var key, i, subObj, subLen, parserFn,
                        flagThumb, flagBanner;

                    ret = {
                        id: json.id,
                        title: json.title,
                        description: json.description,
                        publishedDate: json.pubDate,
                        availableDate: json.availableDate
                    };


                    ret.metadata = {};

                    //leaving categories to be populated in interface layer, since it has to call API again
                    ret.categories = [];

                    ret.images = [];

                    flagThumb = false;
                    flagBanner = false;

                    ax.util.each(json.thumbnails, function(thumbnail) {

                        if (flagBanner && flagThumb) return ax.util.breaker;

                        //parse thumbnail
                        if (!flagThumb && (ax.util.indexOf(thumbnail.assetTypes, TVSHOW_IMAGES.THUMBNAIL) > -1 || thumbnail.isDefault)) {

                            ret.images.push(parseResource(thumbnail));

                            ret.metadata["thumbnail$resId"] = thumbnail.id;

                            flagThumb = true;
                            return;
                        }


                        if (!flagBanner && ax.util.indexOf(thumbnail.assetTypes, TVSHOW_IMAGES.BANNER) > -1) {
                            ret.images.push(parseResource(thumbnail));

                            ret.metadata["banner$resId"] = thumbnail.id;
                            flagBanner = true;
                            return;
                        }

                    });

                    ret.metadata["seriesRef"] = json["pl1$seriesRef"];

                    //parse keywords
                    ret.metadata["keywords"] = json["keywords"];

                    return new TVShow(ret);
                }

                exportObj.parseTVShow = parseTVShow;

                function parseTVShowEntries(entries) {

                    var tvShows = [];
                    ax.util.each(entries, function(entry) {
                        tvShows.push(parseTVShow(entry));
                    });

                    return tvShows;
                }

                exportObj.parseTVShowEntries = parseTVShowEntries;

                function parseEpisodeAsset(json) {

                    var key, i, subObj, subLen, parserFn,
                        promiseStack = [],
                        //parse Episode video
                        videoContent;


                    // parse basic data
                    ret = {
                        type: Asset.TYPE.EPISODE,
                        id: json.id,
                        title: json.title,
                        description: json.description,
                        publishedDate: json.pubDate,
                        availableDate: json.availableDate,
                        metadata: {}
                    };

                    //parse credits
                    ret.credits = __arrayToMapHelper(json.credits, ["role", "name"]);

                    //leaving categories to be populated in interface layer, since it has to call API again
                    ret.categories = [];

                    ret.parentalRatings = [json["pl1$parentalControlRating"]];

                    // add episode specific metadata
                    ret.metadata["VOD$tvShowId"] = json["seriesId"];
                    ret.metadata["VOD$tvShowTitle"] = json["seriesTitle"];
                    ret.metadata["VOD$episodeNumber"] = json["pl1$episode"];



                    //parse images
                    ret.images = [];

                    ax.util.each(json.thumbnails, function(thumbnail) {

                        //parse thumbnail
                        if (thumbnail.isDefault) {

                            ret.images.push(parseResource(thumbnail));

                            ret.metadata["thumbnail$resId"] = thumbnail.id;
                            return ax.util.breaker;
                        }


                    });

                    //parse videos
                    ret.videos = [];

                    ax.util.each(json.content, function(video) {

                        if (video.format.toUpperCase() !== "MPEG4") {
                            return;
                        }

                        if (!videoContent && video.format === "MPEG4") {
                            videoContent = video;
                            return;
                        }

                        if (videoContent && video.width > videoContent.width) {
                            videoContent = video;
                        }

                    });

                    if (videoContent) {
                        ret.videos.push(parseResource(videoContent));

                        ret.metadata["content$resId"] = videoContent.id;
                    }

                    return new Asset(ret);
                }

                exportObj.parseEpisodeAsset = parseEpisodeAsset;


                function parseEpisodeEntries(entries) {

                    var episodes = [];
                    ax.util.each(entries, function(entry) {
                        episodes.push(parseEpisodeAsset(entry));
                    });

                    return episodes;
                }


                exportObj.parseEpisodeEntries = parseEpisodeEntries;


                // -- linear content parsers
                function parseProgramAsset(json) {


                    var ret;

                    // parse basic data
                    ret = {
                        type: Asset.TYPE.PROGRAM,
                        id: json.id,
                        title: json.program.title,
                        description: json.program.description,
                        metadata: {}
                    };


                    return new Asset(json);
                }

                exportObj.parseProgramAsset = parseProgramAsset;


                function parseTVChannelEntries(entries) {

                    //actually parsing TVListing 
                    var ret = [];


                    ax.util.each(entries, function(entry) {
                        ret.push(parseTVChannel(entry));

                    });

                    //return array of tv listing
                    return ret;

                }

                exportObj.parseTVChannelEntries = parseTVChannelEntries;

                function parseTVChannel(json) {

                    var ret;

                    ret = {
                        id: json.id,
                        channelNumber: json.channelNumber,
                        metadata: {}
                    };

                    ret.images = [];

                    //parse first station as Channel info
                    ax.util.each(json.stations, function(pair) {
                        var station = pair.value,
                            thumbnail,
                            stream,
                            tuneAny;

                        ret.title = station.title;

                        //parse logo
                        thumbnail = new Resource({
                            id: pair.key + "/logo",
                            url: station.thumbnails.logo.url,
                            mimeType: "image/undefined",
                            width: station.thumbnails.logo.width,
                            height: station.thumbnails.logo.height,
                        });

                        //set logo
                        ret.images.push(thumbnail);

                        ret.metadata["logo$resId"] = thumbnail.get("id");

                        //parse streams
                        tuneAny = station.tuningInstruction["urn:theplatform:tv:location:any"];

                        stream = new Resource({
                            id: pair.key + '/stream',
                            url: tuneAny.streamingUrl,
                            mimeType: tuneAny.format,
                            language: tuneAny.language,
                            width: tuneAny.width,
                            height: tuneAny.height,
                            geolock: false
                        });

                        ret.streams = [stream];
                        ret.metadata["content$resId"] = stream.get("id");

                        //get the first one
                        return ax.util.breaker;
                    });

                    //temporary fix for CES
                    ret.startTime = json.startTime;
                    ret.endTime = json.endTime;


                    return new TVChannel(ret);
                }


                exportObj.parseTVChannel = parseTVChannel;


                function parseTVListingEntries(entries) {
                    //actually parsing TVListing 
                    var ret = [];


                    ax.util.each(entries, function(entry) {
                        ret.push(parseTVListing(entry));

                    });

                    //return array of tv listing
                    return ret;
                }

                exportObj.parseTVListingEntries = parseTVListingEntries;


                function parseTVListing(json) {
                    //json is acutally at channel level
                    var ret;

                    ret = {
                        "channelId": json.id,
                    };

                    //parse programs
                    ret.programs = [];

                    ax.util.each(json.listings, function(listing) {

                        ret.programs.push(parseProgramAsset(listing));

                    });

                    return new TVListing(ret);
                }


                exportObj.parseTVListing = parseTVListing;

                // reformat an array to map with selected attributes
                function __arrayToMapHelper(json, parseMap) {
                    var subObj = json,
                        tempObj = {},
                        NAME = parseMap[0],
                        VALUE = parseMap[1],
                        subObjName, subObjValue, subLen, i;

                    subLen = subObj.length;

                    for (i = 0; i < subLen; i++) {
                        subObjName = subObj[i][NAME];
                        subObjValue = subObj[i][VALUE];
                        if (!subObjName || !subObjValue) {
                            continue;
                        }
                        // parse array to object
                        tempObj[subObjName] = subObjValue;
                    }



                    return tempObj;
                }

                return exportObj;


            })(),

            OvpService = ax.klass.create([VODContentService], {
                TVSHOW_CAT_FID: TVSHOW_CAT_FID,
                MOVIE_CAT_FID: MOVIE_CAT_FID,
                SORT_BY: SORT_BY,
            }, {
                /**
                 * CachingHelper Instance, may be used for any OVP related service.
                 * @member __cache
                 * @memberof tve/ThePlatformOvpService#
                 * @private
                 */
                __cache: null,


                __allCategoriesCache: null,

                /*
                 * Init the ThePlatformOvpService moudle
                 * @method init
                 * @memeberof tve/ThePlatformOvpService#
                 * @private
                 */
                init: function(opts) {
                    if (opts && opts.feeds) {
                        this.__cache = new CacheHelper();
                        ovpServiceApi.init(opts.feeds, this.__cache);
                    } else {
                        throw new TVEError(FACILITY.GENERAL, ERROR.INTERNAL, "no baseURL is provided, cannot init service");
                    }
                },

                // ==== Impliment Interfaces ====

                // Private function

                /**
                 * Get all Categories
                 * @method getCategories
                 * @public
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/name
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */

                getAllCategories: function(opts) {


                    if (this.__allCategoriesCache) {

                        return this.__allCategoriesCache;
                    }


                    this.__allCategoriesCache = ovpServiceApi.categoryRequest(opts).then(function(json) {

                        var hasParents = [],
                            noParents = [],
                            entries = json.entries,
                            entry;

                        while (entries.length > 0) {
                            entry = entries.shift();

                            if (entry.parentId !== "") {
                                hasParents.push(entry);
                            } else {
                                noParents.push(entry);
                            }
                        }

                        //parses both separately
                        return ax.promise.all([ovpServiceParser.parseCategoryEntries(hasParents),
                            ovpServiceParser.parseCategoryEntries(noParents)
                        ]);

                    }).then(function(bothEntries) {

                        var hasParents = bothEntries[0],
                            noParents = bothEntries[1],
                            i, j,
                            child, parent;

                        // tries to find parent for hasParent, NOTE: one level only.
                        for (i = 0; i < hasParents.length; i++) {



                            for (j = 0; j < noParents.length; j++) {

                                if (hasParents[i].get("parentId") === noParents[j].get("id")) {
                                    noParents[j].get("categories").push(hasParents[i]);
                                    break;
                                }

                            }

                        }

                        return noParents.concat(hasParents);

                    });

                    return this.__allCategoriesCache;
                },

                /**
                 * Get Category By Id
                 * @method getCategoryById
                 * @public
                 * @param {String} id CategoryID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/name
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getCategoryById: function(id, opts) {
                    if (!id) {
                        throw new TVEError(FACILITY.GENERAL, ERROR.INTERNAL, "no id is provided");
                    }
                    opts = opts || {};

                    var _ret = null;

                    return this.getAllCategories(opts).then(function(categories) {

                        ax.util.each(categories, function(category) {
                            if (category.get("id") === id) {
                                _ret = category;
                                return ax.util.breaker;
                            }
                        });

                        if (!_ret) {
                            throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "Category doesn't exist, id:" + id);
                        }

                        return _ret;

                    });



                },

                /**
                 * Get Categories By Ids
                 * @method getCategoriesByIds
                 * @public
                 * @param {String[]} ids CategoryIDs
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/name
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getCategoriesByIds: function(ids, opts) {
                    if (!ids) {
                        throw new TVEError(FACILITY.GENERAL, ERROR.INTERNAL, "no ids are provided");
                    }
                    opts = opts || {};

                    var _ret = [];


                    return this.getAllCategories(opts).then(function(categories) {

                        ax.util.each(categories, function(category) {
                            if (ax.util.indexOf(ids, category.get("id")) > -1) {
                                _ret.push(category);
                            }
                        });

                        if (_ret.length !== ids.length) {
                            throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "Some category doesn't exist, ids:" + ids.join(','));
                        }

                        return _ret;
                    });

                },


                __populateAssetCategories: function(asset, assetData) {

                    return this.getCategoriesByIds(assetData["categoryIds"]).then(function(categories) {

                        asset.set("categories", categories);

                        return asset;
                    });

                },

                __populateAssetEntriesCategories: function(assets, assetsDataEntries) {

                    var i = 0,
                        len = assets.length,
                        promises = [];


                    for (; i < len; i++) {

                        promise = this.__populateAssetCategories(assets[i], assetsDataEntries[i]);
                        promises.push(promise);

                    }

                    return ax.promise.all(promises);

                },


                // ==== ILinearContentService ====

                _channelParams: function(opts) {

                    opts.startTime = ALL_CHANNELS_START_TIME;
                    opts.endTime = ALL_CHANNELS_END_TIME;

                    return opts;
                },


                /**
                 * Get all Channels
                 * @method getAllChannels
                 * @public
                 * @param {Object} opts The options object
                 * @param {String} [opts.sortBy] sort by id/channelNumber/title
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getAllChannels: function(opts) {
                    opts = opts || {};


                    var _json;

                    this._channelParams(opts);


                    return ovpServiceApi.tvListingRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseTVChannelEntries(json.entries);

                    }).then(function(entries) {

                        return {
                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex,
                        }
                    });
                },

                /**
                 * Get Channel by Id
                 * @method getChannelById
                 * @public
                 * @param {String} id ChannelID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/channelNumber/title
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getChannelById: function(id, opts) {
                    if (!id) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided"));
                    }
                    opts = opts || {};
                    opts.id = id;

                    var _json;


                    this._channelParams(opts);


                    return ovpServiceApi.tvListingRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseTVChannel(json.entries[0]);

                    });
                },

                /**
                 * Get Channels by Ids
                 * @method getChannelsByIds
                 * @public
                 * @param {String[]} ids ChannelIDs
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/channelNumber/title
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getChannelsByIds: function(ids, opts) {
                    if (!ids) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no ids are provided"));
                    }
                    opts = opts || {};
                    opts.id = ids;

                    var _json;

                    return ovpServiceApi.tvListingRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseTVChannelEntries(json.entries);

                    });
                },

                /**
                 * Get Channels by Category
                 * @method getChannelsByCategoryId
                 * @public
                 * @param {String} categoryId CategoryID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/channelNumber/title
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getChannelsByCategoryId: function(categoryId, opts) {

                    return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "getChannelsByCategoryId not supported in theplatfom"));

                },

                /**
                 * Get all TVListings
                 * @method getAllTVListings
                 * @public
                 * @param {String} startTime startTime
                 * @param {String} endTime endTime
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/channelId
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getAllTVListings: function(startTime, endTime, opts) {

                    if (!startTime || !endTime) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no startime or endtime is provided"));
                    }
                    opts = opts || {};
                    opts.startTime = startTime;
                    opts.endTime = endTime;

                    var _json;

                    return ovpServiceApi.tvListingRequest(opts).then(function(json) {
                        _json = json;

                        return ovpServiceParser.parseTVListingEntries(json.entries);

                    }).then(function(entries) {


                        return {

                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex

                        };
                    });
                },

                /**
                 * Get TVListing by channel Id
                 * @method getTVListingByChannelId
                 * @public
                 * @param {String} channelId ChannelID
                 * @param {String} startTime startTime
                 * @param {String} endTime endTime
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/channelId
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getTVListingByChannelId: function(channelId, startTime, endTime, opts) {
                    if (!channelId) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no channelId is provided"));
                    }
                    if (!startTime || !endTime) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no startime or endtime is provided"));
                    }
                    opts = opts || {};
                    opts.channelId = channelId;
                    opts.startTime = startTime;
                    opts.endTime = endTime;


                    return ovpServiceApi.tvListingRequest(opts).then(function(json) {

                        return ovpServiceParser.parseTVListing(json.entries[0]);

                    });
                },

                /**
                 * Get TVListing by channel Ids
                 * @method getTVListingsByChannelIds
                 * @public
                 * @param {String[]} channelIds ChannelIDs
                 * @param {String} startTime startTime
                 * @param {String} endTime endTime
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/channelId
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getTVListingsByChannelIds: function(channelIds, startTime, endTime, opts) {
                    if (!channelIds) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no channelIds are provided"));
                    }
                    if (!startTime || !endTime) {
                        return ax.promise.reject(new TVEError(FACILITY.LINEAR_CONTENT_SERVICE, ERROR.INTERNAL, "no startime or endtime is provided"));
                    }
                    opts = opts || {};
                    opts.channelId = channelIds;
                    opts.startTime = startTime;
                    opts.endTime = endTime;

                    var _json;

                    return ovpServiceApi.tvListingRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseTVListingEntries(json.entries);

                    }).then(function(entries) {

                        return {

                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex

                        };
                    });
                },

                // ==== IVODContentService ====

                _movieMediaParams: function(opts) {

                    opts.feed = MOVIE_FEED;

                    if (!opts.categoryId) {
                        opts.categoryId = MOVIE_CAT_FID;
                    } else if (ax.util.isArray(opts.categoryId)) {
                        opts.categoryId.push(MOVIE_CAT_FID);
                    }

                    return opts;
                },

                /**
                 * Get all Movies
                 * @method getAllMovies
                 * @public
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
                 * @param {String} [opts.keywords] query by keyword
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getAllMovies: function(opts) {

                    var _json;

                    opts = opts || {};

                    opts = this._movieMediaParams(opts);

                    // send request
                    return ovpServiceApi.mediaRequest(opts).then(function(json) {

                        _json = json;
                        //parse movies
                        return ovpServiceParser.parseMovieEntries(json.entries);

                    }).then(ax.util.bind(function(entries) {

                        return this.__populateAssetEntriesCategories(entries, _json.entries);

                    }, this)).then(function(entries) {

                        //map to correct output
                        return {
                            entries: entries,
                            startIndex: _json.startIndex,
                            totalCount: _json.entryCount
                        };
                    });
                },

                /**
                 * Get Movie by Id
                 * @method getMovieById
                 * @public
                 * @param {String} id MovieID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getMovieById: function(id, opts) {

                    if (!id) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided");
                    }
                    opts = opts || {};
                    opts.id = id;
                    opts.feed = MOVIE_FEED;

                    opts = this._movieMediaParams(opts);

                    var _json;

                    return ovpServiceApi.mediaRequest(opts).then(function(json) {
                        _json = json;
                        return ovpServiceParser.parseMovieAsset(json.entries[0]);

                    }).then(ax.util.bind(function(movie) {

                        return this.__populateAssetCategories(movie, _json.entries[0]);

                    }, this));
                },

                /**
                 * Get Movies by Ids
                 * @method getMoviesByIds
                 * @public
                 * @param {String[]} ids MovieIDs
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getMoviesByIds: function(ids, opts) {


                    var _json;

                    if (!ids) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no ids are provided");
                    }

                    if (!ax.util.isArray(ids) || ids.length < 1) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "getMoviesByIds: ids must be provided more than one");
                    }

                    opts = opts || {};
                    opts.id = ids;

                    opts = this._movieMediaParams(opts);



                    return ovpServiceApi.mediaRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseMovieEntries(json.entries);

                    }).then(ax.util.bind(function(entries) {

                        return this.__populateAssetEntriesCategories(entries, _json.entries);

                    }, this)).then(function(entries) {

                        //map to correct output
                        return {
                            entries: entries,
                            startIndex: _json.startIndex,
                            totalCount: _json.entryCount
                        };
                    });
                },


                /**
                 * Get Movies by category
                 * @method getMoviesByCategoryId
                 * @public
                 * @param {String} categoryId CategoryID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getMoviesByCategoryId: function(categoryId, opts) {

                    var _json;

                    if (!categoryId) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no categoryId is provided");
                    }
                    opts = opts || {};
                    opts.categoryId = categoryId;

                    opts = this._movieMediaParams(opts);


                    return ovpServiceApi.mediaRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseMovieEntries(json.entries);

                    }).then(ax.util.bind(function(entries) {

                        return this.__populateAssetEntriesCategories(entries, _json.entries);

                    }, this)).then(function(entries) {

                        return {
                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex
                        };
                    });
                },


                _tvShowMediaParams: function(opts) {

                    opts.feed = TVSHOW_FEED;

                    if (!opts.categoryId) {
                        opts.categoryId = TVSHOW_CAT_ID;
                    } else if (ax.util.isArray(opts.categoryId)) {
                        opts.categoryId.push(TVSHOW_CAT_ID);
                    }

                    return opts;
                },
                /**
                 * Get all TV Shows
                 * @method getAllTVShows
                 * @public
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
                 * @param {String} [opts.showEpisodes] flag to show episodes in return
                 * @param {String} [opts.keywords] keywords to query TVShows
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getAllTVShows: function(opts) {

                    opts = opts || {};
                    opts = this._tvShowMediaParams(opts);

                    var _json;

                    return ovpServiceApi.mediaRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseTVShowEntries(json.entries);

                    }).then(ax.util.bind(function(entries) {

                        return this.__populateAssetEntriesCategories(entries, _json.entries);

                    }, this)).then(function(entries) {

                        return {

                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex

                        };
                    });
                },

                /**
                 * Get TV Show by Id
                 * @method getTVShowById
                 * @public
                 * @param {String} id TVShowID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
                 * @param {String} [opts.showEpisodes] flag to show episodes in return
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getTVShowById: function(id, opts) {



                    if (!id) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided");
                    }

                    opts = opts || {};
                    opts.id = id;

                    opts = this._tvShowMediaParams(opts);

                    var _json;

                    return ovpServiceApi.mediaRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseTVShow(json.entries[0]);

                    }).then(ax.util.bind(function(tvShow) {

                        return this.__populateAssetCategories(tvShow, _json.entries[0]);

                    }, this));
                },

                /**
                 * Get TV Shows by Ids
                 * @method getTVShowsByIds
                 * @public
                 * @param {String[]} ids TVShowIDs
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
                 * @param {String} [opts.showEpisodes] flag to show episodes in return
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getTVShowsByIds: function(ids, opts) {
                    if (!ids) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no ids are provided");
                    }

                    if (!ax.util.isArray(ids) || ids.length < 1) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "getTVShowsByIds: ids must be provided more than one");
                    }

                    opts = opts || {};
                    opts.id = ids;

                    opts = this._tvShowMediaParams(opts);


                    var _json;

                    return ovpServiceApi.mediaRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseTVShowEntries(json.entries);

                    }).then(ax.util.bind(function(entries) {

                        return this.__populateAssetEntriesCategories(entries, _json.entries);

                    }, this)).then(function(entries) {
                        return {
                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex
                        };
                    });

                },

                /**
                 * Get TV Shows by category
                 * @method getTVShowsByCategoryId
                 * @public
                 * @param {String} categoryId CategoryID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate
                 * @param {String} [opts.showEpisodes] flag to show episodes in return
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getTVShowsByCategoryId: function(categoryId, opts) {
                    if (!categoryId) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no categoryId is provided");
                    }
                    opts = opts || {};
                    opts.categoryId = categoryId;

                    opts = this._tvShowMediaParams(opts);

                    var _json;

                    return ovpServiceApi.mediaRequest(opts).then(function(json) {

                        _json = json;
                        return ovpServiceParser.parseTVShowEntries(json.entries);

                    }).then(ax.util.bind(function(entries) {

                        return this.__populateAssetEntriesCategories(entries, _json.entries);

                    }, this)).then(function(entries) {

                        return {
                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex
                        };
                    });
                },

                _episodeMediaParams: function(opts) {

                    opts.feed = EPISODE_FEED;

                    if (!opts.categoryId) {
                        opts.categoryId = EPISODE_CAT_ID;
                    } else if (ax.util.isArray(opts.categoryId)) {
                        opts.categoryId.push(EPISODE_CAT_ID);
                    }

                    return opts;
                },

                /**
                 * Get all Episodes
                 * @method getAllEpisodes
                 * @public
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getAllEpisodes: function(opts) {

                    opts = opts || {};

                    opts = this._episodeMediaParams(opts);

                    var _json;


                    return ovpServiceApi.mediaRequest(opts).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseEpisodeEntries(json.entries);

                    }).then(function(parsedEpisodes) {

                        return {
                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex
                        };
                    });
                },

                /**
                 * Get Episodes by Id
                 * @method getEpisodeById
                 * @public
                 * @param {String} id EpisodeID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getEpisodeById: function(id, opts) {
                    if (!id) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no id is provided");
                    }
                    opts = opts || {};
                    opts.id = id;


                    opts = this._episodeMediaParams(opts);

                    return ovpServiceApi.mediaRequest(opts).then(function(json) {



                        return ovpServiceParser.parseEpisodeEntries(json.entries[0]);
                    });
                },

                /**
                 * Get Episode by TVShow Id
                 * @method getEpisodeByTVShowId
                 * @public
                 * @param {String} showId showID
                 * @param {Object} [opts] The options object
                 * @param {String} [opts.sortBy] sort by id/title/publishedDate/availableDate/tvShowId/tvShowTitle
                 * @returns {module:ax/promise~Promise} A object extended from the promise
                 * @memberof tve/ThePlatformOvpService#
                 */
                getEpisodesByTVShowId: function(showId, opts) {
                    if (!showId) {
                        throw new TVEError(FACILITY.VOD_CONTENT_SERVICE, ERROR.INTERNAL, "no showId is provided");
                    }
                    opts = opts || {};

                    opts = this._episodeMediaParams(opts);

                    var _json;

                    return this.getTVShowById(showId).then(function(tvshow) {

                        opts.seriesRef = tvshow.get("metadata").seriesRef;

                        return ovpServiceApi.episodeRequest(opts);

                    }).then(function(json) {

                        _json = json;

                        return ovpServiceParser.parseEpisodeEntries(json.entries);

                    }).then(function(entries) {


                        return {
                            entries: entries,
                            totalCount: _json.entryCount,
                            startIndex: _json.startIndex
                        };
                    });

                }
            });

        return OvpService;
    });
/**
 * @name tve/sServiceHolder
 * @memberof tve
 * @class Returns a singleton responsible for providing the remote services public interfaces.
 */
define("tve/sServiceHolder", ["tve/AppGridService", "tve/AccedoOvpService", "tve/ThePlatformOvpService", "tve/AccedoConnectService", "ax/class", "ax/util", "ax/config", "ax/console"], function(AppGridService, AccedoOvpService, ThePlatformOvpService, ConnectService, klass, util, config, console) {

    var ServiceHolder = klass.create({}, {

        /**
         * Service holder for configurationService which has a public getter function
         * @memberof tve/sServiceHolder
         * @private
         */
        __configurationService: null,

        /**
         * Service holder for statusService which has a public getter function
         * @memberof tve/sServiceHolder
         * @private
         */
        __statusService: null,

        /**
         * Service holder for loggingService which has a public getter function
         * @memberof tve/sServiceHolder
         * @private
         */
        __loggingService: null,

        /**
         * Service holder for analyticsService which has a public getter function
         * @memberof tve/sServiceHolder
         * @private
         */
        __analyticsService: null,

        /**
         * Service holder for socialService which has a public getter function
         * @memberof tve/sServiceHolder
         * @private
         */
        __socialService: null,

        /**
         * Service holder for vodContentService which has a public getter function
         * @memberof tve/sServiceHolder
         * @private
         */
        __vodContentService: null,

        /**
         * Service holder for linearContentService which has a public getter function
         * @memberof tve/sServiceHolder
         * @private
         */
        __linearContentService: null,

        /**
         * Service holder for interactionService which has a public getter function
         * @memberof tve/sServiceHolder
         * @private
         */
        __interactionService: null,

        /**
         * Service holder for appGridService which is for setting the public service
         * @memberof tve/sServiceHolder
         * @private
         */
        __appGridService: null,

        /**
         * Service holder for ovpService which is for setting the public service
         * @memberof tve/sServiceHolder
         * @private
         */
        __ovpService: null,

        /**
         * The Platform service 
         * @memberof tve/sServiceHolder
         * @private
         */
        __thePlatformService: null,

        /**
         * Service holder for connectService which is for setting the public service
         * @memberof tve/sServiceHolder
         * @private
         */
        __connectService: null,

        /**
         * Return the ConfigurationService
         * @method getConfigurationService
         * @memberof tve/sServiceHolder
         * @public
         */
        getConfigurationService: function(){
            this.__configurationService = this.__configurationService || this.__getAppGridService();
            return this.__configurationService;
        },

        /**
         * Return the ResourceService
         * @method getResourceService
         * @memberof tve/sServiceHolder
         * @public
         */
        getResourceService: function(){
            this.__resourceService = this.__resourceService || this.__getAppGridService();
            return this.__resourceService;
        },

        /**
         * Return the StatusService
         * @method getStatusService
         * @memberof tve/sServiceHolder
         * @public
         */
        getStatusService: function(){
            this.__statusService = this.__statusService || this.__getAppGridService();
            return this.__statusService;
        },

        /**
         * Return the StatusService
         * @method getLoggingService
         * @memberof tve/sServiceHolder
         * @public
         */
        getLoggingService: function(){
            this.__loggingService = this.__loggingService || this.__getAppGridService();
            return this.__loggingService;
        },

        /**
         * Return the AnalyticsService
         * @method getAnalyticsService
         * @memberof tve/sServiceHolder
         * @public
         */
        getAnalyticsService: function(){
            this.__analyticsService = this.__analyticsService || this.__getAppGridService();
            return this.__analyticsService;
        },

        /**
         * Return the UserSettingService
         * @method getUserSettingService
         * @memberof tve/sServiceHolder
         * @public
         */
        getUserSettingService: function(){
            this.__userSettingService = this.__userSettingService || this.__getAppGridService();
            return this.__userSettingService;
        },

        /**
         * Return the VODContentService
         * @method getConfigurationService
         * @memberof tve/sServiceHolder
         * @public
         */
        getVODContentService: function(){
            this.__vodContentService = this.__vodContentService || this.__getAccedoOvpService();
            return this.__vodContentService;
        },

        /**
         * Return the LinearContentService
         * @method getConfigurationService
         * @memberof tve/sServiceHolder
         * @public
         */
        getLinearContentService: function(){
            this.__linearContentService = this.__linearContentService || this.__getAccedoOvpService();
            return this.__linearContentService;
        },

        /**
         * Return the SocialService
         * @method getSocialService
         * @memberof tve/sServiceHolder
         * @public
         */
        getSocialService: function(){
            this.__socialService = this.__socialService || this.__getAppGridService();
            return this.__socialService;
        },
        
        /**
         * Return the LinearContentService
         * @method getConfigurationService
         * @memberof tve/sServiceHolder
         * @public
         */
        getInteractionService: function(){
            this.__interactionService = this.__interactionService || this.__getConnectService();
            return this.__interactionService;
        },

        /**
         * Return the AppGridService.
         * Because ax/device is needed for AppGrid service, this service only can be called after sEnv is loaded.
         * @method __getAppGridService
         * @memberof tve/sServiceHolder
         * @private
         */
        __getAppGridService: function(){
            if(this.__appGridService){
                return this.__appGridService;
            }

            var device = amd.require("ax/device"),
                appGridConfig = config.get("tve.appgrid");

            if(appGridConfig && appGridConfig.api && appGridConfig.appKey){
                var appKey  = appGridConfig.appKey,
                    api = appGridConfig.api,
                    uuid = device.id.getUniqueID();

                this.__appGridService = new AppGridService(api, appKey, uuid);
                return this.__appGridService;
            }

            console.error("### Unable to initialize the AppGrid Service, can not find the API or app key");
        },

        /**
         * Return the OvpService
         * @method __getOvpService
         * @memberof tve/sServiceHolder
         * @private
         */
        __getAccedoOvpService: function(){
            if (this.__ovpService) {
                return this.__ovpService;
            }

            var ovpConfig = config.get("tve.ovp");

            if(ovpConfig && ovpConfig.api) {
                this.__ovpService = new AccedoOvpService({
                    baseURL: ovpConfig.api
                });
                return this.__ovpService;
            }

            console.error("### Unable to initialize the Ovp Service, can not find the API end-point and keys from config");
        },

        /**
         * Return the Platform Service
         * @method __getThePlatformOvpService
         * @memberof tve/sServiceHolder
         * @private
         */
        __getThePlatformOvpService: function() {

            if (this.__thePlatformService) {
                return this.__thePlatformService;
            }

            var thePlatformConfig = config.get("tve.theplatform");

            if(thePlatformConfig && thePlatformConfig.feeds) {
                this.__thePlatformService = new ThePlatformOvpService({
                    feeds: thePlatformConfig.feeds
                });
                return this.__thePlatformService;
            }

            console.error("### Unable to initialize the Ovp Service, can not find the API end-point and keys from config");
        },

        /**
         * Return the InteractionService
         * @method __getInteractionService
         * @memberof tve/sServiceHolder
         * @private
         */
        __getConnectService: function(){
            var device = amd.require("ax/device"),
                connectConfig = config.get("tve.connect");
            if(connectConfig && connectConfig.apiKey && connectConfig.connectServer){
                var apiKey  = connectConfig.apiKey,
                    connectServer = connectConfig.connectServer,
                    uuid = device.id.getUniqueID();

                if (!this.__connectService) {
                    this.__connectService = new ConnectService(uuid, apiKey, connectServer);
                }
                return this.__connectService;
            }

            console.error("### Unable to initialize Accedo Connect, can not find the API or app key");
        },

        /**
         * Init the sServiceHolder moudle
         * @method init
         * @memberof tve/sServiceHolder
         * @private
         */
        init: function(){

        }
    });
    
    return new ServiceHolder();
});

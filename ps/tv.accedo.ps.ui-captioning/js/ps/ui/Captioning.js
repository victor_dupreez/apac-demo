/**
 * Widget for displaying subtitles/captions.
 * @class ps/ui/Captioning
 * @extends ax/af/Container
 * @author Niklas Bjorken <niklas.bjorken@accedo.tv>
 */
define("ps/ui/Captioning", ["ax/class", "ax/ext/ui/Label", "ax/Element", "ax/util", "ax/console",
        "ax/promise", "ax/ajax", "ax/core"
    ],
    function(klass, Label, Element, util, console, promise, ajax, core) {

        var CaptioningClass = klass.create({
            /**
             * @static
             */

        }, {
            _videoPlayer: null,
            _captionsContainer: null,
            _captionsElement: null,
            _subtitleFileUrl: null,
            _captions: null,
            _currentCaption: null,
            _clearTimeout: null,
            _disabled: false,

            /**
             * subtitleFileUrl
             * videoPlayer
             * captionsContainer (Element)
             */
            init: function(opts) {
                opts = opts || {};
                opts.subtitleFileUrl = opts.subtitleFileUrl || false;
                opts.videoPlayer = opts.videoPlayer || false;
                opts.typeOverride = opts.typeOverride || false;
                opts.captionsContainer = opts.captionsContainer || false;

                if (!opts.subtitleFileUrl || !opts.videoPlayer || !opts.captionsContainer) {
                    console.err("Captioning.js Error! Required parameters missing");
                    return;
                }

                this._subtitleFileUrl = opts.subtitleFileUrl;
                this._videoPlayer = opts.videoPlayer;
                this._captionsContainer = opts.captionsContainer;

                this.parseCaptions = util.bind(this.parseCaptions, this);
                this._timeTick = util.bind(this._timeTick, this);
                this._captionDurationOverCallback = util.bind(this._captionDurationOverCallback, this);

                this._loadSubtitleFile().then(this.parseCaptions);
                this._setupVideoPlayerHooks();
                this._initCaptionsContainer();
            },

            deinit: function() {
                this._removeVideoPlayerHooks();
                
                this._videoPlayer = null;
                this._captions = null;
                this._subtitleFileUrl = null;
            },
            
            disable: function() {
                util.clearDelay(this._clearTimeout);
                this._updateCaptions("");
                this._disabled = true;
            },
            
            enable: function() {
                this._disabled = false;
            },

            _loadSubtitleFile: function() {
                var url = this.getSubtitleFileUrl(),
                    deferred = promise.defer();

                ajax.request(url, {
                    method: "get"
                }).then(function(transport) {
                    var data = transport.responseText;
                    deferred.resolve(data);

                }, function(rejection) {
                    deferred.reject("Error while fetching subtitle file: " + rejection);
                });

                return deferred.promise;
            },

            _setupVideoPlayerHooks: function() {
                if (this._videoPlayer) {
                    this._videoPlayer.addEventListener(this._videoPlayer.EVT_TIME_UPDATE, this._timeTick);
                }
            },

            _removeVideoPlayerHooks: function() {
                if (this._videoPlayer) {
                    this._videoPlayer.removeEventListener(this._videoPlayer.EVT_TIME_UPDATE, this._timeTick);
                }
            },

            _timeTick: function(time) {
                if (this._disabled) {
                    return;
                }
                
                time = time * 1000;

                var caption = this._getCaptionsAtTimePoint(time);
                if (caption && caption !== this._currentCaption) {

                    if (caption.regionClass !== (this._currentCaption && this._currentCaption.regionClass)) {
                        this._updateRegionContainer(caption.regionClass);
                    }

                    this._currentCaption = caption;

                    if (this._clearTimeout) {
                        this._captionDurationOverCallback();
                    }

                    this._updateCaptions(caption.content);

                    this._clearTimeout = core.getGuid();
                    util.delay((caption.dur - 100) / 1000, this._clearTimeout)
                        .then(this._captionDurationOverCallback).done();
                }
            },

            _updateRegionContainer: function(region) {
                if (region) {
                    this._captionsContainer.removeClass("default");
                    this._captionsElement.addClass(region);
                } else {
                    if (this._currentCaption) {
                        this._captionsElement.removeClass(this._currentCaption.regionClass);
                        this._captionsContainer.addClass("default");
                    }
                }

                return;
            },

            _captionDurationOverCallback: function() {
                util.clearDelay(this._clearTimeout);
                this._clearTimeout = null;
                this._clearCaptions();
            },

            _getCaptionsAtTimePoint: function(time) {
                var caps = this._captions && this._captions.captions;
                if (caps) {
                    for (var i = 0; i < caps.length; i++) {
                        if (time >= caps[i].begin && time < caps[i].begin + caps[i].dur) {
                            //console.log("###### FOUND CAPTION: begin="+caps[i].begin+" end="+(caps[i].begin + caps[i].dur));
                            return caps[i];
                        }
                    }
                }
            },

            _initCaptionsContainer: function() {
                if (this._captionsContainer) {
                    this._captionsContainer.getRoot().appendTo(core.root.document.body);
                    this._captionsContainer.addClass("default"); // This is removed if a region is specified
                    this._captionsElement = new Element("div");
                    this._captionsContainer.getRoot().append(this._captionsElement);
                }
            },

            _deinitCaptionsContainer: function() {

            },

            _clearCaptions: function() {
                this._updateCaptions("");
            },

            _updateCaptions: function(html) {
                console.log("Caption: " + html);
                this._captionsElement.setInnerHTML(html);
            },

            /**
             * Abstract, must be implemented by subclass
             */
            parseCaptions: function(rawString) {
                console.err("Captioning.js Error! Abstract function called!");
            },

            setParsedCaptions: function(captions) {
                console.log("Captions parsed and ready!");
                this._captions = captions;
            },

            getSubtitleFileUrl: function() {
                return this._subtitleFileUrl;
            },

            getVideoPlayer: function() {
                return this._videoPlayer;
            }
        });

        return CaptioningClass;
    });

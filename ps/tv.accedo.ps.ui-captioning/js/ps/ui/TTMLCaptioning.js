/**
 * Implementation of TTML captioning
 * @class ps/ui/TTMLCaptioning
 * @extends ps/ui/Captioning
 * @author Niklas Bjorken <niklas.bjorken@accedo.tv>
 */
define("ps/ui/TTMLCaptioning", ["ps/ui/Captioning", "ax/class", "ax/Env", "ax/util", "ps/util/cssUtil", "ax/console"],
    function(Captioning, klass, Env, util, cssUtil, console) {

        var ttmlCssPrefix = "xdk-ttml-",
            ttmlRegionCssPrefix = ttmlCssPrefix + "region-",
            cssNodes = [],
            Env = Env.singleton();


        /**
         * Takes a string representation of time and returns
         * time in ms
         */
        function parseTime(timeString) {
            var INTEGER = 1,
                FLOAT = 2,
                UNIT = 3,
                HOUR = 1,
                MINUTE = 2,
                SECOND = 3,
                MILLISECOND = 4;

            var format1 = /(\d+):(\d+):(\d+)[\.|:](\d+)/, // eg. 00:00:13.231
                format2 = /(\d+)\.(\d+)(\w+)/i, // eg. 34.7s
                format1Res, format2Res,
                i, f, u,
                h, m, s, ms,
                res = -1;

            if (timeString) {
                format1Res = timeString.match(format1);
                if (format1Res) {
                    h = parseInt(format1Res[HOUR], 10);
                    m = parseInt(format1Res[MINUTE], 10);
                    s = parseInt(format1Res[SECOND], 10);
                    ms = parseInt(format1Res[MILLISECOND], 10);

                    if (h > 0 || m > 0 || s > 0 || ms > 0) {
                        res = 0;

                        if (h > 0) {
                            res += h * 60 * 60 * 1000;
                        }

                        if (m > 0) {
                            res += m * 60 * 1000;
                        }

                        if (s > 0) {
                            res += s * 1000;
                        }

                        if (ms > 0) {
                            res += ms;
                        }
                    }

                } else {
                    format2Res = timeString.match(format2);

                    if (format2Res) {
                        i = parseInt(format2Res[INTEGER], 10);
                        f = parseInt(format2Res[FLOAT], 10);
                        u = format2Res[UNIT];

                        if (u === "ms") {
                            res = i;

                        } else if (u === "s") {
                            res = i * 1000;
                            res += f;

                        } else if (u === "m") {
                            res = i * 60 * 1000;
                            res += f * 1000;

                        } else if (u === "h") {
                            res = i * 60 * 60 * 1000;
                            res += f * 60 * 1000;
                        }

                    }
                }
            }

            return res;
        }

        var cssTranslator = {
            textAlign: "text-align",
            fontStyle: "font-style",
            fontFamily: "font-family",
            fontWeight: "font-weight",
            backgroundColor: "background-color"
        };

        function toCssAttribute(str) {
            var res = cssTranslator[str];
            return res || str;
        }

        function toPixelValue(val, aspect) {
            var deviceWidth = Env.resolution.width,
                deviceHeight = Env.resolution.height,
                res = false;

            if (val && val.length) {
                var last = val[val.length - 1];
                if (last === "%") {
                    if (aspect === "x") {
                        res = (parseInt(val, 10) / 100) * deviceWidth;
                    } else if (aspect === "y") {
                        res = (parseInt(val, 10) / 100) * deviceHeight;
                    }

                } else if (last === "x") {
                    res = parseInt(val, 10);
                }
            }

            if (res !== false) {
                res = res + "px";
            }

            return res;
        }

        function htmlifyInnerTag(node, styles) {
            var tmpNodeName = node.nodeName,
                tmpAttrs = node.attributes,
                tmpStr;

            if (tmpNodeName === "br") {
                return "<br/>";
            }

            tmpStr = "<" + tmpNodeName;

            if (tmpAttrs.length) {
                tmpStr += " style=\"";
                for (var y = 0; y < tmpAttrs.length; y++) {
                    tmpStr += toCssAttribute(tmpAttrs[y].nodeName.replace("tts:", "")) + ": " + tmpAttrs[y].nodeValue + "\"";
                }
            }

            tmpStr += ">" + node.textContent + "</" + tmpNodeName + ">";

            return tmpStr;
        }

        function createRegionStyle(region, id) {
            var regionStyle = "." + ttmlRegionCssPrefix + id + " {",
                whFormat = /(\w+%?)\s?(\w*%?)/, // eg. "10px 25%", "10em 90px", "20%" etc
                tmp1, tmp2;

            for (var i = 0; i < region.length; i++) {
                if (region[i].prop === "origin") {
                    tmp1 = region[i].val.match(whFormat);

                    if (tmp1[1]) {
                        regionStyle += "left: " + toPixelValue(tmp1[1], "x") + ";";

                        if (tmp1[2]) {
                            regionStyle += "top: " + toPixelValue(tmp1[2], "y") + ";";
                        } else {
                            regionStyle += "top: " + toPixelValue(tmp1[1], "y") + ";";
                        }
                    }

                } else if (region[i].prop === "extent") {
                    tmp1 = region[i].val.match(whFormat);

                    if (tmp1[1]) {
                        regionStyle += "width: " + toPixelValue(tmp1[1], "x") + ";";

                        if (tmp1[2]) {
                            regionStyle += "height: " + toPixelValue(tmp1[2], "y") + ";";
                        } else {
                            regionStyle += "height: " + toPixelValue(tmp1[1], "y") + ";";
                        }
                    }


                } else if (region[i].prop === "displayAlign") {
                    // Not supported?

                } else if (region[i].prop === "textAlign") {
                    regionStyle += "text-align: " + region[i].val + ";";

                } else if (region[i].prop === "backgroundColor") {
                    regionStyle += "background-color: " + region[i].val + ";";

                } else if (region[i].prop === "showBackground") {
                    // Not supported?
                }
            }

            regionStyle += "}";

            return regionStyle;
        }


        var captionStyles = {};

        function getCaptionStyle(caption, id) {
            var res = "",
                cStyle = captionStyles[id];

            if (cStyle) {
                for (var i = 0; i < cStyle.length; i++) {
                    res += cStyle[i].prop + ": " + cStyle[i].val + "; ";
                }
            }

            return res;
        }

        function wrap(caption, styleId) {
            var style = getCaptionStyle(caption, styleId),
                pStart = "<p><span style=\"" + style + "\">",
                pEnd = "</span></p>",
                html;

            html = pStart;

            for (var i = 0; i < caption.length; i++) {
                if (caption[i] === "<br/>" || caption[i] === "<br />") {
                    html += pEnd + pStart;
                    continue;
                }
                html += caption[i];
            }

            html += "</p>";

            return html;
        }

        return klass.create(Captioning, {
            /**
             * @static
             */

        }, {
            /**
             * subtitleFileUrl
             * videoPlayer
             */
            init: function(opts) {
                this._super(opts);
            },

            deinit: function() {
                // Clear all css created
                for (var i = 0; i < cssNodes.length; i++) {
                    cssUtil.removeCssNode(cssNodes[i]);
                }
                cssNodes = [];
                this._super();
            },

            /**
             * Implementation of abstract function
             */
            parseCaptions: function(rawString) {
                console.log("Parse Captions called in TTMLCaptioning!");
                this._parseTTML(rawString);
            },

            _parseTTML: function(ttmlString) {
                var dp = new DOMParser(),
                    ttml,
                    subs = {};

                try {
                    ttml = dp.parseFromString(ttmlString, "text/xml");
                } catch (e) {
                    console.err("Error parsing subtitles file! " + e);
                    return;
                }

                // Parse root container region
                var rootTags = ttml.getElementsByTagName("region");

                var attrs, so, id, nn, nv;
                for (var t = 0; t < rootTags.length; t++) {
                    attrs = rootTags[t].attributes;
                    so = [];
                    id = null;

                    for (var i = 0; i < attrs.length; i++) {
                        nn = attrs[i].nodeName.replace("xml:", "");
                        nv = attrs[i].nodeValue;

                        if (nn.indexOf("tts:") > -1) {
                            so.push({
                                prop: nn.replace("tts:", ""),
                                val: nv
                            });
                        } else if (nn === "id") {
                            id = nv;
                        }
                    }

                    cssUtil.addCss(createRegionStyle(so, id)).then(function(data) {
                        cssNodes.push(data.node);
                    });
                }

                // Parse styling
                var styleTags = ttml.getElementsByTagName("style");

                for (var s = 0; s < styleTags.length; s++) {
                    attrs = styleTags[s].attributes;
                    so = [];
                    id = null;

                    for (var j = 0; j < attrs.length; j++) {
                        nn = attrs[j].nodeName.replace("xml:", "");
                        nv = attrs[j].nodeValue;

                        if (nn.indexOf("tts:") > -1) {
                            so.push({
                                prop: toCssAttribute(nn.replace("tts:", "")),
                                val: nv
                            });
                        } else if (nn === "id") {
                            id = nv;
                        }
                    }
                    captionStyles[id] = so;
                }


                // Parse captions
                var captionTags = ttml.getElementsByTagName("p");
                subs.captions = [];

                var begin, end, dur, styleName, regionName, content, cNodes,
                    tmpStr;
                for (var c = 0; c < captionTags.length; c++) {
                    begin = captionTags[c].getAttribute("begin");
                    end = captionTags[c].getAttribute("end");
                    dur = captionTags[c].getAttribute("dur");
                    styleName = captionTags[c].getAttribute("style");
                    regionName = captionTags[c].getAttribute("region");

                    if (regionName) {
                        regionName = ttmlRegionCssPrefix + regionName;
                    }

                    begin = parseTime(begin);
                    end = parseTime(end);
                    dur = parseTime(dur);

                    if (end && dur === -1) {
                        dur = end - begin;
                    }

                    content = [];
                    cNodes = captionTags[c].childNodes;

                    for (var x = 0; x < cNodes.length; x++) {
                        if (cNodes[x].length && cNodes[x].nodeValue !== " ") {
                            tmpStr = cNodes[x].nodeValue;
                        } else if (cNodes[x].getAttribute) {
                            tmpStr = htmlifyInnerTag(cNodes[x]);
                        }

                        if (tmpStr) {
                            content.push(tmpStr);
                            tmpStr = null;
                        }
                    }

                    content = wrap(content, styleName);

                    if (begin > -1 && dur > -1) {
                        subs.captions.push({
                            begin: begin,
                            dur: dur,
                            content: content,
                            regionClass: regionName
                        });
                    }
                }

                this.setParsedCaptions(subs);
            }
        });
    });

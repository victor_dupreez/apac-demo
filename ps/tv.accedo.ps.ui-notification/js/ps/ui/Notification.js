/* jshint expr: true */
/**
 * Widget to display notifications. The widget is 100 % stylable with CSS and can be used for
 * intrusive and non-intrusive notifications, actionable notifications and sticky notifications.
 *
 * Through CSS this widget supports notification bar type notifications, full-screen notifications,
 * popup-style notifications or any other type of visual notification. It's all configurable using CSS.
 *
 * Usage example:

            var notification = new Notification({
                type: {
                    ATTENTION: {
                        actionable: true,
                        sticky: false,
                        id: "attention"
                    }
                }
            });

            notification.add({
                type: notification.options.type.INFO,
                header: "Test notification",
                text: "This notification will auto-disappear in 2 seconds.",
                hideDelay: 2000
            });

            notification.add({
                type: notification.options.type.INFO,
                header: "Test again",
                text: "You can continue using the app while this notification is showing."
            });

            notification.add({
                type: notification.options.type.ATTENTION,
                header: "Attention!",
                text: "This is to get your attention. You will soon be allowed to resume all the fun!"
            });

            notification.add({
                type: notification.options.type.INFO,
                header: "Yay!",
                text: "You can now resume using the app.",
                hideDelay: 1300
            });

            notification.add({
                type: notification.options.type.ERROR,
                header: "Whoops",
                text: "What to do about this error?",
                buttons: [
                    {
                        label: "Nothing!",
                        focused: true,
                        action: function () {
                            console.log("OK CLICKED!");
                        }
                    },
                    {
                        label: "Exit the Application",
                        action: function () {
                            console.log("EXIT CLICKED!");
                            window.location.reload();
                        }
                    },
                    {
                        label: "Show me puppies",
                        action: function () {
                            console.log("PUPPIES CLICKED!");
                            window.location.href = "https://www.google.com/search?q=puppies&safe=active&site=imghp&source=lnms&tbm=isch&sa=X&ei=MbWYUsWVB-P0ygPUnYDIAw&ved=0CAkQ_AUoAQ";
                        }
                    }
                ]
            });
 *
 */
define("ps/ui/Notification", [
        "ax/core",
        "ax/class",
        "ax/af/evt/type",
        "ax/af/Container",
        "ax/ext/ui/Layout",
        "ax/af/Component",
        "ax/Element",
        "ax/ext/ui/Label",
        "ax/ext/ui/Button",
        "ax/af/focusManager",
        "ax/util",
        "ax/af/mvc/AppRoot"
    ],
    function(
        core,
        klass,
        evtType,
        Container,
        Layout,
        Component,
        Element,
        Label,
        Button,
        focusMgr,
        util,
        AppRoot
    ) {

        return klass.create(Container, {}, {

            EVT_SHOW: "ps:ui:notification:show",
            EVT_HIDE: "ps:ui:notification:hide",

            /**
             * These are the default options. They can be
             * overridden and/or extended on init.
             */
            options: {
                type: {
                    INFO: {
                        sticky: false, // Notification disappears automatically
                        actionable: false, // No user interaction with the notification
                        id: "info" // ID is used as css class for this type of notification
                    },
                    ERROR: {
                        sticky: true, // Notification does not disappear automatically
                        actionable: true, // User input (via buttons)
                        id: "error"
                    }
                },

                // ms, default time a non-sticky notification is shown before hidden
                hideDelay: 5000,

                // ms, this value is the wait time between two back-to-back notifications
                transitionDelay: 0,

                // Default alignment of buttons is Horizontal
                alignment: Layout.HORIZONTAL,

                // If set, focus will be returned to this component instead of the default (previous focused component)
                returnFocus: null
            },

            // Used to keep a queue of notifications if many are fired at once
            _queue: [],

            /**
             * override's parent's init() method
             * @method
             * @protected
             */
            init: function(opts) {

                this.AppRoot = AppRoot.singleton();

                opts = opts || {};

                this._initOpts(opts);

                opts.focusable = true;
                this._super(opts);

                this._onFocusChange = util.bind(this._onFocusChange, this);

                var rootContainer = new Container({
                    type: Container,
                    root: this._root,
                    css: "wgt-notification-container"
                });

                this._dimElement = new Container({
                    type: Container,
                    css: "wgt-notification-dimmer"
                }).hide();
                rootContainer.attach(this._dimElement);

                this._rootElement = new Container({
                    type: Container,
                    parent: rootContainer,
                    css: "wgt-notification"
                });

                this._headerElement = new Label({}).addClass("header");
                this._rootElement.attach(this._headerElement);
                this._messageElement = new Label({}).addClass("message");
                this._rootElement.attach(this._messageElement);

                this._buttonsContainer = new Layout({
                    alignment: this.options.alignment,
                    type: Layout,
                    parent: this._rootElement,
                    css: "button-container",
                    autoNavigation: true
                });

                this.setOverlayVisibility(false);
                this._root.appendTo(core.root.document.body);
            },

            /**
             * Parses the options passed to constructor
             */
            _initOpts: function(opts) {
                var type = opts.type || false,
                    hideDelay = opts.hideDelay || false,
                    transitionDelay = opts.transitionDelay || false,
                    alignment = opts.alignment || false,
                    returnFocus = opts.returnFocus || false;

                if (type) {
                    for (var t in type) {
                        if (type.hasOwnProperty(t)) {
                            this.options.type[t] = type[t];
                        }
                    }
                }

                if (hideDelay) {
                    this.options.hideDelay = hideDelay;
                }

                if (transitionDelay) {
                    this.options.transitionDelay = transitionDelay;
                }

                if (alignment) {
                    this.options.alignment = alignment;
                }

                if (returnFocus) {
                    this.options.returnFocus = returnFocus;
                }
            },

            /**
             * Adds a notification to the queue
             * If there is nothing in the queue, it will be shown instantly
             */
            add: function(opts) {
                opts.type = opts.type || false,
                opts.header = opts.header || false,
                opts.text = opts.text || false,
                opts.buttons = opts.buttons || false,
                opts.hideDelay = opts.hideDelay || this.options.hideDelay,
                opts.transitionDelay = opts.transitionDelay || this.options.transitionDelay;

                if (!opts.type) {
                    //console.log("Notification error: No ''type'' specified.");
                    return false;
                }

                var notification = this._createNotification(opts);
                this._addToQueue(notification);

                return true;
            },

            clearQueue: function() {
                this._queue = [];
            },

            _createNotification: function(opts) {
                return {
                    type: opts.type,
                    header: opts.header,
                    text: opts.text,
                    buttons: opts.buttons,
                    hideDelay: opts.hideDelay,
                    transitionDelay: opts.transitionDelay,
                    sticky: opts.type.sticky,
                    actionable: opts.type.actionable
                };
            },

            _addToQueue: function(notification) {
                this._queue.push(notification);

                if (this._queue.length === 1) {
                    this._showNext();
                }
            },

            _show: function() {
                this._rootElement.removeClass("hidden");
            },

            _hide: function() {
                this._rootElement.addClass("hidden");
            },

            hideCurrent: function() {
                var self = this,
                    root = this._rootElement,
                    current = this._queue.shift(),
                    endedCallback = function() {
                        self.dispatchEvent(self.EVT_HIDE, {
                            type: current.type.id
                        });
                        if (self.options.returnFocus) {
                            focusMgr.focus(self.options.returnFocus);
                        } else {
                            self._previousFocus && focusMgr.focus(self._previousFocus);
                        }
                        self._previousFocus = undefined;
                        self._dimElement.hide();
                        self._headerElement.setText("");
                        self._messageElement.setText("");
                        self._buttonsContainer.detachAll();
                        self._rootElement.removeClass(current.type.id);
                        self._showNext();
                    };

                this.AppRoot.getView().removeEventListener(evtType.FOCUS, this._onFocusChange);

                this.setOverlayVisibility(false);
                current && setTimeout(endedCallback, current.transitionDelay);
            },

            _showNext: function() {
                if (this._queue.length) {
                    var next = this._queue[0];
                    this._showNotification(next);
                }
            },

            _showNotification: function(notification) {
                /* jshint loopfunc: true */
                var self = this;

                this._rootElement.addClass(notification.type.id);
                notification.header && this._headerElement.setText(notification.header);
                notification.text && this._messageElement.setText(notification.text);

                this.setOverlayVisibility(true);

                if (!notification.sticky) {
                    this._showTimeout = setTimeout(function() {
                        self.hideCurrent();
                    }, notification.hideDelay);
                }

                if (notification.actionable) {
                    var hasSetFocus = false,
                        buttons = [];
                    this._rootElement.addClass("actionable");
                    this._dimElement.show();

                    this._previousFocus = focusMgr.getCurFocus();
                    focusMgr.focus(this);

                    if (notification.buttons) {
                        var len = notification.buttons.length;
                        for (var i = 0; i < len; i++) {
                            var btn = new Button({
                                text: notification.buttons[i].label
                            });
                            btn.addEventListener(evtType.CLICK, (function(i) {
                                return function() {
                                    notification.buttons[i].action && notification.buttons[i].action();
                                    self.hideCurrent();
                                };
                            })(i), false);

                            // Adds a "first" and "last" class to buttons if there are more than one
                            if (len > 1) {
                                if (i === 0) {
                                    btn.addClass("first");
                                } else if (i === len - 1) {
                                    btn.addClass("last");
                                }
                            }

                            this._buttonsContainer.attach(btn);

                            if (notification.buttons[i].focused) {
                                hasSetFocus = true;
                                focusMgr.focus(btn);
                            }

                            buttons.push(btn);
                        }

                        if (!hasSetFocus) {
                            focusMgr.focus(buttons[0]);
                        }
                    }
                }

                this.dispatchEvent(this.EVT_SHOW, {
                    type: notification.type.id
                });

                this.AppRoot.getView().addEventListener(evtType.FOCUS, this._onFocusChange);
            },

            _onFocusChange: function(evt) {
                if (!this._buttonsContainer.getChildren().length) {
                    return;
                }
                if (evt && evt.target && evt.target.getParent && evt.target.getParent()) {
                    if (evt.target.getParent().getRoot) {
                        if (evt.target.getParent().getRoot() !== this._buttonsContainer) {
                            focusMgr.focus(this._buttonsContainer.getActiveChild());
                        }
                    }
                }
            },

            isShowing: function() {
                return this._queue.length > 0;
            },

            /**
             */
            deinit: function() {
                this._super();
            },

            setOverlayVisibility: function(show) {
                this[show ? "show" : "hide"]();
            }
        });
    });

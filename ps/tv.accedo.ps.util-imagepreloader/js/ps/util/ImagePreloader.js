/**
 * Util for managing preloading of images
 *
 * @author Niklas Bjorken <niklas.bjorken@accedo.tv>
 * @class ps/util/ImagePreloader
 * @param {Object} opts (optional) The options object
 * @param {Number} opts.max (optional) The max number of preloaded images to store in this instance before the oldest images are freed. Default is 5 images.
 * @example
 * // Create an instance. The max option is optional and
 * // specifies the max number of preloaded images kept
 * // in memory.
 * var ip = new ImagePreloader({ max: 10 });
 *
 * // Preload an image. preload() returns a promise that
 * // resolves when the image has loaded, or rejects when
 * // the image fails to load
 * ip.preload("http://example.com/img/image.jpg").then(
 *     function loaded(data){
 *         console.log("LOADED. Image url: " + data.value);
 *         // Do something with the image, like assign the url to an existing image in the view
 *         myView.find("my-image").setSrc(data.value);
 *
 *         // Let the preloader know you are done with an image so
 *         // that the resource can be freed.
 *         ip.doneWith("http://example.com/img/image.jpg");
 *     },
 *
 *     function failed(data){
 *         console.log("FAILED. Image url: " + data.value);
 *     }
 * );
 *
 */
define("ps/util/ImagePreloader", ["ax/class", "ax/promise"],
    function(klass, promise) {

        var ImagePreloader = klass.create({
            /**
             * @static
             */
        }, {
            // Stores a list of preloaded images
            _images: [],

            // Default maximum number of preloaded images kept in memory
            _maxImages: 5,


            /**
             * Initiates the ImagePreloader
             * @methid init
             * @private
             * @memberof ps/util/ImagePreloader#
             */
            init: function(opts) {
                opts = opts || {};

                this._images = [];
                this._maxImages = opts.max || this._maxImages;
            },

            /**
             * Deinitiates the ImagePreloader
             * @methid deinit
             * @private
             * @memberof ps/util/ImagePreloader#
             */
            deinit: function() {
                this._images = [];
            },

            /**
             * Preloads the image specified by imageUrl.
             * Returns a promise that will resolve with
             * {
             *     value: imageUrl,
             *     width: (number specifying image width),
             *     height: (number specifying image height),
             *     type: "loaded"
             * }
             * when the image has loaded successfully. The promise will
             * be rejected if the image fails to load for some reason.
             * @method preload
             * @public
             * @memberof ps/util/ImagePreloader#
             * @param {String} imageUrl URL to image resource to preload
             * @return {ax/promise} Deferred promise object
             */
            preload: function(imageUrl) {
                var img = new Image(),
                    deferred = promise.defer(),
                    _images = this._images;

                img.onload = function() {
                    deferred.resolve({
                        value: imageUrl,
                        type: "loaded",
                        width: img.width,
                        height: img.height
                    });
                };

                img.onerror = function() {
                    deferred.reject({
                        value: imageUrl,
                        type: "error"
                    });
                };

                img.src = imageUrl;
                this._images.push(img);

                if (this._images.length > this._maxImages) {
                    this._images.shift();
                }

                return deferred.promise;
            },

            /**
             * Call this to remove an image from the list of preloaded images
             * to free up that resource.
             * @method doneWith
             * @public
             * @memberof ps/util/ImagePreloader#
             * @param {String} imageUrl URL to image resource to free
             */
            doneWith: function(imageUrl) {
                var i = 0,
                    imgs = this._images,
                    img;

                for (; i < imgs.length; i++) {
                    if (imgs[i].src === imageUrl) {
                        img = imgs.splice(i, 1);
                        img[0].onload = null;
                        img[0].onerror = null;
                        break;
                    }
                }
            }

        });

        return ImagePreloader;
    });

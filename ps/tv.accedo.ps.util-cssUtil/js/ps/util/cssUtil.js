/*global domReady*/
/**
 * CSS util to replace the temporary XDK CSS Util which currently
 * only allows adding css, not removing or managing it.
 *
 * @module ps/util/cssUtil
 * @author Niklas Bjorken <niklas.bjorken@accedo.tv>
 * @example
 * [...]
 * setup: function() {
 *     // Add dynamic css
 *     cssUtil.addCss(".myView { color: red }").then(function(data) {
 *         this._cssNodeForThisView = data.node;
 *     });
 * },
 * [...]
 * deinit: function() {
 *     // Clean up dynamic css
 *     cssUtil.removeCssNode(this._cssNodeForThisView);
 * }
 */
define("ps/util/cssUtil", [
    "ax/promise"
], function(promise) {

    return {
        /**
         * Add some CSS into document.head. Returns a promise containing
         * a reference to the CSS node. Promise resolves when the CSS node
         * has been added to the DOM, but not necessarily before the CSS
         * has been parsed and applied!
         *
         * @method addCss
         * @memberof module:ax/cssUtil
         * @param {String} css the CSS rules to add
         * @return {ax/promise} Deferred promise object
         */
        addCss: function(css) {
            var deferred = promise.defer();

            domReady(function() {
                var head = document.head || document.getElementsByTagName("head")[0],
                    node = document.createElement("style");

                node.type = "text/css";
                if (node.styleSheet) {
                    node.styleSheet.cssText = css;
                } else {
                    node.appendChild(document.createTextNode(css));
                }
                head.appendChild(node);

                deferred.resolve({
                    node: node
                });
            });

            return deferred.promise;
        },

        /**
         * Remove a CSS node
         * @method removeCssNode
         * @param {DOMNode} node the CSS node to remove from DOM
         * @memberof module:ax/cssUtil
         */
        removeCssNode: function(node) {
            if (node && node.parentNode) {
                node.parentNode.removeChild(node);
            }
        },

        /**
         * Append CSS to a CSS node
         * @method appendCss
         * @param {String} css the CSS rules to append to node
         * @param {DOMNode} node the CSS node to remove from DOM
         * @memberof module:ax/cssUtil
         */
        appendCss: function(css, node) {
            if (node) {
                if (node.styleSheet) {
                    node.styleSheet.cssText += css;
                } else {
                    node.appendChild(document.createTextNode(css));
                }
            }
        }
    };
});

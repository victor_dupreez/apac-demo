require({
    "paths": {
        "ax/af": "../dep/tv.accedo.xdk.af/js/ax/af",
        "ax/af/mvc/css": "../dep/tv.accedo.xdk.af/js/ax/af/mvc/css",
        "json": "../dep/tv.accedo.xdk.base/js/ax/loader/json",
        "image": "../dep/tv.accedo.xdk.base/js/ax/loader/image",
        "ax": "../dep/tv.accedo.xdk.base/js/ax",
        "css": "../dep/tv.accedo.xdk.base/js/ax/loader/css",
        "ax/ext/device/samsung": "../dep/tv.accedo.xdk.ext.device-samsung/js/ax/ext/device/samsung",
        "ax/ext/device/tizen": "../dep/tv.accedo.xdk.ext.device-tizen/js/ax/ext/device/tizen",
        "ax/ext/history": "../dep/tv.accedo.xdk.ext.history/js/ax/ext/history",
        "ax/ext/mouse": "../dep/tv.accedo.xdk.ext.mouse/js/ax/ext/mouse",
        "ax/ext/ui": "../dep/tv.accedo.xdk.ext.ui-basic/js/ax/ext/ui",
        "ax/ext/ui/interface": "../dep/tv.accedo.xdk.ext.ui-basic/js/ax/ext/ui/interface",
        "ax/ext/ui/grid": "../dep/tv.accedo.xdk.ext.ui-grid/js/ax/ext/ui/grid",
        "ax/ext/ui/input": "../dep/tv.accedo.xdk.ext.ui-input/js/ax/ext/ui/input"
    }
});

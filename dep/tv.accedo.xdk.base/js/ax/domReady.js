// XDK notes: due to requirements, this modules is modified to become a global object
/**
 * A global function, that registers callbacks for DOM ready event.
 * @module domReady
 */
window.domReady = (function () {
    "use strict";

    var isReady = false,
        callbacks = [];


    // Sets the page as loaded.
    function pageLoaded() {
        if (!isReady) {
            isReady = true;

            var i;
            for (i = 0; i < callbacks.length; i += 1) {
                callbacks[i](document);
            }
        }
    }

    if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", pageLoaded, false);
        window.addEventListener("load", pageLoaded, false);
        window.addEventListener("show", pageLoaded, false);
        window.onShow = pageLoaded;
    } else if (window.attachEvent) {
        window.attachEvent("onload", pageLoaded);
    }

    if (document.readyState === "complete") {
        pageLoaded();
    }

    /**
     * Registers a callback for DOM ready. If DOM is already ready, the
     * callback is called immediately. Globally accessible.
     * @param {Function} callback the callback function to register
     * @method
     * @memberof domReady
     */
    function domReady(callback) {
        if (isReady) {
            callback(document);
        } else {
            callbacks.push(callback);
        }
        return domReady;
    }

    return domReady;
})();
/**
 * Thennable is a helper module for promise to handle `thenable` type of methods. Please
 * refer to promise-a-plus specification 2.3.3 for more details
 * </p>
 *
 * @class ax/Thennable
 */

define("ax/Thennable", [
    "require",
    "ax/util"
], function (
    require,
    util
) {
    "use strict";

    var getPromise = function () {
        return require("./promise");
    };

    /**
     * Constructor method
     */
    function Thennable(obj, then) {
        this.pending = true;
        this.obj = obj;
        this.then = then;
    }

    /**
     * Reject the thennable instance
     * @param  {T} reason Reason to reject the thennable
     * @private
     */
    Thennable.prototype.reject = function (reason) {
        if (!this.pending) {
            return;
        }
        this.pending = false;
        this.rejecter(reason);
    };

    /**
     * Fulfil the thennable instance
     * @param  {T} value Value resolving the thennable
     * @private
     */
    Thennable.prototype.fulfil = function (value) {

        if (!this.pending) {
            return;
        }

        this.pending = false;

        if (getPromise().isPromise(value)) {
            value.then(this.resolver, this.rejecter);
            return;
        }

        try {
            if (!Thennable.handle(value, this.resolver, this.rejecter)) {
                this.resolver(value);
            }
        } catch (e) {
            this.rejecter(e);
        }
    };

    /**
     * Run the thennable
     * @param  {Function} resolver callback when thennable fulfilled
     * @param  {Function} rejecter callback when thennable rejected
     * @method run
     * @memberOf ax/Thennable#
     */
    Thennable.prototype.run = function (resolver, rejecter) {

        var obj = this.obj,
            then = this.then;

        this.resolver = resolver;
        this.rejecter = rejecter;

        try {
            then.call(obj, util.bind(this.fulfil, this), util.bind(this.reject, this));
        } catch (e) {
            this.reject(e);
        }
    };

    /**
     * Try to handle a value as Thennable
     * @param  {T} value    The value that maybe a Thennable
     * @param  {Method} resolver Callback on Thennable fulfilled
     * @param  {Method} rejecter Callback on Thennable rejected
     * @return {Boolean}          True if value has then handled as Thennable
     * @method handle
     * @memberOf ax/Thennable
     */
    Thennable.handle = function (value, resolver, rejecter) {

        if (!value) {
            return false;
        }

        if (getPromise().isPromise(value)) {
            return false;
        }

        if (!util.isFunction(value) && !util.isPlainObject(value)) {
            return false;
        }

        // Avoid calling ES5 getter twice. See https://promisesaplus.com/#point-75
        var then = value.then;

        if (!util.isFunction(then)) {
            return false;
        }

        var thennable = new Thennable(value, then);
        thennable.run(resolver, rejecter);

        return true;
    };

    return Thennable;
});
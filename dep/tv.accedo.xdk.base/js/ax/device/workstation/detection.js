define("ax/device/workstation/detection", function () {
    "use strict";
    var detection = (function () {
        var agent;
        if ( !! navigator && !! navigator.userAgent) {
            agent = navigator.userAgent.toLowerCase();
            if ((agent.indexOf("windows nt") !== -1) || (agent.indexOf("linux") !== -1) || (agent.indexOf("macintel") !== -1) || (agent.indexOf("macintosh") !== -1)) {
                //only support chrome and firefox
                if (agent.indexOf("chrome") !== -1 || agent.indexOf("firefox") !== -1) {
                    return true;
                }
            }
        }
        return false;
    })();
    return detection;
});
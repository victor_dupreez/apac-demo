/**
 * System class to handle the system api and connection checking
 *
 * ### Config Params
 *
 *  Attribute | Value
 * --------- | ---------
 * Key      | device.workstation.1080p
 * Type     | Boolean
 * Desc     | Set the workstation display resolution 1080p instead of 720p. Default will use the 720p
 * Default  | false
 * Usage    | {@link module:ax/config}
 * @class ax/device/workstation/System
 * @augments ax/device/shared/Html5System
 */
define("ax/device/workstation/System", [
    "ax/class",
    "ax/device/shared/Html5System",
    "ax/config",
    "ax/device/shared/ConnectionPoller",
    "ax/device/interface/System",
    "ax/util",
    "ax/core",
    "ax/promise"
], function (
    klass,
    Html5System,
    config,
    ConnectionPoller,
    ISystem,
    util,
    core,
    promise
) {
    "use strict";

    return klass.create(Html5System, {}, {
        /**
         * @protected
         * @name _device
         * @memberof ax/device/workstation/System#
         */
        _device: undefined,

        /**
         * @protected
         * @name _document
         * @memberof ax/device/workstation/System#
         */
        _document: undefined,

        /**
         * @private
         * @name __bindedOnVisibilityChange
         * @memberof ax/device/workstation/System#
         */
        __bindedOnVisibilityChange: undefined,

        /**
         * The number of network status change event listeners
         *
         * @private
         * @member {Number}
         * @memberof ax/device/workstation/System#
         */
        __networkEventListenerCount: 0,

        init: function () {
            this._super();
            this.__checkNetworkStatus();
        },
        /**
         * Has mouse on workstation
         * @public
         * @name hasMouse
         * @memberof ax/device/workstation/System#
         */
        hasMouse: function () {
            return true;
        },
        /**
         * Has hardware keyboard
         * @public
         * @name hasFixedKeyboard
         * @memberof ax/device/workstation/System#
         */
        hasFixedKeyboard: function () {
            return true;
        },
        /**
         * To get the display resolution and now it is either 1280x720 or 1920x1080.Default is 1280x720.To use 1080p
         * need to set "device.workstation.1080p" to be true
         * @public
         * @name getDisplayResolution
         * @return {Object} object {width:1280,height:720}
         * @memberof ax/device/workstation/System#
         */
        getDisplayResolution: function () {
            //the display resolution in workstation are 1280p and 1080p
            var resolution1080p = config.get("device.workstation.1080p", false);
            if (resolution1080p) {
                return {
                    width: 1920,
                    height: 1080
                };
            }
            return {
                width: 1280,
                height: 720
            };
        },
        /**
         * Workstation supports SSL
         * @public
         * @name supportSSL
         * @return {Boolean} True
         * @memberof ax/device/workstation/System#
         */
        supportSSL: function () {
            return true;
        },
        /**
         * Override the abstract system and get the current network status
         * @method getNetworkStatus
         * @return {Promise.<Boolean>}  Return true if it is conencted to internet
         * @public
         * @memberof ax/device/workstation/System#
         */
        getNetworkStatus: function () {
            return promise.resolve(this._isInternetConnected());
        },
        /**
         * To check the Internet connected or not. Relies on navigator.onLine
         * @method _isInternetConencted
         * @return {Boolean} Return true if it is connected
         * @protected
         * @memberof ax/device/workstation/System#
         */
        _isInternetConnected: function () {
            return navigator.onLine;
        },
        /**
         * To override the abstract network status checking which replace the ajax network checking method. Now it will create a poller that keep tracking on the status of the device.
         * @method _postInit
         * @protected
         * @memberof ax/device/workstation/System#
         */
        __checkNetworkStatus: function () {
            var checkStatus, onStatusChange;

            //the checking method
            checkStatus = util.bind(function (__checkStatusDoneCb) {

                __checkStatusDoneCb(this._isInternetConnected());

            }, this);

            //the checking callback
            onStatusChange = util.bind(function (status) {

                this.dispatchEvent(ISystem.EVT_NETWORK_STATUS_CHANGED, status);

            }, this);

            //start the poller, set check method and callback.
            this._internetPoller = new ConnectionPoller(checkStatus, onStatusChange, {
                networkStatus: this._isInternetConnected()
            });
        },
        /**
         * Overrides parent method to start the polling on add event listener.
         *
         * @method
         * @param {ax/af/evt/type|String} type event type
         * @param {Function} handler event listener function
         * @param {Boolean} [once] set to true if listener will be called only once
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @memberof ax/device/workstation/System#
         */
        addEventListener: function (type, handler, once) {
            if (this._internetPoller && type === ISystem.EVT_NETWORK_STATUS_CHANGED) {
                if (this.__networkEventListenerCount === 0) {
                    // start the polling when adding the event listener
                    this._internetPoller.startPolling();
                }

                this.__networkEventListenerCount++;
            }

            return this._super(type, handler, once);
        },
        /**
         * Overrides parent method to stop the polling on event listener remove.
         *
         * @method
         * @param {ax/af/evt/type|String} type Event type
         * @param {Function} [handler] event listener function.  If not provided, all listeners will be removed for the specific type.
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @memberof ax/device/workstation/System#
         */
        removeEventListener: function (type, handler) {
            if (this._internetPoller && type === ISystem.EVT_NETWORK_STATUS_CHANGED) {
                this.__networkEventListenerCount--;

                if (this.__networkEventListenerCount === 0) {
                    // stop the polling if no more listener is listening
                    this._internetPoller.stopPolling();
                }
            }

            return this._super(type, handler);
        },
        /**
         * Setting the screen saver On/Off
         * @method setScreenSaver
         * @param {Boolean} flag True to turn on and off to turn off.
         * @return {Boolean}  Return false.
         * @memberof ax/device/workstation/System#
         */
        /*jshint unused:false*/
        setScreenSaver: function (flag) {
            return false;
        },
        /*jshint unused:true*/
        /**
         * Setting the system to mute/unmute.
         * @method setSystemMute
         * @param {Boolean} flag True to mute and false to unmute
         * @return {Boolean} Return false;
         * @memberof ax/device/workstation/System#
         */
        /*jshint unused:false*/
        setSystemMute: function (flag) {
            return false;
        },
        /*jshint unused:true*/
        /**
         * Power off the system
         * @method powerOff
         * @return {Boolean} Return false
         * @memberof ax/device/workstation/System#
         */
        powerOff: function () {
            return false;
        },
        /**
         * @method redraw
         * @param {HTMLElement} [element=document.body] the target element. Default will be document.body
         * @param {Boolean} false when not support
         * @memberof ax/device/workstation/System#
         */
        /*jshint unused:false*/
        redraw: function (element) {
            return false;
        },
        /*jshint unused:true*/
        /**
         * Exit the application
         * @method exit
         * @param {Object} [obj] To set the information when exit
         * @param {Boolean} [obj.toTV] True when exit back to TV source, false will go back to smartHub/App Store page.Default is true
         * @return {Boolean} Return false
         * @memberof ax/device/workstation/System#
         */
        exit: function () {
            return false;
        },
        /**
         * Whether same-origin policy exists in the device (i.e. cross domain ajax is not allowed). Modern browsers should have this policy.
         * @name hasSOP
         * @method
         * @return {Boolean} Return true if cross domain ajax is not allowed natively. Proxy / Allow-Origin header setup may be needed.
         * @memberof ax/device/workstation/System#
         */
        hasSOP: function () {
            return true;
        }
    });
});
/**
 * Id class to handle the device information like firmware version
 * @class ax/device/workstation/Id
 * @extends ax/device/AbstractId
 */
define("ax/device/workstation/Id", ["ax/class", "ax/device/AbstractId", "ax/device/interface/Id", "ax/console"], function (klass, abstrId, IId, console) {
    "use strict";
    return klass.create(abstrId, [IId], {}, {
        /**
         * To return the device type as "workstation"
         * @method getDeviceType
         * @deprecated replaced with getHardwareType
         * @memberof ax/device/workstation/Id#
         */
        getDeviceType: function () {
            return "Workstation";
        },
        /**
         * To return the hardware type
         * @method getHardwareType
         * @returns {ax/device/interface/Id.HARDWARE_TYPE} device type as {@link ax/device/interface/Id.HARDWARE_TYPE.WORKSTATION}
         * @memberof ax/device/workstation/Id#
         */
        getHardwareType: function () {
            return IId.HARDWARE_TYPE.WORKSTATION;
        },
        /**
         * To get the firmware version
         * @method getFirmware
         * @memberof ax/device/workstation/Id#
         */
        getFirmware: function () {
            //to get the browser and version
            var name = "workstation",
                version = "DummyFirmware",
                ua = navigator.userAgent.toLowerCase();
            try {
                if (ua.indexOf("chrome") !== -1) {
                    name = "Chrome";
                    version = (/chrome\/([\d.]+)/.exec(ua))[1];
                } else if (ua.indexOf("firefox") !== -1) {
                    name = "Firefox";
                    version = (/firefox\/([\d.]+)/.exec(ua))[1];
                } else if (ua.indexOf("opera") !== -1) {
                    name = "opera";
                    version = (/version\/([\d.]+)/.exec(ua))[1];
                } else if (ua.indexOf("safari") !== -1) {
                    name = "safari";
                    version = (/version\/([\d.]+)/.exec(ua))[1];
                }
            } catch (ex) {
                console.warn("unable to determine the firmware");
            }
            return name + "/" + version;
        }
    });
});
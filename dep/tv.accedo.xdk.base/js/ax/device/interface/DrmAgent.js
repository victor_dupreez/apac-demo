/**
 * The drm agent interface.
 *
 * @class ax/device/interface/DrmAgent
 */
define(["ax/Interface"], function (Interface) {
    "use strict";
    var IDrmAgent = Interface.create("DrmAgent", {
        /**
         * Send the license
         * @name sendLicense
         * @function
         * @abstract
         * @return {Promise.<Boolean>} True if sending license properly
         * @memberof ax/device/interface/DrmAgent
         * @public
         */
        sendLicense: ["url", "opts"],
        /**
         * Set the drm messenger
         * @name setDrmMessenger
         * @function
         * @abstract
         * @param {ax/device/interface/DrmMessenger} messenger the drm messenger to generate that drm.
         * @return {Promise.<Boolean>} True if setting the drm messenger correct
         * @memberof ax/device/interface/DrmAgent
         * @public
         */
        setDrmMessenger: ["messenger"]
    });

    /**
     * Collection of the drm agent sucess result code.
     * <http://developer.lge.com/webOSTV/api/webos-service-api/drm/?wos_flag=allFlipOpen#senddrmmessage>
     * @enum {String}
     * @memberof ax/device/interface/DrmAgent
     */
    IDrmAgent.RESULT = {
        /** Sucessful */
        "0": "Successful",
        /**  Unknown */
        "1": "Unknown error: failed because an unspecified error occurred.",
        /**  Cannot process */
        "2": "Cannot process request",
        /**  Unknow MIME */
        "3": "Unknown MIME type",
        /**  User Consent needed */
        "4": "User consent needed"
    };
    /**
     * Collection of the drm agent error code.
     * <http://developer.lge.com/webOSTV/api/webos-service-api/drm/?wos_flag=allFlipOpen#getrightserror>
     * @enum {String}
     * @memberof ax/device/interface/DrmAgent
     */
    IDrmAgent.ERROR = {
        /** Vendor error */
        "500": "Vendor managed error",
        /** API is not supported */
        "501": "This API is not supported in the activated drm",
        /** No matching client Id */
        "502": "There is no process matching to input client id",
        /** Cannot find key file */
        "503": "It cannot find key file in drm store",
        /** Invalid data or format */
        "504": "A part of whole parameters is not valid data or format",
        /** Not supported drm type */
        "505": "It's not support drmType",
        /** Invalid key file */
        "506": "The key file is not valid format",
        /** Cannot get valid time */
        "507": "It cannot get the valid time information"
    };
    return IDrmAgent;
});
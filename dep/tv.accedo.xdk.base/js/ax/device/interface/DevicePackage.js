/**
 *
 * @class ax/device/interface/DevicePackage
 */
define("ax/device/interface/DevicePackage", [
    "ax/Interface"
], function (
    Interface
) {
    "use strict";
    var DevicePackage = Interface.create("DevicePackage", {
        /**
         * set up the Device Package
         * @name setup
         * @function
         * @memberof ax/device/interface/DevicePackage
         * @param {Function} onDeviceLoaded callback when the device is loaded.
         * @public
         */
        setup: ["onDeviceLoaded"],
        /**
         * get the id of the device package
         * @name init
         * @function
         * @memberof ax/device/interface/DevicePackage
         * @returns {String} the id of the device package
         * @public
         */
        getId: [],
        /**
         * get the default player when no players provided
         * @name getDefaultPlayer
         * @function
         * @memberof ax/device/interface/DevicePackage
         * @returns {String[]} Array of the player
         * @public
         */
        getDefaultPlayer: []
    });

    return DevicePackage;
});
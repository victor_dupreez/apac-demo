/**
 * Suspendable is an interface for {@link ax/device/interface/Player} instance. Any player supporting suspension should implement this interface.<br/>
 * A suspended playback can be restored later according to the application requirement.
 *
 * @class ax/device/interface/Suspendable
 * @see {@link ax/device/interface/Suspendable.SuspendedPlayback}
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define("ax/device/interface/Suspendable", [
    "ax/Interface"
], function (
    Interface
) {
    "use strict";

    var Suspendable = Interface.create("Suspendable", {
        /**
         * Suspends the current playback, returning a {@link ax/device/interface/Suspendable.SuspendedPlayback} object for restoration.
         *
         * @method suspend
         * @abstract
         * @returns {ax/device/interface/Suspendable.SuspendedPlayback} The suspended playback
         * @memberof ax/device/interface/Suspendable#
         */
        suspend: [],

        /**
         * Gets the number of current active suspended playbacks.
         * All suspended playback are considered as active unless they are disposed.
         *
         * @method getSuspendedCount
         * @abstract
         * @returns {Number} The number of active suspended playbacks
         * @memberof ax/device/interface/Suspendable#
         */
        getSuspendedCount: []
    });

    /**
     * Media information.
     *
     * @typedef {Object} MediaMeta
     * @property {String} url The url of the media
     * @property {Object} opts The JSON object storing the other metadata such as type, drm, etc
     */

    /**
     * SuspendedPlayback is an interface for playback restoration, which will be returned by {@link ax/device/interface/Suspendable#suspend}.<br/>
     * A suspended playback can be restored later according to the application requirement.<br/>
     * Suspended playback object may hold some system resources. The framework has no knowledge on when they can be release.
     * Therefore, it is the application responsibility to dispose the object after it is no longer needed, no matter it has been restored or not.
     *
     * @class ax/device/interface/Suspendable.SuspendedPlayback
     * @see {@link ax/device/interface/Suspendable}
     * @author Marco Fan <marco.fan@accedo.tv>
     */
    Suspendable.SuspendedPlayback = Interface.create("SuspendedPlayback", {
        /**
         * Restores the suspended playback.
         * The playback states before suspension will retain.
         *
         * @method restore
         * @abstract
         * @param {Function} cb The callback that to be called when the restoration completes
         * @memberof ax/device/interface/Suspendable.SuspendedPlayback#
         */
        restore: ["cb"],

        /**
         * Disposes the suspended playback, destroying the playback states before suspension.
         * A disposed playback cannot be restored anymore.
         *
         * @method dispose
         * @abstract
         * @memberof ax/device/interface/Suspendable.SuspendedPlayback#
         */
        dispose: [],

        /**
         * Returns the media information.
         *
         * @method getMediaMeta
         * @abstract
         * @returns {MediaMeta} The media metadata
         * @memberof ax/device/interface/Suspendable.SuspendedPlayback#
         */
        getMediaMeta: [],

        /**
         * Returns the player instance who is responsible for restoration.
         *
         * @method getPlayer
         * @abstract
         * @returns {ax/device/interface/Player} The player instance
         * @memberof ax/device/interface/Suspendable.SuspendedPlayback#
         */
        getPlayer: []
    });

    return Suspendable;
});
/**
 * Preloadable is an interface for {@link ax/device/interface/Player} instance. Any player supporting preloading should implement this interface.<br/>
 * Preloading is an operation that pre-load a media while another media is being played.
 * This helps to reduce the waiting time from the user perspective, as the preloading media could be buffered in the background.
 *
 * @class ax/device/interface/Preloadable
 * @see {@link ax/device/interface/Preloadable.Preloaded}
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define("ax/device/interface/Preloadable", [
    "ax/Interface",
    "ax/class"
], function (
    Interface,
    klass
) {
    "use strict";

    var Preloadable = Interface.create("Preloadable", {
        /**
         * Preloads a media for playback.<br/>
         * This interface is identical to {@link ax/device/interface/Player#load}, except that the media is loaded at a background player.
         *
         * @method preload
         * @abstract
         * @param {String} url The URL of the media
         * @param {Object} [opts] the options for loading the media
         * @param {String} [opts.codec] The media codec
         * @param {String} [opts.type] The media type
         * @param {String} [opts.drm] The DRM to be used
         * @returns {ax/device/interface/Preloadable.PreloadedPlayback} The metadata of the preloading
         * @memberof ax/device/interface/Preloadable#
         */
        preload: ["url", "opts"],

        /**
         * Gets the number of active preloaded playback.
         * All preloaded playbacks are considered active before they got disposed.
         *
         * @method getPreloadedCount
         * @abstract
         * @returns {Number} The number of active preloaded playbacks
         * @memberof ax/device/interface/Preloadable#
         */
        getPreloadedCount: []
    });

    /**
     * PreloadedPlayback is an interface for playing a preloaded playback, which will be returned by {@link ax/device/interface/Preloadable#preload}.<br/>
     * A preloaded playback can be played at anytime. It should be disposed if it is no longer needed, no matter it is played or not.
     *
     * @class ax/device/interface/Preloadable.PreloadedPlayback
     * @see {@link ax/device/interface/Preloadable}
     * @author Marco Fan <marco.fan@accedo.tv>
     */
    Preloadable.PreloadedPlayback = Interface.create("PreloadedPlayback", {
        /**
         * Plays the preloaded playback.
         *
         * @method play
         * @abstract
         * @memberof ax/device/interface/Preloadable.PreloadedPlayback#
         */
        play: ["pos"],

        /**
         * Disposes the preloaded playback, destroying the playback resources.
         * A disposed playback cannot be played anymore.
         *
         * @method dispose
         * @abstract
         * @memberof ax/device/interface/Preloadable.PreloadedPlayback#
         */
        dispose: [],

        /**
         * Returns the media information.
         *
         * @method getMediaMeta
         * @abstract
         * @returns {MediaMeta} The media metadata
         * @memberof ax/device/interface/Preloadable.PreloadedPlayback#
         */
        getMediaMeta: [],

        /**
         * Returns the player instance who is responsible for restoration.
         *
         * @method getPlayer
         * @abstract
         * @returns {ax/device/interface/Player} The player instance
         * @memberof ax/device/interface/Preloadable.PreloadedPlayback#
         */
        getPlayer: []
    });

    return Preloadable;
});
/**
 * The drm agent creator interface.
 *
 * @class ax/device/interface/DrmMessenger
 */
define(["ax/Interface"], function (Interface) {
    "use strict";
    return Interface.create("DrmMessenger", {
        /**
         * Create the drm message
         * @name init
         * @function
         * @abstract
         * @param {String} drm the type of drm
         * @memberof ax/device/interface/DrmMessenger
         * @public
         */
        init: ["drm"],
        /**
         * Create the drm message
         * @name create
         * @function
         * @abstract
         * @param {String} message the drm message
         * @param {Function} onResult the success callback
         * @param {Function} onError the fail callback
         * @return {Promise.<Boolean>} whether successfully send the message
         * @memberof ax/device/interface/DrmMessenger
         * @public
         */
        sendMessage: ["message", "onResult", "onError"]
    });
});
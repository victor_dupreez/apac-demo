/**
 * The subtitle strategy interface, which will be awared by subtitle.
 *
 * @class ax/device/interface/SubtitleStgy
 */
define("ax/device/interface/SubtitleStgy", ["ax/Interface"], function (Interface) {
    "use strict";
    var SubtitleStgy = Interface.create("SubtitleStgy", {
        /**
         * Set the player to the subtitle strategy.
         * @name setPlayer
         * @function
         * @abstract
         * @memberof ax/device/interface/SubtitleStgy
         * @public
         */
        setPlayer: ["player"],
        /**
         * show the substitle.
         * @name showSubtitle
         * @function
         * @abstract
         * @param {String} id the subtitle id
         * @returns {Promise.<Boolean>} the result of showing substitle. True when successfully loaded.
         * @memberof ax/device/interface/SubtitleStgy
         * @public
         */
        showSubtitle: ["id"],
        /**
         * hide the subtitle
         * @name hideSubtitle
         * @function
         * @abstract
         * @returns {Promise.<Boolean> the result of hiding subtitle. True when succesfully hided.
         * @memberof ax/device/interface/SubtitleStgy
         * @public
         */
        hideSubtitle: [],
        /**
         * get the current subtitle
         * @name getCurrentSubtitle
         * @function
         * @abstract
         * @return {Promise.<ax/device/Subtitle.subsitle>} the current subtitle
         * @memberof ax/device/interface/SubtitleStgy
         * @public
         */
        getCurrentSubtitle: [],
        /**
         * get all the available subtitle
         * @name getSubtitles
         * @function
         * @abstract
         * @return {Promise.<ax/device/Subtitle.subsitle[]>} the available substitle array
         * @memberof ax/device/interface/SubtitleStgy
         * @public
         */
        getSubtitles: []

    });
    /**
     * The type constant for internal subtitle strategy.
     * @property {String}
     * @memberof ax/device/interface/SubtitleStgy
     */
    SubtitleStgy.TYPE = "internal";

    return SubtitleStgy;
});
/**
 * The System interface. Implement to handle the system related functionality.
 *
 * @class ax/device/interface/System
 * @author Mike Leung <mike.leung@accedo.tv>
 */

define("ax/device/interface/System", ["ax/Interface"], function(Interface) {
    "use strict";
    var System = Interface.create("System", {
        /**
         * Setting the screen saver On/Off
         * @name setScreenSaver
         * @method
         * @abstract
         * @param {Boolean} flag True to turn on and off to turn off.
         * @return {Boolean}  Return true if success or no state change. Return false if system API not available.
         * @memberof ax/device/interface/System
         */
        setScreenSaver: ["flag"],
        /**
         * Setting the system to mute/unmute.
         * @name setSystemMute
         * @method
         * @abstract
         * @param {Boolean} flag True to mute and false to unmute
         * @return {Boolean}  Return true if
         * success or no state change. Return false if system API not available.
         * @memberof ax/device/interface/System
         */
        setSystemMute: ["flag"],
        /**
         * Power off the system
         * @name powerOff
         * @method
         * @abstract
         * @return {Boolean} Return false if system API not available.
         * @memberof ax/device/interface/System
         */
        powerOff: [],
        /**
         * Exit the application
         * @name exit
         * @method
         * @abstract
         * @param {Object} [obj] To set the information when exit
         * @param {Boolean} [obj.toTV] True when exit back to TV source, false will go back to smartHub/App Store page.Default is true
         * @return {Boolean} Return false if system API not available.
         * @memberof ax/device/interface/System
         */
        exit: [],
        /**
         * Determine whether device support mouse
         * @name hasMouse
         * @method
         * @abstract
         * @return {Boolean} Return true if the platform has pointer devices/emulation support.Return false if not available.
         * @memberof ax/device/interface/System
         */
        hasMouse: [],
        /**
         * Determine whether device support hardware keyboard
         * @name hasFixedKeyboard
         * @method
         * @abstract
         * @return {Boolean} Return true if the platform has hardware keyboard support..Return false if not available.
         * @memberof ax/device/interface/System
         */
        hasFixedKeyboard: [],
        /**
         * @typedef Resolution
         * @type {Object}
         * @property {Number} width in pixels
         * @property {Number} height in pixels
         * @memberof ax/device/interface/System
         */
        /**
         * Get the getDisplayResolution
         * @name getDisplayResolution
         * @method
         * @abstract
         * @return {ax/device/interface/System.Resolution} Return the display resolution from of the platform.Return {width:0,height:0} if not available.
         * @memberof ax/device/interface/System
         */
        getDisplayResolution: [],
        /**
         * Whether SSL is supported in the device
         * @name suportSSL
         * @method
         * @abstract
         * @return {Boolean} Return true if the platform support SSL certificate verification.
         * @memberof ax/device/interface/System
         */
        supportSSL: [],
        /**
         * Get the current network status
         * @name getNetworkStatus
         * @method
         * @abstract
         * @return {Promise.<Boolean>}  Return true if the network is connected.
         * @memberof ax/device/interface/System
         */
        getNetworkStatus: [],
        /**
         * Since some devices may be unable to detect the update of the dom, it may only repaint without reflow. It may cause issues on the display.
         * It provides a way to update the elements after the update and force it to redraw the element.
         * @name redraw
         * @method
         * @param {HTMLElement} [element=document.body] the target element. Default will be document.body
         * @memberof ax/device/interface/System
         */
        redraw:["element"]

    });

    /**
     * The event when changes in network connection. True if network is connected
     * @event EVT_NETWORK_STATUS_CHANGED
     * @type {Boolean} 
     * @memberof ax/device/interface/System
     * @example
     * //add the event listener to the system, where the event type is from the interface ax/device/interface/System.EVT_NETWORK_STATUS_CHANGED
     * Device.system.addEventListener(ISystem.EVT_NETWORK_STATUS_CHANGED, function(newStatus){
     *      //when there is status change, it will receive the new status and developers can perform action   
     *      console.log("the status changed into"+ newStatus);
     * });
     *
     */
    System.EVT_NETWORK_STATUS_CHANGED = "device:system:network-status-changed";

    return System;
});
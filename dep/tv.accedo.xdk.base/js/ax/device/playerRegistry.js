/**
 * Media player registery, registers media players along with their capabilities.
 * During preparation of playback, this registry will be consulted for the appropriate player.
 * @module ax/device/playerRegistry
 */
define("ax/device/playerRegistry", ["ax/util", "require"], function (util, require) {
    "use strict";    
    var mediaRegistry = [];
    return {
        /**
         * Register a player implementation
         * @method
         * @param {ax/device/interface/Player} playerObj the player implementation to register
         * @param {string} id - the identifier for the player instance at runtime
         * @memberof module:ax/device/playerRegistry
         */
        register: function (playerObj, id) {
            var cap = playerObj.getCapabilities(),
                types = cap.type || ["*"],
                drms = cap.drms || [],
                type, drm, i, object = {
                    type: {},
                    drm: {},
                    player: null
                };

            if (!util.isArray(types)) {
                types = [types];
            }

            if (!util.isArray(drms)) {
                drms = [drms];
            }

            for (i in types) {
                type = types[i];
                if (type) {
                    object.type[type] = true;
                }
            }

            for (i in drms) {
                drm = drms[i];
                if (drm) {
                    object.drm[drm] = true;
                }
            }

            playerObj.setId(id);
            object.id = id;
            object.player = playerObj;

            mediaRegistry.push(object);
        },
        /**
         * Unregister a player implementation
         * @method
         * @param {ax/device/interface/Player} playerObj the player implementation to unregister
         * @memberof module:ax/device/playerRegistry
         */
        unregister: function (playerObj) {
            util.each(mediaRegistry, function (obj, i) {
                if (obj.player === playerObj) {
                    playerObj.reset();
                    mediaRegistry.splice(i, 1);
                    return util.breaker;
                }
            });
        },
        /**
         * deinit the playerRegistry and deinit and unregister all the player
         * @method deinit
         * @memberof module:ax/device/playerRegistry
         */
        deinit: function () {
            util.each(mediaRegistry, util.bind(function (obj) {
                this.unregister(obj.player);
                obj.player.deinit();
            }, this));
        },
        /**
         * Get the registered player instance by Player
         * @method getPlayerByClass
         * @param {String} klass The name of the player class. e.g. "ax/device/shared/Html5Player".
         * @return {ax/device/interface/Player|Null} player instance. Null if not found
         * @memberOf module:ax/device/playerRegistry
         */
        getPlayerByClass: function (klass) {

            var playerClass = require(klass),
                player = null;
            util.each(mediaRegistry, function (item) {
                if (item.player.constructor === playerClass) {
                    player = item.player;
                    return util.breaker;
                }
            });

            return player;

        },
        /**
         * Find the first registered matched player by the type and drm. Uses
         * {@link module:ax/device/playerRegistry.getPlayers|getPlayers}.
         * @method getPlayer
         * @param {Object} [opts]
         * @param {String} [opts.type] media type
         * @param {String} [opts.drm] drm type
         * @return {Object|Null} first registered matched player is returned.
         * @memberof module:ax/device/playerRegistry
         */
        getPlayer: function (opts) {
            var players = this.getPlayers(opts);

            if (players.length === 0) {
                return null;
            }
            return players[0];
        },
        /**
         * Find the players by the media type and drm. For list of supported
         * media types, please see device player API documentation. e.g.
         * {@link module:ax/device/workstation/DevicePackage|workstation/DevicePackage}
         *
         * @method getPlayers
         * @param {object} [opts]
         * @param {String} [opts.type] media type
         * @param {String} [opts.drm] drm type
         * @return {Array} list of available player with id as key and player object as value.
         * @memberof module:ax/device/playerRegistry
         * @exmaple playerRegistry.getPlayers({ type: "hls", drm: "AES-128"});
         *
         */
        getPlayers: function (opts) {

            var resultArr = [],
                type = opts.type,
                drm = opts.drm;

            if (!type && !drm) {
                util.each(mediaRegistry, function (item) {
                    resultArr.push(item.player);
                });
                return resultArr;
            }



            util.each(mediaRegistry, function (item) {
                if (type && item.type[type] || type === "*") {
                    if ((drm && item.drm[drm]) || !drm) {
                        resultArr.push(item.player);
                    }
                }

                if (!type && drm && item.drm[drm]) {
                    resultArr.push(item.player);
                }
            });

            return resultArr;
        }

    };
});
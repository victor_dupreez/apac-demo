/**
 * Contain definition for abstracted virtual keys in XDK.
 *
 * **Note** that device specific keys (e.g. Samsung SmartHub Key) are defined within Samsung package.
 *
 *
 * @module ax/device/vKey
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @example
 * // Handling the key event dispatched from Env
 * sEnv.addEventListener(sEnv.EVT_ONKEY, function(keyEvt) {
 *
 *      switch (keyEvt.id) {
 *
 *          // compare to see if it"s a virtual key
 *          case vKey.KEY_0.id:
 *
 *          // custom key events emitted by specific device
 *          case "vkey:samsung:smart-hub":
 *             ...
 *          break;
 *
 *      }
 * }
 */
define("ax/device/vKey", [], function () {
    "use strict";
    return {
        /**
         * @name KEY_0
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:0"
         * @property {String} text Text value of the Virtual Key. "0"
         * @memberof module:ax/device/vKey
         */
        KEY_0: {
            id: "device:vkey:0",
            text: "0"
        },
        /**
         * @name KEY_1
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:1"
         * @property {String} text Text value of the Virtual Key. "1"
         * @memberof module:ax/device/vKey
         */
        KEY_1: {
            id: "device:vkey:1",
            text: "1"
        },
        /**
         * @name KEY_2
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:2"
         * @property {String} text Text value of the Virtual Key. "2"
         * @memberof module:ax/device/vKey
         */
        KEY_2: {
            id: "device:vkey:2",
            text: "2"
        },
        /**
         * @name KEY_3
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:3"
         * @property {String} text Text value of the Virtual Key. "3"
         * @memberof module:ax/device/vKey
         */
        KEY_3: {
            id: "device:vkey:3",
            text: "3"
        },
        /**
         * @name KEY_4
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:4"
         * @property {String} text Text value of the Virtual Key. "4"
         * @memberof module:ax/device/vKey
         */
        KEY_4: {
            id: "device:vkey:4",
            text: "4"
        },
        /**
         * @name KEY_5
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:5"
         * @property {String} text Text value of the Virtual Key. "5"
         * @memberof module:ax/device/vKey
         */
        KEY_5: {
            id: "device:vkey:5",
            text: "5"
        },
        /**
         * @name KEY_6
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:6"
         * @property {String} text Text value of the Virtual Key. "6"
         * @memberof module:ax/device/vKey
         */
        KEY_6: {
            id: "device:vkey:6",
            text: "6"
        },
        /**
         * @name KEY_7
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:7"
         * @property {String} text Text value of the Virtual Key. "7"
         * @memberof module:ax/device/vKey
         */
        KEY_7: {
            id: "device:vkey:7",
            text: "7"
        },
        /**
         * @name KEY_8
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:8"
         * @property {String} text Text value of the Virtual Key. "8"
         * @memberof module:ax/device/vKey
         */
        KEY_8: {
            id: "device:vkey:8",
            text: "8"
        },
        /**
         * @name KEY_9
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:9"
         * @property {String} text Text value of the Virtual Key. "9"
         * @memberof module:ax/device/vKey
         */
        KEY_9: {
            id: "device:vkey:9",
            text: "9"
        },
        /**
         * @name OK
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:ok"
         * @memberof module:ax/device/vKey
         */
        OK: {
            id: "device:vkey:ok"
        },
        /**
         * @name UP
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:up"
         * @memberof module:ax/device/vKey
         */
        UP: {
            id: "device:vkey:up"
        },
        /**
         * @name DOWN
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:down"
         * @memberof module:ax/device/vKey
         */
        DOWN: {
            id: "device:vkey:down"
        },
        /**
         * @name LEFT
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:left"
         * @memberof module:ax/device/vKey
         */
        LEFT: {
            id: "device:vkey:left"
        },
        /**
         * @name RIGHT
         * @property {String} id Virtual Key ID. "device:vkey:right"
         * @memberof module:ax/device/vKey
         */
        RIGHT: {
            id: "device:vkey:right"
        },
        /**
         * @name EXIT
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:left"
         * @memberof module:ax/device/vKey
         */
        EXIT: {
            id: "device:vkey:exit"
        },
        /**
         * @name BACK
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:back"
         * @memberof module:ax/device/vKey
         */
        BACK: {
            id: "device:vkey:back"
        },
        /**
         * @name RED
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:green"
         * @memberof module:ax/device/vKey
         */
        RED: {
            id: "device:vkey:red"
        },
        /**
         * @name GREEN
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:green"
         * @memberof module:ax/device/vKey
         */
        GREEN: {
            id: "device:vkey:green"
        },
        /**
         * @name YELLOW
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:yellow"
         * @memberof module:ax/device/vKey
         */
        YELLOW: {
            id: "device:vkey:yellow"
        },
        /**
         * @name BLUE
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:blue"
         * @memberof module:ax/device/vKey
         */
        BLUE: {
            id: "device:vkey:blue"
        },
        /**
         * @name PLAY
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:play"
         * @property {String} text Text value of the Virtual Key. "play"
         * @memberof module:ax/device/vKey
         */
        PLAY: {
            id: "device:vkey:play"
        },
        /**
         * @name PLAY_PAUSE
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:play-pause"
         * @memberof module:ax/device/vKey
         */
        PLAY_PAUSE: {
            id: "device:vkey:playPause"
        },
        /**
         * @name PAUSE
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:pause"
         * @memberof module:ax/device/vKey
         */
        PAUSE: {
            id: "device:vkey:pause"
        },
        /**
         * @name STOP
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:stop"
         * @memberof module:ax/device/vKey
         */
        STOP: {
            id: "device:vkey:stop"
        },
        /**
         * @name FF
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:ff"
         * @memberof module:ax/device/vKey
         */
        FF: {
            id: "device:vkey:ff"
        },
        /**
         * @name RW
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:rw"
         * @memberof module:ax/device/vKey
         */
        RW: {
            id: "device:vkey:rw"
        },
        /**
         * @name NEXT
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:next"
         * @memberof module:ax/device/vKey
         */
        NEXT: {
            id: "device:vkey:next"
        },
        /**
         * @name PREV
         * @type {object}
         * @property {String} id Virtual Key ID. "device:vkey:prev"
         * @memberof module:ax/device/vKey
         */
        PREV: {
            id: "device:vkey:prev"
        }

    };
});
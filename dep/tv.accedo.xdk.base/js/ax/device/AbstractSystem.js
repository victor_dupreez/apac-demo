/**
 * System class to handle the system related functionality.
 *
 * System also provide a internet status checking fallback handling to all platforms. Using device.internet-poll.url config.
 *
 * ###Configuration Parameters
 *
 *  Attribute | Value
 * --------- | ---------
 * Key      | device.ajax-network-check.url
 * Type     | String
 * Desc     | The url to check internet status by ajax call.  Null disables internet checking
 * Default  | null
 * Usage    | {@link module:ax/config}
 *  --------------------------
 *  Attribute | Value
 * --------- | ---------
 * Key      | device.ajax-network-check.timeout
 * Type     | Number
 * Desc     | Connection timeout in second
 * Default  | 10
 * Usage    | {@link module:ax/config}
 *  -----------------------------
 * Attribute | Value
 * --------- | ---------
 * Key      | device.ajax-network-check.consec-failure-threshold
 * Type     | Number
 * Desc     |  How many polling failures to consider the internet is disconnected.
 * Default  | 3
 * Usage    | {@link module:ax/config}
 * --------------------------
 * Attribute | Value
 * --------- | ---------
 * Key      | device.ajax-network-check.interval
 * Type     | Number
 * Desc     | Polling interval in second
 * Default  |  15
 * Usage    | {@link module:ax/config}
 *
 * @class ax/device/AbstractSystem
 * @augments ax/device/interface/System
 * @fires ax/device/interface/System.EVT_NETWORK_STATUS_CHANGED
 *
 */
define("ax/device/AbstractSystem", ["ax/class", "ax/util", "ax/config", "ax/device/shared/ConnectionPoller", "ax/console", "ax/core", "ax/ajax", "ax/device/interface/System", "ax/EventDispatcher", "ax/promise"], function(klass, util, config, ConnectionPoller, console, core, ajax, ISystem, EventDispatcher, promise) {
    "use strict";
    return klass.createAbstract(EventDispatcher, [ISystem], {}, {
        /**
         * The internet connection poller used in the abstract system
         * @private
         * @name _internetPoller
         * @type {ax/device/shared/ConnectionPoller}
         * @memberof ax/device/AbstractSystem#
         */
        _internetPoller: null,
        /**
         * The current network status
         * @private
         * @name __networkStatus
         * @type {Boolean}
         * @memberof ax/device/AbstractSystem#
         */
        __networkStatus: true,
        init: function() {
            this._super();
            this._postInit();
        },
        /**
         * Setting the screen saver On/Off
         * @name setScreenSaver
         * @method
         * @param {Boolean} flag True to turn on and off to turn off.
         * @return {Boolean}  Return true if success or no state change. Return false if system API not available.
         * @memberof ax/device/AbstractSystem#
         */
        /*jshint unused:false */
        setScreenSaver: function(flag) {
            return false;
        },
        /*jshint unused:true */
        /**
         * Setting the system to mute/unmute.
         * @name setSystemMute
         * @method
         * @param {Boolean} flag True to mute and false to unmute
         * @return {Boolean}  Return true if
         * success or no state change. Return false if system API not available.
         * @memberof ax/device/AbstractSystem#
         */
        /*jshint unused:false */
        setSystemMute: function(flag) {
            return false;
        },
        /*jshint unused:true */
        /**
         * Power off the system
         * @name powerOff
         * @method
         * @return {Boolean} Return false if system API not available.
         * @memberof ax/device/AbstractSystem#
         */
        powerOff: function() {
            return false;
        },
        /**
         * Exit the application
         * @name exit
         * @method
         * @param {Object} [obj] To set the information when exit
         * @param {Boolean} [obj.toTV] True when exit back to TV source, false will go back to smartHub/App Store page.Default is true
         * @return {Boolean} Return false if system API not available.
         * @memberof ax/device/AbstractSystem#
         */
        exit: function() {
            return false;
        },
        /**
         * Determine whether device support mouse
         * @name hasMouse
         * @method
         * @return {Boolean} Return true if the platform has pointer devices/emulation support.Return false if not available.
         * @memberof ax/device/AbstractSystem#
         */
        hasMouse: function() {
            return false;
        },
        /**
         * Determine whether device support hardware keyboard
         * @name hasFixedKeyboard
         * @method
         * @return {Boolean} Return true if the platform has hardware keyboard support..Return false if not available.
         * @memberof ax/device/AbstractSystem#
         */
        hasFixedKeyboard: function() {
            return false;
        },
        /**
         * @typedef Resolution
         * @type {Object}
         * @property {Number} width in pixels
         * @property {Number} height in pixels
         * @memberof ax/device/AbstractSystem
         */
        /**
         * Get the getDisplayResolution
         * @name getDisplayResolution
         * @method
         * @return {ax/device/AbstractSystem.Resolution} Return the display resolution from of the platform.Return {width:0,height:0} if not available.
         * @memberof ax/device/AbstractSystem#
         */
        getDisplayResolution: function() {
            return {
                width: 0,
                height: 0
            };
        },
        /**
         * Whether SSL is supported in the device
         * @name suportSSL
         * @method
         * @return {Boolean} Return true if the platform support SSL certificate verification.
         * @memberof ax/device/AbstractSystem#
         */
        supportSSL: function() {
            return false;
        },

        /**
         * Whether same-origin policy exists in the device (i.e. cross domain ajax is not allowed). Modern browsers should have this policy.
         * @name hasSOP
         * @method
         * @return {Boolean} Return true if cross domain ajax is not allowed natively. Proxy / Allow-Origin header setup may be needed.
         * @memberof ax/device/AbstractSystem#
         */
        hasSOP: function() {
            return true;
        },
        /**
         * Get the current network status. If there is connection poller which is polling, it will get the status from connection poller.
         * Otherwise, it will try to do a ajax call to determine the current status if there is internet check url.
         * If none of the above case, it will reject the promise (is connected to network)
         * @method getNetworkStatus
         * @return {Promise.<Boolean>}  Return true if it is connected to internet.
         * @throws {Promise.<Undefined>}  Reject promise if no method to determine the network status.
         * @public
         * @memberof ax/device/AbstractSystem#
         */
        getNetworkStatus: function() {
            var internetCheckUrl;

            //if there is internet poller and polling then no need to use an ajax to test. Directly obtain the result from the connection poller.
            if (this._internetPoller && this._internetPoller.isPolling()) {
                return promise.resolve(this._internetPoller.isConnected());
            }

            //do the ajax call to examine the network connection when not polling
            internetCheckUrl = config.get("device.ajax-network-check.url", null);

            if (internetCheckUrl) {

                return ajax.request(internetCheckUrl, {
                    timeOut: config.get("device.ajax-network-check.timeout", 10)
                }).then(
                    function() {
                        return true;
                    }, function() {
                        return false;
                    }
                );
            }

            return promise.reject();
        },
        /**
         * set the current network status checking using the ajax network checking method
         * @method _postInit
         * @protected
         * @memberof ax/device/AbstractSystem#
         */
        _postInit: function() {
            var internetCheckUrl = config.get("device.ajax-network-check.url", null),
                checkStatus, onStatusChange;

            if (!internetCheckUrl) {
                return;
            }

            //the checking method
            checkStatus = util.bind(function(__checkStatusDoneCb) {

                if (!internetCheckUrl) {
                    throw core.createException("PollingUrlNotSet", "You must set polling URL using setPollingUrl().");
                }
                ajax.request(internetCheckUrl, {
                    timeOut: config.get("device.ajax-network-check.timeout", 10)
                }).then(
                    function() {
                        __checkStatusDoneCb(true);
                    }, function() {
                        __checkStatusDoneCb(false);
                    }
                ).done();
            }, this);

            //the onStatusChange
            onStatusChange = util.bind(function(status) {
                this.dispatchEvent(ISystem.EVT_NETWORK_STATUS_CHANGED, status);
            }, this);

            //start the poller, set check method and onStatusChange
            this._internetPoller = new ConnectionPoller(checkStatus, onStatusChange, {
                consecFailureThreshold: config.get("device.ajax-network-check.consec-failure-threshold", 3),
                pollingInterval: config.get("device.ajax-network-check.interval", 15),
                pollingTimeout: config.get("device.ajax-network-check.timeout", 10)
            });

        },
        /**
         * Overrides parent method to start the polling on add event listener
         * @method addEventListener
         * @param {ax/af/evt/type|String} type event type
         * @param {Function} handler event listener function
         * @param {Boolean} [once] set to true if listener will be called only once
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @public
         * @memberof ax/device/AbstractSystem#
         */
        addEventListener: function(type, handler, once) {
            var ret = this._super(type, handler, once);

            if (this._internetPoller && type === ISystem.EVT_NETWORK_STATUS_CHANGED) {
                //start the polling when adding the event listener
                this._internetPoller.startPolling();
            }

            return ret;
        },
        /**
         * Overrides parent method to remove the polling on event listener remove
         * @method removeEventListener
         * @param {ax/af/evt/type|String} type Event type
         * @param {Function} [handler] event listener function.  If not provided, all listeners will be removed for the specific type.
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @public
         * @memberof ax/device/AbstractSystem#
         */
        removeEventListener: function(type, handler) {
            if (this._internetPoller && type === ISystem.EVT_NETWORK_STATUS_CHANGED) {
                //stop the polling when remove the event listener
                this._internetPoller.stopPolling();
            }

            return this._super(type, handler);
        },
        /**
         * Dummy function to redraw the element
         * @param {HTMLElement} [element=document.body] the target element. Default will be document.body
         * @public
         * @memberof ax/device/AbstractSystem#
         */
        /*jshint unused:false*/
        redraw:function(element){
            //dummy function for those with no redraw
        }
    });
});
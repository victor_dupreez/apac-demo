/**
 * Id class to identification information of a device.
 * @class ax/device/AbstractId
 * @augments ax/device/interface/Id
 */
define("ax/device/AbstractId", [
    "ax/class",
    "ax/device/interface/Id",
    "require",
    "ax/device/helper/storageUniqueId"
], function (
    klass,
    IId,
    require,
    storageUniqueId
) {
    "use strict";
    return klass.createAbstract([IId], {}, {
        /**
         * Get the MAC address.
         * @name getMac
         * @function
         * @return {String} mac address. null if not available.
         * @memberof ax/device/AbstractId#
         * @public
         */
        getMac: function () {
            return null;
        },
        /**
         * Get the system's firmware version.
         * @name getFirmware
         * @function
         * @return {String} firmware version."dummyFirmware" if not available.
         * @memberof ax/device/AbstractId#
         * @public
         */
        getFirmware: function () {
            return "dummyFirmware";
        },
        /**
         * Get the year from the system's firmware version.
         * @name getFirmwareYear
         * @function
         * @memberof ax/device/AbstractId#
         * @return {Number} firmware year Return 0 if not available.
         * @public
         */
        getFirmwareYear: function () {
            return 0;
        },
        /**
         * Get a uniqueID of the device. UUID is gernerated and stored in localStorage to pretent a
         * unique if device API is not available.
         * @name getUniqueID
         * @function
         * @return {String} unique ID
         * @memberof ax/device/AbstractId#
         * @public
         */
        getUniqueID: function () {
            return storageUniqueId.getUniqueID();
        },
        /**
         * Get device's model number.
         * @name getModel
         * @function
         * @return {String} "dummyModel" if not available.
         * @memberof ax/device/AbstractId#
         * @public
         */
        getModel: function () {
            return "dummyModel";
        },
        /**
         * Get device's internal IP.If there are no related api, it will return 0.0.0.0 and
         * developer may need to send ajax to public api service to get the ip.
         * @name getIP
         * @function
         * @memberof ax/device/AbstractId#
         * @return {String} ip address."0.0.0.0" if it's not available.
         * @public
         */
        getIP: function () {
            return "0.0.0.0";
        }
    });
});
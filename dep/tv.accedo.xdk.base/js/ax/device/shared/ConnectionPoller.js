/**
 * To provide the polling for the connection checking like the Lan status or internet status
 * @name ConnectionPoller
 * @class ax/device/shared/ConnectionPoller
 * @fires ax/device/shared/ConnectionPoller.EVT_STATUS_CHANGED
 * @param {CheckNetworkStatus} checkStatus The function to check the status about the connection
 * @param {OnNetworkStatusChange} onStatusChange The callback function to call when the network status has changed
 * @param {Object} [opts] optional parameter
 * @param {Boolean} [opts.networkStatus=true] False if it is original disconnected from network. So it will dispatch the status change when there is difference from the initial or previous status.
 * @augments EventDispatcher
 * @example
 * new ConnectionPoller(function(checkStatusCB){
 *     checkStatusCB(deviceAPI.isConnected);  //where deviceAPI.isConnected (Boolean) may be the device api to get the current network information.
 * }, function(newStatus){
 *    //onStatusChange receive the event of new Status then system will dispatch the event.
 *    system.dispatchEvent(ISystem.EVT_NETWORK_STATUS_CHANGED, newStatus);
 * }, {
 *     consecFailureThreshold: 3,
 *     pollingInterval:  15,
 *     pollingTimeout: 10
 * });
 */
/**
 * A function to check the network status.
 * This function accepts a callback and will execute it with the latest network status once the check is complete.
 *
 * @callback CheckNetworkStatus
 * @param {OnNetworkCheckDone} onStatusChange The status check completion callback
 */
/**
 * A callback that passed into {@link CheckNetworkStatus}.
 * This will be called with the latest network status once the check is complete.
 *
 * @callback OnNetworkCheckDone
 * @param {Boolean} status The network status. True means connected, false means disconnected.
 */
/**
 * A callback that being called when the network status has changed.
 *
 * @callback OnNetworkStatusChange
 * @param {Boolean} status The new network status. True means connected, false means disconnected.
 */
define("ax/device/shared/ConnectionPoller", [
    "ax/core",
    "ax/class",
    "ax/util",
    "ax/console",
    "ax/config",
    "ax/exception"
], function (
    core,
    klass,
    util,
    console,
    config,
    exception
) {
    "use strict";

    var ConnectionPoller = klass.create({}, {
        /**
         * The timer to save the interval
         * @private
         * @name __timer
         * @memberof ax/device/shared/ConnectionPoller#
         */
        __timer: null,
        /**
         * The interval to pull the data
         * @private
         * @name __pollingInterval
         * @memberof ax/device/shared/ConnectionPoller#
         */
        __pollingInterval: 15,
        /**
         * The timeout for polling
         * @private
         * @name __pollingTimeout
         * @memberof ax/device/shared/ConnectionPoller#
         */
        __pollingTimeout: 10,
        /**
         * Storing the current connection status. Assumed true at the beginning by default
         * @private
         * @name __currentStatus
         * @memberof ax/device/shared/ConnectionPoller#
         */
        __currentStatus: true,
        /**
         * Consecutive polling failures
         * @private
         * @name __consecFailure
         * @memberof ax/device/shared/ConnectionPoller#
         */
        __consecFailure: 0,
        /**
         * Number of consecutive polling failures required to consider the connection is disconnected
         * @protected
         * @member
         * @name __consecFailureThreshold
         * @memberof ax/device/shared/ConnectionPoller#
         */
        __consecFailureThreshold: 3,
        /**
         * The check status function when checking the current network status
         * @protected
         * @method
         * @name _checkStatus
         * @param {Function} checkStatusCB the callback function when check status change. Run checkStatusCHangeCB(true) if changed into network connected.
         * @memberof ax/device/shared/ConnectionPoller#
         */
        _checkStatus: null,
        init: function(checkStatus, onStatusChange, opts) {
            if (!util.isFunction(checkStatus) || !util.isFunction(onStatusChange)) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "Check Status or onStatusChange function is not defined properly.");
            }

            this._checkStatus = checkStatus;
            this._onStatusChange = onStatusChange;
            opts = opts || {};

            // assume network status at the beginning is true for default
            this.__currentStatus = util.isBoolean(opts.networkStatus) ? opts.networkStatus : true;

            this.__consecFailureThreshold = util.isNumber(opts.consecFailureThreshold) ? opts.consecFailureThreshold : this.__consecFailureThreshold;
            this.__pollingInterval = util.isNumber(opts.pollingInterval) ? opts.pollingInterval : this.__pollingInterval;
            this.__pollingTimeout = util.isNumber(opts.pollingTimeout) ? opts.pollingTimeout : this.__pollingTimeout;
        },
        /**
         * Get the lastest connection status from polling
         * @method isConnected
         * @public
         * @return {Boolean} internet connection status
         * @memberof ax/device/shared/ConnectionPoller#
         */
        isConnected: function() {
            return this.__currentStatus;
        },
        /**
         * Start the internet polling
         * @method startPolling
         * @public
         * @memberof ax/device/shared/ConnectionPoller#
         */
        startPolling: function () {
            // start polling
            this.__timer = core.getGuid() + "-ConnectionPoller-polling";
            util.delay(this.__pollingInterval, this.__timer).then(util.bind(this.__poll, this), function () {}).done();
        },
        /**
         * get the current status of the connection poller whether it is pooling
         * @method isPolling
         * @public
         * @memberof module:ax/device/shared/InternetPoller#
         */
        isPolling: function() {
            return this.__timer !== null;
        },
        /**
         * Handle the polling cycle
         * @method __poll
         * @private
         * @memberof ax/device/shared/ConnectionPoller#
         */
        __poll: function () {
            var __checkStatusDoneCb = util.bind(function (__currentStatus, newStatus) {
                // no status change, poll again later
                if (__currentStatus === newStatus) {
                    this.startPolling();
                    return;
                }

                // success or fail count >= limit, confirm the status change
                if (newStatus || (!newStatus && this.__consecFailure >= this.__consecFailureThreshold - 1)) {
                    this.__currentStatus = newStatus;
                    this._onStatusChange(this.__currentStatus);
                    this.__consecFailure = 0;

                    this.startPolling();
                    return;
                }

                // failed, check again immediately
                this.__consecFailure++;
                this._checkStatus(__checkStatusDoneCb);
            }, this, this.__currentStatus);

            this._checkStatus(__checkStatusDoneCb);
        },
        /**
         * To stop the polling
         * @method stopPolling
         * @public
         * @memberof ax/device/shared/ConnectionPoller#
         */
        stopPolling: function () {
            util.clearDelay(this.__timer);
            this.__timer = null;
            this.__consecFailure = 0;
        },
        /**
         * Set the polling interval, default value is 20 if not set
         * @method setPollingInterval
         * @public
         * @param {Number} interval polling interval in seconds
         * @memberof ax/device/shared/ConnectionPoller#
         */
        setPollingInterval: function(interval) {
            this.__pollingInterval = interval;
        },
        /**
         * Get the polling interval
         * @method getPollingInterval
         * @public
         * @return {Number} polling interval in seconds
         * @memberof ax/device/shared/ConnectionPoller#
         */
        getPollingInterval: function() {
            return this.__pollingInterval;
        }
    });

    return ConnectionPoller;
});
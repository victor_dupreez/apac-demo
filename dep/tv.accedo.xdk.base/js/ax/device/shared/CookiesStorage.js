/**
 * To provide storage interface using Cookies
 * @class ax/device/shared/CookiesStorage
 * @augments ax/device/AbstractStorage
 */
define("ax/device/shared/CookiesStorage", ["ax/class", "ax/device/AbstractStorage", "ax/util", "ax/console"], function (klass, AbstractStorage, util, console) {
    "use strict"; 
    var CookiesStorage = klass.create(AbstractStorage, {}, {
        /**
         * To temporary store the cookies instead of getting and setting each tiem
         * @private
         * @name __tempCookies
         * @memberof ax/device/shared/CookiesStorage#
         */
        __tempCookies: null,
        /**
         * to set the variable
         * @function
         * @param {String} k the key to store the value to
         * @param {mixed} v the value to store
         * @memberof ax/device/shared/CookiesStorage#
         */
        set: function (k, v) {
            console.log("using cookies storage");
            //to load the cookies when the cookies is not loaded yet
            if (!this.__tempCookies) {
                this.__getCookies();
            }

            this.__tempCookies[k] = v;
            this.__setCookies();
        },
        /**
         * to get the variable
         * @function
         * @param {String} k the key to store the value to
         * @returns {mixed} v the retrieved value.Return false if key doesn't exist in storage.
         * @memberof ax/device/shared/CookiesStorage#
         */
        get: function (k) {
            //to load the cookies when the cookies is not loaded yet
            if (!this.__tempCookies) {
                this.__getCookies();
            }

            var value;

            if (this.__tempCookies && this.__tempCookies[k]) {
                value = this.__tempCookies[k];

                if (!util.isUndefined(value)) {
                    return value;
                }
            }

            return null;
        },
        /**
         * to unset the variable
         * @function
         * @param {String} k the key to store the value to
         * @memberof ax/device/shared/CookiesStorage#
         */
        unset: function (k) {
            //to load the cookies when the cookies is not loaded yet
            if (!this.__tempCookies) {
                this.__getCookies();
            }

            if (this.__tempCookies && !util.isUndefined(this.__tempCookies[k])) {
                this.__tempCookies[k] = null;
                delete this.__tempCookies[k];
                this.__setCookies();
            }
        },
        /**
         * To clear the local storage
         * @protected
         * @method
         * @name _doClear
         * @memberof ax/device/shared/CookiesStorage#
         */
        _doClear: function () {
            this.__tempCookies = {};
            this.__setCookies();
        },
        /**
         * to get the cookies from the device (which should be get at the first time), other cases will get from the __tempCookies
         * @private
         * @name clear
         * @memberof ax/device/shared/CookiesStorage#
         */
        __getCookies: function () {
            var cookies, cookieString, i, l;
            if (!util.isString(document.cookie)) {
                console.warn("document.cookie doesn't exist and unable to get the cookies");
                return;
            }

            if (!document.cookie) {
                //empty cookies
                this.__tempCookies = {};
                return;
            }

            cookies = document.cookie.split(";");
            for (i = 0, l = cookies.length; i < l; i++) {
                cookieString = util.strip(cookies[i]);
                if (util.startsWith(cookieString, this._uniquePrefix + "=")) {
                    cookieString = cookieString.substring((this._uniquePrefix + "=").length);
                    if (cookieString) {
                        try {
                            this.__tempCookies = util.parse(decodeURIComponent(cookieString));
                            return;
                        } catch (e) {
                            console.warn("unable to load the cookies");
                        }
                    }

                }
            }
            this.__tempCookies = {};
        },
        /**
         * to set the cookies
         * @private
         * @name __setCookies
         * @memberof ax/device/shared/CookiesStorage#
         */
        __setCookies: function () {
            if (!this.__tempCookies) {
                console.warn("cookie Object is undefined and unable to set into the cookies");
                return false;
            }

            var days = 365,
                date, expires, cookieString;
            if (!util.isEmpty(this.__tempCookies)) {
                date = (new Date());
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "; expires= -1";
            }

            cookieString = this._uniquePrefix + "=" + encodeURIComponent(util.stringify(this.__tempCookies)) + expires + "; domain=" + location.hostname + "; path=/";
            document.cookie = cookieString;
        }
    });
    return CookiesStorage;
});
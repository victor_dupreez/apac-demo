/**
 * Html5PlayerPreloadedPlayback is an implementation of PreloadedPlayback.
 * This class encapsulates the information and procedures for playing the preloaded playback.
 *
 * @class ax/device/shared/Html5PlayerPreloadedPlayback
 * @memberof ax/device/shared/Html5Player
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define("ax/device/shared/Html5PlayerPreloadedPlayback", [
    "ax/class",
    "ax/device/interface/Preloadable"
], function (
    klass,
    Preloadable
) {
    "use strict";

    return klass.create([Preloadable.PreloadedPlayback], {}, {
        /**
         * Constructs a new Html5PlayerPreloadedPlayback.
         * The Media instance is needed such that circular dependency can be avoided.
         *
         * @method
         * @param {ax/device/shared/MultiObjectHtml5Player} player The player instance
         * @param {Video} video The HTML5 video tag object
         * @param {String} url The media url
         * @param {Object} [opts] The media loading options
         * @memberof ax/device/shared/FallbackSuspendedPlayback#
         */
        init: function (player, video, url, opts) {
            this.__player = player;
            this.__video = video;
            this.__url = url;
            this.__opts = opts || {};
        },

        /**
         * Plays the preloaded playback.
         *
         * @method
         * @param {Number} pos The position to start playback
         * @memberof ax/device/shared/Html5PlayerPreloadedPlayback#
         */
        play: function (pos) {
            this.__player._swapPlayerObject(this.__video.id);
            this.__player.play({
                sec: pos
            });
        },

        /**
         * Disposes the suspended playback, destroying the playback states before suspension.
         * A disposed playback cannot be restored anymore.
         *
         * @method
         * @memberof ax/device/shared/Html5PlayerPreloadedPlayback#
         */
        dispose: function () {
            this.__player._disposePreloaded(this);
            this.__video = null;
            this.__player = null;
        },

        /**
         * Returns the media information.
         *
         * @method
         * @returns {MediaMeta} The media metadata
         * @memberof ax/device/shared/Html5PlayerPreloadedPlayback#
         */
        getMediaMeta: function () {
            return {
                url: this.__url,
                opts: this.__opts
            };
        },

        /**
         * Returns the player instance who is responsible for restoration.
         *
         * @method
         * @returns {ax/device/interface/Player} The player instance
         * @memberof ax/device/shared/Html5PlayerPreloadedPlayback#
         */
        getPlayer: function () {
            return this.__player;
        },
        /**
         * Returns the playerObject
         * @protected
         * @method
         * @returns {HTMLElement} The playerObject
         * @memberof ax/device/shared/Html5PlayerPreloadedPlayback#
         */
        getPlayerObject: function () {
            return this.__video;
        }
    });
});
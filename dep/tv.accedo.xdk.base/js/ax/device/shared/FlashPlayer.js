/**
 * Flash media player. Able to play RTMP streams.
 * @augments ax/device/interface/Player
 * @class ax/device/shared/FlashPlayer
 */
define("ax/device/shared/FlashPlayer", ["ax/core", "ax/class", "ax/device/AbstractPlayer", "ax/device/interface/Player", "ax/device/Media",
    "ax/util", "ax/console", "require"
], function (core, klass, AbstractPlayer, IPlayer, Media, util, console, require) {
    "use strict";
    var sMedia = Media.singleton(),
        resolution = null,
        getResolution = function () {
            if (resolution) {
                return resolution;
            }
            resolution = require("ax/device")
                .system.getDisplayResolution();
            return resolution;
        };

    var PLAYBACK_ERRORS = IPlayer.PLAYBACK_ERRORS;

    return klass.create(AbstractPlayer, {}, {
        _curTime: 0,
        /**
         * to see the html 5 player is onLoaded or not. If it is onloaded, user can seek time
         * @protected
         * @constant
         * @memberof ax/device/shared/FlashPlayer#
         */
        _onLoaded: false,
        /**
         * container name of the player
         * @protected
         * @constant
         * @memberof ax/device/shared/FlashPlayer#
         */
        _CONTAINER_NAME: "FlashPlayerContainer",
        /**
         * parent node of the player object
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _parentNode: null,
        /**
         * the player object
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _playerObject: null,
        /**
         * the init status of the player, if it isn't true, the event Listener or player may not been attached to DOM
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _prepared: false,
        /**
         * total time of the video
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _duration: 0,
        /**
         * connection time out (in sec) when the video is fail to play
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _connectionTimeLimit: 90,
        // in sec
        /**
         * store the timeout of the connection time out
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _connectionTimeOut: null,
        /**
         * to check if it is connected
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _connected: false,
        /**
         * to check if it is paused
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _pausedVideo: false,
        /**
         * if the video format HLS is supported
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _supportHLS: false,
        /**
         * to check if the video format is supported
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _support: true,
        /**
         * to check if it needs to seek before play
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _seek: false,
        /**
         * to store the time which needed to be seek
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _seekTime: 0,
        /**
         * to store the last loaded media URL
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _url: null,
        init: function (devicePackage) {
            //Flash player doesn't support in 2011 samsung(limitation:like unable to change the screen size and unable to dynamic create the object) & 2010
            if (devicePackage.getId() === "samsung" && devicePackage.getInterface(devicePackage.constructor.ID)
                .getFirmwareYear() < 2012) {
                throw core.createException("FlashPlayerNotSupported",
                    "Flash Player is not supported when less than 2012.");
            }
        },
        /**
         * Prepare the player, should be called before using
         * @name prepare
         * @method
         * @param {object} opts The opts parameter passed from {@link ax/device/Media#load|device.media.load}
         * @param {HTMLElement} [opts.parentNode] parentNode to be append the player object.
         * @public
         * @memberof ax/device/shared/FlashPlayer#
         */
        prepare: function (opts) {

            if (!opts.swfUrl) {
                console.warn("Unable to find the url of the flash");
                return false;
            }

            //check if prepared and check parentNode
            if (this._prepared) {
                return;
            }

            //default parent node should be the body
            this._parentNode = window.document.getElementsByTagName("body")[0];

            //create div container tag
            this._playerContainer = document.createElement("div");
            this._playerContainer.id = this._CONTAINER_NAME;
            this._playerContainer.className = "playerContainer";

            //create the player object
            try {
                var paramUrl, paramQuality, paramwmode, paramBgColor;
                this._playerObject = document.createElement("object");
                this._playerObject.setAttribute("id", "FlashPlayer");
                this._playerObject.className = "playerObject";
                this._playerObject.setAttribute("type", "application/x-shockwave-flash");

                paramUrl = document.createElement("param");
                paramUrl.name = "movie";
                paramUrl.value = opts.swfUrl;
                this._playerObject.appendChild(paramUrl);

                paramQuality = document.createElement("param");
                paramQuality.name = "quality";
                paramQuality.value = "high";
                this._playerObject.appendChild(paramQuality);

                paramwmode = document.createElement("param");
                paramwmode.name = "wmode";
                paramwmode.value = "transparent";
                this._playerObject.appendChild(paramwmode);

                paramBgColor = document.createElement("param");
                paramBgColor.name = "bgcolor";
                paramBgColor.value = "000000";
                this._playerObject.appendChild(paramBgColor);

                this._playerContainer.appendChild(this._playerObject);

                this._parentNode.appendChild(this._playerContainer);
                window.__flashCallback = util.bind(this.callback, this);

                this._inited = true;

            } catch (e) {
                console.error("VideoPlayer error: while creating the player object: " + e);
                return false;
            }
            this._prepared = true;

        },
        /**
         * resets the video player, to non-playing mode
         * @method
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        reset: function () {
            if (!this._prepared) {
                return;
            }
            this.hide();
            this._stopPlayback();
            this.deinit();
        },
        /**
         * deinit the video player and remove the eventListener
         * @method
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        deinit: function () {
            if (!this._prepared) {
                return;
            }
            this._prepared = false;

            if (this._playerContainer) {
                this._playerContainer.removeChild(this._playerObject);
                this._parentNode.removeChild(this._playerContainer);
                this._playerContainer = null;
            } else {
                this._parentNode.removeChild(this._playerObject);
            }

            delete window.__flashCallback;
            console.info("Flash video player deinit successfull.");
            return;
        },
        /**
         * play the video item
         * @method
         * @param {Object} opts object containing the required options
         * @param {Number} opts.sec Play the video at the specified second
         * @public
         * @memberof ax/device/shared/FlashPlayer#
         */
        play: function (opts) {
            console.info("[XKD flash] Play requested.");

            if (!this._support) {
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.RENDER.UNSUPPORTED, "unsupport format in the flash video player");
                return;
            }

            //if stop the video and press play, it will play the previous video
            if (sMedia.isState(sMedia.STOPPED) && this._url && this._playerObject.loadUrl) {
                if (this._urlOpts.type === "rtmp") {
                    //to filter the useless attribute when passing into flash
                    var flashOpt = {};
                    if (this._urlOpts.streamName) {
                        flashOpt.streamName = this._urlOpts.streamName;
                    }
                    if (this._urlOpts.liveStreaming) {
                        flashOpt.liveStreaming = this._urlOpts.liveStreaming;
                        this._noProgressive = true;
                    } else {
                        this._noProgressive = false;
                    }
                    this._playerObject.loadUrl(this._url, flashOpt);
                } else if (this._urlOpts.type === "mp4") {
                    this._playerObject.loadUrl(null, {
                        streamName: this._url
                    });
                }

            }

            this.show();

            if (!this._connected) {
                this._connected = true;
                sMedia._onConnecting();
                try {
                    this._playerObject.play();
                } catch (ex) {
                    console.warn(
                        "unable to play the video.Flash hasn't been initialized correctly. Please check the url"
                    );
                }
                this._removeConnectionTimeOut();
                this._connectionTimeOut = core.getGuid();
                util.delay(this._connectionTimeLimit, this._connectionTimeOut)
                    .then(util.bind(this._onConnectionTimeout, this))
                    .done();
            }

            this._pausedVideo = false;
            this._seek = false;

            if (opts && opts.sec) {
                this._seek = true;
                this._seekTime = opts.sec;
            }

            if (sMedia.isState(sMedia.PAUSED)) {
                if (this.__withholdSeek) {
                    this.__withholdSeek = false;
                    this.seek(this.__withholdSeekTime);
                }
                this._playerObject.play();
                sMedia._onPlaying();
            }
        },
        /**
         * pause the video item
         * @method
         * @public
         * @memberof ax/device/shared/FlashPlayer#
         */
        pause: function () {
            console.info("[XDK flash] Pausing video.");
            this._pausedVideo = true;
            this._playerObject.pause();
            sMedia._onPause();
        },
        /**
         * stop the video playback
         * @method
         * @public
         * @memberof ax/device/shared/FlashPlayer#
         */
        stop: function () {
            console.info("[XDK flash] Stopping video");

            this._stopPlayback();

            sMedia._onStopped();
        },
        /**
         * stop the video playback for real
         * @method
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _stopPlayback: function () {
            this.__withholdSeek = false;
            this.__withholdSeekTime = 0;
            if (this._playerObject && this._playerObject.pause) {
                this._playerObject.pause();
            }
            this.hide();
            this._removeConnectionTimeOut();
            this._connected = false;
            this._onLoaded = false;
        },
        /**
         * resume playing the video
         * @method
         * @public
         * @memberof ax/device/shared/FlashPlayer#
         */
        resume: function () {
            console.info("[XDK flash] Resuming...");

            //perform the seek when resume from pause
            if (sMedia.isState(sMedia.PAUSED) && this.__withholdSeek) {
                this.__withholdSeek = false;
                this.seek(this.__withholdSeekTime);
            }

            this._playerObject.play();
            this._pausedVideo = false;
            sMedia._onPlaying();
        },
        /**
         * Skip the playback forward/backward for certain seconds
         * @method
         * @param {Number} sec number of seconds to skip (10 by default)
         * @public
         * @memberof ax/device/shared/FlashPlayer#
         */
        skip: function (sec) {
            //if it is paused and then press skip, it will save and do it after resume.
            if (sMedia.isState(sMedia.PAUSED) || sMedia._prevState === sMedia.PAUSED) {
                this.__withholdSeek = true;
                if (this.getCurTime() + sec > this.getDuration() && this.getCurTime() + sec >= 0) {
                    this.__withholdSeekTime = this.getDuration();
                    //update the player time
                }
                this.__withholdSeekTime = this.getCurTime() + sec;
                this._onTimeUpdate(this.__withholdSeekTime);
                return;
            }
            this._playerObject.seek(this.getCurTime() + sec);
        },
        /**
         * Seek to specifiy position of the video
         * @method
         * @param {Number} sec the position to seek to in seconds
         * @memberof ax/device/shared/FlashPlayer#
         */
        seek: function (sec) {
            if (sec < 0) {
                return false;
            }

            if (this._inited && this._url && sec >= 0) {
                //since it is invalid time in some video to set the duration and then make it to be duration -1.
                if (sec === this._duration) {
                    sec = Math.floor(this.getDuration() - 1);
                }
                this._playerObject.seek(sec);
                sMedia._onTimeUpdate(sec);
                return true;
            }
        },
        /**
         * Get the current playback time
         * @method
         * @return {Number} the current playback time
         * @memberof ax/device/shared/FlashPlayer#
         * @public
         */
        getCurTime: function () {
            return this._curTime;
        },
        /**
         * to set the total time of the player
         * @method
         * @memberof ax/device/shared/FlashPlayer#
         * @proteced
         */
        _setDuration: function (time) {
            this._duration = time;
        },
        /**
         * to get the total time of the video
         * @method
         * @return {number} the total time of the video
         * @memberof ax/device/shared/FlashPlayer#
         * @public
         */
        getDuration: function () {
            return this._duration;
        },
        /**
         * Show the player
         * @method
         * @public
         * @memberof ax/device/shared/FlashPlayer#
         */
        show: function () {
            if (this._playerObject) {
                this._playerObject.style.visibility = "visible";
            }
        },
        /**
         * Hide the player
         * @method
         * @public
         * @memberof ax/device/shared/FlashPlayer#
         */
        hide: function () {
            if (this._playerObject) {
                this._playerObject.style.visibility = "hidden";
            }
        },
        /**
         * Gets the player's capabilities
         * Removed MP4 supported from flash player
         * @method
         * @return {ax/device/interface/Player~PlayerCapabilites}
         * @memberof ax/device/shared/FlashPlayer#
         */
        getCapabilities: function () {
            var cap = {
                type: ["rtmp"]
            };
            return cap;
        },
        /**
         * Loads the specified media
         * @method
         * @param {String} mediaUrl the URL address of the media
         * @param {Object} [opts] the options for loading this media
         * @param {String} [opts.swfUrl] (Flash) set the flash swf url
         * @param {String} [opts.streamName] (Flash) the streamName
         * @param {Boolean} [opts.liveStreaming] (Flash) to  indicate if it is liveStreaming. If it is live streaming, no time update
         * @memberof ax/device/shared/FlashPlayer#
         */
        load: function (mediaUrl, opts) {
            this._playerOpts = opts;
            if (!this._prepared) {
                return false;
            }

            if (this._playerObject) {
                //move to the correct place
                if (opts.parentNode) {
                    if (this._playerContainer) {
                        this._parentNode.removeChild(this._playerContainer);
                        this._playerContainer = null;
                    }
                    this._parentNode = opts.parentNode;
                    this._parentNode.appendChild(this._playerObject);
                }
                this._url = mediaUrl;
                this._urlOpts = opts;
            }
            return mediaUrl;
        },
        /**
         * Sets video window size
         * @method
         * @param {Object} obj window size parameter
         * @param {Boolean} param.relativeToParent True if relative to the parent position and then use the css position:relative. \
         *   Default will be false and use position:fixed
         * @param {Number} obj.top window top
         * @param {Number} obj.left window left
         * @param {Number} obj.width window width
         * @param {Number} obj.height window height
         * @memberof ax/device/shared/FlashPlayer#
         */
        setWindowSize: function (obj) {
            if (obj.relativeToParent) {
                this._playerObject.style.position = "relative";
            } else {
                this._playerObject.style.position = "fixed";
            }

            if (this._playerObject) {
                this._playerObject.style.width = obj.width + "px";
                this._playerObject.style.height = obj.height + "px";
                this._playerObject.style.left = obj.left + "px";
                this._playerObject.style.top = obj.top + "px";
                if (this._playerObject.setWindowSize) {
                    this._playerObject.setWindowSize(obj.width, obj.height);
                }
            }

            this._currentWindowSize = obj;
            return;
        },
        /**
         * Sets video window size
         * @method
         * @memberof ax/device/shared/FlashPlayer#
         */
        setFullscreen: function () {
            this.setWindowSize({
                top: 0,
                left: 0,
                height: getResolution()
                    .height,
                width: getResolution()
                    .width
            });
            this._playerObject.style.position = "fixed";
        },
        /**
         * Timeout to throw error if it is still in connecting with the connectionTimeLimit
         * @method
         * @protected
         * @memberof ax/device/shared/FlashPlayer#
         */
        _onConnectionTimeout: function () {
            this._stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.NETWORK.DISCONNECTED);
            console.info("[xdk flash] Connection error");
        },
        /**
         * Onload of the video to load the duration of the video
         * @method
         * @param {object} data send from the flash player, like the duration
         * @memberof ax/device/shared/FlashPlayer#
         * @protected
         */
        _onLoad: function (data) {
            this._onLoaded = true;
            this._removeConnectionTimeOut();
            this._setDuration(data.duration);
            //seek time should be after setting the total time, otherwise there are conflict and condition checking error
            if (this._seek && this._seekTime > 0) {
                this._playerObject.seek(this._seekTime);
                this._seek = false;
            }

            if (!this._connected) {
                return;
            }

            console.info("[XDK flash] onload event");
            sMedia._onBufferingStart();
            sMedia._onBufferingProgress();
        },
        /**
         * To remove the connection time out set when set Media url and play
         * @method
         * @memberof ax/device/shared/FlashPlayer#
         * @protected
         */
        _removeConnectionTimeOut: function () {
            if (this._connectionTimeOut !== null) {
                util.clearDelay(this._connectionTimeOut);
                this._connectionTimeOut = null;
            }
        },
        /**
         * _onPlaying to be invoked when the player play
         * @method
         * @memberof ax/device/shared/FlashPlayer#
         * @protected
         */
        _onPlaying: function () {
            if (!this._connected) {
                return;
            }
            console.info("[xdk flash] onplaying event");
            this._removeConnectionTimeOut();
            if (!this._pausedVideo) {
                sMedia._onPlaying();
            } else {
                sMedia._onPause();
            }
            if (sMedia.isState(sMedia.BUFFERING)) {
                sMedia._onBufferingFinish();
            }
        },
        /**
         * to update the time when the video is playing
         * @method
         * @param {Number} time
         * @memberof ax/device/shared/FlashPlayer#
         * @protected
         */
        _onTimeUpdate: function (time) {
            if (!this._connected) {
                return;
            }
            this._curTime = time;
            sMedia._onTimeUpdate(time);
        },
        /**
         * when the video finish playing the video
         * @method
         * @memberof ax/device/shared/FlashPlayer#
         * @protected
         */
        _onFinished: function () {
            console.info("[XDK flash] finished event");
            this._stopPlayback();
            sMedia._onFinish();
        },
        /**
         * when the video has some error
         * @method _onError
         * @param {String} errorMsg from the player
         * @memberof ax/device/shared/FlashPlayer#
         * @protected
         */
        _onError: function (errorMsg) {
            // to avoid throw second error when it is alreay in error state or it is already finished
            if (sMedia.isState(sMedia.STOPPED)) {
                return;
            }
            this._stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.RENDER.FAILED, {
                code: null,
                msg: errorMsg
            }, errorMsg);
            console.error("[xdk flash] onRenderError : " + errorMsg);
        },
        /**
         * the callback from the flash player
         * @method callback
         * @param {String} evt the callback name
         * @param {Object} data the related data of the event
         * @memberof ax/device/shared/FlashPlayer#
         */
        callback: function (evt, data) {
            data = util.parse(data);
            console.debug("[xdk flash] callback" + evt + "data" + data.code);
            switch (evt) {
            case "timeUpdate":
                this._onTimeUpdate(data.time);
                break;
            case "loadedmetadata":
                console.debug("load meta data");
                this._onLoad(data);
                break;
            case "statuschange":
                switch (data.code) {
                case "NetStream.Finish":
                    this._onFinished();
                    break;
                case "NetStream.Buffer.Full":
                case "NetStream.Seek.Complete":
                    this._onPlaying();
                    break;
                case "NetStream.Play.Start":
                case "NetStream.Buffer.Empty":
                case "NetStream.Seek.Notify":
                    if (sMedia.isState(sMedia.PAUSED) || sMedia.isState(sMedia.SKIPPING)) {
                        return;
                    }
                    if (!sMedia.isState(sMedia.BUFFERING)) {
                        sMedia._onBufferingStart();
                    } else {
                        sMedia._onBufferingProgress();
                    }
                    break;
                case "NetStream.Play.Stop":
                    break;
                case "NetStream.Play.StreamNotFound":
                    sMedia._onError(PLAYBACK_ERRORS.NETWORK.FILE, null, data);
                    break;
                case "NetStream.Play.FileStructureInvalid":
                    sMedia._onError(PLAYBACK_ERRORS.RENDER.UNSUPPORTED, null, data);
                    break;
                }
                break;
            }
        }
    });
});
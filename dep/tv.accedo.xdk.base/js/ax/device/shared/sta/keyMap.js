/**
 * The STA keyMAP which uses the window.VK_DOWN to get the keyCode to map the keys
 * @module ax/device/shared/browser/keyMap
 **/
define("ax/device/shared/sta/keyMap", ["ax/util", "ax/device/vKey"], function (util, VKey) {
    "use strict";
    var keys = {
        VKey: {},
        keyCode: {}
    },
        VKKey = ["DOWN", "UP", "LEFT", "RIGHT", "ENTER", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "RED", "BLUE", "GREEN", "YELLOW", "PLAY", "PAUSE", "STOP", "FAST_FWD", "REWIND", "BACK"];
    util.each(VKKey, function (item) {
        var code = window["VK_" + item];
        if (!code) {
            return;
        }

        if (item.length !== 1) {
            if (item === "ENTER") {
                item = "OK";
            }
            if (item === "FAST_FWD") {
                item = "FF";
            }
            if (item === "REWIND") {
                item = "RW";
            }
            keys.VKey[code] = VKey[item];
            keys.keyCode[item] = code;
        } else {
            //number set
            keys.VKey[code] = VKey["KEY_" + item];
            keys.keyCode["KEY_" + item] = code;
        }
    });
    return {
        /*
         *The key mapping of sta. It will map the following keys "DOWN", "UP", "LEFT", "RIGHT", "ENTER", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "RED", "BLUE", "GREEN", "YELLOW", "PLAY", "PAUSE", "STOP", "FAST_FWD", "REWIND", "BACK"
         * @name STA
         * @constant
         * @memberof module:ax/device/shared/sta/keyMap
         * @public
         */
        STA: keys
    };
});
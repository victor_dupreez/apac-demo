/**
 * Class extend from the brwoser tvkey which uses the sta tvkey
 * @class ax/device/shared/sta/TvKey
 * @extends ax/device/shared/browser/TvKey
 */
define("ax/device/shared/sta/TvKey", ["ax/class", "ax/device/shared/browser/TvKey", "ax/device/shared/sta/keyMap"], function (klass, browserTvKey, staKeyMap) {
    "use strict";   
    return klass.create(browserTvKey, {}, {
        init: function () {
            this.initKeyMapping(staKeyMap.STA);
            this.initKeyHandling();
        }
    });
});
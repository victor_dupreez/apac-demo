/**
 * HTML5 System to handle the basic html5 system feature. It provides the visibility change.
 *
 * @class ax/device/shared/Html5System
 */
define("ax/device/shared/Html5System", [
    "ax/class",
    "ax/util",
    "ax/core",
    "ax/console",
    "require",
    "ax/EventDispatcher",
    "ax/device/interface/System"
], function (
    klass,
    util,
    core,
    console,
    require,
    EventDispatcher,
    ISystem
) {
    "use strict";

    return klass.createAbstract(EventDispatcher, [ISystem], {}, {
        /**
         * @protected
         * @name _device
         * @memberof ax/device/shared/Html5System#
         */
        _device: undefined,

        /**
         * @protected
         * @name _document
         * @memberof ax/device/shared/Html5System#
         */
        _document: undefined,

        /**
         * @private
         * @name __bindedOnVisibilityChange
         * @memberof ax/device/shared/Html5System#
         */
        __bindedOnVisibilityChange: undefined,
        init: function () {
            this._document = core.root.document;
            this._device = require("ax/device");

            if (!util.isUndefined(this._document)) {
                this._addVisibilityChangeListener();
            }
        },
        /**
         * Get device
         * @method _getDevice
         * @protected
         * @memberof ax/device/shared/Html5System#
         */
        _getDevice: function () {
            return this._device;
        },
        /**
         * Get document which should be window.document
         * @method _getDevice
         * @protected
         * @memberof ax/device/shared/Html5System#
         */
        _getDocument: function () {
            return this._document;
        },
        /**
         * To listen the page visibility change and notice deviceHandler resume and pause
         * @method _addVisibilityChangeListener
         * @protected
         * @memberof ax/device/shared/Html5System#
         */
        _addVisibilityChangeListener: function () {
            var _document = this._getDocument(),
                visibility = this._getVisibility();

            if (util.isUndefined(visibility)) {
                return;
            }

            this.__bindedOnVisibilityChange = util.bind(this._onVisibilityChange, this);
            _document.addEventListener(visibility.eventName, this.__bindedOnVisibilityChange, false);
        },
        /**
         * To get visibility API detail
         * @method _getVisibility
         * @protected
         * @memberof ax/device/shared/Html5System#
         */
        _getVisibility: function () {
            var _document = this._getDocument(),
                hidden, eventName;

            if (!util.isUndefined(_document.hidden)) {
                hidden = "hidden";
                eventName = "visibilitychange";
            } else if (!util.isUndefined(_document.mozHidden)) {
                hidden = "mozHidden";
                eventName = "mozvisibilitychange";
            } else if (!util.isUndefined(_document.msHidden)) {
                hidden = "msHidden";
                eventName = "msvisibilitychange";
            } else if (!util.isUndefined(_document.webkitHidden)) {
                hidden = "webkitHidden";
                eventName = "webkitvisibilitychange";
            } else {
                console.warn("Page Visibility API is not supported in this environment.");
                return;
            }

            return {
                hidden: hidden,
                eventName: eventName
            };
        },
        /**
         * Event visibilitychange listener
         * @method _onVisibilityChange
         * @protected
         * @param {Object} e Event
         * @memberof ax/device/shared/Html5System#
         */
        _onVisibilityChange: function (e) {
            var device = this._getDevice(),
                deviceHandler = device.getDeviceHandler(),
                _document = this._getDocument(),
                visibility = this._getVisibility();

            if (_document[visibility.hidden]) {
                deviceHandler.onDevicePause();
            } else {
                deviceHandler.onDeviceResume();
            }
        },
        /**
         * @method deinit
         * @protected
         * @memberof  ax/device/shared/Html5System#
         */
        deinit: function () {
            var _document, visibility;

            if (!util.isUndefined(this.__bindedOnVisibilityChange)) {
                _document = this._getDocument();
                visibility = this._getVisibility();
                _document.removeEventListener(visibility.eventName, this.__bindedOnVisibilityChange);
            }
        },
        /**
         * To set the system mute
         * @public
         * @method setSystemMute
         * @param {Boolean} flag True if turn on the mute, false when turn off.
         * @param {Boolean} True when send the request successfully.
         * @memberof ax/device/shared/Html5System#
         */
        setSystemMute: klass.abstractFn,
        /**
         * To exit the apps
         * @public
         * @param {Object} [opts]
         * @param {boolean} [opts.toTV=true] True when exit the application to TV. False when suspend the apps and to show the app bar.
         * @method exit
         * @memberof ax/device/shared/Html5System#
         */
        exit: klass.abstractFn,
        /**
         * Has mouse on webos
         * @public
         * @method hasMouse
         * @returns {Boolean} true
         * @memberof ax/device/shared/Html5System#
         */
        hasMouse: klass.abstractFn,
        /**
         * To get the display resolution
         * @public
         * @method getDisplayResolution
         * @return {Object} object {width:1280,height:720} or {width:1920, height1080}
         * @memberof ax/device/shared/Html5System#
         */
        getDisplayResolution: klass.abstractFn,
        /**
         * webos supports SSL
         * @public
         * @method supportSSL
         * @return {Boolean} True
         * @memberof ax/device/shared/Html5System#
         */
        supportSSL: klass.abstractFn,
        /**
         * webos supports SSL
         * @public
         * @method supportSSL
         * @return {Boolean} True
         * @memberof ax/device/shared/Html5System#
         */
        getNetworkStatus: klass.abstractFn,
        /**
         * Determine whether support cross domain ajax.
         * @method hasSOP
         * @return {Boolean} Return true if cross domain ajax is not allowed natively. Proxy / Allow-Origin header setup may be needed.
         * @memberof ax/device/shared/Html5System#
         */
        hasSOP: klass.abstractFn,

        /**
         * Setting the screen saver On/Off
         * @method setScreenSaver
         * @param {Boolean} flag True to turn on and off to turn off.
         * @return {Boolean}  Return false.
         * @memberof ax/device/shared/Html5System#
         */
        setScreenSaver: klass.abstractFn,
        /**
         * Power off the system
         * @method powerOff
         * @return {Boolean} Return false
         * @memberof ax/device/shared/Html5System#
         */
        powerOff: klass.abstractFn,
        /**
         * @method redraw
         * @param {HTMLElement} [element=document.body] the target element. Default will be document.body
         * @param {Boolean} false when not support
         * @memberof ax/device/shared/Html5System#
         */
        redraw: klass.abstractFn,
        /**
         * @method hasFixedKeyboard
         * @return {Boolean} Return true.
         * @memberof ax/device/shared/Html5System#
         */
        hasFixedKeyboard: klass.abstractFn
    });
});
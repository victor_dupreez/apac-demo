/**
 * Html5PlayerPool is a class that controls the HTML5 <video> object creation.
 * A limit could be set to controll the number of object being created.
 *
 * @class ax/device/shared/Html5PlayerPool
 * @param {Number} [max=3] The maximum number of objects that should be created
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define([
    "ax/class"
], function (
    klass
) {
    "use strict";

    return klass.create({}, {
        /**
         * The object list, storing the <video> objects created.
         * @private
         * @member {VIDEO[]}
         * @memberof ax/device/shared/Html5PlayerPool#
         */
        __objects: [],

        /**
         * The maximum number of player objects being created.
         * @private
         * @member {Number}
         * @memberof ax/device/shared/Html5PlayerPool#
         */
        __max: 3,

        init: function (max) {
            if (max > 0) {
                this.__max = max;
            }
        },

        /**
         * Creates and returns a valid player object.
         *
         * @method
         * @returns {VIDEO} A clean player object
         * @throws {Error} If the player object count has reached the limit
         * @memberof ax/device/shared/Html5PlayerPool#
         */
        get: function () {
            if (this.__objects.length >= this.__max) {
                throw new Error("Number of objects reached limit: " + this.__max);
            }

            var obj = document.createElement("video");

            this.__objects.push(obj);

            return obj;
        },

        /**
         * releases the player object so that the resources can be released.
         * This will reduce the object number count.
         *
         * @method
         * @param {VIDEO} obj The player object returned previously
         * @throws {Error} If the argument is not a valid player object
         * @memberof ax/device/shared/Html5PlayerPool#
         */
        release: function (obj) {
            var index = this.__objects.indexOf(obj);

            if (index < 0) {
                throw new Error("The object being released is not valid.");
            }

            this.__objects.splice(index, 1);
        }
    });
});
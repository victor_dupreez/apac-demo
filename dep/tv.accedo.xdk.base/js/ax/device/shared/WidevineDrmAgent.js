/**
 * The widevine drm agent
 *
 * @class ax/device/shared/WidevineDrmAgent
 * @implements ax/device/interface/DrmAgent
 */
define([
    "ax/class",
    "ax/device/interface/DrmAgent",
    "ax/promise",
    "ax/util",
    "ax/console",
    "ax/core",
    "ax/exception"
], function (
    klass,
    IDrmAgent,
    promise,
    util,
    console,
    core,
    exception
) {
    "use strict";
    return klass.create([IDrmAgent], {}, {
        /**
         * Set the drm messenger to send out the license request
         * @public
         * @param {ax/device/interface/DrmMessenger} messenger the target drm messenger
         * @method
         * @memberof ax/device/shared/WidevineDrmAgent#
         */
        setDrmMessenger: function (messenger) {
            this.__messenger = messenger;
        },
        /**
         * Send the license request
         * @public
         * @method
         * @param {String} url  the media url
         * @param {Object} opts the option configuration
         * @param {String} [opts.drmUrl] Set the DRM license url
         * @param {String} [opts.userData]  Set the user data
         * @param {String} [opts.portal]  Set the portal
         * @param {String} [opts.deviceId]  Set the device id
         * @param {String} [opts.streamId]  Set the stream id
         * @param {String} [opts.clientIp]  Set the client IP
         * @param {String} [opts.drmAckUrl]  Set the drm acknowledge url
         * @param {String} [opts.heartbeatUrl]  Set the heartbeat url
         * @param {Stinrg} [opts.heartbeatPeriod] Set the hearbeat period
         * @param {String} [opts.storeFront] Set the store front
         * @param {String} [opts.bandwidthCheckUrl] Set the bandwidth check url
         * @param {String} [opts.bandwidthCheckInterval Set the bandwidth check interval
         * @returns {String} the xml for license acquisition
         * @memberof ax/device/shared/WidevineDrmAgent#
         */
        sendLicense: function (url, opts) {
            var deferred = promise.defer(),
                message;

            opts = opts || {};

            message = this._createMessage(url, opts);

            console.info("[WidevineDrmAgent] Send the license request out with message " + message);
            this.__messenger.sendMessage(message, function (resultCode, resultMsg) {
                console.info("[WidevineDrmAgent] Receive the result");
                // DRM API does not return the msgId, resultCode, resultMsg for Widevine type.
                deferred.resolve(true);
            }, function (errorCode, errorText) {
                var description = "[WidevineDrmAgent] onError ( errorCode:" + errorCode + " - " + IDrmAgent.ERROR[errorCode] + ")";
                console.warn(description);
                deferred.reject(core.createException(errorCode, errorText));
            });

            return deferred.promise;
        },
        /**
         * Create message into the XML
         * @protected
         * @method
         * @memberof ax/device/shared/WidevineDrmAgent#
         */
        _createMessage: function (url, opts) {
            /*jshint quotmark: false */
            return '<?xml version="1.0" encoding="utf-8"?>' +
                '<WidevineCredentialsInfo xmlns="http://www.smarttv-alliance.org/DRM/widevine/2012/protocols/">' +
                '<ContentURL>' + (url || "") + '</ContentURL>' +
                '<DeviceID>' + (opts.deviceId || "") + '</DeviceID>' +
                '<StreamID>' + (opts.streamId || "") + '</StreamID>' +
                '<ClientIP>' + (opts.clientIp || "") + '</ClientIP>' +
                '<DRMServerURL>' + (opts.drmUrl || "") + '</DRMServerURL>' +
                '<DRMAckServerURL>' + (opts.drmAckUrl || "") + '</DRMAckServerURL>' +
                '<DRMHeartBeatURL>' + (opts.heartbearUrl || 0) + '</DRMHeartBeatURL>' +
                '<DRMHeartBeatPeriod>' + (opts.heartbeatPeriod || "") + '</DRMHeartBeatPeriod>' +
                '<UserData>' + (opts.userData || "") + '</UserData>' +
                '<Portal>' + (opts.portal || "") + '</Portal>' +
                '<StoreFront>' + (opts.storeFront || "") + '</StoreFront>' +
                '<BandwidthCheckURL>' + (opts.bandwidthCheckUrl || "") + '</BandwidthCheckURL>' +
                '<BandwidthCheckInterval>' + (opts.bandwidthCheckInterval || "") + '</BandwidthCheckInterval>' +
                '</WidevineCredentialsInfo>';
            /*jshint quotmark: true */
        }
    });
});
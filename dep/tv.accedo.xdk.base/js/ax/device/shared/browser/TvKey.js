/**
 * Extended class from abstract tv key to handle the regular tv key code via the document onkeydown and onkeypress. It provides the basic feature on the browser platform to start with
 * @class ax/device/shared/browser/TvKey
 */
define("ax/device/shared/browser/TvKey", ["ax/class", "ax/console", "ax/util", "ax/device/shared/browser/keyMap"], function (klass, console, util, keyMap) {
    "use strict";
    return klass.create({}, {
        __keys: null,
        __chars: null,
        init: function () {
            this.initKeyMapping(keyMap.REMOTE);
        },
        /**
         * To init the KeyMapping via the map object which can use the {@link module:ax/device/shared/browser/keyMap.REMOTE} or {@link module:ax/device/shared/browser/keyMap.KEYBOARD}.
         * @param {Object} keyMap the keyCode object mapping e.g { 38: keyboardVKey.UP }. Each keyCode object should be keycode to keyboardVKey or VKey reference
         * @param {Obejct} [charMap] the charCode object mapping. Each charCode object should be keycode to keyboardVKey or VKey reference
         * @public
         * @method initKeyMapping
         * @memberof ax/device/shared/browser/TvKey#
         * @example
         * keyMap.REMOTE = {
         *   keyCode: {
         *     38: keyboardVKey.UP,
         *     40: keyboardVKey.DOWN,
         *     37: keyboardVKey.LEFT,
         *     39: keyboardVKey.RIGHT,
         *     13: keyboardVKey.OK
         *    }
         *    }
         *    this.initKeyMapping(keyMap.REMOTE);
         */
        initKeyMapping: function (keyMap, charMap) {
            if (keyMap) {
                if (!this.__keys) {
                    this.__keys = {};
                }
                util.each(keyMap.VKey, util.bind(function (item) {
                    this.__keys[item.key] = item.value;
                }, this));
            }

            if (charMap && charMap.VKey) {
                if (!this.__chars) {
                    this.__chars = {};
                }
                util.each(charMap.VKey, util.bind(function (item) {
                    this.__chars[item.key] = item.value;
                }, this));
            }
        },
        /**
         * initializes the key handling logic for the device, after DOM ready. SInce it is for browser based. It will add keydown and keypress event
         * @method initKeyHandling
         * @function
         * @memberof ax/device/shared/browser/TvKey#
         * @public
         */
        initKeyHandling: function () {
            document.onkeypress = util.bind(this.keyHandler, this);
            document.onkeydown = util.bind(this.keyHandler, this);
        },
        /**
         * To handle the key when receive key event
         * @method keyHandler
         * @function
         * @param {Object} evt the event from the browser key event
         * @param {Function} dispatchKey the callback function when dispatch the key
         * @memberof ax/device/shared/browser/TvKey#
         * @public
         */
        keyHandler: function (evt) {
            var virtualKey, tvKey, deviceHandler;

            deviceHandler = require("ax/device").getDeviceHandler();

            //convert the event to our key standard with code and isChar attribute
            virtualKey = this.getVirtualKey(evt);
            tvKey = this.getMappedKey(virtualKey);
            if (tvKey) {
                deviceHandler.onDeviceKey(tvKey);
                evt.preventDefault();
                return false;
            }
            return true;
        },
        /* To check if it should handle in the char code instead of the keycode. 
         * @private
         * @method __isCharTyped
         * @returns {Boolean}
         * @memberof ax/device/shared/browser/TvKey#
         */
        __isCharTyped: function (keyCode) {
            if ((keyCode >= 65 && keyCode <= 90) || // a-z
                (keyCode >= 48 && keyCode <= 57) || // 0-9
                (keyCode >= 96 && keyCode <= 111) || // numpad
                keyCode === 32 // space and symbols
            ) {
                return true;
            }
            return false;
        },
        /* To get the mapped key from the virtual key
         * @public
         * @methold getMappedKey
         * @param {Object} evt the event from the browser key event
         * @returns {Object} return the object from {@link module:ax/device/shared/keyboardVKey} or {@link module:ax/device/VKey}
         * @memberof ax/device/shared/browser/TvKey#
         */
        getMappedKey: function (virtualKey) {
            var keyCode = virtualKey.code,
                isChar = virtualKey.isChar;
            console.info("Received key, code :" + keyCode + " isChar : " + isChar);
            if (!isChar) {
                if (this.__chars && this.__isCharTyped(keyCode)) {
                    return false;
                }
                if (this.__keys[keyCode]) {
                    return this.__keys[keyCode];
                }
            } else { // key press event
                if (this.__chars && this.__chars[keyCode]) {
                    return this.__chars[keyCode];
                }
                // unknown key, let key down handler handle it
            }
            return false;
        },
        /* To standardize the key object from the key event
         * @public
         * @methold getVirtualKey
         * @param {Obejct} virtualKey the virtual key format in keyCode and isChar
         * @returns {Object} return standard format of key event
         * @memberof ax/device/shared/browser/TvKey#
         */
        getVirtualKey: function (evt) {
            if (evt.type.toLowerCase() === "keypress") {
                return {
                    code: evt.charCode,
                    isChar: true
                };
            }
            return {
                code: evt.keyCode,
                isChar: false
            };
        },
        /* To get all the key mapping of charCode/ keyCode to keyboardVKey obejct from the browser TvKey
         * @public
         * @methold getKeyMapping
         * @returns {Object} return object of keyCode and charCode Object
         * @memberof ax/device/shared/browser/TvKey#
         */
        getKeyMapping: function () {
            var map = {
                keyCode: this.__keys
            };
            if (this.__chars) {
                map.charCode = this.__chars;
            }
            return map;
        }
    });
});
/**
 * HTML5 media player that supports multiple objects.
 * The number of objects can be managed by the configuration device.playerMaxObjects.
 * If not specified, maximum 3 object will be created.
 *
 * @example
 * <caption>Configuration example</caption>
 * {
 *   "device.playerMaxObjects": {
 *     "ax/device/shared/MultiObjectHtml5Player": 5
 *   }
 * }
 *
 * @class ax/device/shared/MultiObjectHtml5Player
 * @augments ax/device/shared/Html5Player
 * @augments ax/device/interface/Suspendable
 * @augments ax/device/interface/Preloadable
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define("ax/device/shared/MultiObjectHtml5Player", [
    "ax/core",
    "ax/class",
    "ax/device/AbstractPlayer",
    "ax/device/shared/Html5Player",
    "ax/device/interface/Player",
    "ax/device/Media",
    "ax/util",
    "ax/device/interface/Suspendable",
    "ax/device/shared/Html5PlayerSuspendedPlayback",
    "ax/device/shared/Html5PlayerPreloadedPlayback",
    "ax/device/interface/Preloadable",
    "./Html5PlayerPool",
    "ax/config",
    "ax/console"
], function (
    core,
    klass,
    AbstractPlayer,
    Html5Player,
    IPlayer,
    Media,
    util,
    Suspendable,
    Html5PlayerSuspendedPlayback,
    Html5PlayerPreloadedPlayback,
    Preloadable,
    PlayerPool,
    config,
    console
) {
    "use strict";

    var sMedia = Media.singleton();


    /**
     * Enumeration of the states of the native player.
     * @enum {Number}
     * @private
     * @memberof ax/device/shared/MultiObjectHtml5Player
     */
    var NATIVE_PLAYER_STATE = {
        /** The player is idle, freshly initialized */
        IDLE: 0,
        /** The player has loaded the media */
        LOADED: 1,
        /** The player is playing a media */
        PLAYING: 2,
        /** The player has been paused */
        PAUSED: 3,
        /** The media on the player has been stopped */
        STOPPED: 4,
        /** The player is suspended, with a media loaded */
        SUSPENDED_LOADED: 11,
        /** The player has been suspended whth a playing media */
        SUSPENDED_PLAYING: 12,
        /** The player has been suspended with a paused media */
        SUSPENDED_PAUSED: 13,
        /** The player has been suspended with a stopped media */
        SUSPENDED_STOPPED: 14
    };

    var PLAYBACK_ERRORS = IPlayer.PLAYBACK_ERRORS;

    return klass.create(Html5Player, [Suspendable, Preloadable], {}, {
        /**
         * The default parent node if none is specified in prepare().
         * @private
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __defaultParentNode: document.createElement("div"),

        /**
         * The current playback state.
         * @private
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __state: IPlayer.PLAYBACK_STATE.STOPPED,

        /**
         * The video object created.
         * @private
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __videos: [],

        /**
         * The native player registry.
         * @private
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __playerRegistry: {},

        /**
         * The list of active preloaded playback.
         * @private
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _preloadedPlaybackList: [],

        /**
         * The list of active suspended playback.
         * @protected
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _suspendedPlaybackList: [],

        /**
         * PlayerPool.
         * @private
         * @member {ax/device/shared/Html5PlayerPool}
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __playerPool: null,

        /**
         * Constructor of the class.
         *
         * @method
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        init: function () {
            this._super();

            var maxObjectsConfig = config.get("device.playerMaxObjects", {});
            var max = maxObjectsConfig[this.getId()];
            this.__playerPool = new PlayerPool(max);

            this.__bindedOnBackgroundVideoError = util.bind(this._onBackgroundVideoError, this);
        },

        /**
         * deinit the video player and remove the eventListener
         *
         * @method
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        deinit: function () {
            if (!this._prepared) {
                return;
            }

            this._prepared = false;

            if (!this._playerObject) {
                return;
            }

            this._stopPlayback();
            this._removeVideoEventListeners(this._playerObject);

            // the video tag may has been removed due to suspend()
            try {
                this.__parentNode.removeChild(this._playerObject);
            } catch (ex) {
                console.warn("[MultiObjectHtml5Player] deinit(): " + ex.message);
            }

            this.__playerPool.release(this._playerObject);
            this._playerObject = null;
        },

        getCurTime: function () {
            if (!this._playerObject) {
                return 0;
            }

            return this._playerObject.currentTime;
        },

        /**
         * Gets the number of active preloaded playback.
         * All preloaded playbacks are considered active before they got disposed.
         *
         * @method
         * @returns {Number} The number of active preloaded playbacks
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        getPreloadedCount: function () {
            return this._preloadedPlaybackList.length;
        },

        /**
         * Gets the number of current active suspended playbacks.
         * All suspended playback are considered as active unless they are disposed.
         *
         * @method
         * @returns {Number} The number of active suspended playbacks
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        getSuspendedCount: function () {
            return this._suspendedPlaybackList.length;
        },

        /**
         * pause the video item
         *
         * @method
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        pause: function () {
            this._super();
            this.__state = IPlayer.PLAYBACK_STATE.PAUSED;
        },

        /**
         * Plays the specified media.
         *
         * @method
         * @param {Object} [opts] object containing the required options
         * @param {Number} [opts.sec=0] Play the video at the specified second
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        play: function (opts) {
            if (!this._support) {
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.RENDER.UNSUPPORTED, "unsupport format in the html5 video player");
                return;
            }

            if (this.__errorReported) {
                console.warn("[Html5Player] Play after error. A load() operation is expected to reset the state.");
                return;
            }

            // handle error thrown while in background
            var playerMeta = this.__getPlayerMetaById(this._playerObject.id);
            var backgroundError = playerMeta.error;
            if (backgroundError) {
                // _onError has to be called in defer because the modules calling play may access the metadata
                // calling _onError will interrupt the play operation
                util.defer().then(util.bind(function () {
                    this._onError(backgroundError);
                }, this));
                playerMeta.error = null;
                return;
            }

            // if the url is not set stop or the url is not updated
            // play the previous video
            var previousSrc = this._playerObject.getAttribute("src");
            if (previousSrc && this.url !== previousSrc) {
                this.url = previousSrc;
            }

            if (this._currentWindowSize) {
                this.setWindowSize(this._currentWindowSize);
            }
            this.show();

            if (!this._connected) {
                this._connected = true;
                sMedia._onConnecting();
                this._removeConnectionTimeOut();
                this._connectionTimeOut = core.getGuid();
                util.delay(this._connectionTimeLimit, this._connectionTimeOut)
                    .then(util.bind(this._onConnectionTimeout, this), function () {
                        // ignore clear delay
                        return;
                    })
                    .done();
            }

            if (this._playerObject.readyState !== this._playerObject.HAVE_NOTHING) {
                this._removeConnectionTimeOut();
                this._setDuration();
            }

            this._playerObject.play();
            this._pausedVideo = false;
            this.__state = IPlayer.PLAYBACK_STATE.PLAYING;

            this._seek = false;

            if (opts && opts.sec) {
                this._seek = true;
                this._seekTime = opts.sec;
            }

            if (this._onLoaded || this._playerObject.readyState !== this._playerObject.HAVE_NOTHING) {
                if (this._seek && this._seekTime > 0) {
                    this._playerObject.currentTime = this._seekTime;
                    this._seek = false;
                }
            }
        },

        /**
         * Preloads a media for playback.
         * This interface is identical to {@link ax/device/interface/Player#load}, except that the media is loaded at a background player.
         *
         * @method
         * @param {String} url The URL of the media
         * @param {Object} [opts] the options for loading the media
         * @param {String} [opts.codec] The media codec
         * @param {String} [opts.type] The media type
         * @param {String} [opts.drm] The DRM to be used
         * @returns {ax/device/interface/Preloadable.PreloadedPlayback} The metadata of the preloading
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        preload: function (url, opts) {
            opts = opts || {};

            var video = this._getPlayerObject();
            var preloaded = new Html5PlayerPreloadedPlayback(this, video, url, opts);

            this._preloadedPlaybackList.push(preloaded);

            this.__registerPlayerMeta(video.id, {
                state: NATIVE_PLAYER_STATE.LOADED,
                url: url,
                opts: opts,
                player: video,
                linked: true
            });

            this.__attachPlayer(video);
            this.__pushToBack(video);
            video.setAttribute("src", url);
            video.load();

            return preloaded;
        },

        /**
         * Prepare the player
         *
         * @method
         * @param {object} opts The opts parameter passed from {@link ax/device/Media#load|device.media.load}
         * @param {HTMLElement} [opts.parentNode] parentNode to be append the player object
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        prepare: function (opts) {
            // check if the player has prepared
            if (this._prepared) {
                return;
            }

            opts = opts || {};

            this.__docBody = document.body;
            this.__docBody.appendChild(this.__defaultParentNode);

            // add a container to handle the object
            this.__parentNode = opts.parentNode || this.__defaultParentNode;
            this.__parentNode.style.backgroundColor = "black";
            this.__parentNode.id = "videoParentNode";
            this.__parentNode.className = "playerContainer";

            this._playerObject = this._getPlayerObject();

            this.__attachPlayer(this._playerObject);
            this.__bringToTop(this._playerObject);
            this.__registerPlayerMeta(this._playerObject.id, {
                state: NATIVE_PLAYER_STATE.IDLE,
                player: this._playerObject
            });

            this.hide();
            this._prepared = true;
        },

        reset: function () {
            if (!this._prepared) {
                return;
            }

            this.hide();
            this._stopPlayback();

            var disposeAll = function (disposableList) {
                util.each(disposableList, function (disposable) {
                    disposable.dispose();
                });
            };

            // dispose all suspended playbacks
            disposeAll(this._suspendedPlaybackList);

            // dispose all suspended playbacks
            disposeAll(this._preloadedPlaybackList);

            this.deinit();
        },

        resume: function () {
            this._super();
            this.__state = IPlayer.PLAYBACK_STATE.PLAYING;
        },

        /**
         * Suspends the current playback, returning a {@link ax/device/interface/Suspendable.SuspendedPlayback|SuspendedPlayback} object for restoration.
         *
         * @method
         * @returns {ax/device/shared/Html5PlayerSuspendedPlayback} The suspended playback
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        suspend: function () {
            var suspendedPlayback = new Html5PlayerSuspendedPlayback(this, this._playerObject, {
                url: this.url,
                pos: this._playerObject.currentTime,
                state: this.__state
            });

            this._suspendedPlaybackList.push(suspendedPlayback);

            this._playerObject.pause();
            this.__pushToBack(this._playerObject);

            var currentPlayerMeta = this.__getPlayerMetaById(this._playerObject.id);
            currentPlayerMeta.linked = true;

            this._playerObject = null;

            return suspendedPlayback;
        },

        /**
         * Attaches the video to the DOM.
         *
         * @method
         * @private
         * @param {Video} video The video to attach
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __attachPlayer: function (video) {
            var meta = this.__getPlayerMetaById(video.id);

            if (meta && meta.parentNode) {
                this.__parentNode = meta.parentNode;
            }

            this.__parentNode.appendChild(video);
        },

        /**
         * Detachess the video from the DOM.
         *
         * @method
         * @private
         * @param {Video} video The video to detach
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __detachPlayer: function (video) {
            this.__parentNode.removeChild(video);
        },

        /**
         * Pushes a <video> to background.
         * This will eventually hide the player from the UI and handle the event listeners.
         *
         * @method
         * @private
         * @param {Video} video The video to manage
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __pushToBack: function (video) {
            video.style.visibility = "hidden";
            this._removeVideoEventListeners(video);
            video.addEventListener("error", this.__bindedOnBackgroundVideoError);
        },

        /**
         * Brings a <video> from background to front.
         * This will eventually show the player in the UI and handle the event listeners.
         *
         * @method
         * @private
         * @param {Video} video The video to manage
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __bringToTop: function (video) {
            video.style.visibility = "visible";
            this._addVideoEventListeners(video);
            video.removeEventListener("error", this.__bindedOnBackgroundVideoError);
        },

        /**
         * Returns the native video instance to the allocator to release the resources.<br/>
         * The caller should not use the disposed video for any operations after this point.
         * Any video that will not be used any more must be returned, otherwise the next request may ending up getting nothing.
         *
         * @method
         * @protected
         * @param {Video} The HTML video object
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _disposePlayerObject: function (video) {
            if (this._playerObject === video) {
                var meta = this.__getPlayerMetaById(video.id);
                meta.linked = false;
            } else {
                this.__detachPlayer(video);
                video.removeEventListener("error", this.__bindedOnBackgroundVideoError);
                this.__unregisterPlayerMeta(video.id);
                this.__playerPool.release(video);
            }
        },

        /**
         * Disposes a preloaded media.
         * System resources are preserved after calling preload. The application is responsible to dispose any abandon preloaded media.
         * A disposed preloaded media cannot be played anymore.
         *
         * @method
         * @protected
         * @param {ax/device/shared/Html5PlayerPreloadedPlayback} preloaded The preloaded playback
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _disposePreloaded: function (preloaded) {
            var video = preloaded.__video;
            var index = this._preloadedPlaybackList.indexOf(preloaded);

            if (index > -1) {
                this._preloadedPlaybackList.splice(index, 1);
            }

            this._disposePlayerObject(video);
        },

        /**
         * Disposes the suspended playback.
         *
         * @method
         * @protected
         * @param {ax/device/shared/Html5PlayerSuspendedPlayback} suspended The suspended playback
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _disposeSuspended: function (suspended) {
            var video = suspended.__video;
            var index = this._suspendedPlaybackList.indexOf(suspended);

            if (index > -1) {
                this._suspendedPlaybackList.splice(index, 1);
            }

            this._disposePlayerObject(video);
        },

        /**
         * Do the actual job to load a new media.
         *
         * @method
         * @protected
         * @param {String} mediaUrl The media url
         * @param {Object} opts The loading options
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _doLoad: function (mediaUrl, opts) {
            this.__getPlayerMetaById(this._playerObject.id).hasError = false;
            this._super(mediaUrl, opts);
        },

        /**
         * Gets a native video instance for playback operation.
         * This method controls how the players are allocated.
         * There is a limit on the number of players. If there is no available video, an exception will be thrown.
         *
         * @method
         * @protected
         * @returns {Video} The HTML video object
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _getPlayerObject: function () {
            try {
                var playerObject = this.__playerPool.get();
                playerObject.id = "Html5Player-" + core.getGuid();
                playerObject.className = "playerObject";

                return playerObject;
            } catch (e) {
                this.__errorReported = true;
                throw e;
            }
        },

        /**
         * Gets the registered native player metadata by id.
         *
         * @method
         * @private
         * @param {String} id The player id
         * @returns {Object} The registered native player metadate
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __getPlayerMetaById: function (id) {
            return this.__playerRegistry[id];
        },

        /**
         * when the video has error
         * @method
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         * @protected
         */
        _onError: function (e) {
            // to avoid throw second error when it is alreay in error state or it is already finished
            if (this.__hasFinished || this.__errorReported) {
                return;
            }

            var msg = "";

            if (this._playerObject.networkState === this._playerObject.NETWORK_NO_SOURCE) {
                // media not found
                msg = "NETWORK_NO_SOURCE - no audio/video source found";
                this._removeConnectionTimeOut();
                this.hide();
                this._connected = false;
                this._onLoaded = false;
            } else {
                switch (e.target.error.code) {
                case e.target.error.MEDIA_ERR_ABORTED:
                    msg = "You aborted the video playback.";
                    break;
                case e.target.error.MEDIA_ERR_NETWORK:
                    msg = "A network error caused the video download to fail part-way.";
                    break;

                case e.target.error.MEDIA_ERR_DECODE:
                    msg = "The video playback was aborted due to a corruption problem or because the video used features your browser did not support.";
                    break;

                case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
                    msg = "The media resource indicated by the src attribute or assigned media provider object was not suitable.";
                    break;

                default:
                    msg = "An unknown error occurred.";
                    break;
                }
                this._stopPlayback();
            }

            this.__errorReported = true;

            var currentPlayerMeta = this.__getPlayerMetaById(this._playerObject.id);
            currentPlayerMeta.hasError = true;

            sMedia._onError("onRenderError", msg);
        },

        /**
         * Callback function for the video error event while the <video> is in background.
         *
         * @callback
         * @protected
         * @param {Event} e The DOM error event
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _onBackgroundVideoError: function (e) {
            var playerMeta = this.__getPlayerMetaById(e.target.id);
            playerMeta.error = e;
        },

        /**
         * Prepares the player object for the later playback.
         * Overrides the super class.
         * Always return true.
         *
         * @method
         * @param {Object} opts The loading options
         * @returns {Boolean} Return true if the preparation success, false otherwise
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _preparePlayerObject: function (opts) {
            var playerMeta;

            // native player has been dereference by suspend()
            if (!this._playerObject) {
                this._playerObject = this._getPlayerObject();

                this.__attachPlayer(this._playerObject);

                playerMeta = {
                    state: NATIVE_PLAYER_STATE.IDLE,
                    player: this._playerObject
                };

                this.__registerPlayerMeta(this._playerObject.id, playerMeta);
            } else {
                playerMeta = this.__getPlayerMetaById(this._playerObject.id);
            }

            if (opts.parentNode && opts.parentNode !== this.__parentNode) {
                // detach from current parent node
                if (this.__parentNode) {
                    this.__parentNode.removeChild(this._playerObject);
                }

                // move to new parent node
                this.__parentNode = playerMeta.parentNode = opts.parentNode;
                this.__parentNode.appendChild(this._playerObject);
            }

            return true;
        },

        /**
         * Registers native player metadata.
         * Any new registration will overwrite the existing entry.
         *
         * @method
         * @private
         * @param {String} id The player id
         * @param {Object} meta The metadata
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __registerPlayerMeta: function (id, meta) {
            this.__playerRegistry[id] = meta;
        },

        /**
         * Stops the video playback for real.
         *
         * @method
         * @protected
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _stopPlayback: function () {
            this.__state = IPlayer.PLAYBACK_STATE.STOPPED;

            if (this._playerObject) {
                this._playerObject.pause();
                //XDK-2053 Fail to set current time if the video is not loaded
                if (this._onLoaded) {
                    this._playerObject.currentTime = 0;
                }
            }

            this.hide();
            this._removeConnectionTimeOut();
            this._connected = false;
        },

        /**
         * Swaps the active video object with the specifed video object, making the specifed one active.
         *
         * @method
         * @param {String} videoId The id of the video object
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _swapPlayerObject: function (videoId) {
            var isUsing = false;

            if (this._playerObject) {
                isUsing = (videoId !== this._playerObject.id);
            }

            if (isUsing) {
                var currentPlayerMeta = this.__getPlayerMetaById(this._playerObject.id);
                var currentPlayer = currentPlayerMeta.player;

                currentPlayer.pause();

                if (!currentPlayerMeta.linked) {
                    this.__detachPlayer(currentPlayer);
                    this.__unregisterPlayerMeta(this._playerObject.id);
                    this.__playerPool.release(this._playerObject);
                } else {
                    this.__pushToBack(currentPlayer);
                }
            }

            var playerMeta = this.__getPlayerMetaById(videoId);
            var video = playerMeta.player;

            // update the state of the video
            playerMeta.state = NATIVE_PLAYER_STATE.STOPPED;

            // update the reference of the video
            this._playerObject = video;
            this.__bringToTop(video);

            // update the player error flag
            this.__errorReported = playerMeta.hasError;
            this.__hasFinished = false;
        },

        /**
         * Unregisters native player metadata.
         * The unregistered metadata will be set undefined.
         *
         * @method
         * @private
         * @param {String} id The player id
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        __unregisterPlayerMeta: function (id) {
            this.__playerRegistry[id] = undefined;
        },

        getId: function () {
            return "ax/device/shared/MultiObjectHtml5Player";
        }
    });
});
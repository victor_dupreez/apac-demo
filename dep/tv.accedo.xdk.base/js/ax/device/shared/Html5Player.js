/**
 * HTML5 media player.
 *
 * @class ax/device/shared/Html5Player
 * @augments ax/device/interface/Player
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/device/shared/Html5Player", [
    "ax/core",
    "ax/class",
    "ax/device/AbstractPlayer",
    "ax/device/interface/Player",
    "ax/device/Media",
    "ax/util",
    "ax/console",
    "require"
], function (
    core,
    klass,
    AbstractPlayer,
    IPlayer,
    Media,
    util,
    console,
    require) {
    "use strict";
    var sMedia = Media.singleton(),
        resolution = null,
        getResolution = function () {
            if (resolution) {
                return resolution;
            }
            resolution = require("ax/device")
                .system.getDisplayResolution();
            return resolution;
        };

    var PLAYBACK_ERRORS = IPlayer.PLAYBACK_ERRORS;

    return klass.create(AbstractPlayer, {}, {
        /**
         * to see the html 5 player is onLoaded or not. If it is onloaded, user can seek time
         * @protected
         * @constant
         * @memberof ax/device/shared/Html5Player#
         */
        _onLoaded: false,
        /**
         * container name of the player
         * @protected
         * @constant
         * @memberof ax/device/shared/Html5Player#
         */
        _CONTAINER_NAME: "Html5PlayerContainer",
        /**
         * parent node of the player object
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _parentNode: null,
        /**
         * the player object
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _playerObject: null,
        /**
         * the init status of the player, if it isn't true, the event Listener or player may not been attached to DOM
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _prepared: false,
        /**
         * total time of the video
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _duration: 0,
        /**
         * connection time out (in sec) when the video is fail to play
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _connectionTimeLimit: 90,
        // in sec
        /**
         * store the timeout of the connection time out
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _connectionTimeOut: null,
        /**
         * to check if it is connected
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _connected: false,
        /**
         * to check if it is paused
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _pausedVideo: false,
        /**
         * if the video format HLS is supported
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _supportHLS: false,
        /**
         * to check if the video format is supported
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _support: true,
        /**
         * to check if it needs to seek before play
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _seek: false,
        /**
         * to store the time which needed to be seek
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _seekTime: 0,
        /**
         * to store the last loaded media URL
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _url: null,

        /**
         * Sets to true once the media is finished.
         * @private
         * @type {Boolean}
         * @memberof ax/device/shared/Html5Player#
         */
        __hasFinished: true,

        /**
         * Sets to true after the error is reported, and further error shouldn't be reported until the next load.
         * @private
         * @type {Boolean}
         * @memberof ax/device/shared/Html5Player#
         */
        __errorReported: false,

        init: function () {
            this.__bindedOnLoad = util.bind(this._onLoad, this);
            this.__bindedOnTimeUpdate = util.bind(this._onTimeUpdate, this);
            this.__bindedOnWaiting = util.bind(this._onWaiting, this);
            this.__bindedOnPlaying = util.bind(this._onPlaying, this);
            this.__bindedOnError = util.bind(this._onError, this);
            this.__bindedOnFinished = util.bind(this._onFinished, this);
            this.__bindedOnCanPlay = util.bind(this._onCanPlay, this);
            this.__bindedOnDurationChange = util.bind(this._onDurationChange, this);
            this.__bindedOnRateChange = util.bind(this._onRateChange, this);
        },

        /**
         * Prepare the player
         * @name prepare
         * @method
         * @param {object} opts The opts parameter passed from {@link ax/device/Media#load|device.media.load}
         * @param {HTMLElement} [opts.parentNode] parentNode to be append the player object.
         * @public
         * @memberof ax/device/shared/Html5Player#
         */
        prepare: function (opts) {
            console.info("html5 player prepare" + opts);
            //check if prepared and check parentNode
            if (this._prepared) {
                return;
            }

            //default position to place the video object
            this._parentNode = document.getElementsByTagName("body")[0];

            //add a container to handle the object
            this._playerContainer = document.createElement("div");
            this._playerContainer.id = this._CONTAINER_NAME;
            this._playerContainer.className = "playerContainer";

            //create the player object
            this._playerObject = document.createElement("video");
            this._playerObject.id = "Html5Player";
            this._playerObject.className = "playerObject";

            this._playerContainer.appendChild(this._playerObject);
            this._parentNode.appendChild(this._playerContainer);

            this._addVideoEventListeners(this._playerObject);

            this.hide();
            this._prepared = true;
        },

        /**
         * resets the video player, to non-playing mode
         * @method
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        reset: function () {
            if (!this._prepared) {
                return;
            }
            this.hide();
            this._stopPlayback();
            this._removeVideoEventListeners(this._playerObject);
            this.deinit();
        },
        /**
         * deinit the video player and remove the eventListener
         * @method
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        deinit: function () {
            if (!this._prepared) {
                return;
            }
            this._prepared = false;
            this._stopPlayback();

            if (this._playerContainer) {
                this._playerContainer.removeChild(this._playerObject);
                this._parentNode.removeChild(this._playerContainer);
                this._playerContainer = null;
            } else {
                this._parentNode.removeChild(this._playerObject);
            }
            console.info("HTML5 video player deinit successfull.");
            return;
        },
        /**
         * play the video item
         * @method
         * @param {Object} opts object containing the required options
         * @param {Number} opts.sec Play the video at the specified second
         * @public
         * @memberof ax/device/shared/Html5Player#
         */
        play: function (opts) {
            if (this.__errorReported) {
                console.warn("[Html5Player] Play after error. A load() operation is expected to reset the state.");
                return;
            }

            console.info("[XKD HTML5] Play requested.");
            if (!this._support) {
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.RENDER.UNSUPPORTED, "unsupport format in the html5 video player");
                return;
            }

            //if stop the video and press play, it will play the previous video
            if (this.url && (!this._playerObject.getAttribute("src") || this._playerObject.getAttribute("src") !==
                    this.url)) {
                this._playerObject.setAttribute("src", this.url);
                this._playerObject.load();
            }

            this.show();

            if (!this._connected) {
                this._connected = true;
                sMedia._onConnecting();
                this._playerObject.play();
                this._removeConnectionTimeOut();
                this._connectionTimeOut = core.getGuid();
                util.delay(this._connectionTimeLimit, this._connectionTimeOut)
                    .then(util.bind(this._onConnectionTimeout, this), function () {
                        // ignore clear delay
                        return;
                    })
                    .done();

            }

            this.__hasFinished = false;
            this._pausedVideo = false;
            this._seek = false;

            //allow to set sec:0
            if (opts && !util.isUndefined(opts.sec)) {
                this._seek = true;
                this._seekTime = opts.sec;
            }

            //make sure the speed is 1.
            if (this.getPlaybackSpeed() !== 1) {
                this._playerObject.playbackRate = 1;
            }

            if (this._onLoaded) {
                //remove time out when it is onloaded
                this._removeConnectionTimeOut();
                if (this._seek && this._seekTime > 0) {
                    this.__setCurrentTime(this._seekTime);
                    this._seek = false;

                    // create a seeked event listener that works only once
                    var play = util.bind(function () {
                        this._playerObject.play();
                        this._playerObject.removeEventListener("seeked", play);
                    }, this);

                    this._playerObject.addEventListener("seeked", play);

                    return;
                }
            }


            this._playerObject.play();
        },
        /**
         * pause the video item
         * @method
         * @public
         * @memberof ax/device/shared/Html5Player#
         */
        pause: function () {
            console.info("[XDK HTML5] Pausing video.");
            this._pausedVideo = true;
            this._playerObject.pause();
            sMedia._onPause();
        },
        /**
         * stop the video playback
         * @method
         * @public
         * @memberof ax/device/shared/Html5Player#
         */
        stop: function () {
            console.info("[XDK HTML5] Stopping video");

            this._stopPlayback();

            sMedia._onStopped();
        },
        /**
         * stop the video playback for real
         * @method
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _stopPlayback: function () {
            this._playerObject.pause();

            this.hide();
            this._removeConnectionTimeOut();
            this._connected = false;

            //XDK-2053 Fail to set current time if the video is not loaded
            if (this._onLoaded && !this.__hasFinished) {
                this.__setCurrentTime(0);
            }
        },
        /**
         * resume playing the video
         * @method
         * @public
         * @memberof ax/device/shared/Html5Player#
         */
        resume: function () {
            console.info("[XDK HTML5] Resuming...");
            this._playerObject.play();
            this._pausedVideo = false;

            //make sure the speed is 1.
            if (this.getPlaybackSpeed() !== 1) {
                this._playerObject.playbackRate = 1;
            }
        },
        /**
         * Skip the playback forward/backward for certain seconds
         * @method
         * @param {Number} sec number of seconds to skip (10 by default)
         * @public
         * @memberof ax/device/shared/Html5Player#
         */
        skip: function (sec) {
            this.__setCurrentTime(this.getCurTime()+sec);
        },
        /**
         * Seek to specifiy position of the video
         * @method
         * @param {Number} sec the position to seek to in seconds
         * @memberof ax/device/shared/Html5Player#
         */
        seek: function (sec) {
            //since it is so strange when seek the duration and no response, try to seek a sec before the end.
            if (sec === this._duration) {
                sec = Math.floor(this.getDuration() - 1);
            }
            this.__setCurrentTime(sec);
        },
        /**
         * returns playback speed
         * @method
         * @return {Number} playback speed
         * @memberof ax/device/shared/Html5Player#
         * @public
         */
        getPlaybackSpeed: function () {
            return this._playerObject.playbackRate;
        },
        /**
         * Get the current playback time
         * @method
         * @return {Number} the current playback time
         * @memberof ax/device/shared/Html5Player#
         * @public
         */
        getCurTime: function () {
            return this._playerObject.currentTime;
        },
        /**
         * to set the total time of the player
         * @method
         * @memberof ax/device/shared/Html5Player#
         * @proteced
         */
        _setDuration: function () {
            this._duration = this._playerObject.duration;
        },
        /**
         * to get the total time of the video
         * @method
         * @return {number} the total time of the video
         * @memberof ax/device/shared/Html5Player#
         * @public
         */
        getDuration: function () {
            return this._duration;
        },
        /**
         * Show the player
         * @method
         * @public
         * @memberof ax/device/shared/Html5Player#
         */
        show: function () {
            if (this._playerObject) {
                this._playerObject.style.visibility = "visible";
            }
        },
        /**
         * Hide the player
         * @method
         * @public
         * @memberof ax/device/shared/Html5Player#
         */
        hide: function () {
            if (this._playerObject) {
                this._playerObject.style.visibility = "hidden";
            }
        },
        /**
         * Gets the player's capabilities
         * @method
         * @return {ax/device/interface/Player~PlayerCapabilites}
         * @memberof ax/device/shared/Html5Player#
         */
        getCapabilities: function () {
            var cap = {
                type: ["mp4", "mp3"]
            };
            if (this._supportHLS) {
                cap.type.push("hls");
            }
            return cap;
        },
        /**
         * Loads the specified media
         * @method
         * @param {String} url the URL address of the media
         * @param {Object} [opts] the options for loading this media
         * @param {module:ax/device/playerRegistry.DRM} [opts.drm] DRM to be used
         * @memberof ax/device/shared/Html5Player#
         */
        load: function (mediaUrl, opts) {
            this.url = mediaUrl;
            if (!this._prepared) {
                return false;
            }

            opts = opts || {};

            // prepare the player object then do the loading
            // if the player object cannot be prepared, then skip loading
            // todo: throw exception?
            if (this._preparePlayerObject(opts)) {
                this._doLoad(mediaUrl, opts);
            }

            return mediaUrl;
        },

        /**
         * Return the readiness of the video metadata.
         * @method
         * @returns {Boolean} True if the metadata has been loaded, false otherwise.
         * @memberof ax/device/shared/Html5Player#
         */
        isMetadataLoaded: function () {
            return this._onLoaded;
        },

        /**
         * Sets video window size
         * @method
         * @param {Object} param window size parameter
         * @param {Boolean} param.relativeToParent True if relative to the parent position and then use the css position:relative. \
         *                   Default will be false and use position:fixed
         * @param {Integer} param.top window top
         * @param {Integer} param.left window left
         * @param {Integer} param.width window width
         * @param {Integer} param.height window height
         * @memberof ax/device/shared/Html5Player#
         */
        setWindowSize: function (obj) {
            if (!this._playerObject) {
                console.warn("[Html5Player] Fail to set the size due to no playerObject");
                return;
            }

            var objClassName = "playerObject",
                deviceResolution = getResolution();

            //determine whether it is relative to parent which add a relative class with "position:relative"
            if (obj.relativeToParent) {
                objClassName += " relative";
            }

            //determine whether it is fullscreen and add a fullscreen selector
            if (obj.width === deviceResolution.width && obj.height === deviceResolution.height) {
                objClassName += " fullscreen";
            }

            this._playerObject.className = objClassName;
            this._playerObject.style.width = obj.width + "px";
            this._playerObject.style.height = obj.height + "px";
            this._playerObject.style.left = obj.left + "px";
            this._playerObject.style.top = obj.top + "px";

            this._currentWindowSize = obj;

            return;
        },
        /**
         * Sets video window size
         * @method
         * @memberof ax/device/shared/Html5Player#
         */
        setFullscreen: function () {
            var deviceResolution = getResolution();
            this.setWindowSize({
                top: 0,
                left: 0,
                height: deviceResolution.height,
                width: deviceResolution.width
            });
        },

        /**
         * Prepares the player object for the later playback.
         *
         * @method
         * @protected
         * @param {Object} opts The loading options
         * @returns {Boolean} Return true if the preparation success, false otherwise
         * @memberof ax/device/shared/Html5Player#
         */
        _preparePlayerObject: function (opts) {
            if (!this._playerObject) {
                return false;
            }

            if (opts.parentNode) {
                //move to the correct place
                if (this._playerContainer) {
                    this._parentNode.removeChild(this._playerContainer);
                    this._playerContainer = null;
                }
                this._parentNode = opts.parentNode;
                this._parentNode.appendChild(this._playerObject);
            }

            return true;
        },

        /**
         * Do the actual job to load a new media.
         *
         * @method
         * @protected
         * @param {String} mediaUrl The media url
         * @param {Object} opts The loading options
         * @memberof ax/device/shared/Html5Player#
         */
        _doLoad: function (mediaUrl, opts) {
            this.__hasFinished = false;
            this.__errorReported = false;
            this._onLoaded = false;
            this._connected = false;
            this._pausedVideo = true;
            this._playerObject.setAttribute("src", mediaUrl);
            this._playerObject.load();
            this.hide();

            if (opts.type === "mp3") {
                this.setWindowSize({
                    "width": 0,
                    "height": 0,
                    "top": 0,
                    "left": 0
                });
            }

            this._support = this._canPlayMedia(opts);
        },

        /**
         * Checks if the player could play the specific media type and drm.
         *
         * @method
         * @protected
         * @param {Object} opts The load options
         * @returns {Boolean} True if the player could play the media, false otherwise
         * @memberof ax/device/shared/MultiObjectHtml5Player#
         */
        _canPlayMedia: function (opts) {
            var capabilities = this.getCapabilities();

            var type = opts.type;
            var drm = opts.drm;

            var typePass = !(type && capabilities.type.indexOf(type) === -1) || type === "*";
            var drmPass = !(drm && capabilities.drms.indexOf(drm) === -1);

            return typePass && drmPass;
        },

        /**
         * event handler for HTML5 canplay event
         * @method
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _onCanPlay: function () {
            //since it isn't onConnected, so it can't play
            if (this._pausedVideo || !this._connected) {
                return;
            }
            this._playerObject.play();
        },
        /**
         * Timeout to throw error if it is still in connecting with the connectionTimeLimit
         * @method
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _onConnectionTimeout: function () {
            this._stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.NETWORK.FAILED);
        },

        /**
         * Callback for durationchange event.
         *
         * @method
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _onDurationChange: function () {
            this._onLoaded = true;
            this._setDuration();
            this._performInitialSeek();
        },

        /**
         * Onload of the video to load the duration of the video
         * @method
         * @memberof ax/device/shared/Html5Player#
         * @protected
         */
        _onLoad: function () {
            this._removeConnectionTimeOut();
            this._onDurationChange();

            //ignore the state change when speeding
            if (!this._connected || this.getPlaybackSpeed() !== 1) {
                return;
            }

            console.info("[XDK HTML5] onload event");
            sMedia._onBufferingStart();
            sMedia._onBufferingProgress();
        },

        /**
         * Performs the seek operation for the initial play(sec) command.
         * This should be done after the duration is ready, otherwise it won't work.
         *
         * @method
         * @memberof ax/device/shared/Html5Player#
         */
        _performInitialSeek: function () {
            if (!this._seek || this._seekTime < 0) {
                return;
            }

            this.__setCurrentTime(this._seekTime);
            this._seek = false;
        },

        /**
         * Adds the event listeners to the video object.
         *
         * @method
         * @protected
         * @param {Video} video The HTML5 video object
         * @memberof ax/device/shared/Html5Player#
         */
        _addVideoEventListeners: function (video) {
            video.addEventListener("loadedmetadata", this.__bindedOnLoad);
            video.addEventListener("waiting", this.__bindedOnWaiting);
            video.addEventListener("timeupdate", this.__bindedOnTimeUpdate);
            video.addEventListener("seeking", this.__bindedOnWaiting);
            video.addEventListener("seeked", this.__bindedOnPlaying);
            video.addEventListener("playing", this.__bindedOnPlaying);
            video.addEventListener("error", this.__bindedOnError);
            video.addEventListener("ended", this.__bindedOnFinished);
            video.addEventListener("canplay", this.__bindedOnCanPlay);
            video.addEventListener("durationchange", this.__bindedOnDurationChange);
            video.addEventListener("ratechange", this.__bindedOnRateChange);
        },

        /**
         * Removes the event listeners from the video object.
         *
         * @method
         * @protected
         * @param {Video} video The HTML5 video object
         * @memberof ax/device/shared/Html5Player#
         */
        _removeVideoEventListeners: function (video) {
            video.removeEventListener("loadedmetadata", this.__bindedOnLoad);
            video.removeEventListener("waiting", this.__bindedOnWaiting);
            video.removeEventListener("timeupdate", this.__bindedOnTimeUpdate);
            video.removeEventListener("seeking", this.__bindedOnWaiting);
            video.removeEventListener("seeked", this.__bindedOnPlaying);
            video.removeEventListener("playing", this.__bindedOnPlaying);
            video.removeEventListener("error", this.__bindedOnError);
            video.removeEventListener("ended", this.__bindedOnFinished);
            video.removeEventListener("canplay", this.__bindedOnCanPlay);
            video.removeEventListener("durationchange", this.__bindedOnDurationChange);
            video.removeEventListener("ratechange", this.__bindedOnRateChange);
        },

        /**
         * To remove the connection time out set when set Media url and play
         * @method
         * @memberof ax/device/shared/Html5Player#
         * @protected
         */
        _removeConnectionTimeOut: function () {
            if (this._connectionTimeOut !== null) {
                util.clearDelay(this._connectionTimeOut);
                this._connectionTimeOut = null;
            }
        },
        /**
         * to invoke the waiting msg
         * @method
         * @memberof ax/device/shared/Html5Player#
         * @protected
         */
        _onWaiting: function () {
            if (sMedia.isState(sMedia.CONNECTING) || this.__needBlockStateChange()) {
                return;
            }
            console.info("[xdk html5] waiting");
            sMedia._onBufferingStart();
            sMedia._onBufferingProgress();
        },
        /**
         * __needBlockStateChange, when it is not connected or speeding, try to block the state change and maintain the state.
         * @method
         * @returns {Boolean} True if the need to block the state change
         * @memberof ax/device/shared/Html5Player#
         * @private
         */
        __needBlockStateChange: function () {
            return !this._connected || (this.getPlaybackSpeed() !== 1);
        },
        /**
         * _onPlaying to be invoked when the player play
         * @method
         * @memberof ax/device/shared/Html5Player#
         * @protected
         */
        _onPlaying: function () {
            //ignore the state change when speeding
            if (this.__needBlockStateChange()) {
                return;
            }
            console.info("[xdk html5] onplaying event");
            this._removeConnectionTimeOut();
            if (!this._pausedVideo) {
                sMedia._onPlaying();
            } else {
                sMedia._onPause();
            }
            if (sMedia.isState(sMedia.BUFFERING)) {
                sMedia._onBufferingFinish();
            }
        },
        /**
         * to update the time when the video is playing
         * @method
         * @memberof ax/device/shared/Html5Player#
         * @protected
         */
        _onTimeUpdate: function () {
            if (!this._connected) {
                return;
            }

            sMedia._onTimeUpdate(this.getCurTime());
        },
        /**
         * when the video finish playing the video
         * @method
         * @memberof ax/device/shared/Html5Player#
         * @protected
         */
        _onFinished: function () {
            console.info("[XDK HTML5] finished event");
            this.__hasFinished = true;
            this._stopPlayback();
            this.__hasFinished = true;
            sMedia._onFinish();
        },
        /**
         * when the video has error
         * @method
         * @memberof ax/device/shared/Html5Player#
         * @protected
         */
        _onError: function (e) {
            // to avoid throw second error when it is alreay in error state or it is already finished
            if (this.__hasFinished || this.__errorReported) {
                return;
            }

            var msg = "",
                error;

            if (this._playerObject.networkState === this._playerObject.NETWORK_NO_SOURCE) {
                // media not found
                error = PLAYBACK_ERRORS.NETWORK.FILE;

                this._removeConnectionTimeOut();
                this.hide();
                this._connected = false;
                this._onLoaded = false;

            } else {
                switch (e.target.error.code) {
                case e.target.error.MEDIA_ERR_ABORTED:
                    error = PLAYBACK_ERRORS.RENDER.ABORTED;
                    break;
                case e.target.error.MEDIA_ERR_NETWORK:
                    error = PLAYBACK_ERRORS.NETWORK.FAILED;
                    break;
                case e.target.error.MEDIA_ERR_DECODE:
                    error = PLAYBACK_ERRORS.RENDER.DECODE;
                    break;
                case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
                    error = PLAYBACK_ERRORS.RENDER.UNSUPPORTED;
                    break;
                default:
                    error = PLAYBACK_ERRORS.RENDER.FAILED;
                    break;
                }
                this._stopPlayback();
            }


            this.__errorReported = true;

            console.error("[XDK HTML5] onRenderError : " + msg);

            sMedia._onError(error);
        },

        /**
         * handle the player event when playback rate change
         * @protected
         * @memberof ax/device/shared/Html5Player#
         */
        _onRateChange: function() {
            var rate = this._playerObject.playbackRate;

            //check from the rate change event to determine whether it is speeding or not.
            if (rate !== 1) {
                sMedia._onSpeeding();
                return;
            }

            sMedia._onPlaying();
        },
        /**
         * get the playobject
         * @protected
         * @returns {HTML5Element} the html5 video object
         * @memberof ax/device/shared/Html5Player#
         */
        getPlayerObject: function () {
            return this._playerObject;
        },
        /**
         * Speed up/down the media playback
         * @method speed
         * @param {Number} speed The speed for the video. It now only accepts number larger than 0, where 1 is the normal speed. 
         * @memberof ax/device/shared/Html5Player#
         * @public
         */
        speed: function (speed) {

            if (!util.isNumber(speed)) {
                console.warn("Speed value must be a number");
                return;
            }

            if (speed <= 0) {
                console.warn("Negative playbackRate is not supported");
                return;
            }

            if (sMedia.isState(sMedia.PAUSED)) {
                this._playerObject.play();
            }

            this._playerObject.playbackRate = speed;
            this._pausedVideo = false;
        },
        /**
         * set the player object current time
         * @method __setCurrentTime
         * @param {Number} sec the target second 
         * @memberof ax/device/shared/Html5Player#
         * @private
         */
        __setCurrentTime: function (sec) {
            this._playerObject.currentTime = sec;
        }
    });
});
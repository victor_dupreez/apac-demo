/**
 * The playready drm agent
 *
 * @class ax/device/shared/PlayreadyDrmAgent
 * @implements ax/device/interface/DrmAgent
 */
define([
    "ax/class",
    "ax/device/interface/DrmAgent",
    "ax/promise",
    "ax/util",
    "ax/console",
    "ax/core",
    "ax/exception"
], function (
    klass,
    IDrmAgent,
    promise,
    util,
    console,
    core,
    exception
) {
    "use strict";
    return klass.create([IDrmAgent], {}, {
        /**
         * Counter for the DRM request
         * @protected
         * @type {Number}
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        _requestCounter: 0,
        /**
         * The license acquisition defer object
         * @type {ax/promise~PromiseDefer}
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        __laDeferred: undefined,
        /**
         * The last license acquisition promise object in the queue
         * @type {Boolean}
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        __queuedPromise: undefined,
        /**
         * Flag to indicate if an acquisition is processing
         * @private
         * @type {Boolean}
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        __acquiringLicense: false,
        /**
         * Process queue
         * @private
         * @type {Promise[]}
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        __queue: [],
        /**
         * Flag to indicate if the queue is being processed
         * @private
         * @type {Boolean}
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        __processingQueue: false,
        /**
         * Set the drm messenger to send out the license request
         * @public
         * @param {ax/device/interface/DrmMessenger} messenger the target drm messenger
         * @method
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        setDrmMessenger: function (messenger) {
            this.__messenger = messenger;
        },
        /**
         * Process the request queue.
         * @public
         * @method
         * @param {String} url the media url
         * @param {Object} opts the option configuration
         * @param {String} [opts.drmUrl] Set the DRM license url
         * @param {String} [opts.customData]  Set the custom data
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        sendLicense: function (url, opts) {
            opts = opts || {};
            var deferred = promise.defer(),
                laUrl = opts.drmUrl,
                customData = opts.customData,
                process = util.bind(function (_defer) {
                    // reset counter for each process
                    this._requestCounter = 0;

                    this.__laDeferred = _defer;

                    var _promise = _defer.promise;

                    this._sendDRMMessages(laUrl, customData);

                    // the license acquisition resolved, resolve the individual promise
                    return _promise.then(function (msg) {
                        _defer.resolve(msg);
                    }).fail(function (reason) {
                        _defer.reject(reason);
                    });
                }, this, deferred);

            // only process the request if none is working AND the queue is not empty
            // otherwise push to the queue
            if (!this.__acquiringLicense && this.__queue.length === 0) {
                this.__acquiringLicense = true;
                process(deferred);
            } else {
                this.__queue.push(process);
            }

            return deferred.promise;

        },
        /**
         * Process the request queue.
         * @protected
         * @method
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        _processQueue: function () {
            if (this.__processingQueue) {
                return;
            }

            this.__processingQueue = true;
            var fn = this.__queue.shift();
            if (fn) {
                fn().then(util.bind(function () {
                    this.__processingQueue = false;
                    this._processQueue();
                }, this));
            } else {
                this.__processingQueue = false;
            }
        },
        /**
         * Create the custom data message into the XML
         * @protected
         * @method
         * @param {String} custom data  the custom data
         * @returns {String} the xml for license acquisition
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        _createCustomDataMessage: function (customData) {
            /*jshint quotmark: false */
            return '<?xml version="1.0" encoding="utf-8"?>' +
                '<PlayReadyInitiator xmlns="http://schemas.microsoft.com/DRM/2007/03/protocols/">' +
                '<SetCustomData><CustomData> ' + customData + ' </CustomData></SetCustomData>' +
                '</PlayReadyInitiator>';
            /*jshint quotmark: true */
        },
        /**
         * Create the license url message into the XML
         * @protected
         * @method
         * @param {String} laUrl The url of the license acquisition server
         * @returns {String} the xml for license acquisition
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        _createLicenseUrlMessage: function (laUrl) {
            /*jshint quotmark: false */
            return '<?xml version="1.0" encoding="utf-8"?>' +
                '<PlayReadyInitiator xmlns="http://schemas.microsoft.com/DRM/2007/03/protocols/">' +
                '<LicenseServerUriOverride>' +
                '<LA_URL>' + laUrl + '</LA_URL>' +
                '</LicenseServerUriOverride>' +
                '</PlayReadyInitiator>';
            /*jshint quotmark: true */
        },
        /**
         * The actual function to send DRM messages.
         * @protected
         * @method
         * @param {String} laUrl The url of the license acquisition server
         * @param {String} [customData] The custom data
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        _sendDRMMessages: function (laUrl, customData) {
            try {
                // license URL
                if (util.isString(laUrl)) {
                    this._requestCounter++;
                    this.__messenger.sendMessage(this._createLicenseUrlMessage(laUrl), util.bind(this._onResult, this), util.bind(this._onError, this));
                    console.info("[PlayreadyDrmAgent] sendMessage (License Acquisition URL): " + laUrl);
                }

                // custom Data
                if (util.isString(customData)) {
                    this._requestCounter++;
                    this.__messenger.sendMessage(this._createCustomDataMessage(customData), util.bind(this._onResult, this), util.bind(this._onError, this));
                    console.info("[PlayreadyDrmAgent] sendMessage (CustomData): " + customData);
                }
            } catch (e) {
                console.warn("[PlayreadyDrmAgent] ERROR: sendDRMMessage (" + e + ")");
                this._requestCounter = 0; // fail, reset counter
                this.__acquiringLicense = false;
                this.__laDeferred.reject(core.createException(exception.INTERNAL, "sendDRMMessage failed: " + e));
            }
        },

        /**
         * Callback to be called by the native drm agent when sendMessage() is finished.
         * @protected
         * @method
         * @param {Number} resultCode Integer result code
         * @param {String} resultMsg the result message
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        _onResult: function (resultCode, resultMsg) {
            this._requestCounter--;

            console.info("[PlayreadyDrmAgent] onResult: resultCode:" + resultCode + " - " + resultMsg + ")");
            console.debug("[PlayreadyDrmAgent] counter down to " + this._requestCounter);

            if (this._requestCounter === 0) {
                this._onLicenseAcquired();
            }
        },

        /**
         * Callback to be called when the license acquisition is done.
         * This function will resolve the license acquisition defer.
         * @protected
         * @method
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        _onLicenseAcquired: function () {
            console.info("[PlayreadyDrmAgent] license acquired");
            this.__laDeferred.resolve(true);
            this.__acquiringLicense = false;
            this._processQueue();
        },

        /**
         * Callback to be called by the native drm agent when license acquisition fails.
         * This function will reject the license acquisition defer.
         * @protected
         * @method
         * @param {Number} errorCode Error state code
         * @param {String} errorText Error text
         * @memberof ax/device/shared/PlayreadyDrmAgent#
         */
        _onError: function (errorCode, errorText) {
            console.warn("[PlayreadyDrmAgent] onError ( errorCode:" + errorCode + " - " + errorText + ")");
            this.__laDeferred.reject(core.createException(errorCode, errorText));
            this.__acquiringLicense = false;
            this._processQueue();
        }
    });
});
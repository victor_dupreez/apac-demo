/**
 * The abstract player provides base function of some optional features.
 * For example, speed is not supported in all the player. The abstract player will consist of those optional function.
 * @class ax/device/AbstractPlayer
 * @augments ax/device/interface/Player
 */
define("ax/device/AbstractPlayer", ["ax/class", "ax/console", "ax/device/interface/Player"], function (klass, console, IPlayer) {
    "use strict";
    return klass.createAbstract([IPlayer], {
        /**
         * The playback states of the player.
         * @deprecated  
         * @enum {String}
         * @memberof ax/device/AbstractPlayer
         */
        PLAYBACK_STATE: {
            /**
             * The playback is playing, this also includes skipping, speeding, bufferring.
             */
            PLAYING: "playing",
            /**
             * The playback is paused, this also includes bufferring.
             */
            PAUSED: "paused",
            /**
             * The playback is stopped, or not started yet.
             */
            STOPPED: "stopped"
        }

    }, {
        /**
         * The id of the player
         * @name __id
         * @private
         * @memberof ax/device/AbstractPlayer#
         */
        __id: null,
        /**
         * Speed up/down the media playback
         * @method
         * @param {Number} speed the playback speed to set
         * @memberof ax/device/AbstractPlayer#
         */
        speed: function (speed) {
            console.warn("Speeding " + speed + " is not supported in the player");
        },
        /**
         * Get the playback speed
         * @method
         * @return {Number} the playback speed
         * @memberof ax/device/AbstractPlayer#
         */
        getPlaybackSpeed: function () {
            console.warn("no play back speed in the player");
            return 1;
        },
        /**
         * Get the media bitrates
         * @method
         * @return {ax/device/interface/Player~MediaBitrates} current bitrate and available bitrates
         * @memberof ax/device/AbstractPlayer#
         */
        getBitrates: function () {
            console.debug("no bitrate provided");
            return null;
        },
        /**
         * Get back the path
         * @method
         * @public
         * @return {String} the path of the player
         * @memberof ax/device/AbstractPlayer#
         */
        getId: function () {
            return this.__id;
        },
        /**
         * set the id of the player
         * @method
         * @public
         * @param {String} id the path of the player
         * @memberof ax/device/AbstractPlayer#
         */
        setId: function (id) {
            this.__id = id;
        }
    });
});
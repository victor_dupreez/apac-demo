/**
 * Media class. Loads and plays media (by calling underlying media player).
 *
 * This class is designed as a singleton, developer should use {@link ax/device/Media.singleton|singleton} to obtain the instance.
 * Creating instance using the _new_ keyword is prohibited.
 *
 * @class ax/device/Media
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @author Andy Hui <andy.hui@accedo.tv>
 * @fires ax/device/Media#EVT_STATE_CHANGED
 * @fires ax/device/Media#EVT_BUFFERING_PROGRESS
 * @fires ax/device/Media#EVT_TIME_UPDATE
 * @fires ax/device/Media#EVT_FINISHED
 * @fires ax/device/Media#EVT_ERROR
 * @fires ax/device/Media#EVT_PUSHED
 * @fires ax/device/Media#EVT_POPPED
 */
define("ax/device/Media", [
    "ax/class",
    "ax/EventDispatcher",
    "ax/device/interface/Suspendable",
    "ax/device/interface/Preloadable",
    "ax/device/FallbackSuspendedPlayback",
    "ax/device/FallbackPreloadedPlayback",
    "ax/device/playerRegistry",
    "ax/core",
    "ax/util",
    "ax/console",
    "ax/config",
    "require",
    "ax/exception",
    "ax/promise",
    "ax/device/AbstractPlayer",
    "ax/device/interface/Player",
    "ax/device/MediaSubtitle",
    "css!./css/Media"
], function (
    klass,
    EvtDisp,
    Suspendable,
    Preloadable,
    FallbackSuspendedPlayback,
    FallbackPreloadedPlayback,
    registry,
    core,
    util,
    console,
    config,
    require,
    exception,
    promise,
    AbstractPlayer,
    IPlayer,
    MediaSubtitle
) {
    "use strict";

    var Media,
        instance;

    var PLAYBACK_STATE = IPlayer.PLAYBACK_STATE;
    var PLAYBACK_ERRORS = IPlayer.PLAYBACK_ERRORS;

    Media = klass.create(EvtDisp, {
        /**
         * Get the singleton instance of this class.
         * @method
         * @static
         * @returns {ax/device/Media} The singleton
         * @memberOf ax/device/Media
         */
        singleton: function () {
            if (!instance) {
                instance = new Media();
            }

            return instance;
        }
    }, {
        /**
         * PlayState of the video Object
         * @public
         * @constant
         * @memberof ax/device/Media#
         */
        STOPPED: 0,
        /**
         * PlayState of the video Object
         * @public
         * @constant
         * @memberof ax/device/Media#
         */
        PLAYING: 1,
        /**
         * PlayState of the video Object
         * @public
         * @constant
         * @memberof ax/device/Media#
         */
        PAUSED: 2,
        /**
         * PlayState of the video Object
         * @public
         * @constant
         * @memberof ax/device/Media#
         */
        SKIPPING: 3,
        /**
         * PlayState of the video Object
         * @public
         * @constant
         * @memberof ax/device/Media#
         */
        SPEEDING: 4,
        /**
         * PlayState of the video Object
         * @public
         * @constant
         * @memberof ax/device/Media#
         */
        BUFFERING: 5,
        /**
         * PlayState of the video Object
         * @public
         * @constant
         * @memberof ax/device/Media#
         */
        CONNECTING: 6,
        /**
         * Event: playState change
         *
         * @event EVT_STATE_CHANGED
         * @memberof ax/device/Media#
         */
        EVT_STATE_CHANGED: "media:state-change",
        /**
         * Event: buffering progress
         *
         * @event EVT_BUFFERING_PROGRESS
         * @memberof ax/device/Media#
         */
        EVT_BUFFERING_PROGRESS: "media:buffering-progress",
        /**
         * Event: time update
         *
         * @event EVT_TIME_UPDATE
         * @memberof ax/device/Media#
         */
        EVT_TIME_UPDATE: "media:time-update",
        /**
         * Event: video finish
         *
         * @event EVT_FINISHED
         * @memberof ax/device/Media#
         */
        EVT_FINISHED: "media:finish",
        /**
         * Event: video error
         *
         * @event EVT_ERROR
         * @memberof ax/device/Media#
         */
        EVT_ERROR: "media:error",
        /**
         * Event: playback state pushed to stack
         *
         * @event EVT_PUSHED
         * @property {String} url The media url
         * @property {Object} opts The media opts
         * @property {Number} pos The suspended position
         * @property {ax/device/Media#State} state The state just before suspension
         * @property {ax/device/interface/Player} player The corresponding player
         * @memberof ax/device/Media#
         */
        EVT_PUSHED: "media:pushed",
        /**
         * Event: playback state popped from stack
         *
         * @event EVT_POPPED
         * @property {String} url The media url
         * @property {Object} opts The media opts
         * @property {Number} pos The suspended position
         * @property {ax/device/Media#State} state The state just before suspension
         * @property {ax/device/interface/Player} player The corresponding player
         * @memberof ax/device/Media#
         */
        EVT_POPPED: "media:popped",
        /**
         * Playback state
         * @private
         * @memberof ax/device/Media#
         */
        __state: 0,
        /**
         * Previous playback state
         * @private
         * @memberof ax/device/Media#
         */
        __prevState: 0,
        /**
         * last received non skip state
         * It will store the latest non skip state which will be restored to after skipping
         * @private
         * @memberof ax/device/Media#
         */
        __lastReceivedNonSkipState: 0,
        /**
         * A flag to indicate whether the media is currently under an "error state".
         * This is introduced because "error state" is not one of the valid state in the lifecycle,
         * but in certain scenario we need to know whether error is appearing.
         * @private
         * @memberof ax/device/Media#
         */
        __hasError: false,
        /**
         * The player implementation
         * @private
         * @memberof ax/device/Media#
         */
        __player: null,
        /**
         * The media URL
         * @private
         * @memberof ax/device/Media#
         */
        __url: "",
        /**
         * The media options
         * @private
         * @memberof ax/device/Media#
         */
        __opts: null,
        /**
         * The media type format
         * @private
         * @memberof ax/device/Media#
         */
        __type: null,
        /**
         * The DRM being used
         * @private
         * @memberof ax/device/Media#
         */
        __drm: null,
        /**
         * The parent Node which store the media
         * @private
         * @memberof ax/device/Media#
         */
        __parentNode: null,
        /**
         * Progressive skip delay handle
         * @private
         * @memberof ax/device/Media#
         */
        __skipDelayHandle: null,
        /**
         * Progressive skip accumulated arrival time
         * @private
         * @memberof ax/device/Media#
         */
        __accumulatedArrivalTime: null,
        /**
         * Progressive skip accumulated skip step
         * @private
         * @memberof ax/device/Media#
         */
        __accumulatedStep: null,
        /**
         * The time returned from on time update event.
         * @private
         * @memberof ax/device/Media#
         */
        __updatedTime: null,
        /**
         * The current Multi Audio Track Strategy linking to the player
         * @private
         * @memberof ax/device/Media#
         */
        __multiAudioStgy: null,

        /**
         * The suspended playback stack. Works with the Suspendable interface.
         * @private
         * @memberof ax/device/Media#
         */
        __playbackStack: [],

        /**
         * A flag to indicate if push() is happening
         * @private
         * @memberof ax/device/Media#
         */
        __pushing: false,

        /**
         * A flag to indicate if pop() is happening
         * @private
         * @memberof ax/device/Media#
         */
        __popping: false,

        /**
         * A event dispatcher used internally to notify the playback state change
         * @private
         * @memberof ax/device/Media#
         */
        __stateMachine: null,

        /**
         * An object holding all un-disposed preloaded playback
         * @private
         * @memberof ax/device/Media#
         */
        __preloaded: {},

        /**
         * The list of working players in the background.
         * @private
         * @memberof ax/device/Media#
         */
        __backgroundPlayerList: [],

        /**
         * The subtitle module instance
         * @private
         * @memberof ax/device/Media#
         */
        __subtitleModule: null,

        /**
         * The saved strategies for the preloadable
         * @private
         * @memberof ax/device/Media#
         */
        __savedStrategies: {},

        /**
         * The saved strategies for the suspendable
         * @private
         * @memberof ax/device/Media#
         */
        __suspendedStrategies: [],
        /**
         * Initializes this module
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        init: function () {
            this._super();
            this.__stateMachine = new EvtDisp();
        },
        /**
         * De-initializes this module
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        deinit: function () {
            if (this.__player) {
                this.__player.reset();
            }
            this.__resetStgies();
            // as url is an identifier for reload or not (see this.load), therefore reset
            this.__url = "";
            this.__opts = null;
            this._super();
        },
        /**
         * De-initializes this module
         * @method
         * @protected
         * @param {String} url the URL address of the media
         * @return {String} ext extension of the media
         * @memberof ax/device/Media#
         */
        _extractMediaContainer: function (url) {
            if (/.mp4\?|\.mp4$/i.test(url)) {
                return "mp4";
            }

            if (/.wmv\?|\.wmv$|.wma\?|\.wma$|.asf\?|\.asf$/i.test(url)) {
                return "asf";
            }

            if (/.m3u8\?|\.m3u8$/i.test(url)) {
                return "hls";
            }

            if (/.mp3\?|\.mp3$/i.test(url)) {
                return "mp3";
            }

            return "*";
        },
        /**
         * Loads the specified media. The following are general options for setting the video. Some players may have special setting like bitrate.
         * Please refer to the specific player and read the properties.
         * @method
         * @param {String} url the URL address of the media
         * @param {Object} [opts] the options for loading this media
         * @param {String} [opts.type] the media type format like "mp4","hls"
         * @param {String} [opts.drm] DRM to be used e.g "playready", "wmdrm", "widevine", "verimatrix"
         * @param {Object} [opts.player] the player to play the media
         * @param {String} [opts.drmUrl] (Widevine || Playready || Verimatrix || WMDRM) Set the DRM license url
         * @param {String} [opts.serviceProvider] (Verimatrix) Set the Verimatrix DRM Service provider Name
         * @param {String} [opts.iptvServerIp] (Verimatrix) Set the Verimatrix DRM IPTV Client server IP address
         * @param {String} [opts.webServerIp] (Verimatrix) Set the Verimatrix DRM  WebClient server IP address 
         * @param {String} [opts.customData] (Playready) Set the Playready DRM Custome Data if the platform is supported
         * @param {String} [opts.portal] (Widevine) Set the portal
         * @param {String} [opts.userData] (Widevine) Set the user data
         * @param {String} [opts.deviceId] (Widevine) Set the device id
         * @param {String} [opts.deviceType] (Widevine) Set the device type
         * @param {String} [opts.authCookiesUrl] (WMDRM) Set the url to get the authentication cookies (e.g LG and Samsung)
         * @param {String} [opts.cadUrl] (WMDRM) Set the cad url to generate the xml, then player will get the authentication (e.g Philips)
         * @param {String} [opts.swfUrl] (Flash) set the flash swf url
         * @param {String} [opts.streamName] (Flash) the streamName
         * @param {Boolean} [opts.liveStreaming] (Flash) to  indicate if it is liveStreaming. If it is live streaming, no time update
         * @param {Boolean} [opts.use3d] true if to use 3D
         * @param {String} [opts.type3d] the type of 3D
         * @param {Boolean} [opts.forceReload] to forceLoad even if the url is the same
         * @param {String} [opts.startBitrate] (HLS || HAS) To set the initial bitrate e.g 2600000
         * @param {Number} [opts.bitratesFrom] (HLS || HAS) the lowest bitrate setting
         * @param {Number} [opts.bitratesTo] (HLS || HAS) the highest bitrate setting
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} The specified player cannot play the specified media
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} None of the registered player can play the specified media
         * @memberof ax/device/Media#
         */
        load: function (url, opts) {
            if (this.__popping) {
                return;
            }

            this._doLoad(url, opts);
        },

        _doLoad: function (url, opts) {
            var player,
                prepareStgies = util.bind(function () {
                    this.__resetStgies();

                    //prepare again for strategies
                    this.__multiAudioStgy = this.__prepareAudioTrack(this.__player);
                    this.__subtitleModule = this.__prepareSubtitle(this.__player, opts);
                }, this);

            opts = opts || {};
            opts.type = opts.type || this._extractMediaContainer(url);
            opts.drm = opts.drm || undefined;


            // get the appropriate player based on the type & drm requirements
            try {
                player = this.__getPlayer(opts);
            } catch (ex) {
                this._onError(PLAYBACK_ERRORS.RENDER.UNSUPPORTED, "Exception caught when trying to load the media with type = " + opts.type + ", drm = " + opts.drm + ":\n" + ex.message);
                if (this.__player) {
                    // only reset the player if no background job is being done
                    if (this.__backgroundPlayerList.indexOf(this.__player) < 0) {
                        this.__player.reset();
                    }
                    this.__player = null;
                }

                throw ex;
            }

            // no need to load again if it is already loaded.
            // unless 1) the application force it, or 2) there is an error in previous trial
            if (this.__url === url && player === this.__player && !(opts.forceReload || this.__hasError)) {
                prepareStgies();
                return;
            }

            // reset the error flag for a fresh load
            this.__hasError = false;

            // set after url and player are confirmed to be different
            this.__url = url;

            // swap the player
            if (this.__player !== player) {

                // only reset the player if no background job is being done
                if (this.__player && this.__backgroundPlayerList.indexOf(this.__player) < 0) {
                    this.__player.reset();
                }

                this.__player = player;
            }

            this.__player.prepare(opts);
            prepareStgies();

            this.__type = opts.type;
            this.__drm = opts.drm;
            this.__url = url;
            this.__opts = opts;

            this.__player.load(url, opts);

        },

        /**
         * To prepare the subtitle strategy.
         * @method __prepareSubtitle
         * @param {ax/device/interface/Player} player the player
         * @param {Object} opts the load options
         * @param {ax/device/interface/Preloaded.PreloadedPlayback} [preloaded] item from the player, it is just for preload to get information for the current preloadable item
         * @returns {ax/device/Subtitle} the subtitle instance
         * @private
         * @memberof ax/device/Media#
         */
        __prepareSubtitle: function (player, opts, preloaded) {
            var playerId, subtitleObj, subtitleList, mSubtitle;

            playerId = player.getId();

            subtitleList = config.get("device.subtitleStgy", null);

            if (!subtitleList) {
                // no subtitle list and no need to prepare the subtitle
                return null;
            }

            subtitleObj = subtitleList[playerId];

            //do nothing when no subtitle setting and subtitle object for internal or external
            if (!subtitleObj || !(subtitleObj.internal || subtitleObj.external)) {
                return null;
            }

            try {
                mSubtitle = new MediaSubtitle(subtitleObj, player, preloaded);
                mSubtitle.prepare(opts.subtitle);
                console.info("[XDK] create the subtitle");
            } catch (e) {
                console.warn("[XDK] Fail to prepare subtitle");
            }

            return mSubtitle;
        },
        /**
         * To prepare the audio track strategy.
         * @param {ax/device/interface/Player} the player
         * @param {ax/device/interface/Preloadable} [preloaded] item from the player, it is just for preload to get information for the current preloadable item
         * @returns {ax/device/interface/MultipleAudioTrackStgy} the multiple audio track strategy
         * @method
         * @private
         * @memberof ax/device/Media#
         */
        __prepareAudioTrack: function (player, preloaded) {
            var playerId, multiAudioTrackList, multiAudioTrackId, MultiAudioTrackStgy, mMultiAudioStgy;

            //after getting player, find the relative multi audio track
            playerId = player.getId();

            multiAudioTrackList = config.get("device.multiAudioTrackStgy", null);

            if (!multiAudioTrackList) {
                // no multiple audio track
                return null;
            }
            multiAudioTrackId = multiAudioTrackList[playerId];

            //do nothing when there is no multi audio stgy for the player
            if (!multiAudioTrackList || !multiAudioTrackId) {
                return null;
            }

            console.info("[XDK] Multi Audio Track exist " + multiAudioTrackId);
            //get back the strategy
            MultiAudioTrackStgy = require(multiAudioTrackId);

            //link the multi track and pass the playerInstance to the multiAudioTrack strategy since it may need the information about the player in the strategy.
            try {
                mMultiAudioStgy = new MultiAudioTrackStgy(player);
                console.info("[XDK] Linked the player and multi audio track");
            } catch (e) {
                console.warn("[XDK] Fail to load the audio track : " + multiAudioTrackId + " : " + e.name);
            }

            return mMultiAudioStgy;
        },
        /**
         * Start the media playback. Play function will be blocked when the state is buffering, skipping or connecting.
         * @method
         * @param {Object} [opts] the options for starting playback
         * @param {Number} [opts.sec] the playback start time
         * @memberof ax/device/Media#
         */
        play: function (opts) {
            if (this.__popping) {
                return;
            }

            this._doPlay(opts);
        },

        _doPlay: function (opts) {
            if (this.isState(this.SKIPPING) || this.isState(this.BUFFERING) || this.isState(this.CONNECTING)) {
                return;
            }
            if (!this.__player) {
                console.warn("Player is not ready, please call load() first to load up a player implementation.");
                return;
            }

            //if it is number then convert into the object
            if (util.isNumber(opts)) {
                opts = {
                    sec: opts
                };
            } else if (!opts) {
                opts = {};
            }

            this.__player.play(opts);
        },

        /**
         * Stop the media playback
         * @method
         * @memberof ax/device/Media#
         */
        stop: function () {
            if (this.isState(this.STOPPED) || this.__popping) {
                return;
            }

            this._clearSkipDelayData();

            if (this.__player) {
                this.__player.stop();
            }
        },
        /**
         * Pause the media playback
         * @method
         * @memberof ax/device/Media#
         */
        pause: function () {
            if (this.__popping) {
                return;
            }

            this._doPause();
        },

        _doPause: function () {
            if (!this.isState(this.PLAYING) && !this.isState(this.SPEEDING)) {
                return;
            }
            this.__player.pause();
        },

        /**
         * Resume the media playback
         * @method
         * @memberof ax/device/Media#
         */
        resume: function () {
            if (!this.isState(this.PAUSED) || this.__popping) {
                return;
            }
            this.__player.resume();
        },
        /**
         * During media playback, seek to certain position
         * @method
         * @param {Number} sec the position to seek to in seconds
         * @memberof ax/device/Media#
         */
        seek: function (sec) {
            if (this.isState(this.STOPPED) || this.__popping) {
                return;
            }
            if (this.isState(this.SKIPPING)) {
                this._clearSkipDelayData();
                this._changeState(this.__prevState);
            }
            //XDK-1475 To limit the seek time interval. Only between 0 and its duration is allowed.
            if (sec > this.getDuration()) {
                console.info("[Media] Seek time " + sec + " is larger than the duration " + this.getDuration() + ", now it will seek the duration time");
                sec = this.getDuration();
            } else if (sec < 0) {
                console.info("[Media] Seek time " + sec + " is smaller than 0 " + this.getDuration() + ", now it will seek 0s");
                sec = 0;
            }
            this.__player.seek(sec);
        },
        /**
         * Clears the progressive skip timeout and the data related to progressive skipping.
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _clearSkipDelayData: function () {
            this._clearSkipDelay();
            this.__accumulatedArrivalTime = null;
            this.__accumulatedStep = null;
        },
        /**
         * Clears the timeout of progressive skipping.
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _clearSkipDelay: function () {
            if (this.__skipDelayHandle) {
                util.clearDelay(this.__skipDelayHandle);
                this.__skipDelayHandle = null;
            }
        },
        /**
         * Skip the playback forward/backward for certain seconds.
         * There are 2 kinds of skip operation: simple and progressive.
         * For the simple skip, the operation will be carried out whenever the command is called.
         * For the progressive skip, it will be carried after a short delay. Any further progressive skips within this delay will be carried at once.
         * 
         * Please note that any simple skip comes during the progressive skip delay will be ignored.
         *
         * @method
         * @param {Object} param parameter for the skipping
         * @param {Number} param.sec number of seconds to skip (10 by default)
         * @param {Boolean} [param.progressive] whether it is progressive skipping or not
         * @param {Number} [param.multipleFactor] for progressive, multiple of increase in jump distance for each skip
         * @param {Number} [param.delaySec] for progressive, seconds of delay before the skip is fired
         * @param {Number} [param.progressiveCap] for progressive, maximum amount of second in each increase
         * @memberof ax/device/Media#
         */
        skip: function (param) {
            if (this.__state === this.STOPPED || this.__popping) {
                return;
            }

            if (util.isNumber(param)) {
                param = {
                    sec: param
                };
            }
            param = param || {};
            param.sec = param.sec || 10;

            // for progressive, multiple of increase in jump distance for each detection
            var multipleFactor = param.multipleFactor || 1;
            // for progressive, seconds of delay before the skip is fired
            var delaySec = param.delaySec || 2;
            // for progressive, maximum amount of second in each increase
            var progressiveCap = param.progressiveCap || 300;
            // the target resulting time after skip
            var targetTime;
            // the actual skip action
            var __doSkip;

            if (!param.progressive || this.__player._noProgressive) {
                // issuing immediate skip during progressive will mess the state up
                if (this.__accumulatedArrivalTime) {
                    console.warn("Simple skip is forbidden during progressive skip. Abort the skip operation.");
                    return;
                }

                targetTime = this.getCurTime() + param.sec;
                if (targetTime > this.getDuration()) {
                    targetTime = this.getDuration();
                } else if (targetTime < 0) {
                    targetTime = 0;
                }
                this._clearSkipDelayData();
                this.__player.skip(targetTime - this.getCurTime());
                return;
            }

            this._onSkipping();

            if (multipleFactor < 1) {
                multipleFactor = 1; //couldn't be smaller and smaller, right?
            }

            // make sure arrival time is set
            if (!util.isNumber(this.__accumulatedArrivalTime)) {
                this.__accumulatedArrivalTime = this.getCurTime(true);
            }
            // get the skip step ready
            if (!this.__accumulatedStep || this.__accumulatedStep * param.sec < 0) {
                this.__accumulatedStep = param.sec;
            } else {
                this.__accumulatedStep = Math.round(this.__accumulatedStep * multipleFactor);
            }
            // cap the skip step
            if (this.__accumulatedStep > progressiveCap) {
                this.__accumulatedStep = progressiveCap;
            } else if (this.__accumulatedStep < -progressiveCap) {
                this.__accumulatedStep = -progressiveCap;
            }

            // if requesting a huge skip, forget about the progressive step
            if (param.sec >= progressiveCap || param.sec <= -progressiveCap) {
                this.__accumulatedArrivalTime += param.sec;
            } else { // skip a step
                this.__accumulatedArrivalTime += this.__accumulatedStep;
            }
            targetTime = this.__accumulatedArrivalTime;

            // do not go out of boundary
            if (targetTime > this.getDuration()) {
                targetTime = this.getDuration();
            } else if (targetTime < 0) {
                targetTime = 0;
            }

            __doSkip = util.bind(function () {
                this.__player.skip(targetTime - this.__player.getCurTime());
                this._clearSkipDelayData();
                this._changeState(this.__lastReceivedNonSkipState);
                console.info("[XDK] delayed skip action committed.");
            }, this);

            this._clearSkipDelay();
            this.__skipDelayHandle = core.getGuid();
            util.delay(delaySec, this.__skipDelayHandle).then(__doSkip);
            this._onTimeUpdate(targetTime, true);
        },
        /**
         * Speed up/down the media playback
         * @method
         * @param {Number} speed the playback speed to set
         * @memberof ax/device/Media#
         */
        speed: function (speed) {
            if (!this.isState(this.PLAYING) && !this.isState(this.SPEEDING) && !this.isState(this.PAUSED) || this.__popping) {
                return;
            }
            this.__player.speed(speed);
        },
        /**
         * Get the media playback speed
         * @method
         * @return {Number} the playback speed
         * @memberof ax/device/Media#
         */
        getPlaybackSpeed: function () {
            if (this.__state === this.STOPPED) {
                return 0;
            }
            if (this.__state === this.PLAYING) {
                return 0;
            }
            return this.__player.getPlaybackSpeed();
        },
        /**
         * Set the video window size.
         * @method
         * @param {Object} param window size parameter
         * @param {Integer} param.top window top
         * @param {Integer} param.left window left
         * @param {Integer} param.width window width
         * @param {Integer} param.height window height
         * @memberof ax/device/Media#
         */
        setWindowSize: function (param) {
            if (!this.__player) {
                console.warn("Player is not ready, please call load() first to load up a player implementation.");
                return;
            }
            this.__player.setWindowSize(param);
        },
        /**
         * Set full screen
         * @method
         * @memberof ax/device/Media#
         */
        setFullscreen: function () {
            if (!this.__player) {
                console.warn("Player is not ready, please call load() first to load up a player implementation.");
                return;
            }
            this.__player.setFullscreen();
        },
        /**
         * Get the media playback speed
         * @method
         * @return {ax/device/interface/Player~PlayerCapabilites} current playback bitrate
         * @memberof ax/device/Media#
         */
        getBitrates: function () {
            if (this.__state === this.STOPPED) {
                return null;
            }
            return this.__player.getBitrates();
        },
        /**
         * Get the media playback state
         * @method
         * @return {Number} the current playback state
         * @memberof ax/device/Media#
         */
        getState: function () {
            return this.__state;
        },
        /**
         * Check if the media playback state is the provided one
         * @method
         * @return {Boolean} true if the same
         * @memberof ax/device/Media#
         */
        isState: function (state) {
            return state === this.__state;
        },
        /**
         * Get the current playback time
         * @method
         * @return {Number} the current playback time
         * @memberof ax/device/Media#
         */
        getCurTime: function () {
            if (this.__state === this.STOPPED) {
                return 0;
            }
            return this.__player.getCurTime();
        },
        /**
         * Get the total playback time
         * @method
         * @return {Number} the total playback time
         * @memberof ax/device/Media#
         */
        getDuration: function () {
            if (!this.__player) {
                console.warn("Player is not ready, please call load() first to load up a player implementation.");
                return 0;
            }
            return this.__player.getDuration();
        },

        /**
         * Suspends the current playback and puts the current playback states to the stack for later resumption.
         *
         * @method
         * @memberof ax/device/Media#
         */
        push: function () {
            var suspendedPlayback;

            this.__pushing = true;
            this._clearSkipDelayData();

            var state = this.getState();
            var pState;

            switch (state) {
            case this.STOPPED:
                pState = PLAYBACK_STATE.STOPPED;
                break;
            case this.PAUSED:
                pState = PLAYBACK_STATE.PAUSED;
                break;
            default:
                pState = PLAYBACK_STATE.PLAYING;
                state = this.PLAYING;
                break;
            }

            var evtObject = {
                url: this.getUrl(),
                opts: this.getOpts(),
                pos: this.getCurTime(),
                state: state,
                player: this.__player
            };

            if (klass.hasImpl(this.__player.constructor, Suspendable)) {
                suspendedPlayback = this.__player.suspend();

                if (this.__backgroundPlayerList.indexOf(this.__player) === -1) {
                    this.__backgroundPlayerList.push(this.__player);
                }
            } else {
                suspendedPlayback = new FallbackSuspendedPlayback(this, this.__player, {
                    url: evtObject.url,
                    opts: evtObject.opts,
                    pos: evtObject.pos,
                    state: pState
                });

                this.__player.stop();
            }

            suspendedPlayback.__evtObject = evtObject;
            this.__playbackStack.push(suspendedPlayback);

            this.__suspendedStrategies.push({
                "audioTrack": this.__multiAudioStgy,
                "subtitleModule": this.__subtitleModule
            });

            // clear any states as if nothing happened
            this.__state = this.STOPPED;
            this.__url = undefined;
            this.__opts = undefined;
            this.__type = undefined;
            this.__drm = undefined;
            this.__player = undefined;

            this.__subtitleModule = undefined;
            this.__multiAudioStgy = undefined;

            this.__pushing = false;
            this.dispatchEvent(this.EVT_PUSHED, evtObject);
        },

        /**
         * Restores the playback to which at the peak of the suspended playback stack.
         * Will fire a {@link ax/device/Media#EVT_POPPED} event upon finish.
         *
         * @method
         * @throws {exception.ILLEGAL_STATE} No previous suspended playback
         * @memberof ax/device/Media#
         */
        pop: function () {
            var peak = this.__playbackStack.pop(),
                strategies = this.__suspendedStrategies.pop();

            if (!peak) {
                throw core.createException(exception.ILLEGAL_STATE, "There is no suspended playback state in the stack.");
            }

            this.__popping = true;

            if (this.__player) {
                this.__player.stop();
            }

            peak.restore(util.bind(function () {
                var media = peak.getMediaMeta();
                var evtObject = peak.__evtObject;

                this.__url = media.url;
                this.__opts = media.opts;
                this.__player = peak.getPlayer();
                this.__state = evtObject.state;

                this.__subtitleModule = strategies.subtitleModule;
                this.__multiAudioStgy = strategies.audioTrack;

                peak.dispose();

                this.__resetUnusedPlayers();

                this.__popping = false;
                this.dispatchEvent(this.EVT_POPPED, evtObject);
            }, this));
        },

        /**
         * Preload a media for lesser bufferring time.
         *
         * @method
         * @param {String} url The media url
         * @param {Object} [opts] The options for loading
         * @param {ax/device/interface/Player} [opts.player] The target player to do preloading
         * @param {String} [opts.type] the media type format like "mp4","hls"
         * @param {String} [opts.drm] DRM to be used e.g "playready", "wmdrm", "widevine"
         * @param {String} [opts.drmUrl] (Widevine || Playready || Verimatrix || WMDRM) Set the DRM license url
         * @param {String} [opts.customData] (Playready) Set the Playready DRM Custome Data if the platform is supported
         * @param {String} [opts.portal] (Widevine) Set the portal
         * @param {String} [opts.userData] (Widevine) Set the user data
         * @param {String} [opts.deviceId] (Widevine) Set the device id
         * @param {String} [opts.deviceType] (Widevine) Set the device type
         * @param {String} [opts.authCookiesUrl] (WMDRM) Set the url to get the authentication cookies (e.g LG and Samsung)
         * @param {String} [opts.cadUrl] (WMDRM) Set the cad url to generate the xml, then player will get the authentication (e.g Philips)
         * @param {String} [opts.swfUrl] (Flash) set the flash swf url
         * @param {String} [opts.streamName] (Flash) the streamName
         * @param {Boolean} [opts.liveStreaming] (Flash) to indicate if it is liveStreaming. If it is live streaming, no time update
         * @param {Boolean} [opts.use3d] true if to use 3D
         * @param {String} [opts.type3d] the type of 3D
         * @returns {String} The token for identifying the preloaded playback for playing
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} The specified player cannot play the specified media
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} None of the registered player can play the specified media
         * @memberof ax/device/Media#
         */
        preload: function (url, opts) {
            var preloaded;

            // clone the options
            opts = util.extend({}, opts || {}, true);
            opts.type = opts.type || this._extractMediaContainer(url);

            var player = this.__getPlayer(opts),
                token,
                strategies;

            if (klass.hasImpl(player.constructor, Preloadable)) {
                delete opts.player;

                player.prepare(opts);

                preloaded = player.preload(url, opts);

                if (this.__backgroundPlayerList.indexOf(player) === -1) {
                    this.__backgroundPlayerList.push(player);
                }
            } else {

                // player is not a Preloadable, provide fallback
                preloaded = new FallbackPreloadedPlayback(this, url, opts);

            }

            //prepare the strategies
            strategies = {
                "audioTrack": this.__prepareAudioTrack(player, preloaded),
                "subtitleModule": this.__prepareSubtitle(player, opts, preloaded)
            };

            token = this.__savePreloaded(preloaded);

            this.__savedStrategies[token] = strategies;

            return token;
        },

        /**
         * Play the preloaded media.
         *
         * @method
         * @param {String} token The token returned from {@link ax/device/Media#preload}
         * @param {Number} pos The position where the playback should begin, counted in seconds
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} No associated preloaded playback
         * @memberof ax/device/Media#
         */
        playPreloaded: function (token, pos) {
            this.stop();

            var preloaded = this.__getPreloadedByToken(token);

            var mediaMeta = preloaded.getMediaMeta();

            preloaded.play(pos);

            this.__url = mediaMeta.url;
            this.__opts = mediaMeta.opts;
            this.__player = preloaded.getPlayer();

            if (!this.__savedStrategies[token]) {
                this.__subtitleModule = null;
                this.__multiAudioStgy = null;
                return;
            }

            //since the subtitle and audiotrack prepare when play in the FallbackPreloaded, 
            //it will undergo another Media.load which will further call the player to prepare the object
            //it will then create another object and handle for the another subtitl creation.e
            //as a result, assign back the correct strategy for the token saved strategy from the current subtitle strategy and audio strategy

            //other case will directly use the saved strategy and mapped correctly due to no Media.load and player.prepare.
            if (preloaded instanceof FallbackPreloadedPlayback) {
                this.__savedStrategies[token].subtitleModule = this.__subtitleModule;
                this.__savedStrategies[token].audioTrack = this.__multiAudioStgy;
            } else {
                this.__subtitleModule = this.__savedStrategies[token].subtitleModule;
                this.__multiAudioStgy = this.__savedStrategies[token].audioTrack;
            }

        },
        /**
         * Dispose a preloaded object. A disposed preloaded object cannot be use any more.
         *
         * @method
         * @param {String} token The token returned from {@link ax/device/Media#preload}
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} No associated preloaded playback
         * @memberof ax/device/Media#
         */
        disposePreloaded: function (token) {
            var preloaded = this.__getPreloadedByToken(token);

            preloaded.dispose();

            this.__savedStrategies[token] = null;

            this.__deletePreloadedByToken(token);

            this.__resetUnusedPlayers();
        },

        /**
         * Returns the preloaded playback associated with the token.
         *
         * @method
         * @private
         * @param {String} token The token of the preloaded playback
         * @returns {ax/device/interface/Preloadable.Preloaded} The associated preloaded playback
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} No associated preloaded playback
         * @memberof ax/device/Media#
         */
        __getPreloadedByToken: function (token) {
            var preloaded = this.__preloaded[token];

            if (!preloaded) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "No associated preloaded playback.");
            }

            return preloaded;
        },

        /**
         * Saves the preloaded playback, returns a token for later retrieval.
         * Developer should not save any object twice!
         *
         * @method
         * @private
         * @param {ax/device/interface/Preloadable.Preloaded} preloaded The preloaded playback
         * @returns {String} The token associated to the preloaded playback
         * @memberof ax/device/Media#
         */
        __savePreloaded: function (preloaded) {
            var token = core.getGuid() + "";
            this.__preloaded[token] = preloaded;

            return token;
        },

        /**
         * Deletes the preloaded playback associated with the token.
         * No exception will be thrown if no preloaded playback is associating because it should be deleted anyway.
         *
         * @method
         * @private
         * @param {String} token The token of the preloaded playback
         * @memberof ax/device/Media#
         */
        __deletePreloadedByToken: function (token) {
            this.__preloaded[token] = null;
        },

        /**
         * Gets the appropricate player instance. Capabilities are ensured in this method.
         * If player is specified, the requirements are checked against the player.
         * If non is specified, the player will be retrieved from the player registry.
         * For convenience, the _opts_ argument from _load()_ could be used directly.
         *
         * @method
         * @private
         * @param {Object} opts The capabilities requirements
         * @param {String} [opts.type] The media type
         * @param {String} [opts.drm] The media drm
         * @param {ax/device/interface/Player} [opts.player] The player object to check against
         * @returns {ax/device/interface/Player} The player object that fulfills all requirements
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} The player does not fulfill the requirements
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} No registered player can fulfill the requirements
         * @memberof ax/device/Media#
         */
        __getPlayer: function (opts) {
            var player;

            if (opts.player) {
                player = opts.player;

                if (!this.__checkCapabilities(player, opts)) {
                    throw core.createException(exception.ILLEGAL_ARGUMENT,
                        "The specified player is not capable to play the media.");
                }

                return player;
            }

            player = registry.getPlayer(opts);

            if (!player) {
                throw core.createException(exception.ILLEGAL_ARGUMENT,
                    "None of the registered player can play this type of media.");
            }

            return player;
        },

        /**
         * Checks if the player could play the specific media type and drm.
         *
         * @method
         * @private
         * @param {ax/device/interface/Player} player The player to check against
         * @param {Object} opts The load options
         * @returns {Boolean} True if the player could play the media, false otherwise
         * @memberof ax/device/Media#
         */
        __checkCapabilities: function (player, opts) {
            var capabilities = player.getCapabilities();

            var type = opts.type;
            var drm = opts.drm;

            var typePass = !(type && capabilities.type.indexOf(type) === -1) || type === "*";
            var drmPass = !(drm && capabilities.drms.indexOf(drm) === -1);

            return typePass && drmPass;
        },

        /**
         * Resets the unused players that are sitting in background.
         *
         * @method
         * @private
         * @memberof ax/device/Media#
         */
        __resetUnusedPlayers: function () {
            for (var i = this.__backgroundPlayerList.length - 1; i >= 0; i--) {
                var player = this.__backgroundPlayerList[i];

                // if no preloaded and suspended playback associated with the player, then the player has nothing doing in background
                if ((!klass.hasImpl(player.constructor, Preloadable) || player.getPreloadedCount() === 0) &&
                    (!klass.hasImpl(player.constructor, Suspendable) || player.getSuspendedCount() === 0)) {
                    this.__backgroundPlayerList.splice(i, 1);

                    // but only reset the player if it is not in foreground
                    if (player !== this.__player) {
                        player.reset();
                    }
                }
            }
        },

        /**
         * Should be calld when player updates current time
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onTimeUpdate: function (time, isSkip) {
            if (this.isState(this.SKIPPING) && !isSkip) {
                return; // currently skipping, should not update time
            }
            this.__updatedTime = time;
            this.dispatchEvent(this.EVT_TIME_UPDATE, time);
        },
        /**
         * Should be calld when player starts connecting
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onConnecting: function () {
            this._changeState(this.CONNECTING);
        },
        /**
         * Should be calld when player starts playing
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onPlaying: function () {
            this._changeState(this.PLAYING);
        },
        /**
         * Should be calld when player paused
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onPause: function () {
            this._changeState(this.PAUSED);

        },
        /**
         * Should be calld when playback stopped
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onStopped: function () {
            this._changeState(this.STOPPED);
        },
        /**
         * Should be calld when progressive skipping has started, but skip action has not actually taken
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onSkipping: function () {
            this._changeState(this.SKIPPING);
        },
        /**
         * Should be calld when speeding has started
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onSpeeding: function () {
            this._changeState(this.SPEEDING);
        },
        /**
         * Should be calld when buffering starts
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onBufferingStart: function () {
            this._changeState(this.BUFFERING);
        },
        /**
         * Should be calld when buffering is progressing
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onBufferingProgress: function (data) {
            this.dispatchEvent(this.EVT_BUFFERING_PROGRESS, data);
        },
        /**
         * Should be calld when buffering has finished
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onBufferingFinish: function () {
            if (this.isState(this.STOPPED)) {
                return;
            }

            //when skipping, the state turned into buffering complete, the previous state should be playing.
            //so after skipping, it should back to play state instead of buffering.

            //if previous state of buffering is connecting, it will change into play instead of.
            if (this.__prevState === this.CONNECTING || this.__prevState === this.BUFFERING) {
                this._changeState(this.PLAYING);
                return;
            }

            console.debug("change back to prev state" + this.__prevState);
            this._changeState(this.__prevState);
        },
        /**
         * Should be calld when playback has finished
         * @method
         * @protected
         * @memberof ax/device/Media#
         */
        _onFinish: function () {
            this._changeState(this.STOPPED);
            this.dispatchEvent(this.EVT_FINISHED);
        },
        /**
         * Should be calld when playback encountered error
         * @method
         * @protected
         * @param {String} errorCode the error code
         * @param {String} errorMessage the error message
         * @memberof ax/device/Media#
         */
        _onError: function (errorType, errorData, originalData) {

            this.__hasError = true;

            if (this.__popping) {
                this.__popping = false;
            }

            this._changeState(this.STOPPED);

            var code, msg, type;

            var error = {
                errorType: "",
                errorMessage: "",
                errorCode: 0,
                errorData: null
            };

            if (util.isObject(errorType)) {
                //if errorType is an object, pre process the error object properties
                code = errorType.code;
                msg = errorType.desc;
                type = errorType.evt;
                //set the internal accedo errorCode as it's available
                error.errorCode = code;
            } else {
                //if string
                type = errorType;
            }

            if (util.isObject(errorData)) {
                //override the message, by the native error message spefified.
                msg = errorData.msg || errorType.desc;
            } else if (util.isString(errorData)) {
                msg = errorData;
            }

            error.errorType = type;
            error.errorMessage = msg;

            if (originalData) {
                error.errorData = originalData;
            }

            this.dispatchEvent(this.EVT_ERROR, error);

        },
        /**
         * Internal function to change playback state
         * @protected
         * @param {String} state internal state to be changed
         * @param {*} data data to be dispatched along with the event
         * @method
         * @memberof ax/device/Media#
         */
        _changeState: function (state, data) {
            if (this.__state === state) {
                console.debug("[XDK] Change State Fail as in the same state " + state);
                return;
            }

            //XDK-2039 Handling for the state change when skipping, store the last received non skip state which will be restored after skip
            if (state !== this.SKIPPING) {
                this.__lastReceivedNonSkipState = state;
            }

            //ignore any state change when performing progressive skip
            if (this.__skipDelayHandle !== null) {
                return;
            }

            this.__prevState = this.__state;
            this.__state = state;

            var evtObject = util.extend({
                fromState: this.__prevState,
                toState: this.__state
            }, data || {});

            this.__stateMachine.dispatchEvent(this.EVT_STATE_CHANGED, evtObject);

            if (this._shouldSkipOutgoingStateChangeEvent()) {
                return;
            }

            this.dispatchEvent(this.EVT_STATE_CHANGED, evtObject);
        },

        /**
         * Returns a boolean value indicating if the state change should be ignored.
         * Ignoring the state change will also prevent the state change event from dispatch.
         *
         * @method
         * @protected
         * @returns {Boolean} True if state change should be ignored, false otherwise
         * @memberof ax/device/Media#
         */
        _shouldSkipOutgoingStateChangeEvent: function () {
            return this.__pushing || this.__popping;
        },

        /**
         * To get the audio Tracks from the media
         * @method
         * @public
         * @returns {Promise.<String[]>} id The id of the track which is mainly for switching the audio track via setAudioTrack
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} unsupported feature
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} wrong formated parameter.
         * @memberof ax/device/Media#
         */
        getAudioTracks: function () {
            if (!this.__multiAudioStgy) {
                return promise.reject(core.createException(exception.UNSUPPORTED_OPERATION, "MultiAudioTracks strategy hasn't set yet"));
            }

            return this.__multiAudioStgy.getAudioTracks();
        },
        /**
         * To get the audio Tracks from the media
         * @method
         * @public
         * @param {String} id The id of the audio track
         * @returns {Promise.<Boolean>} Return the result after call in a promise
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} missing or invalid parameter.
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} unsupported
         * @memberof ax/device/Media#
         */
        setAudioTrack: function (id) {
            if (!this.__multiAudioStgy) {
                return promise.reject(core.createException(exception.UNSUPPORTED_OPERATION, "MultiAudioTracks strategy hasn't set yet"));
            }

            if (!(util.isString(id))) {
                return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Argument type is not valid"));
            }

            return this.__multiAudioStgy.setAudioTrack(id);
        },
        /* Get audio track attribute
         * @method getAudioTrackAttr
         * @public
         * @param {String} id The id of the audio track
         * @param {String} attr The attribute name such as {@link ax/device/interface/MultipleAudioTrackStgy#LANGCODE}
         * @param {String} defaultValue The default value of the attribute
         * @returns {Promise.<String>} return the retrieved value or default value if unable to find on the device and given that default value
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} no correct id
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} device is not ready yet
         * @memberof ax/device/Media#
         */
        getAudioTrackAttr: function (id, attr, defaultValue) {
            if (!this.__multiAudioStgy) {
                return promise.reject(core.createException(exception.UNSUPPORTED_OPERATION, "MultiAudioTracks strategy hasn't set yet"));
            }

            if (!util.isString(id) || !util.isString(attr) || !util.isString(defaultValue)) {
                return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Argument type is not valid or missing"));
            }

            return this.__multiAudioStgy.getAudioTrackAttr(id, attr, defaultValue);
        },
        /* Get current audio track id
         * @method getCurrentAudioTrackId
         * @public
         * @returns {Promise.<String|null>} The id of the audio track. Some audio track is unable to obtain the current track id, it will return null.
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} device is not ready yet
         * @memberof ax/device/Media#
         */
        getCurrentAudioTrackId: function () {
            if (!this.__multiAudioStgy) {
                return promise.reject(core.createException(exception.UNSUPPORTED_OPERATION, "MultiAudioTracks strategy hasn't set yet"));
            }
            return this.__multiAudioStgy.getCurrentAudioTrackId();
        },
        /* Get current media url
         * @method getUrl
         * @public
         * @returns {String} the url of the current media
         * @memberof ax/device/Media#
         */
        getUrl: function () {
            return this.__url;
        },
        /* Get current media options
         * @method getOpts
         * @public
         * @returns {Object} The option object set when load the url
         * @memberof ax/device/Media#
         */
        getOpts: function () {
            return this.__opts;
        },
        /* Show the target subtitle
         * @method showSubtitle
         * @param {String} id the id of subtitle
         * @returns {Promise.<Boolean>} True when showing the subtitle successfully.
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} when no subtitle exist.
         * @memberof ax/device/Media#
         * @public
         */
        showSubtitle: function (id) {
            if (!this.__subtitleModule) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "No available subtitle strategy"));
            }

            return this.__subtitleModule.showSubtitle(id);
        },
        /* Hide the subtitle
         * @method hideSubtitle
         * @returns {Promise.<Boolean>} True when showing the subtitle successfully.
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} when no subtitle exist.
         * @memberof ax/device/Media#
         * @public
         */
        hideSubtitle: function () {
            if (!this.__subtitleModule) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "No available subtitle strategy"));
            }
            return this.__subtitleModule.hideSubtitle();
        },
        /* Get current subtitle
         * @method getCurrentSubtitle
         * @returns {Promise.<Object>} The current subtitle object
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} when no subtitle exist.
         * @memberof ax/device/Media#
         * @public
         */
        getCurrentSubtitle: function () {
            if (!this.__subtitleModule) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "No available subtitle strategy"));
            }
            return this.__subtitleModule.getCurrentSubtitle();
        },
        /* Get the available subtitles
         * @method getSubtitles
         * @returns {Promise.<Object[]>} The available subtitles
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} when no subtitle exist.
         * @memberof ax/device/Media#
         * @public
         */
        getSubtitles: function () {
            if (!this.__subtitleModule) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "No available subtitle strategy"));
            }
            return this.__subtitleModule.getSubtitles();
        },
        /* Reset the current linked strategies
         * @method __resetStgies
         * @memberof ax/device/Media#
         * @private
         */
        __resetStgies: function () {
            if (this.__multiAudioStgy && this.__multiAudioStgy.deinit) {
                this.__multiAudioStgy.deinit();
            }

            if (this.__subtitleModule && this.__subtitleModule.deinit) {
                this.__subtitleModule.deinit();
            }

            this.__subtitleModule = null;
            this.__multiAudioStgy = null;
        }
    });

    // enforce the instance creation before returning, preventing 2 instances being created
    // (2 executions may go into the creation at the same time...)
    Media.singleton();

    return Media;
});
/**
 * FallbackSuspendedPlayback is a fallback version of SuspendedPlayback used by {@link ax/device/Media} for the player that has not implemented {@link ax/device/interface/Suspendable.SuspendedPlayback}.
 *
 * @class ax/device/FallbackSuspendedPlayback
 * @memberof ax/deivce/Media
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define("ax/device/FallbackSuspendedPlayback", [
    "ax/class",
    "ax/device/interface/Suspendable",
    "ax/device/AbstractPlayer",
    "ax/util"
], function (
    klass,
    Suspendable,
    AbstractPlayer,
    util
) {
    "use strict";

    return klass.create([Suspendable.SuspendedPlayback], {}, {
        /**
         * Constructs a new FallbackSuspendedPlayback.
         * The Media instance is needed such that circular dependency can be avoided.
         *
         * @method
         * @param {ax/device/Media} media The Media instance
         * @param {ax/device/interface/Player} player The player
         * @param {Object} metadata The metadata for playback resume
         * @param {String} metadata.url The media url
         * @param {Object} [metadata.opts] The media loading options
         * @param {Number} [metadata.pos = 0] The resume position
         * @param {String} [metadata.state] The playback state
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        init: function (media, player, metadata) {
            this.__media = media;
            this.__player = player;
            this.__url = metadata.url;
            this.__opts = metadata.opts || {};
            this.__pos = metadata.pos || 0;
            this.__state = metadata.state;
        },

        /**
         * Restores the suspended playback.
         * The playback states before suspension will retain.
         *
         * @method
         * @param {Function} cb The callback that to be called when the restoration completes
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        restore: function (cb) {
            var sMedia = this.__media;
            var loadOpts = util.extend(util.extend({}, this.__opts, true), {
                player: this.__player
            });

            var stopped = (this.__state === AbstractPlayer.PLAYBACK_STATE.STOPPED);

            //add the state change listener for handling playing and paused
            if (!stopped) {
                // one time event listener to handle the playback state
                var onStateChange = util.bind(function (evt) {
                    switch (evt.toState) {
                    case sMedia.PLAYING:
                        if (this.__state === AbstractPlayer.PLAYBACK_STATE.PAUSED) {
                            sMedia._doPause();
                        }

                        sMedia.__stateMachine.removeEventListener(sMedia.EVT_STATE_CHANGED, onStateChange);
                        cb();
                        break;
                    }
                }, this);

                sMedia.__stateMachine.addEventListener(sMedia.EVT_STATE_CHANGED, onStateChange);
            }

            sMedia._doLoad(this.__url, loadOpts);

            //stop directly and perform callback when stopped
            if (stopped) {
                cb();
                return;
            }

            sMedia._doPlay({
                sec: this.__pos
            });
        },

        /**
         * Disposes the suspended playback, destroying he playback states before suspension.<br/>
         * A disposed playback cannot be restored anymore.
         *
         * @method
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        dispose: function () {
            this.__player = null;
        },

        /**
         * Returns the media information.
         *
         * @method
         * @returns {MediaMeta} The media metadata
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        getMediaMeta: function () {
            return {
                url: this.__url,
                opts: this.__opts
            };
        },

        /**
         * Returns the player instance who is responsible for restoration.
         *
         * @method
         * @returns {ax/device/interface/Player} The player instance
         * @memberof ax/device/FallbackSuspendedPlayback#
         */
        getPlayer: function () {
            return this.__player;
        }
    });
});
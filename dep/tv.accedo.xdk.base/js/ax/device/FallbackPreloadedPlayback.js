/**
 * FallbackPreloadedPlayback is a fallback version of PreloadedPlayback used by {@link ax/device/Media} for the player that has not implemented {@link ax/device/interface/Preloadable.PreloadedPlayback}.
 *
 * @class ax/device/FallbackPreloadedPlayback
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define("ax/device/FallbackPreloadedPlayback", [
    "ax/class",
    "ax/device/interface/Preloadable",
    "ax/util",
    "ax/device/playerRegistry",
    "ax/core",
    "ax/exception"
], function (
    klass,
    Preloadable,
    util,
    playerRegistry,
    core,
    exception
) {
    "use strict";

    return klass.create([Preloadable.PreloadedPlayback], {}, {
        /**
         * Constructs a new FallbackPreloadedPlayback.
         * The Media instance is needed such that circular dependency can be avoided.
         *
         * @method
         * @param {ax/device/Media} media The Media instance
         * @param {String} url The media url
         * @param {Object} [opts] The media loading options
         * @memberof ax/device/FallbackPreloadedPlayback#
         */
        init: function (media, url, opts) {
            this.__media = media;
            this.__url = url;
            this.__opts = opts || {};

            if (this.__opts.player) {
                this.__player = this.__opts.player;
            } else {
                this.__player = playerRegistry.getPlayer(this.__opts);
            }
        },

        /**
         * Plays the preloaded playback.
         *
         * @method
         * @memberof ax/device/FallbackPreloadedPlayback#
         */
        play: function (pos) {
            var sMedia = this.__media;

            sMedia.load(this.__url, this.__opts);
            sMedia.play({
                sec: pos
            });
        },

        /**
         * Disposes the suspended playback, destroying he playback states before suspension.<br/>
         * A disposed playback cannot be restored anymore.
         *
         * @method
         * @memberof ax/device/FallbackPreloadedPlayback#
         */
        dispose: function () {
            // do nothing as nothing has been preloaded inside the fallback implementation
        },

        /**
         * Returns the media information.
         *
         * @method
         * @returns {MediaMeta} The media metadata
         * @memberof ax/device/FallbackPreloadedPlayback#
         */
        getMediaMeta: function () {
            return {
                url: this.__url,
                opts: this.__opts
            };
        },

        /**
         * Returns the player instance who is responsible for restoration.
         *
         * @method
         * @returns {ax/device/interface/Player} The player instance
         * @memberof ax/device/FallbackPreloadedPlayback#
         */
        getPlayer: function () {
            return this.__player;
        }
    });
});
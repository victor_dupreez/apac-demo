/**
 * Subtitle class which composites of external and internal subtitle strategies.
 * Each media will has its own subtitle instance which store information for the subtitles.
 *
 * @class ax/device/MediaSubtitle
 */
define("ax/device/MediaSubtitle", [
    "ax/class",
    "ax/config",
    "ax/console",
    "ax/promise",
    "ax/core",
    "ax/exception",
    "ax/util",
    "ax/device/interface/SubtitleStgy",
    "ax/device/interface/ExtSubtitleStgy",
    "require"
], function (
    klass,
    config,
    console,
    promise,
    core,
    exception,
    util,
    ISubtitleStgy,
    IExternalStgy,
    require
) {
    "use strict";
    /**
     * Subtitle Object which stores the information for the subtitle. It provides the langauage info and type
     * @typedef {Object} Subtitle
     * @property {String} id the id of the subtitle
     * @property {String} type The actual data entries
     * @property {String} lang the langauge code for the subtitle. It should follow the standard lang code <https://accedobroadband.jira.com/wiki/display/XDKDEV/Media+Subtitle+Strategy+support+-+2.4#MediaSubtitleStrategysupport-2.4-Standardlangcode>
     * @property {String} tag the extra information for the subtitle
     * @memberof ax/device/MediaSubtitle#
     */


    /**
     * Subtitle Option for each external subtitle information provided by developer.
     * @typedef {Object} subtitleOption
     * @property {String} url the url of the subtitle
     * @property {String} type The actual data entries
     * @property {String} lang the langauge code for the subtitle. It should follow the standard lang code <https://accedobroadband.jira.com/wiki/display/XDKDEV/Media+Subtitle+Strategy+support+-+2.4#MediaSubtitleStrategysupport-2.4-Standardlangcode>
     * @property {String} tag the extra information for the subtitle
     * @memberof ax/device/MediaSubtitle#
     */

    var INTERNAL_TYPE = ISubtitleStgy.TYPE,
        EXTERNAL_TYPE = IExternalStgy.TYPE;

    return klass.create({}, {
        /**
         * The player instance
         * @private
         * @memberof ax/device/MediaSubtitle#
         */
        __player: null,
        /**
         * The current seletced subtitle id
         * @private
         * @memberof ax/device/MediaSubtitle#
         */
        __currentId: -1,
        /**
         * The subtitle object array
         * @private
         * @memberof ax/device/MediaSubtitle#
         */
        __subtitleArr: null,
        /**
         * The id mapping of the original subtitle id in strategy
         * @private
         * @memberof ax/device/MediaSubtitle#
         */
        __idMapping: [],

        /**
         * The internatl strategy instance
         * @private
         * @memberof ax/device/MediaSubtitle#
         */
        __internalStgy: null,

        /**
         * The external strategy instance
         * @private
         * @memberof ax/device/MediaSubtitle#
         */
        __externalStgy: null,

        /**
         * The external strategy instance
         * @private
         * @memberof ax/device/MediaSubtitle#
         */
        __subtitleStgy: null,
        /**
         * init the subtitle module
         * @param {Object} subtitleStgy the object which store the information for the stgy class name.
         *  example {"internal": "InternalSubtitleStgy", "external":"ExternalSubtitleStgy"}
         * @param {ax/device/interface/Player} player player instance
         * @param {ax/device/interface/Preloaded.PreloadedPlayback} [preloaded] the preloadable item
         * @method
         * @memberof ax/device/MediaSubtitle#
         */
        init: function (subtitleStgy, player, preloaded) {
            this.__subtitleStgy = subtitleStgy || {};
            this.__player = player;
            this.__preloaded = preloaded;
        },
        /**
         * Prepare the subtitle
         * @param {subtitleOption[]} opts external subtitle information
         * @public
         * @method
         * @memberof ax/device/MediaSubtitle#
         */
        prepare: function (opts) {
            var internalStgyId, InternalStgy, externalStgyId, ExternalStgy;

            internalStgyId = this.__subtitleStgy.internal;
            console.info("[XDK] internal strategy is loading now");

            if (internalStgyId) {

                InternalStgy = require(internalStgyId);

                if (!klass.hasImpl(InternalStgy, ISubtitleStgy)) {
                    throw core.createException(exception.ILLEGAL_STATE, "Internal Subtitle doesn't implement subtitle strategy");
                }

                try {
                    this.__internalStgy = new InternalStgy();
                    console.info("[XDK] Success to create subtitle internal strategy " + internalStgyId);
                } catch (ex) {
                    console.warn("[XDK] Fail to create subtitle internal strategy " + internalStgyId);
                }

                this.__internalStgy.setPlayer(this.__player, this.__preloaded);
            }

            externalStgyId = this.__subtitleStgy.external;
            console.info("[XDK] external strategy is loading now");

            if (externalStgyId) {

                ExternalStgy = require(externalStgyId);

                if (!klass.hasImpl(ExternalStgy, IExternalStgy)) {
                    throw core.createException(exception.ILLEGAL_STATE, "External Subtitle doesn't implement subtitle strategy");
                }

                try {
                    this.__externalStgy = new ExternalStgy();
                    console.info("[XDK] Success to create subtitle external strategy " + externalStgyId);
                } catch (ex) {
                    console.warn("[XDK] Fail to create subtitle external strategy " + externalStgyId);
                }

                this.__externalStgy.setPlayer(this.__player, this.__preloaded);
                this.__externalStgy.prepare(opts);
            }
        },
        /**
         * Show the specific subtitle
         * @param {String} [id] the subtitle id.If it is empty, it will be the current Id.
         * @returns {Promise.<Boolean>} true when show subtitle successfully
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} no available subtitle track and fail to show
         * @public
         * @method
         * @memberof ax/device/MediaSubtitle#
         */
        showSubtitle: function (id) {
            if (util.isUndefined(id)) {

                if (this.__currentId === -1) {
                    return promise.reject(core.createException(exception.ILLEGAL_STATE, " Fail to sho the subtitle when no available subtitle strategy."));
                }

                id = this.__currentId;
            }

            var targetStgy;

            try {
                targetStgy = this.__getStgyById(id);
            } catch (mException) {
                return promise.reject(mException);
            }

            if (targetStgy) {
                //get back the conversion for the id
                var originalId = this.__idMapping[id];

                this.__currentId = id;

                return targetStgy.showSubtitle(originalId);
            }

            return promise.reject(core.createException(exception.ILLEGAL_STATE, " Fail to show the subtitle when no available subtitle strategy."));

        },
        /**
         * Hide the current subtitle track
         * @returns {Promise.<Boolean>} true when hide subtitle successfully
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} no available subtitle track and fail to hide
         * @public
         * @method
         * @memberof ax/device/MediaSubtitle#
         */
        hideSubtitle: function () {
            if (this.__currentId === -1) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, " Fail hide the subtitle when no available subtitle to hide."));
            }

            var curStgy = this.__getStgyById(this.__currentId);

            if (curStgy) {
                return curStgy.hideSubtitle();
            }

            return promise.reject(core.createException(exception.ILLEGAL_STATE, " Fail hide the subtitle when no available subtitle strategy."));
        },
        /**
         * Get the strategy whether internla or external
         * @param {String} id the id of the subtitle track
         * @returns {ax/device/interface/SubtitleStgy} the subtitle strategy. Null when no available strategy
         * @private
         * @method
         * @memberof ax/device/MediaSubtitle#
         */
        __getStgyById: function (id) {
            if (!this.__subtitleArr || id > this.__subtitleStgy.length) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, " the id is not in a possible range");
            }

            var curSubtitle = this.__subtitleArr[id];

            if (curSubtitle.type === INTERNAL_TYPE && this.__internalStgy) {
                return this.__internalStgy;
            }

            if (curSubtitle.type === EXTERNAL_TYPE && this.__externalStgy) {
                return this.__externalStgy;
            }

            return null;
        },
        /**
         * Get the current subtitle information
         * @returns {Promise.<Subtitle>} the subtitle information
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} no available subtitle track
         * @memberof ax/device/MediaSubtitle#
         */
        getCurrentSubtitle: function () {
            //try to get it from the internal strategy
            if (this.__currentId === -1) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, " Fail to get current substitle since it doesn't exist or set"));
            }

            return promise.resolve(this.__subtitleArr[this.__currentId]);

        },
        /**
         * Get the current subtitle information
         * @returns {Promise.<Subtitle[]>} array of the subtitle information
         * @method
         * @memberof ax/device/MediaSubtitle#
         */
        getSubtitles: function () {

            //only perform once and return the data directly
            if (this.__subtitleArr) {
                return promise.resolve(this.__subtitleArr);
            }

            var getSubtitlePromiseArr = [],
                addSubtitle = util.bind(function (item) {
                    //standardise the id to be index numbers

                    //record the original id
                    this.__idMapping.push(item.id);

                    item.id = this.__subtitleArr.length;

                    this.__subtitleArr.push(item);
                }, this);

            this.__subtitleArr = [];

            if (this.__externalStgy) {
                getSubtitlePromiseArr.push(this.__externalStgy.getSubtitles().then(util.bind(function (externalSubtitles) {
                    //push the external subtitle information into the subtitle module
                    util.each(externalSubtitles, addSubtitle);

                }, this)));
            }

            if (this.__internalStgy) {
                getSubtitlePromiseArr.push(this.__internalStgy.getSubtitles().then(util.bind(function (internalSubtitles) {
                    //push the internal subtitle information into the subtitle module
                    util.each(internalSubtitles, addSubtitle);
                }, this)));
            }

            return promise.all(getSubtitlePromiseArr).then(util.bind(function () {
                return this.__subtitleArr;
            }, this));
        },

        /**
         * deinit
         * @method
         * @memberof ax/device/MediaSubtitle#
         */
        deinit: function () {
            if (this.__externalStgy && this.__externalStgy.deinit) {
                this.__externalStgy.deinit();
            }

            if (this.__internalStgy && this.__internalStgy.deinit) {
                this.__internalStgy.deinit();
            }

        }
    });

});
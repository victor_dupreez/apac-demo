/**
 * The device's module type constants.
 * @module ax/device/basicInterfaces
 * @author Marcus Yip <marcus.yip@accedo.tv>
 */
define("ax/device/basicInterfaces", function() {
    "use strict";

    return {
        STORAGE: "storage",
        MEDIA_PLAYER: "media",
        TV_KEY: "tvkey",
        ID: "id",
        SYSTEM: "system"
    };
});

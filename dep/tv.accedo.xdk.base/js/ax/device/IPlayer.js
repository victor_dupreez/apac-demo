/**
 * The media player interface. Implement this interfaces and register to {@link ax/device/playerRegistry|Player Registry}.
 *
 * @class ax/device/IPlayer
 * @deprecated as it is changed into other implementation using ax/device/interface/Player.
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/device/IPlayer", ["ax/class"], function (klass) {
    "use strict";
    /**
     * @typedef PlayerCapabilites
     * @inner
     * @desc the player's capabilities
     * @type {object}
     * @property {Array} drms Player supported DRM
     * @property {Array} type Player'supported container format. e.g. ["mp4","hls"]
     * @memberof ax/device/IPlayer
     * @example
     * {
     *      "drms" : ["aes128", "widevine"],
     *      "type" : ["hls", "mp4"]
     * }
     */

    /**
     * @typedef MediaBitrates
     * @desc the media's bitrates
     * @type {object}
     * @property {Array} [currentBitrate] current playback bitrate
     * @property {Array} [availableBitrates] available playback bitrates
     * @inner
     * @memberof ax/device/IPlayer
     *
     */

    return klass.create({}, {
        /**
         * Gets the player's capabilities
         * @method
         * @abstract
         * @return {ax/device/IPlayer~PlayerCapabilites} the player's capabilities
         * @memberof ax/device/IPlayer#
         */
        getCapabilities: klass.abstractFn,
        /**
         * Prepare the player, should be called before using
         * @method
         * @abstract
         * @param {string} type The media type to be played
         * @param {string} [codec] The media codec
         * @param {string} [drm] DRM to be used
         * @memberof ax/device/IPlayer#
         */
        prepare: klass.abstractFn,
        /**
         * Reset the player, should be called when it is to replaced by other player
         * @method
         * @abstract
         * @memberof ax/device/IPlayer#
         */
        reset: klass.abstractFn,
        /**
         * Loads the specified media
         * @method
         * @abstract
         * @param {String} url the URL address of the media
         * @param {Object} [opts] the options for loading this media
         * @param {string} [opts.codec] the media codec
         * @param {string} [opts.type] explicitly specify the media type
         * @param {string} [opts.drm] DRM to be used
         * @memberof ax/device/IPlayer#
         */
        load: klass.abstractFn,
        /**
         * Start the playback
         * @method
         * @abstract
         * @param {Object} [opts] the options for starting playback
         * @param {Number} [opts.sec] the playback start time
         * @memberof ax/device/IPlayer#
         */
        play: klass.abstractFn,
        /**
         * Stop the playback
         * @method
         * @abstract
         * @memberof ax/device/IPlayer#
         */
        stop: klass.abstractFn,
        /**
         * Pause the playback
         * @method
         * @abstract
         * @memberof ax/device/IPlayer#
         */
        pause: klass.abstractFn,
        /**
         * Resume the playback
         * @method
         * @abstract
         * @memberof ax/device/IPlayer#
         */
        resume: klass.abstractFn,
        /**
         * Seek to specifiy position of the video
         * @method
         * @abstract
         * @param {Number} sec the position to seek to in seconds
         * @memberof ax/device/IPlayer#
         */
        seek: klass.abstractFn,
        /**
         * Skip the playback forward/backward for certain seconds
         * @method
         * @abstract
         * @param {Number} sec number of seconds to skip (10 by default)
         * @memberof ax/device/IPlayer#
         */
        skip: klass.abstractFn,
        /**
         * Speed up/down the media playback
         * @method
         * @abstract
         * @param {Number} speed the playback speed to set
         * @memberof ax/device/IPlayer#
         */
        speed: klass.abstractFn,
        /**
         * Get the playback speed
         * @method
         * @abstract
         * @return {Number} the playback speed
         * @memberof ax/device/IPlayer#
         */
        getPlaybackSpeed: klass.abstractFn,
        /**
         * Sets video window size
         * @method
         * @abstract
         * @param {Object} param window size parameter
         * @param {Integer} param.top window top
         * @param {Integer} param.left window left
         * @param {Integer} param.width window width
         * @param {Integer} param.height window height
         * @memberof ax/device/IPlayer#
         */
        setWindowSize: klass.abstractFn,
        /**
         * Sets video window size to be fullscreen
         * @method
         * @abstract
         * @memberof ax/device/IPlayer#
         */
        setFullscreen: klass.abstractFn,
        /**
         * Get the media bitrates
         * @method
         * @abstract
         * @return {ax/device/IPlayer~MediaBitrates} current bitrate and available bitrates
         * @memberof ax/device/IPlayer#
         */
        getBitrates: klass.abstractFn,
        /**
         * Get the current playback time
         * @method
         * @abstract
         * @return {Number} the current playback time
         * @memberof ax/device/IPlayer#
         */
        getCurTime: klass.abstractFn,
        /**
         * Get the total playback time
         * @method
         * @abstract
         * @return {Number} the total playback time
         * @memberof ax/device/IPlayer#
         */
        getDuration: klass.abstractFn,
        /**
         * Get back the path
         * @method
         * @public
         * @return {String} the path of the player
         * @memberof ax/device/IPlayer#
         */
        getId: function () {
            return this.__id;
        },
        /**
         * set the id of the player
         * @method
         * @public
         * @param {String} id the path of the player
         * @memberof ax/device/IPlayer#
         */
        setId: function (id) {
            this.__id = id;
        }
    });
});
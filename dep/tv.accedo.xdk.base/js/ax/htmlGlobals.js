/**
 * Html globals module. Export standard HTML built-in objects as dependencies
 * for AMD dependency injections.
 *
 * @module ax/htmlGlobals
 */
define(["exports"], function(exports) {

    "use strict";

    exports.document = window.document;
});
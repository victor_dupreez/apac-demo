/**
 * XDK framework "meta" information or capabilities
 * @module ax/core
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/core", [], function () {
    "use strict";
    var core, guid = 1,
        CustomException = function (name, message) {
            this.name = name || "XDKCustomError";
            this.message = message || "";
        };

    core = {
        /**
         * Current XDK version number
         * @member version
         * @memberof module:ax/core
         * @public
         */
        version: "2.5.3",
        /**
         * The build number of the current version
         * @name buildNumber
         * @memberof module:ax/core
         * @public
         */
        buildNumber: "1446698925-936f62e",
        /**
         * Context root of current Javascript runtime
         * @name root
         * @memberof module:ax/core
         * @public
         */
        root: (function () {
            return (typeof window !== "undefined" ? window : null);
        })(),
        /**
         * Create a custom exception (i.e. a runtime error) with name and message, that can be thrown later
         * @method createException
         * @memberof module:ax/core
         * @param {String} [name] exception name
         * @param {String} [message] exception message
         * @return {CustomException} exception the custom XDK exception
         * @public
         */
        createException: function (name, message) {
            CustomException.prototype = new Error(); // create error here to record the stack
            CustomException.prototype.constructor = CustomException;
            return new CustomException(name, message);
        },
        /**
         * Gets the next global unique
         * @method getGuid
         * @return {Number} guid a unique global ID
         * @public
         * @memberof module:ax/core
         */
        getGuid: function () {
            return guid++;
        }
    };
    return core;
});
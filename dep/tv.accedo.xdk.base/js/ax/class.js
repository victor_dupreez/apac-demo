/**
 * Simulate class inheritance using javascript's prototypical inheritance
 * @module ax/class
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/class", ["ax/util", "ax/core", "ax/Interface", "require"], function (util, core, Interface, require) {
    "use strict";
    var classModule, IS_DONTENUM_BUGGY = true,
        p,
        inheriting = false,
        // Internal extend utility function
        extendFn = function(destination, source) {
            var k, value;
            for (k in source) {
                value = source[k];
                // if is a plain object or array, do not share the reference, use a clone instead
                if (util.isArray(value)) {
                    destination[k] = value.slice();
                    continue;
                }
                if (util.isPlainObject(value)) {
                    destination[k] = util.clone(value, true);
                    continue;
                }
                destination[k] = value;
            }
        },
        // Utility function to only copy over objects and arrays
        copyObjectFn = function(destination, source) {
            var k, value;
            for (k in source) {
                value = source[k];
                // if is a plain object or an array, do not share the reference, use a clone instead
                if (util.isArray(value)) {
                    destination[k] = value.slice();
                    continue;
                }
                if (util.isPlainObject(value)) {
                    destination[k] = util.clone(value, true);
                    continue;
                }
            }
        },
        emptyFn = function() {},

        create, addMethods, addStaticMethods, createAbstract, isAbstract,
        /**
         * Reference function for absract function declaration
         * @memberof module:ax/class
         * @method abstractFn
         * @public
         */
        abstractFn = function() {
            var console = require("ax/console"),
                core = require("ax/core");
            console.error("Abstract function called!");
            throw core.createException("AbstractMethodInvoked",
                "Abstract method called! This method must be implemented in subclass!");
        };

    for (p in {
        toString: 1
    }) {
        if (p === "toString") {
            IS_DONTENUM_BUGGY = false;
        }
    }

    /**
     * Add/Overload class members
     * @memberof module:ax/class
     * @deprecated
     * @function addMethods
     * @param {Object} source Members to be added to current class
     * @param {Boolean} isStatic Whether the members are static or instance memebers
     * @return {Class}
     */
    addMethods = function(klass, source, isStatic) {
        var ancestor = isStatic ? klass._superclass : klass._superclass && klass._superclass.prototype,
            i, length, property, value, origMethod, injectSuper, properties = util.keys(source);

        if (IS_DONTENUM_BUGGY) {
            if (source.toString !== Object.prototype.toString) {
                properties.push("toString");
            }
            if (source.valueOf !== Object.prototype.valueOf) {
                properties.push("valueOf");
            }
        }
        injectSuper = function(name, fn) {
            var _injected = function() {
                var ret, temp = this._super;
                // Add a new ._super() method that is the same method
                // but on the super-class
                this._super = ancestor[name];

                // fail-over incase ancestor has no such method
                if (!util.isFunction(this._super)) {
                    this._super = emptyFn;
                }

                // The method only need to be bound temporarily, so we
                // remove it when we're done executing
                ret = fn.apply(this, arguments);
                if (temp) {
                    this._super = temp;
                } else {
                    delete this._super;
                }

                return ret;
            };

            // add introspection for interface impl. checking
            _injected.isSuperWrapper = true;
            _injected.orig = fn;

            return _injected;
        };
        for (i = 0, length = properties.length; i < length; i++) {
            property = properties[i];
            value = source[property];

            if (ancestor && util.isFunction(value)) {
                origMethod = value;
                value = injectSuper(property, origMethod);
                value.valueOf = util.bind(origMethod.valueOf, origMethod);
                value.toString = util.bind(origMethod.toString, origMethod);
            }

            if (isStatic) {
                klass[property] = value;
            } else {
                klass.prototype[property] = value;
            }
        }
        return klass;
    };

    /**
     * Add/Override static members
     * @memberof module:ax/class
     * @deprecated
     * @function addStaticMethods
     * @param {Object} source Static members to be added to current class
     * @returns {Class}
     */
    addStaticMethods = function(klass, source) {
        return addMethods(klass, source, true);
    };


    function classCreate() {
        var Parent = null,
            args = Array.prototype.slice.call(arguments),
            intfs = [],
            statics = null,
            members = null,
            console;

        if (util.isFunction(args[0])) {
            Parent = args.shift();
        }

        // get the interfaces declaration
        if (util.isArray(args[0])) {
            intfs = args.shift();
        }

        // get static members declaration
        statics = args.shift();

        if (!util.isPlainObject(statics) && !util.isFunction(statics)) {
            console = require("ax/console");
            console.error("Static members parameter must be an object or a function!");
            throw core.createException("IncorrectParam",
                "Static members parameter must be an object or a function!");
        }

        // get instance members declaration
        members = args.shift();

        function klass() {
            /*jshint validthis:true */
            if (!(this instanceof klass)) {
                var console = require("ax/console");
                console.error(
                    "Please use \'new\' operator! This is a class constructor function, it cannot be called directly!"
                );
                throw core.createException("IncorrectInvocation",
                    "Please use \'new\' operator! This is a class constructor function, it cannot be called directly!"
                );
            }

            if (!inheriting) {
                copyObjectFn(this, klass.__defaults);
                this.init.apply(this, arguments);
            }
        }

        if (Parent) {
            extendFn(klass, Parent); // inherit parent's static members
            inheriting = true;
            klass.prototype = new Parent();
            inheriting = false;
            // not to reference the same object for different classes
            copyObjectFn(klass.prototype, Parent.prototype);
        } else {
            extendFn(klass, classModule.methods); // bring in basic static memebers
        }

        klass._superclass = Parent;

        //add statics methods
        if (util.isFunction(statics)) {
            statics = statics.apply(null);
        }
        addStaticMethods(klass, statics);

        //Add memebers
        if (util.isFunction(members)) {
            members = members.apply(null);
        }

        if (members) {
            addMethods(klass, members);
            // save up instance default values of objects and arrays
            klass.__defaults = {};
            copyObjectFn(klass.__defaults, klass.prototype); // inherited defaults
            copyObjectFn(klass.__defaults, members); // overriden defaults
        }
        if (!members) {
            throw core.createException("IncorrectParam", "Instance memebers are not provided!");
        }

        if (!klass.prototype.init) {
            klass.prototype.init = emptyFn;
        }

        klass.prototype.constructor = klass;

        if (util.isFunction(klass.prototype.deinit)) {
            klass.prototype.destructor = klass.prototype.deinit;
        }

        //add the interface
        klass.intfs = intfs;

        if (util.isFunction(klass.main)) {
            klass.main();
        } else if (!klass.main) {
            klass.main = emptyFn;
        }

        //add the parent interface
        if (Parent && Parent.intfs && Parent.intfs.length > 0) {
            klass.intfs = klass.intfs.concat(Parent.intfs);
        }

        return klass;
    }
    /**
     * Creates a class and returns a constructor function for instances
     * of the class. Calling the constructor function (typically as part of a new
     * statement) will invoke the class's init method.
     *
     * If a subclass overrides an instance method declared in a superclass, the
     * subclass's method can still access the original method. To do so, call
     * this._super which is available within the overrided method.
     *
     *
     * To extend a class after it has been defined, use {@link module:ax/class.addMethods}.
     *
     * @name create
     * @memberof module:ax/class
     * @function create
     * @param {Class} [parentClass] Parent class to inherit from.
     * @param {ax/Interface[]} {interfaces} Interfaces to implement.
     * @param {Object} staticMember Static members of the class. For function, it will be executed and
     *  the returned object will be used.
     * @param {Object} [instanceMember] Instance members of the class. For function, it will be executed and
     *  the returned object will be used.
     * @return {Class} the created class' constructor
     * @example
     * klass.create(ParentClass, [
     *        "MyInterface1",
     *        "MyInterface2"
     * ],
     * {
     * // Static declarations
     *      ...
     * },{
     * // Member declarations
     *
     *      //constructor method
     *      init:function(opts) {
     *
     *          this._super(opts); //calling super class method
     *      }
     * });
     */
    /**
     * Stop NetBeans from complaining
     * @ignore
     */
    create = function() {
        var klass, i, parent;

        //normal class create
        klass = classCreate.apply(this, arguments);

        parent = klass._superclass;

        //ensure interface implmentation
        if (klass.intfs) {
            for (i = 0; i < klass.intfs.length; i++) {
                Interface.ensureImpl(klass, klass.intfs[i]);
            }
        }

        klass.__abstract = false;

        return klass;
    };
    /**    
     * create an abstract class. The different between create and createAbstract is abstract class won't check if any abstract function
     * or interface that not implement. The class inherit from abstract class need to implement all the abstractFn from the abstract class.
     * Otherwise, error will be thrown.
     * @name createAbstract
     * @memberof module:ax/class
     * @function
     * @param {Class} [parentClass] Parent class to inherit from.
     * @param {ax/Interface[]} {interfaces} Interfaces to implement.
     * @param {Object} staticMember Static members of the class. For function, it will be executed and
     *  the returned object will be used.
     * @param {Object} [instanceMember] Instance members of the class. For function, it will be executed and
     *  the returned object will be used.
     * @return {Class} the created class' constructor
     * @example
     * klass.createAbstract(ParentClass, [
     *        "MyInterface1",
     *        "MyInterface2"
     * ],
     * {
     * // Static declarations
     *      ...
     * },{
     * // Member declarations
     *
     *      //constructor method
     *      init:function(opts) {
     *
     *          this._super(opts); //calling super class method
     *      }
     * });
     */
    createAbstract = function() {
        var klass = classCreate.apply(this, arguments);

        klass.__abstract = true;

        return klass;
    };
    /**    
     * check if current class is abstract
     * @name isAbstract
     * @method
     * @param {ax/Class} klass the Class to check
     * @returns {Boolean} True if it is abstract class
     * @memberof ax/Class
     */
    isAbstract = function(klass) {
        return klass.__abstract;
    };
    /**    
     * Check if a Class has implemented an Interface
     * @name hasImpl
     * @method
     * @param {ax/Class} klass the Class to check
     * @param {ax/Interface} intf the Interface to look for
     * @memberof ax/Class
     */
    function hasImpl(klass, intf) {

        /**
         * @ignore
         */
        function checkIntfs(implIntfs) {

            var ret = false;

            util.each(implIntfs, function(implIntf) {

                if (implIntf === intf) {
                    ret = true;
                    return util.breaker;
                }

                if (checkIntfs(implIntf.exts)) {
                    ret = true;
                    return util.breaker;
                }

            });

            return ret;

        }

        var anctrKlass = klass,
            hasIntf = false;

        while (anctrKlass) {
            if (checkIntfs(anctrKlass.intfs, intf)) {
                hasIntf = true;
                break;
            } else {
                anctrKlass = anctrKlass._superclass;
            }
        }

        return hasIntf;

    }

    classModule = {
        create: create,
        addMethods: addMethods,
        addStaticMethods: addStaticMethods,
        abstractFn: abstractFn,
        hasImpl: hasImpl,
        createAbstract: createAbstract,
        isAbstract: isAbstract
    };

    return classModule;
});
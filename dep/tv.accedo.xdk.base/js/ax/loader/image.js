/**
 * Image is an AMD loader plugin.
 *
 * If optimization is required, extra configuration have to be made according to {@link module:ax/loader/imageBuilder}.
 *
 * @module image
 * @author Marco Fan <marco.fan@accedo.tv>
 * @example
 * // this example assumes logo.png sits beside the view file
 * define("demo/tmpl/main", [
 *     "ax/ext/ui/Layout",
 *     "ax/ext/ui/Image",
 *     "image!./logo.png",
 *     "image!http://xdk.accedo.tv/img/header.jpg"
 * ], function (
 *     Layout,
 *     Image,
 *     imgLogoUrl,
 *     imgHeaderUrl
 * ) {
 *     "use strict";
 *
 *     return function () {
 *         return {
 *             klass: Layout,
 *             children: [{
 *                 klass: Image,
 *                 src: imgLogoUrl
 *             }, {
 *                 klass: Image,
 *                 src: imgHeaderUrl
 *             }]
 *         };
 *     };
 * });
 */
define("image", function () {
    "use strict";

    var exports = {
        /**
         * pluginBuilder is a string that points to another module to use instead of the current plugin when the plugin is used as part of an optimizer build.
         *
         * @see {@link http://requirejs.org/docs/plugins.html#apipluginbuilder}
         * @member {String} pluginBuilder
         * @memberof module:image
         */
        pluginBuilder: "ax/loader/imageBuilder"
    };

    /**
     * Resolves a temporary path that may contain dots or double slashes.
     * Input can be in the form of "./this/../is/a///path", and that will be resolved to "is/a/path".
     *
     * @method
     * @private
     * @param {String} path The temporary path
     * @memberof module:image
     * @example
     *     resolvePath("./this/../is/a///path"); // becomes "is/a/path"
     */
    function resolvePath(path) {
        path = path.replace(/\/+/g, "/");
        var array = path.split("/");
        var i, part, length = array.length;

        for (i = 0; i < length; i++) {
            part = array[i];
            if (part === ".") {
                array.splice(i, 1);
                i -= 1;
            } else if (part === "..") {
                if (i === 1 && (array[2] === ".." || array[0] === "..")) {
                    // Keep at least one non-dot path segment at the front so it can be mapped correctly to disk. 
                    // Otherwise, there is likely no path mapping for a path starting with '..'.
                    // This can still fail, but catches the most reasonable uses of ..
                    break;
                } else if (i > 0) {
                    array.splice(i - 1, 2);
                    i -= 2;
                }
            }
        }

        return array.join("/");
    }

    /**
     * Load a resource.
     * Assuming the resource ID do not need special ID normalization.
     *
     * @method
     * @param {String} resId The resource ID that the plugin should load.
     * @param {Function} req A local require function to use to load other modules. This require function has some utilities on it:
     *  * require.toUrl("moduleId+extension"). See the require.toUrl API notes for more information.
     * @param {Function} load A function to call once the value of the resource ID has been determined. This tells the loader that the plugin is done loading the resource.
     * @param {Object} [config] The configuration object of AMD
     * @memberof module:image
     */
    exports.load = function (resId, req, load, config) {
        if (config && config.image && config.image.map) {
            // for optimized image
            for (var baseUrlToImgDir in config.image.map) {
                if (config.image.map[baseUrlToImgDir].indexOf(resId) >= 0) {
                    // the resource is optmized, load it using the optimized path
                    load(resolvePath(config.baseUrl + "/" + baseUrlToImgDir + "/" + resId));
                    return;
                }
            }
        }

        // reaching here means the map doesn't exist or it does not contain the resource
        // the resource should not have been optimized, load it in the normal way
        load(req.toUrl(resId));
    };

    return exports;
});
/**
 * A CSS loader plugin that is responsible to load CSS files.
 * @module css
 */
define("css", [], function() {
    "use strict";
    var exports = {
            /**
             * If the plugin has a dynamic property set to true, then it means
             * the loader MUST NOT cache the value of a normalized plugin dependency,
             * instead call the plugin's load method for each instance of a plugin dependency.
             * @property {Boolean} dynamic
             * @memberof module:css
             */
            dynamic: false,

            /**
             * pluginBuilder is a string that points to another module to use instead of the current plugin when the plugin is used as part of an optimizer build. 
             * Without this property, the optimization WILL fail. 
             * @see {@link http://requirejs.org/docs/plugins.html#apipluginbuilder}
             * @property {String} pluginBuilder
             * @memberof module:css
             */
            pluginBuilder: "ax/loader/cssBuilder"
        },
        head = document.getElementsByTagName("head")[0];

    /**
     * Load a CSS file by inserting a <link> node to the DOM head section. 
     * This will execute the onSuccess callback when the node gets loaded. 
     * If the CSS file does not have an extension (.css), one will be appended.
     * @method
     * @private
     * @param {String} cssFile The CSS file location
     * @param {Function} onSuccess The success callback
     * @memberof module:css
     */
    function loadCssFile(cssFile, onSuccess) {
        if (cssFile.indexOf(".css") === -1) {
            cssFile += ".css";
        }

        var node = document.createElement("link");
        node.type = "text/css";
        node.rel = "stylesheet";
        node.href = cssFile;

        // inject the node, then execute the callback (assume it can be loaded)
        head.appendChild(node);
        onSuccess(cssFile);
    }

    /**
     * Load a resource.  
     * Assuming the resource IDs do not need special ID normalization.
     * @method
     * @param {String} resourceId The resource ID that the plugin should load. This ID MUST be normalized.
     * @param {Function} require A local require function to use to load other modules. This require function has some utilities on it: 
     *  * require.toUrl("moduleId+extension"). See the require.toUrl API notes for more information.
     * @param {Function} load A function to call once the value of the resource ID has been determined. This tells the loader that the plugin is done loading the resource.
     * @param {Object} [config] A configuration object. This is a way for the optimizer and the web app to pass configuration information to the plugin. An optimization tool may set an isBuild property in the config to true if this plugin (or pluginBuilder (TODOC)) is being called as part of an optimizer build.
     * @memberof module:css
     */
    exports.load = function (resourceId, require, load) {
        // load the CSS file
        loadCssFile(require.toUrl(resourceId), function (value) {
            load(value);
        });
    };

    return exports;
});

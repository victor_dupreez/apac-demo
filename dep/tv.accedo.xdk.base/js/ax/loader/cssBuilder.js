/* global java: true, fs:true, console: true, readFile: true */
/**
 * A builder of css loader plugin that will be used instead of the original css plugin during optimization.
 * During the optimization, the CSS files will be loaded.
 * When it is finished, an optimized CSS file will be outputed to the specific location.
 * This output file can be configured using the __cssDir__ and/or __cssOut__ properties in the configuration file.
 * The developers should include the ouput CSS file in their application using <link> tag (or any equivalent method).
 * <p>
 * If __cssDir__ is not specified, __baseUrl__ will be used as the output directory.<br/>
 * If __cssOut__ is not specified, the name of the bootstrap JS file will be used as the output file name.<br/>
 * The output file will be placed at {baseUrl}/{cssDir}/{cssOut}.
 * </p>
 *
 * @module ax/cssBuilder
 */
define(function () {

    "use strict";

    var config,
        cssContent = {},
        extResRegEx = /^([^\:\/]+:\/)?\//,
        exports;

    /**
     * Read the file content from the specific file.
     * @private
     * @method
     * @param fileName The full name of the file (absolute path + file name)
     * @memberof module:ax/cssBuilder
     */
    function readFileContent(fileName) {
        var content = readFile(fileName);
        return content;
    }

    /**
     * Write string content to a specific file.
     * @private
     * @method
     * @param {String} fileName The full name of the file (absolute path + file name)
     * @param {String} content The content to write
     * @memberof module:ax/cssBuilder
     */
    function writeFile(fileName, content) {
        var writer,
            index = fileName.lastIndexOf("/"),
            dirName = fileName.substring(0, index),
            dir,
            file;

        //for Node js to use the fs for handling io
        if (fs) {

            //folder handling
            if (!fs.existsSync(dirName)) {
                fs.mkdirSync(dirName);
            }

            //write the file
            fs.writeFileSync(fileName, content, "utf8");

        } else {

            dir = new java.io.File(dirName);
            file = new java.io.File(fileName);

            // create the necessary directories and the file
            dir.mkdirs();
            file.createNewFile();

            // write the content to the file using BufferedWriter & OutputStream
            writer = new java.io.BufferedWriter(new java.io.OutputStreamWriter(
                new java.io.FileOutputStream(file), "utf-8"));
            writer.write(content);

            // must close the writer to flush the output stream
            writer.close();

        }
    }

    /**
     * Normalize the module id based on the __base__ path.
     * @private
     * @method
     * @param {String} id The module id
     * @param {String} base The location where the module should relative to
     * @memberof module:ax/cssBuilder
     */
    function normalize(id, base) {
        var baseParts = base && base.split("/"),
            normalizedBaseParts,
            lastIndex;

        // normalize only if the given id is a relative id
        if (id && id.charAt(0) === ".") {
            if (base) {
                // Convert base to array, and lop off the last part,
                // so that . matches that 'directory' and not id of the base's module. 
                // For instance, base of 'one/two/three', maps to 'one/two/three.js', 
                // but we want the directory, 'one/two' for this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                id = id.split("/");
                lastIndex = id.length - 1;

                id = normalizedBaseParts.concat(id);
                trimDots(id);
                id = id.join("/");
            } else if (id.indexOf("./") === 0) {
                // no base, so the id is resolved relative to baseUrl
                // remove the leading dot
                id = id.substring(2);
            }
        }

        return id;
    }

    /**
     * Trims the . and .. from an array of path segments.
     * It will keep a leading path segment if a .. will become
     * the first path segment, to help with module name lookups,
     * which act like paths, but can be remapped. But the end result,
     * all paths that use this function should look normalized.
     * NOTE: this method MODIFIES the input array.
     * @method
     * @private
     * @param {Array} array the array of path segments
     * @memberof module:amd
     * @example
     *     trimDots([".", "this", "..", "is", ".", "a", "path"]); // becomes ["is", "a", "path"]
     *     trimDots(["..", "this", "..", "is", ".", "another", "path"]); // becomes ["..", "is", "another", "path"]
     */
    function trimDots(array) {
        var i, part, length = array.length;
        for (i = 0; i < length; i++) {
            part = array[i];
            if (part === ".") {
                array.splice(i, 1);
                i -= 1;
            } else if (part === "..") {
                if (i === 1 && (array[2] === ".." || array[0] === "..")) {
                    // end of the line.
                    // Keep at least one non-dot path segment at the front so it can be mapped correctly to disk. 
                    // Otherwise, there is likely no path mapping for a path starting with '..'.
                    // This can still fail, but catches the most reasonable uses of ..
                    break;
                } else if (i > 0) {
                    array.splice(i - 1, 2);
                    i -= 2;
                }
            }
        }
    }


    exports = {
        /**
         * Load a resource.
         * Assuming the resource IDs do not need special ID normalization.
         * @method
         * @param {String} resourceId The resource ID that the plugin should load. This ID MUST be normalized.
         * @param {Function} require A local require function to use to load other modules. This require function has some utilities on it:
         *  * require.toUrl("moduleId+extension"). See the require.toUrl API notes for more information.
         * @param {Function} load A function to call once the value of the resource ID has been determined. This tells the loader that the plugin is done loading the resource.
         * @param {Object} [configuration] A configuration object. This is a way for the optimizer and the web app to pass configuration information to the plugin. An optimization tool may set an isBuild property in the config to true if this plugin (or pluginBuilder (TODOC)) is being called as part of an optimizer build.
         * @memberof module:ax/cssBuilder
         */
        load: function (name, require, load, configuration) {
            config = config || configuration;

            // skip external resources
            if (name.match(extResRegEx)) {
                return;
            }

            cssContent[name] = readFileContent(require.toUrl(name + ".css"));
            load(name);
        },

        /**
         * Function being called by r.js when a module is parsed.
         * In this implementation a __css!name__ module is inserted into the output file.
         * By this, the amd module will not request these css modules in runtime.
         * @see {@link http://requirejs.org/docs/plugins.html#apiwrite}
         * @param {String} pluginName The plugin name
         * @param {String} name The module name
         * @param {Function} write A function to write to the output file
         * @memberof ax/cssBuilder
         */
        write: function (pluginName, name, write) {
            // skip external resources
            if (name.match(extResRegEx)) {
                return;
            }

            // define this css module in the optimized js file to prevent this module from loading in runtime
            write.asModule(pluginName + "!" + name, "define('" + pluginName + "!" + name + "',function(){})");
        },

        /**
         * Function being called by r.js when each optimization layer end.<br/>
         * In this implementation a concatenated css file is created to reduce app loading time from http requests.
         * The file will be named as same as the layer name.
         * @see {@link http://requirejs.org/docs/plugins.html#apionlayerend}
         * @method
         * @param {Function} write A function to be called with a string of output to write to the optimized layer
         * @param {Object} data The data of this layer
         * @memberof module:ax/cssBuilder
         */
        onLayerEnd: function (write, data) {
            var css = "";
            var cssOut, cssDir, outputRelPath, outputPath;

            cssOut = config.cssOut || (data.name + ".css");
            cssDir = (config.cssDir || "."); // "." = baseUrl

            outputRelPath = cssDir + "/" + cssOut;

            // normalize based on baseUrl
            outputPath = normalize(outputRelPath, config.baseUrl);

            // concatenate all css
            for (var key in cssContent) {
                css += cssContent[key];
            }

            console.log("[cssBuilder] ==> write the concatenated CSS to " + outputPath + "\n");
            writeFile(outputPath, css); // write to a local file
        }
    };

    return exports;
}); /* global java: false, fs:false,  console: false, readFile: false */
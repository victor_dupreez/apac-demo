/**
 * imageBuilder is an AMD loader plugin for optimization.
 *
 * To successfully optimize the modules, an "image" object property must be added to the optimization configuration.
 * This object has 2 properties
 *   * outDir: the output directory for the images relative to baseUrl
 *   * builtBaseUrlToImageDir: the path from baseUrl to the image directory after build
 *   * outMap: the output file for the image path map, relative to baseUrl
 *
 * After the optimization, an image map JS file will be exported according to the __outMap__ property.
 * Developer should load this file AFTER amd.js but BEFORE the actual build file.
 *
 * @module ax/loader/imageBuilder
 * @author Marco Fan <marco.fan@accedo.tv>
 * @example
 * // consider baseUrl = "src/js" and the following structure
 * // src
 * //  |-- js
 * //       |-- app.js
 * //       |-- logo.png
 *
 * // require.build.js
 * {
 *   image: {
 *     builtBaseUrlToImageDir: "../img",
 *     outDir: "../../build/img",
 *     outMap: "../../build/image.map.js"
 *   },
 *   // other configurations
 * }
 *
 * // the above configurations should produce the following
 * // build
 * //  |-- img
 * //  |-- js
 * //  |-- image.map.js
 * //  |-- index.html (this is implied by builtBaseUrlToImageDir)
 */
/* global java: true, Packages: true, fs: true, path: true, console: true */
define(function () {
    "use strict";

    var exports = {};
    var registry = {};
    var config = {
        image: {}
    };

    var REGEXP_EXTERNAL = /^([^\:\/]+:\/)?\//;


    /**
     * Trims the . and .. from an array of path segments.
     * It will keep a leading path segment if a .. will become
     * the first path segment, to help with module name lookups,
     * which act like paths, but can be remapped. But the end result,
     * all paths that use this function should look normalized.
     * NOTE: this method MODIFIES the input array.
     *
     * @method
     * @private
     * @param {Array} array the array of path segments
     * @memberof module:ax/loader/imageBuilder
     * @example
     *     trimDots([".", "this", "..", "is", ".", "a", "path"]); // becomes ["is", "a", "path"]
     *     trimDots(["..", "this", "..", "is", ".", "another", "path"]); // becomes ["..", "is", "another", "path"]
     */
    function trimDots(array) {
        var i, part, length = array.length;
        for (i = 0; i < length; i++) {
            part = array[i];
            if (part === ".") {
                array.splice(i, 1);
                i -= 1;
            } else if (part === "..") {
                if (i === 1 && (array[2] === ".." || array[0] === "..")) {
                    // end of the line.
                    // Keep at least one non-dot path segment at the front so it can be mapped correctly to disk. 
                    // Otherwise, there is likely no path mapping for a path starting with '..'.
                    // This can still fail, but catches the most reasonable uses of ..
                    break;
                } else if (i > 0) {
                    array.splice(i - 1, 2);
                    i -= 2;
                }
            }
        }
    }

    /**
     * Remove the unnecessary relative components (./ or ../) from the path, and return the updated one.
     *
     * @method
     * @private
     * @param {String} path The path
     * @memberof module:ax/loader/imageBuilder
     */
    function simplifyPath(path) {
        var comps = path.split("/");
        trimDots(comps);
        return comps.join("/");
    }

    /**
     * Normalizes the module id based on the __base__ path.
     *
     * @method
     * @private
     * @param {String} id The module id
     * @param {String} base The location where the module should relative to
     * @memberof module:ax/loader/imageBuilder
     */
    function normalize(id, base) {
        var baseParts = base && base.split("/"),
            normalizedBaseParts,
            lastIndex;

        // normalize only if the given id is a relative id
        if (id && id.charAt(0) === ".") {
            if (base) {
                // Convert base to array, and lop off the last part,
                // so that . matches that 'directory' and not id of the base's module. 
                // For instance, base of 'one/two/three', maps to 'one/two/three.js', 
                // but we want the directory, 'one/two' for this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                id = id.split("/");
                lastIndex = id.length - 1;

                id = normalizedBaseParts.concat(id);
                trimDots(id);
                id = id.join("/");
            } else if (id.indexOf("./") === 0) {
                // no base, so the id is resolved relative to baseUrl
                // remove the leading dot
                id = id.substring(2);
            }
        }

        return id;
    }

    var fileApi = {
        /**
         * Returns the directory from the full file path.
         *
         * @method
         * @private
         * @param {String} path The full file path
         * @memberof module:ax/loader/imageBuilder~fileApi
         */
        getDir: function (path) {
            return path.substring(0, path.lastIndexOf("/"));
        },

        /**
         * Creates the directory named by this abstract pathname, including any
         * necessary but nonexistent parent directories.
         * Note that if this operation fails it may have succeeded in creating
         * some of the necessary parent directories.
         *
         * @method
         * @private
         * @param {String} path The absolute path to check
         * @memberof module:ax/loader/imageBuilder~fileApi
         */
        ensureDirExist: undefined,

        /**
         * Copies the source file to a destination file.
         *
         * @method
         * @private
         * @param {String} src The absolute path of the source file
         * @param {String} dest The absolute path of the destination file
         * @throws {Error} The java exception if any is thrown
         * @memberof module:ax/loader/imageBuilder~fileApi
         */
        copyFile: undefined,

        /**
         * Writes the content to a file.
         *
         * @method
         * @private
         * @param {String} dest The absolute path of the destination file
         * @param {String} content The string content to write
         * @throws {Error} The java exception if any is thrown
         * @memberof module:ax/loader/imageBuilder~fileApi
         */
        write: undefined
    };

    // create the file API abstraction
    // assume only nodeJs and Rhino are available
    if (fs) {
        fs.mkdirParentSync = function (dir, mode) {
            try {
                fs.mkdirSync(dir, mode);
            } catch (ex) {
                // code ENOENT => parent directory does not exist
                if (ex.code === "ENOENT") {
                    // create the parent directory recursively
                    fs.mkdirParentSync(path.dirname(dir), mode);
                    // create the original requested directory
                    fs.mkdirParentSync(dir, mode);
                }
            }
        };

        fileApi.ensureDirExist = function (path) {
            var dir = fileApi.getDir(path);
            if (!fs.existsSync(dir)) {
                fs.mkdirParentSync(dir);
            }
        };

        fileApi.copyFile = function (src, dest) {
            // create the necessary directories
            fileApi.ensureDirExist(dest);

            var inputStream = fs.createReadStream(src, {
                flags: "r"
            });
            var outputStream = fs.createWriteStream(dest, {
                flags: "w"
            });

            inputStream.pipe(outputStream, {
                end: false
            });
        };

        fileApi.write = function (dest, content) {
            // create the necessary directories
            fileApi.ensureDirExist(dest);

            fs.writeFileSync(dest, content, "utf8");
        };
    } else {
        fileApi.ensureDirExist = function (path) {
            var dir = new java.io.File(fileApi.getDir(path));
            dir.mkdirs();
        };

        fileApi.copyFile = function (src, dest) {
            // create the necessary directories
            fileApi.ensureDirExist(dest);

            var inputStream, outputStream;
            var error;

            try {
                var buffer = new Packages.java.lang.reflect.Array.newInstance(java.lang.Byte.TYPE, 1024);
                inputStream = new java.io.BufferedInputStream(new java.io.FileInputStream(src));
                outputStream = new java.io.BufferedOutputStream(new java.io.FileOutputStream(dest));

                var length = inputStream.read(buffer);
                while (length !== -1) {
                    outputStream.write(buffer, 0, length);
                    length = inputStream.read(buffer);
                }
            } catch (ex) {
                error = ex;
            } finally {
                if (inputStream) {
                    inputStream.close();
                }
                if (outputStream) {
                    outputStream.close();
                }
            }

            if (error) {
                throw error;
            }
        };

        fileApi.write = function (dest, content) {
            // create the necessary directories
            fileApi.ensureDirExist(dest);

            var writer;
            var error;

            try {
                // write the content to the file using BufferedWriter & OutputStream
                var outputStream = new java.io.FileOutputStream(dest);
                writer = new java.io.BufferedWriter(new java.io.OutputStreamWriter(outputStream, "utf-8"));
                writer.write(content);
            } catch (ex) {
                error = ex;
            } finally {
                if (writer) {
                    writer.close();
                }
            }

            if (error) {
                throw error;
            }
        };
    }

    /**
     * Load a resource.
     * Assuming the resource ID do not need special ID normalization.
     *
     * @method
     * @param {String} resId The resource ID that the plugin should load.
     * @param {Function} req A local require function to use to load other modules. This require function has some utilities on it:
     *  * require.toUrl("moduleId+extension"). See the require.toUrl API notes for more information.
     * @param {Function} load A function to call once the value of the resource ID has been determined. This tells the loader that the plugin is done loading the resource.
     * @param {Object} [configuration] A configuration object. This is a way for the optimizer and the web app to pass configuration information to the plugin. An optimization tool may set an isBuild property in the config to true if this plugin (or pluginBuilder (TODOC)) is being called as part of an optimizer build.
     * @memberof module:ax/loader/imageBuilder
     */
    exports.load = function (resId, req, load, configuration) {
        config = configuration || config;

        // is bulding and not external image
        if (config.isBuild && !resId.match(REGEXP_EXTERNAL)) {
            registry[resId] = {
                require: req
            };
        }

        load(req.toUrl(resId));
    };

    /**
     * Function being called by r.js when each optimization layer end.<br/>
     * In this implementation a concatenated css file is created to reduce app loading time from http requests.
     * The file will be named as same as the layer name.
     *
     * @see {@link http://requirejs.org/docs/plugins.html#apionlayerend}
     * @method
     * @param {Function} write A function to be called with a string of output to write to the optimized layer
     * @param {Object} data The data of this layer
     * @memberof module:ax/loader/imageBuilder
     */
    exports.onLayerEnd = function (write, data) {
        if (typeof config.image !== "object") {
            throw new Error("The \"image\" property does not exist in the optimization configuration.");
        }
        if (!config.image.outDir) {
            throw new Error("The \"image.outDir\" property does not exist in the optimization configuration.");
        }
        if (!config.image.builtBaseUrlToImageDir) {
            throw new Error("The \"image.builtBaseUrlToImageDir\" property does not exist in the optimization configuration.");
        }
        if (!config.image.outMap) {
            throw new Error("The \"image.outMap\" property does not exist in the optimization configuration.");
        }

        var outDir = normalize(config.image.outDir, config.baseUrl) + "/";
        var mapOutDir = normalize(config.image.outMap, config.baseUrl);
        var baseUrlToImgDir = config.image.builtBaseUrlToImageDir;
        var imageMap = {};
        var images = [];

        // copy every image to the build directory
        for (var key in registry) {
            var resId = key;
            var req = registry[key].require;

            var absInPath = simplifyPath(req.toUrl(resId));
            var absOutPath = simplifyPath(req.toUrl(outDir + resId));

            console.log("Copying image from " + absInPath + " to " + absOutPath);
            fileApi.copyFile(absInPath, absOutPath);
            images.push(resId);
        }

        imageMap[baseUrlToImgDir] = images;
        console.log("Writing the image map to " + mapOutDir);
        fileApi.write(mapOutDir, "require({\"image\":{\"map\":" + JSON.stringify(imageMap) + "}})");
    };

    return exports;
});
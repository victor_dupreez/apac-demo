/**
 * Element wrapper class. Provides element manipulation and event functionalities.
 * @class ax/Element
 * @param {String|DOMElement} type DOM element tag name, or just DOM element instance to wrap
 * @param {Object} [attributes] attributes to assigned to DOM element
 * @param {Object} [attributes.style] CSS styles to assigned to DOM element
 * @param {ax/Element|DOMElement} [parent] parent element to append to
 * @author Thomas Lee <thomas.lee@accedo.tv>
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/Element", ["ax/class", "ax/core", "ax/console", "ax/util", "ax/device"], function (klass, core, console, util, device) {
    "use strict";
    var ElementClass, isOldMaple = (function () {
            var ua = navigator.userAgent.toLowerCase();
            if (ua.indexOf("maple") !== -1 && ua.indexOf("5.1") !== -1) {
                return true;
            }
            return false;
        }()),
        /**
         * Whether the platform supports "DOMElement.classList" property
         * @method supportsClassList
         * @private
         * @memberof ax/Element
         * @static
         */
        supportsClassList = (function () {
            // document.body is not available now, go for the one that must exist: a script element
            var scriptEle = document.getElementsByTagName("script")[0];
            return !!(scriptEle.classList && scriptEle.classList.add);
        }()),
        /**
         * Handler method for cancelling the event default action.
         * @name preventDefault
         * @private
         * @memberof ax/Element
         * @static
         */
        preventDefault = function () {
            this.returnValue = false;
        },
        /**
         * Handler method for stopping event propagation.
         * @name stopPropagation
         * @private
         * @memberof ax/Element
         * @static
         */
        stopPropagation = function () {
            this.cancelBubble = true;
        },
        /**
         * Fix up an event so that can use W3C standard methods to cancel the event and stop event propagation.
         * Brought in a lot of ideas from Dean Edwards' addEvent()
         * @name fixEvent
         * @param {Event} evt
         * @returns {Event}  the updated native event object
         * @private
         * @memberof ax/Element
         * @static
         */
        fixEvent = function (evt) {
            // add W3C standard event methods
            if (!evt.preventDefault) {
                evt.preventDefault = preventDefault;
            }
            if (evt.stopPropagation) {
                evt.stopPropagation = stopPropagation;
            }
            return evt;
        },
        /**
         * Map from pseudo events to real attachable event types.
         * @name pseudoEvents
         * @private
         * @memberof ax/Element
         * @static
         */
        pseudoEvents = {
            mouseenter: "mouseover",
            mouseleave: "mouseout"
        },
        /**
         * Wraps a pseudo event listener, makes it possible to be attached onto a browser event.
         * @name wrapPseudoEventListener
         * @param type the pseudo event type
         * @param listener the pseudo event listener function
         * @returns {Function} the wrapped listener function, which is ready to be attached to a browser event
         * @private
         * @memberof ax/Element
         * @static
         */
        wrapPseudoEventListener = function (type, listener) {
            if (!pseudoEvents[type]) { // no such pseudo event
                return null;
            }

            return function (evt) {
                var related = evt.relatedTarget,
                    origType = evt.type;
                while (related && related !== this._dom) { // search up DOM tree from related target
                    try {
                        related = related.parentNode;
                    } catch (e) {
                        break;
                    }
                }
                if (related !== this._dom) { // a real entrance/leave
                    evt.type = type;
                    listener.apply(this, arguments);
                    evt.type = origType;
                }
            };
        };

    ElementClass = klass.create({
            /**
             * Gets the element by ID and returns a corresponding wrapper or null of element does not exist.
             * @method getById
             * @memberof ax/Element
             * @param {String} id String ID of the element in the DOM
             * @return {ax/Element} Element wrapper
             * @static
             */
            getById: function (id) {
                var _dom = document.getElementById(id);
                if (_dom && util.isDOMElement(_dom)) {
                    return new ElementClass(_dom);
                }
                return null;
            }
        },
        /** @lends ax/Element.prototype */
        {
            /**
             * The underlying HTML DOM element.
             * @member _dom
             * @protected
             * @memberof ax/Element#
             */
            _dom: null,
            /**
             * The container for event listeners.
             * @member __event
             * @private
             * @memberof ax/Element#
             */
            __events: null,
            /**
             * Store the style display of the element for hide/show purpose
             * @member __origDisplayStyle
             * @private
             * @memberof ax/Element#
             */
            __origDisplayStyle: "",
            /**
             * constructs ax/Element
             */
            init: function (type, attributes, parent) {
                if (util.isDOMElement(type)) {
                    this._dom = type;
                } else {
                    this._dom = document.createElement(type);
                }

                //Parse and set attributes as requested
                if (util.isPlainObject(attributes)) {
                    this.attr(attributes);
                }


                //Append child to parent if possible
                if (parent) {
                    //if the parent is DOMElement and then we create and then append
                    if (parent && util.isDOMElement(parent)) {
                        parent = new ElementClass(parent);
                    }
                    parent.append(this._dom);
                }
            },
            /**
             * Returns the actual HTML Element.
             * @method getHTMLElement
             * @returns {Element} the actual HTML Element
             * @public
             * @memberof ax/Element#
             */
            getHTMLElement: function () {
                return this._dom;
            },
            /**
             * Gets parent element wrapped in DOM util.
             * @method getParent
             * @returns {Element} the parent HTML Element
             * @public
             * @memberof ax/Element#
             */
            getParent: function () {
                var parent = this._dom.parentNode;
                if (parent) {
                    return new ElementClass(parent);
                }

                return null;
            },
            /**
             * Detach element from the DOM tree.
             * @method detach
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            detach: function () {
                //no dom object since it is already detached or hasn't attached to dom yet
                if (!this._dom) {
                    return this;
                }
                var parent = this._dom.parentNode,
                    child;
                if (parent) {
                    //Prevent memory leaks in Samsung devices using a left-hand operand
                    child = parent.removeChild(this._dom);
                }

                child = null;
                parent = null;
                return this;
            },
            /**
             * Sets the element as depending on its parent (so it shows it, if its parent is not hidden itself)
             * @method show
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            show: function () {
                var originalDisplay = this._dom.style.display;
                if (originalDisplay !== "none") {
                    this.__origDisplayStyle = originalDisplay;
                    return this;
                }
                this._dom.style.display = this.__origDisplayStyle;
                return this;
            },
            /**
             * Hides element
             * @method hide
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            hide: function () {
                var originalDisplay = this._dom.style.display;
                if (originalDisplay !== "none") {
                    this.__origDisplayStyle = originalDisplay;
                }
                this._dom.style.display = "none";
                return this;
            },
            /**
             * Hides or shows the element (if its display is none, sets it to inherit, otherwise set it to none.
             * It means you'll lose the current value by toggling twice, if display was "inline" or "block" for instance)
             * @method toggle
             * @public
             * @memberof ax/Element#
             * @return {boolean} false if the element is hidden after toggling, true otherwise
             */
            toggle: function () {
                if (this._dom.style.display === "none") {
                    this.show();
                    return true;
                }
                this.hide();
                return false;
            },
            /**
             * Add event listener to the DOM element.
             * The listener function will have "this" pointing to current element. Also added "mouseenter" and "mouseleave" as pseudo events.
             * @see {@link http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html#event-type-mouseenter}
             * @see {@link http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html#event-type-mouseleave}
             * @method addEventListener
             * @param {String} type Event type
             * @param {Function} listener Function to call back on event fire
             * @param {Boolean} useCapture Use capture?
             * @return {Element} The element itself, for chaining
             * @public
             * @example
             * myElement.addEventListener("mouseenter",
             *     function(evt){
             *         console.log(this.getInnerText());
             *         },
             *     true);
             * @memberof ax/Element#
             */
            addEventListener: function (type, listener, useCapture) {
                var element = this._dom,
                    events = this.__events,
                    pseudoType = type,
                    origListener = listener,
                    listeners, listenerEntry;

                useCapture = !!useCapture;

                // wrap up pseudo-event listners such as "mouseenter" and "mouseleave"
                if (pseudoEvents[pseudoType]) {
                    type = pseudoEvents[pseudoType];
                    listener = wrapPseudoEventListener(pseudoType, listener);
                }
                listener = util.bind(listener, this);

                // assign each event handler a unique ID
                // so that they can be removed later to prevent memory leaks on some devices
                if (!origListener.$$guid) {
                    origListener.$$guid = core.getGuid();
                }

                // create a hash table of event types for the element
                if (!events) {
                    this.__events = events = {};
                }
                // create a hash table of event handlers for each element/event pair
                listeners = events[pseudoType];
                if (!listeners) {
                    listeners = events[pseudoType] = {};
                    // if not handling pseudo event currently, store the existing event handler (if there is one)
                    if (pseudoType === type && element["on" + type]) {
                        listeners[0] = {};
                        // not using capture for onXXX event
                        listeners[0][false] = [element["on" + type]];
                    }
                }

                // store the event handler in the hash table
                listeners[origListener.$$guid] = listeners[origListener.$$guid] || {};
                listenerEntry = listeners[origListener.$$guid];
                listenerEntry[useCapture] = listenerEntry[useCapture] || [];
                listenerEntry[useCapture].push(listener);

                if (element.addEventListener) {
                    element.addEventListener(type, listener, useCapture);
                } else { // use home-baked handler that calls all registered handlers
                    element["on" + type] = util.bind(this._handleEvent, this);
                }
                return this;
            },
            /**
             * Handles an event in case native addEventListener is not supported.
             * @method _handleEvent
             * @returns {boolean} false if any of the handlers returned false
             * @private
             * @memberof ax/Element#
             */
            _handleEvent: function (evt) {
                var returnValue = true,
                    type = evt.type,
                    handlers, entry, i, j;
                // grab the event object (IE uses a global event object)
                evt = evt || fixEvent(window.event);
                // get a reference to the hash table of event handlers
                handlers = this.__events[type];
                if (pseudoEvents[type]) { // also check pseudo events
                    handlers = handlers.concat(this.__events[pseudoEvents[type]]);
                }
                // execute each event handler
                for (i in handlers) {
                    // not using capture for onXXX event
                    entry = handlers[i][false.toString()];
                    for (j in entry) {
                        this.$$handleEvent = entry[j];
                        if (this.$$handleEvent(evt) === false) {
                            returnValue = false;
                        }
                    }
                }
                return returnValue;
            },
            /**
             * Removes event listener from the DOM util.
             * @method removeEventListener
             * @param {String} [type] Event type, removes all event listeners if not provided
             * @param {Function} [listener] Function to call back on event fire, removes all event listeners of the specified type if not provided
             * @param {Boolean} [useCapture] Use capture or not
             * @return {ax/Element} The element itself, for chaining
             * @public
             * @memberof ax/Element#
             */
            removeEventListener: function (type, listener, useCapture) {
                var element = this._dom,
                    events = this.__events,
                    wrappedListeners = null,
                    attachedListener = listener,
                    attachedType = type,
                    listeners, curType, curGuid, curCapture;



                if (!type) { // remove all listeners
                    if (!events) {
                        return this;
                    }
                    for (curType in events) {
                        this.removeEventListener(curType);
                    }
                    this.__events = null; // remove all listeners
                    return this;
                }

                if (!listener) { // remove all listeners of certain type
                    if (!events) {
                        return this;
                    }
                    listeners = events[type];
                    for (curGuid in listeners) {
                        for (curCapture in listeners[curGuid]) {
                            // actually, for second param, all we need is the guid
                            while (listeners[curGuid] && listeners[curGuid][curCapture] && listeners[curGuid][curCapture].length) {
                                this.removeEventListener(type, {
                                    $$guid: curGuid
                                }, curCapture === true.toString());
                            }
                        }
                    }
                    delete events[type]; // remove whole type
                    return this;
                }

                // just remove one specific listener
                useCapture = !!useCapture;
                if (events && events[type] && events[type][listener.$$guid] && events[type][listener.$$guid][useCapture]) {

                    wrappedListeners = events[type][listener.$$guid][useCapture];
                    attachedListener = wrappedListeners.pop();
                    attachedType = pseudoEvents[type] || type;

                    if (wrappedListeners.length === 0) {
                        // delete current specific entry, if no more listeners in there
                        delete events[type][listener.$$guid][useCapture];
                        // delete whole Guid entry, if no more listeners in there
                        if (!events[type][listener.$$guid][!useCapture] || events[type][listener.$$guid][!useCapture].length === 0) {
                            delete events[type][listener.$$guid];
                        }
                    }

                }
                if (element.removeEventListener) {
                    element.removeEventListener(attachedType, attachedListener, useCapture);
                }
                return this;
            },
            /**
             * Removes all event listeners from the DOM util.
             * @method removeAllListeners
             * @return {Element} The element itself, for chaining
             * @public
             * @memberof ax/Element#
             */
            removeAllListeners: function () {
                if (!this.__events) { // for better performance
                    return this;
                }
                this.removeEventListener();
                return this;
            },
            /**
             * This function removes a child node from a parent node.
             * @method removeChild
             * @param {Element} child The child node
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            removeChild: function (child) {
                if (!util.isDOMElement(child)) {
                    child = child.getHTMLElement();
                }

                //to ensure the it is child of its parent
                if (child.parentNode !== this._dom) {
                    console.warn("unable to remove the non children node");
                    return this;
                }

                if (util.isFunction(this._dom.removeChild)) {
                    this._dom.removeChild(child);
                } else {
                    util.each(
                        Array.prototype.slice.call(this._dom.childNodes, 0), function (_child) {
                            if (_child === child) {
                                //Prevent memory leaks in Samsung devices using a left-hand operand
                                var myChild = this._dom.removeChild(_child);
                                myChild = null;
                            }
                        }, this);
                }
                return this;
            },
            /**
             * This function removes all child nodes from a parent node.
             * @method removeAll
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            removeAll: function () {

                var child = this._dom.childNodes[0];
                while (child) {
                    this.removeChild(child);
                    child = this._dom.childNodes[0];
                }
                return this;
            },
            /**
             * This function appends a child node to a parent node.
             * @method append
             * @param {Element | ax/Element} child The child node
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            append: function (child) {
                if (util.isDOMElement(child)) {
                    this._dom.appendChild(child);
                } else {
                    this._dom.appendChild(child.getHTMLElement());
                }
                return this;
            },
            /**
             * Append to a parent
             * @method appendTo
             * @param {Element | ax/Element} parent the parent node
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            appendTo: function (parent) {
                if (util.isDOMElement(parent)) {
                    parent.appendChild(this._dom);
                } else {
                    parent.append(this);
                }
                return this;
            },
            /**
             * Inserts a new child node to this element, before one of its children.
             * @method insertBefore
             * @param {Element | ax/Element} existingChild The existing child node
             * @param {Element | ax/Element} newChild The new child node
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            insertBefore: function (existingChild, newChild) {
                var existingC, newC;
                existingC = util.isDOMElement(existingChild) ? existingChild : existingChild.getHTMLElement();
                newC = util.isDOMElement(newChild) ? newChild : newChild.getHTMLElement();
                this._dom.insertBefore(newC, existingC);
                return this;
            },
            /**
             * Inserts a new child node to this element, after one of its children.
             *
             * @method insertAfter
             * @param {Element | ax/Element} existingChild The existing child node
             * @param {Element | ax/Element} newChild The new child node
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            insertAfter: function (existingChild, newChild) {
                var existingC, newC;
                existingC = util.isDOMElement(existingChild) ? existingChild : existingChild.getHTMLElement();
                newC = util.isDOMElement(newChild) ? newChild : newChild.getHTMLElement();
                if (this._dom.lastChild === existingC) {
                    this._dom.appendChild(newC);
                } else {
                    this._dom.insertBefore(newC, existingC.nextSibling);
                }
                return this;
            },
            /**
             * This function attach a new child node to a parent node, replacing one of its children.
             * @method replaceChild
             * @param {Element | ax/Element} existingChild The existing child node
             * @param {Element | ax/Element} newChild The new child node
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            replaceChild: function (existingChild, newChild) {
                var existingC, newC, sibling;
                existingC = util.isDOMElement(existingChild) ? existingChild : existingChild.getHTMLElement();
                newC = util.isDOMElement(newChild) ? newChild : newChild.getHTMLElement();
                if (this._dom.lastChild === existingC) {
                    this._dom.removeChild(existingC);
                    this._dom.appendChild(newC);
                } else {
                    sibling = existingC.nextSibling;
                    this._dom.removeChild(existingC);
                    this._dom.insertBefore(newC, sibling);
                }
                return this;
            },
            /**
             * This function appends a new child node to a parent node, before the first child.
             * @method prepend
             * @param {Element | ax/Element} newChild The new child node
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            prepend: function (newChild) {
                if (util.isDOMElement(newChild)) {
                    this._dom.insertBefore(newChild, this._dom.firstChild);
                } else {
                    this._dom.insertBefore(newChild.getHTMLElement(), this._dom.firstChild);
                }
                return this;
            },
            /**
             * Prepend itself to the begining of the taret
             * @method prependTo
             * @param {Element | ax/Element} parent the parent node
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            prependTo: function (parent) {
                if (util.isDOMElement(parent)) {
                    parent.insertBefore(this._dom, parent.childNodes[0]);
                } else {
                    parent.prepend(this);
                }
                return this;
            },
            /**
             * This function finds out whether this node is attached to a parent or not.
             * @method isAttached
             * @public
             * @memberof ax/Element#
             * @returns {Boolean} True when it is attached. Otherwise return false
             */
            isAttached: function () {
                return this.getHTMLElement().parentNode !== null;
            },
            /**
             * This function removes an attribute from an DOM element.
             * @method removeAttr
             * @param {String} attribute The attribute
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            removeAttr: function (attribute) {
                if (this.hasAttr(attribute)) {
                    this._dom.removeAttribute(attribute);
                }
                return this;
            },
            /**
             * This function removes a style definition from an DOM element.
             * @method removeStyle
             * @param {String} styleName The style name
             * @return {ax/Element} the Element itself
             * @public
             * @memberof ax/Element#
             */
            removeStyle: function (styleName) {
                if (this.css(styleName)) {
                    this._dom.style[styleName] = "";
                    //it is unable to delete the style e.g style["display"]
                    delete this._dom.style[styleName];
                }
                return this;
            },
            /**
             * This function checks if an element has a certain attribute defined or not.
             * @method hasAttr
             * @param {String} attribute The attribute
             * @return {Boolean} True if it contains certain attribute.
             * @public
             * @memberof ax/Element#
             */
            hasAttr: function (attribute) {
                if (this._dom.hasAttribute) {
                    return this._dom.hasAttribute(attribute);
                }
                return !util.isUndefined(this._dom[attribute]);
            },
            /**
             * Gets the style text of the element
             * @method __getStyleText
             * @return {String} the style text of the element
             * @memberof ax/Element#
             * @private
             */
            __getStyleText: function () {
                if (!util.isUndefined(this._dom.style.cssText)) {
                    return this._dom.style.cssText;
                } else {
                    return this._dom.getAttribute("style");
                }
            },
            /**
             * Sets the style text of the element, will overwrites the original text
             * @method __setStyleText
             * @function
             * @param {String} styleText the style text to set into the element
             * @memberof ax/Element#
             * @private
             */
            __setStyleText: function (styleText) {
                if (!util.isUndefined(this._dom.style.cssText)) {
                    this._dom.style.cssText = styleText;
                } else {
                    this._dom.setAttribute("style", styleText);
                }
            },
            /**
             * This function sets style attributes.
             * @method __setStyle
             * @param {Object} styles A object containing CSS name/value pairs
             * @return {Element} The element itself, for chaining
             * @private
             * @memberof ax/Element#
             */
            __setStyle: function (styles) {
                var style, curStyleStr, curCSS = this.__getStyleText() || "",
                    newCSS = curCSS,
                    isCurCSSEmpty = !curCSS,
                    overrideArr = [],
                    key;
                // set style text way does not work at all on Samsung 2010
                if (isOldMaple) {
                    for (style in styles) {
                        if (styles.hasOwnProperty(style)) {
                            this._dom.style[style] = styles[style];
                        }
                    }
                    return this;
                }

                for (style in styles) {
                    if (styles.hasOwnProperty(style)) {
                        style = util.camelize(style);
                        curStyleStr = util.decamelize(style, "-") + ":" + styles[style] + ";";
                        if (isCurCSSEmpty) {
                            newCSS += curStyleStr;
                        } else if (!util.isNull(this._dom.style[style]) && !util.isUndefined(this._dom.style[style]) && this._dom.style[style] !== "") { // existing style
                            overrideArr.push(style);
                        } else { // non-existing style
                            newCSS = curStyleStr + newCSS;
                        }
                    }
                }
                this.__setStyleText(newCSS); // set new css in batch
                for (key in overrideArr) { // override old ones one by one
                    style = overrideArr[key];
                    this._dom.style[style] = styles[style];
                }
                return this;
            },
            /**
             * Gets a given style for the DOM element.
             * @method __getStyle
             * @memberof ax/Element#
             * @param {String} style Style name to get.
             * @private
             * @return style value or null
             */
            __getStyle: function (style) {
                style = style === "float" ? "cssFloat" : util.camelize(style);
                var value = null;
                if (this._dom.currentStyle) {
                    value = this._dom.currentStyle[style];
                }
                //in opera currentStyle exist but it fails to get the style so it needs to ensure by checking style.
                if (!value) {
                    value = this._dom.style[style];
                }
                if (style === "opacity") {
                    return value ? parseFloat(value) : 1.0;
                }
                return value === "auto" ? null : value;
            },
            /**
             * Gets Cumulative offset for element.
             * Returned data is an object with x and y properties.
             * @method cumulativeOffset
             * @public
             * @memberof ax/Element#
             * @return {Object} Offset object with x and y properties
             */
            cumulativeOffset: function () {
                var valueT = 0,
                    valueL = 0,
                    element = this._dom;
                if (element.parentNode) {
                    do {
                        valueT += element.offsetTop || 0;
                        valueL += element.offsetLeft || 0;
                        element = element.offsetParent;
                    } while (element);
                }
                return {
                    x: valueL,
                    y: valueT
                };
            },
            /**
             * This function adds one or more CSS classes to the given element.
             * @method addClass
             * @param {String | Array} className Either an array of CSS classname or a single CSS classname
             * @return {Element} The element itself, for chaining
             * @public
             * @memberof ax/Element#
             */
            addClass: function (className) {
                if (util.isArray(className)) {
                    util.each(className, function (def) {
                        this.addClass(def);
                    }, this);
                } else if (util.isString(className)) {
                    //Remove extra space to avoid DOM exception
                    className = className.replace(/\s+/g, " ").replace(/^\s/, "").replace(/\s$/, "");
                    if (className.indexOf(" ") >= 0) { // className contains space and multiple classes, need to chop them up
                        util.each(className.split(" "), function (def) {
                            this.addClass(def);
                        }, this);
                    } else if (supportsClassList) { // platform supports classList feature, so just use it
                        this._dom.classList.add(className);
                    } else if (!this.hasClass(className)) { // do it the old fashioned way
                        //Either add space or do not add space before appending className
                        this._dom.className += (this._dom.className ? " " : "") + className;
                    }
                }

                return this;
            },
            /**
             * Checks if the element has a CSS Class name assigned.
             * @method hasClass
             * @function
             * @public
             * @memberof ax/Element#
             * @param {String} className CSS Class name to look for.
             * @return {Boolean} true if element has class name, false otherwise
             */
            hasClass: function (className) {
                if (supportsClassList) { // platform supports classList feature, so just use it
                    return this._dom.classList.contains(className);
                }

                var elemClass = this._dom.className;
                return (elemClass.length > 0 && (elemClass === className || new RegExp("(^|\\s)" + className + "(\\s|$)").test(elemClass)));
            },
            /**
             * Removes a given CSS class name from the element.
             * @method removeClass
             * @function
             * @public
             * @memberof ax/Element#
             * @param {String} className CSS Class name to remove from the element.
             * @return {Element} The element itself, for chaining
             */
            removeClass: function (className) {
                if (supportsClassList) { // platform supports classList feature, so just use it
                    this._dom.classList.remove(className);

                    return this;
                }

                var cname = this._dom.className.replace(new RegExp("(^|\\s+)" + className + "(\\s+|$)"), " ");
                cname = util.strip(cname);
                this._dom.className = cname;

                return this;
            },
            /**
             * Replaces the current CSS class(es) with given the given className
             * @method setClass
             * @function
             * @param {String} className CSS Classname
             * @return {Element} The element itself, for chaining
             * @public
             * @memberof ax/Element#
             */
            setClass: function (className) {
                this._dom.className = util.strip(className);
                return this;
            },
            /**
             * This function sets the textual content of a DOM element. HTML code should be used setInnerHTML instead of.
             * @method setInnerText
             * @param {String} content The text content to set
             * @return {Element} The element itself, for chaining
             * @public
             * @memberof ax/Element#
             */
            setInnerText: function (content) {
                if (!util.isUndefined(this._dom.innerText)) {
                    this._dom.innerText = content;
                } else if (!util.isUndefined(this._dom.textContent)) {
                    // firefox is unable to set InnerText and textContent only available
                    this._dom.textContent = content;
                } else {
                    console.warn("fail to set Text" + content);
                }

                if (device.system) {
                    device.system.redraw(this._dom);
                }
                return this;
            },
            /**
             * This function sets the innerHTML of the element
             * @method setInnerHTML
             * @param {String} html content The text content to set
             * @return {Element} The element itself, for chaining
             * @public
             * @memberof ax/Element#
             */
            setInnerHTML: function (html) {
                this._dom.innerHTML = html;
                if (device.system) {
                    device.system.redraw(this._dom);
                }
                return this;
            },
            /**
             * This function returns the textual content of a DOM element.
             * @method getInnerText
             * @returns {String} the text content of the element
             * @public
             * @memberof ax/Element#
             */
            getInnerText: function () {
                if (!util.isUndefined(this._dom.innerText)) {
                    return this._dom.innerText;
                }

                //firefox is unable to set InnerText and textContent only available
                if (!util.isUndefined(this._dom.textContent)) {
                    return this._dom.textContent;
                }
                console.warn("Fail to get innerText!");
                return undefined;
            },
            /**
             * This function returns the innerHTML of a DOM element.
             * @method getInnerHTML
             * @returns {String} the innerHTML of the element
             * @public
             * @memberof ax/Element#
             */
            getInnerHTML: function () {
                return this._dom.innerHTML;
            },
            /**
             * Get the farthest-back ancestor of our node
             * @method _findUltimateAncestor
             * @return {Element} the farthest-back ancestor
             * @private
             * @memberof ax/Element#
             */
            _findUltimateAncestor: function () {
                var ancestor = this._dom;
                while (ancestor.parentNode) {
                    ancestor = ancestor.parentNode;
                }
                return ancestor;
            },
            /**
             * If the current element is in DOM tree.
             * @method isInDOMTree
             * @return {Boolean} True if it is inside the DOM tree
             * @public
             * @memberof ax/Element#
             */
            isInDOMTree: function () {
                return this._findUltimateAncestor().body === core.root.document.body;
            },
            /**
             * To delete, remove all the event listeners and detach from DOM.
             * @method discard
             * @public
             * @memberof ax/Element#
             */
            discard: function () {
                this.detach();
                this.removeAllListeners();
                this._dom = null;
            },
            /**
             * This function signals this element will not be used any more, and should collect memory to prevent any memory leaks.
             * @method deinit
             * @public
             * @memberof ax/Element#
             */
            deinit: function () {
                this.discard();
            },
            /**
             * To set or get the attribute of the element
             * @method attr
             * @param {Object|String} name Object to set a set of attributes to the element, or the attribute name to set/get
             * @param {String} [value] To set the string/number of the attribute
             * @return {Element|String} Return element when setAttribute while return string or null when getAttribute
             * @example <caption>To get the attribtue </caption>
             * element.attr("id");
             * //return the id of the element
             * <caption>To set the attribute using plainObject</caption>
             * element.attr({"alt":"desc","testing":123"});
             * //return itself and set successfully
             * <caption>To set the attribute</caption>
             * element.attr("alt","testing");
             * //return itself and set successfully
             * @public
             * @memberof ax/Element#
             */
            attr: function (name, value) {
                var i;
                //if attribute name is undefined
                if (util.isUndefined(name)) {
                    throw core.createException("elementAttrError", "Undefined attribute name");
                }

                //if it is plain object, then set attributes
                if (util.isPlainObject(name)) {
                    for (i in name) {
                        this.attr(i, name[i]);
                    }
                    return this;
                }

                //to determine whether it is get/set. If the value is undefined, then it is get attr function
                if (util.isUndefined(value)) {
                    //use the default get attribute
                    if (util.isFunction(this._dom.getAttribute)) {
                        return this._dom.getAttribute(name);
                    }

                    //if no getAttribute and fail to retrieve the attribute value, it will return null
                    return null;
                }

                //set Attribute of string/value
                if (util.isString(value) || util.isNumber(value)) {
                    this._dom.setAttribute(name, value);
                    return this;
                }
                if (name === "style" && util.isPlainObject(value)) {
                    this.css(value);
                }

                return this;
            },
            /**
             * To set and get the css of the element
             * @method css
             * @param {Object} name Object to set a set of css attributes to the element, or the css attribute to set/get
             * @param {String} [value] To set the string/number of the attribute
             * @return {Element|String} Return element when set css while return string or null when get css
             * @example <caption>To get the css attribtue </caption>
             * element.css("background");
             * //return the bg color of the element
             * <caption>To set the css attribute using plainObject</caption>
             * element.css({"background":"green","display":inline-block"});
             * //return itself and set successfully
             * <caption>To set the css attribute</caption>
             * element.css("background","yellow");
             * //return itself and set successfully
             * @public
             * @memberof ax/Element#
             */
            css: function (name, value) {
                //if attribute name is undefined
                if (util.isUndefined(name)) {
                    throw core.createException("elementCssError", "Undefined css name");
                }

                //if it is plain object, then set attributes
                if (util.isPlainObject(name)) {
                    if (name.display) {
                        if (name.display === "none") {
                            this.__origDisplayStyle = this._dom.style.display;
                        } else {
                            this.__origDisplayStyle = value;
                        }
                    }
                    this.__setStyle(name);
                    return this;
                }

                //to determine whether it is get/set. If the value is undefined, then it is get css function
                if (util.isUndefined(value)) {
                    //use the default get attribute
                    return this.__getStyle(name);
                }

                if (name === "display") {
                    if (value === "none") {
                        this.hide();
                    } else {
                        this.__origDisplayStyle = value;
                        this._dom.style.display = this.__origDisplayStyle;
                    }
                }

                //set Attribute of string/number value
                if (util.isString(value) || util.isNumber(value)) {
                    var styleObj = {};
                    styleObj[name] = value;
                    this.__setStyle(styleObj);
                    return this;
                }

                return this;
            },
            /**
             * Toogle the css class of the element.
             * The class will be added if it doesn't exist, or removed if it exists already.
             *
             * @method toggleClass
             * @param {String} className The css class name to toggle
             * @return {Element} The element itself, for chaining
             * @public
             * @memberof ax/Element#
             */
            toggleClass: function (className) {
                return (this.hasClass(className)) ? this.removeClass(className) : this.addClass(className);
            }
        });

    return ElementClass;
});
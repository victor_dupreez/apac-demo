/**
 * The device handler interface to resolve the state of the application when the platform is detected and application is loaded, paused, resumed or unloaded.
 *
 * @class ax/interface/DeviceHandler
 */
define("ax/interface/DeviceHandler", ["ax/Interface"], function (Interface) {
    "use strict";

    return Interface.create("DeviceHandler", {
        /**
         * when the platform is detected before loading the device package
         * @name onDeviceDetect
         * @function
         * @param {String} platform the id of the platform
         * @memberof ax/interface/DeviceHandler
         * @public
         */
        onDeviceDetect: ["platform"],
        /**
         * when the device package is loaded and application is ready to load
         * @name onDeviceLoad
         * @function
         * @param {Boolean} [isPaused=false] whether the device load again from pause state
         * @memberof ax/interface/DeviceHandler
         * @public
         */
        onDeviceLoad: ["isPaused"],
        /**
         * when the application pause
         * @name onDevicePause
         * @function
         * @memberof ax/interface/DeviceHandler
         * @public
         */
        onDevicePause: [],
        /**
         * when the application resume
         * @name onDeviceResume
         * @function
         * @memberof ax/interface/DeviceHandler
         * @public
         */
        onDeviceResume: [],
        /**
         * when the application unloads
         * @name onDeviceUnload
         * @function
         * @memberof ax/interface/DeviceHandler
         * @public
         */
        onDeviceUnload: [],
        /**
         * Handle the onKey from the device package, it will then dispatch the onKey event to the appliation.
         * @name onDeviceKey
         * @function
         * @memberof ax/interface/DeviceHandler
         * @param {Object} tvKey the key object of {@link module:ax/device/shared/keyboardVKey} or {@link module:ax/device/VKey}
         * @param {String} source the source of the key event. Default will be "device"
         * @public
         */
        onDeviceKey: []
    });
});
/**
 * MemCache is a utility for caching.
 * MemCache accepts any type of data to be cached. The cache has a size limit, counted in the number of entries.
 * Various configuration can be set during construction. See _init_ for details.
 * Besides a global TTL set for the entire cache, specific TTL value can be set for each object. See _set_ for details.
 *
 * @name MemCache
 * @class ax/MemCache
 * @author Marco Fan <marco.fan@accedo.tv>
 * @author Andy Hui <andy.hui@accedo.tv>
 *
 * @example
 * // Construct a cache that caches the entries forever unless exceed cache limit:
 * var cache = new MemCache({
 *         ttl: MemCache.TTL.FOREVER,
 *         gc: MemCache.GC.EXCEED_LIMIT
 *     });
 *
 * // Construct a cache that caches the entries for 30 minutes:
 * var cache = new MemCache({
 *         ttl: 30 * 60
 *     });
 */
define("ax/MemCache", [
    "ax/class",
    "ax/core",
    "ax/promise",
    "ax/console",
    "ax/util"
], function(
    klass,
    core,
    promise,
    console,
    util) {
    "use strict";

    var MemCache = klass.create({

            /**
             * Enums for garbage collection
             * @name GC
             * @type {Object}
             * @memberOf ax/MemCache
             * @static
             **/
            GC: {
                /**
                 * Garbage collect on expiry
                 * @name EXPIRY
                 * @type {Number}
                 * @memberOf ax/MemCache.GC
                 **/
                EXPIRY: 0,

                /**
                 * Garbage collect on limit exceeded only, expired items remains in memory
                 * @name EXCEED_LIMIT
                 * @type {Number}
                 * @memberOf ax/MemCache.GC
                 **/
                EXCEED_LIMIT: 1
            },

            /**
             * Available codes of cache TTL setting
             * @name TTL
             * @type {object}
             * @memberOf ax/MemCache
             * @static
             */
            TTL: {
                NO_CACHE: -2,
                FOREVER: -1,
                DEFAULT: 0
            }
        },

        {
            /**
             * The time of how long the cached object will stay valid
             * @member __ttl
             * @memberOf ax/MemCache#
             * @private
             */
            __ttl: null,

            /**
             * The maximum number of the objects that can be cached
             * @member __size
             * @memberOf ax/MemCache#
             * @private
             */
            __size: null,

            /**
             * The storage for the cached objects.
             * @member __cache
             * @memberOf ax/MemCache#
             * @private
             */
            __cache: null,

            /**
             * The perferred garbage collection option
             * @member __gc
             * @memberOf ax/MemCache#
             * @private
             */
            __gc: null,

            /**
             * The interval object that triggers garbage collector
             * @member __gcInterval
             * @memberOf ax/MemCache#
             * @private
             */
            __gcInterval: null,

            /**
             * Time for each gc interval
             * @member __gcIntervalTime
             * @memberOf ax/MemCache#
             * @private
             */
            __gcIntervalTime: null,

            /**
             * Init the cache helper, set the cache, and time of purging
             * @param {Object} [opts] Object to be set by using the different attributes
             * @param {Number|ax/MemCache.TTL} [opts.ttl=300]  The live time for the
             * cached objects, counted in seconds.
             * @param {Number} [opts.size=100]  The maximum number of entries for the cache.
             * @param {ax/MemCache.GC} [opts.gc] garbage collection mechanism. Default to
             * ax/MemCache.GC.EXPIRY
             * @param {Number} [opts.gcIntervalTime=5000] interval to do expired item
             * garbage collect. Default to 5 seconds.
             * @memberOf ax/MemCache#
             */
            init: function(opts) {
                opts = opts || {};
                this.__ttl = opts.ttl || 5 * 60;
                this.__size = opts.size || 100;
                this.__gc = opts.gc || this.constructor.GC.EXPIRY;
                this.__gcIntervalTime = opts.gcIntervalTime || 5000;

                this.__cacheReference = [];
                this.__cache = {};


                if (this.__gc === MemCache.GC.EXPIRY) {
                    this.__gcInterval = setInterval(util.bind(this.__garbageCollector, this),
                        this.__gcIntervalTime);
                }
            },

            /**
             * Garbage collector to check if each key in the cache is expired and remove from cache
             * @memberOf ax/MemCache#
             * @private
             */
            __garbageCollector: function() {
                var cacheRef = this.__cacheReference;

                util.each(cacheRef, util.bind(function(key, idx) {
                    if (this.isExpired(key)) {
                        delete this.__cache[key];
                        util.remove(cacheRef, idx);
                    }
                }, this));
            },

            /**
             * Save obj to cache with a key. A specific TTL can be set for this particular object.
             * @param {String} key The unique key for this cache
             * @param {Object} obj The actual data to be cached
             * @param {Number|ax/MemCache.TTL} [customTTL] The specific TTL for this object
             * @memberOf ax/MemCache#
             */
            set: function(key, obj, customTTL) {
                // if the object is set to no cache, then return without saving
                if (customTTL && customTTL === MemCache.TTL.NO_CACHE) {
                    return;
                }

                var cacheRef = this.__cacheReference,
                    currentSize = util.size(cacheRef),
                    index = util.indexOf(cacheRef, key),
                    ttl = customTTL || this.__ttl,
                    timestamp = new Date().getTime();

                if (index > -1) {
                    util.remove(cacheRef, index);
                } else if (currentSize && currentSize === this.__size) {
                    delete this.__cache[cacheRef[0]];
                    util.remove(cacheRef, 0);
                }
                cacheRef.push(key);

                this.__cache[key] = {
                    data: obj,
                    timestamp: timestamp,
                    expiration: ttl === MemCache.TTL.FOREVER ? MemCache.TTL.FOREVER : (timestamp + ttl * 1000)
                };

                if (customTTL) {
                    this.__cache[key].ttl = customTTL;
                }
            },

            /**
             * Update the timestamp of the cached object to the current time.
             * The TTL will be extended to the TTL set in the cache.
             * @param {String} key The unique key for the cached object
             * @memberOf ax/MemCache#
             */
            renewTimestamp: function(key) {
                var obj = this.__cache[key],
                    time,
                    ttl;

                if (!obj) {
                    return;
                }

                ttl = obj.ttl || this.__ttl;
                time = new Date().getTime();

                obj.timestamp = time;

                if (ttl !== MemCache.TTL.FOREVER) {
                    obj.expiration = time + ttl * 1000;
                }
            },

            /**
             * Check if the cache has expired.
             * @param {String} key The unique key for the cached object
             * @returns {Boolean} true if expired, false otherwise
             * @memberOf ax/MemCache#
             */
            isExpired: function(key) {
                var obj = this.__cache[key];

                // if nothing is mapped to the key
                if (!obj) {
                    return true;
                }

                // if the cache is set to "forever", then return false directly.
                if (obj.expiration === MemCache.TTL.FOREVER) {
                    return false;
                }

                if (new Date().getTime() > obj.expiration) {
                    return true;
                }

                return false;
            },

            /**
             * Get the cached object by the key.
             * @param {String} key The unique key for the cached object
             * @returns {Object|undefined} The cached object
             * @memberOf ax/MemCache#
             */
            get: function(key) {

                var obj = this.__cache[key];

                if (!obj) {
                    return undefined;
                }

                if (this.__gc === MemCache.GC.EXPIRY && this.isExpired(key)) {
                    return undefined;
                }

                return obj.data;
            },

            /**
             * Get the timestamp when an object was cached.
             * @param {String} key The unique key of the cached object
             * @returns {Number|undefined} The timestamp, undefined if the object doesn't exist.
             * @memberOf ax/MemCache#
             */
            getTimestamp: function(key) {

                var obj = this.__cache[key];

                if (!obj) {
                    return undefined;
                }

                if (this.__gc === MemCache.GC.EXPIRY && this.isExpired(key)) {
                    return undefined;
                }

                return obj.timestamp;
            },

            /**
             * Get the occupied size of the current cache
             * @returns {Number} the
             * @memberOf ax/MemCache#
             */
            getCacheSize: function() {
                return util.size(this.__cacheReference);
            },

            /**
             * Destructor method. Should be called to ensure cache is no longer needed
             * @memberOf ax/MemCache#
             */
            deinit: function() {
                if (this.__gc === this.constructor.GC.EXPIRY && this.__gcInterval) {
                    clearInterval(this.__gcInterval);
                }
            }

        });

    return MemCache;
});
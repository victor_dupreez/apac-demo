/**
 * Javascript globals module. Export standard ECMA built-in objects as dependencies
 * for AMD dependency injections.
 *
 * @module ax/jsGlobals
 */
define(["exports"], function(exports) {

    "use strict";

    exports.Array = Array;
    exports.Object = Object;
});
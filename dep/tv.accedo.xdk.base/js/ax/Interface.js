/**
 * Interface emulation for javascript. Provider a helper funciton to create Interface as well as a
 * ensure function to do implementation check.
 *
 * *NOTE* The constructor is a private. Use {@link ax/Interface.create}.
 *
 * @class ax/Interface
 * @param {String} name Name of the interface
 * @param {ax/Interface[]} exts List of extended interfaces
 * @param {Object} methods Map of methods declared
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/Interface", ["ax/util", "ax/core"], function (util, core) {
    "use strict";
    var __creating = false;

    function Interface(name, exts, methods) {
        if (!__creating) {
            throw core.createException("PrivateConstructor", "Use Interface.create instead");
        }
        this.name = name;
        this.exts = exts || [];
        this.methods = methods || {};
    }

    Interface.prototype.toString = function () {

        var fnName,
            ret = "Interface " + this.name + " : { ";
        for (fnName in this.methods) {
            ret += this.printFn(fnName);
        }
        ret += " }";

        return ret;
    };

    Interface.prototype.printFn = function (fnName) {
        return fnName + "(" + this.methods[fnName].join() + ")";
    };

    /**
     * @ignore
     */
    function isInterface(intf) {
        return (intf instanceof Interface);
    }

    /**
     * Create an Interface
     * @name create
     * @param {String} name Name for the new Interface. Usually the module id of the AMD.
     * @param {ax/Interface[]} [exts] Extended interfaces
     * @param {Object} methods List of methods to be declared
     * @function
     * @public
     * @memberof ax/Interface
     */
    function create() {

        if (arguments.length < 2) {
            throw core.createException("InvalidArgument", "intf.create called with " + arguments.length + "arguments, but expected at least 2.");
        }

        var args = Array.prototype.slice.call(arguments),
            name,
            exts,
            methods,
            ret,
            i, len, fnName;

        name = args.shift();
        if (!util.isString(name)) {
            throw core.createException("InvalidArgument", "intf.create: expects the first argument to be String");
        }


        // First arguments to be extended Interfaces
        if (args.length > 1) {
            exts = args.shift();

            if (!exts || !util.isArray(exts)) {
                throw core.createException("InvalidArgument", "intf.create: expects the second argument of three provided as an array");
            }

            for (i = 0, len = exts.length; i < len; i++) {
                if (!isInterface(exts[i])) {
                    throw core.createException("InvalidArgument", "intf.create: expects the first argument array contains only interfaces");
                }
            }
        }

        methods = args.shift();

        if (!methods || !util.isPlainObject(methods)) {
            throw core.createException("InvalidArgument", "intf.create: expects an Object for the last argument");
        }

        util.each(methods, function (pair) {

            fnName = pair.key;

            if (!util.isArray(pair.value)) {
                throw core.createException("InvalidArgument", "intf.create: expects method param list to be passed in as an array.");
            }

            for (i = 0, len = pair.value.length; i < len; i++) {
                if (!util.isString(pair.value[i])) {
                    throw core.createException("InvalidArgument", "intf.create: expects method param to be passed in as a string.");
                }
            }
        });

        __creating = true;
        ret = new Interface(name, exts, methods);
        __creating = false;

        // create an interface
        return ret;
    }

    /**
     * Ensure an Class implements an Interface. Class should implement the same function name with equal or larger number of arguments in order to fulfil the interface.
     * @name ensureImpl
     * @param {ax/Class} klass The Class to check
     * @param {ax/Interface} intf the Interface to ensure
     * @function
     * @public
     * @memberof ax/Interface
     **/
    function ensureImpl(klass, intf) {

        var i, len, params, fnExist, fn, prototype, fnName;

        //Check extended interfaces
        if (intf.exts) {

            for (i = 0, len = intf.exts.length; i < len; i++) {
                ensureImpl(klass, intf.exts[i]);
            }

        }

        //check this interface
        util.each(intf.methods, function (pair) {
            fnName = pair.key;
            params = pair.value;
            prototype = klass.prototype;
            fnExist = false;


            while (!fnExist && prototype) {

                if (prototype[fnName] && util.isFunction(prototype[fnName])) {

                    //get the orig function if it's been super injected
                    fn = prototype[fnName].isSuperWrapper ? prototype[fnName].orig : prototype[fnName];

                    // Check arguments length
                    if (fn.length >= intf.methods[fnName].length) {
                        fnExist = true;
                    }
                }

                prototype = prototype.prototype;
            }

            if (!fnExist) {
                throw core.createException("InterfaceViolation", "intf.ensureImpl: object does not implement the " + intf.name + " interface. Method " + intf.printFn(fnName) + " was not found.");
            }

        });

    }

    Interface.create = create;
    Interface.ensureImpl = ensureImpl;

    return Interface;
});
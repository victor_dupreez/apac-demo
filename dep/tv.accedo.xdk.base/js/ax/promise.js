/**
 * Promise represents the eventual value returned from the single completion of an operation.<br/>
 * There are three states: pending, fulfilled and rejected cases.
 * With promise, developer can handle and control the deferred and asynchronous computations.
 * <p>
 * A general error handling function can be set on the promise module, i.e. promise.onerror.<br/>
 * If this is set, any unhandled rejection reaching the end of the promise chain (.done()) or exception thrown in onProgress callback will go through that function.
 * Otherwise, the default action will be an exception thrown to the global space directly, leading to an unhandle-able exception.
 * </p>
 *
 * @module ax/promise
 */
define("ax/promise", [
    "ax/core",
    "require",
    "exports",
    "./Thennable",
    "ax/util",
    "ax/exception",
    "ax/console"
], function(
    core,
    require,
    exports,
    Thennable,
    util,
    exception,
    console
) {
    "use strict";

    var nextTick;

    /**
     * It is a task-scheduling queue which handles the tasks one by one.<br/>
     * It will return the function to process the next Tick until no more items in the queue.
     *
     * @returns {Function} A function to perform the next task
     * @method nextTick
     * @memberof module:ax/promise
     */
    exports.nextTick = (function() {
        var head, tail, flushing, requestTick;
        //handle the list of tasks
        head = {
            task: undefined,
            next: null
        };
        tail = head;
        flushing = false;

        function flush() {
            while (head.next) {
                head = head.next;
                var task = head.task;
                head.task = undefined;

                try {
                    task();
                } catch (e) {
                    throw e;
                }
            }

            flushing = false;
        }

        nextTick = function(task) {
            tail = tail.next = {
                task: task,
                next: null
            };

            if (!flushing) {
                flushing = true;
                requestTick();
            }
        };

        requestTick = function() {
            setTimeout(flush, 0);
        };


        return nextTick;
    })();

    /**
     * A PromiseDefer object contains a promise, which will be fulfilled/rejected according to the defer object.<br/>
     * To fulfill the promise, invoke {@link module:ax/promise~PromiseDefer#resolve}.
     * To reject, invoke {@link module:ax/promise~PromiseDefer#reject}.
     *
     * @class PromiseDefer
     * @property {module:ax/promise~Promise} promise The promise contained in the defer object, which is sensitive to the defer object's state.
     * @property {Function} resolve resolve the deferred promise with a value,
     * without wrapping the value with a Promise if it's promise.
     * @property {Function} fulfil fulfil the deferred promise with a value.
     * @property {Function} reject reject the deferred promise with a reason.
     * @property {Function} onCancel delegate to be triggered upon
     * associated promise being cancelled.
     * @memberof module:ax/promise~
     * @example
     *
     * var deferred = promise.defer();
     *
     * deferred.promise.then(function(succeed) {
     *    console.log(succeed);
     * }, function(fail) {
     *    console.log(fail);
     * }, function(notify) {
     *    console.log(notify);
     * });
     *
     * @example
     * // The state of the contained promise can be controlled through the methods of the defer object.
     * // resolve/fulfil/reject can only be called once while in pending state.
     * // It is forbidden to call the methods while the promise is not in pending state.
     * deferred.resolve("pass");
     * deferred.fulfil("pass");
     * deferred.reject("fail");
     * deferred.notify("processing");
     *
     * @example
     * //Cancellation
     *
     * deferred.onCancel =function(message) {
     *     //reject the promise with message upon cancel request
     *     deferred.reject(message);
     * };
     *
     * deferred.promise.cancel();
     *
     * @example
     * // To inspect the state of the defer
     * var stateObj = deferred.inspect();
     *
     * @example
     * // typical usage
     * function doSomething() {
     *     var deferred = promise.defer();
     *
     *     // an asynchronous task
     *     setTimeout(function() {
     *         deferred.resolve("done");
     *     }, 20000);
     *
     *     return deferred.promise;
     * }
     *
     * // somewhere
     * doSomething().then(function() {
     *     // this will be executed when the defer is resolved
     * }).done();
     *
     **/

    /**
     * @typedef {Object} module:ax/promise~PromiseState
     * @property {String} state The state of the defer/promise object
     * @property {T} value The value of the fulfilled promise
     * @property {T} reason The reason of the rejected promise
     * @memberof module:ax/promise
     */

    var STATE = {
        PENDING: "pending",
        FULFILLED: "fulfilled",
        REJECTED: "rejected",
        CANCELLED: "cancelled"
    };

    var EVT = {
        PROGRESSED: "progressed",
        FULFILLED: "fulfilled",
        REJECTED: "rejected",
        CANCELLED: "cancelled"
    };

    var Event = {

        init: function() {
            this._listeners = {};
        },

        on: function(evt, clientId, listener) {
            this._listeners[evt] = this._listeners[evt] || {};
            this._listeners[evt][clientId] = this._listeners[evt][clientId] || [];
            this._listeners[evt][clientId].push(listener);
        },

        off: function(evt, clientId) {
            if (clientId && this._listeners[evt]) {
                delete this._listeners[evt][clientId];
                return;
            }

            delete this._listeners[evt];
        },

        fire: function(evt, value) {
            var clients = this._listeners[evt];
            if (!clients) {
                return;
            }

            util.each(clients, function(pair) {
                var listeners = pair.value;
                for (var i = 0; i < listeners.length; i++) {
                    listeners[i](value);
                }
            });
        }
    };

    function cleanUp() {
        /*jshint validthis: true */
        Event.off.call(this, EVT.FULFILLED);
        Event.off.call(this, EVT.REJECTED);
        Event.off.call(this, EVT.PROGRESSED);
        this._onCancel = null;
    }

    /**
     * The Promise object
     *
     * @class Promise
     * @memberof module:ax/promise~
     * @protected
     * @example
     * <caption>General Usage</caption>
     * var defer = promise.defer();
     * defer.promise.then(function(succeed){
     *    console.log(succeed);
     * },function(fail){
     *    console.log(fail);
     * },function(notify){
     *    console.log(notify);
     * });
     *
     **/

    function Promise(task, onCancel) {

        if (!util.isFunction(task)) {
            throw core.createException(exception.ILLEGAL_ARGUMENT, "task is not a Function");
        }

        this._id = core.getGuid();

        this._status = {
            state: STATE.PENDING
        };

        this._activeThens = [];

        Event.init.apply(this);

        var resolver = util.bind(fulfill, this),
            rejecter = util.bind(reject, this),
            progresser = util.bind(progress, this);

        try {
            task.call(this, resolver, rejecter, progresser);
        } catch (e) {
            rejecter(e);
        }

        if (util.isFunction(onCancel)) {
            this._onCancel = util.bind(onCancel, this);
        }

        function fulfill(value) {
            /*jshint validthis: true */
            if (!this.isPending()) {
                return;
            }

            this._status.state = STATE.FULFILLED;
            this._status.value = value;

            nextTick(util.bind(function() {
                Event.fire.call(this, EVT.FULFILLED, value);
                cleanUp.call(this);
            }, this));
        }

        function reject(reason) {
            /*jshint validthis: true */
            if (!this.isPending()) {
                return;
            }

            this._status.state = STATE.REJECTED;
            this._status.reason = reason;

            nextTick(util.bind(function() {
                Event.fire.call(this, EVT.REJECTED, reason);
                cleanUp.call(this);
            }, this));
        }

        function progress(update) {
            /*jshint validthis: true */
            if (!this.isPending()) {
                return;
            }
            nextTick(util.bind(function() {
                Event.fire.call(this, EVT.PROGRESSED, update);
            }, this));
        }
    }


    /**
     * Guid of the promise instance
     *
     * @method getId
     * @returns {Number} Identified in integer
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.getId = function() {
        return this._id;
    };

    /**
     * To handle the cases when fulfill(success), reject(fail) or notify(progress)
     *
     * @method then
     * @param {module:ax/promise~Promise#onFulfill} fulfilled  Callback function when the promise is fulfilled
     * @param {module:ax/promise~Promise#onReject} rejected  Callback function when the promise is rejected
     * @param {module:ax/promise~Promise#onProgress} progressed  Callback function when the promise is processing
     * @returns {module:ax/promise~Promise} Promise is returned
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.then = function(onFulfill, onReject, onProgress, onCancel) {

        var p = (function(waited) {

            function thenResolver(resolve, reject, notify) {
                /*jshint validthis: true */

                function handler(thenHandler, propagator, valueOrReason) {
                    /*jshint validthis: true */


                    if (!util.isFunction(thenHandler)) {
                        propagator(valueOrReason);
                        return;
                    }

                    try {
                        var ret = thenHandler(valueOrReason);
                        if (ret === this) {
                            reject(new TypeError("promise's handler cannot return itself (same promise instance)"));
                            return;
                        }
                        if (isPromise(ret)) {
                            // wait on the returned Promise rather than parent
                            waited = ret.then(resolve, reject);
                            return;
                        }
                        //thennable, no progress support
                        if (!Thennable.handle(ret, resolve, reject)) {
                            resolve(ret);
                        }
                    } catch (e) {
                        reject(e);
                    }
                }

                function handleProgress(progress) {
                    if (!util.isFunction(onProgress)) {
                        notify(progress);
                        return;
                    }

                    try {
                        var ret = onProgress(progress);
                        notify(ret);
                    } catch (e) {
                        reject(e);
                    }
                }

                if (waited.isPending()) {
                    Event.on.call(waited, EVT.FULFILLED, this.getId(), util.bind(handler, this, onFulfill, resolve));
                    Event.on.call(waited, EVT.REJECTED, this.getId(), util.bind(handler, this, onReject, reject));
                    Event.on.call(waited, EVT.PROGRESSED, this.getId(), handleProgress);
                    return;
                }

                if (waited.isFulfilled()) {
                    nextTick(util.bind(handler, this, onFulfill, resolve, waited.inspect()
                        .value));
                    return;
                }

                if (waited.isRejected()) {
                    nextTick(util.bind(handler, this, onReject, reject, waited.inspect()
                        .reason));
                    return;
                }

            }

            function thenOnCancel(message) {
                /*jshint validthis: true */

                waited.cancel(message, this.getId());
            }

            var child = new Promise(thenResolver, thenOnCancel);

            function handleCancel() {

                //cancel decesdant promise
                child.cancel();

                if (util.isFunction(onCancel)) {
                    nextTick(onCancel);
                }
            }

            if (waited.isPending()) {
                Event.on.call(waited, EVT.CANCELLED, child.getId(), handleCancel);
            }

            return child;

        }(this));

        this._activeThens.push(p.getId());

        return p;
    };

    /**
     * Returns the current status of the Promise
     *
     * @method inspect
     * @returns {Object} see example
     * @memberof module:ax/promise~Promise#
     * @example
     *
     * promise.resolve('value').inspect(); // { status: 'fulfilled', value: 'value' };
     * promise.rejected('reason').inspect(); // { status: 'rejected', reason: 'reason' };
     * promise.defer().promise.inspect(); // { status: 'pending' };
     */
    Promise.prototype.inspect = function() {
        return this._status;
    };

    /**
     * Check if promise is in pending state
     *
     * @method isPending
     * @returns {Boolean} true if pending
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.isPending = function() {
        return this._status.state === STATE.PENDING;
    };

    /**
     * Check if promise is in fulfilled state
     *
     * @method isFulfilled
     * @returns {Boolean} true if fulfilled
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.isFulfilled = function() {
        return this._status.state === STATE.FULFILLED;
    };

    /**
     * Check if promise is in rejected state
     *
     * @method isRejected
     * @returns {Boolean} true if rejected
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.isRejected = function() {
        return this._status.state === STATE.REJECTED;
    };

    /**
     * Check if promise is in cancelled state
     *
     * @method isCancelled
     * @returns {Boolean} true if cancelled
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.isCancelled = function() {
        return this._status.state === STATE.CANCELLED;
    };

    /**
     * To pass a cancel request to the Producer of the promise. Behavior depends on
     * specific cancellation case.
     *
     * @method cancel
     * @param {T} [message] Message to pass
     * @memberof module:ax/promise~Promise#
     */
    function cancel(message, from) {
        /*jshint validthis: true */

        if (!this.isPending()) {
            return;
        }


        if (from) {
            // Implicit cancellations through then will be ignored if 
            // there are still active thens waited, suppose originate then
            // has been cancelled already
            var fromIdx = this._activeThens.indexOf(from);
            if (fromIdx > -1) {
                this._activeThens.splice(fromIdx, 1);
            }

            Event.off.call(this, EVT.FULFILLED, from);
            Event.off.call(this, EVT.REJECTED, from);
            Event.off.call(this, EVT.PROGRESSED, from);

            if (this._activeThens.length > 0) {
                return;
            }
        }

        this._status.state = STATE.CANCELLED;

        this._onCancel(message);

        Event.fire.call(this, EVT.CANCELLED);

        cleanUp.call(this);
    }

    Promise.prototype.cancel = cancel;

    exports.Promise = Promise;

    exports.promise = function(resolver) {
        return new Promise(resolver);
    };

    /**
     * To check if the given promise is still pending.
     * @method isPending
     * @memberof module:ax/promise
     * @param {object} object The target promise object
     * @returns {Boolean} True if it is still pending, false otherwise
     */
    function isPending(obj) {
        return isPromise(obj) && obj.isPending();
    }
    exports.isPending = isPending;

    /**
     * To check if the given Promise has fulfilled.
     * @method isFulfilled
     * @memberof module:ax/promise
     * @param {object} object The target promise object
     * @returns {Boolean} True if it is fulfilled or non promise, false otherwise
     */
    function isFulfilled(obj) {
        return !isPromise(obj) || obj.isFulfilled();
    }
    exports.isFulfilled = isFulfilled;

    /**
     * To check if the given Promise has rejected.
     * @method isRejected
     * @memberof module:ax/promise
     * @param {object} object The target promise object
     * @returns {Boolean} True if it is rejected or non promise, false otherwise
     */
    function isRejected(obj) {
        return isPromise(obj) && obj.isRejected();
    }
    exports.isRejected = isRejected;

    /**
     * To indicate whether the given object is a promise or not.
     * @method isPromise
     * @memberof module:ax/promise
     * @param {Object} object The target object
     * @returns {Boolean} True if it is promise, false otherwise
     */
    function isPromise(value) {
        return value instanceof Promise;
    }
    exports.isPromise = isPromise;

    /**
     * To create a fulfilled promise with the value specified.
     * @method fulfill
     * @memberof module:ax/promise
     * @param {T} value The value when fulfill
     * @returns {module:ax/promise~Promise.<T>} A fulfilled promise with the value specified
     */
    function fulfill(value) {
        return new Promise(function(resolve, reject) {
            resolve(value);
        });
    }
    exports.fulfill = fulfill;


    /**
     * To create a rejected promise with the reason specified.
     *
     * @method reject
     * @memberof module:ax/promise
     * @param {T} reason the reson to reject
     * @returns {module:ax/promise~Promise.<T>} A rejected promise with the reason specified
     */
    function reject(reason) {
        return new Promise(function(resolve, reject) {
            reject(reason);
        });
    }
    exports.reject = reject;
    /**
     * To create a promise, which has fulfilled with the given value.
     *
     * @method resolve
     * @memberof module:ax/promise
     * @param {T} value The value when resolve
     * @returns {module:ax/promise~Promise.<T>} A fulfilled promise with the value specified
     */
    function resolve(value) {
        if (isPromise(value)) {
            return value;
        }
        return fulfill(value);
    }
    exports.resolve = resolve;

    /**
     * Create a PromiseDefer object.
     *
     *
     *
     * @method defer
     * @memberof module:ax/promise
     * @returns {module:ax/promise~PromiseDefer} Return the deferred object
     * @example
     * var deferred = promise.defer();
     */
    function defer() {
        var ret = {};

        ret.promise = new Promise(function(resolve, reject, progress) {
            ret.fulfill = resolve; //deprecated, keep for backward-compatibility
            ret.resolve = resolve;
            ret.reject = reject;
            ret.notify = progress;
        }, function(message) {
            var onCancel = ret.onCancel;
            if (util.isFunction(onCancel)) {
                onCancel(message);
            }
            ret.onCancel = null;
        });

        return ret;
    }
    exports.defer = defer;



    /**
     * To handle the failure case of the promise.
     *
     * @param {module:ax/promise~Promise} promise targed promise
     * @param {module:ax/promise~Promise#onReject} rejected  Callback function when the promise is rejected
     * @returns {module:ax/promise~Promise} Promise is returned
     * @method fail
     * @memberof module:ax/promise
     * @private
     */
    function fail(promise, rejected) {
        return when(promise, undefined, rejected);
    }
    exports.fail = fail;
    /**
     * To handle the failure case of the promise.
     *
     * @param {module:ax/promise~Promise#onReject} rejected  Callback function when the promise is rejected
     * @returns {module:ax/promise~Promise} Promise is returned
     * @method fail
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.fail = function(rejected) {
        return fail(this, rejected);
    };

    /**
     * To handle the progress case of the promise.
     *
     * @param {module:ax/promise~Promise} promise targed promise
     * @param {module:ax/promise~Promise#onProgress} progressed  Callback function when the promise is processing
     * @returns {module:ax/promise~Promise} Promise is returned
     * @method progress
     * @memberof module:ax/promise
     * @private
     */
    function progress(promise, progressed) {
        return when(promise, undefined, undefined, progressed);
    }
    exports.progress = progress;

    /**
     * To handle the progress case of the promise.
     *
     * @param {module:ax/promise~Promise#onProgress} progressed  Callback function when the promise is processing
     * @returns {module:ax/promise~Promise} Promise is returned
     * @method progress
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.progress = function(progressed) {
        return progress(this, progressed);
    };

    /**
     * To create a fulfilled promise when it fulfill the value.
     * @method when
     * @memberof module:ax/promise
     * @param {Object} value  promise or immediate reference to observe
     * @param {module:ax/promise~Promise#onFulfill} fulfilled  Callback function when the promise is fulfilled
     * @param {module:ax/promise~Promise#onReject} rejected  Callback function when the promise is rejected
     * @param {module:ax/promise~Promise#onProgress} progressed  Callback function when the promise is processing
     * @param {module:ax/promise~Promise#onCancel} cancelled  Callback function when the promise is cancelled
     * @returns {module:ax/promise~Promise} Promise is returned
     */
    function when(value, fulfilled, rejected, progressed, cancelled) {
        return resolve(value)
            .then(fulfilled, rejected, progressed, cancelled);
    }
    exports.when = when;


    /**
     * To handle the promise after it has either fulfilled or rejected.<br/>
     * This method is used to handle any common logic regardless of the attached promise result.
     * The value of the attached promise will not be passed into the onComplete callback, but be cascaded to the promise chain.
     *
     * @param {module:ax/promise~Promise} promise targed promise
     * @param {module:ax/promise~Promise#onComplete.<T>} callback  Callback function when the promise completes
     * @returns {module:ax/promise~Promise.<T>} A promise value cascaded from the attached promise
     * @method complete
     * @memberof module:ax/promise
     * @private
     */
    function complete(promise, callback) {
        return when(promise, function(value) {
            return when(callback(),
                function() {
                    return value;
                }
            );
        }, function(exception) {
            return when(callback(),
                function() {
                    return reject(exception);
                }
            );
        }, null, function() {
            callback();
        });
    }

    /**
     * To handle the promise after it has either fulfilled or rejected.<br/>
     * The value of the attached promise will be cascaded. This method is usually used for resource clean up.
     *
     * @param {module:ax/promise~Promise#onComplete.<T>} callback  Callback function when the promise completes
     * @returns {module:ax/promise~Promise.<T>} A promise value cascaded from the attached promise
     * @method complete
     * @memberof module:ax/promise~Promise#
     * @example
     * var fs = FileSystem.init();
     * var file = fs.open(fileName);
     *
     * // assume writeToFile(String, String) is a function writing content to the specified file
     * // this function return a fulfilled promise when success, or a rejected promise when fail
     * file.writeToFile(fileName, content)
     *   .complete(function() {
     *       file.close();
     *   }).then(function(value) {
     *       // continue the logic
     *   }, function(reason) {
     *       // show failure popup
     *   }).done();
     */

    Promise.prototype.complete = function(callback) {
        return complete(this, callback);
    };
    exports.complete = complete;

    /**
     * Terminates a chain of promises, forcing unhandled rejection to be thrown as exceptions.
     *
     * @param {module:ax/promise~Promise} promise targed promise
     * @param {module:ax/promise~Promise#onFulfill} fulfilled  Callback function when the promise is fulfilled
     * @param {module:ax/promise~Promise#onReject} rejected  Callback function when the promise is rejected
     * @param {module:ax/promise~Promise#onProgress} progressed  Callback function when the promise is processing
     * @param {module:ax/promise~Promise#onProgress} cancelled  Callback function when the promise is cancelled
     * @returns {module:ax/promise~Promise} Promise is returned
     * @method done
     * @memberof module:ax/promise
     * @private
     */
    function done(promise, fulfilled, rejected, progress, cancelled) {
        var promiseToHandle, onUnhandledError;
        onUnhandledError = function(error) {
            // forward to a future turn so that ``when``
            // does not catch it and turn it into a rejection.
            nextTick(function() {

                if (exports.onerror) {
                    exports.onerror(error);
                } else if (error instanceof Error) {
                    throw error;
                } else {
                    console.warn(error);
                }
            });
        };

        // Avoid unnecessary `nextTick`ing via an unnecessary `when`.
        promiseToHandle = fulfilled || rejected || progress || cancelled ? when(promise, fulfilled, rejected, progress, cancelled) :
            promise;

        fail(promiseToHandle, onUnhandledError);
    }
    exports.done = done;
    /**
     * Terminates a chain of promises, forcing unhandled rejection to be thrown as exceptions.
     *
     * @param {module:ax/promise~Promise#onFulfill} fulfilled  Callback function when the promise is fulfilled
     * @param {module:ax/promise~Promise#onReject} rejected  Callback function when the promise is rejected
     * @param {module:ax/promise~Promise#onProgress} progressed  Callback function when the promise is processing
     * @returns {module:ax/promise~Promise} Promise is returned
     * @method done
     * @memberof module:ax/promise~Promise#
     */
    Promise.prototype.done = function(fulfilled, rejected, progress, cancelled) {
        return done(this, fulfilled, rejected, progress, cancelled);
    };



    /**
     * Turns an array of promises into a promise for an array.<br/>
     * The promise will get fulfilled when all given promises fulfill.
     * If any of the given promises gets rejected, the whole array is rejected immediately.
     * Cancelling a all would imply a cancel to all subsequence pending promises.
     *
     * @method all
     * @memberof module:ax/promise~Promise
     * @param {Array} promises an array (or promise for an array) of values (or promises for values)
     * @returns {module:ax/promise~Promise.<Array>} a promise for an array of the corresponding values
     * @throws {module:ax/promise~Promise.<Error|Object>} the value of the rejected promise
     */
    function all(promises) {

        return (function() {

            var countDown = 0,
                pendingIds = {},
                resolve,
                reject,
                notify;

            function waitFor(promise, index) {

                var p = promise.then(
                    function(value) {
                        promises[index] = value;
                        delete pendingIds[promise.getId()];

                        //resolved when all promises resolved
                        if (--countDown === 0) {
                            resolve(promises);
                            util.clear(pendingIds);
                        }
                    },
                    function(reason) {
                        reject(reason);
                        util.clear(pendingIds);
                    },
                    function(progress) {
                        notify({
                            index: index,
                            value: progress
                        });
                    });

                pendingIds[promise.getId()] = p.getId();
            }

            function allResolver(_resolve, _reject, _notify) {

                resolve = _resolve;
                reject = _reject;
                notify = _notify;

                util.each(promises, function(promise, index) {

                    if (!isPromise(promise)) {
                        return;
                    }

                    if (isRejected(promise)) {
                        reject(promise.inspect()
                            .reason);
                    }

                    if (isFulfilled(promise)) {
                        promises[index] = promise.inspect()
                            .value;

                        return;
                    }

                    ++countDown;

                    waitFor(promise, index);
                });

                if (countDown === 0) {
                    resolve(promises);
                }
            }

            function allOnCancel(message) {

                util.each(promises, function(promise, index) {

                    if (!isPromise(promise)) {
                        return;
                    }

                    if (promise.isPending()) {
                        promise.cancel(message, pendingIds[promise.getId()]);
                        delete pendingIds[promise.getId()];
                    }
                });

            }

            return new Promise(allResolver, allOnCancel);
        }());

    }
    exports.all = all;

    /**
     * Turns an array of promises into a promise for an array.<br/>
     * The promise will get fulfilled when all given promises are fulfilled or rejected.
     * Cancelling a spread would imply a cancel to all subsequence pending promises
     *
     * @method allSettled
     * @memberof module:ax/promise~Promise
     * @param {Array} promises an array (or promise for an array) of values (or promises for values)
     * @returns {module:ax/promise~Promise.<Array>} a promise for an array of the corresponding inspect object
     * @example
     *
     * var all = promise.allSettled([
     *         promise.resolve(1),
     *         promise.reject(2)
     *     ]);
     *
     * all.then(function (results) {
     *     // results[0] - { state: "fulfilled", value: 1 }
     *     // results[1] - { state: "rejected", reason: 2 }
     * });
     */
    function allSettled(promises) {
        return all(util.map(promises, function(promise) {
            if (!isPromise(promise)) {
                promise = resolve(promise);
            }

            function regardless() {
                return promise.inspect();
            }
            return promise.then(regardless, regardless);
        }));
    }
    exports.allSettled = allSettled;

    /**
     * Turns an array of promises into a promise for an array.<br/>
     * a promise that resolves or rejects as soon as one of the promises in the iterable resolves or rejects, with the value or reason from that promise.
     * Cancelling a race would imply cancelling to all subsequence pending promises.
     *
     * @method race
     * @memberof module:ax/promise~Promise
     * @param {Array} promises an array (or promise for an array) of values (or promises for values)
     * @returns {module:ax/promise~Promise.<Object>} corresponding value of the fastest promise
     * @throws {module:ax/promise~Promise.<Error|Object>} the reason of the fastest promise
     * @example
     *
     * var a = promise.defer(),
     *     b = promise.defer();
     *
     * promise.race([
     *     a.promise,
     *     b.promise
     * ]).then(function(value) {
     *     // value - "a"
     * }, null);
     *
     * a.resolve("a");
     */
    function race(promises) {
        var deferred = defer(),
            pendingPromises = [];

        when(promises, function(promises) {
            util
                .each(promises, function(promise, index) {

                    if (!isPromise(promise)) {
                        promise = resolve(promise);
                    }
                    var p = when(promise, deferred.resolve, deferred.reject);
                    pendingPromises.push(p);
                });
        });


        deferred.onCancel = function(message) {

            util.each(pendingPromises, function(promise, index) {
                promise.cancel(message);
            });

        };

        return deferred.promise;
    }
    exports.race = race;

    /**
     * Spreads the values of a promised array into fulfill callback.
     * The fulfill callback will then be invoked with a list of arguments,
     * each of them corresponds to the element inside the promise array.
     *
     * @method spread
     * @private
     * @memberof module:ax/promise
     * @param {module:ax/promise~Promise} promise to observe
     * @param {module:ax/promise~Promise#onFulfill} fulfilled  Callback function when the promise is fulfilled
     * @param {module:ax/promise~Promise#onReject} rejected  Callback function when the promise is rejected
     * @returns {module:ax/promise~Promise} Promise is returned
     */
    function spread(promise, fulfilled, rejected) {
        return when(promise, function(valuesOrPromises) {
            return all(valuesOrPromises)
                .then(function(values) {
                    return fulfilled.apply(undefined, values);
                }, rejected);
        }, rejected);

    }

    /**
     * Spreads the values of the attached promised array into fulfill callback.
     * The fulfill callback will then be invoked with a list of arguments,
     * each of them corresponds to the element inside the promise array.
     * This is usually used together with {@link module:ax/promise.all}.
     * Cancelling a spread would imply a cancel to all subsequence pending promises.
     *
     * @method spread
     * @memberof module:ax/promise~Promise#
     * @param {module:ax/promise~Promise#onFulfill} fulfilled  Callback function when the promise is fulfilled
     * @param {module:ax/promise~Promise#onReject} rejected  Callback function when the promise is rejected
     * @returns {module:ax/promise~Promise} Promise is returned
     * @example
     * promise.all([
     *   promise.resolve(1),
     *   promise.resolve(2)
     * ]).spread(function(a, b) {
     *   console.log(a + b);
     * }).done();
     * // output
     * // 3
     *
     * promise.all([
     *   promise.resolve(1),
     *   promise.reject(2)
     * ]).spread(function(a, b) {
     *   console.log(a + b);
     * }, function(reason) {
     *   console.error(reason);
     * }).done();
     * // output
     * // 2
     */
    Promise.prototype.spread = function(fulfilled, rejected) {
        return spread(this, fulfilled, rejected);
    };

    exports.spread = spread;



});
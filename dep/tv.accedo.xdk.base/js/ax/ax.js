/**
 * AX - The short hand module for XDK core package. To avoid too much ceremony
 * using AMD.
 * @module ax/ax
 */
define("ax/ax", ["ax/core", "ax/Element", "ax/class", "ax/config", "ax/device", "ax/Env", "ax/util", "ax/console", "ax/ajax", "ax/promise"], function (core, Element, klass, config, device, Env, util, console, ajax, promise) {
    "use strict";
    /**
     * exports ax/ax
     */
    var ax = {},
        sEnv = Env.singleton();


    /**
     * @var {module} core
     * @memberOf module:ax/ax
     * @see module:ax/core
     */
    ax.core = core;

    /**
     * @var {Class} Element
     * @memberOf module:ax/ax
     * @see module:ax/Element
     */
    ax.Element = Element;

    /**
     * @var {module} klass
     * @memberOf module:ax/ax
     * @see module:ax/class
     */
    ax.klass = klass;

    /**
     * @var {module} config
     * @memberOf module:ax/ax
     * @see module:ax/config
     */
    ax.config = config;
    /**
     * @var {module} sEnv
     * @memberOf module:ax/ax
     * @see ax/Env
     */
    ax.sEnv = sEnv;

    /**
     * @var {module} util
     * @memberOf module:ax/ax
     * @see module:ax/util
     */
    ax.util = util;

    /**
     * @var {module} ajax
     * @memberOf module:ax/ax
     * @see module:ax/ajax
     */
    ax.ajax = ajax;

    /**
     * @var {module} promise
     * @memberOf module:ax/ax
     * @see module:ax/promise
     */
    ax.promise = promise;

    /**
     * @var {module} console
     * @memberOf module:ax/ax
     * @see module:ax/console
     */
    ax.console = console;
    //add into ax by sEnv EVT_ONLOAD since the device is not ready and not loaded yet, so the platform info will only be known when it is ready
    sEnv.addEventListener(sEnv.EVT_ONLOAD, function () {
        /**
         * @var {module} platform
         * @memberOf module:ax/ax
         * @see module:ax/device
         */
        ax.platform = device.platform;

        /**
         * @var {module} vKey
         * @memberOf module:ax/ax
         * @see module:ax/device/vKey
         */
        ax.vKey = device.vKey;

        /**
         * @var {module} id
         * @memberOf module:ax/ax
         * @see module:ax/device/AbstractId
         */
        ax.id = device.id;

        /**
         * @var {module} system
         * @memberOf module:ax/ax
         * @see module:ax/device/AbstractSystem
         */
        ax.system = device.system;

        /**
         * @var {module} storage
         * @memberOf module:ax/ax
         * @see module:ax/device/AbstractStroage
         */
        ax.storage = device.storage;

        /**
         * @var {ax/device/Media} media
         * @memberOf module:ax/ax
         */
        ax.media = device.media;
    });

    return ax;
});
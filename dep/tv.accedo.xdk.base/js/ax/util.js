/**
 * Utilities module.
 *
 * @module ax/util
 */
define("ax/util", [
    "require",
    "ax/core",
    "ax/exception",
    "ax/jsGlobals"
], function(
    require,
    core,
    exception,
    jsGlobals
) {

    "use strict";

    var util = {},
        Array = jsGlobals.Array,
        Object = jsGlobals.Object,
        getPromise = function() {
            return require("ax/promise");
        };
    /**
     * flattening and specifying the start index.
     *
     * @private
     * @param {Array} array The array to flatten.
     * @param {Boolean} [isDeep=false] Specify a deep flatten.
     * @param {Boolean} [isStrict=false] Restrict flattening to arrays and `arguments` objects.
     * @param {Number} [fromIndex=0] The index to start from.
     * @returns {Array} Returns the new flattened array.
     * @example
     *
     * var arr = [1, [2], [3, [[4]]]];
     * baseFlatten(arr, true, false, 0)
     * // [1, 2, 3, 4]
     *
     * baseFlatten(arr, false, false, 0)
     * // [1, 2, [3, 4]]
     */
    function baseFlatten(array, isDeep, isStrict, fromIndex) {
        var index = (fromIndex || 0) - 1,
            length = array.length,
            resIndex = -1,
            result = [];

        while (++index < length) {
            var value = array[index];

            if (value && typeof value === "object" && typeof value.length === "number" &&
                (util.isArray(value) || util.isArguments(value))) {
                // recursively flatten arrays (susceptible to call stack limits)
                if (isDeep) {
                    value = baseFlatten(value, isDeep, isStrict);
                }
                var valIndex = -1,
                    valLength = value.length;

                result.length += valLength;
                while (++valIndex < valLength) {
                    result[++resIndex] = value[valIndex];
                }
            } else if (!isStrict) {
                result[++resIndex] = value;
            }
        }
        return result;
    }

    //Object Part
    (function () {
        /**
         * A reference to naitive toString function.
         * @name _toString
         * @private
         */
        var __toString,
            BOOLEAN_CLASS = "[object Boolean]",
            NUMBER_CLASS = "[object Number]",
            STRING_CLASS = "[object String]",
            ARRAY_CLASS = "[object Array]",
            DATE_CLASS = "[object Date]",
            FUNCTION_TYPE = "function";
        __toString = Object.prototype.toString;
        /**
         * Extends a dest with properties from src.
         * Possible deep copy/extending by setting deep = true.
         * @method extend
         * @memberof module:ax/util
         * @param {Object} dest Destination object to be extended
         * @param {Object} src Source object with properties to extend with
         * @param {Boolean} [deep] Deep copy
         * @return {Object} extended object
         * @public
         */
        util.extend = function (dest, src, deep) {
            var objIsArray, orig, copy, clone, att;
            for (att in src) {

                orig = dest[att];
                copy = src[att];
                // Prevent never-ending loop
                if (dest === copy) {
                    continue;
                }

                objIsArray = this.isArray(copy);
                if (deep && copy && (objIsArray || this.isPlainObject(copy))) {
                    if (objIsArray) {
                        objIsArray = false;
                        clone = orig && this.isArray(orig) ? orig : [];
                    } else {
                        clone = orig && this.isPlainObject(src) ? orig : {};
                    }

                    // Never move original objects, clone them
                    dest[att] = this.extend(clone, copy, true);
                } else if (!this.isUndefined(copy)) {
                    dest[att] = copy;
                }
            }
            return dest;
        };
        /**
         * Clones an object.
         * @method clone
         * @memberof module:ax/util
         * @param {Object|Array} obj Object to check
         * @param {Boolean} [deep] Deep copy
         * @return {Object} a copy of the object
         * @public
         */
        util.clone = function (obj, deep) {
            if (this.isArray(obj)) {
                return this.extend([], obj, deep);
            }
            return this.extend({}, obj, deep);
        };
        /**
         * Checks is object is undefined.
         * @method isUndefined
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is undefined, false otherwise
         * @public
         */
        util.isUndefined = function (obj) {
            return typeof obj === "undefined";
        };
        /**
         * Checks if the object is a plain object, created using {} or new Object().
         * @method isPlainObject
         * @memberof module:ax/util
         * @param {Object} obj Object to test.
         * @return {Boolean} true if parameter is an object, false otherwise
         * @public
         */
        util.isPlainObject = function (obj) {
            if (!obj || typeof obj !== "object" || obj.nodeType) {
                return false;
            }

            // Not own constructor property must be Object
            if (obj.constructor && !obj.hasOwnProperty("constructor") && !obj.constructor.prototype.hasOwnProperty("isPrototypeOf")) {
                return false;
            }

            //Loop through all properties to get access to the last one.
            var key;
            for (key in obj) {}

            //Own properties are iterated first, so it is enough to look at the last one.
            return (key === undefined || obj.hasOwnProperty(key));
        };
        /**
         * Checks if the object is an array.
         * @method isArray
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is an Array, false otherwise
         * @public
         */
        util.isArray = Array.isArray ? Array.isArray : function (obj) {

            //Information  on the best way to perform this type of check is taken from:
            // http://perfectionkills.com/instanceof-considered-harmful-or-how-to-write-a-robust-isarray/
            return __toString.call(obj) === ARRAY_CLASS;
        };
        /**
         * Checks if the object is Function.
         * @method isFunction
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a Function, false otherwise
         * @public
         */
        util.isFunction = function (obj) {
            return typeof obj === FUNCTION_TYPE || false;
        };
        /**
         * Checks if the object is a string.
         * @method isString
         * @memberof module:ax/util
         * @function
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a String, false otherwise
         * @public
         * @static
         */
        util.isString = function (obj) {
            return __toString.call(obj) === STRING_CLASS;
        };
        /**
         * Checks if the object is a number.
         * @method isNumber
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a Number, false otherwise
         * @public
         */
        util.isNumber = function (obj) {
            return __toString.call(obj) === NUMBER_CLASS;
        };
        /**
         * Checks if the object is NaN
         * @method isNan
         * @memberof module:ax/util
         * @param {object} obj Object to check
         * @return {Boolean} true if object is NaN, false otherwise
         */
        util.isNaN = function (obj) {
            return util.isNumber(obj) && obj !== +obj;
        };
        /**
         * Checks if the object is Finite
         * @method isFinite
         * @memberof module:ax/util
         * @param {object} obj Object to check
         * @return {Boolean} true if object is Finite, false otherwise
         */
        util.isFinite = function (obj) {
            return isFinite(obj);
        };
        /**
         * Checks if the object is a boolean.
         * @method isBoolean
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a Boolean, false otherwise
         * @public
         */
        util.isBoolean = function (obj) {
            return __toString.call(obj) === BOOLEAN_CLASS;
        };
        /**
         * Checks if the object is a date.
         * @method isDate
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a Date, false otherwise
         * @public
         */
        util.isDate = function (obj) {
            return __toString.call(obj) === DATE_CLASS;
        };
        /**
         * Checks if the object is a DOM element.
         * @method isDOMElement
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is a DOM element, false otherwise
         * @public
         */
        util.isDOMElement = function (obj) {
            if (!obj) {
                return false;
            }
            //Browsers not supporting W3 DOM2 don't have HTMLElement and
            //an exception is thrown and we end up here. Testing some
            //properties that all elements have. (works on IE7)
            return (typeof obj === "object") && (obj.nodeType === 1) && (typeof obj.style === "object") && (typeof obj.ownerDocument === "object") && (typeof obj.nodeName === "string");
        };
        /**
         * Tests whether an object is empty or not (an empty object is {})
         * @method isEmpty
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is empty, false otherwise
         * @public
         */
        util.isEmpty = function (obj) {
            //other than plainobject return false
            if (!this.isPlainObject(obj)) {
                return false;
            }
            var prop;
            for (prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    return false;
                }
            }

            return true;
        };
        /**
         * Tests whether an object is null
         * @method isNull
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is empty, false otherwise
         * @public
         */
        util.isNull = function (obj) {
            return obj === null;
        };
        /**
         * To check if it is object. It is different from plainObject which suppose has no contructor
         * @method isObject
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object, false otherwise
         * @public
         */
        util.isObject = function (obj) {
            return obj === Object(obj);
        };
        /**
         * To check if it is an argument object
         * @method isArgument
         * @memberof module:ax/util
         * @param {Object} obj Object to check
         * @return {Boolean} true if object is argument, false otherwise
         */
        util.isArguments = function (obj) {
            return util.has(obj, "callee");
        };

        /**
         * Checks if the 2 specified plain objects are equal.
         * 2 objects are considered equal iff all of the corresponding properties are equal (recursively).
         * `deepness` could be provided to control the deepness of checking. The properties under the specified depth are ignored.
         *
         * @method
         * @param {Object} original Original plain object
         * @param {Object} target Plain object to check against
         * @param {Number} [deepness] The depth limit (>= 1) of the check. If not specified (or < 1), a complete check will be performed among all properties (recursively).
         * @returns {Boolean} True if the objects are equal before the specific depth, false otherwise.
         * @memberof module:ax/util
         */
        util.areObjectsEqual = function (original, target, deepness) {
            if (!util.isPlainObject(original) || !util.isPlainObject(target)) {
                return false;
            }

            var key;
            var keyMap = {};
            var lastLevel = false;

            // in the recursive case, if deepness is not defined, deepness - 1 will be NaN
            // NaN is essentially having the same effect as undefined, which leads to full check
            if (deepness === 1) {
                lastLevel = true;
            }

            for (key in original) {
                var prop1 = original[key];
                var prop2 = target[key];

                if (util.isPlainObject(prop1) && util.isPlainObject(prop2)) {
                    if (!lastLevel && !util.areObjectsEqual(prop1, prop2, deepness - 1)) {
                        return false;
                    }
                } else if (util.isArray(prop1) && util.isArray(prop2)) {
                    if (!lastLevel && !util.areArraysEqual(prop1, prop2, deepness - 1)) {
                        return false;
                    }
                } else if (prop1 !== prop2) {
                    return false;
                }

                keyMap[key] = true;
            }

            // any property not in `original` => not equal
            for (key in target) {
                if (!keyMap[key]) {
                    return false;
                }
            }

            return true;
        };

        /**
         * Checks if the 2 specified arrays are equal.
         * 2 arrays are considered equal iff all of the elements exist in both arrays and are in same order.
         * Array or object element inside the array will be checked recursively.
         * `deepness` could be provided to control the deepness of checking. The elements under the specified depth are ignored.
         *
         * @method
         * @param {Array} original Original array
         * @param {Array} target Array to check against
         * @param {Number} [deepness] The depth limit (>= 1) of the check. If not specified (or < 1), a complete check will be performed among all elements (recursively).
         * @returns {Boolean} True if the arrays are equal, false otherwise.
         * @memberof module:ax/util
         */
        util.areArraysEqual = function (original, target, deepness) {
            if (!util.isArray(original) || !util.isArray(target)) {
                return false;
            }

            var len1 = original.length;
            var len2 = target.length;
            var lastLevel = false;

            if (len1 !== len2) {
                return false;
            }

            // in the recursive case, if deepness is not defined, deepness - 1 will be NaN
            // NaN is essentially having the same effect as undefined, which leads to full check
            if (deepness === 1) {
                lastLevel = true;
            }

            for (var i = 0; i < len1; i++) {
                if (util.isPlainObject(original[i]) && util.isPlainObject(target[i])) {
                    if (!lastLevel && !util.areObjectsEqual(original[i], target[i], deepness - 1)) {
                        return false;
                    }
                } else if (util.isArray(original[i]) && util.isArray(target[i])) {
                    if (!lastLevel && !util.areArraysEqual(original[i], target[i], deepness - 1)) {
                        return false;
                    }
                } else if (original[i] !== target[i]) {
                    return false;
                }
            }

            return true;
        };
    })();


    //fn Part
    (function () {
        var update, merge;
        /**
         * Updates an array (of arguments) with values arguments list (args).
         * This function modifies the "array".
         * @method update
         * @param {Array} array Array to update
         * @param {Object} args Arguments list
         * @return Updated array of arguments
         * @memberof module:ax/util
         * @private
         */
        update = function (array, args) {
            var arrayLength = array.length,
                length = args.length;
            while (length--) {
                array[arrayLength + length] = args[length];
            }
            return array;
        };

        /**
         * Copies array and updates it with arguments.
         * Same as _update but does not modify the original array
         * @method merge
         * @param {Array} array Array to update (Not modified)
         * @param {Object} args Arguments list
         * @return Updated array of arguments
         * @memberof module:ax/util
         * @private
         */
        merge = function (array, args) {
            array = Array.prototype.slice.call(array, 0);
            return update(array, args);
        };
        /**
         * Bind a function (func) to a context.
         * If used directly on the function and extending function is allowed, first argument can be omited.
         * @method bind
         * @memberof module:ax/util
         * @param {Function} func to extend (if function extending allowed, this is to be omited)
         * @param {Object} context The context to bind to
         * @param {Object} third or extra arguments will be passed as an input parameter to the Function func binded.
         * @return {Function} Wraped function where execution is guaranteed in the given context
         * @public
         */
        util.bind = function(func, context) {

            if (util.isFunction(Function.prototype.bind)) {
                return Function.prototype.bind.apply(func, Array.prototype.slice.call(arguments, 1));
            }
            
            if (arguments.length < 2 && util.isUndefined(arguments[0])) {
                return func;
            }
            
            var __method = func,
                args = Array.prototype.slice.call(arguments, 2);
            return function () {
                var a = merge(args, arguments);
                return __method.apply(context, a);
            };
        };
        /**
         * Return a list of argument names for a function.
         * @method argumentNames
         * @memberof module:ax/util
         * @param {Function} func to return a list of arguments for
         * @return {Array} An array of strings (argument names)
         * @public
         */
        util.argumentNames = function (func) {
            var names = func.toString().match(/^[\s\(]*function[^(]*\(([^)]*)\)/)[1].replace(/\/\/.*?[\r\n]|\/\*(?:.|[\r\n])*?\*\//g, "").replace(/\s+/g, "").split(",");
            return names.length === 1 && !names[0] ? [] : names;
        };


        /**
         * Promise timeout functions
         * @private
         **/
        (function () {

            var delayRefs = {};

            /**
             * Keep reference a request by is
             * @name refDelay
             * @module
             * @memberof module:ax/util~
             * @private
             */

            function refDelay(id, delay) {

                if (!id || !delay) {
                    return;
                }

                if (delayRefs[id]) {
                    throw core.createException("Repeated id for deferred creation");
                }

                delayRefs[id] = delay;

                return delayRefs[id];

            }

            /**
             * Get the reference of a request by id
             * @name getDelay
             * @module
             * @memberof module:ax/util~
             * @private
             */

            function getDelay(id) {
                return delayRefs[id];
            }


            /**
             * Get the reference of a request by id
             * @name unrefDelay
             * @module
             * @memberof module:ax/util~
             * @private
             */

            function unrefDelay(id) {
                delayRefs[id] = undefined;
            }

            /**
             * Delays execution of function "func" for a time specified in seconds by "timeout".
             *
             * @method delay
             * @memberof module:ax/util
             * @param {Number} timeout Delay time in seconds
             * @param {Number|String} [id] unqiue id for the delay
             * @return {module:ax/promise~Promise}
             * @public
             * @example
             *
             * var id = core.getGuid();
             * // delay 1 second
             * util.delay(1, id).then(function(){
             *     ...do something
             * });
             *
             * // clear the delay, note that id not required if there's no need to clear the delay
             * util.clearDelay(id);
             *
             */
            util.delay = function (timeout, id) {

                var deferred = getPromise().defer(),
                    handle;

                handle = core.root.setTimeout(function () {

                    if (id) {
                        unrefDelay(id);
                    }

                    return deferred.resolve();

                }, timeout * 1000);

                if (id) {
                    refDelay(id, {
                        handle: handle,
                        deferred: deferred
                    });
                }

                return deferred.promise;
            };

            /**
             * Defers execution of the function until the processor is ready.
             * This is essencialy the same as setting timeout to 1ms.
             * @method defer
             * @param {Number|String} [id] unqiue id for the delay
             * @see module:ax/util.delay
             * @memberof module:ax/util
             * @return {module:ax/util~Promise}
             * @public
             */
            util.defer = function (id) {
                return util.delay(0.01, id);
            };


            /**
             * Cancel the promise requested
             *
             * @method clearDelay
             * @memberof module:ax/util
             * @param {module:ax/util~CancellablePromise} timeout Delay time in seconds
             * @public
             */
            util.clearDelay = function (id) {

                if (!id) {
                    return;
                }

                var delay = getDelay(id);

                if (!delay) {
                    return;
                }

                clearTimeout(delay.handle);

                delay.deferred.reject("delay aborted");

                unrefDelay(id);
            };

        })();

        /**
         * Returns a function "wrapped" around the original function.
         * This function lets you easily build on existing functions by specifying before and after behavior,
         * transforming the return value, or even preventing the original function from being called.
         *
         *  The wraper function is called with this signature:
         *      function wrapper(callOriginal[, args...])
         *
         * "callOriginal" is a function that can be used to call the original (wrapped) function (or not, as appropriate).
         * It is not a direct reference to the original function, there's a layer of indirection in-between that sets up
         * the proper context for it.
         *
         * @method wrap
         * @memberof module:ax/util
         * @param {Function} target  to wrap
         * @param {Function} wrapper to wrap with
         * @return Wrapped function with correct references
         * @public
         */
        util.wrap = function (target, wrapper) {
            return function () {
                var a = update([util.bind(target, this)], arguments);
                return wrapper.apply(this, a);
            };
        };
        /**
         * Wraps the function inside another function that, when called, pushes 'this' to the original function
         * as the first argument (with any further arguments following it).
         * This method transforms the original function that has an explicit first argument to a function that
         * passes `this` (the current context) as an implicit first argument at call time.
         * It is useful when wanted to transform a function that takes an object to a method of that object or
         * its prototype, shortening its signature by one argument.
         * @method methodize
         * @memberof module:ax/util
         * @param {Function} target to transform.
         * @return {Function} wrapped function
         * @public
         */
        util.methodize = function (target) {
            /**
             * @ignore
             */
            if (target._methodized) {
                return target._methodized;
            }

            var __method = target;
            /**
             * @ignore
             */
            target._methodized = function () {
                var a = update([this], arguments);
                return __method.apply(null, a);
            };
            return target._methodized;
        };
    }());

    //string Part
    (function () {
        /**
         * Insert a string to a given position
         * @method insert
         * @memberof  module:ax/util
         * @param {String} str String to work with
         * @param {Number} position Number of position to insert (Default last).
         * if number is smaller than 0 or larger the string position, it will insert at the end.
         * @param {String} phrase A phrase to inserted (Default "")
         * @return {String|Boolean} Inserted string. it will return false when it is not string
         * @public
         * @example <caption> To insert the string at position 2</caption>
         * util.insert("testing", 2, "@@");
         */
        util.insert = function (str, position, phrase) {
            if (!util.isString(str)) {
                str = "" + str;
            }

            if (util.isUndefined(position) || position < 0 || position > str.length) {
                position = str.length;
            }
            phrase = util.isUndefined(phrase) ? "" : phrase;

            return str.slice(0, position) + phrase + str.slice(position, str.length);
        };
        /**
         * Truncates a string to a given length, with optional truncation
         * @method truncate
         * @memberof module:ax/util
         * @public
         * @param {String} str String to work with
         * @param {Number} length Number of characters to strip at (Default 30). If it is smaller than 0, it will be 0.
         * @param {String} truncation A truncation(ending) string. (Default "...")
         * @return {String|Boolean} Truncated string.it will return false when it is not string
         * @example <caption> To truncate the string</caption>
         * util.truncate("testing", 4, "...");
         */
        util.truncate = function (str, length, truncation) {
            if (!util.isString(str)) {
                str = "" + str;
            }
            length = util.isNumber(length) ? length : 30;
            truncation = util.isUndefined(truncation) ? "..." : truncation;
            var resultLen = length < 0 ? 0 : length;
            return str.length > length ? str.slice(0, resultLen) + truncation : String(str);
        };
        /**
         * Strips all leading and trailing whitespaces from a string.
         * @method strip
         * @memberof module:ax/util
         * @public
         * @param {String} str String to work with
         * @return {String|Boolean} String with striped whitespaces.it will return false when it is not string
         * @example <caption> To strip the string</caption>
         * util.strip("     testing");
         */
        util.strip = function (str) {
            if (!util.isString(str)) {
                str = "" + str;
            }
            return str.replace(/^\s+/, "").replace(/\s+$/, "");
        };
        /**
         * Converts a string separated by dashes into a camelCase equivalent. For
         * instance, `"foo-bar"` would be converted to `"fooBar"`.
         * @method camelize
         * @memberof module:ax/util
         * @public
         * @param {String} str String to work with.
         * @return {String|Boolean} Camelized string.it will return false when it is not string
         * @example <caption> To camelize the string from foo-bar to fooBar</caption>
         * util.camelize("foo-bar");
         */
        util.camelize = function (str) {
            if (!util.isString(str)) {
                str = "" + str;
            }
            return str.replace(/-+(.)?/g, function (match, chr) {
                return chr ? chr.toUpperCase() : "";
            });
        };
        /**
         * Converts a string from camelCase to decamelized equivalent. For
         * instance, `"fooBar"` would be converted to `"foo-bar"`.
         * @method decamelize
         * @memberof module:ax/util
         * @public
         * @param {String} str String to work with.
         * @param {String} separator String to separate the words. (Default is "-")
         * @return {String|Boolean} Decamelized string.it will return false when it is not string
         * @example <caption> To decamelize the string of fooBar to foo-bar</caption>
         * util.decamelize("fooBar");
         */
        util.decamelize = function (str, separator) {
            if (!util.isString(str)) {
                str = "" + str;
            }
            separator = separator || "-";
            return str.replace(/([a-z])([A-Z])/g, "$1" + separator + "$2").toLowerCase();
        };
        /**
         * Checks if the string is empty or only contains whitespaces.
         * @method empty
         * @memberof module:ax/util
         * @public
         * @param {String} str String to work with
         * @return {Boolean} true if string is empty, false otherwise.
         * @example <caption> To check if the type is empty</caption>
         * util.empty(" ");
         */
        util.empty = function (str) {
            if (!util.isString(str)) {
                str = "" + str;
            }
            return (/^\s*$/).test(str);
        };
        /**
         * Checks if a string starts by the same characters than another specified string
         * @method startsWith
         * @memberof module:ax/util
         * @public
         * @param {String} str String inside which we search for a starting substring
         * @param {String} startStr Starting substring to search for
         * @return {Boolean} true if str starts with startStr.it will return false when it is not string
         * @example <caption> To check if the string start with specific string "a"</caption>
         * util.startsWith("accedo","a");
         */
        util.startsWith = function (str, startStr) {
            //is either undefined, return false;
            if (util.isUndefined(str) || util.isUndefined(startStr)) {
                return false;
            }
            if (!util.isString(str)) {
                str = "" + str;
            }
            if (!util.isString(startStr)) {
                startStr = "" + startStr;
            }
            return str.substr(0, startStr.length) === startStr;
        };
        /**
         * encode the character like & to "&amp;"
         * @method encodeHTML
         * @memberof module:ax/util
         * @public
         * @param {String} str String inside which we need to encode
         * @return {String} s String which is encoded
         * @example <caption> To encode the string from "acc>do" to "acc&g t;do"</caption>
         * util.encodeHTML("acc>do");
         */
        util.encodeHTML = function (str) {
            if (!util.isString(str)) {
                str = "" + str;
            }
            var s = "";
            if (str.length === 0) {
                return "";
            }
            s = str.replace(/&/g, "&amp;");
            s = s.replace(/</g, "&lt;");
            s = s.replace(/>/g, "&gt;");
            s = s.replace(/ /g, "&nbsp;");
            s = s.replace(/\'/g, "'");
            s = s.replace(/\"/g, "&quot;");
            s = s.replace(/\n/g, "<br>");
            return s;
        };
        /**
         * decode the character like &amp; to &
         * @method decodeHTML
         * @memberof module:ax/util
         * @public
         * @param {String} str String which we need to decode
         * @return {String} s String which is decoded
         * @example <caption> To decode the string from "acc&g t;do" to "accedo"</caption>
         * util.decodeHTML("acc&g t;do");
         */
        util.decodeHTML = function (str) {
            if (!util.isString(str)) {
                str = "" + str;
            }
            var s = "";
            if (str.length === 0) {
                return "";
            }
            s = str.replace(/&amp;/g, "&");
            s = s.replace(/&lt;/g, "<");
            s = s.replace(/&gt;/g, ">");
            s = s.replace(/&nbsp;/g, " ");
            s = s.replace(/'/g, "\'");
            s = s.replace(/&quot;/g, "\"");
            s = s.replace(/<br>/g, "\n");
            return s;
        };
    }());


    // JSON Part
    // create and return a valid JSON module
    var root = core.root,
        nullRegex = /^\s*null(\s)*$/,
        jsonModule = {
            /**
             * Parse the JSON string into the object
             * @method parse
             * @memberof module:ax/util
             * @param {String} str the string to be parsed
             * @returns {Object} If the input string is correct format, it will return the JSON object.
             * @public
             * @example <caption> To change the json string into object</caption>
             * util.parse("{"a": 1, "b": 2, "c": 3}");
             */
            parse: function (str) {
                // use the native parse function if available
                if (root.JSON && root.JSON.parse) {
                    return root.JSON.parse(str);
                }

                if (util.isString(str) || util.isNumber(str)) {
                    if (nullRegex.test(str)) {
                        return null;
                    }

                    try {
                        /*jshint -W054 */
                        return (new Function("var input = " + str + "; if (input === undefined || input === null) throw \"Unexpected token\"; return input;"))();
                    } catch (ex) {
                        throw core.createException("JSONParseError", "JSON parse Error");
                    }
                }
                throw core.createException("JSONParseError", "JSON parse Error");
            },
            /**
             * Stringify the object
             * @method  stringify
             * @memberof module:ax/util
             * @param {Object} obj Object to be stringified
             * @returns {String} the JSON string
             * @throws {module:ax/exception.ILLEGAL_ARGUMENT} if the object is not a valid JSON object
             * @public
             * @see the MDN {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify|JSON.stringify} documentation
             * @example <caption> To get the string format of the object</caption>
             * util.stringify({a: 1, b: "boy", c: 3});
             */
            stringify: function (obj) {
                // use the native stringify function if available
                if (root.JSON && root.JSON.stringify) {
                    return root.JSON.stringify(obj);
                }

                var results = [],
                    len, value, property, i;

                if (util.isDOMElement(obj)) {
                    throw core.createException(exception.ILLEGAL_ARGUMENT, "Coverting DOM element to JSON");
                }

                if (obj === null) {
                    return "null";
                }

                if (obj === undefined) {
                    return undefined;
                }

                if (util.isFunction(obj.toJSON)) {
                    return obj.toJSON();
                }

                if (util.isBoolean(obj) || util.isNumber(obj)) {
                    return obj.toString();
                }

                if (util.isString(obj)) {
                    return "\"" + obj.replace(/"/g, "\"") + "\"";
                }

                if (util.isArray(obj)) {
                    var arrayString = "[";

                    len = obj.length;

                    for (i = 0; i < len; i++) {
                        value = this.stringify(obj[i]);
                        arrayString += (util.isUndefined(value) ? null : value) + ",";
                    }

                    return arrayString.substring(0, arrayString.length - 1) + "]";
                }

                if (util.isPlainObject(obj)) {
                    for (property in obj) {
                        value = this.stringify(obj[property]);
                        if (!util.isUndefined(value)) {
                            results.push("\"" + property.toString() + "\":" + value);
                        }
                    }
                    return "{" + results.join(",") + "}";
                }

                return undefined;
            }
        };

    // set the JSON methods under util namespace
    util.parse = jsonModule.parse;
    util.stringify = jsonModule.stringify;

    //breaker will be used in the array and plainObject to break the loop in the each function
    util.breaker = {};

    //Array Part
    (function () {
        /**
         * Returns the first element of the array.
         * @method first
         * @memberof  module:ax/util
         * @param {Array} arr Array to fetch element of
         * @return first element of the array. If it is not array or length is 0. null will be returned.
         * @public
         * @example  <caption> get the first item in the array</caption>
         * var arr = [1,3,5];
         * console.log(util.first(arr));
         * //log 1
         */
        util.first = function (arr) {
            if (!util.isArray(arr)) {
                throw core.createException("ArrayFirstError", "Type is not an array");
            }
            return arr[0];
        };
        /**
         * Returns the first index at which a given element can be found in the array, or -1 if it is not present.
         * IndexOf implementation for JS < 1.6.
         * @method indexOf
         * @memberof  module:ax/util
         * @param {Array} arr Array to search in
         * @param {Object} item Item to search for
         * @param {Number} [i] Index to search from
         * @return {Number} first index at which element is found int he array or -1 if not present
         * @public
         * @example  <caption>get the item index from the array</caption>
         * var arr = [1,3,5];
         * console.log(util.indexOf(arr,3);
         * //log 1
         */
        util.indexOf = function (arr, item, i) {
            if (!util.isArray(arr)) {
                throw core.createException("ArrayIndexOfError", "Type is not an array");
            }

            //to use the default indexOf
            if (util.isFunction(Array.prototype.indexOf)) {
                return arr.indexOf(item, i);
            }

            if (!i) {
                i = 0;
            }
            var len = arr.length;
            if (i < 0) {
                i = len + i;
            }

            for (; i < len; i++) {
                if (arr[i] === item) {
                    return i;
                }
            }
            return -1;
        };
        /**
         * Returns the last element of the array.
         * @method last
         * @memberof  module:ax/util
         * @param {Array} arr Array to fetch element of
         * @return {Object} last element of the array or null when the length is 0 or it is not an array
         * @public
         * @example  <caption>get the item index from the array</caption>
         * var arr = [1,3,5];
         * console.log(util.last(arr);
         * //log 5
         */
        util.last = function (arr) {
            if (!util.isArray(arr)) {
                throw core.createException("ArrayLastError", "Type is not an array");
            }
            return arr[arr.length - 1];
        };
        /**
         * Returns the position of the last occurrence of "item" within the array or -1 if "item" doesn't exist in the array.
         * @method lastIndexOf
         * @memberof  module:ax/util
         * @param {Array} arr Array to search in
         * @param {Object} item Item to search for
         * @param {Number} [i] Index to start searching from
         * @return {Number} last index of the item in the array or -1 if item does not exist
         * @public
         * @example <caption>Get the last index of 5</caption>
         * var ret = util.lastIndexOf([1, 3, 5, 7], 5);
         * //log 2
         * console.log(ret);
         */
        util.lastIndexOf = function (arr, item, i) {

            if (!util.isArray(arr)) {
                throw core.createException("ArrayLastError", "Type is not an array");
            }

            //use the browser default function
            if (util.isFunction(Array.prototype.lastIndexOf)) {
                if (util.isUndefined(i)) {
                    return arr.lastIndexOf(item);
                }
                return arr.lastIndexOf(item, i);
            }

            i = !util.isNumber(i) ? arr.length : (i < 0 ? arr.length + i : i) + 1;
            var n = this.indexOf(arr.slice(0, i).reverse(), item);
            return (n < 0) ? n : i - n - 1;
        };

        /**
         * Flatten out an array, either recursively (by default), or just one level.
         * @method flatten
         * @memberof module:ax/util
         * @param {Array} arr An array to flatten
         * @param {Boolean} isDeep Flatten nested array or not
         * @return {Array} flatten array
         * @example
         *
         * util.flatten([1, 2, [3, [4]]])
         * // [1, 2, 3, [4]]
         *
         * util.flatten([1, 2, [3, [4]]], true)
         * // [1, 2, 3, 4]
         */
        util.flatten = function (arr, isDeep) {
            return baseFlatten(arr, isDeep, false);
        };

        /**
         * Returns the length of array. This is the same as calling arr.length
         * @method size
         * @memberof  module:ax/util
         * @param {Array} arr Array to get size of
         * @return {Number} length of the array. -1 when it is not array
         * @public
         * @example <caption>get the size of the array</caption>
         * var ret = [1,3,5];
         * console.log(util.size(ret));
         * //log 3
         **/
        util.size = function (arr) {
            if (!util.isArray(arr)) {
                throw core.createException("ArraySizeError", "Type is not an array");
            }
            return arr.length;
        };
        /**
         * Removes all items of an Array from an index to another
         * /!\ Your array will be modified, not a copy of it
         * @method remove
         * @memberof  module:ax/util
         * @param {Array} arr Array to work on
         * @param {Number} from Index from which we remove elements
         * @param {Number} [to] Index up to which we remove elements
         * @returns {Number} the resulting Array's length
         * @public
         * @example <caption>Remove the second item from the array [1,2,3] and array will become [1,3]</caption>
         * util.remove([1,2,3],1);
         * <caption>Remove the second-to-last item from the array [1,2,3,4] and array will become [1,2,4]</caption>
         * util.remove([1,2,3,4],-2);
         * <caption>Remove the second and third items  from the array [1,2,3,4,5] and array will become [1,4,5]</caption>
         *  util.remove([1,2,3,4,5],1,2);
         * <caption> Remove the last and second-to-last items from the array [1,2,3,4,5] and array will become [1,2,5]</caption>
         *  util.remove([1,2,3,4,5],-3,-2);
         */
        util.remove = function (arr, from, to) {
            if (!util.isArray(arr)) {
                throw core.createException("ArrayRemoveError", "Type is not an array");
            }
            if (from >= arr.length || (to && to >= arr.length)) {
                throw core.createException("ArrayRemoveError", "Range is out of the array length");
            }
            var rest = arr.slice((to || from) + 1 || arr.length);
            arr.length = from < 0 ? arr.length + from : from;
            return arr.push.apply(arr, rest);
        };

        /**
         * Apply a function against an accumulator and each value of the array (from left-to-right) as to reduce it to a single value.
         * @method reduce
         * @memberof  module:ax/util
         * @param {Array} arr Array to work on
         * @param {Function} iter Iterator function
         * @param {Object} [initialValue] the first argument to the first call of the callback.
         * @public
         * @example <caption>To sum up all the value from left to right</caption>
         * util.reduce([0,1,2,3,4],
         *     function(previousValue, currentValue, index, array){
         *         return previousValue + currentValue;
         *     }
         * );
         */
        util.reduce = function (arr, iter, initialValue) {

            if (!util.isArray(arr)) {
                throw core.createException("ArrayReduceError", "Type is not an array");
            }

            iter = iter || function (x) {
                return x;
            };

            if (arr.length === 0) {
                return initialValue;
            }

            //to use the default some
            if (util.isFunction(Array.prototype.reduce)) {
                if (util.isUndefined(initialValue)) {
                    return arr.reduce(iter);
                }
                return arr.reduce(iter, initialValue);
            }

            if (!util.isFunction(iter)) {
                throw core.createException("ArrayReduceError", "iter is not a function");
            }

            var index = 0,
                length = arr.length,
                value, isValueSet = false;
            if (!util.isUndefined(initialValue)) {
                value = initialValue;
                isValueSet = true;
            }
            for (; length > index; ++index) {
                if (!arr.hasOwnProperty(index)) {
                    continue;
                }
                if (isValueSet) {
                    value = iter(value, arr[index], index, arr);
                } else {
                    value = arr[index];
                    isValueSet = true;
                }
            }
            return value;
        };
        /**
         * Apply a function against an accumulator and each value of the array (from right to left) as to reduce it to a single value.
         * @method reduceRight
         * @memberof  module:ax/util
         * @param {Array} arr Array to work on
         * @param {Function} iter Iterator function
         * @param {Object} [initialValue] initialValue the first argument to the first call of the callback.
         * @public
         * @example <caption>To concate all the value from right to left</caption>
         * var ret = util.reduceRight(["a", "c", "c", "e", "d", "o"],
         *    function(previousValue, currentValue, index, array) {
         *        return previousValue + currentValue;
         *   }
         * );
         * console.log(ret);
         * //log "odecca"
         */
        util.reduceRight = function (arr, iter, initialValue) {

            if (!util.isArray(arr)) {
                throw core.createException("ArrayReduceRightError", "Type is not an array");
            }

            iter = iter || function (x) {
                return x;
            };

            //to use the default some
            if (util.isFunction(Array.prototype.reduceRight)) {
                if (util.isUndefined(initialValue)) {
                    return arr.reduceRight(iter);
                }
                return arr.reduceRight(iter, initialValue);
            }

            if (!util.isFunction(iter)) {
                throw core.createException("ArrayReduceRightError", "iter is not a function");
            }

            var length = arr.length,
                index = length - 1,
                value, isValueSet = false;
            if (!util.isUndefined(initialValue)) {
                value = initialValue;
                isValueSet = true;
            }
            for (; - 1 < index; --index) {
                if (!arr.hasOwnProperty(index)) {
                    continue;
                }
                if (isValueSet) {
                    value = iter(value, arr[index], index, arr);
                } else {
                    value = arr[index];
                    isValueSet = true;
                }
            }
            return value;
        };
        /**
         * Creates a new array with the results of calling a provided iterator function on every element in this array.
         * @method map
         * @memberof  module:ax/util
         * @param {Array} arr Array to work on
         * @param {Function} iter Iterator function to call on every object
         * @param {Object} context Scope to bind everything to
         * @return {Array} New array with modified values
         * @public
         * @example <caption>Get the last index of 5</caption>
         * var ret = [1,3,5];
         * var newArray = util.map(ret,function(i){return i+2;});
         * console.log(newArray);
         * //[2,5,7];
         */
        util.map = function (arr, iter, context) {

            if (!util.isArray(arr)) {
                throw core.createException("ArrayMapError", "Type is not an array");
            }

            iter = iter || function (x) {
                return x;
            };

            //to use the default indexOf
            if (util.isFunction(Array.prototype.map)) {
                return arr.map(iter, context);
            }

            if (!util.isFunction(iter)) {
                throw core.createException("ArrayMapError", "iter is not a function");
            }

            var results = [];

            this.each(arr, function (value, index) {
                results.push(iter.call(context, value, index));
            });

            return results;
        };
        /**
         * Creates a new array with all elements that pass the test implemented by the provided iterator function.
         * For compatibility with JS 1.6 this also passes while array to iterator as 3rd argument
         * @method filter
         * @memberof  module:ax/util
         * @param {Array} arr Array to search in
         * @param {Function} iter Iterator function to use for testing (returns true for pass, false for fail)
         * @param {Object} context Scope of the iterator
         * @return {Array} Elements that passed the test
         * @public
         * @example <caption> To iterate the function for every item in the array and
         * filter the items that satisfy the condition (is Number)</caption>
         * var arr = [1,2,"a",3];
         * var count = 0;
         * var ret = util.filter(arr, function(i){
         *  if(typeof i === "number"){
         *      return true;
         *  }
         *  return false;
         * });
         * //ret will be [1,2,3];
         * console.log(ret);
         */
        util.filter = function (arr, iter, context) {

            if (!util.isArray(arr)) {
                throw core.createException("ArrayFilterError", "Type is not an array");
            }

            iter = iter || function (x) {
                return x;

            };

            //to use the browser default
            if (util.isFunction(Array.prototype.filter)) {
                return arr.filter(iter, context);
            }

            if (!util.isFunction(iter)) {
                throw core.createException("ArrayFilterError", "iter is not a function");
            }

            var results = [];
            this.each(arr, function (value, index) {
                if (iter.call(context, value, index, arr)) {
                    results.push(value);
                }
            });

            return results;
        };
        /**
         * iterate over the array and return the first element that callback function returns turthy
         * @method findIndex
         * @memberof module:ax/util
         * @param {Array} arr Array to search in
         * @param {Function} iter Iterator function to use for testing (returns true for pass, false for fail)
         * @param {Object} context Scope of the iterator
         * @return {Number} index of first found element
         * @example
         *
         * var characters = [
         *   { 'name': 'barney',  'age': 36, 'blocked': false },
         *   { 'name': 'fred',    'age': 40, 'blocked': true },
         *   { 'name': 'pebbles', 'age': 1,  'blocked': false }
         * ];
         *
         * util.findIndex(characters, function(chr) {
         *   return chr.age < 20;
         * });
         * // 2
         */
        util.findIndex = function (arr, iter, context) {
            var index = -1,
                length = arr ? arr.length : 0;

            iter = iter || function (x) {
                return x;
            };

            while (++index < length) {
                if (iter.call(context, arr[index], index, arr)) {
                    return index;
                }
            }
            return -1;
        };
        /*
         * iterate over the array and return the first element that callback function returns turthy
         * @method find
         * @memberof module:ax/util
         * @param {Array} arr Array to search in
         * @param {Function} iter Iterator function to use for testing (returns true for pass, false for fail)
         * @param {Object} context Scope of the iterator
         * @return {Number} first found element
         * @example
         *
         * var characters = [
         *   { 'name': 'barney',  'age': 36, 'blocked': false },
         *   { 'name': 'fred',    'age': 40, 'blocked': true },
         *   { 'name': 'pebbles', 'age': 1,  'blocked': false }
         * ];
         *
         * util.find(characters, function(chr) {
         *   return chr.age < 20;
         * });
         * // { 'name': 'pebbles', 'age': 1,  'blocked': false }
         */
        util.find = function (arr, iter, context) {
            var index = util.findIndex(arr, iter, context);
            return index > -1 ? arr[index] : undefined;
        };
        /**
         * Tests whether some element in the array passes the test implemented by the provided iterator function.
         * @method some
         * @memberof  module:ax/util
         * @param {Array} arr Array to work on
         * @param {Function} iter Iterator function to test with
         * @param {Object} context Scope to bind everything to
         * @return {Boolean} true if at least one object passes iterator test, false otherwise
         * @public
         * @example <caption>To check if the array contain any number</caption>
         * var arr = ["a","b","c",1];
         * var ret = util.some(arr,function(i){
         *      if(typeof i === "number"){
         *          return true;
         *      }
         *     return false;
         * });
         * console.log(ret);
         * //log: true
         */
        util.some = function (arr, iter, context) {

            if (!util.isArray(arr)) {
                throw core.createException("ArraySomeError", "Type is not an array");
            }

            iter = iter || function (x) {
                return x;
            };
            //to use the default some
            if (util.isFunction(Array.prototype.some)) {
                return arr.some(iter, context);
            }

            if (!util.isFunction(iter)) {
                throw core.createException("ArraySomeError", "iter is not a function");
            }

            var result = false;
            this.each(arr, function (value, index) {
                result = iter.call(context, value, index);
                if (result) {
                    return util.breaker;
                }
            }, this);
            return result;
        };
        /*
         * Retrieves the value of a specified property from all elements in the collection.
         * @method pluck
         * @memberof module:ax/util
         * @param {Array.<Object>} arr collection of object
         * @param {String} propertyName The name of the property to pluck.
         * @return {Array} Array of property values
         * @example
         *
         * var characters = [
         *     { 'name': 'barney', 'age': 36 },
         *     { 'name': 'fred',   'age': 40 }
         * ];
         *
         * util.pluck(characters, 'name');
         * // ['barney', 'fred']
         */
        util.pluck = function (arr, propertyName) {
            var mapper = function (obj) {
                return (propertyName === null) ? undefined : obj[propertyName];
            };
            return util.map(arr, mapper);
        };
        /*
         * Return a copy of the object only containing the whitelisted properties.
         * @method pick
         * @memberof module:ax/util
         * @param {Object} obj An plain object
         * @param {Function|...String|String[]} iter Iteratee or property keys
         * @param {Object} context The this binding of iter function
         * @return {Object} a copy of the object only containing the whitelisted properties.
         * @example
         *
         * util.pick({ "name": "fred", "_userid": "fred" }, "name");
         * // { "name": "fred" }
         *
         * util.pick({ "name": "fred", "_userid": "fred" }, "name", "_userid");
         * // { "name": "fred", "_userid": "fred" }
         *
         * util.pick({ "name": "fred", "_userid": "fred" }, ["name", "_userid"]);
         * // { "name": "fred", "_userid": "fred" }
         *
         * util.pick({ "name": "fred", "_userid": "fred1" }, function(value, key) {
         *     return key.charAt(0) != "_";
         * })
         */
        util.pick = function (obj, iter, context) {
            var result = {},
                key;
            if (obj === null) {
                return result;
            }
            if (util.isFunction(iter)) {
                for (key in obj) {
                    var value = obj[key];
                    if (iter.call(context, value, key, obj)) {
                        result[key] = value;
                    }
                }
            } else {
                var keys = baseFlatten(arguments, false, false, 1);
                obj = new Object(obj);
                for (var i = 0, length = keys.length; i < length; i++) {
                    key = keys[i];
                    if (key in obj) {
                        result[key] = obj[key];
                    }
                }
            }
            return result;
        };
    }());

    //plainObject part
    (function () {
        /**
         * Returns an array of keys.
         * @method keys
         * @memberof module:ax/util
         * @param {Object} object Object to check
         * @return {Array} an array contain all keys
         * @public
         * @example <caption> To return the keys of the object</caption>
         * util.keys({"1": "studing", "studied": "false", "hong": "kong"});
         * It will return ["1", "studied", "hong"];
         */
        util.keys = function (object) {
            var results = [],
                property;

            // use native keys function
            if (util.isFunction(Object.keys)) {
                return Object.keys(object);
            }

            for (property in object) {
                if (object.hasOwnProperty(property)) {
                    results.push(property);
                }
            }
            return results;
        };

        /**
         * Returns the debug-oriented string representation of the object
         * @method inspect
         * @memberof module:ax/util
         * @param {Object} object object need to be inspected
         * @return {String} string representation of the object
         * @public
         * @example <caption>To get all the information about the object object </caption>
         * var obj = {"mon":"13/5","tue":14,"wed":15,"thurs":"diecisiete","fri":"eighteen"};
         * console.log(util.inspect(obj));
         * //log "#<object:{mon:13/5, tue:14, wed:15, thurs:diecisiete, fri:eighteen}>"
         * empty object will be "#<object:{}"
         */
        util.inspect = function (object) {
            var result, keys = this.keys(object),
                values = this.values(object),
                i, length = keys.length;
            result = "#<object:{";
            if (length === 0) {
                return result + "}>";
            }
            for (i = 0; i < length; i++) {
                result = result + keys[i] + ":" + values[i] + ", ";
            }
            result = result.substr(0, result.length - 2) + "}>";
            return result;
        };

        /**
         * Returns an array of values.
         * Note: Key order is depending on JS implementation
         * @method values
         * @memberof module:ax/util
         * @param {Object} object Object to get the values
         * @return {Array} Values of this object
         * @public
         * @example <caption> To get the values of the obejct </caption>
         * util.values({a:1, b:2, c:3}); -> [1,2,3]
         */
        util.values = function (object) {
            var v = [],
                key;
            for (key in object) {
                v.push(object[key]);
            }
            return v;
        };
        /**
         * Returns a URL-encoded string containing the object's contents as query parameters.
         * @method toQueryString
         * @memberof module:ax/util
         * @param {Object} object object Object to querystring
         * @return {String} key-value pairs represented as a querystring
         * @public
         * @example <caption>To change into the query string and usually for the ajax</caption>
         *  util.toQueryString({action: "ship",
         *      order_id: 123,
         *      fees: ["f1", "f2"]
         *  })-> "action=ship&order_id=123&fees=f1&fees=f2"
         *
         *  util.toQueryString({comment: "",
         *      "key with spaces": true,
         *      related_order: undefined,
         *      contents: null,
         *      "label": "a demo"
         *  }); // -> "comment=&key%20with%20spaces=true&related_order&contents=&label=a%20demo"
         *
         *  // an empty object is an empty query string:
         *  util.toQueryString(); // -> ""
         */
        util.toQueryString = function (object) {
            var results = [],
                __toQueryPair;
            /**
             * Converts a key/value pair to a query pair in the form of "key=value"
             * @param {String} key the key
             * @param {Object} value the value
             * @private
             */
            __toQueryPair = function (key, value) {
                if (util.isUndefined(value)) {
                    return key;
                }
                return (key + "=" + encodeURIComponent((value === null) ? "" : String(value)));
            };
            util.each(object, function (pair) {
                var key = encodeURIComponent(pair.key),
                    values = pair.value,
                    i, len, value, queryValues;
                if (values && typeof values === "object") {
                    if (util.isArray(values)) {
                        i = 0;
                        len = values.length;
                        queryValues = [];
                        for (; i < len; i++) {
                            value = values[i];
                            queryValues.push(__toQueryPair(key, value));
                        }
                        results = results.concat(queryValues);
                    }
                } else {
                    results.push(__toQueryPair(key, values));
                }

            }, this);
            return results.join("&");
        };

        /*
         * Check if an object has a given property directly
         * @method has
         * @memberof module:ax/util
         * @param {Object} obj An plain object
         * @param {String} key Property name
         * @return {Boolean} whether object has the property
         * @public
         * @example
         * util.has({"a":1,"b":2},"a");
         * //true
         */
        util.has = function (obj, key) {
            return obj !== null && hasOwnProperty.call(obj, key);
        };
    }());

    //Share part between array and plainObject
    (function () {
        var checkType = function () {
            if (util.isArray(arguments[0])) {
                return true;
            }
            if (util.isPlainObject(arguments[0])) {
                return false;
            }

            throw core.createException("Exception", "No such related function of the type" + typeof arguments[0]);
        };
        /**
         * Clears the array/object (makes it empty) and returns the array/object reference.
         * @method clear
         * @memberof  module:ax/util
         * @param {Array|Object} target Targeted object or array want to clear
         * @return {Array|Object} array/object reference.
         * @public
         * @example <caption> To clear up the array and return the array e</caption>
         * var arr = [1,3,5];
         * util.clear(arr);
         * console.log(arr);
         * //[]
         *
         * var obj = {a:1,b:2,c:3};
         * util.clear(obj);
         * console.log(obj);
         * //{}
         */
        util.clear = function (target) {
            var obj, i, arr, len, keys;
            //array
            if (checkType(target)) {
                arr = target;
                arr.length = 0;
                return arr;
            }
            //object
            obj = target;
            i = 0;
            keys = util.keys(obj);
            len = keys.length;
            for (; i < len; i++) {
                delete obj[keys[i]];
            }
            return obj;
        };
        /**
         * Iterates array/object and for each item calls iterator function, with given context.
         * To break the loop, throw {@link ax/util.breaker}
         * @method each
         * @memberof  module:ax/util
         * @param {Array|Object} target Array/Object to iterate
         * @param {Function} iter Iterator function
         * @param {Object} context A context to bind iterator to
         * @public
         * @example <caption> To iterate the function for each item in the array</caption>
         * //array
         * var arr = [1,2,3];
         * var arr1 = util.each(arr, function(i){console.log(i);});
         *
         * //object
         *  var string = "";
         * var count = 0;
         * util.each({a:1,b:2,c:3},function(pair){
         *  string += pair.key;
         *  count += pair.value;
         * });
         * <caption>To stop when reach "b", we return util.breaker to end the each function </caption>
         * util.each({a:1,b:2,c:3},function(pair){
         *     if(pair.key === "b"){
         *         return util.breaker;
         *     }
         * });
         */
        util.each = function (target, iter, context) {
            var arr, i, len, key, value, pair, object;
            iter = iter || function (x) {
                return x;
            };
            if (!util.isFunction(iter)) {
                throw core.createException("EachError", "iter is not a function");
            }
            if (checkType(target)) {
                arr = target;

                // it is not possible to break a forEach natively, so we use some function instead
                // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach
                if (util.isFunction(Array.prototype.some)) {
                    arr.some(function () {
                        // workaround, check if util.breaker is returned from iter function
                        return (iter.apply(context, arguments) === util.breaker);
                    }, context);
                    return;
                }

                i = 0;
                len = arr.length;
                for (; i < len; i++) {
                    if (iter.call(context, arr[i], i, arr) === util.breaker) {
                        return;
                    }
                }
                return;
            }
            object = target;
            for (key in object) {
                value = object[key];
                pair = [key, value];
                pair.key = key;
                pair.value = value;
                if (iter.call(context, pair) === util.breaker) {
                    return;
                }
            }

        };
        /**
         * Tests whether all elements in the array/object pass the test implemented by the provided iterator function
         * @method every
         * @memberof  module:ax/util
         * @param {Array|Object} target Array to loop through
         * @param {Function} iter Iterator function
         * @param {Object} context A context to bind iterator to
         * @return {Boolean} true if every element passes the test, false otherwise
         * @public
         * @example <caption> To iterate the function for every item in the array and check if all the items are number</caption>
         * var arr = [1,2,"a",3];
         * var obj = {"a":1, b:"a", "c": 5, "d":"b"};
         * var typeCheck = function(i){
         *  if(typeof i === "number"){
         *      return true;
         *  }
         *  return false;
         * }
         *
         * //array
         * var ret = util.every(arr, typeCheck);
         * //ret will be false as not all the number is number
         * console.log(ret);
         *
         * //object
         * var ret = util.every(obj, typeCheck);
         * //false since not all the values are number
         *
         */
        util.every = function (target, iter, context) {
            var arr, result,
                isArray = false;

            iter = iter || function (x) {
                return x;
            };

            if (checkType(target)) {
                isArray = true;
                //array case

                //to use the browser default
                if (util.isFunction(target.every)) {
                    return target.every(iter, context);
                }
            }

            result = true;

            //plain object case and array without array.every function
            this.each(target, function (value, index) {

                if (!isArray) {
                    value = value.value;
                }

                result = result && !!iter.call(context, value, index, arr);
                //Stop execution and break directly in the each function
                if (!result) {
                    return util.breaker;
                }
            }, this);
            return result;
        };
        /**
         * Determine if the array or object contains a given value (using `===`).
         * @method contains
         * @memberof module:ax/util
         * @param {Array|Object} arr Collection
         * @param {String|Number|Object} target Value
         * @return {Boolean} contain the value or not
         * @example
         *
         * util.contains([1, 2, 3], 1);
         * // true
         
         * util.contains({"a":1,"b":2, "c":3}, 1);
         * // true
         */
        util.contains = function (arr, target) {
            if (arr === null) {
                return false;
            }
            if (arr.length !== +arr.length) {
                arr = util.values(arr);
            }
            return util.indexOf(arr, target) >= 0;
        };

    }());
    return util;
});
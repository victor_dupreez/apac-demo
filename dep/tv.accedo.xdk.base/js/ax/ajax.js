/**
 * Ajax module
 *
 * @module ax/ajax
 */
/* jshint strict:false*/
define("ax/ajax", ["ax/core", "ax/util", "ax/promise", "ax/console"], function (core, util, promise, console) {
    //util fn
    var requestRefs = {};

    //determine whether need clone the transport object since some devices will delete the transport to have a better memory control
    //and avoid multiple transport, so we have to clone the response and keep the data.
    var needCloneTransport = (function () {
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf("maple") !== -1 || typeof tizen !== "undefined") {
            return true;
        }
        return false;
    })();

    //determine whether support specific method
    function isMethodSupported(method) {
        //final check for different platform to avoid those unsupported request
        var agent = navigator.userAgent.toLowerCase();

        //samsung 2010 checking (user agent will container maple 5.1 )since delete is not supported.
        if (agent.indexOf("maple 5.1") !== -1 && method === "delete") {
            return false;
        }

        //sharp && huawei
        if (agent.indexOf("aquosbrowser") > -1 || agent.indexOf("ipad;") > -1) {
            if (method === "delete" || method === "put") {
                return false;
            }
        }

        return true;
    }


    /**
     * Keep reference a request by id
     * @name refRequest
     * @memberof module:ax/ajax
     * @private
     */

    function refRequest(requestId, request) {

        if (!requestId || !request) {
            return;
        }

        if (requestRefs[requestId]) {
            throw core.createException("Repeated id for deferred creation");
        }

        requestRefs[requestId] = request;

        return requestRefs[requestId];

    }

    /**
     * Get the reference of a request by id
     * @name refRequest
     * @memberof module:ax/ajax
     * @private
     */

    function getRequest(requestId) {
        return requestRefs[requestId];
    }


    /**
     * Get the reference of a request by id
     * @name unrefRequest
     * @memberof module:ax/ajax
     * @private
     */

    function unrefRequest(requestId) {
        requestRefs[requestId] = undefined;
    }


    /**
     * create the XMLHttpRequest
     * @returns {Object} A new XHR object
     * @memberof module:ax/ajax
     * @method createStandardXHR
     * @private
     */

    function createStandardXHR() {
        try {
            return new XMLHttpRequest();
        } catch (e) {}
    }
    /**
     * create the ActiveXObject("Msxml2.XMLHTTP")
     * @returns {Object} a new XHR Object
     * @memberof module:ax/ajax
     * @method createActiveXHR
     * @private
     */

    function createActiveXHR() {
        try {
            return new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {}
    }
    /**
     * create the ActiveXObject("Microsoft.XMLHTTP")
     * @returns {Object} a new XHR Object
     * @memberof module:ax/ajax
     * @method createActiveXHRMicrosoft
     * @private
     */

    function createActiveXHRMicrosoft() {
        try {
            return new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {}
    }
    /**
     * Gets a transport by trying all of the know XHR objects
     * @returns A new XHR object or false if none found
     * @memberof module:ax/ajax
     * @method getTransport
     * @private
     */

    function getTransport() {
        //TODO: see if we would like to support IE XDomainRequest
        //http://msdn.microsoft.com/en-us/library/ie/cc288060(v=vs.85).aspx
        return createStandardXHR() || createActiveXHR() || createActiveXHRMicrosoft() || false;
    }


    /**
     * The internal Ajax request object used by {@link module:ax/util}
     *
     * @class AjaxRequest
     * @see {@link module:ax/util}
     * @memberof module:ax/ajax
     * @param {String} url the URL to send request to
     * @param {Object} options the options passed in during an Aajx request
     * @private
     */

    function AjaxRequest(url, options) {

        options = options || {};

        /**
         * The native ajax transport object.
         * @memberof module:ax/ajax.AjaxRequest#
         * @type {XMLHttpRequest}
         * @private
         */
        var __transport = null,

            /**
             * The native ajax transport object.
             * @memberof module:ax/ajax.AjaxRequest#
             * @type {XMLHttpRequest}
             * @private
             */
            __deferred,
            /**
             * Whether this request has finished.
             * @memberof module:ax/ajax.AjaxRequest#
             * @type {boolean}
             * @private
             */
            __completed = false,
            /**
             * To store the timeout when it sends out the request
             * @memberof module:ax/ajax.AjaxRequest#
             * @type {Object}
             * @private
             */
            __connectionTimeout,
            /**
             * Options mapping.
             * @memberof module:ax/ajax.AjaxRequest#
             * @type {Object}
             * @private
             */
            __options, __aborted = false,
            __url = url,
            params, body;



        //default is 30s
        options.timeOut = util.isNumber(options.timeOut) ? options.timeOut : 30;

        // options should be a Hash object, cast to plain object
        if (util.isFunction(options.toObject)) {
            options = options.toObject();
        }

        // Defaults
        __options = {
            method: "get",
            async: true,
            checkContentType: false,
            parameters: ""
        };



        util.extend(__options, options);


        __options.method = __options.method.toLowerCase();

        __transport = getTransport();

        //reference this request by id
        if (__options.id) {
            refRequest(__options.id, this);
        }

        //Request parameters
        params = util.isString(__options.parameters) ? __options.parameters : util.toQueryString(__options.parameters);

        //If method is not GET or POST or DELETE or PUT, set _method argument and append it to querystringUtil. Change method to POST
        if (["get", "post", "delete", "put"].indexOf(__options.method) === -1) {
            params += (params ? "&" : "") + "_method=" + __options.method;
            __options.method = "post";
        }

        //Prepare URL for GET request
        if (params && __options.method === "get") {
            if (util.isString(params) && params.length > 0) {
                // when GET, append parameters to URL
                __url += ((__url.indexOf("?") > 0) ? "&" : "?") + params;
            }
        }

        /**
         * Internal State change callback function.
         * Dispatched ReadyState handler if needed.
         * @method __onStateChange
         * @memberof module:ax/ajax.AjaxRequest#
         * @private
         */

        function __onStateChange() {
            var readyState = __transport.readyState;
            if (readyState > 1 && !((readyState === 4) && __completed)) {
                __respondToReadyState(readyState);
            }
        }



        /**
         * Internal ReadyState handler.
         * Parses the response and also tries to parse the response to JSON if needed.
         * Calls state handlers from options array if such are set.
         * @method __respondToReadyState
         * @memberof module:ax/ajax.AjaxRequest#
         * @private
         */

        function __respondToReadyState(readyState) {
            var data, status, success, resp, contentType, newTransport;
            data = null;
            try {
                // State == 4 means DONE
                if (readyState === 4 && !__aborted) {
                    //when able to receive any data within time, clear timeout
                    if (__connectionTimeout) {
                        clearTimeout(__connectionTimeout);
                        __connectionTimeout = null;
                    }

                    __completed = true;
                    status = __transport.status;
                    success = true;
                    resp = __transport.responseText;

                    //Parse the json if response if json
                    contentType = __transport.getResponseHeader("content-type");

                    if (contentType && contentType.toLowerCase().indexOf("json") > -1) {
                        try {
                            __transport.responseJSON = util.parse(resp);
                        } catch (ex) {
                            console.info("Unable to parse JSON");
                        }
                    }

                    //clone the transport object for samsung
                    if (needCloneTransport) {
                        newTransport = {};
                        newTransport.response = __transport.response || "";
                        newTransport.responseText = __transport.responseText || "";
                        newTransport.responseType = __transport.responseType || "";
                        newTransport.responseJSON = __transport.responseJSON || "";
                        newTransport.responseXML = __transport.responseXML || "";
                        newTransport.readyState = __transport.readyState || null;
                        newTransport.status = __transport.status;
                        newTransport.statusText = __transport.statusText || "";
                        (function (allHeaders) {
                            newTransport.getAllResponseHeaders = function () {
                                return allHeaders;
                            };

                            var allHeadersObj;
                            newTransport.getResponseHeader = function (key) {
                                if (!key) {
                                    return null;
                                }

                                // only parse allheaders when needed
                                if (!allHeadersObj) {
                                    allHeadersObj = {};

                                    if (allHeaders) {
                                        util.each(allHeaders.split("\n"), function (line) {
                                            var colIdx = line.indexOf(":");

                                            if (colIdx < 0) {
                                                return;
                                            }

                                            var k = line.substr(0, colIdx).toLowerCase();

                                            allHeadersObj[k] = line.substr(colIdx + 1);
                                        });
                                    }
                                }

                                // Header keys are case-insensitive
                                key = key.toLowerCase();

                                // Return null according to standard
                                return key in allHeadersObj ? allHeadersObj[key] : null;
                            };
                        })(__transport.getAllResponseHeaders());
                    }

                    //status 0 means its state is in UNSENT / OPENED or ERROR flag is set
                    if (((status >= 200 && status < 300) || status === 304)) {

                        __deferred.resolve(newTransport || __transport);


                    } else {
                        //Unsuccessfull request
                        success = false;
                        __deferred.reject({
                            transport: newTransport || __transport,
                            type: "failure"
                        });

                    }

                    cleanUp();
                }

            } catch (ex) {

                __deferred.reject({
                    transport: newTransport || __transport,
                    ex: ex,
                    type: "exception"
                });

                cleanUp();

                console.error(ex);
            }
        }

        /**
         * Do the abort requst task
         * @name __abort
         * @method
         * @memberof module:ax/ajax.AjaxRequest#
         * @private
         */

        function cleanUp() {
            // unref from promise registry
            if (__options.id) {
                unrefRequest(__options.id);
            }

            //this is for Samsung only, in official doc they say if not destroy() it"ll cause memory issue
            if (__transport.destroy) {
                __transport.destroy();
            }
            /*jshint -W051 */
            delete __transport; // this is also for Samsung, on 3.0 compatible TVs
        }

        /**
         * Do the abort requst task
         * @name doAbort
         * @method
         * @memberof module:ax/ajax.AjaxRequest#
         * @private
         */

        function doAbort() {

            console.info("abort the transport");
            /* The idea is to allow user to abort the request and stop execution.*/
            //Notify this request has been aborted (will be checked against in the onSuccess)
            __aborted = true;

            try {
                __transport.abort();
            } catch (e) {
                //Abort is not supported
                __aborted = null;
            }

            if (__connectionTimeout) {
                clearTimeout(__connectionTimeout);
                __connectionTimeout = null;
            }

            cleanUp();

        }

        /**
         * The abort function for external trigger
         * @name abort
         * @method
         * @memberof module:ax/ajax.AjaxRequest#
         * @private
         */

        function abort() {
            console.info("abort the transport");
            /* The idea is to allow user to abort the request and stop execution.*/
            //Notify this request has been aborted (will be checked against in the onSuccess)
            doAbort();

            __deferred.reject({
                transport: __transport,
                type: "abort"
            });
        }

        this.abort = abort;


        /**
         * Opens the transport, initialize it correctly and send the request
         * @name send
         * @method
         * @memberof module:ax/ajax.AjaxRequest#
         * @private
         */

        function send() {
            var headers, name, avoid;
            //Prepare request and send it

            //to create promise
            __deferred = promise.defer();

            try {

                __transport.open(__options.method.toUpperCase(), __url, __options.async);

                //ReadyState
                __transport.onreadystatechange = __onStateChange;

                //Set request headers if needed
                avoid = false;
                headers = __options.requestHeaders || {};

                for (name in headers) {
                    console.info(name + " - " + headers[name]);
                    __transport.setRequestHeader(name, headers[name]);
                }
                console.log(__transport);

                /* withCredentials support on request: http://www.html5rocks.com/en/tutorials/cors/
                make sure XHR supports withCredentials, it only works with cookies */

                // only set withCredentials if it is explicitly set to true in options
                if (!!__options.withCredentials) {
                    var withCredentialsException = function () {
                        doAbort();
                        console.warn("withCredentials is not supported in this browser");
                        return promise.reject({
                            transport: __transport,
                            ex: core.createException("withCredentials is not supported in this browser"),
                            type: "exception"
                        });
                    };

                    if ("withCredentials" in __transport) {
                        try {
                            __transport.withCredentials = true;
                        } catch (ex) {
                            return withCredentialsException();
                        }
                    } else {
                        return withCredentialsException();
                    }
                }


                body = null;

                //Prepare body
                if (!util.isUndefined(__options.postBody)) {
                    body = __options.postBody;
                } else if (!util.isUndefined(params)) {
                    body = params;
                }

                avoid = !isMethodSupported(__options.method);

                if (avoid) {
                    doAbort();
                    console.info("unsupport method in this device" + __options.method);

                    return promise.reject({
                        transport: __transport,
                        ex: core.createException("unsupport method in this device"),
                        type: "exception"
                    });
                }

                //set connection timeout before send
                __connectionTimeout = setTimeout(function () {

                    doAbort();

                    //treat timeout as connection failure
                    __deferred.reject({
                        transport: __transport,
                        type: "failure"
                    });


                    cleanUp();

                    __connectionTimeout = null;

                }, __options.timeOut * 1000);

                if (body) {
                    __transport.send(body);
                } else {
                    __transport.send();
                }

                /* Force Firefox to handle ready state 4 for synchronous requests */
                /* Seems current Firefox does not have this issue, so code block removed */

            } catch (ex) {
                __deferred.reject({
                    transport: __transport,
                    ex: ex,
                    type: "exception"
                });
                console.error(ex.stack);
            }

            return __deferred.promise;
        }
        this.send = send;
    }
    return {
        /**
         *  Extends from XMLHttpRequest <http://www.w3.org/TR/XMLHttpRequest/>
         * Transport object
         * @typedef Transport
         * @memberof module:ax/ajax
         * @property {Object} responseJSON Parsed JSON from repsonseText if ContentType is JSON
         */

        /**
         * Rejection object
         * @typedef Rejection
         * @memberof module:ax/ajax
         * @property {module:ax/ajax.Transport}  transport
         * @property {String} type either "failure", "exception" or "abort"
         * @property {Exception} [ex] the exception thrown
         */

        /**
         * Extends from {@link module:ax/promise~Promise}
         * It is an extends promise to call then and done. Inside the promise on the ajax, we promise to give the success response.
         * If the request if successful, the promise is fullfilled and you will get the Transport {@link module:ax/ajax.Transport}  object from fulfilled callback.
         * Other than sucessfully, failure, abort, and exception cases the ajax will go into rejected one. And the Rejection {@link module:ax/ajax.Rejection} object  will be passed inside.
         * @typedef Promise
         * @property {Function} abort To abort the ajax.
         * @memberof module:ax/ajax
         * @see module:ax/ajax.Rejection
         * @see module:ax/ajax.Transport
         * @example <caption> To handle succeed case and error case, so it is easier to handle and be more promise...</caption>
         *
         * // normally
         * ajax.request("http://accedo.tv").then(
         *     function(transport){
         *         console.log(transport.responseJSON);
         *     },
         *     function(rejection){
         *         //rejection.type may be "onFailure"/"onAbort"/"onException"
         *         console.log(rejection.type);
         *     }
         * );
         *
         **/

        /**
         * Sends an ajax request.
         *
         * Tested platform : Samsung, LG, Sharp, huawei, opera tv store
         * LG,Samsung 2011TV,Samsung 2012 TV, opera and workstation support post, get, put and delete methods
         * Samsung 2010 TV supports post,get and put methods.But method delete will crash the apps
         * Sharp and huawei supports post and get methods. Delete and put methods will automatically change into get method
         *
         * Tested platform : Samsung, LG, Sharp, huawei, opera tv store
         * LG,Samsung 2011TV,Samsung 2012 TV, opera and workstation support post, get, put and delete methods
         * Samsung 2010 TV supports post,get and put methods.But method delete will crash the apps
         * Sharp and huawei supports post and get methods. Delete and put methods will automatically change into get method
         *
         * @method request
         * @param {String} url The URL to send request to.
         * @param {Object} [options] Object to be set by using the different attributes
         * @param {String} [options.method]  the HTTP request method, i.e. "get" "post", use lower case only! Defaults to "get".
         * @param {String} [options.async] whether the request to send is asynchronous or not. Defaults to be true.
         * @param {Object} [options.requestHeaders]  the header settings, should be contained in an Javascript plain object.e.g content tpye. But some devices may not support.
         * e.g WD Liverpool is unable to set "Content-Type".
         * @param {String} [options.postBody] the post body for the "post" and "put" reqeust.
         * @param {Number} [options.timeOut] request timeout interval(in sec), defaults to 30 seconds.
         * @param {Number|String} [options.id] provide request id for ajax.abort usage.
         * @param {Boolean} [options.withCredentials] enable the withCredentials for the request. It needs to work with exist value in document.cookies
         * @param {String | Object} [options.parameters] the request parameters. Note that it would be set as postBody when doing "post" or "put" request.
         * In case to add querystring in "post" or "put", you should append to url instead
         * @returns {Promise.<Object>} the native transport object. The actual object may differ from platforms
         * @throws {Promise.<Object>} an error object that consists of the native transport object and the failure type
         * @memberof module:ax/ajax
         * @public
         * @example <caption> POST request and send as form data</caption>
         *
         * ajax.request("http://analytics.appledaily.com.hk/smarttv/m.php", {
         *      method: "post",
         *      requestHeaders: {
         *           "Content-Type" : "application/json;charset=UTF-8"
         *      },
         *      parameters:{
         *          type:"pageview",
         *          pageview:"/article/2"
         * }}).then(function(transport) {
         *      console.log (transport.responseJSON);
         * }, function(rejection) {
         *      if (rejection.type == "failure") {
         *          //dosomething
         *      }
         * });
         *
         */
        request: function (url, options) {
            var request = new AjaxRequest(url, options);
            //Manage the transport and send the request
            return request.send();
        },
        /**
         * Abort an ajax request.
         * @method abort
         * @param {Number|String} requestId The request id to abort
         * @memberof module:ax/ajax
         * @public
         * @example
         * var requestId = core.getGuid();
         *
         * ajax.request("http://analytics.appledaily.com.hk/smarttv/m.php", {
         *      id: requestId
         * }});
         *
         * //abort a request
         * ajax.abort(requestId);
         *
         */
        abort: function (requestId) {

            var request = getRequest(requestId);

            if (request) {
                request.abort();
            }
        }

    };


});
/* jshint strict:true */
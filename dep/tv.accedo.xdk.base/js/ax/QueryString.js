/**
 * Utility class on the query strings of current URL.  
 *
 * @class ax/QueryString
 * @since 2.1
 * @author Marco Fan <marco.fan@accedo.tv>
 * @param {String} [url=window.location.href] The url used to parse the query string. Query string with leading "?" sign are also accepted.
 * @example
 * var queryString = new QueryString("http://127.0.0.1/queryStringExample?a=1");
 * var anotherQueryString = new QueryString("?a=1");
 */
define("ax/QueryString", ["ax/class", "ax/util"], function (klass, util) {
    "use strict";    
    return klass.create({
        /**
         * Extracts the parameters from the specified query string to a key-value paired object.  
         * 
         * @function parse
         * @static
         * @memberof ax/QueryString
         * @param {String} queryString The query string to be processed
         * @returns {Object} An string-string key-value paired object
         * @example
         * var queryString = new QueryString("http://127.0.0.1/queryStringExample?a=1&b=2"),
         *     strQueryString = queryString.getQueryString(); // "a=1&b=2"
         * 
         * QueryString.parse(strQueryString); // {"a": "1", "b": "2"}
         */
        parse : function(queryString) {
            if (!queryString || !util.isString(queryString)) {
                return {};
            }

            var pairs,
                matches,
                params = {},
                i,
                length;

            // each pair falls into the set {"_key_", "_key_=", "_key_=_value_"}
            pairs = queryString.split("&");

            for (i = 0, length = pairs.length; i < length; i++) {
                matches = pairs[i].match(/^([^=]+)=?(.*)$/);

                if (matches) {
                    params[matches[1]] = matches[2];
                }
            }

            return params;
        }
    }, {
        /**
         * The parameters extracted from the uri.
         * @prop __parameters
         * @private
         * @memberof ax/QueryString#
         */
        __parameters: [],

        /**
         * The query string in string format.
         * @prop __queryString
         * @private
         * @memberof ax/QueryString#
         */
        __queryString: "",

        /* Constructor of QueryString, which initiate the query string instance. */
        init: function(url) {
            if (!url) {
                url = window.location.href;
            }

            var matches;

            // extract the query string from the url
            matches = url.match(/^([^?]*)\?(.*)$/);
            if (!matches) {
                this.__queryString = "";
            } else {
                this.__queryString = matches[2];
            }

            this.__parameters = this.constructor.parse(this.__queryString);
        },

        /**
         * Retrieves the value of a parameter in the query string using a specified key.  
         * 
         * @function getValue
         * @public
         * @memberof ax/QueryString#
         * @param {String} key The key of the parameter to ask for
         * @returns {String} Mapped value of the key, or null if none exists
         * @example
         * var queryString = new QueryString("http://127.0.0.1/queryStringExample?a=1&b=2");
         * 
         * queryString.getValue("a"); // "1"
         * queryString.getValue("c"); // null
         */
        getValue : function(key) {
            // every parameter should be a string-string key-value pair
            if (util.isString(this.__parameters[key])) {
                return this.__parameters[key];
            }

            return null;
        },

        /**
         * Retrieves the query string, where the result is processed by the options.  
         * The result can be altered to exclude some parameters, and/or be overrided. 
         * Whenever they have conflict, exclusion dominates overriding.  
         * Note:  
         * - the sequence of the parameters may not be preserved  
         * - the query string is not url encoded, if the input was not
         * 
         * @function getQueryString
         * @public
         * @memberof ax/QueryString#
         * @param {Object} [options] An option object
         * @param {Object} [options.exclude] An array that specified a list of parameters to exclude from the result
         * @param {Object} [options.extendFrom] An object that extends the final result should extend from. Eventally this will overrides some parameters.
         * @returns {String} The constructed query string without the leading "?" sign
         * @example
         * var queryString = new QueryString("http://127.0.0.1/queryStringExample?a=1&b=2");
         * 
         * queryString.getQueryString(); // "a=1&b=2"
         * // exclude __a__
         * queryString.getQueryString({"exclude": ["a"]}); // "b=2"
         * // override __a__
         * queryString.getQueryString({"extendFrom": {"a": "lower_a"}}); // "a=lower_a&b=2"
         * // exclude __a__ & override __c__
         * queryString.getQueryString({"exclude": ["a"], "extendFrom": {"c": "lower_c"}}); // "b=2&c=lower_c"
         */
        getQueryString: function(options) {
            if (!util.isObject(options)) {
                // don't have to modifying anything, return the orignal query string
                return this.__queryString;
            }

            var params,
                queryString = "",
                key,
                exclude;

            // create a clone to prevent modication from the original copy
            params = util.clone(this.__parameters);

            // extend the resultant parameters from the _extendFrom_ option
            if (util.isObject(options.extendFrom)) {
                params = util.extend(params, options.extendFrom);
            }

            // exclude the parameters
            if (util.isArray(options.exclude)) {
                exclude = options.exclude;

                for (key in params) {
                    // if key exists in _exclude_, remove it from _params_
                    if (util.indexOf(exclude, key) !== -1) {
                        if (params.hasOwnProperty(key)) {
                            delete params[key];
                        }
                    }
                }
            }

            // make a query string from the parameters
            for (key in params) {
                queryString += key + "=" + params[key] + "&";
            }

            // remove the trailing "&"
            if (queryString.length > 0) {
                queryString = queryString.substring(0, queryString.length - 1);
            }

            return queryString;
        },

        /**
         * Extracts the parameters from this query string to a key-value paired object.  
         * 
         * @function toObject
         * @memberof ax/QueryString#
         * @returns {Object} An string-string key-value paired object
         * @example
         * var queryString = new QueryString("http://127.0.0.1/queryStringExample?a=1&b=2");
         * 
         * queryString.toObject(); // {"a": "1", "b": "2"}
         */
        toObject: function() {
            return this.__parameters;
        }
    });
});
/**
 * The logger base, can be used as a starting point of building any logger.
 *
 * @class ax/logger/loggerBase
 */
define("ax/logger/loggerBase", ["ax/config"], function (config) {
    "use strict";
    return {
        /**
         * Log level debug
         * @member LV_DEBUG
         * @memberof module:ax/logger/loggerBase
         * @public
         */
        LV_DEBUG: 0,
        /**
         * Log level info
         * @member LV_INFO
         * @memberof module:ax/logger/loggerBase
         * @public
         */
        LV_INFO: 1,
        /**
         * Log level log
         * @member LV_LOG
         * @memberof module:ax/logger/loggerBase
         * @public
         */
        LV_LOG: 2,
        /**
         * Log level warning
         * @member LV_WARNING
         * @memberof module:ax/logger/loggerBase
         * @public
         */
        LV_WARNING: 3,
        /**
         * Log level error
         * @member LV_ERROR
         * @memberof module:ax/logger/loggerBase
         * @public
         */
        LV_ERROR: 4,
        /**
         * Current log level
         * @member level
         * @memberof module:ax/logger/loggerBase
         * @public
         */
        level: config.get("console.lv", 2),
        /**
         * Send a log message
         * @method log
         * @memberof ax/logger/loggerBase
         * @public
         * @param {Mixed} string 1st log message
         * @param {Mixed} [...] nth log message
         */
        log: function () {
            if (this.level > this.LV_LOG) {
                return;
            }
            var args = Array.prototype.slice.call(arguments);
            this._log(args.join(" "));
        },
        /**
         * Send a debug message
         * @method debug
         * @public
         * @memberof ax/logger/loggerBase
         * @param {Mixed} string 1st log message
         * @param {Mixed} [...] nth log message
         */
        debug: function () {
            if (this.level > this.LV_DEBUG) {
                return;
            }
            var args = Array.prototype.slice.call(arguments);
            this._log("[DEBUG]: " + args.join(" "));
        },
        /**
         * Send a info message
         * @method info
         * @public
         * @memberof ax/logger/loggerBase
         * @param {Mixed} string 1st log message
         * @param {Mixed} [...] nth log message
         */
        info: function () {
            if (this.level > this.LV_INFO) {
                return;
            }
            var args = Array.prototype.slice.call(arguments);
            this._log("[INFO]: " + args.join(" "));
        },
        /**
         * Send a warn message
         * @method warn
         * @public
         * @memberof ax/logger/loggerBase
         * @param {Mixed} string 1st log message
         * @param {Mixed} [...] nth log message
         */
        warn: function () {
            if (this.level > this.LV_WARNING) {
                return;
            }
            var args = Array.prototype.slice.call(arguments);
            this._log("[WARN]: " + args.join(" "));
        },
        /**
         * Send a error message
         * @method error
         * @public
         * @memberof ax/logger/loggerBase
         * @param {Mixed} string 1st log message
         * @param {Mixed} [...] nth log message
         */
        error: function () {
            if (this.level > this.LV_ERROR) {
                return;
            }
            var args = Array.prototype.slice.call(arguments);
            this._log("[ERROR]: " + args.join(" "));
        },
        /**
         * Log a message
         * @method _log
         * @memberof ax/logger/loggerBase
         * @param {Mixed} string 1st log message
         * @param {Mixed} [...] nth log message
         * @protected
         * @abstract
         */
        _log: null
    };
});
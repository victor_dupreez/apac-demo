/**
 *
 * Remote logger implemetation of ILogger interface.
 * To use the remote logging, developers need to set the url of the console.php ("remotelogger.url" in config). It will automatically
 * generate an xdkLog.txt file in the same directoy.
 *
 * ### Config Params
 *
 *  Attribute | Value
 * --------- | ---------
 * Key      | remotelogger.url
 * Type     | String
 * Usage    | {@link module:ax/config}
 * Desc     | To receive and display the log message {@link module:ax/logger/remoteLogger.url}
 * Default  | ""
 * -------------------------------------------
 *   Attribute | Value
 * --------- | ---------
 * Key      | remotelogger.logInterval
 * Type     | Number
 * Usage    | {@link module:ax/config}
 * Desc     | The Interval(sec) of checking log. If there are awaiting logs, it will send out.{@link module:ax/logger/remoteLogger.logInterval}
 * Default  | 10
 * -------------------------------------------
 *    Attribute | Value
 * --------- | ---------
 * Key      | remotelogger.logFilePath
 * Type     | String
 * Usage    | {@link module:ax/config}
 * Desc     | the path to save the log file {@link module:ax/logger/remoteLogger.logFilePath}
 * Default  | xdkLog.txt
 * -------------------------------------------
 *   Attribute | Value
 * --------- | ---------
 * Key      | remotelogger.async
 * Type     | Boolean
 * Usage    | {@link module:ax/config}
 * Desc     | The async of the ajax send to the php file. Default is false {@link module:ax/logger/remoteLogger.async}
 * Default  | false
 *
 * @module ax/logger/remoteLogger
 * @extends ax/logger/loggerBase
 * @author Thomas Lee <thomas.lee@accedo.tv>
 */
define("ax/logger/remoteLogger", ["ax/config", "ax/logger/loggerBase"], function (config, loggerBase) {
    "use strict";
    var messages = [],
        remoteLogger, item, request;
    request = function (url, opts) {
        var req = new XMLHttpRequest();
        req.open("post", url, opts.async);
        req.send(opts.parameters);
    };
    remoteLogger = {
        /**
         * To receive and display the log message
         * Gets from config **remotelogger.urL**, with default value of "".
         * To get the logger php,please go to jira wiki page
         * @member url
         * @memberof module:ax/logger/remoteLogger
         * @public
         */
        url: config.get("remotelogger.url", ""),
        /**
         * The Interval of checking log. If there are awaiting logs, it will send out.
         * Gets from config **remotelogger.logInterval**, with default value of "10s"
         * @member logInterval
         * @memberof module:ax/logger/remoteLogger
         * @public
         */
        logInterval: config.get("remotelogger.logInterval", 10),
        /**
         * The async of the ajax send to the php file. Default is false
         * @member async
         * @memberof module:ax/logger/remoteLogger
         * @public
         */
        async: config.get("remotelogger.async", false),
        /**
         * the path to save the log file
         * Gets from config **remotelogger.logFilePath**, with default value of "xdkLog.txt" in the same folder with the php
         * @member logFilePath
         * @memberof module:ax/logger/remoteLogger
         * @public
         */
        logFilePath: config.get("remotelogger.logFilePath", "xdkLog.txt"),
        /**
         * To store the time out before sending the log out.
         * @member _timeInterval
         * @memberof module:ax/logger/remoteLogger
         * @private
         */
        _timeInterval: null,
        /**
         * To send the log using ajax
         * @method __sendLog
         * @memberof module:ax/logger/remoteLogger
         * @private
         */
        __sendLog: function () {
            //if no url or no msg, then did nth
            if (!remoteLogger.url || messages.length < 1) {
                return;
            }

            var data = {
                msg: messages,
                logPath: remoteLogger.logFilePath
            },
                sendMsg;
            //send the message out
            if (!JSON || !JSON.stringify) {
                sendMsg = remoteLogger.__stringify(data);
            } else {
                sendMsg = JSON.stringify(data);
            }
            request(remoteLogger.url, {
                parameters: sendMsg,
                async: remoteLogger.async
            });
            //remove the message
            messages = messages.slice(messages.length);
        },
        /**
         * To stringify the obejct as some platform may not have the JSON.stringify function and this logger is independent of the util and util json part
         * @method  __stringify
         * @param {object} obj object to stringify
         * @memberof module:ax/logger/remoteLogger
         * @private
         */
        __stringify: function (obj) {
            var results = [],
                len, value, property, i = 0;
            if (obj === null) {
                return "null";
            }

            if (obj.toJSON && typeof obj.toJSON === "function") {
                return obj.toJSON();
            }

            if (typeof obj === "boolean" || typeof obj === "number") {
                return obj.toString();
            }

            if (typeof obj === "string") {
                return "\"" + obj.replace(/"/g, "\\\"") + "\"";
            }

            if (Object.prototype.toString.call(obj) === "[object Array]") {
                len = obj.length;

                for (; i < len; i++) {
                    value = remoteLogger.__stringify(obj[i]);
                    if (typeof obj !== "undefined") {
                        results.push(value);
                    }
                }
                return "[" + results.join(",") + "]";
            }

            if (obj) {
                for (property in obj) {
                    value = remoteLogger.__stringify(obj[property]);
                    if (typeof obj !== "undefined") {
                        results.push("\"" + property.toString() + "\":" + value);
                    }
                }
                return "{" + results.join(",") + "}";
            }

            return false;
        },
        /**
         * Log a message
         * @method _log
         * @memberof module:ax/logger/remoteLogger
         * @param {String} msg log message
         * @protected
         */
        _log: function (msg) {
            //if the awaiting message size is larger than 500, just keep the latest 500 messages
            if (messages.length >= 500) {
                messages = messages.slice(1);
            }
            messages.push(msg);
            if (!this._timeInterval) {
                this._timeInterval = setInterval(this.__sendLog, this.logInterval * 1000);
            }
        }
    };

    for (item in loggerBase) {
        if (!remoteLogger[item]) {
            remoteLogger[item] = loggerBase[item];
        }
    }

    return remoteLogger;
});
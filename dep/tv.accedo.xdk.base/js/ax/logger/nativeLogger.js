/*global alert*/
/**
 *
 *  A native console module that mimics the browser's console with logging capability
 *  If its userAgent contains "maple", it will use alert as the native logger.
 *
 * @module ax/logger/nativeLogger
 * @extends ax/logger/loggerBase
 * @author Thomas Lee <thomas.lee@accedo.tv>
 */
define("ax/logger/nativeLogger", ["ax/logger/loggerBase", "ax/core"], function (loggerBase, core) {
    "use strict";
    //get the root from core which is window
    var root = core.root;

    // if is a Samsung device, use alert() which is the native logger
    if (root.navigator && root.navigator.userAgent.toLowerCase().indexOf("maple") !== -1) {
        //override the loggerBase _log function
        loggerBase._log = function () {
            var args = Array.prototype.slice.call(arguments),
                msg = args.join(", ");
            if (alert) {
                alert(msg);
            }
        };
        return loggerBase;
    }

    //no native setting and use the window.console
    if (root.console) {
        return root.console;
    }

    return {
        log: null,
        error: null,
        debug: null,
        info: null,
        warn: null
    };
});
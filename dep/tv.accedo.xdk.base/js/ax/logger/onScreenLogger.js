/**
 * On-screen logger extend from loggerBase so it shows the log messages on the screen
 *
 * ####Config Params
 * Attribute | Value
 * --------- | ---------
 * Key      | onscreenlogger.maxline
 * Type     | Number
 * Usage    | {@link module:ax/config}
 * Desc     | Maximum number of lines that can be printed onto screen. {@link module:ax/logger/onScreenLogger.maxLine}
 * Default  | 14
 * -------------------------------------------
 * Attribute | Value
 * --------- | ---------
 * Key      | onscreenlogger.bgcolor
 * Type     | String
 * Usage    | {@link module:ax/config}
 * Desc     | Color code or name for styling the log area's background color. {@link module:ax/logger/onScreenLogger.bgColor}
 * Default  | green
 * -----------------------------------------
 *  Attribute | Value
 * --------- | ---------
 * Key      | onscreenlogger.key
 * Type     | String
 * Usage    | {@link module:ax/config}
 * Desc     | The key to toggle the display of onScreen whether show or hide {@link module:ax/logger/onScreenLogger.triggerKey}
 * Default  | "KEY_1"
 * -------------------------------------------
 *  Attribute | Value
 * --------- | ---------
 * Key      |onscreenlogger.dimension
 * Type     | Object
 * Usage    | {@link module:ax/config}
 * Desc     | Dimension of the log area, in the format of {Object}, stating top, left, width and height. {@link module:ax/logger/onScreenLogger.dimension}
 * Default  |
 *     {
 *         "top": 480,
 *         "left": 0,
 *         "width": 1280,
 *         "height": 240
 *     }
 *
 * @module ax/logger/onScreenLogger
 * @extends ax/logger/loggerBase
 */
define("ax/logger/onScreenLogger", ["ax/config", "ax/logger/loggerBase", "require"], function (config, loggerBase,
    require) {
    "use strict";
    var inited = false,
        logArea, messageEle, messages = [],
        onScreenLogger, item;
    onScreenLogger = {
        /**
         * Maximum number of lines that can be printed onto screen.
         * Gets from config **onscreenlogger.maxline**, with default value of 14.
         * @member maxLine
         * @memberof module:ax/logger/onScreenLogger
         * @public
         */
        maxLine: config.get("onscreenlogger.maxline", 14),
        /**
         * Color code or name for styling the log area's background color.
         * Gets from config **onscreenlogger.bgcolor**, with default value of "green".
         * @member bgColor
         * @memberof module:ax/logger/onScreenLogger
         * @public
         */
        bgColor: config.get("onscreenlogger.bgcolor", "green"),
        /**
         * The key to toggle the display of onScreen whether show or hide
         * Gets from config **onscreenlogger.key**, with default value of "KEY_1".
         * @member triggerKey
         * @memberof module:ax/logger/onScreenLogger
         * @public
         */
        triggerKey: config.get("onscreenlogger.key", "KEY_1"),
        /**
         * Dimension of the log area, in the format of {Object}, stating top, left, width and height.
         * Gets from config **onscreenlogger.dimension**, with default value of 240px in height.
         * @member dimension
         * @memberof module:ax/logger/onScreenLogger
         * @public
         */
        dimension: config.get("onscreenlogger.dimension", {
            "top": 480,
            "left": 0,
            "width": 1280,
            "height": 240
        }),
        /**
         * Initializes and create the on-screen logger when the first log
         * @method __init
         * @memberof module:ax/logger/onScreenLogger
         * @priavate
         */
        __init: function () {
            if (inited) {
                return;
            }
            inited = true;

            logArea = document.createElement("div");

            logArea.setAttribute("id", "logArea");
            logArea.style.position = "absolute";
            logArea.style.left = this.dimension.left + "px";
            logArea.style.top = this.dimension.top + "px";
            logArea.style.width = this.dimension.width + "px";
            logArea.style.height = this.dimension.height + "px";
            logArea.style.backgroundColor = this.bgColor;
            logArea.style.zIndex = "99";
            logArea.style.opacity = "0.5";
            logArea.style.overflow = "hidden";

            messageEle = document.createElement("pre");
            messageEle.style.margin = "0 0 0 10px";
            logArea.appendChild(messageEle);
            domReady(function () {
                document.body.appendChild(logArea);
            });

            this.show();
            this.__setKey();
        },
        /**
         * To set the key which toggle the display of onscreen logger when the env is ready
         * @method __setKey
         * @memberof module:ax/logger/onScreenLogger
         * @private
         */
        __setKey: function () {
            try {
                var sEnv = require("ax/Env").singleton(),
                    vKey = require("ax/device/vKey");

                if (sEnv) {
                    sEnv.addEventListener(sEnv.EVT_ONKEY, function (key) {
                        switch (key.id) {
                        case vKey[onScreenLogger.triggerKey].id:
                            if (logArea.style.display === "none") {
                                onScreenLogger.show();
                            } else {
                                onScreenLogger.hide();
                            }
                            break;
                        }
                        return false;
                    });
                }
            } catch (ex) {
                //keep trying until the env is ready
                setTimeout(this.__setKey, 500);
            }
        },
        /**
         * Log a message
         * @method _log
         * @memberof module:ax/logger/onScreenLogger
         * @param {String} msg log message
         * @protected
         */
        _log: function (msg) {
            this.__init();
            messages.unshift(msg);
            if (messages.length > this.maxLine) {
                messages.pop();
            }
            messageEle.innerHTML = messages.join("\n");
        },
        /**
         * Hide the on-screen log
         * @method hide
         * @memberof module:ax/logger/onScreenLogger
         * @public
         */
        hide: function () {
            logArea.style.display = "none";
        },
        /**
         * Show the on-screen log
         * @method show
         * @memberof module:ax/logger/onScreenLogger
         * @public
         */
        show: function () {
            logArea.style.display = "block";
        },
        /**
         * Clear the on-screen log
         * @method clear
         * @memberof module:ax/logger/onScreenLogger
         * @public
         */
        clear: function () {
            messages.length = 0;
            messageEle.innerHTML = "";
        }
    };

    for (item in loggerBase) {
        if (!onScreenLogger[item]) {
            onScreenLogger[item] = loggerBase[item];
        }
    }

    return onScreenLogger;
});
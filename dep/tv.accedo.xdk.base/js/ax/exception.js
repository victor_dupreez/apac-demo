/**
 * The exception names referencing from JAVA Exception.
 * @see {@link http://www.tutorialspoint.com/java/java_builtin_exceptions.htm}
 * @see {@link http://rymden.nu/exceptions.html}
 * @module ax/exception
 * @example
 * core.createException(exception.UNSUPPORTED_OPERATION, "Unsupported on this device.");
 */
define("ax/exception", {
    /**
     * @typedef Exception
     * @type {String}
     * @desc Provide the exception name and will be used in {@link module:ax/core.createException}
     * @memberof module:ax/exception
     */
    /**
     * Thrown to indicate that a method has been passed an illegal or inappropriate argument.
     * @typedef ILLEGAL_ARGUMENT
     * @type {Exception}
     * @memberof module:ax/exception
     */
    ILLEGAL_ARGUMENT: "ax:exception:illegal-argument",
    /**
     * Thrown when a method has been invoked at an illegal or inappropriate time
     * @typedef ILLEGAL_STATE
     * @type {Exception}
     * @memberof module:ax/exception
     */
    ILLEGAL_STATE: "ax:exception:illegal-state",
    /**
     * Thrown when a call to a library or RPC throws error.
     * @typedef INTERNAL
     * @type {Exception}
     * @memberof module:ax/exception
     */
    INTERNAL: "ax:exception:internal",
    /**
     * Thrown when access to certain resource fails due to unsufficient privilege.
     * @typedef UNAUTHORIZED
     * @type {Exception}
     * @memberof module:ax/exception
     */
    UNAUTHORIZED: "ax:exception:unauthorized",
    /**
     * Thrown to indicate that the requested operation is not supported.
     * @typedef UNSUPPORTED_OPERATION
     * @type {Exception}
     * @memberof module:ax/exception
     */
    UNSUPPORTED_OPERATION: "ax:exception:unsupported-operation"
});
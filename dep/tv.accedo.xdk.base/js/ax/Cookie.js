/**
 * Cookie is a utility for the browser cookie object. This class is a singleton class, as the fact that the cookie can be accessed globally in the application.
 *
 * @class ax/Cookie
 * @author Marco Fan <marco.fan@accedo.tv>
 */
define([
    "ax/class",
    "ax/htmlGlobals",
    "ax/util",
    "ax/config"
], function (
    klass,
    htmlGlobals,
    util,
    config
) {
    "use strict";

    // construct a Date object with time set to 1970-01-01T00:00:00
    var date = new Date(0);
    date.setTime(0);

    /**
     * A constant for expired date in UTC string.
     *
     * @private
     * @constant {String}
     * @memberof ax/Cookie
     */
    var EXPIRED_DATE_UTC_STRING = date.toUTCString();

    var instance;

    var Cookie = klass.create({
        /**
         * Returns the singleton instance of this class.
         *
         * @method
         * @returns {ax/Cookie}
         * @memberof ax/Cookie
         */
        singleton: function () {
            if (!instance) {
                instance = new Cookie();
            }

            return instance;
        }
    }, {
        init: function () {
            this._document = htmlGlobals.document;
            this._keyPrefix = config.get("device.storage.prefix", "AccedoXdkStorage_");
            this._memory = this.__parseFromCookie();
        },

        /**
         * Writes the value to the browser cookie.
         *
         * @method
         * @param {String} key The name of the cookie
         * @param {String} value The value to save
         * @param {Object} [opts] The options for writing the cookie
         * @param {Date|String} opts.expires The expires value of the cookie. This could be a date in GMTString format or Date object. If not specified the cookie will expire at the end of session.
         * @param {String} opts.path The path from where the cookie will be readable. If not specified, defaults to the current path of the current document location.
         * @param {String} opts.domain The domain from where the cookie will be readable. If not specified, defaults to the host portion of the current document location.
         * @memberof ax/Cookie#
         */
        setItem: function (key, value, opts) {
            opts = opts || {};
            var expires = opts.expires;
            var path = opts.path;
            var domain = opts.domain;

            var valueString = this._keyPrefix + key + "=" + encodeURIComponent(value);
            var expiresString = "";

            if (expires) {
                switch (expires.constructor) {
                case String:
                    expiresString = "; expires=" + expires;
                    break;
                case Date:
                    expiresString = "; expires=" + expires.toUTCString();
                    break;
                }
            }

            this._memory[key] = value;
            this._document.cookie = valueString + expiresString + (path ? "; path=" + path : "") + (domain ? "; domain=" + domain : "");
        },

        /**
         * Returns the cookie for a specific key.
         * If the key does not exist, undefined will be returned.
         *
         * @method
         * @param {String} key The key of the cookie
         * @returns {String|undefined} The cookie value
         * @memberof ax/Cookie#
         */
        getItem: function (key) {
            return this._memory[key];
        },

        /**
         * Remove a specific cookie item by key.
         *
         * @method
         * @param {String} key The key of the cookie
         * @memberof ax/Cookie#
         */
        removeItem: function (key) {
            this.setItem(key, "", {
                expires: EXPIRED_DATE_UTC_STRING
            });
            delete this._memory[key];
        },

        /**
         * Clear all items in the cookie.
         *
         * @method
         * @returns {String} The cookie value
         * @memberof ax/Cookie#
         */
        clear: function () {
            for (var key in this._memory) {
                this.removeItem(key);
            }
        },

        /**
         * Reads and parses the data from the browser cookie.
         *
         * @method
         * @private
         * @returns {Object} The key-value data object
         * @memberof ax/Cookie#
         */
        __parseFromCookie: function () {
            var cookies = this._document.cookie.split(";");
            var prefix = this._keyPrefix;
            var prefixLength = prefix.length;
            var memory = {};

            util.each(cookies, function (item) {
                var matches = item.match(/(\w+)=(.*)/);
                if (!matches) {
                    return;
                }

                var key = matches[1];
                var value = matches[2];

                if (util.startsWith(key, prefix)) {
                    memory[key.substr(prefixLength)] = decodeURIComponent(value);
                }
            });

            return memory;
        }
    });

    return Cookie;
});
/**
 * An event dispatcher, implementing the Observer design pattern.
 * @class ax/EventDispatcher
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/EventDispatcher", [
    "ax/class",
    "ax/util",
    "ax/console",
    "ax/core"
], function (
    klass,
    util,
    console,
    core
) {

    "use strict";

    return klass.create({}, {
        /**
         * Registery for all event handlers
         * @member __eventCallbacks
         * @memberof ax/EventDispatcher#
         * @private
         */
        __eventCallbacks: {},
        /**
         * Get the event handlers by given type
         * @method __getEventHandlers
         * @param {String} type event type
         * @return {Array} event handlers of given type
         * @private
         * @memberof ax/EventDispatcher#
         */
        __getEventHandlers: function (type) {
            var handlers = this.__eventCallbacks[type];
            if (!handlers && !util.isArray(handlers)) {
                handlers = [];
            }

            return handlers;
        },
        /**
         * Dispatches an event with additional data.
         * @method dispatchEvent
         * @param {String} type event type to dispatch
         * @param {Object} data the event data object
         * @param {Boolean} handleError catch the errors on thrown in listener and handle gracefully. Default is false.
         * @return {Boolean} result dispatch success for true
         * @public
         * @memberof ax/EventDispatcher#
         */
        dispatchEvent: function (type, data, handleError) {
            if (!type) {
                throw core.createException("IncorrectParam", "Failed to dispatch event due to no event type!");
            }

            /**
             * @TODO still keep this onDispatchEvent() thing?
             */
            if (this.onDispatchEvent && !this.onDispatchEvent(type, data)) {
                console.log("Event blocked for type=" + type + " data=" + data);
                return false;
            }

            var onces = [],
                obj, i, handlers = this.__getEventHandlers(type),
                result = true,
                curResult;


            if (util.isUndefined(data)) {
                data = {};
            }

            for (i = 0; i < handlers.length; i++) {
                obj = handlers[i];

                if (util.isFunction(obj.handler)) {

                    if (handleError===true) {
                        try {
                            curResult = obj.handler.call(this, data);
                        }
                        catch (e) {
                            //errors caught should be logged properly, dispatcher shouldn'
                            console.error(e.name + ":" + e.message);
                            if (e.stack) {
                                console.error(e.stack);
                            }
                        }
                    }
                    else {
                        curResult = obj.handler.call(this, data);
                    }
                }

                //Remove handlers that is listen for once only
                if (obj.once) {
                    onces.push(obj);
                }

                if (curResult === false) {
                    result = false;
                }
            }

            for (i = 0; i < onces.length; i++) {
                this.removeEventListener(type, onces[i].handler);
            }

            return result;
        },
        /**
         * Registers an event listener for a certain type.
         * @method addEventListener
         * @param {String} type event type
         * @param {Function} handler event listener function
         * @param {Boolean} [once] set to true if listener will be called only once
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @public
         * @memberof ax/EventDispatcher#
         */
        addEventListener: function (type, handler, once) {
            if (!type || !handler) {
                throw core.createException("IncorrectParam", "Failed to add event listener due to no event type or listener!");
            }

            if (!once) {
                once = false;
            }

            var i, handlers = this.__eventCallbacks[type],
                obj;
            if (!handlers || !util.isArray(handlers)) {
                handlers = [];
                this.__eventCallbacks[type] = handlers;
            }

            obj = {
                handler: handler,
                once: once
            };

            //Do not add handler if it is already registred
            for (i = 0; i < handlers.length; i++) {
                if (handlers[i].handler === handler) {
                    return this;
                }
            }

            handlers.push(obj);

            return this;
        },
        /**
         * Removes one or more event listeners.
         * @method removeEventListener
         * @param {ax/af/evt|String} type Event type
         * @param {Function} [handler] event listener function.  If not provided, all listeners will be removed for the specific type.
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @public
         * @memberof ax/EventDispatcher#
         */
        removeEventListener: function (type, handler) {
            var handlers = this.__eventCallbacks[type],
                i;

            if (!handlers) {
                return this;
            }
            if (!handlers.length) {
                delete this.__eventCallbacks[type];
                return this;
            }

            if (!handler) { // should remove handlers regardless of handler function
                for (i in handlers) {
                    this.removeEventListener(type, handlers[i].handler);
                }
                delete this.__eventCallbacks[type];
                return this;
            }

            // find the exact handler
            for (i = 0; i < handlers.length; i++) {
                if (handlers[i].handler === handler) {
                    break;
                }
            }

            if (i < handlers.length) {
                handlers.splice(i, 1);
            }

            return this;
        },
        /**
         * Removes all event listeners.
         * @see ax/EventDispatcher#removeEventListener
         * @method removeAllListeners
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @public
         * @memberof ax/EventDispatcher#
         */
        removeAllListeners: function () {
            var type;
            for (type in this.__eventCallbacks) {
                this.removeEventListener(type);
            }
            return this;
        },
        /**
         * Deinitialize this event dispatcher
         * @method deinit
         * @protected
         * @memberof ax/EventDispatcher#
         */
        deinit: function () {
            this.removeAllListeners();
        }
    });
});
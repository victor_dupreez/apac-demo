/**
 * @module ax/device
 * @description
 *
 * Device module handles device detection and platform module loading. Every device abstraction package should provide a detection module, providing the API to check if itself is the correct package to use. The order of  package detection is determined from config **device.packages**
 *
 * ###Config Params
 *
 *  Attribute | Value
 * --------- | ---------
 * Key      | device.initTimeout
 * Type     | Number
 * Desc     |  Timeout for loading device packages in milliseconds.
 * Default  | 10000
 * Usage    | {@link module:ax/config}
 * -----------------------------------------------------
 * Attribute | Value
 * --------- | ---------
 * Key      | device.target-platform
 * Type     |  String
 * Desc     | The target platform when loading the platform. No detection will be checked and directly load. The priority of device.target-platform will be higher than the device.packages
 * Default  | null
 * Usage    | {@link module:ax/config}
 * Example  | "ax/ext/device/lg"
 * -----------------------------------------
 * Attribute | Value
 * --------- | ---------
 * Key      | device.packages
 * Type     | Array
 * Desc     | The list of device package to be added for detection
 * Default  | ["ax/device/workstation"]
 * Usage    | {@link module:ax/config}
 * Example  |  ["ax/ext/device/samsung", "ax/ext/device/lg", "ax/device/workstation"]
 * ----------------------------------------------------------------------------------
 * Attribute | Value
 * --------- | ---------
 * Key      | device.players
 * Type     | Object
 * Usage    | {@link module:ax/config}
 * Desc     | The list of players added into that platform
 * Default  | Error occur if it is empty
 * Example  |
 *{
 *    "ps3": [ "ax/ext/device/ps3/PSPlayer"],
 *    "playstation": [ "ax/ext/device/playstation/PSPlayer"],
 *    "opera": ["ax/device/shared/FlashPlayer"],
 *    "workstation": ["ax/device/shared/Html5Player", "ax/device/shared/FlashPlayer"],
 *    "samsung": ["ax/ext/device/samsung/SefPlayer", "ax/device/shared/FlashPlayer", "ax/ext/device/samsung/InfolinkPlayer"]
 *}
 *
 * ### Abstracted Device Interfaces
 *
 * Device module abstracted a set of abstracted device interfaces. They unify functionality and interface across different platforms. These APIs can be directly accessible through Device module. e.g.**device.id**
 *
 * * {@link ax/device/AbstractId|device.id}
 * * {@link ax/device/AbstractStorage|device.storage}
 * * {@link ax/device/AbstractSystem|device.system}
 * * {@link ax/device/AbstractTvKey|device.tvkey}
 * * {@link ax/device/Media|device.media}
 *
 * @author Thomas Lee <thomas.lee@accedo.tv>
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/device", [
    "ax/config",
    "ax/console",
    "ax/core",
    "ax/util",
    "ax/device/AbstractDevicePackage",
    "ax/device/vKey",
    "require",
    "ax/class",
    "ax/interface/DeviceHandler",
    "ax/exception",
    "ax/device/interface/Player",
    "ax/promise",
    "ax/device/playerRegistry",
    "ax/device/basicInterfaces"
], function (
    config,
    console,
    core,
    util,
    abstrDevicePackage,
    _vKey,
    require,
    klass,
    IDeviceHandler,
    exception,
    IPlayer,
    promise,
    playerRegistry,
    basicInterfaces
) {
    "use strict";
    /**
     * To store the device detect from the deviceHandler
     * @name onDeviceDetect
     * @memberof module:ax/device
     * @private
     */
    var onDeviceDetect,
        /**
         * To store the device loaded from the deviceHandler
         * @name onDeviceLoad
         * @memberof module:ax/device
         * @private
         */
        onDeviceLoad,
        /**
         * To store the device package
         * @name devicePackage
         * @memberof module:ax/device
         * @private
         */
        devicePackage,
        /**
         * The timeout handle for loading package
         * @name packageTimeoutHandle
         * @memberof module:ax/device
         * @private
         */
        packageTimeoutHandle,
        /**
         * whether the dom is ready
         * @name ready
         * @memberof module:ax/device
         * @private
         */
        ready = false,
        /**
         * whether the device is inited
         * @name inited
         * @memberof module:ax/device
         * @private
         */
        inited = false,
        /**
         * whether detected
         * @name  detected
         * @memberof module:ax/device
         * @private
         */
        detected = false,
        /**
         * detectedPackage
         * @name detectedPackage
         * @memberof module:ax/device
         * @private
         */
        detectedPackage = null,
        /**
         * the device handler
         * @name deviceHandler
         * @memberof module:ax/device
         * @private
         */
        deviceHandler,
        /**
         * The callback when the desired device package is loaded and fallback to the device module.
         * @method onInitedSuccess
         * @param {Object} devicePackageDef the devicePackage when finsih inited
         * @param {Boolean} isPaused is the device package from pause state and init again
         * @throws {Exception.<exception.ILLEGAL_ARGUMENT>} when the target player doesn't implement interface Player or fail to load the multi audio track strategy
         * @memberof module:ax/device
         * @private
         */
        onInitedSuccess = function (devicePackageDef, isPaused) {
            var playerList, intf, handleName;

            inited = true;

            //to ensure thing are attached to device package
            util.each(basicInterfaces, function (pair) {
                handleName = pair.value;
                intf = devicePackageDef[handleName];

                if (!intf) {
                    console.error("Basic interface not implemented in device package:name=" + handleName);
                }

                //put the device back to device level
                deviceModule[handleName] = intf;
            });

            clearTimeout(packageTimeoutHandle);

            //init key handling
            //the callback function is for backward compatability
            deviceModule.tvkey.initKeyHandling(function () {
                return false;
            });

            //to config the player list
            playerList = config.get("device.players", false);

            if (playerList && playerList[devicePackage.getId()]) {
                //grab the predefined player list
                playerList = playerList[devicePackage.getId()];
            } else {

                //apply the new player list
                if (devicePackage.getDefaultPlayer) {
                    //get the default player list
                    playerList = devicePackage.getDefaultPlayer();
                }

                //backward compatible only, apply when the playerlist is empty(which is not set in the getDefaultPlayer) and _getDefaultPlayer function exists
                if (playerList.length === 0 && devicePackage._getDefaultPlayer) {
                    playerList = devicePackage._getDefaultPlayer();
                }
            }

            if (playerList.length > 0) {
                //to load the player list file
                require(playerList, function () {

                    var playerStgyPromiseArr = [],
                        playerHandler;

                    playerHandler = function (Player, i) {
                        if (!klass.hasImpl(Player, IPlayer)) {
                            throw core.createException(exception.ILLEGAL_ARGUMENT, "Player does not implement ax/device/interface/Player: " + playerId);
                        }
                        var playerInstance, multiAudioTrackList, playerId, subtitleList, stgyArr = [],
                            playerSubtitle, playerStgyDefer;

                        playerId = playerList[i];

                        try {
                            playerInstance = new Player(devicePackage);
                        } catch (e) {
                            console.warn("[XDK] Fail to load the player : " + playerId + " : " + e.name);
                            return;
                        }

                        //register the player
                        playerRegistry.register(playerInstance, playerId);

                        //player strategy loading
                        //multiple audio track
                        multiAudioTrackList = config.get("device.multiAudioTrackStgy", null);

                        //to get the default multi audio track config
                        if (multiAudioTrackList && multiAudioTrackList[playerId]) {
                            stgyArr.push(multiAudioTrackList[playerId]);
                            console.info("[XDK] Multi Audio Track exist " + multiAudioTrackList + " for " + playerId);
                        }

                        //subtitle
                        subtitleList = config.get("device.subtitleStgy", null);

                        //to get the subtitle config
                        if (subtitleList && subtitleList[playerId]) {
                            
                            playerSubtitle = subtitleList[playerId];

                            //load the internal player subtitle stgy
                            if (playerSubtitle.internal) {
                                console.info("[XDK] Loading the internal player strategy " + playerSubtitle.internal);
                                stgyArr.push(playerSubtitle.internal);
                            }

                            if (playerSubtitle.external) {
                                console.info("[XDK] Loading the external player strategy " + playerSubtitle.external);
                                stgyArr.push(playerSubtitle.external);
                            }

                        }

                        //no need to load external strategy
                        if (stgyArr.length === 0) {
                            return;
                        }

                        playerStgyDefer = promise.defer();

                        playerStgyPromiseArr.push(playerStgyDefer.promise);

                        //to load the strategies for that player
                        require(stgyArr, function () {
                            console.info("[XDK] Loading the player strategies");
                            playerStgyDefer.resolve();
                        }, function () {
                            throw core.createException(exception.ILLEGAL_ARGUMENT, "Unable to load the player related strategies. Please check the paths.");
                        });
                    };

                    util.each(Array.prototype.slice.call(arguments), playerHandler);

                    //continue devie initialization in a new thread
                    promise.all(playerStgyPromiseArr).then(util.bind(onDeviceLoad, this, isPaused)).done();
                }, function () {
                    throw core.createException(exception.UNSUPPORTED_OPERATION, "Unable to load the player list. Please check the paths.");
                });

                return;
            }

            onDeviceLoad(isPaused);
        },
        /**
         * To handle the device package loading failure
         * @method onInitFailure
         * @throws {Exception.<exception.UNSUPPORTED_OPERATION>} when fail to initialize the device package
         * @memberof module:ax/device
         * @private
         */
        onInitFailure = function () {
            clearTimeout(packageTimeoutHandle);
            console.error("Initialize device package timeout!");
            throw core.createException(exception.UNSUPPORTED_OPERATION, "Initialize device package timeout!");
        },
        /**
         * After loaded the desired platform js file, it will then init the platofrm.
         * @method onLoadSuccess
         * @param {Object} devicePackageDef the device packages
         * @memberof module:ax/device
         * @private
         */
        onLoadSuccess = function (DevicePackageDef) {
            var deferOnDevicePackageInited,
                /**
                 * time out for loading the device
                 * @name DEVICE_INIT_TIMEOUT
                 * @memberof module:ax/device
                 * @private
                 */
                DEVICE_INIT_TIMEOUT = config.get("device.initTimeout", 10 * 1000);

            packageTimeoutHandle = setTimeout(onInitFailure, DEVICE_INIT_TIMEOUT);

            if (!DevicePackageDef) {
                onLoadFailure("Device package reference not found.");
                return;
            }

            // backward compatible setting only
            if (DevicePackageDef.prepare) {
                DevicePackageDef.prepare(onInitedSuccess);
            }

            deferOnDevicePackageInited = util.wrap(onInitedSuccess, function (origin, arg) {
                //to create another thread so that the devicePackage is created before the package is inited and access its public function
                util.delay().then(util.bind(origin, deviceModule, arg));
            });

            //@deprecated to pass the onInitedSuccess in the constructor, the new device package should use setup function.
            devicePackage = new DevicePackageDef(deferOnDevicePackageInited);

            devicePackage.setup(function () {
                //add the platform id into the device
                deviceModule.platform = devicePackage.getId();

                onInitedSuccess.apply(null, arguments);
            });
        },

        /**
         * To handle the device package failure
         * @method onLoadFailure
         * @throws {Exception.<exception.UNSUPPORTED_OPERATION>} when device package fails to load
         * @memberof module:ax/device
         * @private
         */
        onLoadFailure = function (err) {
            console.error("Device package is unable to load due to: " + err.message);
            throw core.createException(exception.UNSUPPORTED_OPERATION, "Device package is unable to load due to: " + err.message);
        },

        /**
         * To load the package
         * @method loadPackage
         * @memberof module:ax/device
         * @private
         */
        loadPackage = function () {

            if (detected) {
                return;
            }
            //default is workstation
            if (!detectedPackage) {
                detectedPackage = "ax/device/workstation";
            }

            detected = true;

            onDeviceDetect(detectedPackage);

            require([detectedPackage + "/DevicePackage"], onLoadSuccess, onLoadFailure);

        },
        /**
         * loading the device package
         * @method loadDevicePackage
         * @throws {Exception.<exception.ILLEGAL_ARGUMENT>} when no available target device package(s)
         * @throws {Exception.<exception.UNSUPPORTED_OPERATION>} when fail to load the list of target device packages
         * @memberof module:ax/device
         * @private
         */
        loadDevicePackage = function () {

            /**
             * the target device platform
             * @name targetPlatform
             * @memberof module:ax/device
             * @private
             */
            var targetPlatform = config.get("device.target-platform", null),
                /**
                 * target device package
                 * @name targetPackages
                 * @memberof module:ax/device
                 * @private
                 */
                targetPackages = config.get("device.packages", ["ax/device/workstation"]),
                /**
                 * the detection list
                 * @name detectionList
                 * @memberof module:ax/device
                 * @private
                 */
                detectionList = [];

            if (inited) {
                if (util.isFunction(onDeviceLoad)) {
                    onDeviceLoad();
                }
                return;
            }

            //if there is targetPlatform, load it without any detection checking
            if (targetPlatform) {

                detectedPackage = targetPlatform;

                //continue load package in a new thread
                util.defer().then(loadPackage).done();

                return;
            }

            //check target packages format
            if (!util.isArray(targetPackages) || targetPackages.length === 0) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "device.packages should be an array:" +
                    targetPackages + " and should not be empty");
            }

            util.each(targetPackages, function (obj) {
                detectionList.push(obj + "/detection");
            });

            require(detectionList, function () {
                util.each(Array.prototype.slice.call(arguments), util.bind(function (obj, i) {
                    if (obj) {
                        detectedPackage = targetPackages[i];
                        return util.breaker;
                    }
                }));

                //continue load package in a new thread
                util.defer().then(loadPackage).done();
            }, function (e) {
                throw core.createException(exception.UNSUPPORTED_OPERATION, "Unable to load detection from the provided packages list: " + targetPackages.toString(), ". Please check your detection file. Stack: " + e.stack);
            });
        },

        /**
         * Default window onload handler.
         * @method onLoad
         * @memberof module:ax/device
         * @private
         */
        onLoad = function () {
            if (ready) {
                return;
            }

            ready = true;

            console.info("[XDK] DOM loaded");
            deviceModule.bootstrap();
        },
        /**
         * the device module
         * @name deviceModule
         * @memberof module:ax/device
         * @private
         */
        deviceModule = {
            //Reserved properties for linking to the device
            id: null,
            system: null,
            media: null,
            tvkey: null,
            storage: null,
            vKey: _vKey,
            /**
             * init the the device module
             * @method init
             * @param {ax/interface/DeviceHandler} targetDeviceHandler the device handler
             * @memberof module:ax/device
             * @throws {Exception.<exception.ILLEGAL_ARGUMENT>} when no valid device handler
             * @public
             */
            init: function (targetDeviceHandler) {
                //set the handler

                //device handler
                if (!klass.hasImpl(targetDeviceHandler.constructor, IDeviceHandler)) {
                    throw core.createException(exception.ILLEGAL_ARGUMENT, "[XDK] Not a valid device handler.");
                }

                deviceHandler = targetDeviceHandler;

                domReady(onLoad);
            },
            /**
             * The bootstrap of the device package. First it will undergo the platform detection and then load the desired platform files.
             * @method _bootstrap
             * @deprecated replaced with {@link module:ax/device.bootstrap}
             * @param {Object} callbackObj Object of function to call when it is initied or detected
             * @param {Function} callbackObj.platformDetectedCb the function to be called when the platform detected.
             * @param {Function} callbackObj.inited the function to be called when the device package is ready
             * @param {Function} failureCb Failure function to be called when fail in loading interfaces
             * @memberof module:ax/device
             * @protected
             */
            _bootstrap: function (callbackObj, failureCb) {

                //to inform back that platform detected, after that device package of that platform will be loaded
                onDeviceDetect = callbackObj.platformDetecetedCb || function (platform) {
                    console.log("platform detected" + platform);
                };

                onDeviceLoad = callbackObj.inited;

                if (util.isFunction(failureCb)) {
                    onLoadFailure = failureCb;
                }

                loadDevicePackage();
            },
            /**
             * The bootstrap of the device which loads the device package.
             * @method bootstrap
             * @memberof module:ax/device
             * @public
             */
            bootstrap: function () {
                var DeviceHandler = this.getDeviceHandler();

                //to inform back that platform detected, after that device package of that platform will be loaded
                onDeviceDetect = util.bind(DeviceHandler.onDeviceDetect, DeviceHandler);

                onDeviceLoad = util.bind(DeviceHandler.onDeviceLoad, DeviceHandler);

                loadDevicePackage();

            },
            /**
             * get the device handler
             * @method getDeviceHandler
             * @returns {ax/interface/DeviceHandler} the device handler
             * @memberof module:ax/device
             * @public
             */
            getDeviceHandler: function () {
                return deviceHandler;
            },
            /**
             * Override to be protected as it should only be called by {@link ax/Env|Env}
             * @method deinit
             * @param {Function} callback Callback function to be called when deinit
             * @memberof  module:ax/device
             * @protected
             */
            deinit: function (callback) {
                //@deprecated
                if (util.isFunction(callback)) {
                    callback();
                }

                //to notify the env is unloaded
                this.getDeviceHandler().onDeviceUnload();
            }
        };

    return deviceModule;
});
/*jshint unused:false*/
/**
 * To output the console, the main purpose of this module is to distinguish which logger to use.
 * Default will use the native logger.e.g in samsung, it will use alert. workstation will use the native console.log
 * Developers can set the config __console.logger__ to be the path of the logger.
 *
 * Available logger
 *
 * 1.  {@link module:ax/logger/onScreenLogger} - to show the log on the screen
 * 2.  {@link module:ax/logger/remoteLogger} - to remote the log message through the php
 *
 * ###Configuration Parameters
 *
 *  Attribute | Value
 * --------- | ---------
 * Key:    | console.logger
 * Desc    | Set the console logger to be used. Choices are {@link module:ax/logger/onScreenLogger} and {@link module:ax/logger/remoteLogger}
 * Type    |String
 * Default  | "ax/logger/nativeLogger"
 * Usage | {@link module:ax/config}
 * --------------------------------------
 *  Attribute | Value
 * --------- | ---------
 * Key     |console.lv
 * Desc    |the level of the show message. It will skip the message that smaller than this level. Default will be 2. So that info and debug message will be excluded. If it is 0, all the messages will be displayed.
 * Type    |Number
 * Default | 2
 * Usage   |{@link module:ax/config}
 *--------------------------------
 * There are 5 levels of log
 *
 * 0:  LV_DEBUG
 * 1:  LV_INFO
 * 2:  LV_LOG
 * 3:  LV_WARNING
 * 4:  LV_ERROR
 *
 * @module ax/console
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/console", ["ax/config", "ax/logger/nativeLogger", "ax/logger/onScreenLogger", "ax/logger/remoteLogger",
    "require"
], function (config, NativeLogger, OnScreenLogger, RemoteLogger) {
    "use strict";
    var console, setLogger;
    /**
     * Sets the logger to use.
     * @method setLogger
     * @memberof module:ax/console
     * @param {ax/logger/ILogger} logger the logger to use
     * @private
     */
    setLogger = function (logger) {
        if (logger) {
            try {
                logger = require(logger);
                console = logger;
                return;
            } catch (ex) {
                //unable to load the console and automatically fallback to console
                console = NativeLogger;
            }
        }
        console = NativeLogger;
    };

    setLogger(config.get("console.logger", null));

    return console;
});
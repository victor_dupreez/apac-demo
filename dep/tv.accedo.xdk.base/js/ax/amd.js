/*globals console:true*/
(function (global) {

    "use strict";

    var loadedModules = {
            // some dependencies are always ready
            "require": "complete",
            "exports": "complete",
            "module": "complete"
        },
        dynamicModules = {},
        definedModules = [],
        execedModules = [],
        modules = {},
        localRequires = {},
        globalRequire,
        exportsRegistry = {},
        LOCAL_REQUIRE = "require",
        EXPORTS = "exports",
        MODULE = "module",
        anonyModuleCounter = 0,
        requireCounter = 0,
        ANONY_MODULE_ID_PREFIX = "__anony-module-",
        REQUIRE_ID_PREFIX = "__require-",
        loadingObjList = {},
        head = document.getElementsByTagName("head")[0],
        checkProgressInterval = 100,
        jsSuffixRegExp = /\.js$/,
        externalRegExp = /^(\/\/)|([^:]+:\/\/)/,
        docRootRegExp = /^\/[^\/]+/,
        readyRegExp = navigator.platform === "PLAYSTATION 3" ? /^complete$/ : /^(complete|loaded)$/,
        // utils stuff
        op = Object.prototype,
        hasOwn = op.hasOwnProperty,
        ostring = op.toString,
        ap = Array.prototype,
        binder = {},
        // configs
        _config = {},
        baseUrl = "./",
        paths = {},
        pathsKeys = [],
        waitInterval = 10000;

    /**
     * Method to pass configuration to AMD module loader.
     * Any existing path will be overridden by new setting.
     * @method
     * @memberof module:amd
     * @param {Object} opts the config object
     * @param {String} [opts.baseUrl] The default base url to load scripts. All scripts will be loaded relative
     * @param {Number} [opts.waitSeconds] The number of seconds to wait before giving up on loading a script. The default is 10.
     * @param {Object} [opts.paths] Path mappings for module names not found directly under baseUrl.
     */
    function config(opts) {
        var key;

        opts = opts || {};
        extendObj(_config, opts, true);

        baseUrl = opts.baseUrl || baseUrl;
        if (!endsWith(baseUrl, "/")) {
            baseUrl += "/";
        }

        // paths = opts.paths || paths;
        if (opts.paths) {
            extendObj(paths, opts.paths, true);

            pathsKeys = [];
            for (key in paths) {
                pathsKeys.push(key);
            }
            pathsKeys.sort().reverse();
        }

        waitInterval = (opts.waitSeconds || 10) * 1000;
    }

    function startsWith(string, prefix) {
        return string.indexOf(prefix) === 0;
    }

    function endsWith(string, suffix) {
        return string.indexOf(suffix, string.length - suffix.length) !== -1;
    }

    /**
     * Check if an object/array contains the specific item.
     * @method
     * @private
     * @param {Object|Array} collection the collection to search
     * @param {Mixed} item the item to search for
     * @returns {Boolean} true if the collection contains the item, false otherwise
     * @memberof module:amd
     */

    function contains(collection, item) {
        if (isArray(collection)) {
            return collection.indexOf(item) !== -1;
        }
        return hasOwn.call(collection, item);
    }

    function isFunction(it) {
        return ostring.call(it) === "[object Function]";
    }

    function isArray(it) {
        return ostring.call(it) === "[object Array]";
    }

    function isObject(it) {
        return ostring.call(it) === "[object Object]";
    }

    function isString(it) {
        return typeof it === "string";
    }

    /**
     * Bind a function (func) to a context.
     * If used directly on the function and extending function is allowed, first argument can be omited.
     * @method
     * @private
     * @param {Function} func to extend (if function extending allowed, this is to be omited)
     * @param {Object} context The context to bind to
     * @param {Object} third or extra arguments will be passed as an input parameter to the Function func binded.
     * @return {Function} Wraped function where execution is guaranteed in the given context
     * @memberof module:amd~binder
     */
    binder.bind = function (func, context) {
        var __method = func,
            args = Array.prototype.slice.call(arguments, 2);
        return function () {
            var a = args.concat(arguments);
            return __method.apply(context, a);
        };
    };

    /**
     * Extends the dest object with the properties in src object.
     * For each property in src, it will be copied to dest if dest has no such property.
     * If dest or src is not an object, then do nothing.
     * @method
     * @private
     * @param {Object} dest the object to extend
     * @param {Object} src the object to extend from
     * @param {Boolean} [force] enforce the copy operation even if the dest has the corresponding property
     * @memberof module:amd
     */
    function extendObj(dest, src, force) {
        if (!isObject(dest) || !isObject(src)) {
            return;
        }

        var prop;
        for (prop in src) {
            if (isObject(src[prop])) {
                dest[prop] = extendObj(dest[prop] || {}, src[prop], force);
            } else if (force || !contains(dest, prop)) {
                dest[prop] = src[prop];
            }
        }

        return dest;
    }

    /**
     * Resolve the module id with the paths given in the configuration by substitution.
     * If the module id matches the paths' value, the match will be replaced by the corresponding path key.
     * @method
     * @private
     * @param {String} moduleId the module id
     * @returns {String} the resolved module id
     * @memberof module:amd
     */

    function lookupModuleId(moduleId) {
        var subKey, subPath = "",
            pathKey;
        for (pathKey in paths) {
            if (startsWith(moduleId, paths[pathKey]) && paths[pathKey].length > subPath.length) {
                subKey = pathKey;
                subPath = paths[pathKey];
            }
        }

        if (subPath !== "") {
            moduleId = moduleId.replace(subPath, subKey);
        }

        return moduleId;
    }

    /**
     * Trims the . and .. from an array of path segments.
     * It will keep a leading path segment if a .. will become
     * the first path segment, to help with module name lookups,
     * which act like paths, but can be remapped. But the end result,
     * all paths that use this function should look normalized.
     * NOTE: this method MODIFIES the input array.
     * @method
     * @private
     * @param {Array} array the array of path segments
     * @memberof module:amd
     * @example
     *     trimDots([".", "this", "..", "is", ".", "a", "path"]); // becomes ["is", "a", "path"]
     *     trimDots(["..", "this", "..", "is", ".", "another", "path"]); // becomes ["..", "is", "another", "path"]
     */

    function trimDots(array) {
        var i, part, length = array.length;
        for (i = 0; i < length; i++) {
            part = array[i];
            if (part === ".") {
                array.splice(i, 1);
                i -= 1;
            } else if (part === "..") {
                if (i === 1 && (array[2] === ".." || array[0] === "..")) {
                    // end of the line.
                    // Keep at least one non-dot path segment at the front so it can be mapped correctly to disk. 
                    // Otherwise, there is likely no path mapping for a path starting with '..'.
                    // This can still fail, but catches the most reasonable uses of ..
                    break;
                } else if (i > 0) {
                    array.splice(i - 1, 2);
                    i -= 2;
                }
            }
        }
    }

    /**
     * @ignore
     */

    function getLoadObjsByDependency(moduleId) {
        var deps, i, loadObjId, loadObjs = [];
        for (loadObjId in loadingObjList) {
            if (loadingObjList.hasOwnProperty(loadObjId)) {
                // anonymous module should only be referenced by the function created it
                if (isAnonymous(loadObjId)) {
                    continue;
                }

                deps = loadingObjList[loadObjId].dependencies;
                i = deps.length;

                while (i--) {
                    if (deps[i] === moduleId) {
                        loadObjs.push(loadingObjList[loadObjId]);
                        break;
                    }
                }
            }
        }

        return loadObjs;
    }

    /**
     * Get the anonymous id for a module loading.
     *
     * @method
     * @private
     * @returns {String} the id
     * @memberof module:amd
     */
    function getAnonyId() {
        // this is a dummy id to reference while the dependencies are loaded
        return ANONY_MODULE_ID_PREFIX + anonyModuleCounter++;
    }

    /**
     * Get an id for a require().
     *
     * @method
     * @private
     * @returns {String} the id
     * @memberof module:amd
     */
    function getRequireId() {
        // this is a dummy id to reference while the dependencies are loaded
        return REQUIRE_ID_PREFIX + requireCounter++;
    }

    /**
     * Checks if a module is anonymous using its id.
     *
     * @method
     * @private
     * @param {String} moduleId The id of the module
     * @returns {Boolean} True if the module is anonymous, false otherwise
     * @memberof module:amd
     */
    function isAnonymous(moduleId) {
        return moduleId.indexOf(ANONY_MODULE_ID_PREFIX) === 0;
    }

    /**
     * Check if the moduleId is specified as CommonJS component.
     * NOTE: This function will modify the _depModules_
     *
     * @method
     * @private
     * @param {String} moduleId the module id
     * @memberof module:amd
     */
    function isCommonJSComponents(moduleId) {
        return moduleId === LOCAL_REQUIRE || moduleId === EXPORTS || moduleId === MODULE;
    }

    /**
     * Import the objects specified by CommonJS spec to the dependent (if applicable).
     * NOTE: This function will modify _loadingObj.depModules_
     *
     * @method
     * @private
     * @param {String} moduleId the module id
     * @param {String} dep the id of the dependency
     * @param {Object} loadingObj The loading object of a module
     * @returns {Boolean} True if component is added, false otherwise
     * @memberof module:amd
     */
    function importCommonJSComponents(moduleId, dep, loadingObj) {
        var depModule;

        if (dep === LOCAL_REQUIRE) {
            // if the loading module is anonymous (not from define()), provide the global require
            if (isAnonymous(moduleId)) {
                depModule = globalRequire;
            } else {
                depModule = localRequires[moduleId];
            }
        } else if (dep === EXPORTS) {
            depModule = getExports(moduleId);
        } else if (dep === MODULE) {
            depModule = {
                id: moduleId
            };
        } else {
            return false;
        }

        loadingObj.depModules[loadingObj.dependencies.indexOf(dep)] = depModule;
        return true;
    }

    /**
     * Checks if the loading of a module has timed out.
     *
     * @method
     * @private
     * @param {Object} loadingObj The loading object
     * @returns {Boolean} True if timed out, false otherwise
     * @memberof module:amd
     */
    function hasLoadingTimedOut(loadingObj) {
        return new Date().getTime() > loadingObj.timeLimit;
    }

    /**
     * Checks the scrip download progress.
     * This method invokes the factory of the module loading when the module is loaded.
     * Similarly, the onFailure will be invoked (or an error will be thrown) when the module loading time out.
     *
     * This method will periodcally repeat itself until the module is either loaded or the loading time out.
     *
     * @method
     * @private
     * @param {String} moduleId The id of the module to check
     * @memberof module:amd
     */
    function checkLoadingProgress(moduleId) {
        var loadingObj = loadingObjList[moduleId],
            deps, i, dep;

        if (!loadingObjList[moduleId]) {
            // module is not loading
            return;
        }

        deps = loadingObj.dependencies;

        if (contains(modules, moduleId)) {
            // module is already loaded
            return;
        }

        if (hasLoadingTimedOut(loadingObj)) {
            if (isFunction(loadingObj.onFailure)) {
                loadingObj.onFailure(new Error("Required dependencies cannot be loaded for module " + moduleId +
                    " wait timeout reached!"));
                return;
            } else {
                var timedOutModules = [];
                i = deps.length;
                while (i--) {
                    dep = deps[i];

                    if (!jsSuffixRegExp.test(dep) && loadedModules[dep] === "complete" && !contains(modules, dep)) {
                        timedOutModules.push(dep);
                    } else if (jsSuffixRegExp.test(dep) && loadedModules[dep] !== "complete") {
                        timedOutModules.push(dep);
                    }
                }
                throw new Error("Required dependencies cannot be loaded for module " + moduleId +
                    " wait timeout reached! Possibliy one of the following: " + timedOutModules.join(","));
            }
        }

        var checkAgainLater = function () {
            setTimeout(function () {
                checkLoadingProgress(moduleId);
            }, checkProgressInterval);
        };

        i = deps.length;
        while (i--) {
            dep = deps[i];

            //if it is error, it will notify when onScriptError and no need to check Progress. So stop and do nothing
            if (loadedModules[dep] === "error") {
                delete loadingObjList[moduleId];
                return;
            }

            if (jsSuffixRegExp.test(dep)) { // JS files
                if (loadedModules[dep] !== "complete") { // not ready
                    checkAgainLater();
                    return;
                }
                continue; // it is ready
            }

            // dependency module not ready
            if (!contains(modules, dep)) {
                // import the CommonJS components (require, exports, module) if any is required
                if (importCommonJSComponents(moduleId, dep, loadingObj)) {
                    continue;
                }

                checkAgainLater();
                return;
            }

            // module was not loaded at last check
            if (loadingObj.depModules[i] === undefined) {
                // loaded plugin resource will be injected by loadResourceUsingPlugin()
                if (dep.indexOf("!") >= 0) {
                    // reaching here means the resource is not loaded yet,  cannot proceed
                    checkAgainLater();
                    return;
                }

                // inject non-plugin resource
                loadingObj.depModules[i] = modules[dep];
            }
        }

        loadingObj.onSuccess(loadingObj.depModules);
        notifyReady(moduleId);
    }

    /**
     * Method to handle when script is loaded
     * @method completeLoad
     * @memberof module:amd
     * @param {String} moduleId
     */
    function completeLoad(moduleId) {
        loadedModules[moduleId] = "complete";
        notifyReady(moduleId);
    }

    /**
     * Notifies that the module is ready, so that they can resume processing.
     * Ready here means all of its dependencies have been loaded, and the factory can be/ has been executed.
     *
     * @method
     * @private
     * @param {String} moduleId The id of the module
     * @memberof module:amd
     */
    function notifyReady(moduleId) {
        if (jsSuffixRegExp.test(moduleId)) {
            // this is just a js file, so nothing to do
            return;
        }

        var loadObjs = getLoadObjsByDependency(moduleId),
            j = loadObjs.length,
            loadObj, deps, dep, i, fullyLoaded;

        // get the absolute module id by looking up the paths config
        moduleId = lookupModuleId(moduleId);

        while (j--) {
            loadObj = loadObjs[j];

            // some modules may be waiting to start in different "thread"
            // don't process those modules to avoid duplicate execution
            if (!loadObj.started) {
                continue;
            }

            deps = loadObj.dependencies;
            i = deps.length;
            fullyLoaded = true;

            // check if all dependencies of the loadObj has loaded
            while (i--) {
                dep = deps[i];

                // ignore *.js file
                if (jsSuffixRegExp.test(dep)) {
                    continue;
                }

                if (loadedModules[dep] !== "complete") {
                    fullyLoaded = false;
                    break;
                }
            }

            if (fullyLoaded) {
                // force an update on the parent
                checkLoadingProgress(loadObj.moduleId);
            }
        }
    }

    function onScriptError(path, msg, url, line) {
        var errMsg, moduleId, loadObjs, loadObj, len;

        errMsg = "Error loading module or script \'" + path + "\": " + msg + ", at line:" + line + " URL:" + url;
        if (typeof (console) !== "undefined") {
            console.error(errMsg);
        }

        // get the absolute module id by looking up the paths config
        moduleId = lookupModuleId(path);

        loadedModules[moduleId] = "error";

        loadObjs = getLoadObjsByDependency(moduleId);
        len = loadObjs.length;

        while (len--) {
            loadObj = loadObjs[len];

            //to notify the onFailure directly
            if (isFunction(loadObj.onFailure)) {
                loadObj.onFailure(new Error("Required dependencies cannot be loaded for module " + moduleId + " wait timeout reached!"));
            }
        }
    }

    /**
     * Method to load the module
     * @method
     * @memberof module:amd
     * @param {String} moduleId the module id
     */
    function load(moduleId) {
        /**
         * @ignore
         */
        var node = document.createElement("script"),
            actualPath;
        node.type = "text/javascript";
        node.charset = "utf-8";
        node.async = true;

        node.onreadystatechange = node.onload = function () {
            if (!node.readyState || readyRegExp.test(node.readyState)) {
                node.onload = node.onreadystatechange = null;
                node.onerror = null;

                // if definedModules does not contain the loaded script, then it is either an ordinary script or anonymous module
                if (!contains(definedModules, moduleId)) {
                    var processed = false;
                    var lastIndex = definedModules.length - 1;

                    if (lastIndex >= 0 && isAnonymous(definedModules[lastIndex])) {
                        var lastDefinedModuleId = definedModules[lastIndex];
                        var loadingObj = loadingObjList[lastDefinedModuleId];

                        // anonymous
                        if (loadingObj) {
                            // create a reference for the real module
                            // the anonymous loading object isn't deleted as it is still referenced by other function
                            loadingObjList[moduleId] = loadingObj;
                            // replace the anonymous id with the real module id
                            loadingObj.moduleId = moduleId;
                            definedModules.pop();
                            definedModules.push(moduleId);
                            // update the base of the module
                            loadingObj.base = moduleId;

                            processed = true;
                        }
                    }

                    // ordinary script
                    if (!processed) {
                        modules[moduleId] = {};
                    }
                }

                completeLoad(moduleId);
            }
        };

        node.onerror = function (msg, url, line) {
            node.onload = node.onreadystatechange = null;
            node.onerror = null;

            onScriptError(moduleId, msg, url, line);
        };

        actualPath = resolvePath(moduleId);
        node.src = actualPath;
        head.appendChild(node);
    }

    /**
     * Method to pass from the path into actual path
     * @method
     * @memberof module:amd
     * @param {String} path the module path
     * @param {String} return the resolved actual path of the js
     */

    function resolvePath(path) {
        var actualPath;
        if (jsSuffixRegExp.test(path)) { // simply load the js file as it is
            return path;
        }

        actualPath = resolveModulePath(path);

        return actualPath + ".js";
    }

    /**
     * Resolve the given module id to the actual path which relative to the base url.
     * Any external resource will be returned as is.
     * @method
     * @private
     * @param {String} moduleId a normalized module id
     * @returns {String} the actual path (relative to the base url) of the module refered by the given module id
     * @memberof module:amd
     */

    function resolveModulePath(moduleId) {
        // test if the module id represents an external resource
        if (externalRegExp.test(moduleId)) {
            // return the extenal module id as is
            return moduleId;
        }

        var actualPath = moduleId,
            i, key;

        for (i in pathsKeys) {
            key = pathsKeys[i];
            if (!startsWith(actualPath, key)) {
                continue;
            }

            actualPath = actualPath.replace(key, paths[key]);
            break;
        }

        // prepend baseUrl only if the actual path does not start from the document root
        if (!docRootRegExp.test(actualPath) && !externalRegExp.test(actualPath)) {
            actualPath = baseUrl + actualPath;
        }

        return actualPath;
    }

    /**
     * Parse the resource id to report any plugin requirement.
     * @method
     * @private
     * @param {String} resourceId The resource id
     * @returns {Object} the parsed object containing
     *  * {String} plugin The required plugin's id, or '' if none is required
     *  * {String} moduleId The actual required module's id (remain in relative if it was)
     * @memberof module:amd
     */

    function parsePlugin(resourceId) {
        var idIndex, pluginId = "",
            moduleId = resourceId;

        idIndex = resourceId.indexOf("!");

        if (idIndex > -1) {
            pluginId = resourceId.substring(0, idIndex);
            moduleId = resourceId.substring(idIndex + 1, resourceId.length);
        }

        return {
            plugin: pluginId,
            moduleId: moduleId
        };
    }

    /**
     * Check if module should is dynamic. i.e. reload every time
     * @method
     * @private
     * @param {String} pluginId The id of the required plugin
     * @param {String} resourceId The id of the required module
     * @param {Object} require The local require object for path normalization
     * @memberof module:amd
     */

    function isModuleDynamic(moduleId) {
        return dynamicModules[moduleId];
    }

    /**
     * Load a resource using a specific plugin.
     * @method
     * @private
     * @param {String} pluginId The id of the required plugin
     * @param {String} resourceId The id of the required module
     * @param {Object} require The local require object for path normalization
     * @param {Object} loadingObj The loading object of a module
     * @memberof module:amd
     */
    function loadResourceUsingPlugin(pluginId, resourceId, require, loadingObj) {
        var fullModuleId = pluginId + "!" + resourceId,
            plugin = getExecModule(pluginId);

        // load the module if it has not loaded yet
        if (plugin.dynamic || !loadedModules[resourceId]) {
            // mark the [pluginId]![resourceId] as loading
            loadedModules[fullModuleId] = "loading";

            if (plugin.dynamic) {
                dynamicModules[fullModuleId] = plugin.dynamic;
            }

            // load the module once the plugin is ready
            plugin.load(resourceId, require, function (resource) {
                modules[fullModuleId] = resource;
                loadingObj.depModules[loadingObj.dependencies.indexOf(fullModuleId)] = resource;
                completeLoad(fullModuleId);
            }, extendObj({
                onFailure: function () {
                    //the plugin file is fail to load and then simulate the onscript error once
                    if (loadedModules[fullModuleId] !== "error") {
                        onScriptError(fullModuleId, "Fail to load the resource " + fullModuleId);
                    }
                }
            }, _config));
        }
    }

    /**
     * Normalize a relative module id to a real module id that can be mapped to a path.
     * @method
     * @param {String} id the relative module id
     * @param {String} baseId a real module id that the id arg is relative to
     * @returns {String} normalized module id
     * @memberof module:amd
     */
    function normalize(id, baseId) {
        var baseParts = baseId && baseId.split("/"),
            normalizedBaseParts,
            idComps,
            lastIndex;

        // normalize only if the given id is a relative id
        if (id && id.charAt(0) === ".") {
            if (baseId) {
                // Convert baseId to array, and lop off the last part,
                // so that . matches that 'directory' and not id of the baseId's module. 
                // For instance, baseId of 'one/two/three', maps to 'one/two/three.js', 
                // but we want the directory, 'one/two' for this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                idComps = id.split("/");
                lastIndex = idComps.length - 1;

                // if wanting node ID compatibility, strip .js from end of IDs.
                if (config.nodeIdCompat && jsSuffixRegExp.test(idComps[lastIndex])) {
                    idComps[lastIndex] = idComps[lastIndex].replace(jsSuffixRegExp, "");
                }

                idComps = normalizedBaseParts.concat(idComps);
                trimDots(idComps);
                id = idComps.join("/");
            } else if (id.indexOf("./") === 0) {
                // no baseId, so the id is resolved relative to baseUrl
                // remove the leading dot
                id = id.substring(2);
            }
        }

        return id;
    }

    /**
     * Actual function to load modules, using the loading object created by other function.
     * This function executes asynchronously.
     * @method
     * @private
     * @param {Object} loadObj the loading object which stores the information about the loading request
     * @param {String} loadObj.moduleId the id of the module which invoke this loading operation
     * @param {String[]} loadObj.dependencies the modules to be loaded
     * @param {Function} loadObj.onSuccess success callback to be called when all dependencies are loaded
     * @param {Function} loadObj.onFailure failure callback to be called if some dependencies cannot be loaded
     */

    function loadModules(loadObj) {
        var moduleId = loadObj.moduleId,
            deps = loadObj.dependencies,
            dep, i, len, defined, hasWork, prop;

        loadObj.timeLimit = new Date().getTime() + waitInterval;

        len = deps.length;
        for (i = 0; i < len; i++) {
            dep = deps[i];
            defined = false;

            if (dep === LOCAL_REQUIRE) {
                loadObj.depModules[i] = localRequires[moduleId];
                continue;
            }

            // already loaded and is not a plugin resource
            if (contains(modules, dep) && dep.indexOf("!") < 0) {
                loadObj.depModules[i] = modules[dep];
                continue;
            }

            // (?) no need to wait until the loading module finished?
            for (prop in loadingObjList) {
                // if the dependency is already defined
                if (loadingObjList[prop].moduleId === dep) {
                    defined = true;
                    hasWork = true;
                    break;
                }
            }

            // skip calling load because it's already defined
            if (defined) {
                continue;
            }

            if (jsSuffixRegExp.test(dep) && loadedModules[dep] === "complete") {
                // js file already loaded
                continue;
            } else if (loadedModules[dep] === "loading" || loadedModules[dep] === "complete") {
                // there is dep loading, need to wait
                hasWork = true;
                continue;
            } else if (parsePlugin(dep).plugin) {
                hasWork = true;

                // the loader plugin will do the work, do not load the resource here
                continue;
            }

            hasWork = true;

            loadedModules[dep] = "loading";
            global.amd.load(dep);
        }

        if (hasWork) {
            checkLoadingProgress(moduleId);
        } else {
            loadObj.onSuccess(loadObj.depModules);
        }
    }

    /**
     * Execute a factory to retrieve the actual module.
     * This is used for the lazy loading mechanism of amd.
     * If the factory has already been executed before, it will be returned directly.
     *
     * @method
     * @private
     * @param {String} moduleId The module id
     * @param {Function|Object} factory The factory of a module under AMD spec.
     * @returns {Object} the actual module
     * @memberof module:amd
     */
    function executeFactoryIfNeeded(moduleId, factory) {
        var ret = factory;

        // run the factory if it's an internal wrapper
        if (isFunction(factory)) {
            // It is possible that a factory uses the local require in a synchronous way.
            // This would be problematic while circular dependency occurs.
            // The factory will be executed more than once in such condition, and it is not easy to catch.
            // See XDK-2538.
            if (factory.__isExecuting) {
                throw new Error("The module factory is executed while it is being executed. This is probably caused by circular dependency.");
            }

            if (factory.__isInternalWrapper) {
                // use the global amd function to allow extension
                ret = global.amd.executeFactory(moduleId, factory);
            }
        }

        return ret;
    }

    /**
     * Execute a factory function of an AMD module.
     * This could be a hooking point for project customization.
     *
     * @method
     * @param {String} moduleId The module id
     * @param {Function} factory The factory function
     * @returns {Object} The actual module
     * @memberof module:amd
     */
    function executeFactory(moduleId, factory) {
        return factory();
    }

    /**
     * Exec a defined module, to lazy trigger the factory method when the module had been required
     * @method
     * @param {String} moduleId The module id
     * @param {Function|Object} loadedModule The module or the factory of the module
     * @private
     */
    function getExecModule(moduleId, loadedModule) {
        var _module;

        if (isModuleDynamic(moduleId) || isCommonJSComponents(moduleId)) {
            _module = executeFactoryIfNeeded(moduleId, loadedModule);
            return _module;
        }


        if (contains(execedModules, moduleId)) {
            _module = modules[moduleId];
            // the module is executing because of the circular dependency
            if (_module.__isInternalWrapper) {
                return undefined;
            }

            return _module;
        }

        _module = loadedModule || modules[moduleId];

        // circular dependency issues
        // When trying to get ExecModule and it is undefined from the list or loadedModule, it will consider to be a cirucular dependency   
        // which it should not be executed and assigned to module list object.
        // If it is an export object or undefined which only export after getting all the executed modules.
        // Otherwise it will load an undefined into the object list and affect the other item in the cirular dependency.
        if (_module === undefined) {
            return undefined;
        }

        modules[moduleId] = executeFactoryIfNeeded(moduleId, _module);
        // declare the module(factory) has been executed
        execedModules.push(moduleId);

        return modules[moduleId];
    }

    /**
     * Exec the defined module, to lazy trigger the factory method when the module had been required
     * @method
     * @param {String[]} modulesIds moduleIds
     * @param {Function[]} loadedModules loaded and defined modules' factory methods from the call stack
     * @private
     */
    function getExecModules(moduleIds, loadedModules) {

        var i, len, _module, moduleId, ret = [];

        for (i = 0, len = moduleIds.length; i < len; i++) {

            moduleId = moduleIds[i];

            if (moduleId === LOCAL_REQUIRE || moduleId === EXPORTS) {
                _module = loadedModules[i];
            } else {
                _module = getExecModule(moduleId, loadedModules[i]);
            }

            ret.push(_module);
        }

        return ret;

    }

    /**
     * Build a self-contained factory function, such that it can be easily invoked by any function.
     * This could be a hooking point for extension.
     *
     * @method
     * @param {String} moduleId The module id
     * @param {Function|Object} factory The original factory defined
     * @param {String[]} [depIds] List of the id of the dependencies of the module
     * @param {Object[]} [depModules] The dependency modules for the factory
     * @returns {Function|Object} The processed factory, or the module itself if no factory is needed.
     * @memberof module:amd
     */
    function buildFactory(moduleId, factory, depIds, depModules) {
        // factory is not a function, but the module itself
        if (!isFunction(factory)) {
            return factory;
        }

        var factoryWrapper = function () {
            factoryWrapper.__isExecuting = true;
            var executedModules = getExecModules(depIds, depModules);
            var _module = factory.apply(global, executedModules);

            if (_module === undefined) {
                _module = getExports(moduleId);
            }

            return _module;
        };

        factoryWrapper.__isInternalWrapper = true;

        return factoryWrapper;
    }

    /**
     * Defines a module. Also accessible from global scope.
     * @method
     * @param {String} moduleId Module identifier
     * @param {Array} [dependencies] Array of dependencies, in strings
     * @param {Function} factory module factory function or module object.
     * @param {Function} [onFailure] failure callback when package loading is unsuccessful.
     * All dependencies will passed as arguements into this factory method.
     * @memberOf module:amd
     * @example
     * define("app/myModule", ["ax/util"], function(util){
     *     var myModule = ...;
     *     ...
     *     util.each(...);
     *     ...
     *     return myModule;
     * });
     */

    function define() {
        var args = ap.slice.call(arguments),
            moduleId, deps, factory, onSuccess, onFailure;

        if (isString(args[0])) {
            moduleId = args.shift();

            //to avoid duplicate module load
            if (loadedModules[moduleId] === "complete") {
                //since some platform has no console
                if (typeof (console) !== "undefined") {
                    console.warn("### Unable to load the multiple module that module is loaded more than once: " + moduleId);
                }
                return;
            }

        } else {
            // apply a temporary id, which will be resolved after node.onload
            moduleId = getAnonyId();
        }

        if (isArray(args[0])) {
            deps = args.shift();
        } else {
            deps = [LOCAL_REQUIRE, EXPORTS, MODULE];
        }

        factory = args.shift();

        onFailure = args.shift();

        // define(moduleId, ..) has been called
        definedModules.push(moduleId);

        // Usually a module starts loading when it is required by another module.
        // But it may happen that the module is defined without `require` (e.g optimized module that has several `define` in a file).
        // In such cases if the module is required by another module, amd will try to load it using the `paths`.
        // This is not intended, and may cause problems.
        // Declare the module is loaded if it didn't go through the `require` process.
        if (!loadedModules[moduleId] && !isAnonymous(moduleId)) {
            loadedModules[moduleId] = "complete";
        }

        localRequires[moduleId] = generateRequire(moduleId);

        onSuccess = function (loadedDepModules, actualModuleId) {
            modules[actualModuleId] = global.amd.buildFactory(actualModuleId, factory, deps, loadedDepModules);
        };

        return requireModules(moduleId, deps, moduleId, onSuccess, onFailure);
    }

    /**
     * Removes a module from registery.
     * Please note that, the module will remain in memory if there is still reference to this module anywhere else.
     * @method
     * @param {String} moduleId the target module's ID
     * @memberOf module:amd
     */

    function undefine(moduleId) {
        if (!moduleId) {
            return;
        }

        delete modules[moduleId];
    }

    /**
     * Load a list of required modules asynchronously, then execute the success callback.
     * This function will try to resolve the dependencies based on the location _base_.
     * This works for actual id and relative id.
     * @method
     * @private
     * @param {String[]} deps a list of dependencies, could be actual or relative
     * @param {String} base the directory to reference while relative id is given
     * @param {Function} onSuccess the callback to execute when all dependencies are loaded
     * @param {Function} onFailure the callback to execute if some of the dependencies cannot be loaded (timeout)
     * @memberOf module:amd
     */
    function requireModules(moduleId, deps, base, onSuccess, onFailure) {
        var onSuccessWrapper, loadObj;
        var depModules = [];

        if (!isArray(deps)) {
            deps = [];
        }

        if (deps.length > 0) {
            depModules[deps.length - 1] = undefined;
        }

        onSuccessWrapper = function (deps) {
            var actualModuleId = loadingObjList[moduleId].moduleId;

            try {
                onSuccess(deps, actualModuleId);
            } catch (ex) {
                // error occurs while executing the factory
                loadedModules[actualModuleId] = "error";

                if (isFunction(_config.onError)) {
                    _config.onError(actualModuleId, ex);
                } else if (typeof (console) !== "undefined") {
                    if (ex.stack) {
                        console.error(ex.stack);
                    }
                    console.error("### Unable to prepare module " + actualModuleId + ".\n\tError: " + ex);
                }
            }

            // delete both the named and anonymous object (if any)
            delete loadingObjList[moduleId];
            delete loadingObjList[actualModuleId];
        };

        loadObj = {
            base: base,
            moduleId: moduleId,
            onSuccess: onSuccessWrapper,
            onFailure: onFailure,
            dependencies: deps,
            depModules: depModules
        };

        loadingObjList[moduleId] = loadObj;

        var doLoadModules = function () {
            localRequires[loadObj.moduleId] = generateRequire(loadObj.moduleId);
            loadObj.started = true;

            if (prepareContext(loadObj)) {
                loadModules(loadObj);
            }
        };

        if (isAnonymous(moduleId)) {
            // queue until node.onload update the module id in the loading object
            // the callback there will update the module id and the resolution base
            setTimeout(function () {
                doLoadModules();
            }, 1);
        } else {
            doLoadModules();
        }
    }

    /**
     * Prepares the context of the module loading object.
     * This method loads the plugin and plugin resource as well.
     *
     * @method
     * @private
     * @param {Object} loadingObj The loading object of the module
     * @returns {Boolean} True if the context is ready and can progress, false if the context cannot be prepared yet
     * @memberof module:amd
     */
    function prepareContext(loadingObj) {
        var base = loadingObj.base;
        var deps = loadingObj.dependencies;

        // check for existence of plugin dependencies
        for (var i = 0, len = deps.length; i < len; i++) {
            var dep = deps[i];
            var parts = parsePlugin(dep);

            if (parts.plugin) {
                // normalize the plugin id, then update the dep
                var absPluginId = lookupModuleId(normalize(parts.plugin, base));
                var req = getLocalRequire(base);

                // load resource if plugin has already loaded
                if (contains(modules, absPluginId)) {
                    var plugin = getExecModule(absPluginId);
                    var absModuleId, fullModuleId;

                    if (isFunction(plugin.normalize)) {
                        absModuleId = plugin.normalize(parts.moduleId, generateNormalize(base));
                    } else {
                        absModuleId = lookupModuleId(normalize(parts.moduleId, base));
                    }

                    fullModuleId = absPluginId + "!" + absModuleId;
                    deps[i] = dep = fullModuleId;

                    // only load resource if 
                    // 1. it is not loaded, or
                    // 2. resource caching is forbidden
                    if (!modules[fullModuleId] || plugin.dynamic) {
                        loadResourceUsingPlugin(absPluginId, absModuleId, req, loadingObj);
                    } else {
                        loadingObj.depModules[i] = modules[fullModuleId];
                    }
                } else {
                    // load the plugin first, then replay the request
                    req([absPluginId], binder.bind(requireModules, null, loadingObj.moduleId, deps, base, loadingObj.onSuccess, loadingObj.onFailure));
                    return false;
                }
            } else {
                // normalize the dependency (from relative to absolute) and lookup its path (match with paths)
                // (e.g. './lib/A' -> 'lib/A' -> 'A')
                deps[i] = lookupModuleId(normalize(dep, base));
            }
        }

        return true;
    }

    /**
     * Returns the local require of the module.
     * If the module id is not specified or is anonymous, then assume it is global.
     *
     * @method
     * @private
     * @param {String} moduleId The id of the module
     * @returns {Function} The local require function of the module
     * @memberof module:amd
     */
    function getLocalRequire(moduleId) {
        // a global require() call
        if (!moduleId || isAnonymous(moduleId)) {
            return globalRequire;
        }

        // an anonymous define() or require()
        if (!localRequires[moduleId]) {
            localRequires[moduleId] = generateRequire(moduleId);
        }

        return localRequires[moduleId];
    }

    /**
     * Resolves the relative module id (may contains plugin) to absolute id.
     *
     * @method
     * @private
     * @param {String} moduleId The id of the module
     * @returns {String} The absolute module id
     * @throws {Error} If the module cannot be resolved. This will happen if a plugin resource id is resolved before the plugin is loaded.
     * @memberof module:amd
     */
    function resolveModuleId(moduleId, base) {
        var parts = parsePlugin(moduleId);

        // no plugin is involved, resolve the id directly
        if (!parts.plugin) {
            return lookupModuleId(normalize(moduleId, base));
        }

        var absPluginId = lookupModuleId(normalize(parts.plugin, base));

        // resource id cannot be resolved if the plugin hasn't loaded
        if (!contains(modules, absPluginId)) {
            throw new Error("Cannot resolve resource id: plugin is not loaded.");
        }

        var plugin = getExecModule(absPluginId);
        var absModuleId;

        // normalize() is defined in the plugin, use it to resolve the resource id
        if (isFunction(plugin.normalize)) {
            absModuleId = plugin.normalize(parts.moduleId, generateNormalize(base));
        } else {
            absModuleId = lookupModuleId(normalize(parts.moduleId, base));
        }

        return absPluginId + "!" + absModuleId;
    }

    /**
     * Generate a require object according to the base path given.
     * If the base is defined, then this will be a local require. Otherwise, this will be a global require.
     * @method
     * @private
     * @param {String} [base] the base path to reference from
     * @returns {Object} a require object referenced to the corresponding path
     * @memberOf module:amd
     */

    function generateRequire(base) {
        /**
         * Loads several dependencies, then fire the onSuccess or onFailure parameter.
         * @method
         * @memberOf module:amd
         * @param {Object} deps the dependencies to load
         * @param {String} [factory] Success callback function, all dependencies will passed as arguments into this method.
         * @param {Object} [onFailure] Failure callback function.
         */
        var require = function (deps, factory, onFailure) {
            var strParams = [],
                len = arguments.length,
                onSuccess, i, absModuleId, _module;

            // require(String)
            // synchronously return the module represented by the module id
            if (isString(deps)) {
                absModuleId = resolveModuleId(deps, base);

                if (!contains(modules, absModuleId)) {
                    throw new Error("### Module \'" + deps + "\" is not yet loaded!");
                }

                var parts = parsePlugin(absModuleId);
                var plugin;
                if (parts.plugin) {
                    plugin = getExecModule(parts.plugin);

                    if (plugin.dynamic) {
                        throw new Error("require(String) cannot load dynamic resources.");
                    }
                }

                _module = getExecModule(absModuleId);

                if (typeof _module === "undefined") {
                    throw new Error("### Module \'" + deps + "\" is not yet loaded!");
                }

                return _module;
            }

            if (len === 1 && isFunction(deps)) { // assume function(require, exports, module)
                factory = deps;
                deps = [LOCAL_REQUIRE, EXPORTS, MODULE];
            }

            if (isArray(deps) && isFunction(factory)) {
                // require(Array, Function)
                // load the required modules
                onSuccess = function (loadedDepModules) {
                    //execDefinedModules if not defined yet
                    var execedDeps = getExecModules(deps, loadedDepModules);
                    factory.apply(global, execedDeps);
                };

                return requireModules(getRequireId(), deps, base, onSuccess, onFailure);
            } else if (typeof base === "undefined" && isObject(deps) && arguments.length === 1) {
                // require(Object)
                // config amd
                config(deps);
            } else {
                // undefined

                for (i = 0; i < len; i++) {
                    strParams[i] = typeof arguments[i];
                }

                throw new Error("The requested usage of require(" + strParams.join(", ") + ") is undefined");
            }
        };

        /**
         * Converts a String that is of the form [module ID] + '.extension' to an URL path.
         * require.toUrl resolves the module ID part of the string
         * using its normal module ID-to-path resolution rules,
         * except it does not resolve to a path with a ".js" extension.
         * Then, the '.extension' part is then added to that resolved path.
         * @method
         * @param {String} moduleIdWithExt the module id to be resolved
         * @memberOf module:amd
         */
        require.toUrl = function (moduleIdWithExt) {
            var extension,
                pureModuleId = moduleIdWithExt,
                index = moduleIdWithExt.lastIndexOf("."),
                firstSegment = moduleIdWithExt.split("/")[0],
                isRelative = firstSegment === "." || firstSegment === "..";

            // have a file extension alias, and it is not the dots from a relative path
            if (index !== -1 && (!isRelative || index > 1)) {
                extension = moduleIdWithExt.substring(index, moduleIdWithExt.length);
                pureModuleId = moduleIdWithExt.substring(0, index);
            }

            var url = resolveModulePath(normalize(pureModuleId, base)) + (extension || "");
            var comps = url.split("/");
            if (comps[0] === ".") {
                comps = comps.slice(1);
                trimDots(comps);
                comps.splice(0, 0, ".");
            } else {
                trimDots(comps);
            }

            return comps.join("/");
        };

        return require;
    }

    function generateNormalize(base) {
        /**
         * Normalize a relative module id to a real module id that can be mapped to a path.
         * @method
         * @name normalize
         * @param {String} id the relative module id
         * @param {String} [baseId] the base directory to relative to. Override the default if provided.
         * @returns {String} normalized module id
         * @memberof module:amd
         */
        return function (id, baseId) {
            var baseParts,
                normalizedBaseParts,
                lastIndex;

            baseId = baseId || base;

            // normalize only if the given id is a relative id
            if (id && id.charAt(0) === ".") {
                if (baseId) {
                    baseParts = baseId && baseId.split("/");
                    // Convert baseId to array, and lop off the last part,
                    // so that . matches that 'directory' and not id of the baseId's module. 
                    // For instance, baseId of 'one/two/three', maps to 'one/two/three.js', 
                    // but we want the directory, 'one/two' for this normalization.
                    normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                    id = id.split("/");
                    lastIndex = id.length - 1;

                    // if wanting node ID compatibility, strip .js from end of IDs.
                    if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                        name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, "");
                    }

                    id = normalizedBaseParts.concat(id);
                    trimDots(id);
                    id = id.join("/");
                } else if (id.indexOf("./") === 0) {
                    // no baseId, so the id is resolved relative to baseUrl
                    // remove the leading dot
                    id = id.substring(2);
                }
            }

            return id;
        };
    }

    function getExports(moduleId) {
        if (!exportsRegistry[moduleId]) {
            exportsRegistry[moduleId] = {};
        }

        return exportsRegistry[moduleId];
    }

    // define a global require object
    globalRequire = generateRequire();


    // load config if already exists
    if (global.amd && global.amd.config) {
        config(global.amd.config);
    }

    define.amd = {}; // indicate we are AMD loader

    // put things out into global
    globalRequire.config = config;

    if (!global.define) {
        global.define = define;
    }

    if (!global.undefine) {
        global.undefine = undefine;
    }

    // put to global only if it was not defined
    if (!global.require) {
        global.require = globalRequire;
    }

    /**
     * The module that contains AMD API and other non standard APIs.
     * @see {@link https://github.com/amdjs/amdjs-api/wiki|AMD specification}
     * This is accessible from global scope.
     * NOTE: Unit test of this module is done using amdjs-tests (https://github.com/amdjs/amdjs-tests).
     *
     * Following is a list of supported features of this AMD implementation:
     *  * both global & local require objects
     *  ** require(String)
     *  ** require(Array, Function)
     *  ** require.toUrl(String)
     *  ** relative module id resolve
     *  * loader plugin
     *
     * Following is a list of features this AMD implementation hasn't support yet:
     *  * common js compactibility
     *
     * Following is a list of additional features provided by this implementation:
     *     * global onError callback. It will be called when a module cannot be defined properly due to internal failure.
     *
     * @name amd
     * @module
     * @example
     * <caption>Using global onError callback</caption>
     * require({
     *     onError: function(moduleId, ex) {
     *         // custom handling
     *         console.log("Error loading " + moduleId, ex);
     *     }
     * });
     *
     */

    if (!global.amd) {
        global.amd = {
            require: globalRequire,
            define: define,
            undefine: undefine,
            config: config,
            load: load,
            completeLoad: completeLoad,
            resolvePath: resolvePath,
            buildFactory: buildFactory,
            executeFactory: executeFactory
        };
    }

}(this));
/**
 * A util to add some css into document.head. Temporary before AMD css plugin is
 * ready
 * @module ax/cssUtil
 */
amd.define("ax/cssUtil", ["ax/promise", "ax/console"], function (promise, console) {
    "use strict";
    return {
        /**
         * Add some CSS into document.head using <style> tag.  
         * Returns a promise containing a reference to the style node. 
         * Promise resolves when the style node is added to the DOM, but not necessarily before the CSS has been parsed and applied.
         *
         * @method
         * @param {String} css The CSS rules to add
         * @return {Promise.<HTMLStyleElement>} The node which has been added to the DOM
         * @memberof module:ax/cssUtil
         */
        addCss: function (css) {
            var deferred = promise.defer();

            domReady(function () {
                try {
                    var head = document.head || document.getElementsByTagName("head")[0],
                        node = document.createElement("style");

                    node.type = "text/css";
                    if (node.styleSheet) {
                        node.styleSheet.cssText = css;
                    } else {
                        node.appendChild(document.createTextNode(css));
                    }

                    head.appendChild(node);
                    deferred.resolve(node);
                } catch (ex) {
                    deferred.reject(ex);
                }
            });

            return deferred.promise;
        },

        /**
         * Remove a style node created by addCss().
         * @method
         * @param {HTMLStyleElement} node The CSS node to remove
         * @memberof module:ax/cssUtil
         */
        removeCssNode: function(node) {
            if (node && node.parentNode) {
                node.parentNode.removeChild(node);
            } else {
                console.warn("[cssUtil] node is not a valid HTMLStyleElement, or is not in DOM");
            }
        },

        /**
         * Append CSS to a style node
         * @method
         * @param {String} css The CSS rules to append
         * @param {HTMLStyleElement} node The CSS node for the rules to append
         * @memberof module:ax/cssUtil
         */
        appendCss: function(css, node) {
            if (node) {
                if (node.styleSheet) {
                    node.styleSheet.cssText += css;
                } else {
                    node.appendChild(document.createTextNode(css));
                }
            } else {
                console.warn("[cssUtil] node is not a valid HTMLStyleElement");
            }
        }
    };
});

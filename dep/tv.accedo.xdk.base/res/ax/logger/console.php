<?php
		//Default
		$filePath = "log.txt";
		$msg = array();
		
		//get the ip
		if (empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['REMOTE_ADDR'];
		} else {
			$ip = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
			$ip = $ip[0];
		}
		
		//get the time
		$date = date('m/d/Y h:i:s', time());
		
		//get the request data
		$request_body = file_get_contents('php://input');
		$object = json_decode($request_body,true);

		if(isset($object['logPath'])){
			$filePath = $object['logPath'];
		}
		if(isset($object['msg'])){
			$msg = $object['msg'];
		}
		
		// Open to read data
		if (file_exists($filePath)) {
			$current = file_get_contents($filePath);
		}else {
			$current = '';
		}
		
		if (!$current) { $current = ''; }
		
		// Append data
		foreach ($msg as $i => $value) {
			$current .= $date ." ".$ip."  ".$msg[$i]."\n";
		}
		
		// Write msg to file
		file_put_contents($filePath, $current);

		//send back the response
		$response = (object) array('status'=>'true','receivedMsg'=>sizeof($msg));
		echo json_encode($response);
		die();		
	?>
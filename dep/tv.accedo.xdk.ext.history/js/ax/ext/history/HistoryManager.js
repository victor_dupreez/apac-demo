/*jslint browser: true, devel: true */
/**
 * Historical back handling module that works with {@link ax/af/mvc/ControllerManager|Controller Manager}.
 * The history manager can take care of the focus while doing history back.
 * Make sure the promise will be resolved after the controller is properly set up.
 *
 * This class is designed as a singleton, developer should use {@link ax/ext/history/HistoryManager.singleton|singleton} to obtain the instance.
 * Creating instance using the _new_ keyword is prohibited.
 *
 * @class ax/ext/history/HistoryManager
 * @author Andy Hui <andy.hui@accedo.tv>
 * @example
 * // In this example, the controller will try to get data from a remote API,
 * // then update the grid, and finally update the UI for the login status.
 * // But if the controller is shown because of a history back, only update the login status.
 * // (The data should be in the grid already)
 *
 * setup: function (context) {
 *     // inside the controller's setup function
 *
 *     // do the set up that should take place every time
 *     this.registerEventListeners();
 *
 *     // context.historyBack must be true if this is a history back
 *     if (context.historyBack) {
 *         return this.updateLoginStatus(); // return promise to inform the set up is finished
 *     }
 *
 *     // normal navigation
 *     return this.getDataFromRemoteApi()
 *         .then(util.bind(this.showDataOnGrid, this))
 *         .then(util.bind(this.updateLoginStatus, this));
 *
 *     // end of setup()
 * }
 *
 * // the history manager will handle the history focus once that promise is resolved.
 * // This feature can ease the focus handling on history back cases.
 */
define("ax/ext/history/HistoryManager", [
    "ax/core", "ax/class", "ax/util", "ax/EventDispatcher",
    "ax/af/mvc/Controller", "ax/af/mvc/ControllerManager", "ax/af/mvc/AppRoot",
    "ax/af/it/DFSCtrlIt", "ax/device/vKey", "ax/console", "ax/exception",
    "ax/promise", "ax/af/focusManager"
], function(core, klass, util, EventDispatcher,
    Controller, ControllerManager, AppRoot,
    DFSCtrlIt, vKey, console, Exception,
    promise, focusMgr) {
    "use strict";
    var HistoryManager,
        instance,
        sAppRoot = AppRoot.singleton(),
        sCtrlMgr = ControllerManager.singleton();

    HistoryManager = klass.create(EventDispatcher, {
        /**
         * Default back keys: [vKey.BACK.id]
         * @constant
         * @name DEFAULT_BACK_KEYS
         * @memberof ax/ext/history/HistoryManager
         */
        DEFAULT_BACK_KEYS: [vKey.BACK.id],
        /**
         * fired when a history back is done
         * @constant
         * @name EVT_HISTORY_BACK
         * @memberof ax/ext/history/HistoryManager
         */
        EVT_HISTORY_BACK: "ui:history:back",

        /**
         * Get the singleton instance of this class.
         * @method
         * @static
         * @returns {ax/ext/history/HistoryManager} The singleton
         * @memberOf ax/ext/history/HistoryManager
         */
        singleton: function() {
            if (!instance) {
                instance = new HistoryManager();
            }

            return instance;
        }
    }, {
        /**
         * Keys array that will be considered as back keys for historyManager.
         * @protected
         * @name _backKeys
         * @memberof ax/ext/history/HistoryManager#
         */
        _backKeys: null,
        /**
         * Storing the history stack
         * @protected
         * @name _history
         * @memberof ax/ext/history/HistoryManager#
         */
        _history: [],
        /**
         * Initialize the history manager
         * @protected
         * @name init
         * @memberof ax/ext/history/HistoryManager#
         */
        init: function() {

            this._super();

            this.setBackKeys(this.constructor.DEFAULT_BACK_KEYS);

        },
        /*jshint unused:false*/
        /**
         * Activate the history manager. Normally it will be activated at the beginning of the application.
         * And it is unable to deactivate or reactivate again.
         * @name activate
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @public
         */
        activate: function() {

            var self = this;
            //register to AppRoot as default history handling
            sAppRoot.setDefaultKeyAction(
                this._backKeys,

                function(keyEvt) {
                    self.back();
                }

            );

            //listen to ControllerManager's events
            sCtrlMgr.addEventListener(sCtrlMgr.constructor.EVT_REPLACED, function(param) {

                //skip on doing history back
                if (param.options.__doHistoryBack) {
                    return;
                }

                self._push({
                    front: param.exist,
                    frontContext: param.exist.getContextTree(),
                    controller: param.replacement,
                    options: param.options
                });
            });

            sCtrlMgr.addEventListener(sCtrlMgr.constructor.EVT_OPENED, function(param) {

                //skip on doing history back
                if (param.options.__doHistoryBack) {
                    return;
                }

                if (param.options.appRootFirstController) {
                    //skip if opening first controller
                    return;
                }

                self._push({
                    front: null,
                    controller: param.controller,
                    options: param.options
                });
            });

            sCtrlMgr.addEventListener(sCtrlMgr.constructor.EVT_CLOSED, function(param) {


                //skip on doing history back
                if (param.options.__doHistoryBack) {
                    return;
                }

                self._push({
                    front: param.controller,
                    frontContext: param.controller.getContextTree(),
                    controller: null,
                    container: param.container,
                    options: param.options
                });
            });
        },
        /*jshint unused:true*/
        /**
         * Wrapper function to set controller's navigation history data
         * @name _setCtrlNav
         * @param ctrl Target controller
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @protected
         */
        _setCtrlNav: function(ctrl, navigation) {
            ctrl.__navigation = navigation;
        },
        /**
         * Wrapper function to get controller's navigation history data
         * @name _getCtrlNav
         * @param ctrl Target controller
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @protected
         */
        _getCtrlNav: function(ctrl) {
            return ctrl.__navigation;
        },
        /**
         * Wrapper function to clear controller's navigation history data
         * @name _clearCtrlNav
         * @param ctrl Target controller
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @protected
         */
        _clearCtrlNav: function(ctrl) {
            if (!ctrl) {
                return;
            }

            ctrl.__navigation = null;
            delete ctrl.__navigation;
        },
        /**
         * Used by {@link ax/ext/history/HistoryManager#_push|_push}. Remove the
         * skipped history from provide skipped controlller
         * @name _removeSkippedHistory
         * @param {Object} controller The skipped controller
         * @param {Boolean} [__nested] For internal use
         * @returns {Object|Undefined} Navigation history of skipped
         * controller's or undefined is not found
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @protected
         */
        _removeSkippedHistory: function(controller, __nested) {
            var itr = new DFSCtrlIt(controller),
                toSkip = [],
                nav = this._getCtrlNav(controller),
                idx, ctrl;

            //get all the navigations within controller subtree
            while (itr.hasNext()) {
                ctrl = itr.next();

                if (this._getCtrlNav(ctrl)) {
                    toSkip.push(this._getCtrlNav(ctrl));
                }
            }

            //remove children navigations that's skipped
            util.each(toSkip, function(nav) {

                //remove the __navigation object from front controller
                this._clearCtrlNav(nav.controller);

                //remove the front's navigation from history
                idx = util.indexOf(this._history, nav);

                if (idx === -1) {
                    return;
                }

                util.remove(this._history, idx);

                // dig to front navigations if not root call
                if ((__nested || nav.controller !== controller) && nav.front) {
                    this._removeSkippedHistory(nav.front, true);
                }

            }, this);

            return nav;

        },
        /**
         * Used by {@link ax/ext/history/HistoryManager#push|push}. Do the history back controller handling
         * @name _doHistoryBack
         * @param {Object} navigation {@link ax/ext/history/HistoryManager#push|Navigation object} that stores the navigation data
         * @param {Object} [context] context param passed from {@link ax/ext/history/HistoryManager#push|push} method.
         * @returns {Promise} A promise that will be resolved after the history back operation is done
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @protected
         */
        _doHistoryBack: function(navigation, context) {
            context = context || {};

            //add the state to check the 
            context.historyBack = true;

            var updateSubContexts = function(subContexts) {
                util.each(subContexts, function(pair) {
                    pair.value.historyBack = true;

                    if (pair.value.subContexts) {
                        updateSubContexts(pair.value.subContexts);
                    }
                });
            };

            if (navigation.front) {
                if (util.isPlainObject(navigation.frontContext)) {
                    context = util.extend(navigation.frontContext, context);
                }

                if (context.subContexts) {
                    updateSubContexts(context.subContexts);
                }
            }

            // change the controller
            return this._changeController(navigation, context).complete(function() {

                var historyFocus;

                function isCompInDOM(comp) {
                    return comp && comp.getRoot() && comp.getRoot().isInDOMTree();
                }

                historyFocus = navigation.options.historyFocus ? navigation.options.historyFocus : null;

                // focus on the history focus only if it is in DOM
                // if not, focus on last active focus
                if (isCompInDOM(historyFocus)) {
                    focusMgr.focus(historyFocus);
                } else {
                    focusMgr.focus(focusMgr.getLastActiveFocus());
                }
            });
        },

        /**
         * Actually change the controller based on the navigation object.
         * @method
         * @protected
         * @param {Object} navigation Navigation object that stores the navigation data
         * @param {Object} context The context object pass to the controller manager for context restore
         * @returns {Promise.<ax/af/mvc/Controller>|Promise.<Undefined>} return the controller when replace with new one or the new opened one. Return promise with undefined when close controller.
         * @returns {Promise.<module:ax/exception.INTERNAL> Fail to find the container to open
         * @memberof ax/ext/history/HistoryManager#
         */
        _changeController: function(navigation, context) {
            // check if the controller is currently displaying
            if (navigation.front && navigation.controller) {
                return sCtrlMgr.replace(navigation.controller, navigation.front, {
                    context: context,
                    __doHistoryBack: true
                });
            } else if (navigation.front) {
                if (!navigation.container) {
                    return promise.reject(core.createException(Exception.INTERNAL, "When doing History back to the previous controller, it fails to find the container to open"));
                }

                return sCtrlMgr.open(navigation.front, navigation.container, {
                    context: context,
                    __doHistoryBack: true
                });
            } else {
                return sCtrlMgr.close(navigation.controller, {
                    __doHistoryBack: true
                });
            }
        },

        /**
         * To push the navigation as history.
         * @name _push
         * @param {Object} navigation Key value map for the input parameters
         * @param {ax/af/mvc/Controller|null} [navigation.front] The front controller
         * @param {ax/af/mvc/Controller|null} [navigation.controller] The new controller
         * @param {Object} [navigation.options] Options object that was passed to the {@link ax/af/mvc/ControllerManager|Controller Manager}'s open, replace and close methods
         * @param {boolean} [navigation.options.historySkip] True if need skip this navigation from history (Note: only appliable to {@link ax/af/ControllerManager#replace|replace} method). Default: false.
         * @param {boolean} [navigation.options.controllerCache]  False if need disable the view cache when saving the view via navigation (Note: only appliable to {@link ax/af/ControllerManager#replace|replace} method). Default: true.
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @protected
         */
        _push: function(navigation) {

            var lastNavigation, skippedNav;

            if (navigation.options && navigation.options.historySkip) {
                // skip history handling on replace
                if (!navigation.front || !navigation.controller) {
                    console.warn("cannot skip history on open and close controller");
                    return;
                } else {
                    skippedNav = this._removeSkippedHistory(navigation.front);

                    if (!(skippedNav && skippedNav.front)) {
                        return;
                    }

                    //relinked skipped navigation's front controller
                    navigation.front = skippedNav.front;
                }
            }

            if (navigation.controller) {
                //save the previous navigation previous controller as linked
                this._setCtrlNav(navigation.controller, navigation);
            }

            //update the cache on controller, only the class contructors are saved
            if (navigation.options.controllerCache === false) {

                //deinit the front controller since no cache. So replace and close manager case will set controller to be constrcuctor.
                if (navigation.front) {
                    navigation.front = navigation.front.constructor;

                    //to set the previous history to be the constructor
                    if (this._history.length > 0) {
                        lastNavigation = this._history[this._history.length - 1];
                        lastNavigation.controller = lastNavigation.controller.constructor;
                    }
                }
            }

            this._history.push(navigation);

        },
        /**
         * Do a history back. Default to back history by one step.
         *
         * Note: The return type of this function has changed to {@link Promise} since 2.2.0, which is breaking the backward compatibility prior 2.1.
         *
         * @name back
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @param {Object} [options]
         * @param {Class} [options.ctrl] Controller's class to back to. The latest occurance will be taken .
         * @param {Object} [options.context] Context object that will be passed to {@link ax/af/mvc/ControllerManager|Controller Manager}
         * @returns {Promise} A promise object that will be resolved after the history back operation is done
         * @public
         */
        back: function(options) {
            options = options || {};

            var navSteps, navigation, unused, i, ctrl, pendingBack;

            if (util.isUndefined(options.ctrl)) {
                navSteps = 1;
            } else {
                ctrl = options.ctrl;
                if (!ctrl.prototype instanceof Controller) {
                    throw core.createException("IncorrectParam", "first arugment should be either a Number or a Controller Subclass.");
                }

                for (i = this._history.length - 1; i >= 0; i--) {
                    if (this._history[i].front instanceof ctrl) {
                        navSteps = this._history.length - i;
                        break;
                    } else if (i === 0) {
                        //unable to find the controller in the history
                        return promise.reject(false);
                    }
                }

            }


            //to handle go to homepage case
            if (navSteps === -1) {
                navSteps = this._history.length;
            }


            if (!this._history.length || navSteps > this._history.length) {
                return promise.reject(false);
            }

            if (navSteps > 1) {

                unused = this._history.splice(this._history.length - navSteps + 1, navSteps - 1);
                //back to the desired step one by one
                for (i = unused.length - 1; i >= 0; i--) {

                    //since there are two connections in reference to the controllers,so need to unref twice
                    console.info("history manager multiback from " + i);

                    if (pendingBack) {
                        pendingBack = pendingBack.then(util.bind(this._doHistoryBack, this, unused[i]));
                    } else {
                        pendingBack = this._doHistoryBack(unused[i]);
                    }
                }

                //remove the reference of the unused items
                unused.splice(0, unused.length);

                console.info(this._history);
            }

            //the last naviation
            navigation = this._history.pop();

            // set up the actual promise on history back done
            if (pendingBack) {
                pendingBack = pendingBack.then(util.bind(this._doHistoryBack, this, navigation, options.context));
            } else {
                pendingBack = this._doHistoryBack(navigation, options.context);
            }

            // dispatch EVT_HISTORY_BACK event after the final step of history back is done
            return pendingBack.then(util.bind(this.dispatchEvent, this, this.constructor.EVT_HISTORY_BACK, navigation));
        },
        /**
         * @name setBackKeys
         * @param {Array} vkeys - Set the keys that History Manager should handle
         * them as going back
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @public
         */
        setBackKeys: function(vkeys) {
            this._backKeys = vkeys;
        },
        /**
         * Clears history.
         * @name clear
         * @memberof ax/ext/history/HistoryManager#
         * @function
         * @public
         */
        clear: function() {

            util.each(this._history, function(nav) {
                this._clearCtrlNav(nav.controller);
            }, this);

            util.clear(this._history);

        },
        /**
         * Returns the current history stack
         * @name getHistoryStack
         * @returns {Array} stack of navigation
         * @public
         * @memberof ax/ext/history/HistoryManager#
         * @function
         */
        getHistoryStack: function() {
            return util.clone(this._history);
        }
    });

    // enforce the instance creation before returning, preventing 2 instances being created
    // (2 executions may go into the creation at the same time...)
    HistoryManager.singleton();

    return HistoryManager;

});
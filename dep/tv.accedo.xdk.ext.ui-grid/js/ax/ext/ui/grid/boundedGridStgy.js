/**
 * A collection of strategies for bounded grid widget.
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @module ax/ext/ui/grid/boundedGridStgy
 */
define("ax/ext/ui/grid/boundedGridStgy", [
    "ax/device/vKey",
    "ax/ext/ui/grid/gridStgy"
], function (
    vKey,
    gridStgy
) {
    "use strict";
    var mod = function (number, divisor) { // modulo operation
            return ((number % divisor) + divisor) % divisor;
        },
        strategies = {};

    // ======== check selectable strategies ========
    strategies.SCROLL_BOUNDARY_CHECK_SELECTABLE_STGY = function (frontBoundary, endBoundary, curState, toState) {
        var toDsIndex = toState.selectedDsIndex,
            toGridRow = toState.selectedGridRow,
            dataCount = curState.ds.getTotalCount(),
            toDsRow, lastDsRow;

        if (toDsIndex < 0) {
            return false;
        }

        toDsRow = Math.floor(toDsIndex / curState.cols);
        lastDsRow = Math.floor((dataCount - 1) / curState.cols);

        // inside the forbidden boundary
        if (toGridRow < frontBoundary || toGridRow >= curState.rows - endBoundary) {
            // need to be reaching ends to allow selection
            if (toDsRow > frontBoundary && toDsRow < lastDsRow - endBoundary) {
                // not allowed
                return false;
            }
        }

        if (toDsIndex < dataCount) { // witnin range
            return true;
        }

        if (toDsRow <= lastDsRow) { // we can still go to that row
            toState.selectedDsIndex = dataCount - 1;
            toState.selectedGridCol = mod(dataCount - 1, this._cols);
            return true;
        }

        return false;
    };

    // ======== key navigation strategies ========
    strategies.SCROLL_BOUNDARY_KEY_NAVIGATION_STGY = function (isVertical, frontBoundary, endBoundary, key, state) {
        var rowStep, curDsIndex = state.selectedDsIndex,
            dataCount = state.ds.getTotalCount(),
            curGridRow = state.selectedGridRow,
            toGridRow, lastDsRow, toDsRow;

        if (isVertical) {
            switch (key) {
            case vKey.UP.id:
                return {
                    colChange: -1
                };
            case vKey.DOWN.id:
                return {
                    colChange: 1
                };
            case vKey.LEFT.id:
                rowStep = -1;
                break;
            case vKey.RIGHT.id:
                rowStep = 1;
                break;
            default:
                break;
            }
        } else {
            switch (key) {
            case vKey.UP.id:
                rowStep = -1;
                break;
            case vKey.DOWN.id:
                rowStep = 1;
                break;
            case vKey.LEFT.id:
                return {
                    colChange: -1
                };
            case vKey.RIGHT.id:
                return {
                    colChange: 1
                };
            default:
                break;
            }
        }


        if (frontBoundary <= 0 && endBoundary <= 0) { // no boundary actually
            return {
                rowChange: rowStep
            };
        }

        toGridRow = curGridRow + rowStep;
        toDsRow = Math.floor(curDsIndex / state.cols) + rowStep;
        lastDsRow = Math.floor((dataCount - 1) / state.cols);

        // inside the forbidden boundary
        if (toGridRow < frontBoundary || toGridRow >= state.rows - endBoundary) {
            // need to be reaching ends to allow selection
            if (toDsRow < frontBoundary || toDsRow > lastDsRow - endBoundary) {
                // allowed
                return {
                    rowChange: rowStep
                };
            }
            return {
                scroll: rowStep
            };
        }

        return {
            rowChange: rowStep
        };
    };

    /**
     * bounded grid onUpdate strategy, update the grid when there are datasource updated like remove, insert, append
     * It will keep the focus on the original position(same selectedDsIndex and selectedRow) and won't change it.
     * However, when the current selected is not possible, it will update the focus.
     *
     * 1. When the current selected DsIndex is not exist, it will redirect to the last selectedDsIndex.
     * e.g the maximium selectedDsIndex is 40 and after remove 1 item, the current selectedDsIndex will become 39.
     *
     * 2. When the new focus is inside the boundary(either front or bottom), the focus will redirect to the possible one.
     * e.g the grid with 3x5, boundary is 1. Only when at the end of the grid, it can focus on the last row. Otherwise, it will fail to focus that row.
     * After insert item, that row might become not the last row and the focus become illegal. This strategy will update the focus to the possible row one.
     *
     * //Before update and size is 44, reaching the end
     * 30 31  32  33  34
     * 35 36  37  38  39
     * 40 41 [42]
     *
     * //After insert 3 items, it will change the row focus to the previous row due to the last row is inside the boundary
     * 27 28  29  30 31
     * 32 33 [34] 35 36
     * 37 38  39  40 41
     *
     * @param {Number} frontBoundary the front boundary
     * @param {Number} endBoundary the end boundary
     * @param {Object} evt the update event from the datasource and grid. It will know whether insert/reset/remove
     * @param {Object} state the current state of the grid
     * @param {ax/af/data/Datasource} the datasource
     * @returns {Boolean} True if perform update to grid
     * @extends ax/ext/ui/grid/gridStgy.BASIC_ONUPDATE_STGY
     * @method
     * @memberof ax/ext/ui/grid/boundedGridStgy
     */
    // ======== onUpdate strategies ========
    strategies.ONUPDATE_STGY = function (frontBoundary, endBoundary, evt, state, ds) {

        var ret = gridStgy.BASIC_ONUPDATE_STGY(evt, state, ds);

        var dataCount = ds.getTotalCount(),
            firstSlotDsIndex = state.firstSlotDsIndex,
            lastSlotDsIndex = state.firstSlotDsIndex + state.rows * state.cols - 1;
        //checking the dsIndex whether lie on the boundary cases

        //bottom boundary
        if (lastSlotDsIndex < dataCount - 1) {

            //check whether the new focus is in the bottom boundary

            var endDiff = state.selectedGridRow - (state.rows - 1 - endBoundary);

            if (endDiff >= 0) {
                //update the new dsIndex and row so that fulfill the end boundary
                state.selectedDsIndex = state.selectedDsIndex - endDiff * state.cols;
                state.selectedGridRow -= endDiff;
                return true;
            }
        }

        //front boundary
        if (firstSlotDsIndex !== 0) {
            //check whether the new focus is in the front boundary
            var frontDiff = frontBoundary - state.selectedGridRow;

            if (frontDiff >= 0) {
                //update the new dsIndex and row so that fulfill the front boundary
                state.selectedDsIndex = state.selectedDsIndex + frontDiff * state.cols;
                state.selectedGridRow += frontDiff;
                return true;
            }
        }

        return ret;
    };

    return strategies;
});
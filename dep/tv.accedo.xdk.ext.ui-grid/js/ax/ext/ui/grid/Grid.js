/**
 *
 * The is the base class for Grid widget.
 *
 * The grid uses sevral strategies to complete its operation.
 * The strategies are (in logical order):
 *
 * 1. pre-ready
 * 2. ds range calculation find the corresponding dsIndex need for the display
 * 3. check scrollable
 * 4. check selectable
 * 5. data mapping into grid slot
 * 6. scroll entrance, for adding child components into grid
 * 7. scroll shift, for animating scrolling
 * 8. scroll exit, for removing child components from grid
 * 9. key navigation, determines the navigation upon arrow strokes
 * 10. onUpdate
 *
 * @class ax/ext/ui/grid/Grid
 * @extends ax/af/Container
 * @param {Object} opts The options object
 * @param {Integer} opts.rows number of rows in the grid
 * @param {Integer} opts.cols number of columns in the grid
 * @param {ax/ext/ui/grid/Grid.SCROLL_BASE_ON_ITEM | ax/ext/ui/grid/Grid.SCROLL_BASE_ON_ROW | ax/ext/ui/grid/Grid.SCROLL_BASE_ON_PAGE} opts.scrollbarBehavior scroll bar bahavior
 */
define("ax/ext/ui/grid/Grid", [
    "ax/class",
    "ax/af/Container",
    "ax/ext/ui/AbstractScrollable",
    "ax/console",
    "ax/af/data/Datasource",
    "ax/af/evt/type",
    "ax/device/vKey",
    "ax/util",
    "ax/core",
    "ax/af/focusManager",
    "ax/promise",
    "ax/ext/ui/grid/gridStgy"
], function (
    klass,
    Container,
    Scrollable,
    console,
    Ds,
    evtType,
    vKey,
    util,
    core,
    focusMgr,
    promise,
    gridStgy
) {

    "use strict";
    var mod = function (number, divisor) { // modulo operation
            //in wiiU 1%0 is 0, so we try to standardize the %0 which should be NaN in all the platform
            if (divisor === 0) {
                return NaN;
            }
            return ((number % divisor) + divisor) % divisor;
        },
        stgyNotReadyErrFn = function () {
            throw core.createException("IncorrectState", "This Grid\"s strategy has not been specified yet!");
        },
        rejectionHandler = function (error) {
            console.info(error);
        },
        rejectionLogger = function (error) {
            console.info(error);
            throw error;
        },
        getPromiseWithMessage = function (message) {
            console.info(message);
            return promise.resolve(message);
        },
        getRejectedPromiseWithMessage = function (message) {
            console.info(message);
            return promise.reject(message);
        };


    return klass.create(Scrollable, {
        /**
         * This is used to indicate a vertical alignment.
         * @memberof ax/ext/ui/grid/Grid
         * @constant
         */
        VERTICAL: 1,
        /**
         * This is used to indicate a horizontal alignment.
         * @constant
         * @memberof ax/ext/ui/grid/Grid
         */
        HORIZONTAL: 2,
        /**
         * This is used to indicate a scrollbar will update layout base on each item movement.
         * @constant
         * @memberof ax/ext/ui/grid/Grid
         */
        SCROLL_BASE_ON_ITEM: 1,
        /**
         * This is used to indicate a scrollbar will update layout base on each row movement.
         * @constant
         * @memberof ax/ext/ui/grid/Grid
         */
        SCROLL_BASE_ON_ROW: 2,
        /**
         * This is used to indicate a scrollbar will update layout base on each page movement.
         * @constant
         * @memberof ax/ext/ui/grid/Grid
         */
        SCROLL_BASE_ON_PAGE: 3
    }, {
        /**
         * ready state of the grid
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _ready: false,
        /**
         * Grid column count
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _cols: 5,
        /**
         * Grid row count
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _rows: 5,
        /**
         * The scroll bar bahavior
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _scrollbarBehavior: 3,
        // ==== grid fillings information ====
        /**
         * Currently selected datasource index
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _selectedDsIndex: 0,
        /**
         * Curent selected grid row
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _selectedGridRow: 0,
        /**
         * Curent selected grid column
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _selectedGridCol: 0,
        /**
         * Curent first grid slot datasource index
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _firstSlotDsIndex: 0,
        /**
         * Curent last grid slot datasource index
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _lastSlotDsIndex: 0,
        /**
         * Grid data temporary storage
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _data: null,
        // ==== ds ====
        /**
         * Datasource instance
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _ds: null,
        // ==== dom stuff ====
        /**
         * Components inside the grid
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _gridComponents: null,
        /**
         * The container of the grid
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _container: null,
        // ==== strategies ====
        /**
         * Pre-ready strategy function
         * @param {Object} state Original grid state
         * @param {Number} state.selectedDsIndex selected datasource index, can be modified to indicate updates
         * @param {Number} state.selectedGridRow selected grid slot row, can be modified to indicate updates
         * @param {Number} state.cols grid column count
         * @param {Number} state.rows grid row count
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _prereadyStgy: stgyNotReadyErrFn,
        /**
         * Check scrollable strategy, checks whether a scroll can be taken.
         * @param {Object} curState Current grid state
         * @param {Number} curState.selectedDsIndex selected datasource index, can be modified to indicate updates
         * @param {Number} curState.selectedGridRow selected grid slot row, can be modified to indicate updates
         * @param {Number} curState.selectedGridCol selected grid slot column
         * @param {Number} curState.firstSlotDsIndex first grid slot datasouce index
         * @param {Number} curState.lastSlotDsIndex last grid slot datasouce index
         * @param {Number} curState.cols grid column count
         * @param {Number} curState.rows grid row count
         * @param {ax/af/data/Datasource} curState.ds grid datasource
         * @param {Object} toState Target grid state
         * @param {Number} toState.selectedDsIndex selected datasource index, can be modified to indicate updates
         * @param {Number} toState.selectedGridRow selected grid slot row, can be modified to indicate updates
         * @param {Number} toState.selectedGridCol selected grid slot column
         * @param {Number} toState.firstSlotDsIndex first grid slot datasouce index
         * @param {Number} toState.lastSlotDsIndex last grid slot datasouce index
         * @returns {Boolean} whether this scroll should be done
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _checkScrollableStgy: stgyNotReadyErrFn,
        /**
         * Check scrollable strategy, checks whether a scroll can be taken.
         * @param {Object} curState Current grid state, includes params like {@link ax/ext/ui/grid/Grid#_checkScrollableStgy}
         * @param {Number} curState.data selected datasource index
         * @param {Number} curState.gridComponents grid's child components
         * @param {Object} toState Target grid state, includes params like {@link ax/ext/ui/grid/Grid#_checkScrollableStgy}
         * @returns {Boolean} whether this selection should be done
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _checkSelectableStgy: stgyNotReadyErrFn,
        /**
         * Data mapping strategy, calculate the data for one grid slot
         * @param {Object} curState Current grid state, includes params like {@link ax/ext/ui/grid/Grid#_checkSelectableStgy}
         * @param {Array} dataArr the whole data array, for the whole grid
         * @param {Array} gridRow the slot's grid row
         * @param {Array} gridCol the slot's grid column
         * @param {Array} dsIndex datasource index of this current slot
         * @returns {Object} The data to be rendered inside this grid slot, null if nothing to render
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _dataMappingStgy: stgyNotReadyErrFn,
        /**
         * Render a grid item according to its assigned data
         * @param {Object} data The data for render
         * @returns {ax/af/Component} The render result
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _displayStgy: stgyNotReadyErrFn,
        /**
         * Scroll entrance strategy, adds the child components into grid's container
         * @param {Array} dataArr the whole data array, for the whole grid
         * @param {Number} scrollStep the scroll step
         * @param {Object} curState Current grid state, includes params like {@link ax/ext/ui/grid/Grid#_checkSelectableStgy}
         * @param {Object} toState Target grid state, includes params like {@link ax/ext/ui/grid/Grid#_checkScrollableStgy}
         * @param {Object} toState.data the whole data array, for the whole grid
         * @param {Object} toState.gridComponents grid's child components
         * @returns {Promise.<Undefined>|Undefined} Promise if shift stgy is async.
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _scrollEntranceStgy: stgyNotReadyErrFn,
        /**
         * Scroll shift strategy, handles animation of the grid scroll
         * @param {Array} dataArr the whole data array, for the whole grid
         * @param {Number} scrollStep the scroll step
         * @param {Object} curState Current grid state, includes params like {@link ax/ext/ui/grid/Grid#_scrollEntranceStgy}
         * @param {Object} toState Target grid state, includes params like {@link ax/ext/ui/grid/Grid#_scrollEntranceStgy}
         * @returns {Promise.<Undefined>|Undefined} Promise if shift stgy is async.
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _scrollShiftStgy: stgyNotReadyErrFn,
        /**
         * Scroll exit strategy, removes the child components from grid's container
         * @param {Array} dataArr the whole data array, for the whole grid
         * @param {Number} scrollStep the scroll step
         * @param {Object} curState Current grid state, includes params like {@link ax/ext/ui/grid/Grid#_scrollEntranceStgy}
         * @param {Object} toState Target grid state, includes params like {@link ax/ext/ui/grid/Grid#_scrollEntranceStgy}
         * @returns {Promise.<Undefined>|Undefined} Promise if shift stgy is async.
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _scrollExitStgy: stgyNotReadyErrFn,
        /**
         * Takes in an arrow key, and indicates the navigation amount.
         * @param {ax/deivce/vKey} the input arrow key
         * @param {Object} state Current grid state, includes params like {@link ax/ext/ui/grid/Grid#_scrollEntranceStgy}
         * @returns {Object} navigation
         * @returns {Number} navigation.scroll the amount to scroll, can be nagetive
         * @returns {Number} navigation.rowChange the amount of row change, can be nagetive
         * @returns {Number} navigation.colChange the amount of column change, can be nagetive
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _keyNavigationStgy: stgyNotReadyErrFn,
        /**
         * onUpdate strategy, update the grid when there are datasource updated like remove, insert, append
         * @param {Object} state Original grid state
         * @param {Number} state.selectedDsIndex selected datasource index, can be modified to indicate updates
         * @param {Number} state.selectedGridRow selected grid slot row, can be modified to indicate updates
         * @param {Number} state.cols grid column count
         * @param {Number} state.rows grid row count
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         */
        _onUpdateStgy: stgyNotReadyErrFn,
        /**
         * ds range calculation strategy, update the grid when calculating the selected dsIndex at specific row.
         * It will find out the relative dsIndex for the grid via 'from' and 'to'. It will give the negative dsIndex.
         * actualFrom and actualTo will reflect the absolute dsIndex to the datasource.
         * After getting the values, it will get the data from the datasouce via getRange.
         * If the grid size(row*col) is smaller than the datasource count, it will fetch the data from actualFrom to datasource total count + 0 to actualTo.
         * After that, it will go into the data mapping to obtain value from the relative dsIndex and value obtained from datasource, and goes on.
         * @param {Number} dsIndex the selected dsIndex
         * @param {Number} targetRow the target row for that selected dsIndex
         * @param {Object} range the information about the datasource
         * @returns {Number} range.from the from ds index in the current displayed grid which is used for the grid to keep tracking the state
         * @returns {Number} range.to the to dsIndex(exclusive) in the current displayed grid
         * @returns {Number} range.actualFrom the first dsIndex to datasource of the grid.
         * @returns {Number} range.actualTo the last dsIndex(exclusive) to datasource index of the grid
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @protected
         * @example
         * <caption>
         * Normally range.from/range.to is always the same as range.actualFrom/range.actualTo.
         * However , they may be different when in the looping cases or bounded scrolling case.
         * </caption>
         * <caption>
         * E.g in the looping/bounded scrolling case, it may request dsIndex -3 on row 0.
         * Expected Range.
         *
         * ({
         *   from:-4,
         *   to: 8,
         *   actualFrom: 6,
         *   actualTo: 8
         * })
         *
         * the dsIndex in the grid
         * =============
         * -4 -3 -2  -1
         *  0  1  2  3
         *  4  5  6  7
         * ============
         * actualFrom/to will getRange(fetch the specific data) and then data mapping strategey
         * will map the specific index with ds index value like the following cases.
         * =========
         * 6 7 8 9
         * 0 1 2 3
         * 4 5 6 7
         * ========
         * </caption>
         */
        _dsRangeCalculationStgy: stgyNotReadyErrFn,
        /**
         * ets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_prereadyStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setPrereadyStgy: function (stgy) {
            this._prereadyStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_checkScrollableStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setCheckScrollableStgy: function (stgy) {
            this._checkScrollableStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_checkSelectableStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setCheckSelectableStgy: function (stgy) {
            this._checkSelectableStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_dataMappingStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setDataMappingStgy: function (stgy) {
            this._dataMappingStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_displayStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setDisplayStgy: function (stgy) {
            this._displayStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_scrollEntranceStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setScrollEntranceStgy: function (stgy) {
            this._scrollEntranceStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_scrollShiftStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setScrollShiftStgy: function (stgy) {
            this._scrollShiftStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_scrollExitStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setScrollExitStgy: function (stgy) {
            this._scrollExitStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_keyNavigationStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setKeyNavigationStgy: function (stgy) {
            this._keyNavigationStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_prereadyStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setOnUpdateStgy: function (stgy) {
            this._onUpdateStgy = stgy;
        },
        /**
         * Sets strategy function.
         * @see {@link ax/ext/ui/grid/Grid#_dsRangeCalculationStgy}
         * @param stgy the strategy function to set
         * @method
         * @memberof ax/ext/ui/grid/Grid#
         * @public
         */
        setDsRangeCalculationStgy: function (stgy) {
            this._dsRangeCalculationStgy = stgy;
        },
        /**
         * Overrides parent init() function.
         * @method
         * @protected
         * @memberof ax/ext/ui/grid/Grid#
         */
        init: function (opts) {
            opts = opts || {};

            if (opts.rows) {
                this._rows = opts.rows;
            }
            if (opts.cols) {
                this._cols = opts.cols;
            }
            if (opts.scrollbarBehavior) {
                this._scrollbarBehavior = opts.scrollbarBehavior;
            }

            this._super(opts);

            this._root.addClass("wgt-grid");

            //to create a container which can be scrolled later and for the animation. width:200% is for horizontal grid
            //as it will go to below of the grid when adding a new row and affect the animation
            this._container = new Container({
                css: "wgt-grid-scroll",
                parent: this
            });

            this.addEventListener(evtType.KEY, this._keyHandler);
            this.addEventListener(evtType.FOCUS, util.bind(this._onFocusHandler, this));

            //Set default ds calculation stgy for backward compatibility
            this.setDsRangeCalculationStgy(gridStgy.BASIC_DS_RANGE_CALCULATION_STGY);
        },
        /**
         * Sets the datasource for this grid, also renders the grid for the first time.
         * @method setDatasource
         * @param {ax/af/data/Datasource} ds datasource to set
         * @returns {Promise.<Undefined>} no actual resolve value
         * @throws {Promise.<String>} "Unable to set set the same datasource" If the datasource is the same
         * @throws {Promise.<String>} "Cannot set an invalid datasource" If the parameter ds is not datasource type
         * @throws {Promise.<String>} "Unable to initialize the datasource, totalCount still equals -1" If the datasource total count is -1 which is not initialized
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         */
        setDatasource: function (ds) {

            if (!(ds instanceof Ds)) {
                return getRejectedPromiseWithMessage("Cannot set an invalid datasource");
            }

            //If the given datasource is already registered for this grid, do nothing
            if (this._ds === ds) {
                return getRejectedPromiseWithMessage("Unable to set set the same datasource");
            }

            //Remove the previous datasource, if any
            if (this._ds && this.__datasourceListener) {
                this._ds.removeEventListener(Ds.EVT_UPDATED, this.__datasourceListener);
            }

            //Remove all the current children
            this.reset();

            this._ready = false;
            //Register the new datasource
            this._ds = ds;


            var range, state;

            // see if need to change anything
            state = {
                selectedDsIndex: this._selectedDsIndex,
                selectedGridRow: this._selectedGridRow,
                cols: this._cols,
                rows: this._rows,
                ds: ds
            };
            this._prereadyStgy(state);
            this._selectedDsIndex = state.selectedDsIndex;
            this._selectedGridRow = state.selectedGridRow;

            range = this._dsRangeCalculationStgy(this._selectedDsIndex, this._selectedGridRow, state);

            // updated the grid state in case strategy have updated it
            this._selectedGridCol = mod(this._selectedDsIndex, this._cols);
            this._firstSlotDsIndex = range.from;


            //private listner ref for removal
            if (!this.__datasourceListener) {
                this.__datasourceListener = util.bind(this._onDatasourceUpdated, this);
            }

            //listen for more the data source if there is any update
            this._ds.addEventListener(Ds.EVT_UPDATED, this.__datasourceListener);

            return this.__validateDs(range.actualFrom, range.actualTo).then(util.bind(this.select, this, this._selectedDsIndex, this._selectedGridRow));
        },
        /**
         * Gets the datasource set for this grid.
         * @method
         * @returns {ax/af/data/Datasource} datasource set
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         */
        getDatasource: function () {
            return this._ds;
        },
        /**
         * Gets the datasource set for this grid.
         * @method
         * @returns {ax/af/data/Datasource} datasource set
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         * @deprecated Replaced  by {@link ax/ext/ui/grid/Grid#getDatasource}
         */
        getDatasouce: function () {
            return this.getDatasource();
        },
        /**
         * Scrolls the grid for certain steps.
         * @method
         * @param {Number} step steps to scroll the grid, negative number means scrolling backwards
         * @param {Object} opts the options for scrolling
         * @param {Boolean} [opts.noAnimation] set to true to turn off scrolling animation
         * @returns {Promise.<Undefined>} no actual resolve value
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         */
        scroll: function (step, opts) {
            if (!this._ready) {
                return getRejectedPromiseWithMessage("Grid is not ready yet");
            }

            var dsStep = step * this._cols,
                selectedGridRow = this._selectedGridRow - step,
                selectedDsIndex = this._selectedDsIndex,
                firstSlotDsIndex = this._firstSlotDsIndex + dsStep,
                lastSlotDsIndex = firstSlotDsIndex + this._rows * this._cols - 1,
                curState, toState;

            // limit selected grid row to within view
            if (selectedGridRow < 0) {
                selectedGridRow = 0;
            } else if (selectedGridRow >= this._rows) {
                selectedGridRow = this._rows - 1;
            }

            // limit selected datasource index to within view
            if (selectedDsIndex < firstSlotDsIndex) {
                do {
                    selectedDsIndex += this._cols;
                } while (selectedDsIndex < firstSlotDsIndex);
            } else if (selectedDsIndex > lastSlotDsIndex) {
                do {
                    selectedDsIndex -= this._cols;
                } while (selectedDsIndex > lastSlotDsIndex);
            }

            // calculate target state
            toState = {
                selectedDsIndex: selectedDsIndex,
                selectedGridRow: selectedGridRow,
                selectedGridCol: this._selectedGridCol,
                firstSlotDsIndex: firstSlotDsIndex,
                lastSlotDsIndex: lastSlotDsIndex
            };

            curState = {
                selectedDsIndex: this._selectedDsIndex,
                selectedGridRow: this._selectedGridRow,
                selectedGridCol: this._selectedGridCol,
                firstSlotDsIndex: this._firstSlotDsIndex,
                lastSlotDsIndex: this._firstSlotDsIndex + this._rows * this._cols - 1,
                rows: this._rows,
                cols: this._cols,
                ds: this._ds
            };

            if (!this._checkScrollableStgy(curState, toState)) {
                return getRejectedPromiseWithMessage("Check scrollable strategy indicates scrolling bound has reached.");
            }

            return this.select(toState.selectedDsIndex, toState.selectedGridRow, opts).fail(rejectionLogger);
        },
        /**
         * Selects a specific item in grid.
         * @method
         * @param {Number} dsIndex the datasource index of the grid slot to select
         * @param {Number} targetRow the target grid row to put that grid item in
         * @param {Object} opts the options for selection
         * @param {Boolean} opts.noAnimation set to true to turn off scrolling animation
         * @param {Boolean} opts.forceRender disregard the selectable checking, and force a render even if not selectable.
         * @returns {Promise.<Undefined>} no actual resolve value
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         */
        select: function (dsIndex, targetRow, opts) {

            this._ready = false;

            opts = opts || {};

            var curState = {
                    selectedDsIndex: this._selectedDsIndex,
                    selectedGridRow: this._selectedGridRow,
                    selectedGridCol: this._selectedGridCol,
                    firstSlotDsIndex: this._firstSlotDsIndex,
                    lastSlotDsIndex: this._firstSlotDsIndex + this._rows * this._cols - 1,
                    data: this._data,
                    gridComponents: this._gridComponents,
                    ds: this._ds,
                    rows: this._rows,
                    cols: this._cols
                },
                curRange = this._dsRangeCalculationStgy(this._selectedDsIndex, this._selectedGridRow, curState),
                toState = {
                    selectedDsIndex: dsIndex,
                    selectedGridRow: targetRow,
                    selectedGridCol: mod(dsIndex, this._cols),
                    firstSlotDsIndex: -1,
                    // not calculated yet
                    lastSlotDsIndex: -1,
                    // not calculated yet
                    data: null,
                    // not calculated yet
                    gridComponents: null // not calculated yet
                },
                self = this,
                selectable, scrollStep, toRange, actualDsFrom, actualDsTo, updateSelectionFn, scrollFn, dataArr = [],
                dataTotalCount = this._ds.getTotalCount(),
                fail;

            // automatically guess target row
            if (!util.isNumber(targetRow)) {
                if (dsIndex >= curRange.from && dsIndex < curRange.to) {
                    targetRow = this._selectedGridRow;
                } else if (dsIndex < curRange.from) {
                    targetRow = 0;
                } else {
                    targetRow = this._rows - 1;
                }

                toState.selectedGridRow = targetRow;
            }

            // calculate some of the new state information
            toRange = this._dsRangeCalculationStgy(dsIndex, targetRow, curState);
            toState.firstSlotDsIndex = toRange.from;
            toState.lastSlotDsIndex = toRange.to - 1;
            // check selectable strategy
            selectable = this._checkSelectableStgy(curState, toState);

            if (opts.forceRender && !selectable) { // disregard selectable boolean
                console.info("Grid: forced render.");
            } else if (!selectable) {
                this._ready = true;
                return getRejectedPromiseWithMessage("Putting item at datasource index " + dsIndex + ", to row " + targetRow + ", is not posible.");
            }


            // calculate scroll steps
            scrollStep = (toState.firstSlotDsIndex - curState.firstSlotDsIndex) / this._cols;


            updateSelectionFn = function () {
                console.info("Grid: updating selection...");

                var compGrid = toState.gridComponents,
                    selectedGridRow = toState.selectedGridRow,
                    selectedGridCol = toState.selectedGridCol;

                try {
                    curState.gridComponents[curState.selectedGridRow][curState.selectedGridCol].removeClass("wgt-grid-selected");
                } catch (ex) {
                    // most likely the component has been deinited
                }
                compGrid[selectedGridRow][selectedGridCol].addClass("wgt-grid-selected");

                self._ready = true;

                if (focusMgr.isCompChildFocused(self)) {
                    focusMgr.focus(compGrid[selectedGridRow][selectedGridCol]);
                }

                if (selectable) {
                    self.setOption("forwardFocus", compGrid[selectedGridRow][selectedGridCol]);
                } else {
                    self.setOption("forwardFocus", undefined);
                }

                self.dispatchEvent(evtType.SELECTION_CHANGED, toState);
            };

            actualDsFrom = toRange.actualFrom;
            actualDsTo = toRange.actualTo;

            fail = util.wrap(getRejectedPromiseWithMessage, function (orign, msg) {
                self._ready = true;
                console.info("fail to selection changed");
                self.dispatchEvent(evtType.SELECTION_CHANGED, toState);
                return orign(msg);
            });

            scrollFn = function (fetchedDataArr) {
                dataArr = dataArr.concat(fetchedDataArr);
                toState.data = dataArr;

                return self._scroll(dataArr, scrollStep, curState, toState, opts);
            };

            if (actualDsFrom >= actualDsTo || this._rows * this._cols > dataTotalCount) { // seems like need looping
                return this._ds.getRange(actualDsFrom, dataTotalCount).then(function (data) {

                    if (actualDsFrom === 0) { // no need to fetch the other half
                        return data;
                    }

                    if (curState.rows * curState.cols > dataTotalCount) { // fetch all remaining
                        actualDsTo = actualDsFrom;
                    } else if (actualDsTo === 0) { // the other half has nothing
                        return data;
                    }

                    dataArr = dataArr.concat(data);
                    return self._ds.getRange(0, actualDsTo); // fetch the other half
                }).then(scrollFn).then(updateSelectionFn).fail(fail);
            }

            return this._ds.getRange(actualDsFrom, actualDsTo).then(scrollFn).then(updateSelectionFn).fail(fail);

        },
        /**
         * Scroll method to be called internally.
         * @method
         * @param {Array} dataArr data array retrieved from datasource
         * @param {Number} scrollStep the step number to scroll
         * @param {Object} curState the grid's current state
         * @param {Object} toState  the grid's target state
         * @param {Object} opts the options
         * @param {Boolean} opts.noAnimation set to true to turn off scrolling animation
         * @returns {Promise.<Boolean|String>} resolve with true if animation is performed, or a string message indicating the scrolling action for other cases
         * @protected
         * @memberof ax/ext/ui/grid/Grid#
         */
        _scroll: function (dataArr, scrollStep, curState, toState, opts) {
            var newRowsCount = Math.min(Math.abs(scrollStep), this._rows),
                newRowsStart = scrollStep > 0 ? this._rows - newRowsCount : 0,
                dataCount = this._ds.getTotalCount(),
                i, j, childComp, curData, isInit = !curState.gridComponents,
                newRowsComponents = [],
                gridComponents = curState.gridComponents,
                self = this,
                updateGridStateFn, actualSelectedDsIdx = mod(toState.selectedDsIndex, dataCount);

            updateGridStateFn = function () {
                // update grid state
                self._selectedDsIndex = toState.selectedDsIndex;
                self._selectedGridRow = toState.selectedGridRow;
                self._selectedGridCol = toState.selectedGridCol;
                self._firstSlotDsIndex = toState.firstSlotDsIndex;
                self._lastSlotDsIndex = toState.lastSlotDsIndex;
                self._data = toState.data;
                // ==== dom stuff ====
                self._gridComponents = toState.gridComponents;
            };

            if (isInit) { // we are initializing the grid!
                newRowsCount = this._rows;
                newRowsStart = 0;
                gridComponents = [];
            }

            // prepare the new rows' components
            for (i = newRowsStart; i < newRowsStart + newRowsCount; i++) {
                for (j = 0; j < this._cols; j++) {

                    curData = this._dataMappingStgy(curState, dataArr, i, j, toState.firstSlotDsIndex + i * this._cols + j);

                    if (!curData) {
                        continue;
                    }

                    childComp = this._displayStgy(curData);
                    childComp.getRoot().addClass("wgt-grid-item");

                    // make sure it is an array
                    newRowsComponents[i - newRowsStart] = newRowsComponents[i - newRowsStart] || [];
                    // put into new row
                    newRowsComponents[i - newRowsStart].push(childComp);
                }
            }

            // calculate the updated grid components
            if (newRowsStart === 0) {
                gridComponents = newRowsComponents.concat(gridComponents).slice(0, this._rows);
            } else {
                gridComponents = gridComponents.concat(newRowsComponents).slice(newRowsCount, this._rows + newRowsCount);
            }

            // information on the newly added rows
            toState.newRowsStart = newRowsStart;
            toState.newRowsCount = newRowsCount;
            toState.gridComponents = gridComponents;

            // dispatch scroll event
            switch (this._scrollbarBehavior) {
            case this.constructor.SCROLL_BASE_ON_ITEM:
                this.dispatchScrolledInfo(actualSelectedDsIdx, 1, dataCount);
                break;
            case this.constructor.SCROLL_BASE_ON_ROW:
                this.dispatchScrolledInfo(actualSelectedDsIdx - mod(actualSelectedDsIdx, this._cols), this._cols, dataCount);
                break;
            case this.constructor.SCROLL_BASE_ON_PAGE:
                this.dispatchScrolledInfo(mod(toState.firstSlotDsIndex, dataCount), this._cols * this._rows, dataCount);
                break;
            }

            // no need to scroll, just return
            if (!isInit && scrollStep === 0) {
                updateGridStateFn();
                return getPromiseWithMessage("No scroll needed.");
            }


            return promise.when(this._scrollEntranceStgy(this._container, scrollStep, curState, toState), function () {

                // initialization, only create elements
                if (isInit) {
                    updateGridStateFn();
                    return getPromiseWithMessage("Currently initializing grid, no shift animation will be performed.");
                }


                // no animation
                if (opts.noAnimation === true) {

                    return promise.when(self._scrollExitStgy(self._container, scrollStep, curState, toState), function () {
                        updateGridStateFn();
                        return getPromiseWithMessage("Animation is turned off, no shift animation will be performed.");
                    });
                }

                // do everything
                return self._scrollShiftStgy(self._container, scrollStep, curState, toState).then(function () {
                    self._scrollExitStgy(self._container, scrollStep, curState, toState);

                    updateGridStateFn();

                    return true;
                });

            });

        },
        /**
         * Clear and resets the grid.
         * @method
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         */
        reset: function () {
            // ==== dom stuff ====
            this._gridComponents = null;

            util.each(this._container.getChildren(), function (child) {
                child.detach();
            });

            this.setOption("forwardFocus", undefined);

            this._selectedDsIndex = 0;
            this._selectedGridRow = 0;
            this._selectedGridCol = 0;
            this._firstSlotDsIndex = 0;
            this._lastSlotDsIndex = 0;
            this._data = null;

        },
        /**
         * Tells whether this grid is ready for manipulation.
         * @method
         * @public
         * @returns {Boolean} whether this grid is ready
         * @memberof ax/ext/ui/grid/Grid#
         */
        isReady: function () {
            return this._ready;
        },
        /**
         * Key handler
         * @method
         * @private
         * @memberof ax/ext/ui/grid/Grid#
         */
        _keyHandler: function (evt) {
            if (!this._ready) {
                return false; // comsume the key
            }

            var focusRet, evtKey = evt.id,
                curFocus, parent, child, toGridCol, toGridRow, toDsIndex, scrollStep, navi, curState, selectResult, maxRow, maxCol;

            switch (evtKey) {
            case vKey.UP.id:
            case vKey.DOWN.id:
            case vKey.LEFT.id:
            case vKey.RIGHT.id:
                focusRet = focusMgr.directionalFocusChangeByKey(evt, this);
                break;
            default:
                return true;
            }

            if (focusRet) {
                return false;
            }

            curFocus = focusMgr.getCurFocus();
            child = curFocus;
            parent = child.getParent();
            while (parent && parent !== this && !child.getRoot().hasClass("wgt-grid-item")) {
                child = parent;
                parent = child.getParent();
            }

            if (!parent || parent === this) { // parent should be container
                return true;
            }

            curState = {
                selectedDsIndex: this._selectedDsIndex,
                selectedGridRow: this._selectedGridRow,
                selectedGridCol: this._selectedGridCol,
                firstSlotDsIndex: this._firstSlotDsIndex,
                lastSlotDsIndex: this._firstSlotDsIndex + this._rows * this._cols - 1,
                data: this._data,
                gridComponents: this._gridComponents,
                ds: this._ds,
                rows: this._rows,
                cols: this._cols
            };

            navi = this._keyNavigationStgy(evt.id, curState);
            navi = util.extend({
                rowChange: 0,
                colChange: 0,
                scroll: 0,
                forceRender: false
            }, navi);

            toGridRow = this._selectedGridRow + navi.rowChange;
            toGridCol = this._selectedGridCol + navi.colChange;
            scrollStep = navi.scroll;

            maxRow = this._rows;

            if (scrollStep < 0) {
                //scrolling backward
                maxCol = this._cols;
            } else {
                maxCol = this._gridComponents[this._selectedGridRow].length < this._cols ? this._gridComponents[this._selectedGridRow].length : this._cols;
            }


            // translate exessive row change into scrolling
            if (toGridRow < 0) {
                toGridRow = 0;
                scrollStep += toGridRow;
            } else if (toGridRow >= maxRow) {
                scrollStep += toGridRow - (maxRow - 1);
                toGridRow = maxRow - 1;
            }

            if (toGridCol < 0) {
                toGridCol = 0;
            } else if (toGridCol >= maxCol) {
                toGridCol = maxCol - 1;
            }

            toDsIndex = this._selectedDsIndex + navi.rowChange * this._cols + navi.scroll * this._cols + (toGridCol - this._selectedGridCol);

            if (this._selectedDsIndex === toDsIndex && this._selectedGridRow === toGridRow && this._selectedGridCol === toGridCol) {
                // no change at all
                return true;
            }

            selectResult = this.select(toDsIndex, toGridRow, {
                forceRender: navi.forceRender
            });

            selectResult.fail(rejectionHandler).done();

            return promise.isRejected(selectResult);
        },
        /**
         * Focus handler
         * @method
         * @private
         * @memberof ax/ext/ui/grid/Grid#
         */
        _onFocusHandler: function (evt) {
            if (!this._ready) {
                return;
            }

            var i = 0,
                j = 0,
                len, rowLen, rowComponents, comp, selectedDsIndex;

            // just focus child if grid gets focused
            if (evt.target === this) {
                focusMgr.focus(this._gridComponents[this._selectedGridRow][this._selectedGridCol]);
                return;
            }

            len = this._gridComponents.length;
            for (i = 0; i < len && this._gridComponents[i]; i++) {
                rowComponents = this._gridComponents[i];
                rowLen = rowComponents.length;
                for (j = 0; j < rowLen && rowComponents[j]; j++) {
                    comp = this._gridComponents[i][j];

                    if (focusMgr.isCompFocused(comp) || focusMgr.isCompChildFocused(comp)) { // found

                        // updated selection
                        this._gridComponents[this._selectedGridRow][this._selectedGridCol].removeClass("wgt-grid-selected");
                        comp.addClass("wgt-grid-selected");

                        selectedDsIndex = this._firstSlotDsIndex + i * this._cols + j;

                        //early return if it is the original selected item
                        if (i === this._selectedGridRow && j === this._selectedGridCol && this._selectedDsIndex === selectedDsIndex) {
                            return;
                        }

                        this._selectedGridRow = i;
                        this._selectedGridCol = j;
                        this._selectedDsIndex = selectedDsIndex;

                        //to set back the forwardfocus to the new component
                        this.setOption("forwardFocus", comp);
                        
                        //dispatch the event about the selection changed
                        this.dispatchEvent(evtType.SELECTION_CHANGED, {
                            selectedDsIndex: this._selectedDsIndex,
                            selectedGridRow: this._selectedGridRow,
                            selectedGridCol: this._selectedGridCol,
                            firstSlotDsIndex: this._firstSlotDsIndex,
                            lastSlotDsIndex: this._lastSlotDsIndex,
                            data: this._data,
                            gridComponents: this._gridComponents
                        });

                        return;
                    }
                }
            }
        },

        /**
         * Datasource update handler
         * @method
         * @private
         * @memberof ax/ext/ui/grid/Grid#
         */
        _onDatasourceUpdated: function (evt) {
            console.info("Grid: datasource updated!");

            if (!this._ready) {
                console.info("Grid: not yet ready to react to datasource update!");
                return;
            }

            var state, ret, selectedDsIndex, selectedGridRow, range, selectForceRender = true;

            // see if need to change anything after the data updated
            state = {
                selectedDsIndex: this._selectedDsIndex,
                selectedGridRow: this._selectedGridRow,
                selectedGridCol: this._selectedGridCol,
                firstSlotDsIndex: this._firstSlotDsIndex,
                cols: this._cols,
                rows: this._rows,
                ds: this._ds
            };

            ret = this._onUpdateStgy(evt, state, this._ds);

            if (!ret) {
                return;
            }

            selectedDsIndex = state.selectedDsIndex;
            selectedGridRow = state.selectedGridRow;

            range = this._dsRangeCalculationStgy(selectedDsIndex, selectedGridRow, state);

            this.reset();

            //forceRender is used to update the grid even no change in selectdDsIndex. But it is not needed when reset.
            if (evt.action === "reset") {
                selectForceRender = false;
            }

            this.__validateDs(range.actualFrom, range.actualTo).then(util.bind(this.select, this, selectedDsIndex, selectedGridRow, {
                forceRender: selectForceRender
            })).fail(function () {
                console.info("fail to update grid");
            }).done();
        },
        /**
         * Make sure the data and total count is available before rendering. If ds is not ready, then try to firstly fetch the data.
         * @method
         * @private
         * @memberof ax/ext/ui/grid/Grid#
         * @param {Number} from the from Index when first fetch the data
         * @param {Number} to the end Index when first fetch the data
         * @returns {Promise.<Undefined>} when data is fetched
         * @throws {Promise.<String>} "Unable to initialize the datasource, totalCount still equals -1" If the total count is still -1 after fetch
         */
        __validateDs: function (from, to) {
            //ds not ready
            if (this._ds.getTotalCount() === -1) {
                return this._ds.getRange(from, to).then(util.bind(function () {

                    if (this._ds.getTotalCount() === -1) {
                        return getRejectedPromiseWithMessage("Unable to initialize the datasource, totalCount still equals -1");
                    }

                }, this));
            }

            //reject with exception if trying to set an empty datasource;
            if (this._ds.getTotalCount() === 0) {
                console.warn("tried to set an datasource 0 total count, grid can will only be updated if it's notified by EVT_UPDATED from datasource with new total count");
            }

            return promise.resolve();
        },
        /**
         * Get the current Ds Index from the grid
         * @method
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         * @returns {Number} the current selection item dsIndex
         */
        getSelectedDsIndex: function () {
            var totalCount;
            if (this._ds) {
                totalCount = this._ds.getTotalCount();
                if (totalCount > 0) {
                    return mod(this._selectedDsIndex, totalCount);
                }
            }
            return -1;
        },
        /**
         * Get the selected component
         * @method
         * @public
         * @returns {ax/af/Component} the current selection item Component
         * @memberof ax/ext/ui/grid/Grid#
         */
        getSelectedComp: function () {
            return this._gridComponents[this._selectedGridRow][this._selectedGridCol];
        },
        /**
         * Get the selected data
         * @method getSelectedData
         * @public
         * @returns {Object} the data object inside the datasource
         * @memberof ax/ext/ui/grid/Grid#
         */
        getSelectedData: function () {
            var ds = this.getDatasource(),
                dsIndex = this.getSelectedDsIndex();

            if (ds && ds.getFetchedData && dsIndex > -1) {
                return ds.getFetchedData(dsIndex);
            }

            return null;
        },
        /**
         * Get the selected Position
         * @method getSelectedPosition
         * @returns {Object} obj the position object
         * @returns {Number} obj.row the current selected item row position
         * @returns {Number} obj.col the current selected item col position
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         */
        getSelectedPosition: function () {
            return {
                row: this._selectedGridRow,
                col: this._selectedGridCol
            };
        },
        /**
         * Get the selected component by the viewport coordinate row and col
         * @method getCompByCoor
         * @returns {ax/af/Component} the desired Component
         * @public
         * @memberof ax/ext/ui/grid/Grid#
         */
        getCompByCoor: function (row, col) {
            if (row < 0 || col < 0 || row >= this._gridComponents.length || col >= this._gridComponents[row].length) {
                return undefined;
            }

            return this._gridComponents[row][col];
        }
    });
});
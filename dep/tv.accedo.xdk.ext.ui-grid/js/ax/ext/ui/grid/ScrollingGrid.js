/**
 * The is the basic scrolling grid widget.
 *
 * ###CSS
 * ####Structural CSS
 *      // to hide the extra element
 *      .wgt-grid-scroll {
 *          overflow: hidden;
 *      }
 *      //to make the row display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-h {
 *          float: none;
 *          clear: both;
 *       }
 *       //to make the element display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-h > * {
 *          float: left;
 *      }
 *      //to make the row display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-v {
 *          float: left;
 *      }
 *      //to make the element display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-v > * {
 *          float: none;
 *           clear: both;
 *      }
 *
 * @class ax/ext/ui/grid/ScrollingGrid
 * @extends ax/ext/ui/grid/Grid
 * @param {Object} opts The options object
 * @param {Integer} opts.rows number of rows in the grid
 * @param {Integer} opts.cols number of columns in the grid
 * @param {ax/ext/ui/grid/Grid.VERTICAL|ax/ext/ui/grid/Grid.HORIZONTAL} [opts.alignment]
 *     grid item alignment direction
 */
define("ax/ext/ui/grid/ScrollingGrid", ["ax/class", "./Grid", "./gridStgy", "ax/util", "css!./css/Grid"], function (klass, Grid, gridStgy, util) {

    "use strict";
    return klass.create(Grid, {}, {
        /**
         * Overrides parent init() function.
         * @method
         * @protected
         * @memberof ax/ext/ScrollingGrid#
         */
        init: function (opts) {
            this._super(opts);

            var isVerticalAlign = opts.alignment === Grid.VERTICAL;

            this.setPrereadyStgy(gridStgy.BASIC_PREREADY_STGY);
            this.setCheckScrollableStgy(gridStgy.BASIC_CHECK_SCROLLABLE_STGY);
            this.setCheckSelectableStgy(gridStgy.BASIC_CHECK_SELECTABLE_STGY);
            this.setDataMappingStgy(gridStgy.BASIC_DATA_MAPPING_STGY);

            this.setScrollEntranceStgy(util.bind(gridStgy.BASIC_SCROLL_ENTRANCE_STGY, this, isVerticalAlign));

            this.setScrollShiftStgy(gridStgy.BASIC_SCROLL_SHIFT_STGY);
            this.setScrollExitStgy(gridStgy.BASIC_SCROLL_EXIT_STGY);

            this.setOnUpdateStgy(gridStgy.BASIC_ONUPDATE_STGY);

            this.setKeyNavigationStgy(
                isVerticalAlign ? gridStgy.BASIC_VERTICAL_KEY_NAVIGATION_STGY : gridStgy.BASIC_HORIZONTAL_KEY_NAVIGATION_STGY);

            this.setDsRangeCalculationStgy(gridStgy.BASIC_DS_RANGE_CALCULATION_STGY);
        }
    });
});
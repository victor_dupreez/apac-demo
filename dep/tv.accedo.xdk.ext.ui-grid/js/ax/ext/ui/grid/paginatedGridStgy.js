/**
 * A collection of strategies for bounded grid widget.
 * @author Marco Fan <marco.fan@accedo.tv>
 * @module ax/ext/ui/grid/paginatedGridStgy
 */
define("ax/ext/ui/grid/paginatedGridStgy", ["ax/af/Component", "ax/af/Container", "ax/device/vKey"], function (Component, Container, vKey) {
    "use strict";
    var mod = function (number, divisor) { // modulo operation
            return ((number % divisor) + divisor) % divisor;
        },
        strategies = {};

    // ======== key navigation strategies ========
    strategies.KEY_NAVIGATION_STGY = function (isVertical, key, state) {
        var curGridRow = state.selectedGridRow;

        if (isVertical) {
            switch (key) {
            case vKey.UP.id:
                return {
                    colChange: -1
                };
            case vKey.DOWN.id:
                return {
                    colChange: 1
                };
            case vKey.LEFT.id:
                if (curGridRow > 0) { // haven't reach the boundary yet
                    // stay in the same page
                    return {
                        rowChange: -1
                    };
                } else {
                    // go to previous page
                    return {
                        scroll: -state.rows,
                        rowChange: state.rows - 1
                    };
                }
                break;
            case vKey.RIGHT.id:
                if (curGridRow < state.rows - 1) { // haven't reach the boundary yet
                    // normal case: stay in the same page
                    return {
                        rowChange: 1
                    };
                } else {
                    // go to next page
                    return {
                        scroll: state.rows,
                        rowChange: 1 - state.rows
                    };
                }
                break;
            default:
                break;
            }
        } else {
            switch (key) {
            case vKey.UP.id:
                if (curGridRow > 0) { // haven't reach the boundary yet
                    // stay in the same page
                    return {
                        rowChange: -1
                    };
                } else {
                    // go to previous page
                    return {
                        scroll: -state.rows,
                        rowChange: state.rows - 1
                    };
                }
                break;
            case vKey.DOWN.id:
                if (curGridRow < state.rows - 1) { // haven't reach the boundary yet
                    // normal case: stay in the same page
                    return {
                        rowChange: 1
                    };
                } else {
                    // go to next page
                    return {
                        scroll: state.rows,
                        rowChange: 1 - state.rows
                    };
                }
                break;
            case vKey.LEFT.id:
                return {
                    colChange: -1
                };
            case vKey.RIGHT.id:
                return {
                    colChange: 1
                };
            default:
                break;
            }
        }

        return {};
    };


    // ======== onUpdate strategies ========
    strategies.ONUPDATE_STGY = function (evt, state, ds) {
        if (evt.action === "reset") {
            //no need to change state and redraw directly.
            return true;
        }
        var dataCount = ds.getTotalCount(),
            updatedFrom = evt.index,
            updatedTo = updatedFrom + evt.length,
            firstSlotDsIndex = state.firstSlotDsIndex,
            lastSlotDsIndex = state.firstSlotDsIndex + state.rows * state.cols - 1,
            currentCount = (lastSlotDsIndex > dataCount ? dataCount : lastSlotDsIndex) - firstSlotDsIndex + 1,
            lastRowCount = currentCount % state.cols,
            maxRow,
            currentRows; // number of rows in the current page

        if (lastSlotDsIndex < 0 && firstSlotDsIndex >= -dataCount && // within first negative page
            mod(firstSlotDsIndex, dataCount) >= updatedTo) { // no overlap with update
            return false;
        }
        if (firstSlotDsIndex > 0 && lastSlotDsIndex < dataCount && // within first positive page
            lastSlotDsIndex < updatedFrom) { // no overlap with update
            return false;
        }


        if (evt.action === "fetch" &&
            (updatedTo < firstSlotDsIndex || //since the changed item is not in the page (assume fetch just update the data)
                updatedFrom > lastSlotDsIndex) //the changed item is not in the page
        ) {
            return false;
        }

        // update selectedDsIndex on datasource change
        while (dataCount <= state.selectedDsIndex) {
            state.selectedDsIndex--;
        }

        maxRow = Math.floor((dataCount - 1) / state.cols);
        currentRows = Math.ceil(currentCount / state.cols);

        // update the selectedGridRow if item is removing from the Ds while the last row has only 1 item
        if (evt.action === "remove" && lastRowCount === 1) {
            // if the selected item is at the last row
            if (state.selectedGridRow === currentRows - 1) {
                state.selectedGridRow--;
                if (state.selectedGridRow < 0) {
                    state.selectedGridRow = state.rows - 1;
                }
            }
        }

        // if a large amount of items is removed suddenly while the selected item was at very later
        if (state.selectedGridRow > maxRow && maxRow < state.rows) {
            state.selectedGridRow = maxRow - 1;
        }

        return true;
    };

    return strategies;
});
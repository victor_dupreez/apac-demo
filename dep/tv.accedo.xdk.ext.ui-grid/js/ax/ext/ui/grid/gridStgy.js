/*jshint unused:false*/
/**
 * A collection of strategies for grid widget.
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @module ax/ext/ui/grid/gridStgy
 */
define("ax/ext/ui/grid/gridStgy", ["ax/console", "ax/af/Component", "ax/af/Container", "ax/device/vKey", "ax/util", "ax/promise"], function (console, Component, Container, vKey, util, promise) {
    "use strict";
    var mod = function (number, divisor) { // modulo operation
            //in wiiU 1%0 is 0, so we try to standardize the %0 which should be NaN in all the platform
            if (divisor === 0) {
                return NaN;
            }
            return ((number % divisor) + divisor) % divisor;
        },
        strategies = {};



    // ======== pre-ready strategies ========
    strategies.BASIC_PREREADY_STGY = function (state) {
        state.selectedDsIndex = 0;
        state.selectedGridRow = 0;
    };


    // ======== check scrollable strategies ========
    strategies.BASIC_CHECK_SCROLLABLE_STGY = function (curState, toState) {
        var dsCount = curState.ds.getTotalCount();
        if (toState.firstSlotDsIndex < -curState.cols || toState.firstSlotDsIndex >= dsCount + curState.cols) {
            // going out of bound, not selectable
            return false;
        }

        if (toState.selectedDsIndex >= dsCount) {
            toState.selectedDsIndex = dsCount - 1;
            toState.selectedGridCol = (dsCount - 1) % curState.cols;
        }
        return true;
    };


    // ======== check selectable strategies ========
    strategies.BASIC_CHECK_SELECTABLE_STGY = function (curState, toState) {
        var toDsIndex = toState.selectedDsIndex,
            dataCount = curState.ds.getTotalCount(),
            toDsRow, lastDsRow;

        if (toDsIndex < 0) {
            return false;
        }

        // Unknown datasource
        if (dataCount === -2) {
            return true;
        }

        if (toDsIndex < dataCount) { // witnin range
            return true;
        }

        toDsRow = Math.floor(toDsIndex / curState.cols);
        lastDsRow = Math.floor((dataCount - 1) / curState.cols);

        if (toDsRow <= lastDsRow) { // we can still go to that row
            toState.selectedDsIndex = dataCount - 1;
            toState.selectedGridCol = mod(dataCount - 1, this._cols);
            return true;
        }

        return false;
    };

    // ======== data mapping strategies ========
    strategies.BASIC_DATA_MAPPING_STGY = function (curState, dataArr, gridRow, gridCol, dsIndex) {
        if (dsIndex < 0 || dsIndex >= curState.ds.getTotalCount()) {
            return null;
        }
        return dataArr[gridRow * curState.cols + gridCol];
    };


    // ======== scroll entrance strategies ========
    // first param determines the item alignments
    // first param should be pre-detemined using util.bind()
    strategies.BASIC_SCROLL_ENTRANCE_STGY = function (verticalAlign, container, scrollStep, curState, toState) {
        var newRowsComponents = toState.gridComponents.slice(toState.newRowsStart, toState.newRowsStart + toState.newRowsCount),
            newRowsCount = toState.newRowsCount,
            i, j, rowComponent, rowChildren, placement, marker,

            // REMOVE    
            children = container.getChildren(),
            len;
        //only deinit when there are children
        if (children.length > 0) {
            //throw all the child when the step is larger than the size
            if (scrollStep > children.length || -scrollStep > children.length) {
                for (i = 0; i < children.length && children[i]; i++) {
                    children[i].deinit();
                }
            } else if (scrollStep > 0) {
                // scrolling downwards, top rows need to be removed
                for (i = 0; i < scrollStep && children[i]; i++) {
                    // throw away child
                    children[i].deinit();
                }
            } else {
                // scrolling upwards, bottoom rows need to be removed
                len = children.length;
                for (i = len + scrollStep; i < len && children[i]; i++) {
                    // throw away child
                    children[i].deinit();
                }
            }
        }

        children = container.getChildren();

        // ADD
        for (i = 0; i < newRowsCount; i++) {
            rowChildren = [];
            for (j = 0; j < curState.cols; j++) {
                if (!newRowsComponents[i] || !newRowsComponents[i][j] || !newRowsComponents[i][j].getRoot()) {
                    break;
                }
                rowChildren.push(newRowsComponents[i][j]);
            }
            rowComponent = new Container({
                css: verticalAlign ? "wgt-grid-row-v" : "wgt-grid-row-h",
                children: rowChildren
            });

            if (children.length > 0 && scrollStep < 0 && curState.gridComponents && curState.gridComponents[0]) {
                placement = Component.PLACE_BEFORE;
                marker = curState.gridComponents[0][0].getParent();
            } else {
                placement = Component.PLACE_APPEND;
                marker = null;
            }

            container.attach(rowComponent, placement, marker);
        }

    };


    // ======== scroll shift strategies ========
    strategies.BASIC_SCROLL_SHIFT_STGY = function (container, scrollStep, curState, toState) {
        var message = "grid shifting...... do nothing for basic grid.";
        console.info(message);
        return promise.resolve(message);
    };


    // ======== scroll exit strategies ========
    strategies.BASIC_SCROLL_EXIT_STGY = function (container, scrollStep, curState, toState) {
        var message = "grid exiting shift, remove items is already done in entrance function...... do nothing for basic grid.";
        console.info(message);
        return promise.resolve(message);
    };


    // ======== key navigation strategies ========
    strategies.BASIC_HORIZONTAL_KEY_NAVIGATION_STGY = function (key, curState) {
        switch (key) {

        case vKey.UP.id:
            return {
                rowChange: -1
            };
        case vKey.DOWN.id:
            return {
                rowChange: 1
            };
        case vKey.LEFT.id:
            return {
                colChange: -1
            };
        case vKey.RIGHT.id:
            return {
                colChange: 1
            };
        default:
            return;
        }
    };
    strategies.BASIC_VERTICAL_KEY_NAVIGATION_STGY = function (key, curState) {
        switch (key) {

        case vKey.UP.id:
            return {
                colChange: -1
            };
        case vKey.DOWN.id:
            return {
                colChange: 1
            };
        case vKey.LEFT.id:
            return {
                rowChange: -1
            };
        case vKey.RIGHT.id:
            return {
                rowChange: 1
            };
        default:
            return;

        }
    };

    // ======== onUpdate strategies ========
    strategies.BASIC_ONUPDATE_STGY = function (evt, state, ds) {
        if (evt.action === "reset") {
            //no need to change state and redraw directly.
            return true;
        }

        var dataCount = ds.getTotalCount(),
            updatedFrom = evt.index,
            updatedTo = updatedFrom + evt.length,
            firstSlotDsIndex = state.firstSlotDsIndex,
            lastSlotDsIndex = state.firstSlotDsIndex + state.rows * state.cols - 1,
            maxRow, currentRow;

        if (lastSlotDsIndex < 0 && firstSlotDsIndex >= -dataCount && // within first negative page
            mod(firstSlotDsIndex, dataCount) >= updatedTo) { // no overlap with update
            return false;
        }
        if (firstSlotDsIndex > 0 && lastSlotDsIndex < dataCount && // within first positive page
            lastSlotDsIndex < updatedFrom) { // no overlap with update
            return false;
        }

        if (evt.action === "fetch" &&
            (updatedTo < firstSlotDsIndex || //since the changed item is not in the page (assume fetch just update the data)
                updatedFrom > lastSlotDsIndex) //the changed item is not in the page
        ) {
            return false;
        }

        //update selectedDsIndex on datasource change
        while (dataCount <= state.selectedDsIndex) {
            state.selectedDsIndex--;
        }


        maxRow = Math.floor((dataCount - 1) / state.cols);
        currentRow = Math.floor(dataCount - firstSlotDsIndex) / state.cols;

        //update the selectedDsIndex when it focus on the last page and remove action, it will move the focus to the previous row in some cases.
        if (evt.action === "remove" && maxRow >= state.rows - 1 && state.selectedGridRow < state.rows - 1 && currentRow <= state.rows - 1) {
            state.selectedDsIndex = state.selectedDsIndex - state.cols;
        }

        if (state.selectedGridRow > maxRow && maxRow < state.rows) {
            state.selectedGridRow = maxRow;
        }

        return true;
    };

    // ======== ds range calculation strategies ========
    strategies.BASIC_DS_RANGE_CALCULATION_STGY = function (dsIndex, targetRow, curState) {
        var result = {},
            targetGridCol = mod(dsIndex, curState.cols),
            dataTotalCount = curState.ds.getTotalCount();

        result.from = dsIndex - (targetRow * curState.cols) - targetGridCol;
        result.to = result.from + curState.rows * curState.cols;

        //since the total count is -1 (which is not ready, the actualFrom and actualTo should be the same as from/to.)
        if (dataTotalCount === -1) {
            result.actualFrom = result.from;
            result.actualTo = result.to;
            return result;
        }

        result.actualFrom = mod(result.from, dataTotalCount);
        result.actualTo = mod(result.actualFrom + curState.rows * curState.cols, dataTotalCount);

        return result;
    };

    return strategies;
});
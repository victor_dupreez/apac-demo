/**
 *
 * The is a looped grid widget with a 'scrolling boundary' at both ends.
 * ###CSS
 * ####Structural CSS
 *     // to hide the extra element
 *     .wgt-grid-scroll {
 *          overflow: hidden;
 *      }
 *      //to make the row display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-h {
 *          float: none;
 *          clear: both;
 *      }
 *      //to make the element display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-h > * {
 *          float: left;
 *      }
 *      //to make the row display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-v {
 *          float: left;
 *      }
 *      //to make the element display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-v > * {
 *          float: none;
 *          clear: both;
 *      }
 *
 * @class ax/ext/ui/grid/LoopedGrid
 * @extends ax/ext/ui/grid/Grid
 * @param {Object} opts The options object
 * @param {Integer} opts.rows number of rows in the grid
 * @param {Integer} opts.cols number of columns in the grid
 * @param {ax/ext/ui/grid/Grid} [opts.alignment grid] item alignment direction
 * @param {Integer} [opts.scrollFrontBoundary]
 *     the boundray at the beginning of the grid, to start scrolling instead of moving selection
 * @param {Integer} [opts.scrollEndBoundary]
 *     the boundray at the end of the grid, to start scrolling instead of moving selection
 */
define("ax/ext/ui/grid/LoopedGrid", ["ax/class", "./Grid", "./gridStgy", "./loopedGridStgy", "ax/util", "ax/af/data/Datasource", "ax/af/data/FiniteDatasource", "ax/promise", "ax/core", "ax/exception", "ax/console", "css!./css/Grid"], function (klass, Grid, gridStgy, loopedGridStgy, util, Datasource, FiniteDatasource, promise, core, exception, console) {
    "use strict";
    var mod = function (number, divisor) { // modulo operation
            //in wiiU 1%0 is 0, so we try to standardize the %0 which should be NaN in all the platform
            if (divisor === 0) {
                return NaN;
            }
            return ((number % divisor) + divisor) % divisor;
        },
        getRejectedPromiseWithMessage = function (message) {
            console.info(message);
            return promise.reject(message);
        };

    return klass.create(Grid, {}, {
        __isFiniteDatasource: false,
        /**
         * Overrides parent init() function.
         * @method
         * @protected
         * @memberof ax/ext/LoopedGrid#
         */
        init: function (opts) {
            this._super(opts);

            var isVerticalAlign = opts.alignment === Grid.VERTICAL,
                frontBoundary = opts.scrollFrontBoundary || 0,
                endBoundary = opts.scrollEndBoundary || 0;

            this.setPrereadyStgy(util.bind(loopedGridStgy.LOOPED_PREREADY_STGY, this, frontBoundary));
            this.setCheckScrollableStgy(loopedGridStgy.LOOPED_CHECK_SCROLLABLE_STGY);
            this.setCheckSelectableStgy(util.bind(loopedGridStgy.LOOPED_CHECK_SELECTABLE_STGY, this, frontBoundary, endBoundary));
            this.setDataMappingStgy(loopedGridStgy.LOOPED_DATA_MAPPING_STGY);

            this.setScrollEntranceStgy(util.bind(gridStgy.BASIC_SCROLL_ENTRANCE_STGY, this, isVerticalAlign));

            this.setScrollShiftStgy(gridStgy.BASIC_SCROLL_SHIFT_STGY);
            this.setScrollExitStgy(gridStgy.BASIC_SCROLL_EXIT_STGY);


            this.setOnUpdateStgy(this.__onUpdateStgy);
            this.setKeyNavigationStgy(
                util.bind(loopedGridStgy.LOOPED_BOUNDARY_KEY_NAVIGATION_STGY, this, isVerticalAlign, frontBoundary, endBoundary));

            this.setDsRangeCalculationStgy(gridStgy.BASIC_DS_RANGE_CALCULATION_STGY);
        },
        /**
         * Sets the on data update strategy. It will use the looped onupdate strategy which use the finite datasource
         * @method  __onUpdateStgy
         * @private
         * @memberof ax/ext/ui/grid/LoopedGrid#
         */
        __onUpdateStgy: function () {
            if (this.__isFiniteDatasource) {
                return loopedGridStgy.LOOPED_FINITEDS_ONUPDATE_STGY.apply(this, arguments);
            }

            return loopedGridStgy.LOOPED_ONUPDATE_STGY.apply(this, arguments);
        },
        /**
         * Sets the finite datasource for this grid, also renders the grid for the first time.
         * @method setDatasource
         * @param {ax/af/data/FiniteDatasource} ds datasource to set
         * @returns {Promise.<Undefined>} no actual resolve value
         * @throws {Promise.<String>} "Unable to set set the same datasource" If the datasource is the same
         * @throws {Promise.<String>} "Cannot set an invalid datasource" If the parameter ds is not datasource type
         * @throws {Promise.<String>} "Unable to initialize the datasource, totalCount still equals -1" If the datasource total count is -1 which is not initialized
         * @public
         * @memberof ax/ext/ui/grid/LoopedGrid#
         */
        setDatasource: function (ds) {

            if (!(ds instanceof Datasource)) {
                return getRejectedPromiseWithMessage("Cannot set an invalid datasource");
            }

            //If the given datasource is already registered for this grid, do nothing
            if (this._ds === ds) {
                return getRejectedPromiseWithMessage("Unable to set set the same datasource");
            }

            if (this.__isFiniteDatasource) {
                //Remove the previous datasource, if any
                if (this._ds && this.__datasourceListener) {
                    this._ds.removeEventListener(FiniteDatasource.EVT_FINITEDS_UPDATED, this.__datasourceListener);
                }
            } else {
                //Remove the previous datasource, if any
                if (this._ds && this.__datasourceListener) {
                    this._ds.removeEventListener(Datasource.EVT_UPDATED, this.__datasourceListener);
                }

            }

            //Remove all the current children
            this.reset();

            this._ready = false;
            //Register the new datasource
            this._ds = ds;

            if (ds instanceof FiniteDatasource) {
                this.__isFiniteDatasource = true;
            } else {
                this.__isFiniteDatasource = false;
            }

            var range, state;

            // see if need to change anything
            state = {
                selectedDsIndex: this._selectedDsIndex,
                selectedGridRow: this._selectedGridRow,
                cols: this._cols,
                rows: this._rows,
                ds: this._ds
            };

            this._prereadyStgy(state);

            this._selectedDsIndex = state.selectedDsIndex;
            this._selectedGridRow = state.selectedGridRow;

            range = this._dsRangeCalculationStgy(this._selectedDsIndex, this._selectedGridRow, state);

            // updated the grid state in case strategy have updated it
            this._selectedGridCol = mod(this._selectedDsIndex, this._cols);
            this._firstSlotDsIndex = range.from;


            //private listner ref for removal
            if (!this.__datasourceListener) {
                this.__datasourceListener = util.bind(this._onDatasourceUpdated, this);
            }

            //private listner ref for removal
            if (this.__isFiniteDatasource) {
                this._ds.addEventListener(FiniteDatasource.EVT_FINITEDS_UPDATED, this.__datasourceListener);

                if (this._ds.getTotalCount() === -1) {
                    return promise.reject(core.createException(exception.ILLEGAL_STATE, "Datasource Total count is not set."));
                }

                return this.select(this._selectedDsIndex, this._selectedGridRow);
            }

            //listen for more the data source if there is any update
            this._ds.addEventListener(Datasource.EVT_UPDATED, this.__datasourceListener);



            //ds not ready
            if (this._ds.getTotalCount() === -1) {
                return this._ds.getRange(range.from, range.to).then(util.bind(function () {

                    if (this._ds.getTotalCount() === -1) {
                        return getRejectedPromiseWithMessage("Unable to initialize the datasource, totalCount still equals -1");
                    }

                    return this.select(this._selectedDsIndex, this._selectedGridRow);

                }, this));
            }

            //reject with exception if trying to set an empty datasource;
            if (this._ds.getTotalCount() === 0) {
                console.warn("tried to set an datasource 0 total count, grid can will only be updated if it's notified by EVT_UPDATED from datasource with new total count");
            }

            return this.select(this._selectedDsIndex, this._selectedGridRow);

        }
    });
});
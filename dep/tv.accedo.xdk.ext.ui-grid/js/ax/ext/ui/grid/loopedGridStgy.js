/**
 * A collection of strategies for looped grid widget.
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @module ax/ext/ui/grid/loopedGridStgy
 */
define("ax/ext/ui/grid/loopedGridStgy", ["ax/device/vKey", "ax/af/data/interface/Datasource"], function (vKey, IDatasource) {
    "use strict";
    var mod = function (number, divisor) { // modulo operation
            return ((number % divisor) + divisor) % divisor;
        },
        strategies = {};

    // ======== pre-ready strategies ========
    strategies.LOOPED_PREREADY_STGY = function (frontBoundary, state) {
        state.selectedDsIndex = frontBoundary * state.cols;
        state.selectedGridRow = frontBoundary;
    };
    /*jshint unused:false*/
    // ======== check scrollable strategies ========
    strategies.LOOPED_CHECK_SCROLLABLE_STGY = function (curState, toState) {
        return true;
    };
    /*jshint unused:true*/
    // ======== onupdate strategies ========
    strategies.LOOPED_ONUPDATE_STGY = function (evt, state, ds) {
        if (evt.action === "reset") {
            //no need to change state and redraw directly.
            return true;
        }
        var dataCount = ds.getTotalCount(),
            firstSlotDsIndex = state.firstSlotDsIndex,
            lastSlotDsIndex = firstSlotDsIndex + state.cols * state.rows - 1,
            updatedFrom = evt.index,
            updatedTo = evt.index + evt.length;

        //when it is fetch and no looped and updatedTo is both less than the first item and last item (assume no total count change)
        if (evt.action === "fetch" &&
            dataCount > state.cols * state.rows &&
            ((updatedTo < mod(firstSlotDsIndex, dataCount) &&
                    updatedTo < mod(lastSlotDsIndex, dataCount)) ||
                (updatedFrom > mod(firstSlotDsIndex, dataCount) &&
                    updatedFrom > mod(lastSlotDsIndex, dataCount)))
        ) {
            return false;
        }

        //since it is loop and there are always has item at the position unless it is empty
        state.selectedDsIndex = firstSlotDsIndex + state.selectedGridRow * state.cols + state.selectedGridCol;

        //no item inside then return
        if (dataCount === 0) {
            state.selectedGridRow = -1;
            state.selectedDsIndex = -1;
        }

        //to ensure after remove all and insert to run the preready stgy which then set the correct first selected index and thus focus on the right item
        if (evt.action === "insert" && dataCount === evt.newData.length) {
            this._prereadyStgy(state);
        }
        return true;
    };
    // ======== onupdate for finite datasource strategies ========
    strategies.LOOPED_FINITEDS_ONUPDATE_STGY = function (evt, state, ds) {

        if (evt.action === IDatasource.ACTION.RESET) {
            //no need to change state and redraw directly.
            return true;
        }
        var dataCount = ds.getTotalCount(),
            firstSlotDsIndex = state.firstSlotDsIndex,
            lastSlotDsIndex = firstSlotDsIndex + state.cols * state.rows - 1,
            updatedFrom,
            updatedTo,
            i, segments = [],
            modFirstSlotDsIndex,
            modLastSlotDsIndex;

        // perform checking when the datacount is larger than the screen size which may not appear on the screen
        if (evt.action === IDatasource.ACTION.FETCH && dataCount > state.cols * state.rows) {
            segments = evt.segments;
            //check if updatedFrom updatedTo is outside the first item and last item
            for (i = 0; i < segments.length; i++) {

                updatedFrom = segments[i].index;
                updatedTo = updatedFrom + segments[i].length;
                modFirstSlotDsIndex = mod(firstSlotDsIndex, dataCount);
                modLastSlotDsIndex = mod(lastSlotDsIndex, dataCount);

                if ((modFirstSlotDsIndex > modLastSlotDsIndex && updatedFrom < modFirstSlotDsIndex && updatedFrom > modLastSlotDsIndex &&
                        updatedTo > modLastSlotDsIndex && updatedTo < modFirstSlotDsIndex) ||
                    (modFirstSlotDsIndex < modLastSlotDsIndex && updatedFrom > modFirstSlotDsIndex && updatedFrom > modLastSlotDsIndex &&
                        updatedTo > modFirstSlotDsIndex && updatedTo > modLastSlotDsIndex)) {
                    return false;
                }

            }
        }

        //since it is loop and there are always has item at the position unless it is empty
        state.selectedDsIndex = firstSlotDsIndex + state.selectedGridRow * state.cols + state.selectedGridCol;

        //no item inside then return
        if (dataCount === 0) {
            state.selectedGridRow = -1;
            state.selectedDsIndex = -1;
        }

        //to ensure after remove all and insert to run the preready stgy which then set the correct first selected index and thus focus on the right item
        if (evt.action === IDatasource.ACTION.INSERT && dataCount === evt.newData.length) {
            this._prereadyStgy(state);
        }
        return true;
    };
    /*jshint unused:false*/
    // ======== check selectable strategies ========
    strategies.LOOPED_CHECK_SELECTABLE_STGY = function (frontBoundary, endBoundary, curState, toState) {
        //if no more item then it is not selectable
        if (curState.ds.getTotalCount() <= 0) {
            return false;
        }
        return true;
    };
    // ======== data mapping strategies ========
    strategies.LOOPED_DATA_MAPPING_STGY = function (curState, dataArr, gridRow, gridCol, dsIndex) {
        var idx = mod(gridRow * curState.cols + gridCol, dataArr.length);
        return dataArr[idx];
    };
    /*jshint unused:true*/
    // ======== key navigation strategies ========
    strategies.LOOPED_BOUNDARY_KEY_NAVIGATION_STGY = function (isVertical, frontBoundary, endBoundary, key, state) {
        var rowStep, curDsIndex = state.selectedDsIndex,
            dataCount = state.ds.getTotalCount(),
            curGridRow = state.selectedGridRow,
            toGridRow, lastDsRow, toDsRow;

        if (isVertical) {
            switch (key) {
            case vKey.UP.id:
                return {
                    colChange: -1
                };
            case vKey.DOWN.id:
                return {
                    colChange: 1
                };
            case vKey.LEFT.id:
                rowStep = -1;
                break;
            case vKey.RIGHT.id:
                rowStep = 1;
                break;
            default:
                break;
            }
        } else {
            switch (key) {
            case vKey.UP.id:
                rowStep = -1;
                break;
            case vKey.DOWN.id:
                rowStep = 1;
                break;
            case vKey.LEFT.id:
                return {
                    colChange: -1
                };
            case vKey.RIGHT.id:
                return {
                    colChange: 1
                };
            default:
                break;
            }
        }


        if (frontBoundary <= 0 && endBoundary <= 0) { // no boundary actually
            return {
                rowChange: rowStep
            };
        }

        toGridRow = curGridRow + rowStep;
        toDsRow = Math.floor(curDsIndex / state.cols) + rowStep;
        lastDsRow = Math.floor((dataCount - 1) / state.cols);

        // inside the forbidden boundary
        if (toGridRow < frontBoundary || toGridRow >= state.rows - endBoundary) {
            return {
                scroll: rowStep
            };
        }

        return {
            rowChange: rowStep
        };
    };


    return strategies;
});
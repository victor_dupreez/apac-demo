/**
 *
 * The is a paginated grid
 *
 * @class ax/ext/ui/grid/PaginatedGrid
 * @extends ax/ext/ui/grid/Grid
 * @param {Object} opts The options object
 * @param {Integer} opts.rows number of rows in the grid
 * @param {Integer} opts.cols number of columns in the grid
 * @param {ax/ext/ui/grid/Grid} [opts.alignment] item alignment direction
 *
 */
define("ax/ext/ui/grid/PaginatedGrid", ["ax/class", "./Grid", "./gridStgy", "./paginatedGridStgy", "ax/util", "css!./css/Grid"], function (klass, Grid, gridStgy, paginatedGridStgy, util) {

    "use strict";
    return klass.create(Grid, {}, {
        /**
         * Overrides parent init() function.
         * @method
         * @protected
         * @memberof ax/ext/PaginatedGrid#
         */
        init: function (opts) {
            this._super(opts);

            var isVerticalAlign = opts.alignment === Grid.VERTICAL;

            this.setPrereadyStgy(gridStgy.BASIC_PREREADY_STGY);
            this.setCheckScrollableStgy(gridStgy.BASIC_CHECK_SCROLLABLE_STGY);
            this.setCheckSelectableStgy(util.bind(gridStgy.BASIC_CHECK_SELECTABLE_STGY, this));
            this.setDataMappingStgy(gridStgy.BASIC_DATA_MAPPING_STGY);

            this.setScrollEntranceStgy(util.bind(gridStgy.BASIC_SCROLL_ENTRANCE_STGY, this, isVerticalAlign));

            this.setScrollShiftStgy(gridStgy.BASIC_SCROLL_SHIFT_STGY);
            this.setScrollExitStgy(gridStgy.BASIC_SCROLL_EXIT_STGY);

            this.setOnUpdateStgy(paginatedGridStgy.ONUPDATE_STGY);

            this.setKeyNavigationStgy(util.bind(paginatedGridStgy.KEY_NAVIGATION_STGY, this, isVerticalAlign));

            this.setDsRangeCalculationStgy(gridStgy.BASIC_DS_RANGE_CALCULATION_STGY);
        }
    });
});
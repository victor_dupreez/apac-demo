/**
 *
 * The is a scrolling grid widget with a "scrolling boundary" at both ends.
 * ###CSS
 * ####Structural CSS
 *      // to hide the extra element
 *      .wgt-grid-scroll {
 *          overflow: hidden;
 *      }
 *      //to make the row display correctly
 *     .wgt-grid-scroll > .wgt-grid-row-h {
 *         float: none;
 *         clear: both;
 *      }
 *      //to make the element display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-h > * {
 *           float: left;
 *       }
 *       //to make the row display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-v {
 *          float: left;
 *      }
 *      //to make the element display correctly
 *      .wgt-grid-scroll > .wgt-grid-row-v > * {
 *          float: none;
 *          clear: both;
 *      }
 *
 * @class ax/ext/ui/grid/BoundedGrid
 * @extends ax/ext/ui/grid/Grid
 * @param {Object} opts The options object
 * @param {Integer} opts.rows number of rows in the grid
 * @param {Integer} opts.cols number of columns in the grid
 * @param {ax/ext/ui/grid/Grid} [opts.alignment grid] item alignment direction
 * @param {Integer} [opts.scrollFrontBoundary]
 *     the boundray at the beginning of the grid, to start scrolling instead of moving selection
 * @param {Integer} [opts.scrollEndBoundary]
 *     the boundray at the end of the grid, to start scrolling instead of moving selection
 */
define("ax/ext/ui/grid/BoundedGrid", ["ax/class", "./Grid", "./gridStgy", "./boundedGridStgy", "ax/util", "css!./css/Grid"], function (klass, Grid, gridStgy, boundedGridStgy, util) {
    "use strict";
    return klass.create(Grid, {}, {
        /**
         * Overrides parent init() function.
         * @method
         * @protected
         * @memberof ax/ext/BoundedGrid#
         */
        init: function (opts) {
            this._super(opts);

            var isVerticalAlign = opts.alignment === Grid.VERTICAL,
                frontBoundary = opts.scrollFrontBoundary || 0,
                endBoundary = opts.scrollEndBoundary || 0;

            this.setPrereadyStgy(gridStgy.BASIC_PREREADY_STGY);
            this.setCheckScrollableStgy(gridStgy.BASIC_CHECK_SCROLLABLE_STGY);
            this.setCheckSelectableStgy(util.bind(boundedGridStgy.SCROLL_BOUNDARY_CHECK_SELECTABLE_STGY, this, frontBoundary, endBoundary));
            this.setDataMappingStgy(gridStgy.BASIC_DATA_MAPPING_STGY);

            this.setScrollEntranceStgy(util.bind(gridStgy.BASIC_SCROLL_ENTRANCE_STGY, this, isVerticalAlign));

            this.setScrollShiftStgy(gridStgy.BASIC_SCROLL_SHIFT_STGY);
            this.setScrollExitStgy(gridStgy.BASIC_SCROLL_EXIT_STGY);

            this.setOnUpdateStgy(util.bind(boundedGridStgy.ONUPDATE_STGY, this, frontBoundary, endBoundary));

            this.setKeyNavigationStgy(
                util.bind(boundedGridStgy.SCROLL_BOUNDARY_KEY_NAVIGATION_STGY, this, isVerticalAlign, frontBoundary, endBoundary));

            this.setDsRangeCalculationStgy(gridStgy.BASIC_DS_RANGE_CALCULATION_STGY);
        }
    });
});
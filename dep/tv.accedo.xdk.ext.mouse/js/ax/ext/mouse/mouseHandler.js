/**
 * Mouse handler, works with {@link ax/af/mvc/AppRoot} internally.
 * It provide the mouse handling for the application.
 *
 * When the mouse is on (which triggers by onmouseon event or other handler), it will allow the mouse to focus item.
 * When the mouse is off (which tiggers by mouseoff or keydown event), it will turn the mouse handler off.
 * e.g when pressing a key on remote, it will off the mouse handler without any key handling (that means no directional change).
 *
 * ###Config Params
 *
 *  Attribute | Value
 * --------- | ---------
 * Key      | mouse.keep-focus
 * Type     | Boolean
 * Desc     |  Whether to keep focus when mouse on or focus on blank item.
 *             When it is false, it will blur the current focus when using mouse on the non-focusable area.
 *             When it is true, it will keep the current focus until mouse is on the another focusable item.
 * Default  |  false
 * Usage    | {@link module:ax/config}
 *
 *
 * Handles:
 *
 * * mouse on/off focus changes
 * * mouse over focusable component, will focus component
 * * mouse over non-focusable component, will blur last focus
 * * mouse wheel
 *
 * @module ax/ext/mouse/mouseHandler
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/ext/mouse/mouseHandler", [
    "ax/core",
    "ax/af/focusManager",
    "ax/Env",
    "ax/device",
    "ax/util",
    "ax/af/mvc/AppRoot",
    "ax/af/evt/type",
    "ax/console",
    "ax/device/interface/Id",
    "ax/config"
], function (
    core,
    fm,
    Env,
    device,
    util,
    AppRoot,
    evtType,
    console,
    IId,
    config
) {
    "use strict";
    var sEnv,
        sAppRoot,
        mouseOnClass = "mouse-on",
        nonFocusableClass = "mouse-focus-on-non-focusable",
        keepFocus,
        rootEle = null,
        mouseOffDelayHandle = null,
        isMouseOn = false,
        origMouseOnHandler = null,
        origMouseOffHandler = null,
        webosMouseOnOffHandler = null,
        webosBlockKey = false,
        //keycode for determining mouseon/mouseoff on webos
        WEBOS_MOUSE_ON = 1536,
        WEBOS_MOUSE_OFF = 1537,
        mouseOnCallback = null,
        mouseOffCallback = null,
        mouseOverCallback = null,
        doFocus, setMouseOnOff, mouseOnHandler, mouseOffHandler, mouseOverHandler,
        registerMouseOnOff, onCursorUpdate, mouseOffDelayFn, unregisterMouseOnOff,
        registerMouseOver, unregisterMouseOver,
        wiiCursorOnScreen = false;

    setMouseOnOff = function (bool) {
        var newIsMouseOn = bool;

        if (newIsMouseOn === isMouseOn) {
            return;
        }

        isMouseOn = newIsMouseOn;

        if (isMouseOn) {
            getRootView().addClass(mouseOnClass);
        } else {
            getRootView().removeClass(mouseOnClass);
        }

        doFocus();
    };

    doFocus = function (component) {

        var focused = false;

        getRootView().removeClass(nonFocusableClass);

        //when mouse on
        if (isMouseOn) {
            //existing component
            if (component) {
                focused = fm.focus(component, {
                    skipForwardFocus: true
                });

                if (focused) {
                    return;
                }
            }

            if (!keepFocus) {
                fm.focus(null);
                return;
            }

            //when it keeps focus and mouse is on the non-focusable component/null, add a class to classify.
            getRootView().addClass(nonFocusableClass);

        }

        fm.ensureFocus();
    };

    mouseOnHandler = function () {
        var callbackResult;
        // run the callback if exist
        if (mouseOnCallback) {
            callbackResult = mouseOnCallback();
            if (callbackResult === false) {
                return true;
            }
        }

        setMouseOnOff(true);
    };

    mouseOffHandler = function () {
        if (!isMouseOn) {
            // don"t even call callback when mouse is already off
            // so repeated key strokes will not result in repeated mouse off callback
            return true;
        }

        var callbackResult;
        // run the callback if exist
        if (mouseOffCallback) {
            callbackResult = mouseOffCallback();
            if (callbackResult === false) {
                return true;
            }
        }

        setMouseOnOff(false);
        return false;
    };

    mouseOverHandler = function (evt) {
        //IE doesn't pass in the event object
        evt = evt || window.event;
        var target = evt.target || evt.srcElement,
            component = target.__comp,
            callbackResult;

        if (getEnv().isBlockingMouse()) {
            return true;
        }

        //mouseover and no setMouseOnOff
        if (!isMouseOn) {
            getRootView().addClass(mouseOnClass);
        }

        isMouseOn = true;

        while (!component) {
            target = target.parentNode;

            if (!target || target === document.body) {
                break;
            }
            component = target.__comp;
        }

        // invalid event, target does not belong to a component
        if (!component) {
            doFocus(null);
            return true;
        }

        //find the right component
        while (component && !fm.isCompFocusable(component)) {
            component = component.getParent();
        }

        // run the callback if exist
        if (mouseOverCallback) {
            callbackResult = mouseOverCallback(component);
            if (callbackResult === false) {
                return true;
            }
        }

        // target is not focusable
        if (!component) {
            doFocus(null);
            return true;
        }

        doFocus(component);
        return true;
    };
    webosMouseOnOffHandler = function (keyEvent) {
        var keycode = keyEvent.keyCode,
            consumed = false;

        switch (keycode) {
            //mouse on
        case WEBOS_MOUSE_ON:
            mouseOnHandler();
            consumed = true;
            break;
            //mouse off
        case WEBOS_MOUSE_OFF:
            mouseOffHandler();
            consumed = true;
            webosBlockKey = true;
            //temporary block the keys,
            //on the TV, it will consecutive dispatch 2 events(1537 and original key event) when key down
            //try to block the second key event in order the keep focus without any direction change.
            util.defer().then(function () {
                webosBlockKey = false;
            }).done();
            break;
        }

        //stop the event to be processed in the keydown/keypress in tvkey
        if (consumed || webosBlockKey) {
            keyEvent.stopPropagation();
            keyEvent.preventDefault();
            return false;
        }

        return true;
    };
    registerMouseOnOff = function () {
        // special handling for LG mouse on/off events
        // notice, LG 2011 emulator has bug preventing window.mouseoff to work
        /**
         * @TODO use config to opt-out this logic
         */

        if (device.platform === "webos") {
            document.addEventListener("keydown", webosMouseOnOffHandler, true);
            return;
        }

        if (device.platform === "lg" && !(device.id.getFirmwareYear() <= 2011 && device.id.getHardwareType() === IId.HARDWARE_TYPE.EMULATOR)) {
            origMouseOnHandler = window.onmouseon;
            origMouseOffHandler = window.onmouseoff;
            window.onmouseon = mouseOnHandler;
            window.onmouseoff = mouseOffHandler;
            return;
        }

        //only add on tv console only
        if (device.platform === "wiiu" && nwf && window === nwf.display.DisplayManager.singleton().getTVDisplay().window) {
            // Wii controllers
            var wiiRemotes = [],
                key, remote;

            wiiRemotes[0] = nwf.input.WiiRemote.getController(nwf.input.WiiRemote.REMOTE_1);
            wiiRemotes[1] = nwf.input.WiiRemote.getController(nwf.input.WiiRemote.REMOTE_2);
            wiiRemotes[2] = nwf.input.WiiRemote.getController(nwf.input.WiiRemote.REMOTE_3);
            wiiRemotes[3] = nwf.input.WiiRemote.getController(nwf.input.WiiRemote.REMOTE_4);

            //mouseon
            for (key in wiiRemotes) {
                remote = wiiRemotes[key];
                remote.cursor.addEventListener(nwf.events.DPDControlEvent.UPDATE, onCursorUpdate);
            }

            //mouseoff
            mouseOffHandler = util.wrap(mouseOffHandler, function (orign) {
                if (wiiCursorOnScreen) {
                    //do nth when it is still on screen
                    return true;
                }
                return orign();
            });
        }
        getRootView().addEventListener(evtType.KEY, mouseOffHandler, true);
        return;
    };
    onCursorUpdate = function (e) {
        wiiCursorOnScreen = true;
        if (!isMouseOn) {
            console.info("[Wii]Mouse ON with the evt" + e);
            mouseOnHandler();
        }
        if (mouseOffDelayHandle) {
            util.clearDelay(mouseOffDelayHandle);
        }

        mouseOffDelayHandle = core.getGuid();

        util.delay(0.2, mouseOffDelayHandle).then(mouseOffDelayFn).done();
    };
    mouseOffDelayFn = function () {
        //to check if it is outside the screen. If it is outside the screen, we can do the mouse off handling
        console.info("[Wii]Mouse off outside the screen");
        wiiCursorOnScreen = false;
        mouseOffDelayHandle = null;
    };
    unregisterMouseOnOff = function () {

        if (device.platform === "webos") {
            document.removeEventListener("keydown", webosMouseOnOffHandler);
            return;
        }

        if (device.platform === "lg" && !(device.id.getFirmwareYear() <= 2011 && device.id.getHardwareType() === IId.HARDWARE_TYPE.EMULATOR)) {
            window.onmouseon = origMouseOnHandler;
            window.onmouseoff = origMouseOffHandler;
            return;
        }

        if (device.platform === "wiiu" && nwf) {
            // Wii controllers
            var wiiRemotes = [],
                key, remote;

            wiiRemotes[0] = nwf.input.WiiRemote.getController(nwf.input.WiiRemote.REMOTE_1);
            wiiRemotes[1] = nwf.input.WiiRemote.getController(nwf.input.WiiRemote.REMOTE_2);
            wiiRemotes[2] = nwf.input.WiiRemote.getController(nwf.input.WiiRemote.REMOTE_3);
            wiiRemotes[3] = nwf.input.WiiRemote.getController(nwf.input.WiiRemote.REMOTE_4);

            for (key in wiiRemotes) {
                remote = wiiRemotes[key];
                remote.cursor.removeEventListener(nwf.events.DPDControlEvent.UPDATE, onCursorUpdate);
            }
        }

        getRootView().removeEventListener(evtType.KEY, mouseOffHandler, true);

    };
    registerMouseOver = function () {
        rootEle.addEventListener("mouseover", mouseOverHandler);
    };
    unregisterMouseOver = function () {
        rootEle.removeEventListener("mouseover", mouseOverHandler);
    };

    var onMouseWheel = function (evt) {
        var target = evt.target || evt.srcElement;
        var eventObj = {
            deltaY: evt.wheelDelta,
            target: target ? target.__comp : null
        };
        var env = getEnv();

        env.dispatchEvent(env.EVT_ON_MOUSE_WHEEL, eventObj);
    };

    var getEnv = function () {
        sEnv = sEnv || Env.singleton();
        return sEnv;
    };

    var getRootView = function () {
        sAppRoot = sAppRoot || AppRoot.singleton();
        return sAppRoot.getView();
    };


    return {
        /**
         * Starts the mouse handlings.
         * These handlings include focus and wheel handling.
         *
         * @memberof module:ax/ext/mouse/mouseHandler
         * @method
         * @public
         */
        start: function () {
            rootEle = rootEle || getRootView().getRoot();

            keepFocus = config.get("mouse.keep-focus", false);

            registerMouseOnOff();
            registerMouseOver();

            if (device.platform === "lg" && window.NetCastGetMouseOnOff) {
                var newIsMouseOn = window.NetCastGetMouseOnOff().toUpperCase() === "ON";

                setMouseOnOff(newIsMouseOn);
            }

            rootEle.addEventListener("mousewheel", onMouseWheel);
        },
        /**
         * Stops the mouse handlings.
         * These handlings include focus and wheel handling.
         *
         * @memberof module:ax/ext/mouse/mouseHandler
         * @method
         * @public
         */
        stop: function () {
            rootEle = rootEle || getRootView().getRoot();

            unregisterMouseOnOff();
            unregisterMouseOver();

            rootEle.removeEventListener("mousewheel", onMouseWheel);

            isMouseOn = false;
        },
        /**
         * Checks whether mouse is currently on
         * @memberof module:ax/ext/mouse/mouseHandler
         * @method
         * @return {Boolean} whether mouse is currently on
         * @public
         */
        isMouseOn: function () {
            return isMouseOn;
        },
        /**
         * Sets a callback function to be called whenever mouse is turned on
         * @memberof module:ax/ext/mouse/mouseHandler
         * @param {Function} callback the callback function
         * @method
         * @public
         */
        setMouseOnCallback: function (callback) {
            mouseOnCallback = callback;
        },
        /**
         * Sets a callback function to be called whenever mouse is turned off
         * @memberof module:ax/ext/mouse/mouseHandler
         * @param {Function} callback the callback function
         * @method
         * @public
         */
        setMouseOffCallback: function (callback) {
            mouseOffCallback = callback;
        },
        /**
         * Sets a callback function to be called whenever mouse is moved over some component.
         * The callback will be called with parameter of the component.
         * @memberof module:ax/ext/mouse/mouseHandler
         * @param {Function} callback the callback function
         * @method
         * @public
         */
        setMouseOverCallback: function (callback) {
            mouseOverCallback = callback;
        }
    };

});
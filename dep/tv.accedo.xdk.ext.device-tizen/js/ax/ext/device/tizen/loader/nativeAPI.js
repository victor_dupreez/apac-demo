/**
 * A native API loader plugin that is responsible to load tizen API files.
 *
 * The loader will return a the window.module name back.
 *
 * e.g `$WEBAPIS/webapis/webapis` will be automatically loaded and return window.webapis.
 *
 * // load the webapis
 * require(["ax/ext/device/tizen/loader/nativeAPI!$WEBAPIS/webapis/webapis"], function (webapis) {
 *     // get the webapis apis
 *
 * });
 *
 * @module ax/ext/device/tizen/loader/nativeAPI
 */
define([
    "ax/ext/device/tizen/detection",
    "ax/QueryString",
    "ax/util"
], function (
    detection,
    QueryString,
    util
) {
    "use strict";
    var exports = {
        /**
         * If the plugin has a dynamic property set to true, then it means
         * the loader MUST NOT cache the value of a normalized plugin dependency,
         * instead call the plugin's load method for each instance of a plugin dependency.
         * @property {Boolean} dynamic
         * @memberof module:ax/ext/device/tizen/loader/nativeAPI
         */
        dynamic: false,

        /**
         * pluginBuilder is a string that points to another module to use instead of the current plugin when the plugin is used as part of an optimizer build.
         * Without this property, the optimization WILL fail.
         * @see {@link http://requirejs.org/docs/plugins.html#apipluginbuilder}
         * @property {String} pluginBuilder
         * @memberof module:ax/ext/device/tizen/loader/nativeAPI
         */
        pluginBuilder: "./nativeAPIBuilder"

    };

    var modules = {};
    /**
     * Instantiate the API object.
     * @private
     * @method
     * @param {String} name The API name
     * @returns {Object} The newly created API object
     * @memberof module:ax/ext/device/samsung/loader/nativeAPI
     */

    function locateAPI(name) {
        var obj = window;

        // remove the place holder of the API name
        name = name.replace("$WEBAPIS/", "");
        name = name.split("/").pop();

        return obj[name];
    }

    /**
     * Load a resource.
     * Assuming the resource IDs do not need special ID normalization.
     * @method
     * @param {String} resourceId The resource ID that the plugin should load. This ID MUST be normalized.
     * @param {Function} require A local require function. Will not be used.
     * @param {Function} load A function to call once the value of the resource ID has been determined.
     * @param {Object} [opts] Option
     * @param {Function} [opts.onFailure] failure callback
     * @memberof module:ax/ext/device/tizen/loader/nativeAPI
     */
    exports.load = function (resourceId, require, load, opts) {
        //Avoiding running this loading modules in optimized(concat) version
        if (!detection) {
            load({});
            return;
        }

        if (modules[resourceId]) {
            load(modules[resourceId]);
            return;
        }

        var path = resourceId;
        if (resourceId.lastIndexOf(".js") < (resourceId.length - 3)) {
            path = resourceId + ".js";
        }

        // require a *.js, no path resolving should be done
        // expecting nothing from the callback as the native API is not a module but a script with global object(s)
        require([path], function () {

            modules[resourceId] = locateAPI(resourceId);

            load(modules[resourceId]);
        }, opts.onFailure);
    };


    return exports;
});
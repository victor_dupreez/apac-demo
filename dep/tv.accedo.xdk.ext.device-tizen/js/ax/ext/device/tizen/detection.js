/**
 * Detection of tizen by checking the tizen object. Return true if it is tizen
 * @module ax/ext/device/tizen/detection
 */
define("ax/ext/device/tizen/detection", [], function () {
    "use strict";
    var detection = (function () {
        //tizen.tv doesn't appear on the private sdk, so checking on the tizen.tv is ignored
        //http://www.samsungdforum.com/TizenGuide/?FolderName=tizen201&FileName=index.html
        //Mozilla/5.0(Linux;Tizen 2.3) AppleWebKit/538.1 (KHTML, like Gecko)Version/2.3 TV Safari/538.1
        return typeof tizen !== "undefined";
    })();
    return detection;
});
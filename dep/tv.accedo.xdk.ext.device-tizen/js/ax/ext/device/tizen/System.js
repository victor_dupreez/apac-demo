/**
 * System class to handle the system api and connection checking
 * @class ax/ext/device/tizen/System
 * @augments ax/device/shared/Html5System
 */
define("ax/ext/device/tizen/System", [
    "ax/class",
    "ax/device/shared/Html5System",
    "ax/console",
    "ax/promise",
    "ax/util",
    "ax/device/interface/System",
    "require",
    "./loader/nativeAPI!$WEBAPIS/webapis/webapis"
], function (
    klass,
    Html5System,
    console,
    promise,
    util,
    ISystem,
    require,
    webapis
) {
    "use strict";

    return klass.create(Html5System, {
        /**
         * To store the key id of the isPaused state in the device storage.
         * @name LIFE_CYCLE_IS_PAUSED
         * @constant
         * @memberof ax/ext/device/tizen/System
         * @public
         */
        LIFE_CYCLE_IS_PAUSED: "__XDK:life-cycle-pause"
    }, {
        /**
         * To store the resolution size
         * @private
         * @name __resolution
         * @memberof ax/ext/device/tizen/System#
         */
        __resolution: null,
        /**
         * To store the network status
         * @private
         * @name __online
         * @memberof ax/ext/device/tizen/System#
         */
        __online: true,
        init: function () {
            this._super();

            var deferObj = promise.defer();

            this.__readyPromise = deferObj.promise;
            this.__checkNetworkStatus();

            //XDK-2488 Since the window.innerWidth is affected by the meta content width size, 
            //used the internal api to get the absolute resolution independent of css and meta viewport
            tizen.systeminfo.getPropertyValue("DISPLAY", util.bind(function (displayInfo) {

                this.__resolution = {
                    width: displayInfo.resolutionWidth,
                    height: displayInfo.resolutionHeight
                };

                deferObj.resolve(true);

            }, this));

        },
        /**
         * to handle when visibility change, and store the life cycle status
         * @method _onVisibilityChange
         * @protected
         * @memberof ax/ext/device/tizen/System#
         */
        _onVisibilityChange: function (e) {

            var _document = this._getDocument(),
                visibility = this._getVisibility(),
                device = require("ax/device");

            if (_document[visibility.hidden]) {
                device.storage.set(this.constructor.LIFE_CYCLE_IS_PAUSED, true);
            } else {
                device.storage.unset(this.constructor.LIFE_CYCLE_IS_PAUSED);
            }

            this._super(e);
        },
        /**
         * To get the promise for the initialization
         * @method getInitPromise
         * @returns {module:ax/promise}
         * @memberof ax/ext/device/tizen/System#
         */
        getInitPromise: function () {
            return this.__readyPromise;
        },
        /**
         * Turn off the screen
         * @name powerOff
         * @method
         * @memberof ax/ext/device/tizen/System#
         */
        powerOff: function () {
            //Turns off the screen// no api
            return false;
        },
        /**
         * Exit the application
         * @name exit
         * @method
         * @param {Object} [opts] the exit properties
         * @param {Boolean} [opts.toTV] whether exit or hide the application. True will exit the application and false will hide the application.
         * @memberof ax/ext/device/tizen/System#
         */
        exit: function (opts) {
            if (!tizen.application) {
                return;
            }

            opts = opts || {
                toTV: true
            };

            if (opts.toTV) {
                tizen.application.getCurrentApplication().exit();
                return;
            }

            tizen.application.getCurrentApplication().hide();
        },
        /**
         * To get the display resolution
         * @public
         * @method
         * @name getDisplayResolution
         * @return {Object} object {width:1920,height:1080}
         * @memberof ax/ext/device/tizen/System#
         */
        getDisplayResolution: function () {
            return this.__resolution;
        },
        /**
         * @public
         * @method supportSSL
         * @return {Boolean} True
         * @memberof ax/ext/device/tizen/System#
         */
        supportSSL: function () {
            return true;
        },
        /**
         * Override the abstract system and return the current network status
         * @method getNetworkStatus
         * @return {Promise.<Boolean>}  Return the current network status
         * @public
         * @memberof ax/ext/device/tizen/System#
         */
        getNetworkStatus: function () {
            return promise.resolve(this.__online);
        },
        /**
         * Handling the network status checking
         * @method __checkNetworkStatus
         * @private
         * @memberof ax/ext/device/tizen/System#
         */
        __checkNetworkStatus: function () {
            webapis.network.addNetworkStateChangeListener(util.bind(function (data) {

                if (data === 1) {
                    this.__online = true;
                } else {
                    this.__online = false;
                }

                this.dispatchEvent(ISystem.EVT_NETWORK_STATUS_CHANGED, this.__online);
            }, this));
        },
        /**
         * To set the system mute
         * @public
         * @method setSystemMute
         * @param {Boolean} flag True if turn on the mute, false when turn off.
         * @memberof ax/ext/device/tizen/System#
         */
        setSystemMute: function (flag) {
            if (tizen.tvaudiocontrol) {
                tizen.tvaudiocontrol.setMute(flag);
            }
        },
        /**
         * Has mouse on tizen
         * @public
         * @method hasMouse
         * @returns {Boolean}  Return true
         * @memberof ax/ext/device/tizen/System#
         */
        hasMouse: function () {
            return true;
        },
        /**
         * Determine whether support cross domain ajax.
         * @method hasSOP
         * @return {Boolean} Return true if cross domain ajax is not allowed natively. Proxy / Allow-Origin header setup may be needed.
         * @memberof ax/ext/device/tizen/System#
         */
        hasSOP: function () {
            return true;
        },
        /**
         * Setting the screen saver On/Off
         * @method setScreenSaver
         * @param {Boolean} flag True to turn on and off to turn off.
         * @return {Boolean}  Return false.
         * @memberof ax/ext/device/tizen/System#
         */
        /*jshint unused:false*/
        setScreenSaver: function (flag) {
            var state = webapis.appcommon.AppCommonScreenSaverState,
                value = state.SCREEN_SAVER_OFF;

            if (flag) {
                value = state.SCREEN_SAVER_ON;
            }

            webapis.appcommon.setScreenSaver(value, function () {
                console.log("Set the screensaver to be active properly");
            }, function () {
                console.log("Fail to set the screensaver to be active.");
            });

            return true;
        },
        /*jshint unused:true*/
        /**
         * @method redraw
         * @param {HTMLElement} [element=document.body] the target element. Default will be document.body
         * @param {Boolean} false when not support
         * @memberof ax/device/tizen/System#
         */
        /*jshint unused:false*/
        redraw: function (element) {
            return false;
        },
        /*jshint unused:true*/
        /**
         * @method hasFixedKeyboard
         * @return {Boolean} Return false
         * @memberof ax/ext/device/tizen/System#
         */
        hasFixedKeyboard: function () {
            return true;
        }
    });
});
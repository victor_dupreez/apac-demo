/**
 * Id class to handle the device information like firmware version
 * @class ax/ext/device/tizen/Id
 */
define("ax/ext/device/tizen/Id", [
    "ax/class",
    "ax/device/interface/Id",
    "ax/util",
    "ax/promise",
    "ax/console",
    "ax/device/helper/storageUniqueId",
    "./loader/nativeAPI!$WEBAPIS/webapis/webapis"
], function (
    klass,
    IId,
    util,
    promise,
    console,
    storageUniqueId,
    webapis
) {
    "use strict";
    return klass.create([IId], {}, {
        /**
         * Get the MAC address.
         * @method getMac
         * @return {String} null when not available
         * @memberof ax/ext/device/tizen/Id#
         * @public
         */
        getMac: function () {
            return webapis.network.getMac();
        },
        /**
         * To return the device type
         * @method getDeviceType
         * @deprecated replaced with getHardwareType
         * @returns {ax/device/interface/Id.HARDWARE_TYPE} hardware type
         * @memberof ax/ext/device/tizen/Id#
         * @public
         */
        getDeviceType: function () {
            return this.getHardwareType();
        },
        /**
         * To return the hardware type
         * @method getHardwareType
         * @returns {ax/device/interface/Id.HARDWARE_TYPE} hardware type
         * @memberof ax/ext/device/tizen/Id#
         * @public
         */
        getHardwareType: function () {
            if (navigator.userAgent.indexOf("sdk") !== -1) {
                // an emulator
                return IId.HARDWARE_TYPE.EMULATOR;
            } else {
                // real device
                return IId.HARDWARE_TYPE.TV;
            }

        },
        /**
         * To get the version of the Web API in the [Major].[Minor] format.
         * @method getFirmware
         * @returns {String} the firmware version. Return "dummyFirmware" when no related information
         * @memberof ax/ext/device/tizen/Id#
         * @public
         */
        getFirmware: function () {
            return webapis.productinfo.getFirmware();
        },
        /**
         * Get the firmware year
         * @method getFirmwareYear
         * @memberof ax/ext/device/tizen/Id#
         * @return {Number} Return 0 if not available.
         * @public
         */
        getFirmwareYear: function () {
            return 0;
        },
        /**
         * To get the device unique id
         * @method getUniqueID
         * @returns {String} the unique id
         * @memberof ax/ext/device/tizen/Id#
         * @public
         */
        getUniqueID: function () {
            return webapis.productinfo.getDuid();
        },
        /**
         * To get the device model
         * @method getModel
         * @returns {String} the model name of the current device. Return "dummyModel" when no information
         * @memberof ax/ext/device/tizen/Id#
         * @public
         */
        getModel: function () {
            return webapis.productinfo.getRealModel();
        },
        /**
         * To get the IPv4 address
         * @method getIP
         * @returns {String} the ip address. Return 0.0.0.0 when no available ip
         * @memberof ax/ext/device/tizen/Id#
         * @public
         */
        getIP: function () {
            return webapis.network.getIp();
        }
    });
});
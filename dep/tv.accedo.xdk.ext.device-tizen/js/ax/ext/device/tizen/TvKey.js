/**
 * TvKey class to handle the key
 * @class ax/ext/device/tizen/TvKey
 * @augments ax/device/shared/browser/TvKey
 */
define("ax/ext/device/tizen/TvKey", [
    "ax/device/shared/browser/TvKey",
    "ax/class",
    "ax/device/vKey",
    "ax/util",
    "ax/device/shared/browser/keyMap",
    "ax/console",
    "require"
], function (
    browserTvKey,
    klass,
    VKey,
    util,
    keyMap,
    console,
    require
) {
    "use strict";
    return klass.create(browserTvKey, {
        /**
         * @name CH_UP
         * @property {String} id Virtual Key ID. "tizen:vkey:chup"
         * @memberof ax/ext/device/tizen/TvKey
         */
        CH_UP: {
            id: "tizen:vkey:chup"
        },
        /**
         * @name CH_DOWN
         * @property {String} id Virtual Key ID. "tizen:vkey:chdown"
         * @memberof ax/ext/device/tizen/TvKey
         */
        CH_DOWN: {
            id: "tizen:vkey:chdown"
        },
        /**
         * @name TOOLS
         * @property {String} id Virtual Key ID. "tizen:vkey:tools"
         * @memberof ax/ext/device/tizen/TvKey
         */
        TOOLS: {
            id: "tizen:vkey:tools"
        },
        /**
         * @name INFO
         * @property {String} id Virtual Key ID. "tizen:vkey:info"
         * @memberof ax/ext/device/tizen/TvKey
         */
        INFO: {
            id: "tizen:vkey:info"
        },
        /**
         * @name GUIDE
         * @property {String} id Virtual Key ID. "tizen:vkey:guide"
         * @memberof ax/ext/device/tizen/TvKey
         */
        GUIDE: {
            id: "tizen:vkey:guide"
        },
        /**
         * @name MTS
         * @property {String} id Virtual Key ID. "tizen:vkey:mts"
         * @memberof ax/ext/device/tizen/TvKey
         */
        MTS: {
            id: "tizen:vkey:mts"
        },
        /**
         * @name  MENU
         * @property {String} id Virtual Key ID. "tizen:vkey:menu
         * @memberof ax/ext/device/tizen/TvKey
         */
        MENU: {
            id: "tizen:vkey:menu"
        },
        /**
         * @name  EXTRA
         * @property {String} id Virtual Key ID. "tizen:vkey:extra
         * @memberof ax/ext/device/tizen/TvKey
         */
        EXTRA: {
            id: "tizen:vkey:extra"
        }
    }, {
        init: function () {
            var keyMapping = util.clone(keyMap.KEYBOARD_KEY, true),
                charMapping = util.clone(keyMap.KEYBOARD_CHAR, true),
                targetSupportedKeys, i, supportedKeys;

            //target supported key mapping
            targetSupportedKeys = {
                //support the color keys
                "ColorF0Red": VKey.RED,
                "ColorF1Green": VKey.GREEN,
                "ColorF2Yellow": VKey.YELLOW,
                "ColorF3Blue": VKey.BLUE,

                //support the playback keys
                "MediaFastForward": VKey.FF,
                "MediaPause": VKey.PAUSE,
                "MediaPlay": VKey.PLAY,
                "MediaRewind": VKey.RW,
                "MediaStop": VKey.STOP,
                "MediaPlayPause": VKey.PLAY_PAUSE,
                //Next and Prev key on the TV screen remote doesn't work and not available to register, pending for the Samsung reply.
                "MediaTrackNext": VKey.NEXT,
                "MediaTrackPrevious": VKey.PREV,

                //Extra on the univeral remote control
                "Extra": this.constructor.EXTRA,

                //support functional keys (basic function keys and some keys that are available on the previous samsung device packages)
                // it may need to verify on the real devices whether it will block the default behaviour on TV.
                "Tools": this.constructor.TOOLS,
                "Back": VKey.BACK,
                "Exit": VKey.EXIT,
                "Info": this.constructor.INFO,
                "MTS": this.constructor.MTS,
                "Guide": this.constructor.GUIDE,
                "ChannelUp": this.constructor.CH_UP,
                "ChannelDown": this.constructor.CH_DOWN
            };


            //support the number keys
            for (i = 0; i < 10; i++) {
                targetSupportedKeys[i] = VKey["KEY_" + i];
            }

            //get the device supported keys which return an array of supported key from the device
            supportedKeys = tizen.tvinputdevice.getSupportedKeys();

            util.each(supportedKeys, function (supportedKey) {
                var keyObj = targetSupportedKeys[supportedKey.name];
                //register the keys when matching the target supported keys
                if (keyObj) {
                    tizen.tvinputdevice.registerKey(supportedKey.name);
                    keyMapping.VKey[supportedKey.code] = keyObj;
                }
            });

            //some keys are no need to register and directly map
            keyMapping.VKey[10009] = VKey.BACK;

            //XDK-2622 Added Numpad Enter key which is charCode 13.
            charMapping.VKey[13] = VKey.OK;

            //after register the keys, they are available to use on the window onkeydown
            this.initKeyMapping(keyMapping, charMapping);
        }
    });
});
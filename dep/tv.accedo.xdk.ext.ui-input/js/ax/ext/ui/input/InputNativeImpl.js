/**
 *
 * Input Native widget use the input tag and is compatible to the mouse. User can click and move the cursor.
 *
 * ###Dom Structure
 *      <div class="wgt-input wgt-input-native" id="input">
 *          <div class="container">
 *              <input class="wgt-input-native-ele">
 *          </div>
 *      </div>
 *
 * ###CSS
 * ####Structural CSS
 *      //To display input properly.
 *      .wgt-input-native-ele{
 *          resize: none;
 *          margin: 0px;
 *          padding: 0px;
 *          border: 0px;
 *          outline: none;
 *          word-wrap: break-word;
 *          height: 100%;
 *      }
 *
 * ####Customizable CSS
 * .wgt-input - to set the size of the input
 *
 * @class ax/ext/ui/input/InputNativeImpl
 * @augments ax/ext/ui/input/interface/InputImpl
 * @param {Object} opts The options object
 * @param {Number} opts.maxLength The available length of the input
 * @param {Boolean} opts.isPassword True if it is password and it will be "*" for the input
 * @param {String} opts.text The default text of the input
 * @param {Number} opts.hideTimeout time to hide the password and only available when opts.isPassword true
 * @param {String} opts.passwordChar The char to replace the "*" which is the default character.
 */
define("ax/ext/ui/input/InputNativeImpl", [
    "ax/class",
    "ax/Element",
    "ax/util",
    "ax/device",
    "ax/console",
    "ax/af/evt/type",
    "ax/device/shared/browser/keyMap",
    "ax/core",
    "./interface/InputImpl",
    "css!./css/InputNativeImpl"
], function (
    klass,
    Element,
    util,
    device,
    console,
    evtType,
    keyMap,
    core,
    InputImpl
) {
    "use strict";

    return klass.create([InputImpl], {}, {
        /**
         * To check if it old samsung device which is older than or equal 2011
         * @private
         * @name __oldSamsung
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __oldSamsung: device.platform === "samsung" && device.id.getFirmwareYear() <= 2011,
        /**
         * @private
         * @name __cursorPosition
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __cursorPosition: null,
        /**
         * @private
         * @name __labelText
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __labelText: null,
        /**
         * @private
         * @name __inputState
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __inputState: false,
        /**
         * @private
         * @name __passwordTimer
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __passwordTimer: null,
        /**
         * @private
         * @name __initEnvOnKey
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __initEnvOnKey: false,
        /**
         * @private
         * @name __maxLength
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __maxLength: 20,
        /**
         * @private
         * @name __isPassword
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __isPassword: false,
        /**
         * @private
         * @name __passwordChar
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __passwordChar: null,
        /**
         * @private
         * @name __hideTimeout
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __hideTimeout: 0,
        /**
         * @private
         * @name __widgetInstance
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __widgetInstance: null,
        /**
         * @private
         * @name __input
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __input: null,
        /**
         * @private
         * @name __element
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __element: null,
        /**
         * To store the place holder word
         * @private
         * @name __placeHolder
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __placeHolder: null,
        /**
         * Flag to see if placeHolder is set or not
         * @private
         * @name __placeHolder
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __isSetPlaceHolder: false,
        /**
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @private
         */
        init: function (opts) {
            var self = this,
                handleClickFunc, handleMouseOut, handleKeyFunc, samsungHandleKeyFunc, handleTextEditKeyFunc, checkDangerousKeyFunc, upKey = keyMap.KEYBOARD_KEY.keyCode.UP,
                downKey = keyMap.KEYBOARD_KEY.keyCode.DOWN,
                leftKey = keyMap.KEYBOARD_KEY.keyCode.LEFT,
                rightKey = keyMap.KEYBOARD_KEY.keyCode.RIGHT,
                insertKey = keyMap.KEYBOARD_KEY.keyCode.INSERT,
                delKey = keyMap.KEYBOARD_KEY.keyCode.DELETE,
                homeKey = keyMap.KEYBOARD_KEY.keyCode.HOME,
                endKey = keyMap.KEYBOARD_KEY.keyCode.END,
                pageUpKey = keyMap.KEYBOARD_KEY.keyCode.PAGE_UP,
                pageDownKey = keyMap.KEYBOARD_KEY.keyCode.PAGE_DOWN,
                tabKey = keyMap.KEYBOARD_KEY.keyCode.TAB,
                backspaceKey = keyMap.KEYBOARD_KEY.keyCode.BACKSPACE;
            //those keys on the keyboard suppose to be blocked and no default navigation/behvaiour on the input field
            opts = opts || {};

            opts.root.addClass("wgt-input-native");

            this.setMaxLength(util.isNumber(opts.maxLength) ? opts.maxLength : 20);
            this.__isPassword = opts.isPassword || false;
            this.__passwordChar = opts.passwordChar || "*";
            this.__hideTimeout = opts.hideTimeout || 0;
            this.__placeHolder = opts.placeHolder || "";

            this.__labelText = opts.text || "";
            this.__cursorPosition = this.__labelText.length;
            this.__widgetInstance = opts.widgetInstance;

            // Create div structure
            this.container = new Element("div");
            this.container.addClass("container");

            this.__input = new Element("input");


            this.__input.addClass("wgt-input-native-ele");
            //XDK-1838
            //since it is part of the implementation of the input and thus append the current container to the root instead of create a new root in the component
            opts.root.append(this.container);
            this.container.append(this.__input);
            this.__element = this.__input.getHTMLElement();
            this.setText(opts.text, true);

            checkDangerousKeyFunc = function (evt) {
                // checks dangerous keys that interacts with input. If there are any specific keys on the platforms, it needs to be included here
                switch (evt.keyCode) {
                case upKey:
                case downKey:
                case leftKey:
                case rightKey:
                case insertKey:
                case delKey:
                case homeKey:
                case endKey:
                case pageUpKey:
                case pageDownKey:
                case tabKey:
                case backspaceKey:
                    return true;
                default:
                    return false;
                }
            };
            // end of ugly keyboard related platform checking
            handleClickFunc = function (evt) {
                console.info("inputNativeImpl: mouse click; inputState: " + self.__inputState);


                util.defer().then(function () {
                    //to make the cursor to be position 0 when there is place holder
                    if (self.__isSetPlaceHolder) {
                        self.setCursorPos(0);
                    } else {
                        self.__updateCursorPosition();
                    }
                    self.startInput();
                }).done();
                return true;
            };
            handleMouseOut = function (evt) {
                console.info("inputNativeImpl: mouse out; inputState: " + self.__inputState);

                self.stopInput();
                return true;
            };
            handleKeyFunc = function (evt) {
                try {
                    evt.preventDefault();
                } catch (ex) {
                    ex.returnValue = false;
                }
                return true; // keep bubbling
            };
            handleTextEditKeyFunc = function (evt) {
                if (checkDangerousKeyFunc(evt)) { // catches dangerous keys that interacts with textarea
                    console.info("inputNativeImpl: blocking text edit key:" + evt.keyCode);
                    try {
                        evt.preventDefault();
                    } catch (ex) {
                        ex.returnValue = false;
                    }
                }
                return true; // keep bubbling
            };

            switch (device.platform) {
            case "samsung":
                if (device.id.getFirmwareYear() === 2010) {
                    console.debug("inputNativeImpl: samsung 2010 device detected, using special handler");
                    samsungHandleKeyFunc = function (evt) {
                        console.debug("inputNativeImpl: samsung workaround handler in action...");
                        // === all these should be in vain ===
                        try {
                            evt.preventDefault();
                        } catch (ex) {
                            ex.returnValue = false;
                        }
                        // === end ===
                        self.setText(self.__value, self.__cursorPosition);
                        return true; // return false won't work as well
                    };
                    this.__input.addEventListener("keydown", samsungHandleKeyFunc); //workaround
                    break;
                }
                /* falls through */
            default:
                this.container.addEventListener("mousedown", handleClickFunc);
                this.container.addEventListener("mouseout", handleMouseOut);
                this.__input.addEventListener("keydown", handleTextEditKeyFunc);
                this.__input.addEventListener("keypress", handleKeyFunc);
                break;
            }

        },
        /**
         * add the place holder when the text value is ""
         * @method __addPlaceHolder
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @private
         */
        __addPlaceHolder: function () {
            this.__isSetPlaceHolder = true;
            //set the placeholder value into the input
            this.__element.value = this.__placeHolder;
            this.__input.addClass("placeholder");
            //make sure the cursor position to be at 0 position.
            this.__setRealCursorPos();
        },
        /**
         * remove the place holder when the text value is not ""
         * @method __removePlaceHolder
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @private
         */
        __removePlaceHolder: function () {
            this.__isSetPlaceHolder = false;
            this.__element.value = "";
            this.__input.removeClass("placeholder");
        },
        /**
         * Insert character to the input field
         * @name insertChar
         * @function
         * @param {String} ch The character to insert to
         * @param {Number} insertPos The position character insert to
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        insertChar: function (ch, insertPos) {
            if (!this.__inputState || !ch) {
                return;
            }
            if (this.__labelText.length >= this.__maxLength) { // If length of __labelText reach maximum, do nothing, and return
                return;
            }

            insertPos = util.isUndefined(insertPos) ? this.__cursorPosition : insertPos;
            var t = this.getText();
            t = util.insert(t, insertPos, ch);
            this.__cursorPosition = this.__cursorPosition + ch.length;
            this.setText(t, undefined, true);
            this.__updateCursorPosition();
            //in huawei device, it is unable to update the cursor position into new line when at the end of the text area
            this.__element.blur();
            this.__element.focus();
        },
        /**
         * Delete the last character in the input field
         * @name backspace
         * @function
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        backspace: function () {
            if (!this.__inputState) {
                return;
            }
            if (this.__cursorPosition <= 0) { // If cursorPosition == 0, there must be no char before cursor, do nothing, and return
                return;
            }

            var text = this.getText(),
                newText = "";
            if (this.__cursorPosition === 1) { // Exception case when delete the first char while the string is have more than 1 char
                if (text.length > 1) {
                    newText = text.slice(this.__cursorPosition, text.length);
                }
            } else {
                if (text.length > 1) {
                    newText = text.slice(0, this.__cursorPosition - 1) + text.slice(this.__cursorPosition, text.length);
                }
            }
            this.__cursorPosition--;
            this.setText(newText, undefined, true);
            this.__updateCursorPosition();
            this.__setRealCursorPos();
        },
        /**
         * Delete the character after cursor in the input field
         * @name del
         * @function
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        del: function () {
            if (!this.__inputState) {
                return;
            }
            var text = this.getText(),
                newText = "";
            if (this.__cursorPosition >= text.length) { // If __cursorPosition == t.length, there must be no char after cursor, do nothing, and return
                return;
            }

            if (this.__cursorPosition === text.length - 1) { // Exception case when delete the last char
                newText = text.slice(0, text.length - 1);
            } else {
                newText = text.slice(0, this.__cursorPosition) + text.slice(this.__cursorPosition + 1, text.length);
            }
            this.setText(newText, undefined, true);
            this.__setRealCursorPos();
        },
        /**
         * Set max length of input field
         * @name setMaxLength
         * @function
         * @param {Integer} l The number of characters to set
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        setMaxLength: function (l) {
            this.__maxLength = l;
        },
        /**
         * Get max length of input field
         * @method getMaxLength
         * @returns {Integer} The maximum character length in the input
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        getMaxLength: function () {
            return this.__maxLength;
        },
        /**
         * Get the input field text
         * @name getText
         * @function
         * @return {String} __labelText Return the current label text
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        getText: function () {
            return this.__labelText;
        },
        /**
         * Set text on the input field
         * @name setText
         * @function
         * @param {String} text
         * @param {Boolean} forceNoTimeout
         * @param {Boolean} noUpdateCursorPosition true won't update the cursor position after the setText, default is false and will update the cursor position to the end when setText
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        setText: function (text, forceNoTimeout, noUpdateCursorPosition) {

            // incorrect input
            if (util.isUndefined(text)) {
                return;
            }

            if (!text) {
                text = "";
            }

            //original has nothing and set text, it will first remove the placeHolder
            if (this.__placeHolder !== null && this.getText() === "" && text !== "") {
                this.__removePlaceHolder();
            }

            var textChanged = (text !== this.__labelText),
                len, wrapperText = "",
                i, isAddition, useTimeout, hideTextFn;
            if (text.length > this.__maxLength) {
                this.__cursorPosition = this.__maxLength;
            } else if (!noUpdateCursorPosition) {
                this.__cursorPosition = text.length;
            }
            text = util.truncate(text, this.__maxLength, "");
            if (this.__isPassword) {
                isAddition = text.length > this.__labelText.length; // we are having more characters or not
                useTimeout = (this.__hideTimeout > 0) && isAddition && !forceNoTimeout; // do we briefly show the last character before cursor or not
                this.__labelText = text;
                len = this.__labelText.length;

                if (this.__passwordTimer) {
                    util.clearDelay(this.__passwordTimer);
                    this.__passwordTimer = null;
                }

                for (i = 0; i < this.__cursorPosition - 1; i++) { // leave out the last character before cursor position
                    wrapperText += this.__passwordChar;
                }
                if (!useTimeout && this.__cursorPosition > 0) {
                    wrapperText += this.__passwordChar;
                } else if (text.length > 0 && this.__cursorPosition - 1 < text.length) {
                    wrapperText += text[this.__cursorPosition - 1];
                }

                for (i = 0; i < len - this.__cursorPosition; i++) { // fill the remaining characters
                    wrapperText += this.__passwordChar;
                }

                this.__setElementText(wrapperText);

                hideTextFn = util.bind(function () { // hide
                    wrapperText = "";
                    for (i = 0; i < this.__labelText.length; i++) {
                        wrapperText += this.__passwordChar;
                    }
                    this.__setElementText(wrapperText);
                    this.__updateCursorPosition();
                    this.__passwordTimer = null;
                }, this);

                if (useTimeout) {
                    this.__passwordTimer = core.getGuid();
                    util.delay(this.__hideTimeout, this.__passwordTimer).then(hideTextFn).done();
                } else {
                    hideTextFn();
                }

            } else {
                this.__setElementText(text);
            }

            this.__labelText = text;
            this.__labelTextCopy = util.truncate(this.__labelText, this.__cursorPosition, "");

            if (!noUpdateCursorPosition) {
                this.__updateCursorPosition();
            }

            if (textChanged && this.__widgetInstance) {
                this.__widgetInstance.dispatchEvent(evtType.TEXT_CHANGED, this.__labelText);
            }

            //if the text is empty, it will set the place holder
            if (this.__placeHolder !== null && text === "") {
                this.__addPlaceHolder();
            }

        },
        /**
         * Sets the text to one of the elements
         * @name __setElementText
         * @param {ax/Element} element Element to set text to
         * @param {String} text Text to set
         * @function
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @private
         */
        __setElementText: function (text) {
            text = util.truncate(text, this.__maxLength, "");

            if (!this.__isPassword) {
                this.__labelText = text;
            }

            this.__element.value = text;

            if (this.__inputState) {
                this.__element.blur();
                this.__element.focus();
            }
            this.__setRealCursorPos();
        },
        /**
         * Clear input field text
         * @name clearText
         * @function
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        clearText: function () {
            this.setText("");
        },
        /**
         * Update the UI of the cursor according to the __cursorPosition number
         * @name __updateCursorPosition
         * @function
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        __updateCursorPosition: function () {
            //block the cursor update when place holder is set.
            if (this.__isSetPlaceHolder) {
                return;
            }

            var ctrl = this.__element,
                cursorPos = 0,
                selection;

            // IE 9+ and all other browsers
            if (ctrl.selectionStart || String(ctrl.selectionStart) === "0") {
                console.debug("InputNativeImpl: element has selectionStart");
                cursorPos = ctrl.selectionStart;
            }
            // IE 9+ and all other browsers (normally the above already works)
            else if (document.getSelection) {
                console.debug("InputNativeImpl: document has getSelection()");
                ctrl.focus();
                selection = document.getSelection().createRange();

                selection.moveStart("character", -ctrl.value.length);

                cursorPos = selection.text.length;
            }

            this.__cursorPosition = cursorPos;
            console.debug("InputNativeImpl: __updateCursorPos() reports cursor position is " + cursorPos);
            return cursorPos;
        },
        /**
         * Move the cursor in certain direction
         * @name moveCursor
         * @function
         * @param {String} direction either "left" or "right"
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        moveCursor: function (direction) {
            if (!this.__inputState) {
                return;
            }
            if (direction.toLowerCase() === "left") {
                this.setCursorPos(this.__cursorPosition - 1);
            } else if (direction.toLowerCase() === "right") {
                this.setCursorPos(this.__cursorPosition + 1);
            }
        },
        /**
         * Move the cursor to a specific position
         * @name setCursorPos
         * @function
         * @param {Number} pos cursor position
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        setCursorPos: function (pos) {
            if (pos > this.getText().length) {
                pos = this.getText().length;
            } else if (pos < 0) {
                pos = 0;
            }

            if (this.__cursorPosition !== pos) {
                this.__cursorPosition = pos;
                this.__setRealCursorPos();
            }
        },
        /**
         * Make the inputField ready for capturing
         * @name startInput
         * @function
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        startInput: function () {
            this.__element.focus();
            this.__inputState = true;
            // update the cursor position when first enter input status for safe-guarding
            // because updating the cursor position during initialization may not work
            if (this.__inputStateFirstOn) {
                this.__inputStateFirstOn = false;
                this.setCursorPos(this.__cursorPosition);
            }

            // some browser (webkit for e.g.) may move the real cursor around when arrow keys are pressed, and no known way to prevent it, not even preventDefault...
            // so here added a work-around to force reset again the cursor position to the correct position
            util.defer().then(util.bind(this.__setRealCursorPos, this)).done();
        },
        /**
         * Set the real cursor to go to position at this.__cursorPosition.
         * This method is also used as a work-around when some glitches causes real cursor to move to an incorrect position.
         * @name __setRealCursorPos
         * @function
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @private
         */
        __setRealCursorPos: function () {
            var pos = this.__cursorPosition,
                ctrl = this.__element,
                offset = 0,
                range;
            if (this.__oldSamsung) {
                console.debug("InputNativeImpl: samsung 2011 or 2010 device detected");

                ctrl.focus();

                ctrl.selectionStart = pos;
            }
            // Chrome, FF/Gecko, IE 9+, Opera 8+
            // Samsung maple
            else if (ctrl.setSelectionRange) {
                try {
                    ctrl.setSelectionRange(pos + offset, pos + offset);
                } catch (e) {
                    console.debug("inputNativeImpl: setSelectionRange() error:" + e.message);
                }
                // the native input will not reflect the selection change on the UI if the selection is not visible and the focus persists.
                // Here we blur the input and regain its focus so that the selection can be reflected correctly.
                // https://accedobroadband.jira.com/browse/XDK-2255
                ctrl.blur();
                ctrl.focus();
            }
            // IE and Opera under 10.5
            else if (ctrl.createTextRange) {
                console.debug("inputNativeImpl: element has createTextRange()");
                range = ctrl.createTextRange();
                range.collapse(true);
                range.moveEnd("character", pos);
                range.moveStart("character", pos);
                range.select();
            }
        },
        /**
         * Make the inputField non-capturing
         * @name stopInput
         * @function
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        stopInput: function () {
            this.__element.blur();
            this.__inputState = false;
        },
        /**
         * Check if inputField is capturing
         * @name isCapturing
         * @function
         * @returns {Boolean} true if capturing; false otherwise
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @public
         */
        isCapturing: function () {
            return this.__inputState;
        },
        /**
         * @method
         * @memberof ax/ext/ui/input/InputNativeImpl#
         * @protected
         */
        deinit: function () {
            this.container.deinit();
            this.__input.deinit();
        }
    });
});
/**
 *
 * Input Div is a input field which provide data input. As it is a div, so it is unable to handle click and moving the cursor by mouse.
 *
 * ###Dom Structure
 *
 *      <div class="wgt-input" id="input">
 *          <div class="container">
 *              <pre class="wrapper" style="left: 0px;"></pre>
 *              <pre class="wrapperCopy" style="left: 0px;"></pre>
 *              <div class="cursor" style="left: 0px; visibility: hidden;"></div>
 *          </div>
 *      </div>
 *
 * ###CSS
 * ####Structural CSS
 *      .wrapperCopy, .wrapper {
 *          position: absolute;
 *          left: 0px;
 *          margin: 0px;
 *      }
 *      .wrapperCopy{
 *          visibility: hidden;
 *      }
 *
 * ####Customizable CSS
 * .wgt-input .cursor - to set the style cursor of the input
 *
 * @class ax/ext/ui/input/InputDivImpl
 * @param {Object} opts The options object
 * @param {Number} opts.maxLength The available length of the input
 * @param {Boolean} opts.isPassword True if it is password and it will be "*" for the input
 * @param {String} opts.text The default text of the input
 * @param {Number} opts.hideTimeout time to hide the password and only available when opts.isPassword true
 * @param {String} opts.passwordChar The char to replace the "*" which is the default character.
 */
define("ax/ext/ui/input/InputDivImpl", [
    "ax/class",
    "ax/Element",
    "ax/util",
    "ax/device",
    "ax/af/evt/type",
    "ax/core",
    "./interface/InputImpl",
    "css!./css/InputDivImpl"
], function (
    klass,
    Element,
    util,
    device,
    evtType,
    core,
    InputImpl
) {
    "use strict";

    return klass.create([InputImpl], {}, {
        /**
         * To check if it old samsung device which is older than or equal 2011
         * @private
         * @name __oldSamsung
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __oldSamsung: device.platform === "samsung" && device.id.getFirmwareYear() <= 2011,
        /**
         * @private
         * @name __inputFieldWidth
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __inputFieldWidth: null,
        /**
         * @private
         * @name __wrapper
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __wrapper: null,
        /**
         * @private
         * @name __wrapperCopy
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __wrapperCopy: null,
        /**
         * @private
         * @name __cursor
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __cursor: null,
        /**
         * @private
         * @name __cursorPosition
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __cursorPosition: null,
        /**
         * @private
         * @name __cursorLeftPosition
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __cursorLeftPosition: null,
        /**
         * @private
         * @name __labelText
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __labelText: null,
        /**
         * @private
         * @name __labelTextCopy
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __labelTextCopy: null,
        /**
         * @private
         * @name __wrapperLeft
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __wrapperLeft: null,
        /**
         * @private
         * @name __inputState
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __inputState: false,
        /**
         * @private
         * @name __passwordTimer
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __passwordTimer: null,
        /**
         * @private
         * @name__initEnvOnKey
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __initEnvOnKey: false,
        /**
         * @private
         * @name __maxLength
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __maxLength: 20,
        /**
         * @private
         * @name __isPassword
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __isPassword: false,
        /**
         * @private
         * @name __passwordChar
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __passwordChar: null,
        /**
         * @private
         * @name __hideTimeout
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __hideTimeout: 0,
        /**
         * @private
         * @name __widgetInstance
         * @memberof ax/ext/ui/input/InputDivImpl#
         */
        __widgetInstance: null,
        /**
         * @private
         * @name __placeHolder
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __placeHolder: null,
        /**
         * @private
         * @name __placeHolderDiv
         * @memberof ax/ext/ui/input/InputNativeImpl#
         */
        __placeHolderDiv: null,
        /**
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @private
         */
        init: function (opts) {
            opts = opts || {};

            opts.root.addClass("wgt-input-div");

            this.setMaxLength(util.isNumber(opts.maxLength) ? opts.maxLength : 20);
            this.__isPassword = opts.isPassword || false;
            this.__passwordChar = opts.passwordChar || "*";
            this.__hideTimeout = opts.hideTimeout || 0;
            this.__placeHolder = opts.placeHolder || "";

            this.__labelText = opts.text || "";
            this.__cursorPosition = this.__labelText.length;
            this.__widgetInstance = opts.widgetInstance;

            // Create div structure
            this.container = new Element("div");
            this.container.addClass("container");

            this.__wrapper = new Element("pre");
            this.__wrapper.addClass("wrapper");

            this.__wrapperCopy = new Element("pre");
            this.__wrapperCopy.addClass("wrapperCopy");

            this.__cursor = new Element("div");
            this.__cursor.addClass("cursor");
            this.__cursor.css({
                visibility: "hidden"
            });


            //XDK-1838
            //since it is part of the implementation of the input and thus append the current container to the root instead of create a new root in the component
            opts.root.append(this.container);
            this.container.append(this.__wrapper);
            this.container.append(this.__wrapperCopy);
            this.container.append(this.__cursor);

            //add the place Holder when it is not empty
            if (this.__placeHolder !== "") {
                this.__placeHolderDiv = new Element("div");
                this.__placeHolderDiv.addClass("placeholder");
                this.__placeHolderDiv.setInnerText(this.__placeHolder);

                if (this.__labelText.length > 0) {
                    this.__placeHolderDiv.hide();
                }

                this.container.append(this.__placeHolderDiv);
            }

            this.setText(opts.text, true);
        },
        /**
         * Insert character to the input field
         * @name insertChar
         * @function
         * @param {String} ch The character to insert to
         * @param {Number} insertPos The position character insert to
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        insertChar: function (ch, insertPos) {
            if (!this.__inputState || !ch) {
                return;
            }
            if (this.__labelText.length >= this.getMaxLength()) { // If length of __labelText reach maximum, do nothing, and return
                return;
            }

            insertPos = util.isUndefined(insertPos) ? this.__cursorPosition : insertPos;
            var t = this.getText();
            t = util.insert(t, insertPos, ch);
            this.__cursorPosition = this.__cursorPosition + ch.length;
            this.setText(t, undefined, true);
            this.updateLeft("add");
            this.updateCursorPosition();
        },
        /**
         * Delete the last character in the input field
         * @name backspace
         * @function
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        backspace: function () {
            if (!this.__inputState) {
                return;
            }
            if (this.__cursorPosition <= 0) { // If cursorPosition == 0, there must be no char before cursor, do nothing, and return
                return;
            }

            var text = this.getText(),
                newText = "";
            if (this.__cursorPosition === 1) { // Exception case when delete the first char while the string is have more than 1 char
                if (text.length > 1) {
                    newText = text.slice(this.__cursorPosition, text.length);
                }
            } else {
                if (text.length > 1) {
                    newText = text.slice(0, this.__cursorPosition - 1) + text.slice(this.__cursorPosition, text.length);
                }
            }
            this.__cursorPosition--;
            this.setText(newText, undefined, true);
            this.updateLeft("del");
            this.updateCursorPosition();
        },
        /**
         * Delete the character after cursor in the input field
         * @name del
         * @function
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        del: function () {
            if (!this.__inputState) {
                return;
            }
            var text = this.getText(),
                newText = "";
            if (this.__cursorPosition >= text.length) { // If __cursorPosition == text.length, there must be no char after cursor, do nothing, and return
                return;
            }

            if (this.__cursorPosition === text.length - 1) { // Exception case when delete the last char
                newText = text.slice(0, text.length - 1);
            } else {
                newText = text.slice(0, this.__cursorPosition) + text.slice(this.__cursorPosition + 1, text.length);
            }
            this.setText(newText, undefined, true);
        },
        /**
         * Set max length of input field
         * @name setMaxLength
         * @function
         * @param {Integer} l The number of characters to set
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        setMaxLength: function (l) {
            this.__maxLength = l;
        },
        /**
         * Get max length of input field
         * @method getMaxLength
         * @returns {Integer}  The maximum character length in the input
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        getMaxLength: function () {
            return this.__maxLength;
        },
        /**
         * Get the input field text
         * @name getText
         * @function
         * @return {String} __labelText Return the current label text
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        getText: function () {
            return this.__labelText;
        },
        /**
         * Set text on the input field
         * @name setText
         * @function
         * @param {String} text
         * @param {Boolean} forceNoTimeout
         * @param {Boolean} noUpdateCursorPosition true won"t update the cursor position after the setText, default is false and will update the cursor position to the end when setText
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        setText: function (text, forceNoTimeout, noUpdateCursorPosition) {

            // incorrect input
            if (util.isUndefined(text)) {
                return;
            }

            if (!text) {
                text = "";
            }

            var textChanged = (text !== this.__labelText),
                len, passwordWrapperText = "",
                passwordWrapperCopyText = "",
                i, wrapperCopyText, isAddition, useTimeout, hideTextFn;
            if (text.length > this.getMaxLength()) {
                this.__cursorPosition = this.getMaxLength();
            } else if (!noUpdateCursorPosition) {
                this.__cursorPosition = text.length;
            }
            text = util.truncate(text, this.getMaxLength(), "");
            if (this.__isPassword) {
                isAddition = text.length > this.__labelText.length; // we are having more characters or not
                useTimeout = (this.__hideTimeout > 0) && isAddition && !forceNoTimeout; // do we briefly show the last character before cursor or not
                this.__labelText = text;
                len = this.__labelText.length;

                if (this.__passwordTimer) {
                    util.clearDelay(this.__passwordTimer);
                    this.__passwordTimer = null;
                }

                for (i = 0; i < this.__cursorPosition - 1; i++) { // leave out the last character before cursor position
                    passwordWrapperText += this.__passwordChar;
                    passwordWrapperCopyText += this.__passwordChar;
                }
                if (!useTimeout && this.__cursorPosition > 0) {
                    passwordWrapperText += this.__passwordChar; // directly hide that last typed character as "*"
                    passwordWrapperCopyText += this.__passwordChar;
                } else {
                    passwordWrapperText += text[this.__cursorPosition - 1]; // show the last typed character
                    passwordWrapperCopyText += text[this.__cursorPosition - 1];
                }

                for (i = 0; i < len - this.__cursorPosition; i++) { // fill the remaining characters
                    passwordWrapperText += this.__passwordChar;
                }

                this.__setElementText(this.__wrapper, passwordWrapperText);
                this.__setElementText(this.__wrapperCopy, passwordWrapperCopyText);

                hideTextFn = util.bind(function () { // hide
                    passwordWrapperText = "";
                    passwordWrapperCopyText = "";
                    for (i = 0; i < len; i++) {
                        passwordWrapperText += this.__passwordChar;
                    }
                    for (i = 0; i < this.__cursorPosition; i++) {
                        passwordWrapperCopyText += this.__passwordChar;
                    }
                    this.__setElementText(this.__wrapper, passwordWrapperText);
                    this.__setElementText(this.__wrapperCopy, passwordWrapperCopyText);
                    this.updateCursorPosition();
                    this.__passwordTimer = null;
                }, this);

                if (useTimeout) {
                    this.__passwordTimer = core.getGuid();
                    util.delay(this.__hideTimeout, this.__passwordTimer).then(hideTextFn);
                } else {
                    hideTextFn();
                }

            } else {
                this.__setElementText(this.__wrapper, text);
                if (this.__cursorPosition === 0) {
                    this.__setElementText(this.__wrapperCopy, "");
                } else {
                    wrapperCopyText = util.truncate(text, this.__cursorPosition, "");
                    this.__setElementText(this.__wrapperCopy, wrapperCopyText);
                }
            }

            this.__labelText = text;
            this.__labelTextCopy = util.truncate(this.__labelText, this.__cursorPosition, "");
            if (!noUpdateCursorPosition) {
                this.updateCursorPosition();
            }

            if (!textChanged) {
                return;
            }

            if (this.__placeHolderDiv) {
                if (this.__labelText.length > 0) {
                    this.__placeHolderDiv.hide();
                } else {
                    this.__placeHolderDiv.show();
                }
            }

            if (this.__widgetInstance) {
                this.__widgetInstance.dispatchEvent(evtType.TEXT_CHANGED, this.__labelText);
            }
        },
        /**
         * Sets the text to one of the elements
         * @name __setElementText
         * @param {ax/Element} element Element to set text to
         * @param {String} text Text to set
         * @function
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @private
         */
        __setElementText: function (element, text) {
            var tailingSpan;
            element.setInnerText(text);
            //use a span to detect workaround for the first trailing space issue
            if (text.length > 0 && element === this.__wrapperCopy && this.__oldSamsung) {
                tailingSpan = new Element("span");
                tailingSpan.setInnerText("t");
                element.append(tailingSpan);
            }
        },
        /**
         * Clear input field text
         * @name clearText
         * @function
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        clearText: function () {
            this.setText("");
        },
        /**
         * Update the left position of both this.wrapper and this.wrapperCopy
         * @name updateLeft
         * @function
         * @param {String} method "add" to indicated this function triggered by insertChar(), and "del" indicate this function triggered by deleteChar()
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        updateLeft: function (method) {
            this.wrapperWidth = this.__wrapper.getHTMLElement().clientWidth;
            this.__wrapperLeft = this.__wrapper.getHTMLElement().offsetLeft;
            this.wrapperCopyWidth = this.__wrapperCopy.getHTMLElement().clientWidth;
            this.wrapperCopyLeft = this.__wrapperCopy.getHTMLElement().offsetLeft;
            var leftPos, wrapperCopyEle, wrapperCopyEndSpan;

            if (this.__oldSamsung && this.__cursorPosition > 0) {
                wrapperCopyEle = this.__wrapperCopy.getHTMLElement();
                wrapperCopyEndSpan = wrapperCopyEle.lastChild;
                this.wrapperCopyWidth -= wrapperCopyEndSpan.clientWidth;
            }

            if (!this.__inputFieldWidth) {
                this.__inputFieldWidth = this.container.getHTMLElement().clientWidth;
            }
            // Perform checking to ensure the cursor is within the inputField visually
            if ((this.wrapperWidth >= this.__inputFieldWidth) && (this.__cursorPosition === this.__labelText.length)) {
                // The cursor on the end, the leftPos of wrapper should be equal to ([InputField.Width] - [Wrapper.Width])
                // in order not to have empty space at the end of inputField, and leftPos must be a negative number.
                leftPos = this.__inputFieldWidth - this.wrapperWidth;
            } else if (this.wrapperWidth >= this.__inputFieldWidth) {
                // There must be some charcter outside the inputField visually
                if (method === "del") {
                    if ((this.wrapperWidth + this.__wrapperLeft) <= this.__inputFieldWidth) {
                        leftPos = this.__inputFieldWidth - this.wrapperWidth;
                    }
                }
            } else {
                // The text is within the width of the InputField, set leftPos to 0
                leftPos = 0;
            }

            if (!util.isUndefined(leftPos)) {
                this.updateLeftByCursor(leftPos);
            }
        },
        /**
         * Update left position by moving cursor
         * @name updateLeftByCursor
         * @function
         * @param {Float} leftPos desired cursor leftPos
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        updateLeftByCursor: function (leftPos) {
            this.__wrapper.css({
                "left": leftPos + "px"
            });
            this.__wrapperCopy.css({
                "left": leftPos + "px"
            });
            this.__wrapperLeft = leftPos;
        },
        /**
         * Update the UI of the cursor according to the __cursorPosition number
         * @name updateCursorPosition
         * @function
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        updateCursorPosition: function () {
            var tempA, leftPos, wrapperCopyEle, wrapperCopyEndSpan;
            var offsetLeft = this.__wrapperCopy.getHTMLElement().offsetLeft;

            // Cursor position is at the beginning
            // When there is no content, the cursor should sit at the left of the wrapper.
            if (this.__cursorPosition === 0) {
                this.__cursor.css({
                    "left": offsetLeft + "px"
                });
                this.updateLeftByCursor(0);
                return;
            }

            leftPos = 0;
            this.wrapperCopyWidth = this.__wrapperCopy.getHTMLElement().offsetWidth;

            if (this.__oldSamsung && this.__cursorPosition > 0) {
                wrapperCopyEle = this.__wrapperCopy.getHTMLElement();
                wrapperCopyEndSpan = wrapperCopyEle.lastChild;
                this.wrapperCopyWidth -= wrapperCopyEndSpan.clientWidth;
            }

            tempA = (this.wrapperCopyWidth + offsetLeft);

            if (tempA > this.__inputFieldWidth - 1) {
                // cursor will go out of inputField, force it back to the end position
                // modify the leftPos of both wrapper and wrapperCopy
                leftPos = this.__inputFieldWidth - 1;
                this.updateLeftByCursor(this.__inputFieldWidth - this.wrapperCopyWidth);
            } else if (tempA > 0) {
                // cursor still within the inputField area, only need to update the cursor position, the leftPos no need to change
                leftPos = tempA;
            } else {
                // cursor will go out of inputField, force it back to the beginning position
                // modify the leftPos of both wrapper and wrapperCopy
                leftPos = 0;
                this.updateLeftByCursor(-this.wrapperCopyWidth);
            }

            this.__cursor.css({
                "left": leftPos + "px"
            });

            this.__cursorLeftPosition = leftPos;
        },
        /**
         * Move the cursor in certain direction
         * @name moveCursor
         * @function
         * @param {String} direction either "left" or "right"
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        moveCursor: function (direction) {
            if (!this.__inputState) {
                return;
            }
            if (direction.toLowerCase() === "left") {
                this.setCursorPos(this.__cursorPosition - 1);
            } else if (direction.toLowerCase() === "right") {
                this.setCursorPos(this.__cursorPosition + 1);
            }
        },
        /**
         * Move the cursor to a specific position
         * @name setCursorPos
         * @function
         * @param {Number} pos cursor position
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        setCursorPos: function (pos) {
            if (pos > this.getText().length) {
                pos = this.getText().length;
            } else if (pos < 0) {
                pos = 0;
            }

            if (this.__cursorPosition !== pos) {
                this.__cursorPosition = pos;

                if (this.__cursorPosition === 0) {
                    this.__labelTextCopy = "";
                } else if (this.__isPassword) {
                    this.__labelTextCopy = util.truncate(this.__wrapper.getInnerText(), this.__cursorPosition, "");
                } else {
                    this.__labelTextCopy = util.truncate(this.__labelText, this.__cursorPosition, "");
                }
                this.__setElementText(this.__wrapperCopy, this.__labelTextCopy);
                this.updateCursorPosition();
            }
        },
        /**
         * Make the inputField ready for capturing
         * @name startInput
         * @function
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        startInput: function () {
            this.__inputState = true;
            if (!this.__inputFieldWidth) {
                this.__inputFieldWidth = this.container.getHTMLElement().clientWidth;
                this.updateLeft("add");
                this.updateCursorPosition();
            }
            this.__cursor.css({
                visibility: "visible"
            });
        },
        /**
         * Make the inputField non-capturing
         * @name stopInput
         * @function
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        stopInput: function () {
            this.__inputState = false;
            this.__cursor.css({
                visibility: "hidden"
            });

        },
        /**
         * Check if inputField is capturing
         * @name isCapturing
         * @function
         * @returns {Boolean} true if capturing; false otherwise
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @public
         */
        isCapturing: function () {
            return this.__inputState;
        },
        /**
         * @method
         * @memberof ax/ext/ui/input/InputDivImpl#
         * @protected
         */
        deinit: function () {
            //no need to super since it is just part of the input which doesn't inside the xdk hierarchy (no parent)
            //so deinit just go through the remove all listner and element deinit only. The Dom element deinit of input will be handled in the input.js
            this.container.deinit();
            this.__wrapper.deinit();
            this.__wrapperCopy.deinit();
            this.__cursor.deinit();
        }
    });
});
/**
 *
 * TextArea Native is a textarea which provide data input. It is compatible to handle click and moving the cursor by mouse.
 *
 * ###Dom Structure
 *      <div class="wgt-textArea wgt-textarea-native">
 *          <div class="container">
 *              <textarea class="wgt-textarea-native-ele" style="overflow: hidden;"></textarea>
 *          </div>
 *      </div>
 *
 * ###CSS
 * ####Structural CSS
 *      //To display the container with 100%.
 *      .wgt-textarea-native .container {
 *          width: 100%;
 *          height: 100%;
 *      }
 *      //to set the native the text area
 *      .wgt-textarea-native-ele {
 *           resize: none;
 *          margin: 0px;
 *          padding: 0px;
 *          border: 0px;
 *          outline: none;
 *          word-break: break-all;
 *          word-wrap: break-word;
 *          height: 100%;
 *          width: 100%;
 *          white-space: pre;
 *      }
 *
 * ####Customizable CSS
 *  .wgt-textArea - to set the size of the textarea
 * @class ax/ext/ui/input/TextAreaNativeImpl
 * @param {Object} opts The options object
 * @param {Number} opts.maxLength The available length of the input
 * @param {Boolean} opts.isPassword True if it is password and it will be "*" for the input
 * @param {String} opts.text The default text of the input
 * @param {String} opts.passwordChar The char to replace the "*" which is the default character.
 * @param {Boolean} opts.spanHack special break line handling on the specific platform/device
 * @param {String} opts.placeHolder To set the place holder
 */
define("ax/ext/ui/input/TextAreaNativeImpl", ["ax/class", "ax/Element", "ax/util", "ax/console", "ax/device", "ax/af/evt/type", "ax/device/shared/browser/keyMap", "./interface/InputImpl", "css!./css/TextAreaNativeImpl"], function (klass, Element, util, console, device, evtType, keyMap, InputImpl) {
    "use strict";
    return klass.create([InputImpl], {}, {
        /**
         * Reference to the actual XDK widget instance.
         * @private
         * @name __widgetInstance
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __widgetInstance: null,
        /**
         * Text area DOM element wrapped in XDK's element object.
         * @private
         * @name __textArea
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __textArea: null,
        /**
         * Text area DOM element.
         * @private
         * @name __element
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __element: null,
        /**
         * Current cursor positon.
         * @private
         * @name __cursorPosition
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __cursorPosition: null,
        /**
         * Text inside text area.
         * @private
         * @name __value
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __value: "",
        /**
         * Whether the native text area scroll bar should be shown.
         * @private
         * @name __isScrollbarShown
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __isScrollbarShown: null,
        /**
         * Whether this widget is marked for being inputted.
         * @private
         * @name __inputState
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __inputState: false,
        /**
         * Used when toggling inputState, so that can tell it is the first time being turned on.
         * @private
         * @name __inputStateFirstOn
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __inputStateFirstOn: true,
        /**
         * @private
         * @name __maxLength
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __maxLength: 200,
        /**
         * @private
         * @name __isPassword
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __isPassword: false,
        /**
         * @private
         * @name __passwordChar
         *@memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __passwordChar: null,
        /**
         * To store the place holder word
         * @private
         * @name __placeHolder
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __placeHolder: null,
        /**
         * Flag to see if placeHolder is set or not
         * @private
         * @name __placeHolder
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         */
        __isSetPlaceHolder: false,
        /**
         * @constructor
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @param {Object} opts The options object
         * @param {String} opts.text The preset text to use
         * @private
         */
        init: function (opts) {
            var self = this,
                handleClickFunc, handleMouseOut, handleKeyFunc, tempText, samsungHandleKeyFunc, handleTextEditKeyFunc, checkDangerousKeyFunc, upKey = keyMap.KEYBOARD_KEY.keyCode.UP,
                downKey = keyMap.KEYBOARD_KEY.keyCode.DOWN,
                leftKey = keyMap.KEYBOARD_KEY.keyCode.LEFT,
                rightKey = keyMap.KEYBOARD_KEY.keyCode.RIGHT,
                insertKey = keyMap.KEYBOARD_KEY.keyCode.INSERT,
                delKey = keyMap.KEYBOARD_KEY.keyCode.DELETE,
                homeKey = keyMap.KEYBOARD_KEY.keyCode.HOME,
                endKey = keyMap.KEYBOARD_KEY.keyCode.END,
                pageUpKey = keyMap.KEYBOARD_KEY.keyCode.PAGE_UP,
                pageDownKey = keyMap.KEYBOARD_KEY.keyCode.PAGE_DOWN,
                tabKey = keyMap.KEYBOARD_KEY.keyCode.TAB,
                backspaceKey = keyMap.KEYBOARD_KEY.keyCode.BACKSPACE;

            opts = opts || {};

            opts.root.addClass("wgt-textarea-native");

            this.__isPassword = opts.isPassword || false;
            this.__passwordChar = opts.passwordChar || "*";

            this.__widgetInstance = opts.widgetInstance;
            this.__placeHolder = opts.placeHolder || "";

            // Create dom structure
            this.container = new Element("div");
            this.container.addClass("container");

            this.__textArea = new Element("textarea");
            this.__textArea.addClass("wgt-textarea-native-ele");

            this.__element = this.__textArea.getHTMLElement();

            //XDK-1838
            //since it is part of the implementation of the textarea and thus append the current container to the root instead of create a new root in the component
            opts.root.append(this.container);
            this.container.append(this.__textArea);

            // End of create dom structure
            this.setMaxLength(util.isNumber(opts.maxLength) ? opts.maxLength : 200);
            tempText = opts.text || "";
            this.setText(tempText, tempText.length);
            this.hideScrollbar();

            checkDangerousKeyFunc = function (evt) {
                // checks dangerous keys that interacts with input. If there are any specific keys on the platforms, it needs to be included here
                switch (evt.keyCode) {
                case upKey:
                case downKey:
                case leftKey:
                case rightKey:
                case insertKey:
                case delKey:
                case homeKey:
                case endKey:
                case pageUpKey:
                case pageDownKey:
                case tabKey:
                case backspaceKey:
                    return true;
                default:
                    return false;
                }
            };
            // end of ugly keyboard related platform checking
            handleClickFunc = function (evt) {
                console.info("textAreaInternalNativeImpl: mouse click and event" + evt);
                console.info(self.__inputState);
                util.defer().then(function () {
                    //to make the cursor to be position 0 when there is place holder
                    if (self.__isSetPlaceHolder) {
                        self.setCursorPos(0);
                    } else {
                        self.__updateCursorPosition();
                    }
                    self.startInput();
                }).done();
                return true;
            };
            handleMouseOut = function (evt) {
                console.info(self.__inputState);
                console.info("textAreaInternalNativeImpl: mouse out and event" + evt);

                self.stopInput();
                return true;
            };
            handleKeyFunc = function (evt) {
                try {
                    evt.preventDefault();
                } catch (ex) {
                    ex.returnValue = false;
                }
                return true; // keep bubbling
            };
            handleTextEditKeyFunc = function (evt) {
                if (checkDangerousKeyFunc(evt)) { // catches dangerous keys that interacts with textarea
                    console.info("textAreaInternalNativeImpl: blocking text edit key:" + evt.keyCode);
                    try {
                        evt.preventDefault();
                    } catch (ex) {
                        ex.returnValue = false;
                    }
                }
                return true; // keep bubbling
            };
            switch (device.platform) {
            case "samsung":
                if (device.id.getFirmwareYear() === 2010) {
                    console.debug("textAreaInternalNativeImpl: samsung 2010 device detected, using special handler");
                    samsungHandleKeyFunc = function (evt) {
                        console.debug("textAreaInternalNativeImpl: samsung workaround handler in action...");
                        // === all these should be in vain ===
                        try {
                            evt.preventDefault();
                        } catch (ex) {
                            ex.returnValue = false;
                        }
                        // === end ===
                        self.setText(self.__value, self.__cursorPosition);
                        return true; // return false won't work as well
                    };
                    this.__textArea.addEventListener("keydown", samsungHandleKeyFunc); //workaround
                    break;
                }
                /* falls through */
            default:
                this.container.addEventListener("mousedown", handleClickFunc);
                this.container.addEventListener("mouseout", handleMouseOut);
                this.__textArea.addEventListener("keydown", handleTextEditKeyFunc);
                this.__textArea.addEventListener("keypress", handleKeyFunc);
                break;
            }
        },
        /**
         * Set the position of the cursor
         * @name setCursorPos
         * @function
         * @param {Number} pos The position to move to
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        setCursorPos: function (pos) {
            this.__cursorPosition = pos;
            //util.defer(util.bind(this.__setRealCursorPos, this));
            this.__setRealCursorPos();
            this.__resetScroll();
        },
        /**
         * Set the real cursor to go to position at this.__cursorPosition.
         * This method is also used as a work-around when some glitches causes real cursor to move to an incorrect position.
         * @name __setRealCursorPos
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @private
         */
        __setRealCursorPos: function () {
            var pos = this.__cursorPosition,
                ctrl = this.__element,
                matches, offset = 0,
                range;
            if (device.platform === "samsung" && device.id.getFirmwareYear() <= 2011) {
                console.debug("textAreaInternalNativeImpl: samsung 2011 or 2010 device detected");

                ctrl.focus();

                ctrl.selectionStart = pos;
            }
            // Chrome, FF/Gecko, IE 9+, Opera 8+
            // Samsung maple
            else if (ctrl.setSelectionRange) {
                console.debug("textAreaInternalNativeImpl: element has setSelectionRange()");
                // opera count a new line as \r\n, two characters
                if (device.platform === "opera" || device.platform === "sagem") {
                    matches = this.__value.substring(0, pos).match(/\n/g); // will be undefined if no match
                    offset = matches ? matches.length : 0;
                    console.debug("textAreaInternalNativeImpl: opera detected, found " + offset + " line breaks before cursor position");
                }

                ctrl.focus();
                try {
                    ctrl.setSelectionRange(pos + offset, pos + offset);
                } catch (e) {
                    console.debug("textAreaInternalNativeImpl: setSelectionRange() error:" + e.message);
                }
            }
            // IE and Opera under 10.5
            else if (ctrl.createTextRange) {
                console.debug("textAreaInternalNativeImpl: element has createTextRange()");
                range = ctrl.createTextRange();
                range.collapse(true);
                range.moveEnd("character", pos);
                range.moveStart("character", pos);
                range.select();
            }
        },
        /**
         * Get the position of the cursor
         * @name getCursorPos
         * @function
         * @returns {Number} position of cursor position
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        getCursorPos: function () {
            return this.__cursorPosition;
        },
        /**
         * Update the position of the cursor
         * @name __updateCursorPos
         * @function
         * @returns {Number} position of cursor position
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @private
         */
        __updateCursorPos: function () {
            //block the cursor update when place holder is set.
            if (this.__isSetPlaceHolder) {
                return;
            }

            var ctrl = this.__element,
                cursorPos = 0,
                selection;

            // IE 9+ and all other browsers
            if (ctrl.selectionStart || String(ctrl.selectionStart) === "0") {
                console.debug("textAreaInternalNativeImpl: element has selectionStart");
                cursorPos = ctrl.selectionStart;
            }
            // IE 9+ and all other browsers (normally the above already works)
            else if (document.getSelection) {
                console.debug("textAreaInternalNativeImpl: document has getSelection()");
                ctrl.focus();
                selection = document.getSelection().createRange();

                selection.moveStart("character", -ctrl.value.length);

                cursorPos = selection.text.length;
            }

            this.__cursorPosition = cursorPos;
            console.debug("textAreaInternalNativeImpl: __updateCursorPos() reports cursor position is " + cursorPos);
            return cursorPos;
        },
        /**
         * Forces the textarea to scroll cursor into view
         * @name __resetScroll
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @private
         */
        __resetScroll: function () {
            if (!this.__inputState) {
                return;
            }
            this.__element.blur();
            this.__element.focus();
        },
        /**
         * Insert character to the text area
         * @name insertChar
         * @function
         * @param {String} ch The character to insert to
         * @param {Number} insertPos The position character insert to
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        insertChar: function (ch, insertPos) {
            var newCursorPos, text;
            if (!this.__inputState) {
                return;
            }
            if (this.__value.length >= this.getMaxLength()) { // If length of text reach maximum, do nothing, and return
                return;
            }

            insertPos = util.isUndefined(insertPos) ? this.__cursorPosition : insertPos;
            text = this.getText();
            text = util.insert(text, insertPos, ch);
            newCursorPos = this.__cursorPosition < insertPos ? this.__cursorPosition : this.__cursorPosition + ch.length;
            this.setText(text, newCursorPos);
            //in huawei device, it is unable to update the cursor position into new line when at the end of the text area
            this.__element.blur();
            this.__element.focus();
        },
        /**
         * Delete the character before cursor in the text area
         * @name backspace
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        backspace: function () {
            if (!this.__inputState) {
                return;
            }
            if (this.__cursorPosition <= 0) { // If cursorPosition == 0, there must be no char before cursor, do nothing, and return
                return;
            }

            var text = this.getText(),
                newText = "";
            if (this.__cursorPosition === 1) { // Exception case when delete the first char while the string is have more than 1 char
                if (text.length > 1) {
                    newText = text.slice(this.__cursorPosition);
                }
            } else {
                if (text.length > 1) {
                    newText = text.slice(0, this.__cursorPosition - 1) + text.slice(this.__cursorPosition);
                }
            }
            this.setText(newText, this.__cursorPosition - 1);
        },
        /**
         * Delete the character after cursor in the text area
         * @name del
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        del: function () {
            if (!this.__inputState) {
                return;
            }
            var text = this.getText(),
                newText = "";
            if (this.__cursorPosition >= text.length) { // If cursorPosition == t.length, there must be no char after cursor, do nothing, and return
                return;
            }

            if (this.__cursorPosition === text.length - 1) { // Exception case when delete the last char
                newText = text.slice(0, text.length - 1);
            } else {
                newText = text.slice(0, this.__cursorPosition) + text.slice(this.__cursorPosition + 1, text.length);
            }
            this.setText(newText, this.__cursorPosition);
        },
        /**hi
         * Set max length of text area
         * @name setMaxLength
         * @function
         * @param {Integer} len The number of characters to set
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        setMaxLength: function (len) {
            this.__maxLength = len;
        },
        /**
         * Get max length of input field
         * @method getMaxLength
         * @returns {Integer} The maximum character length in the input
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        getMaxLength: function () {
            return this.__maxLength;
        },
        /**
         * Get the input field text
         * @name getText
         * @function
         * @returns {String} text Return the current text input value
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        getText: function () {
            return this.__value;
        },
        /**
         * add the place holder when the text value is ""
         * @method __addPlaceHolder
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @private
         */
        __addPlaceHolder: function () {
            this.__isSetPlaceHolder = true;
            //set the placeholder value into the textarea
            this.__element.value = this.__placeHolder;
            this.__textArea.addClass("placeholder");
            //make sure the cursor position to be at 0 position.
            this.__setRealCursorPos();
        },
        /**
         * remove the place holder when the text value is not ""
         * @method __removePlaceHolder
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @private
         */
        __removePlaceHolder: function () {
            this.__isSetPlaceHolder = false;
            this.__element.value = "";
            this.__textArea.removeClass("placeholder");
        },
        /**
         * Set text on the text area
         * @name setText
         * @function
         * @param {String} text
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        setText: function (text, cursorPosition) {
            if (!text) {
                text = "";
            }

            //original has nothing and set text, it will first remove the placeHolder
            if (this.__placeHolder !== null && this.getText() === "" && text !== "") {
                this.__removePlaceHolder();
            }

            var textChanged = false,
                len, passwordText = "",
                i;
            text = util.truncate(text, this.getMaxLength(), "");
            textChanged = (text !== this.__value);

            if (textChanged) {
                this.__value = text;

                if (this.__isPassword) {
                    len = text.length;
                    for (i = 0; i < len; i++) {
                        passwordText += this.__passwordChar;
                    }
                    this.__element.value = passwordText;
                } else {
                    this.__element.value = text;
                }
            }

            if (typeof (cursorPosition) === "number") {
                this.setCursorPos(Math.min(cursorPosition, this.getMaxLength()));
            }

            if (this.__inputState) {
                //this.__element.focus();
            }

            if (textChanged && this.__widgetInstance) {
                console.info("text changed in text area" + this.__value);
                this.__widgetInstance.dispatchEvent(evtType.TEXT_CHANGED, this.__value);
            }

            //if the text is empty, it will set the place holder
            if (this.__placeHolder !== null && text === "") {
                this.__addPlaceHolder();
            }
        },
        /**
         * Clear input field text
         * @name clearText
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        clearText: function () {
            this.setText("", 0);
        },
        /**
         * Move the cursor before this character
         * @name moveCursor
         * @function
         * @param {String} direction indicated direction
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        moveCursor: function (direction) {
            if (!this.__inputState) {
                return;
            }
            if (direction.toLowerCase() === "left") {
                if (this.__cursorPosition > 0) {
                    this.setCursorPos(this.__cursorPosition - 1);
                }
            } else if (direction.toLowerCase() === "right") {
                if (this.__cursorPosition < this.__value.length) {
                    this.setCursorPos(this.__cursorPosition + 1);
                }
            }
            util.defer().then(util.bind(this.__setRealCursorPos, this)).done();
        },
        /**
         * Make the textArea ready for capturing
         * @name startInput
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        startInput: function () {
            this.__element.focus();
            if (this.__inputState) {
                return;
            }
            // update the cursor position when first enter input status for safe-guarding
            // because updating the cursor position during initialization may not work
            if (this.__inputStateFirstOn) {
                this.__inputStateFirstOn = false;
                this.setCursorPos(this.__cursorPosition);
            }

            // some browser (webkit for e.g.) may move the real cursor around when arrow keys are pressed, and no known way to prevent it, not even preventDefault...
            // so here added a work-around to force reset again the cursor position to the correct position
            util.defer().then(util.bind(this.__setRealCursorPos, this));

            this.__inputState = true;
        },
        /**
         * Make the textArea non-capturing
         * @name stopInput
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        stopInput: function () {
            this.__element.blur();
            if (!this.__inputState) {
                return;
            }
            this.__inputState = false;
        },
        /**
         * Check if textArea is capturing
         * @name isCapturing
         * @function
         * @returns {Boolean} true if capturing; false otherwise
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        isCapturing: function () {
            return this.__inputState;
        },
        /**
         * Show the scroll bar of textarea
         * @name showScrollbar
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        showScrollbar: function () {
            this.__isScrollbarShown = true;
            return this.__textArea.css("overflow", "visible");
        },
        /**
         * Hide the scroll bar of textarea
         * @name hideScrollbar
         * @function
         * @memberof ax/ext/ui/input/TextAreaNativeImpl#
         * @public
         */
        hideScrollbar: function () {
            this.__isScrollbarShown = false;
            return this.__textArea.css("overflow", "hidden");
        },
        /**
         * @method
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @protected
         */
        deinit: function () {
            //no need to super since it is just part of the textArea which doesn't inside the xdk hierarchy (no parent)
            //so deinit just go through the remove all listner and element deinit only. The Dom element deinit of textArea will be handled in the textArea
            this.removeAllListeners();
            this.container.deinit();
            this.__textArea.deinit();
        }
    });
});
/**
 *
 * Input widget is an input field where the user can enter data.
 * There are two implementations in the input widget.
 *
 * 1. InputDiv use the div to enter the data
 * 2. InputNative use the input tag as an input field and then enter the data
 *
 *
 * Input will listen to the sEnv onKey event.It inputs the text when its state is startInput and the key value from the key event has an attribute called text.
 * So it will automatically update the display of the input without any handling. It should be worked in both hard keyboard and keyboard widget.
 *
 * In fact, when clicking the button within the keyboard widget, it will dispatch the key event with the text attribute.
 * After that, Input will then receive the event and insert the character when in startInput state.
 *
 * There are two states in the input (which are capturing and non-capturing state)
 * startInput will change the input to capturing state that allows char insert.
 * stopInput will change into the non-capturing state that disable char insert.
 *
 * @class ax/ext/ui/input/Input
 * @augments ax/ext/ui/input/InputBase
 * @fires module:ax/af/evt/type.TEXT_CHANGED
 * @param {Object} opts The options object
 * @param {String} opts.text The preset text to use
 * @param {Boolean} opts.isPassword To set the value shown to be password which make use of the opts.passwordChar
 * @param {Number} opts.maxLength The available length to be entered in the input.Default is 20
 * @param {String} opts.passwordChar The char to use when it is password. Default is "*"
 * @param {Number} opts.hideTimeout The time to hide the character to the passwordChar.
 * @param {Boolean} opts.useNative To use the native impl.
 *
 */
define([
    "ax/class",
    "ax/Element",
    "ax/device",
    "ax/util",
    "./InputDivImpl",
    "./InputNativeImpl",
    "ax/Env",
    "ax/af/evt/type",
    "ax/ext/ui/input/InputBase"
], function (
    klass,
    Element,
    device,
    util,
    InputDivImpl,
    InputNativeImpl,
    Env,
    evtType,
    InputBase
) {
    "use strict";

    return klass.create(InputBase, {}, {
        init: function (opts) {
            opts = opts || {};
            opts.maxLength = util.isNumber(opts.maxLength) ? opts.maxLength : 20;
            opts.css = (!opts.css) ? "wgt-input" : opts.css + " wgt-input";

            this._super(opts);

            opts.root.addClass("wgt-input");

            // useNative to override or default
            if (!opts.useNative) {
                this.__impl = new InputDivImpl(opts);
            } else {
                this.__impl = new InputNativeImpl(opts);
            }
        }
    });
});
/**
 *
 * TextArea is an field where the user can enter data.
 * There are two implementations in the input widget.
 *
 * 1. TextAreaDiv use the div to enter the data
 * 2. TextAreaNative use the input tag as an input field and then enter the data
 *
 *
 * TextArea will listen to the sEnv onKey event.It insert the text when its state is startInput and the key value from the key event has an attribute called text.
 * So it will automatically update the display of the input without any handling. It should be worked in both hard keyboard and keyboard widget.
 *
 * In fact, when clicking the button within the keyboard widget, it will dispatch the key event with the text attribute.
 * After that, Textarea will then receive the event and insert the character when in startInput state.
 *
 * There are two states in the textarea (which are capturing and non-capturing state)
 * startInput will change the input to capturing state that allows char insert.
 * stopInput will change into the non-capturing state that disable char insert.
 *
 * @class ax/ext/ui/input/TextArea
 * @augments ax/ext/ui/input/InputBase
 * @fires module:ax/af/evt/type.TEXT_CHANGED
 * @param {Object} opts The options object
 * @param {String} opts.text The preset text to use
 * @param {Boolean} opts.isPassword To set the value shown to be password which make use of the opts.passwordChar
 * @param {Number} opts.maxLength The available length to be entered in the input.Default is 200
 * @param {String} opts.passwordChar The char to use when it is password. Default is "*"
 * @param {Boolean} opts.useNative To use the native impl.
 * @param {String} opts.placeHolder To set the place holder
 *
 */
define([
    "ax/class",
    "ax/Element",
    "ax/device",
    "ax/util",
    "./TextAreaDivImpl",
    "./TextAreaNativeImpl",
    "ax/Env",
    "ax/af/evt/type",
    "ax/ext/ui/input/InputBase"
], function (
    klass,
    Element,
    device,
    util,
    TextAreaDivImpl,
    TextAreaNativeImpl,
    Env,
    evtType,
    InputBase
) {
    "use strict";
    return klass.create(InputBase, {}, {
        /**
         * @memberof ax/ext/ui/input/TextArea#
         * @param {Object} opts The options object
         * @param {String} opts.text The preset text to use
         * @param {boolean} opt.useNative Set to false if mouse clicks should not change the cursor position.
         *  This will also result in better backward-compatiblility for older devices.
         * @private
         */
        init: function (opts) {
            opts = opts || {};
            opts.maxLength = util.isNumber(opts.maxLength) ? opts.maxLength : 200;

            if (device.platform === "samsung" && device.id.getFirmwareYear() <= 2011) {
                opts.spanHack = device.id.getFirmwareYear() === 2010;
            }

            opts.css = (!opts.css) ? "wgt-textArea" : opts.css + " wgt-textArea";

            this._super(opts);

            // useNative to override or default
            if (!opts.useNative) {
                this.__impl = new TextAreaDivImpl(opts);
            } else {
                this.__impl = new TextAreaNativeImpl(opts);
            }
        }
    });
});
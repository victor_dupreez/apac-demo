/**
 * the input interface where the following features should be included in the input and text area widgets.
 *
 * There are two states in the input (which are input and non-input state)
 * startInput should change the input to input state that allows char insert.
 * stopInput should change into the non-input state that disable char insert.
 *
 * @deprecated as it is changed to another implementation using ax/ext/ui/input/interface/InputDisplay.
 * @class ax/ext/ui/input/IInput
 */
define("ax/ext/ui/input/IInput", ["ax/class", "ax/af/Component"], function (klass, Component) {
    "use strict";
    return klass.create(Component, {}, {
        /**
         * @method insertChar
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        insertChar: klass.abstractFn,
        /**
         * @method backspace
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        backspace: klass.abstractFn,
        /**
         * @method del
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        del: klass.abstractFn,
        /**
         * @method setMaxLength
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        setMaxLength: klass.abstractFn,
        /**
         * @method getText
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        getText: klass.abstractFn,
        /**
         * @method setText
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        setText: klass.abstractFn,
        /**
         * @method clearText
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        clearText: klass.abstractFn,
        /**
         * @method moveCursor
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        moveCursor: klass.abstractFn,
        /**
         * @method setCursorPos
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        setCursorPos: klass.abstractFn,
        /**
         * @method startInput
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        startInput: klass.abstractFn,
        /**
         * @method stopInput
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        stopInput: klass.abstractFn,
        /**
         * @method isInput
         * @memberof ax/ext/ui/input/IInput#
         * @public
         */
        isInput: klass.abstractFn
    });
});
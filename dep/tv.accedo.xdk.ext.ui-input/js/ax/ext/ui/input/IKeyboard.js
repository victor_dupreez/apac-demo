/**
 * @class ax/ext/ui/input/IKeyboard
 * @deprecated as it is removed from the keyboard, so this interface is no longer used.
 * @desc Keyboard Interface
 */
define("ax/ext/ui/input/IKeyboard", ["ax/class", "ax/af/Container"], function (klass, Container) {
    "use strict";
    return klass.create(Container, {}, {
        /**
         * To add a new key map
         * @name addKeyMap
         * @public
         * @param {Object} keyMap add the key map
         * @memberof ax/ext/ui/input/IKeyboard
         */
        addKeyMap: klass.abstractFn,
        /**
         * To add a new key map
         * @name addKeyMap
         * @public
         * @param {Object} keyMap add the key map
         * @memberof ax/ext/ui/input/IKeyboard
         */
        switchKeyMap: klass.abstractFn,
        /**
         * To get the current focus position of the keyboard. If it is not focus, it will return the last focus position
         * @name getFocusPosition
         * @param {String} [id] the id of the keyboard.If it is empty, it will return the current keyboard map focus position. e.g.Keyboard.DEFAULT is the default keyboard
         * @returns {Array} the position (X,Y coord in array) of the focus key e.g [0,0]
         * @public
         * @memberof ax/ext/ui/input/IKeyboard
         */
        getFocusPosition: klass.abstractFn,
        /**
         * To get the keyboard map set id
         * @name  getMapId
         * @returns {String} id the id the keyboard map set
         *  @memberof ax/ext/ui/input/IKeyboard
         * @public
         */
        getMapId: klass.abstractFn,
        /**
         * To get key object
         * @name  getKey
         * @param {Array} arr the coordinate of the key. e.g [0,-1] will return the last key in the first row
         * @returns {Object} obj object of the key
         * @memberof  ax/ext/ui/input/IKeyboard
         * @public
         */
        getKey: klass.abstractFn,
        /**
         * To remove the key map by the id
         * @name  removeKeyMap
         * @param {String} id the id of the keyboard.
         * @memberof  ax/ext/ui/input/IKeyboard
         * @public
         */
        removeKeyMap: klass.abstractFn,
        /**
         * To set the focus position of the keyboard
         * @name setFocusPosition
         * @param {String} id the id of the keyboard.
         * @param {Array} arr the array of the position that want to focus
         * @memberof ax/ext/ui/input/IKeyboard
         * @public
         */
        setFocusPosition: klass.abstractFn
    });
});
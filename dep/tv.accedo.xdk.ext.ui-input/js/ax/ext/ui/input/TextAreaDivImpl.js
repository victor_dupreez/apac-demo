/**
 *
 * TextArea Div is a textarea which provide data input. As it is a div, so it is unable to handle click and moving the cursor by mouse.
 *
 * ###Dom Structure
 *      <div class="wgt-textArea wgt-textArea-noclicktextarea" id="textArea">
 *          <div class="container">
 *              <div class="wrapper"></div>
 *              <div class="wrapperCopy"><span></span>
 *                  <div class="cursor" style="visibility: hidden;"></div>
 *              </div>
 *          </div>
 *      </div>
 *
 * ###CSS
 * ####Structural CSS
 *      .wrapper {
 *          position:absolute;
 *          left:0px;
 *          width:100%;
 *          word-break:break-all;
 *          word-wrap:break-word;
 *          white-space:pre-wrap;
 *      }
 *      .wrapperCopy {
 *          position: absolute;
 *          left: 0px;
 *          width: 100%;
 *      }
 *      .wrapperCopy span{
 *          word-break: break-all;
 *          word-wrap: break-word;
 *          white-space: pre-wrap;
 *      }
 *      .wrapperCopy .cursor{
 *            position: relative;
 *           display: inline-block;
 *           visibility: hidden;
 *       }
 *
 * ####Customizable CSS
 * * .wgt-textArea .cursor - to set the style cursor of the textarea
 * * .wgt-textArea -to set the size of textarea
 *
 * @class ax/ext/ui/input/TextAreaDivImpl
 * @param {Object} opts The options object
 * @param {Number} opts.maxLength The available length of the input
 * @param {Boolean} opts.isPassword True if it is password and it will be "*" for the input
 * @param {String} opts.text The default text of the input
 * @param {String} opts.passwordChar The char to replace the "*" which is the default character.
 * @param {Boolean} opts.spanHack special break line handling on the specific platform/device
 * @param {String} opts.placeHolder To set the place holder
 *
 */
define("ax/ext/ui/input/TextAreaDivImpl", ["ax/class", "ax/Element", "ax/util", "ax/console", "ax/af/evt/type", "./interface/InputImpl", "css!./css/TextAreaDivImpl"], function (klass, Element, util, console, evtType, InputImpl) {
    "use strict";
    return klass.create([InputImpl], {}, {
        /**
         * @private
         * @name __widgetInstance
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __widgetInstance: null,
        /**
         * @private
         * @name __inputHeight
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __inputHeight: null,
        /**
         * @private
         * @name __inputWidth
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __inputWidth: null,
        /**
         * @private
         * @name __lineHeight
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __lineHeight: null,
        /**
         * @private
         * @name __wrapper
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __wrapper: null,
        /**
         * @private
         * @name __wrapperCopy
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __wrapperCopy: null,
        /**
         * @private
         * @name __cursor
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __cursor: null,
        /**
         * @private
         * @name __cursorPosition
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __cursorPosition: null,
        /**
         * @private
         * @name __lastWrapperCopyHeight
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __lastWrapperCopyHeight: 0,
        /**
         * The actual text label for showing the text.
         * @private
         * @name __labelText
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __labelText: null,
        /**
         * The hidden text label only for displacing the cursor.
         * @private
         * @name __labelTextCopy
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __labelTextCopy: null,
        /**
         * The top of the text wrapper, serves for scrolling the text.
         * @private
         * @name __wrapperTop
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __wrapperTop: null,
        /**
         * The height of the text wrapper, serves for scrolling the text.
         * @private
         * @name __wrapperHeight
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __wrapperHeight: null,
        /**
         * The height of the text wrapper copy, serves for scrolling the text.
         * @private
         * @name __wrapperCopyHeight
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __wrapperCopyHeight: null,
        /**
         * The top of the text wrapper copy, serves for scrolling the text.
         * @private
         * @name __wrapperCopyTop
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __wrapperCopyTop: null,
        /**
         * Maximun text length
         * @private
         * @name __maxLength
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __maxLength: null,
        /**
         * Whether this widget is marked for being inputted
         * @private
         * @name ___inputState
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __inputState: false,
        /**
         * If set to true, each character has its own span tag, to workaround styling problem of some old platform
         * @private
         * @name __spanHack
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __spanHack: false,
        /**
         * to indicate it is password text area
         * @private
         * @name __isPassword
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __isPassword: false,
        /**
         * the password char
         * @private
         * @name __passwordChar
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __passwordChar: false,
        /**
         * @private
         * @name __placeHolder
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __placeHolder: null,
        /**
         * @private
         * @name __placeHolderDiv
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         */
        __placeHolderDiv: null,
        /**
         * @constructor
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @param {Object} opts The options object
         * @param {String} opts.text The text label to use for the button
         * @private
         */
        init: function (opts) {
            opts = opts || {};
            opts.root.addClass("wgt-textarea-div");

            this.__isPassword = opts.isPassword || false;
            this.__passwordChar = opts.passwordChar || "*";

            this.__spanHack = opts.spanHack || false;
            this.__labelText = opts.text || "";
            this.__cursorPosition = this.__labelText.length;
            this.__placeHolder = opts.placeHolder || "";

            opts.root.addClass("wgt-textArea-noclicktextarea");
            this.__widgetInstance = opts.widgetInstance;

            // Create div structure
            this.container = new Element("div");
            this.container.addClass("container");

            this.__wrapper = new Element("div");
            this.__wrapper.addClass("wrapper");

            this.__wrapperCopy = new Element("div");
            this.__wrapperCopy.addClass("wrapperCopy");

            this.__cursor = new Element("div");
            this.__cursor.addClass("cursor");

            //XDK-1838
            //since it is part of the implementation of the textarea and thus append the current container to the root instead of create a new root in the component
            opts.root.append(this.container);
            this.container.append(this.__wrapper);
            this.container.append(this.__wrapperCopy);

            if (this.__placeHolder !== "") {

                this.__placeHolderDiv = new Element("div");
                this.__placeHolderDiv.addClass("placeholder");
                this.__placeHolderDiv.setInnerText(this.__placeHolder);

                if (this.__labelText.length > 0) {
                    this.__placeHolderDiv.hide();
                }

                this.container.append(this.__placeHolderDiv);
            }

            this.setText(opts.text);
            this.setMaxLength(util.isNumber(opts.maxLength) ? opts.maxLength : 200);
        },
        /**
         * Insert character to the text area
         * @name insertChar
         * @function
         * @param {String} ch The character to insert
         * @param {Number} insertPos The position character insert to
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        insertChar: function (ch, insertPos) {
            if (!this.__inputState) {
                return;
            }
            if (this.__labelText.length >= this.getMaxLength()) { // If length of labelText reach maximum, do nothing, and return
                return;
            }

            insertPos = util.isUndefined(insertPos) ? this.__cursorPosition : insertPos;
            var text = this.getText();
            text = util.insert(text, insertPos, ch);
            this.__cursorPosition = this.__cursorPosition < insertPos ? this.__cursorPosition : this.__cursorPosition + ch.length;
            this.setText(text);
        },
        /**
         * Delete the character before caret in the text area
         * @name backspace
         * @function
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        backspace: function () {
            if (!this.__inputState) {
                return;
            }
            if (this.__cursorPosition <= 0) { // If cursorPosition == 0, there must be no char before cursor, do nothing, and return
                return;
            }

            var text = this.getText(),
                newText = "";
            if (this.__cursorPosition === 1) { // Exception case when delete the first char while the string is have more than 1 char
                if (text.length > 1) {
                    newText = text.slice(this.__cursorPosition);
                }
            } else {
                if (text.length > 1) {
                    newText = text.slice(0, this.__cursorPosition - 1) + text.slice(this.__cursorPosition);
                }
            }
            this.__cursorPosition--;
            this.setText(newText);
        },
        /**
         * Delete the character after cursor in the text area
         * @name del
         * @function
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        del: function () {
            if (!this.__inputState) {
                return;
            }
            var text = this.getText(),
                newText = "";
            if (this.__cursorPosition >= text.length) { // If cursorPosition == t.length, there must be no char after cursor, do nothing, and return
                return;
            }

            if (this.__cursorPosition === text.length - 1) { // Exception case when delete the last char
                newText = text.slice(0, text.length - 1);
            } else {
                newText = text.slice(0, this.__cursorPosition) + text.slice(this.__cursorPosition + 1, text.length);
            }
            this.setText(newText);
        },
        /**
         * Set max length of text area
         * @name setMaxLength
         * @function
         * @param {Integer} len The number of characters to set
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        setMaxLength: function (len) {
            this.__maxLength = len;
        },
        /**
         * Get max length of input field
         * @method getMaxLength
         * @returns {Integer}  The maximum character length in the textArea
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        getMaxLength: function () {
            return this.__maxLength;
        },
        /**
         * Get the text value
         * @name getText
         * @function
         * @returns {String} labelText Return the text value
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        getText: function () {
            return this.__labelText;
        },
        /**
         * Set text on the text area
         * @name setText
         * @function
         * @param {String} text
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        setText: function (text) {
            if (!text) {
                text = "";
            }

            var textChanged = (text !== this.__labelText),
                len, wrapperText = "",
                i, wrapperCopyText = "";
            if (text.length > this.getMaxLength()) {
                this.__cursorPosition = this.getMaxLength();
            }
            text = util.truncate(text, this.getMaxLength(), "");
            if (this.__spanHack) {
                console.debug("TextAreaInternalDivImpl: spanHack enabled, special break line handling required...");
                len = text.length;
                if (this.__isPassword) {
                    for (i = 0; i < len; i++) {
                        wrapperText += "<span>" + this.__passwordChar + "</span>";
                        if (i === this.__cursorPosition - 1) {
                            wrapperCopyText = wrapperText;
                        }
                    }
                } else {
                    for (i = 0; i < len; i++) {
                        wrapperText += "<span>" + util.encodeHTML(text.substring(i, i + 1)) + "</span>";
                        if (i === this.__cursorPosition - 1) {
                            wrapperCopyText = wrapperText;
                        }
                    }
                }

                wrapperText = "<span>" + wrapperText + "</span>";
            } else if (this.__isPassword) {
                this.__labelText = text;
                len = this.__labelText.length;
                for (i = 0; i < len; i++) {
                    wrapperText += this.__passwordChar;
                }
                wrapperCopyText = util.truncate(wrapperText, this.__cursorPosition, "");
            } else {
                wrapperText = util.encodeHTML(text);
                if (this.__cursorPosition === 0) {
                    wrapperCopyText = "";
                } else {
                    wrapperCopyText = util.encodeHTML(util.truncate(text, this.__cursorPosition, ""));
                }
            }

            this.__wrapper.setInnerHTML(wrapperText);
            this.__wrapperCopy.setInnerHTML("<span>" + wrapperCopyText + "</span>");
            this.__wrapperCopy.append(this.__cursor);

            this.updateTop();

            this.__labelText = text;
            this.__labelTextCopy = util.truncate(text, this.__cursorPosition, "");


            if (!textChanged) {
                return;
            }

            if (this.__placeHolderDiv) {
                if (this.__labelText.length > 0) {
                    this.__placeHolderDiv.hide();
                } else {
                    this.__placeHolderDiv.show();
                }
            }

            if (this.__widgetInstance) {
                console.info("text changed in text area" + this.__labelText);
                this.__widgetInstance.dispatchEvent(evtType.TEXT_CHANGED, this.__labelText);
            }

        },
        /**
         * Clear text area text
         * @name clearText
         * @function
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        clearText: function () {
            this.setText("");
            this.__cursorPosition = 0;
        },
        /**
         * Update the top position of both this.wrapper and this.wrapperCopy due to cursor movement
         * @name updateTop
         * @function
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        updateTop: function () {
            if (util.isNull(this.__inputHeight) || this.__inputHeight === 0) { // dimensions has not been measured
                // update dimensions
                this.__inputHeight = this.container.getHTMLElement().clientHeight;
                this.__inputWidth = this.container.getHTMLElement().clientWidth;
                this.__lineHeight = this.__cursor.getHTMLElement().clientHeight; // good enough approximation
            }

            this.__wrapperHeight = this.__wrapper.getHTMLElement().clientHeight;
            this.__wrapperTop = this.__wrapper.getHTMLElement().offsetTop;
            this.__wrapperCopyHeight = this.__wrapperCopy.getHTMLElement().clientHeight;
            this.__wrapperCopyTop = this.__wrapperCopy.getHTMLElement().offsetTop;

            if (this.__wrapperCopyHeight === this.__lastWrapperCopyHeight) {
                return; // caret stays on the same line
            }

            var topPos, isCursorDown = (this.__wrapperCopyHeight > this.__lastWrapperCopyHeight),
                maxHeight = Math.max(this.__wrapperHeight, this.__wrapperCopyHeight);

            this.__lastWrapperCopyHeight = this.__wrapperCopyHeight;

            // Perform checking to ensure the cursor is within the textArea visually
            if (this.__wrapperCopyHeight <= this.__inputHeight) {
                if (isCursorDown) {
                    return; // after caret move, wrapper copy still not long, safe to do nothing
                }
                topPos = 0; // going up, wrapper copy can fit in container
            } else if (isCursorDown) {
                if (this.__wrapperCopyHeight + this.__wrapperCopyTop <= this.__inputHeight) {
                    return; // cursor still in range, do nothing
                }
                topPos = Math.max(
                    this.__inputHeight - this.__wrapperCopyHeight - this.__lineHeight, // place the caret into view
                    this.__inputHeight - maxHeight // wrapper bottom does not go higher than container bottom
                );
            } else { // cursor is going up
                if (this.__wrapperCopyHeight + this.__wrapperCopyTop >= this.__lineHeight) {
                    return; // cursor still in range, do nothing
                }
                topPos = Math.min(-this.__wrapperCopyHeight + 2 * this.__lineHeight, // place the caret into view
                    0 // don't go too low
                );
            }


            if (!util.isUndefined(topPos)) {
                this.updateTopByPx(topPos);
            }
            return;
        },
        /**
         * Update top position
         * @name updateTopByPx
         * @function
         * @param {Float} topPos desired cursor topPos
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        updateTopByPx: function (topPos) {
            this.__wrapper.css("top", topPos + "px");
            this.__wrapperCopy.css("top", topPos + "px");
            this.__wrapperTop = topPos;
        },
        /**
         * Move the cursor in a specified direction
         * @name moveCursor
         * @function
         * @param {String} direction indicated direction
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        moveCursor: function (direction) {
            if (!this.__inputState) {
                return;
            }
            if (direction.toLowerCase() === "left") {
                this.setCursorPos(this.__cursorPosition - 1);
            } else if (direction.toLowerCase() === "right") {
                this.setCursorPos(this.__cursorPosition + 1);
            }
        },
        /**
         * Set the position of the cursor
         * @name setCursorPos
         * @function
         * @param {Number} pos The position to move to
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        setCursorPos: function (pos) {
            if (pos > this.getText().length) {
                pos = this.getText().length;
            } else if (pos < 0) {
                pos = 0;
            }

            this.__cursorPosition = pos;
            if (this.__cursorPosition === 0) {
                this.__labelTextCopy = "";
            } else {
                this.__labelTextCopy = util.truncate(this.__labelText, this.__cursorPosition, "");
            }

            var wrapperCopyText = "",
                len, i;
            if (this.__spanHack) {
                console.debug("TextAreaInternalDivImpl: spanHack enabled, special break line handling required...");
                len = this.__labelTextCopy.length;
                if (this.__isPassword) {
                    for (i = 0; i < len; i++) {
                        wrapperCopyText += "<span>" + this.__passwordChar + "</span>";
                    }
                } else {
                    for (i = 0; i < len; i++) {
                        wrapperCopyText += "<span>" + util.encodeHTML(this.__labelTextCopy.substring(i, i + 1)) + "</span>";
                    }
                }

            } else if (this.__isPassword) {
                len = this.__labelTextCopy.length;
                for (i = 0; i < len; i++) {
                    wrapperCopyText += this.__passwordChar;
                }
            } else {
                wrapperCopyText = util.encodeHTML(this.__labelTextCopy);
            }

            this.__wrapperCopy.setInnerHTML("<span>" + wrapperCopyText + "</span>");
            this.__wrapperCopy.append(this.__cursor);

            this.updateTop();
        },
        /**
         * Make the textArea ready for capturing
         * @name startInput
         * @function
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        startInput: function () {
            this.__cursor.css("visibility", "visible");
            this.__inputState = true;
            if (util.isNull(this.__inputHeight) || this.__inputHeight === 0) { // dimensions has not been measured
                // update dimensions and top position
                this.updateTop();
            }
        },
        /**
         * Make the textArea non-capturing
         * @name stopInput
         * @function
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        stopInput: function () {
            this.__cursor.css("visibility", "hidden");
            this.__inputState = false;
        },
        /**
         * Check if textArea is capturing
         * @name isCapturing
         * @function
         * @returns {Boolean} true if capturing; false otherwise
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @public
         */
        isCapturing: function () {
            return this.__inputState;
        },
        /**
         * @method
         * @memberof ax/ext/ui/input/TextAreaDivImpl#
         * @protected
         */
        deinit: function () {
            //no need to super since it is just part of the textArea which doesn't inside the xdk hierarchy (no parent)
            //so deinit just go through the remove all listner and element deinit only. The Dom element deinit of textArea will be handled in the textArea
            this.removeAllListeners();
            this.container.deinit();
            this.__wrapper.deinit();
            this.__wrapperCopy.deinit();
            this.__cursor.deinit();
        }
    });
});
/**
 * the input implementation interface where the following features should be included in the input and text area implementation. 
 * Implementation will be used in the InputDisplay interface e.g input and textarea.
 *
 *
 * @class ax/ext/ui/input/interface/InputImpl
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("ax/ext/ui/input/interface/InputImpl", ["ax/Interface"], function (Interface) {
    "use strict";
    return Interface.create("InputImpl", {
        /**
         * Insert character to the text area
         * @name insertChar
         * @function
         * @abstract
         * @param {String} char The character to insert
         * @param {Number} insertPos The position character insert to
         * @method insertChar
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        insertChar: ["char", "insertPos"],
        /**
         * Delete the character before caret in the text area
         * @name backspace
         * @function
         * @abstract
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        backspace: [],
        /**
         * Delete the character after caret in the text area
         * @name del
         * @function
         * @abstract
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        del: [],
        /**
         * Set max length of input field
         * @name setMaxLength
         * @function
         * @abstract
         * @param {Integer} length The number of characters to set
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        setMaxLength: ["length"],
        /**
         * Get the max length of input field
         * @name getMaxLength
         * @function
         * @abstract
         * @returns {Integer} The number of characters to set
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        getMaxLength: [],
        /**
         * Get the text value
         * @name getText
         * @function
         * @abstract
         * @returns {String} labelText Return the text value
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        getText: [],
        /**
         * Set text on the text area
         * @name setText
         * @function
         * @abstract
         * @param {String} text
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        setText: ["text"],
        /**
         * Clear text area text
         * @name clearText
         * @function
         * @abstract
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        clearText: [],
        /**
         * Move the cursor in a specified direction
         * @name moveCursor
         * @function
         * @abstract
         * @param {String} direction indicated direction
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        moveCursor: ["direction"],
        /**
         * Move the cursor to a specific position
         * @name setCursorPos
         * @function
         * @abstract
         * @param {Number} pos cursor position
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        setCursorPos: ["pos"],
        /**
         * Make the textArea ready for input
         * @name startInput
         * @function
         * @abstract
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        startInput: [],
        /**
         * Make the textArea inactive
         * @name stopInput
         * @function
         * @abstract
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        stopInput: [],
        /**
         * Check if textArea is capturing
         * @name isCapturing
         * @function
         * @abstract
         * @returns {Boolean} true if capturing; false otherwise
         * @memberof ax/ext/ui/input/interface/InputImpl#
         * @public
         */
        isCapturing: []
    });
});
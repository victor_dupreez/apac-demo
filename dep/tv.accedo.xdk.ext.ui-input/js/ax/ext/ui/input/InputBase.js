/**
 *
 * The input base for the input, textArea related widgets.
 * It will support implementations for several strategies, e.g native input and div input implementaion.
 * It dedicates the function to the those implemenation.
 *
 * @class ax/ext/ui/input/InputBase
 * @augments ax/ext/ui/input/interface/InputDisplay
 * @param {Object} opts The options object
 * @param {String} opts.text The preset text to use
 * @param {Boolean} opts.isPassword To set the value shown to be password which make use of the opts.passwordChar
 * @param {Number} opts.maxLength The available length to be entered in the input.Default is 200
 * @param {String} opts.passwordChar The char to use when it is password. Default is "*"
 * @param {Boolean} opts.useNative To use the native impl.
 * @param {String} opts.placeHolder To set the place holder
 *
 */
define([
    "ax/class",
    "./interface/InputDisplay",
    "ax/Element",
    "ax/device",
    "ax/util",
    "./InputDivImpl",
    "./InputNativeImpl",
    "ax/Env",
    "ax/af/evt/type",
    "ax/af/Component"
], function (
    klass,
    IInput,
    Element,
    device,
    util,
    InputDivImpl,
    InputNativeImpl,
    Env,
    evtType,
    Component
) {
    "use strict";

    return klass.create(Component, [IInput], {}, {
        /**
         * Internal implementation object who carries out the real work
         * @name __impl
         * @memberof ax/ext/ui/input/InputBase#
         * @private
         */
        __impl: null,
        /**
         * to check if add event listener to the env on Key
         * @name __impl
         * @memberof ax/ext/ui/input/InputBase#
         * @private
         */
        __initEnvOnKey: false,
        /**
         * the binded on key function listening to sEnv
         * @name __impl
         * @memberof ax/ext/ui/input/InputBase#
         * @private
         */
        __bindedOnKey: null,
        /**
         * blocked key list.
         * @name __blockedKeyList
         * @memberof ax/ext/ui/input/InputBase#
         * @private
         */
        __blockedKeyList: [],
        init: function (opts) {
            opts = opts || {};

            opts.text = opts.text || "";
            opts.isPassword = opts.isPassword || false;
            opts.focusable = opts.focusable || false;
            opts.placeHolder = opts.placeHolder || "";


            if (util.isUndefined(opts.clickable)) {
                opts.clickable = true; // default clickable
            }

            opts.passwordChar = opts.passwordChar || "*";
            opts.hideTimeout = opts.hideTimeout || 0;

            opts.root = new Element("div");
            opts.widgetInstance = this; // internal implementation needs the for event firing, as this is the "real component"

            if (util.isUndefined(opts.useNative)) {
                opts.useNative = true;
            }

            switch (device.platform) {
            case "tizen":
            case "toshiba":
            case "ps3":
            case "playstation":
            case "sony":
            case "panasonic":
            case "humax":
                // the above will bring up the IME keyboard
            case "wiiu":
                // wii brings up software keyboard, it doesn't work well with our native implementation
            case "tcl":
            case "hisense":
            case "amazon":
            case "android":
                // android device not support native because it will pop up a IME keyboard which may mess up with the natvie input.   
            case "sharp":
            case "opera":
            case "webos":
                // webos devices are not compatible with the IME. if use IME and then use input api, it will mess up the input.
                opts.useNative = false;
                break;
            case "samsung":
                if (device.id.getFirmwareYear() <= 2011) {
                    opts.useNative = false;
                    break;
                }
                // 2012 use native implementation
            }

            this.__bindedOnKey = util.bind(function (obj) {
                //to ignore the blocked key
                if (this.__blockedKeyList.indexOf(obj.id) > -1) {
                    return true;
                }

                // to perform action when there is text inside
                if (this.isCapturing() && obj.text) {
                    this.insertChar(obj.text);
                }

                return true;
            }, this);

            this._super(opts);
        },
        /**
         * Add the blocked key into the input. If a key is added, it will be ignored and won't insert the text by that key(s).
         * @method addBlockedKey
         * @param {String|String[]} key id of keys(s) to be blocked.
         * @memberof ax/ext/ui/input/InputBase#
         * @example
         * input.addBlockedKey([vKey.KEY_0.id, vKey.KEY_1.id]);
         * @public
         */
        addBlockedKey: function (key) {
            var curBlockedKeyList = [];

            if (util.isString(key)) {
                curBlockedKeyList.push(key);
            } else if (util.isArray(key)) {
                curBlockedKeyList = key;
            }

            util.each(curBlockedKeyList, util.bind(function (item) {
                if (this.__blockedKeyList.indexOf(item) === -1) {
                    this.__blockedKeyList.push(item);
                }
            }, this));
        },
        /**
         * Remove the blocked key from the input
         * @method removeBlockedKey
         * @param {String|String[]} key id of keys(s) to be unblocked.
         * @memberof ax/ext/ui/input/InputBase#
         * @example
         * input.removeBlockedKey([vKey.KEY_0.id]);
         * @public
         */
        removeBlockedKey: function (key) {
            var removeBlockedKeyList = [];

            if (util.isString(key)) {
                removeBlockedKeyList.push(key);
            } else if (util.isArray(key)) {
                removeBlockedKeyList = key;
            }

            util.each(removeBlockedKeyList, util.bind(function (item) {
                var position = this.__blockedKeyList.indexOf(item);

                if (position > -1) {
                    this.__blockedKeyList.splice(position, 1);
                }
            }, this));
        },
        /**
         * Set blocked key, it will override the previous blocked key setting
         * @method setBlockedKey
         * @param {String|String[]} key id of keys(s) to be unblocked.
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        setBlockedKey: function (key) {
            this.__blockedKeyList = [];
            this.addBlockedKey(key);
        },
        /**
         * Insert character to the text area
         * @name insertChar
         * @function
         * @param {String} ch The character to insert
         * @param {Number} insertPos The position character insert to
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        insertChar: function (ch, insertPos) {
            this.__impl.insertChar(ch, insertPos);
        },
        /**
         * Delete the character before caret in the text area
         * @name backspace
         * @function
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        backspace: function () {
            this.__impl.backspace();
        },
        /**
         * Delete the character after caret in the text area
         * @name del
         * @function
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        del: function () {
            this.__impl.del();
        },
        /**
         * Set max length of input field
         * @name setMaxLength
         * @function
         * @param {Integer} l The number of characters to set
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        setMaxLength: function (l) {
            this.__impl.setMaxLength(l);
        },
        /**
         * Get max length of input field
         * @name getMaxLength
         * @function
         * @returns {Integer} The max length of character supported in the input
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        getMaxLength: function () {
            return this.__impl.getMaxLength();
        },
        /**
         * Get the text value
         * @name getText
         * @function
         * @returns {String} labelText Return the text value
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        getText: function () {
            return this.__impl.getText();
        },
        /**
         * Set text on the text area
         * @name setText
         * @function
         * @param {String} text
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        setText: function (text) {
            this.__impl.setText(text);
        },
        /**
         * Clear text area text
         * @name clearText
         * @function
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        clearText: function () {
            this.__impl.clearText();
        },
        /**
         * Set selection on the text area
         * @name setSelection
         * @function
         * @memberof ax/ext/ui/input/InputBase#
         * @deprecated
         * @public
         */
        setSelection: function () {
            this.getRoot().addClass("selected");
        },
        /**
         * Remove selection from the text area
         * @name removeSelection
         * @function
         * @memberof ax/ext/ui/input/InputBase#
         * @deprecated
         * @public
         */
        removeSelection: function () {
            this.getRoot().removeClass("selected");
        },
        /**
         * Check if text area is selected
         * @name isSelected
         * @function
         * @returns {Boolean} true if selected; false otherwise
         * @deprecated
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        isSelected: function () {
            return this.getRoot().hasClass("selected");
        },
        /**
         * Move the cursor in a specified direction
         * @name moveCursor
         * @function
         * @param {String} direction indicated direction
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        moveCursor: function (direction) {
            this.__impl.moveCursor(direction);
        },
        /**
         * Move the cursor to a specific position
         * @name setCursorPos
         * @function
         * @param {Number} pos cursor position
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        setCursorPos: function (pos) {
            var len = this.getText().length;
            if (pos > len) {
                pos = len;
            } else if (pos < 0) {
                pos = 0;
            }
            this.__impl.setCursorPos(pos);
        },
        /**
         * Make the inputField ready for capturing
         * @name startInput
         * @function
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        startInput: function () {
            this.getRoot().addClass("input");
            this.__impl.startInput();

            //only add at the first time startInput and remove when deinit the instance
            if (!this.__initEnvOnKey) {
                var sEnv = Env.singleton();
                sEnv.addEventListener(sEnv.EVT_ONKEY, this.__bindedOnKey);
                this.__initEnvOnKey = true;
            }
        },
        /**
         * Make the inputField non-capturing
         * @name stopInput
         * @function
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        stopInput: function () {
            this.getRoot().removeClass("input");
            this.__impl.stopInput();
        },
        /**
         * [Backward Compatible only] Check if inputField is capturing
         * @name isInput
         * @function
         * @deprecated
         * @returns {Boolean} true if capturing; false otherwise
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        isInput: function () {
            return this.isCapturing();
        },
        /**
         * Check if inputField is capturing
         * @name isCapturing
         * @function
         * @returns {Boolean} true if capturing; false otherwise
         * @memberof ax/ext/ui/input/InputBase#
         * @public
         */
        isCapturing: function () {
            return this.__impl.isCapturing();
        },
        /**
         * @method
         * @memberof ax/ext/ui/input/InputBase#
         * @protected
         */
        deinit: function () {
            //perform the input deinit and the implmentation deinit like elements
            this._super();
            this.__impl.deinit();

            // remove the onKey event handler
            var sEnv = Env.singleton();
            sEnv.removeEventListener(sEnv.EVT_ONKEY, this.__bindedOnKey);
            this.__initEnvOnKey = false;
        }
    });
});
/**
 * Keyboard widget is mainly handling the navigation of the button inside the keyboard
 *
 * It makes of the keyMaps to set the buttons and specific functions and input.
 *
 * Keyboard widget will firstly go through the keyMaps which is the config the keyboard. Then it will create the buttons with respect to the config.
 * After that, it will generate the navigation map which determine the position and places inside the keyboard. Navigation map will be used to determine the up/down/left/right
 * in the keyboard. It is different from the keyMap config as some button may occupy more than 1 rowSpan or 1 column Span.
 *
 * Besides, there are some common features that provided
 *
 * 1. Handling the capital letter / sysmbols
 * 2. Handling the button functions
 * 3. Handling on the edge / special navigation
 * 4. Linking to the input
 *
 *  ####1.Handling the capital letter / sysmbols
 *  To work on this, multiple set of key maps are needed. One for capital letters and another set for small letter. Please refer to the follow examples.
 *  When pressing on specific button, you can handle the function and then switch the keyMap set.
 *
 *  ####2. Handling the button functions
 *  Sometimes we may need to handle the button like delete, switch keyboard. The onHandler function callback  where you can get the id from what you set to the
 *  button in the keyMaps and thus switching cases to handle switch keyMap or ask input to delete character. So special functions can be handled.
 *
 *  {@link ax/ext/ui/input/Keyboard.onHandler}
 *
 *  ####3. Handling on the edge / special navigation
 *  Sometimes we may need to loop the keyboard that edge is connected or handle the special cases when reaching the edge / connect several keyboards together.
 *  We can use callback function onEdge, which send you the direction and the current position.
 *
 *  {@link ax/ext/ui/input/Keyboard.onEdge} it will give more examples on how to use onEdge
 *
 *
 *  ####4. Linking to the input
 *  Normally keyboard is linked to the input/textarea. In fact, each button can set the display(the layout) and input(value of button) attribute.
 *  If the button is string, both display and input are the same.
 *
 *  Keyboard widget will dispatch a key event when the button is click and it has input value. So input/textArea will receive the key press event.
 *
 *
 * ###Dom Structure
 *      <div class="wgt-keyboard">
 *          <div class="wgt-keyboard-container-default">
 *              <div class="wgt-keyboard-comp row-begin"><div class="wgt-label">a</div></div>
 *              <div class="wgt-keyboard-comp"><div class="wgt-label">b</div></div>
 *              <div class="wgt-keyboard-comp"><div class="wgt-label">c</div></div>
 *              <div class="wgt-keyboard-comp"><div class="wgt-label">d</div></div>
 *              <div class="wgt-keyboard-comp"><div class="wgt-label">e</div></div>
 *              <div class="wgt-keyboard-comp row-end"><div class="wgt-label">f</div></div>
 *              <div class="wgt-keyboard-comp row-begin"><div class="wgt-label">g</div></div>
 *              <div class="wgt-keyboard-comp"><div class="wgt-label">h</div></div>
 *              <div class="wgt-keyboard-comp"><div class="wgt-label">i</div></div>
 *              <div class="wgt-keyboard-comp"><div class="wgt-label">j</div></div>
 *              <div class="wgt-keyboard-comp"><div class="wgt-label">k</div></div>
 *              <div class="wgt-keyboard-comp row-end"><div class="wgt-label">l</div>
 *          </div>
 *      <div>
 *
 * ###CSS
 * ####Structural CSS
 *      .wgt-keyboard-comp.row-begin {
 *          clear: left;
 *      }
 *      .wgt-keyboard-comp {
 *          float: left;
 *      }
 * @class ax/ext/ui/input/Keyboard
 * @param {Object} opts The options object
 * @param {Array} opts.focusPosition the focus position in the keyboard with respect to the navigation map.
 * @param {Object} opts.keyMaps multiple keymap sets in form of object of 2d arrays. Please make sure each keyMap has an id and the first one should be named as "default".
 * e.g keyMaps:{ default, [["a","b","c"],["d","e","f"]]};
 * @see {@link ax/ext/ui/input/Keyboard.addKeyMap}  To know the attribute of setting each button of maps
 * @example <caption>The following example</caption>
 *   klass:keyboard
 *   id: "#kb",
 *   useLastFocus: true,
 *   keyMaps: {
 *       "default":
 *                [
 *                  ["a", "b", "c", "d", "e", "f"],
 *                  ["g", "h", "i", "j", "k", "l"],
 *                  ["m", "n", "o", "p", "q", "r"],
 *                  ["s", "t", "u", "v", "w", "x"],
 *                   ["y", "z", {display: new ImageBtn({bg: "left-arrow"}), keyId: "keyboard:left-arrow"}, {display: new ImageBtn({bg: "right-arrow"}), keyId: "keyboard:right-arrow"}, {display: new ImageBtn({bg: "delete"}), keyId: "keyboard:delete"}, {display: new ImageBtn({bg: "enter"}), keyId: "keyboard:enter"}],
 *                  [{display: "ABC", keyId: "keyboard:FromabcToABC"}, {display: "12#", keyId: "keyboard:FromabcToNumber"}, {display: "space", input: " ", colSpan: 2}, {display: "search", keyId: "keyboard:search", colSpan: 2}]
 *              ],
 *      "capital":
 *              [
 *                  ["A", "B", "C", "D", "E", "F"],
 *                  ["G", "H", "I", "J", "K", "L"],
 *                  ["M", "N", "O", "P", "Q", "R"],
 *                  ["S", "T", "U", "V", "W", "X"],
 *                  ["Y", "Z", {display: new ImageBtn({bg: "left-arrow"}), keyId: "keyboard:left-arrow"}, {display: new ImageBtn({bg: "right-arrow"}), keyId: "keyboard:right-arrow"}, {display: new ImageBtn({bg: "delete"}), keyId: "keyboard:delete"}, {display: new ImageBtn({bg: "enter"}), keyId: "keyboard:enter"}],
 *                  [{display: "abc", keyId: "keyboard:FromABCToabc", defaultFocus: true}, {display: "12#", keyId: "keyboard:FromABCToNumber"}, {display: "space", input: " ", colSpan: 2}, {display: "search", keyId: "keyboard:search", colSpan: 2}]
 *              ],
 *      "number":
 *              [
 *                  ["1", "2", "3", "4", "5", "6"],
 *                  ["7", "8", "9", "0", ":", ";"],
 *                  [".", ",", "?", "!", "'", "\""],
 *                  ["@", "&", "$", "#", "(", ")"],
 *                  ["-", "\\", {display: new ImageBtn({bg: "left-arrow"}), keyId: "keyboard:left-arrow"}, {display: new ImageBtn({bg: "right-arrow"}), keyId: "keyboard:right-arrow"}, {display: new ImageBtn({bg: "delete"}), keyId: "keyboard:delete"}, {display: new ImageBtn({bg: "enter"}), keyId: "keyboard:enter"}],
 *                  [{display: "ABC", keyId: "keyboard:FromNumberToABC"}, {display: "abc", keyId: "keyboard:FromNumberToabc", defaultFocus: true}, {display: "space", input: " ", colSpan: 2}, {display: "search", keyId: "keyboard:search", colSpan: 2}]
 *              ]
 *   }
 */
define("ax/ext/ui/input/Keyboard", ["ax/class", "ax/console", "ax/util", "ax/Element", "ax/ext/ui/Button", "ax/af/evt/type", "ax/af/Container", "ax/af/focusManager", "ax/device", "ax/Env", "ax/af/Component", "css!./css/Keyboard"], function (klass, console, util, Element, Button, evtType, Container, focusManager, device, Env, Component) {
    "use strict";
    var sEnv = Env.singleton(),
        keyboard;

    keyboard = klass.create(Container, {
        /**
         * The default ID of the keyboard
         * @name DEFAULT
         * @memberof ax/ext/ui/input/Keyboard
         * @public
         */
        DEFAULT: "default"
    }, {
        /**
         * To store the keyMap according to the setting in the view. It is a map of keys according to the setting and the map set
         * @name __keyMaps
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __keyMaps: {},
        /**
         * To store the HTML container of each keyboard set according to the id
         * @name __containers
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __containers: {},
        /**
         * To store the navigation map of each keyboard set according to the id
         * @name __containers
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __navMaps: {},
        /**
         * To store the current ID of the map
         * @name __mapId
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __mapId: null,
        /**
         * To store the focus position of each focus position of each map
         * @name __focusPosition
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __focusPosition: {},
        /**
         * To set handling callback when the keyboard reach the edge. Developer can set the focus on the another button or do some handling
         *  on the keyboard according to the design
         * @name  onEdge
         * @method
         * @param {Function} cb the callback function. It will then pass the direction and the coordinate of the button.
         * @example
         *  <caption>To set the loop</caption>
         *  this.keyboard.onEdge = function(direction,pos){
         *  //direction will be "nextUp","nextRight","nextLeft","nextDown"
         *  //pos will be the coordinate like [0,8]
         *     if(direction === "nextRight"){
         *        //to set the loop when pressing nextRight on the last item of each row
         *        fm.focus(this.getKey([pos[0],0]));
         *        //return true will then stop the handling in the focusManager.
         *        return false;
         *     }
         *     if(direction === "nextLeft"){
         *        //to set the loop when pressing nextLeft on the first item of each row
         *        fm.focus(this.getKey([pos[0],-1]));
         *        return false;
         *     }
         * }
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        onEdge: null,
        /**
         * To set callback when there are keyId set on that key
         * @name  onHandler
         * @method
         * @param {Function} cb the callback function. It will then pass the id of the keyId and the button object itself
         * @returns {Boolean} true if stop and then do nothing.
         * @example
         *  <caption>handle when there are function</caption>
         *  this.keyboard.onHandler = function(keyId,button){
         *  //id will be the keyId set in the view
         *  //key will be the button object that press enter
         *    if( keyId === "keyboard:switch"){
         *      this.switchKeyMap("default");
         *      return true;
         *    }
         * }
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        onHandler: null,
        /**
         * To store current row number of the focus
         * @name __curRow
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __curRow: 0,
        /**
         * To store current col number of the focus
         * @name __curCol
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __curCol: 0,
        init: function (opts) {
            opts.focusPosition = opts.focusPosition || [0, 0];
            opts.keyMaps = opts.keyMaps || {};
            opts.forwardFocus = opts.useLastFocus || undefined;
            opts.root = new Element("div", {
                "class": "wgt-keyboard"
            });
            this._super(opts);
            this.__curRow = opts.focusPosition[0];
            this.__curCol = opts.focusPosition[1];
            util.each(opts.keyMaps, util.bind(function (obj) {
                this.addKeyMap(obj.key, obj.value);
            }, this));
            this.addEventListener(evtType.KEY, util.bind(this._keyHandler, this), true);
            this.addEventListener(evtType.CLICK, util.bind(this._keyHandler, this));
            if (opts.defaultMap) {
                this.switchKeyMap(opts.defaultMap);
            } else if (this.__keyMaps["default"]) {
                this.switchKeyMap("default");
            } else {
                console.warn("no default maps");
            }

        },
        /**
         * To get the current focus position of the keyboard. If it is not focus, it will return the last focus position
         * @name getFocusPosition
         * @param {String} [id] the id of the keyboard.If it is empty, it will return the current keyboard map focus position.
         * e.g.Keyboard.DEFAULT is the original keyboard set in the view
         * @return {Array} the position of the focus key e.g [0,0]
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        getFocusPosition: function (id) {
            if (util.isUndefined(id)) {
                return this.__focusPosition[this.getMapId()];
            }
            return this.__focusPosition[id];
        },
        /**
         * To get the current focus key of the keyboard.
         * @name getFocusedKey
         * @param {String} [id] the id of the keyboard.If it is empty, it will return the current keyboard map key.
         * e.g.Keyboard.DEFAULT is the original keyboard set in the view
         * @return {Object} the key object focused
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        getFocusedKey: function (id) {
            return this.getKey(this.getFocusPosition(id));
        },
        /**
         * To set the focus position of the keyboard
         * @name setFocusPosition
         * @method
         * @param {String} id the id of the keyboard.
         * @param {Array} arr the array of the position that want to focus
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        setFocusPosition: function (id, arr) {
            var row = arr[0] || 0,
                col = arr[1] || 0;
            this.__focusPosition[id] = [row, col];
        },
        /**
         * To add another key map to the keyboard so that it can swap between
         * @name addKeyMap
         * @method
         * @param {String} id the id of the keyboard set.
         * @param {Array} keyMapConfig the array of object
         * @param {Object|String} keyMapConfig.obj each key can be set as string which will be as a 1x1 button with the same key and value, or can be set as object
         * e.g {display: "123", keyId: "keyboard-FromabcToNumber", css: "key-123"}
         * @param {String|ax/af/Component} obj.display the UI input which show on the screen or Component Instance (e.g like extending the current button and use image instead of label)
         * @param {String} obj.input the button value when pressed. It will directly dispatch to the appsRoot
         * @param {Boolean} obj.defaultFocus true if it is the default focus of that keymap
         * @param {String} obj.keyId the indicate when that key is pressed
         * @param {Boolean} obj.disable To disable the key.Default will be false.
         * @param {Number} obj.colSpan the number of column occupied. Default will be 1.
         * @param {Number} obj.rowSpan the number of row occupied. Default will be 1.
         * @param {Number} obj.css the css of the specific key.
         * @example
         * [
         [{display: "q", defaultFocus: true}, "w", "e", "r", "t", "y", "u", "i", "o", "p"],
         [{display: "a", css: "key-a"}, "s", "d", "f", "g", "h", "j", "k", "l"],
         [{display: new ImageBtn({bg: "up"}), keyId: "keyboard:capital", css: "key-up"}, "z", "x", "c", "v", "b", "n", "m", {display: new ImageBtn({bg: "delete"}), keyId: "keyboard:delete"}],
         [{display: "123", keyId: "keyboard:fromabcToNumber", css: "key-123"}, {display: "fn", keyId: "keyboard:fn"}, {display: "space", input: " ", colSpan: 5, css: "key-space"}, {display: "return", colSpan: 2, keyId: "keyboard:return"}]
         ]
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        addKeyMap: function (id, keyMapConfig) {
            var obj = this.__initKeyMap(keyMapConfig);
            if (this.__keyMaps[id]) {
                console.warn("Specific key map " + id + " is already exist.");
            }

            this.__keyMaps[id] = obj.map;
            this.__containers[id] = obj.container;
            this.__focusPosition[id] = obj.defaultFocus;
            obj.container.addClass("wgt-keyboard-container-" + id);
            //then it will run through the keymap and create specific navigation map which will be used when navigation.
            //keyMapObj which is the setting of array with the setting and obj.map is array of created button
            this.__navMaps[id] = this.__initNavMap(keyMapConfig, obj.map);
            this.setFocusPosition(id, this.__focusPosition[id]);
        },
        /**
         * To remove the key map by the id
         * @name  removeKeyMap
         * @method
         * @param {String} id the id of the keyboard.
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        removeKeyMap: function (id) {
            if (this.getMapId() === id) {
                console.warn("unable to remove the current displayMap");
                return;
            }

            //remove the id stuff
            delete this.__keyMaps[id];
            delete this.__containers[id];
            delete this.__navMaps[id];
            delete this.__focusPosition[id];
        },
        /**
         * To swith the key Map to specific id
         * @name  switchKeyMap
         * @method
         * @param {String} id the id of the keyboard.
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        switchKeyMap: function (id) {
            if (!this.__keyMaps[id] || !this.__containers[id] || !this.__navMaps[id]) {
                console.warn("Unable to load the id " + id + " as it is not exist");
                return false;
            }
            this.detach(this.__containers[this.__mapId]);
            this.__mapId = id;
            this.attach(this.__containers[this.__mapId]);
            console.info("Switch successfully" + id);
            this.setOption("forwardFocus", this.getKey(this.getFocusPosition(id)));
            if (focusManager.isCompChildFocused(this)) {
                focusManager.focus(this);
            }
            return true;
        },
        /**
         * To init the keyMap from the config
         * @name  __initKeyMap
         * @method
         * @param {Array} keyMapConfig the config of the keyMap
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __initKeyMap: function (keyMapConfig) {
            var map = [],
                button, originalConfig, tempKey, defaultFocus = [0, 0],
                container = new Container();
            util.each(keyMapConfig, function (row, i) {
                map[i] = [];
                util.each(row, function (item, j) {
                    if (item.defaultFocus) {
                        defaultFocus = [i, j];
                    }
                    button = this.__initKey(item);

                    //handle css about the position and setObject row and col number
                    if (j === 0) {
                        button.addClass("row-begin");
                        //button.getRoot().css("clear", "left");
                    }
                    if (j === row.length - 1) {
                        button.addClass("row-end");
                        //button.getRoot().css("float", "left");
                    }
                    map[i][j] = button;
                    //standardise the config of each key
                    originalConfig = keyMapConfig[i][j];
                    if (util.isString(originalConfig)) {
                        tempKey = originalConfig;
                        originalConfig = {};
                        keyMapConfig[i][j] = originalConfig;
                        originalConfig.display = tempKey;
                    }
                    //originalConfig.input = originalConfig.input || originalConfig.display;
                    originalConfig.colSpan = originalConfig.colSpan || 1;
                    originalConfig.rowSpan = originalConfig.rowSpan || 1;
                    container.attach(button, null, null);
                }, this);
            }, this);
            return {
                map: map,
                container: container,
                defaultFocus: defaultFocus
            };
        },
        /**
         * To init each key
         * @name  __initKey
         * @method
         * @param {Object} attr the attribute of each key
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __initKey: function (attr) {
            var obj;
            //string, and create a default button
            if (util.isString(attr) || (attr && !(attr.display instanceof Component))) {
                obj = this.__createButton(attr);
            } else if (attr.display instanceof Component) {
                obj = attr.display;
                if (attr.css) {
                    obj.addClass(attr.css);
                }
            } else {
                console.warn("unable to construct or assign the key into the keyboad");
                return;
            }

            obj.addClass("wgt-keyboard-comp");

            if (!util.isUndefined(attr.rowSpan)) {
                obj.addClass("rowSpan-" + attr.rowSpan);
            }
            if (!util.isUndefined(attr.colSpan)) {
                obj.addClass("colSpan-" + attr.colSpan);
            }
            return obj;
        },
        /**
         * To create each button
         * @name  __createButton
         * @method
         * @param  {Object} attr the attribute of each key
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __createButton: function (attr) {
            var btnObject, display;
            if (!attr) {
                console.warn("unable to create the button without any setting");
                return null;
            }

            if (util.isString(attr)) {
                btnObject = new Button({
                    text: attr,
                    isPureText: true
                });
                btnObject.getRoot().setClass("");
            } else if (attr) {
                display = attr.display;
                btnObject = new Button({
                    text: display,
                    isPureText: true
                });
                if (attr.css) {
                    btnObject.getRoot().setClass(attr.css);
                }
                if (attr.disable) {
                    btnObject.disable();
                }
            }

            if (btnObject) {
                return btnObject;
            } else {
                console.warn("unable to create the obj" + attr);
                return null;
            }

        },
        /**
         * To init the navigation map and assign back the button to the map
         * @name  ____initNavMap
         * @method
         * @param {Object} keyMapConfig the attribute of each key
         * @param {Object} keyMap the process key map
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __initNavMap: function (keyMapConfig, keyMap) {
            var navMap = [],
                k, colPlaced, rowSpan, colSpan, set;
            util.each(keyMapConfig, function (row, i) {
                if (!navMap[i]) {
                    navMap[i] = [];
                }
                util.each(row, function (item, j) {
                    rowSpan = item.rowSpan || 1;
                    colSpan = item.colSpan || 1;
                    set = false;
                    //to save the x and y position of the button which will be easier to locate them later.
                    keyMapConfig[i][j].__y = i;
                    keyMap[i][j].__keyboard_config = keyMapConfig[i][j];
                    //to find the first free empty space
                    for (k = 0; k < navMap[i].length; k++) {
                        if (!navMap[i][k]) {
                            navMap[i][k] = keyMap[i][j];
                            keyMapConfig[i][j].__x = k;
                            set = true;
                            break;
                        }
                    }

                    if (!set) {
                        navMap[i].push(keyMap[i][j]);
                        keyMapConfig[i][j].__x = navMap[i].length - 1;
                        set = true;
                    }

                    colPlaced = keyMapConfig[i][j].__x;
                    if (rowSpan > 1) {

                        if (colSpan > 1) {
                            for (k = 1; k < rowSpan; k++) {
                                if (!navMap[i + k]) {
                                    navMap[i + k] = [];
                                }
                                for (k = colPlaced; k < colSpan; k++) {
                                    navMap[i][k] = keyMap[i][j];
                                }
                            }
                        } else {
                            //only has row  Span
                            for (k = 1; k < rowSpan; k++) {
                                if (!navMap[i + k]) {
                                    navMap[i + k] = [];
                                }
                                navMap[i + k][colPlaced] = keyMap[i][j];
                            }
                        }
                    } else {
                        if (colSpan > 1) {
                            //only has colSpan
                            for (k = 1; k < colSpan; k++) {
                                if (!navMap[i][colPlaced + k]) {
                                    navMap[i][colPlaced + k] = keyMap[i][j];
                                }
                            }
                        }
                    }
                });
            });
            return navMap;
        },
        /**
         * To handle the key when keyboard recevie the key
         * @name  _keyHandler
         * @method
         * @param {Object} evt the event received
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        _keyHandler: function (evt) {
            var vKey = device.vKey,
                evtKey = evt.id,
                curFocus, curRow, curCol, targetBtn, btn, direction = null,
                ret, keyInfo;
            curFocus = focusManager.getCurFocus();
            //do Nothing when there is no focus
            if (!curFocus) {
                return;
            }

            curRow = this.__curRow;
            curCol = this.__curCol;
            //to ensure the row and col are correct with respect to the focus key (mainly when mouse case)
            if (curCol > curFocus.__keyboard_config.__x + curFocus.__keyboard_config.colSpan - 1 || curCol < curFocus.__keyboard_config.__x) {
                curCol = curFocus.__keyboard_config.__x;
            }

            if (curRow > curFocus.__keyboard_config.__y + curFocus.__keyboard_config.rowSpan - 1 || curRow < curFocus.__keyboard_config.__y) {
                curRow = curFocus.__keyboard_config.__y;
            }

            this.setFocusPosition(this.getMapId(), [curRow, curCol]);
            if ((evt.type === evtType.KEY && evtKey === vKey.OK.id) || evt.type === evtType.CLICK) {
                this.__focusPosition[this.getMapId()] = [curRow, curCol];
                keyInfo = this.getKeyInfo([curRow, curCol]);
                //to check if there are keyId in the object. If return true and then stop dispatch and don't input anything
                if (keyInfo.keyId && this.onHandler) {
                    ret = this.onHandler(keyInfo.keyId, this.getKey([curRow, curCol]));
                    if (ret === false) {
                        return false;
                    } else {
                        //to dispatch event to let the xdk know key input
                        sEnv.dispatchEvent(sEnv.EVT_ONKEY, {
                            id: keyInfo.keyId,
                            text: ret || undefined,
                            source: "soft-keyboard"
                        });
                        return false;
                    }
                }

                //to dispatch event to let the xdk know key input
                sEnv.dispatchEvent(sEnv.EVT_ONKEY, {
                    id: keyInfo.display,
                    text: keyInfo.input || keyInfo.display,
                    source: "soft-keyboard"
                });
                return false;
            }

            if (evtKey === vKey.DOWN.id || evtKey === vKey.UP.id || evtKey === vKey.LEFT.id || evtKey === vKey.RIGHT.id) {
                //to handle the navigaton based on the current position of the focus item
                do {
                    switch (evtKey) {
                    case vKey.DOWN.id:
                        direction = !direction ? "nextDown" : direction;
                        curRow++;
                        break;
                    case vKey.UP.id:
                        curRow--;
                        direction = !direction ? "nextUp" : direction;
                        break;
                    case vKey.LEFT.id:
                        curCol--;
                        direction = !direction ? "nextLeft" : direction;
                        break;
                    case vKey.RIGHT.id:
                        curCol++;
                        direction = !direction ? "nextRight" : direction;
                        break;
                    }
                    targetBtn = this.getKey([curRow, curCol]);
                    if (!targetBtn || curCol < 0 || curRow < 0) {
                        break;
                    }
                    if (targetBtn && targetBtn !== curFocus && focusManager.isCompFocusable(targetBtn)) {
                        btn = targetBtn;
                        break;
                    }
                } while (true);

                if (btn) {
                    this.__curRow = curRow;
                    this.__curCol = curCol;
                    this.__focusPosition[this.getMapId()] = [curRow, curCol];
                    focusManager.focus(btn);
                    // no need for default action
                    evt.preventDefault();
                    return false;
                }

                console.info("reach edge of the keyboard and dedicate to developer to handle");
                //to dedicate to the develop to handle the edge end.
                if (this.onEdge) {
                    ret = this.onEdge(direction, this.getFocusPosition());
                    if (ret === false) {
                        evt.preventDefault();
                        return false;
                    }
                }
                return;
            }
        },
        /**
         * To get the config info of the specific key
         * @name  getKeyInfo
         * @method
         * @param {Array} arr the coordinate of the key
         * @returns {Object} obj config object
         * @returns {Number} obj.rowSpan
         * @returns {Number} obj.colSpan
         * @returns {String} obj.display
         * @returns {String} obj.input
         * @returns {String} obj.keyId
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        getKeyInfo: function (arr) {
            var btn = this.getKey(arr);
            return btn.__keyboard_config;
        },
        /**
         * To get key object
         * @name  getKey
         * @method
         * @param {Array} arr the coordinate of the key. e.g [0,-1] will return the last key in the first row
         * @returns {Object} obj object of the key
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        getKey: function (arr) {
            return this.__getChildByNaviMap(arr);
        },
        /**
         * To get the children by the navigation map
         * @name  __getChildByNaviMap
         * @method
         * @param {Array} arr the coordinate of the key. e.g [0,-1] will return the last key in the first row
         * @returns {Object} obj object of the key
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __getChildByNaviMap: function (arr) {
            return this.__getChildByMap(this.__navMaps, arr);
        },
        /**
         * To get the children by the keyMap which is the same when config. It is different from navigation map as some key may have more than 1 span in col/row
         * @name  __getChildByKeyMap
         * @method
         * @param {Array} arr the coordinate of the key. e.g [0,-1] will return the last key in the first row
         * @returns {Object} obj object of the key
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __getChildByKeyMap: function (arr) {
            return this.__getChildByMap(this.__keyMaps, arr);
        },
        /**
         * To get the children by the map and array@
         * @name  __getChildByKeyMap
         * @method
         * @param {Object} target the type of the target object e.g Navi map or keymaps
         * @param {Array} arr the coordinate of the key. e.g [0,-1] will return the last key in the first row
         * @returns {Object} obj object of the key
         * @memberof ax/ext/ui/input/Keyboard#
         * @private
         */
        __getChildByMap: function (target, arr) {
            var row = arr[0],
                col = arr[1],
                obj;
            if (row < 0 || col < -1) {
                return null;
            }
            obj = target[this.getMapId()];
            if (obj && obj.length <= row) {
                return null;
            }
            obj = target[this.getMapId()][row];
            if (col === -1) {
                col = target[this.getMapId()][row].length - 1;
            }
            if (obj && obj.length <= col) {
                return null;
            }
            obj = target[this.getMapId()][row][col];
            if (obj) {
                return obj;
            }
            return null;
        },
        /**
         * To get the keyboard map set id
         * @name  getMapId
         * @method
         * @returns {String} id the id the keyboard map set
         * @memberof ax/ext/ui/input/Keyboard#
         * @public
         */
        getMapId: function () {
            return this.__mapId;
        }
    });
    return keyboard;
});
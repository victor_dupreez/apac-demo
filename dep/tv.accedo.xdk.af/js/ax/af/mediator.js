/**
 * Mediator allows communication between different components within the application through channel subscription.  
 * @module ax/af/mediator
 * @since 2.1
 * @author Thomas Lee <thomas.lee@accedo.tv>
 * @example
 *  // Subscribe the channel and console log when receive the message in the channel "hi"
 *  var handler = function(i) {
 *      console.log('hey ' + i)
 *  };
 *
 *  mediator.subscribe('hi', handler);
 *
 *  // publish the message "bob" on the channel "hi" and those handler will receive the message including the above subscriber
 *  mediator.publish('hi', 'bob');
 *
 *  //remove the handler and then above handler won't receive any events.
 *  mediator.unsubscribe('hi', handler);
 */
define("ax/af/mediator", ["ax/EventDispatcher", "ax/util"], function(EventDispatcher, util) {
    "use strict";
    var mediator = new EventDispatcher();
    return {
        /**
         * Publish the message via the channel
         * @method publish
         * @param {String} type event type to dispatch
         * @param {Object} data the event data object
         * @return {Boolean} result dispatch success for true
         * @memberof module:ax/af/mediator
         * @public
         */
        publish: function (type, data) {
            //all errors should be handled gracefully in mediator
            mediator.dispatchEvent.apply(mediator, [type, data, true]);
        },
        /**
         * Subscribe the message via the channel
         * @method subscribe
         * @param {String} type event type
         * @param {Function} handler event listener function
         * @param {Boolean} [once] set to true if listener will be called only once
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @memberof module:ax/af/mediator
         * @public
         */
        subscribe: util.bind(mediator.addEventListener, mediator),

        /**
         * Unsubscribe the channel
         * @method unsubscribe
         * @param {String} type Event type
         * @param {Function} handler event listener function.  If not provided, all listeners will be removed for the specific type.
         * @returns {ax/EventDispatcher} current event dispatcher, for easier chaining
         * @memberof module:ax/af/mediator
         * @public
         */
        unsubscribe: util.bind(mediator.removeEventListener, mediator)
    };
});

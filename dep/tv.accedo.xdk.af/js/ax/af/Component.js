/**
 * Abstract component used as a base for all UI components.
 * This is the abstract component function that all UI object, it can be containers or components, inherrits from.
 * Inherits from {@link ax/af/evt/DualEventDispatcher}
 * @class ax/af/Component
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @param {Object} opts Options
 * @param {ax/Element} opts.root The DOM root element (the container for this component)
 * @param {ax/af/Component} [opts.parent] The DOM parent element
 * @param {ax/af/Component.PLACE_BEFORE | ax/af/Component.PLACE_AFTER | ax/af/Component.PLACE_APPEND | ax/af/Component.PLACE_PREPEND} [opts.placement] - position the child should be placed. By default {@link ax/af/Component.PLACE_APPEND}.
 * @param {ax/af/Component} [opts.marker] - must be a child of current container when using PLACE_AFTER/PLACE_BEFORE, the placement of child will be in relative to this marker.
 * @param {Boolean} opts.clickable Whether this component is clickable
 * @param {Boolean} opts.focusable Whether this component is focusable
 * @param {Boolean} opts.mouseFocusableOnly To set this component is only focusable by mouse
 * @param {String | ax/af/Component} opts.forwardOnMouseOff the focus will be redirect to.If there is no setting, it will redirect to the last non mouse focus when it is mouseFocusableOnly.
 * @param {Boolean} opts.enterAsClick Whether to treat enter keys as clicks for this component
 * @param {String} opts.id The ID of this component
 * @param {Boolean|ax/af/mvc/ModelRef} [opts.visible] the visibility of the component
 *
 * @fires module:ax/af/evt/type.SHOWN
 * @fires module:ax/af/evt/type.HIDDEN
 * @fires module:ax/af/evt/type.ACTIVED
 * @fires module:ax/af/evt/type.INACTIVED
 * @fires module:ax/af/evt/type.DETACH
 * @fires module:ax/af/evt/type.DETACHED_FROM_DOM
 * @fires module:ax/af/evt/type.ATTACH
 * @fires module:ax/af/evt/type.ATTACHED_TO_DOM
 * @fires module:ax/af/evt/type.ATTACHED_TO_CONTROLLER
 * @fires module:ax/af/evt/type.DETACHED_FROM_CONTROLLER
 * @fires module:ax/af/evt/type.FOCUS
 * @fires module:ax/af/evt/type.BLUR
 * @fires module:ax/af/evt/type.KEY
 * @fires module:ax/af/evt/type.CLICK
 * @fires module:ax/af/evt/type.MOUSEOVER
 */
define("ax/af/Component", [
    "ax/class",
    "ax/af/evt/DualEventDispatcher",
    "ax/af/evt/eventManager",
    "ax/util",
    "ax/af/evt/type",
    "ax/af/evt/Event",
    "ax/core",
    "ax/Element",
    "ax/device",
    "ax/af/mvc/ModelRef",
    "ax/exception",
    "ax/console"
], function (
    klass,
    EventDispatcher,
    evtMgr,
    util,
    evtType,
    Event,
    core,
    Element,
    device,
    ModelRef,
    exception,
    console
) {
    "use strict";
    return klass.create(EventDispatcher, {
        /**
         * Placement: before another sibling
         * @constant
         * @name PLACE_BEFORE
         * @memberof ax/af/Component
         *
         */
        PLACE_BEFORE: "before",
        /**
         * Placement: after another sibling
         * @constant
         * @name PLACE_AFTER
         * @memberof ax/af/Component
         */
        PLACE_AFTER: "after",
        /**
         * Placement: append
         * @constant
         * @name PLACE_APPEND
         * @memberof ax/af/Component
         */
        PLACE_APPEND: "append",
        /**
         * Placement: prepend
         * @constant
         * @name PLACE_PREPEND
         * @memberof ax/af/Component
         */
        PLACE_PREPEND: "prepend"
    }, {
        /**
         * Currrently disabled
         * @protected
         * @memberof ax/af/Component
         */
        _disabled: false,
        /**
         * Currrently hidden
         * @protected
         * @memberof ax/af/Component
         */
        _hidden: false,
        /**
         * One of the ancestors is currrently hidden
         * @protected
         * @memberof ax/af/Component
         */
        _ancestorHidden: false,
        /**
         * Currrently in DOM tree
         * @protected
         * @memberof ax/af/Component
         */
        _inDOMTree: false,
        /**
         * Parent component.
         * @protected
         * @memberof ax/af/Component
         */
        _parent: null,
        /**
         * Root element.
         * @protected
         * @memberof ax/af/Component
         */
        _root: null,
        /**
         * Attached controller.
         * @protected
         * @memberof ax/af/Component
         */
        _controller: null,
        /**
         * Options object.
         * @protected
         * @memberof ax/af/Component
         */
        _opts: null,
        /**
         * The registery for wrapped listeners
         * @private
         * @memberof ax/af/Component
         */
        __wrappedListeners: {},
        /**
         * The visibility model reference
         * @private
         * @memberof ax/af/Component
         */
        __visibilityModelRef: null,
        /**
         * The visibility listener
         * @private
         * @memberof ax/af/Component
         */
        __visibilityListener: null,
        /**
         * This function initiates a component using the options given in the constructor.
         * @method
         * @protected
         * @memberof ax/af/Component#
         */
        init: function (opts) {
            var placement, marker;

            opts = opts || {};

            opts.root = opts.root || new Element("div");

            this._super(opts);

            this._root = opts.root;
            if (this._root) {
                this._root.getHTMLElement().__comp = this;
            }

            this._parent = opts.parent;

            this._opts = util.clone(opts, true);

            if (this._root && this._parent) {
                placement = (opts.placement) ? opts.placement : this.PLACE_APPEND;
                marker = (opts.marker) ? opts.marker : undefined;
                this._parent.attach(this, placement, marker);
            }

            if (opts.id) {
                this.setId(opts.id);
            }

            if (opts.css) {
                this._root.addClass(opts.css);
            }

            if (!util.isUndefined(opts.visible)) {
                this.setVisibility(opts.visible);
            }

            this.postInit();
        },
        /**
         * To do after the initialization, when all children are readily included
         * @method
         * @memberof ax/af/Component#
         * @protected
         */
        postInit: function () {
            var style = this._opts.style;
            if (style && style.display && style.display.toLowerCase() === "none") {
                this.hide();
            }
        },
        /**
         * This function signals this component will not be further used, and should do clean up to prevent any memory leaks.
         * @method
         * @protected
         * @memberof ax/af/Component#
         */
        deinit: function () {
            this.detach();

            this._super();

            delete this._root.getHTMLElement().__comp;
            this._root.deinit(); // remove listeners that are attached to DOM element
            this._root = null;
        },
        /**
         * Returns true if component is clickable, false otherwise.
         * @method
         * @public
         * @return {Boolean}
         * @memberof ax/af/Component#
         */
        isClickable: function () {
            return this.getOption("clickable", false);
        },
        /**
         * Checks if component is disabled.
         * @method
         * @public
         * @return {Boolean} true if disabled, false otherwise.
         * @memberof ax/af/Component#
         */
        isDisabled: function () {
            return this._disabled;
        },
        /**
         * Sets disabled state to true. Also adds CSS class "disabled".
         * @method
         * @public
         * @return {ax/af/Component} The component itself, for chaining
         * @memberof ax/af/Component#
         */
        disable: function () {
            this._disabled = true;
            this._root.addClass("disabled");
            return this;
        },
        /**
         * Sets disabled state to true. Also adds CSS class "disabled".
         * @method
         * @public
         * @return {ax/af/Component} The component itself, for chaining
         * @memberof ax/af/Component#
         */
        enable: function () {
            this._disabled = false;
            this._root.removeClass("disabled");
            return this;
        },
        /**
         * This function returns whether this component is hidden or not
         * @name isHidden
         * @function
         * @public
         * @memberof ax/af/Component#
         */
        isHidden: function () {
            return this._hidden;
        },
        /**
         * This function returns whether this component's ancestor is hidden or not
         * @name isAncestorHidden
         * @function
         * @public
         * @memberof ax/af/Component#
         */
        isAncestorHidden: function () {
            return this._ancestorHidden;
        },
        /**
         * Updates the internal _ancestorHidden field for current and child components
         * @memberof ax/af/Component#
         * @method
         * @protected
         */
        _updateAncestorHidden: function () {
            var parent = this.getParent();
            if (!parent) {
                this._ancestorHidden = false;
                return;
            }
            this._ancestorHidden = parent.isAncestorHidden() || parent.isHidden();
        },
        /**
         * Updates the internal _inDOMTree field for current and child components
         * @memberof ax/af/Component#
         * @method
         * @protected
         */
        _updateInDOMTree: function (isAttached) {
            this._inDOMTree = isAttached;
        },
        /**
         * This function shows this component. Dispatches a "show" event.
         * It will set the visibility to be true and remove the model reference assigned via setVisibility.
         * As a result, only the latest visibilitity is set and control the show/hide.
         * @method
         * @public
         * @return {ax/af/Component} The component itself, for chaining
         * @memberof ax/af/Component#
         * @fires module:ax/af/evt/type.SHOWN
         */
        show: function () {
            return this.setVisibility(true);
        },
        /**
         * To set the visibility of the component to be false and hide the component. Dispatches a "hide" event.
         * It will set the visibility to be false and remove the model reference assigned via setVisibility.
         * @method
         * @return {ax/af/Component} The component itself, for chaining
         * @public
         * @memberof ax/af/Component#
         * @fires module:ax/af/evt/type.HIDDEN
         */
        hide: function () {
            return this.setVisibility(false);
        },
        /**
         * This function toggle this component's display state.
         * It will toggle the visibility and remove the model reference assigned via setVisibility.
         * @return {ax/af/Component} The component itself, for chaining
         * @public
         * @memberof ax/af/Component#
         */
        toggle: function () {
            if (this.isHidden()) {
                return this.setVisibility(true);
            }
            return this.setVisibility(false);
        },
        /**
         * Add a CSS class name to this component.
         * @method
         * @public
         * @param {String} className the name of the class
         * @return {ax/af/Component} The component itself, for chaining
         * @memberof ax/af/Component#
         */
        addClass: function (className) {
            if (!this._root) {
                return this;
            }
            this._root.addClass(className);
            return this;
        },
        /**
         * Remove a CSS class name frin this component.
         * @method
         * @param {String} className the name of the class
         * @return {ax/af/Component} The component itself, for chaining
         * @memberof ax/af/Component#
         */
        removeClass: function (className) {
            if (!this._root) {
                return this;
            }
            this._root.removeClass(className);
            return this;
        },
        /**
         * Set this component's id
         * @method
         * @public
         * @param {String} id component's id
         * @memberof ax/af/Component#
         */
        setId: function (id) {
            //When opts.id starts with "#", we also put the ID into DOM element
            if (util.startsWith(id, "#")) {
                if (id.length > 1) {
                    this._root.id = id.substr(1);
                    this._root.attr("id", this._root.id);
                }
            } else {
                this._root.id = id;
            }
        },
        /**
         * Returns this component's id
         * @method
         * @public
         * @returns {String|null} component's id
         * @memberof ax/af/Component#
         */
        getId: function () {
            return this._root.id || null;
        },
        /**
         * This function returns itself if the supplied id matches its own id,
         * otherwise it null is returned
         * @method
         * @public
         * @param {String} id the ID of desired component
         * @memberof ax/af/Component#
         * @returns {ax/af/Component | null}
         */
        find: function (id) {
            return (this.getId() === id) ? this : null;
        },
        /**
         * Check if the current component is the ancestor of specified component.
         * @method
         * @public
         * @param {ax/af/Component} comp The component to check.
         * @memberof ax/af/Component#
         * @returns {boolean}
         */
        isAncestorOf: function (comp) {
            return comp.isDescendantOf(this);
        },
        /**
         * Check if the current component is a descendatn of the specified container.
         * @method
         * @public
         * @param {ax/af/Component} container The component to check.
         * @memberof ax/af/Component#
         * @returns {boolean}
         */
        isDescendantOf: function (container) {
            if (!this._parent) {
                return false;
            }
            if (this._parent === container) {
                return true;
            }
            return this._parent.isDescendantOf(container);
        },
        /**
         * This function returns the specified option and falls back to the supplied
         * default value if this option has not been defined for this component.
         * @method
         * @public
         * @memberof ax/af/Component#
         * @param {String} name The option name
         * @param {String} [defaultValue] The fallback value
         */
        getOption: function (name, defaultValue) {
            var ret = this._opts[name];
            if (util.isUndefined(ret)) {
                ret = defaultValue;
            }
            return ret;
        },
        /**
         * Sets an option for this component.
         * @method
         * @public
         * @memberof ax/af/Component#
         * @param {String} name The option name
         * @param {String} value The value of the option
         * @return {ax/af/Component} The component itself, for chaining
         */
        setOption: function (name, value) {
            this._opts[name] = value;
            return this;
        },
        /**
         * This function returns the root element of this component.
         * @method
         * @return {ax/Element} The root element
         * @public
         * @memberof ax/af/Component#
         */
        getRoot: function () {
            return this._root;
        },
        /**
         * This function returns the parent componet of this component.
         * @method
         * @return {ax/af/Component} The parent componet
         * @public
         * @memberof ax/af/Component#
         */
        getParent: function () {
            return this._parent;
        },
        /**
         * This function attach a controller to this component.
         * @method
         * @param {ax/af/mvc/Controller} controller the controller to attach
         * @return {ax/Element} The parent element
         * @public
         * @memberof ax/af/Component#
         */
        attachController: function (controller) {
            this._controller = controller;
        },
        /**
         * This function detaches a controller from this component.
         * @method
         * @public
         * @memberof ax/af/Component#
         */
        detachController: function () {
            this._controller = null;
        },
        /**
         * This function returns the attached controller of this component.
         * @method
         * @return {ax/af/Controller} The attached controller
         * @public
         * @memberof ax/af/Component#
         */
        getAttachedController: function () {
            return this._controller;
        },
        /**
         * This function returns the immediate controller of this component.
         * @method
         * @return {ax/af/Controller} The immediate controller
         * @public
         * @memberof ax/af/Component#
         */
        getController: function () {
            if (!this._controller) {
                var parent = this.getParent();
                if (!parent) {
                    return null;
                }
                return parent.getController();
            }
            return this._controller;
        },
        /**
         * Get root controller
         * @method
         * @return {ax/af/ui/Controller} The root level controller
         * @public
         * @memberof ax/af/Component#
         **/
        getRootController: function () {
            var parents = [],
                parent = this.getParent();

            while (parent) {
                parents.push(parent);
                parent = parent.getParent();
            }

            while (parents.length) {
                parent = parents.shift();
                if (parent.getAttachedController()) {
                    return parent.getAttachedController();
                }
            }

            return null;
        },
        /**
         * This function detaches this component from its current parent.
         * Dispatches a "detach" event.
         * @name detach
         * @function
         * @return {ax/af/Component} The component itself, for chaining
         * @public
         * @memberof ax/af/Component#
         * @fires module:ax/af/evt/type.DETACH
         * @fires module:ax/af/evt/type.DETACHED_FROM_DOM
         * @fires module:ax/af/evt/type.DETACHED_FROM_CONTROLLER
         */
        detach: function () {
            if (!this._parent) {
                return this;
            }

            this._parent.detach(this);

            return this;
        },
        /**
         * Set as the active widget from the container
         * @name setActive
         * @param {Boolean | ax/af/Component } [noTrailActive] - Optional.If it is boolean, true will only apply to the current component while false will setActive till the root.If it is abstract component, it will set active trail until the input abstractComponent(including).If the abstractComponent is not found,it will set active until the top of the tree and act like the false case. Default is False
         * the entire trail. False to skip.
         * active.
         * @function
         * @memberof ax/af/Component#
         * @public
         */
        setActive: function (noTrailActive) {
            var parent = this.getParent();

            if (!parent) {
                return;
            }

            parent.setActiveChild(this);

            if (noTrailActive === true) {
                return;
            }

            if (parent === noTrailActive) {
                return;
            }

            parent.setActive(noTrailActive);
        },
        /**
         * Returns true if itself is active
         * @method
         * @param {Boolean} [noTrail] Default False to activate
         * the entire trail. True to skip.
         * @return {Boolean} True if current is active
         * @memberof ax/af/Component#
         * @function
         * @public
         */
        isActive: function (noTrail) {
            var parent = this.getParent();

            if (!parent || (!noTrail && parent && !parent.isActive())) {
                return false;
            }

            return parent.isChildActive(this);
        },
        /**
         * Returns true if itself is leaf component
         * @method
         * @return {Boolean} True if itself is leaf component
         * @memberof ax/af/Component#
         * @public
         */
        isLeaf: function () {
            return true;
        },
        /**
         * Get hee mapped DOM event from a XDK event
         * @method
         * @param {module:ax/af/evt/type} type The XDK event type
         * @return {String} the DOM event name
         * @memberof ax/af/Component#
         * @private
         */
        __getMappedDOMEvent: function (type) {
            switch (type) {
                /**
                 * @TODO add more events if necessary
                 */
            case evtType.CLICK:
                //block the mouse click on the tv display only since there are both mouse click and key press A together.
                if (device.platform === "wiiu" && nwf) {
                    var displayMgr = nwf.display.DisplayManager.getInstance();
                    if (window === displayMgr.getTVDisplay().window) {
                        return false;
                    }
                }
                return "click";
            case evtType.MOUSEOVER:
                return "mouseover";
            }
            return false;
        },
        /**
         * Overrides parent's method {@link ax/af/evt/DualEventDispatcher#addEventListener|addEventListener}.
         * Note this function is not full compatible with parent's version.
         * @param {ax/af/evt|String} type event type
         * @param {Function} handler event listener function, by default this function is bind to current component
         * @param {Boolean} [useCapture] whether listener is registered to capturing phase, false by default
         * @param {Boolean} [once] set to true if listener will be called only once
         * @returns {ax/af/evt/DualEventDispatcher} current event dispatcher, for easier chaining
         * @method
         * @public
         * @memberof ax/af/Component#
         */
        addEventListener: function (type, handler, useCapture, once) {
            var domEvtType = this.__getMappedDOMEvent(type),
                wrappedHandler, newEvt;

            if (!domEvtType) {
                return this._super(type, handler, useCapture, once);
            }

            useCapture = !!useCapture;

            // wrap hanlder, so that it can be put into element for direct invocation
            wrappedHandler = function (evt) {
                var listenedComp = this.getHTMLElement().__comp,
                    target = evt.target,
                    targetComp = target.__comp;

                while (!targetComp) {
                    target = target.parentNode;
                    targetComp = target.__comp;
                }

                if (type === evtType.CLICK) {
                    // find the clickable component, we should use that as event target
                    while (!targetComp.isClickable()) {
                        if (targetComp === listenedComp) {
                            // none of the children and component itself is clickable
                            // just do nothing
                            return;
                        }
                        targetComp = targetComp.getParent();
                    }
                }

                newEvt = new Event(type, targetComp);

                util.bind(handler, listenedComp)(newEvt);

                if (once) {
                    listenedComp.removeEventListener(type, handler, useCapture);
                }

                evt.stopPropagation();
                evt.preventDefault();
            };

            this._root.addEventListener(domEvtType, wrappedHandler, useCapture);

            // prepare for later removal from element
            if (!handler.$$guid) {
                handler.$$guid = core.getGuid();
            }
            if (!this.__wrappedListeners[domEvtType]) {
                this.__wrappedListeners[domEvtType] = {};
            }
            if (!this.__wrappedListeners[domEvtType][handler.$$guid]) {
                this.__wrappedListeners[domEvtType][handler.$$guid] = {};
            }
            if (!this.__wrappedListeners[domEvtType][handler.$$guid][useCapture]) {
                this.__wrappedListeners[domEvtType][handler.$$guid][useCapture] = [];
            }
            this.__wrappedListeners[domEvtType][handler.$$guid][useCapture].push(wrappedHandler);

            return this._super(type, handler, useCapture, once);
        },
        /**
         * Overrides parent's method {@link ax/af/evt/DualEventDispatcher#removeEventListener|removeEventListener}.
         * Note: second param must be provided.
         * @method removeEventListener
         * @param {module:ax/af/evt/type|String} type Event type
         * @param {Function} handler event listener function. This param is not optional.
         * @param {Boolean} [useCapture] whether listener is registered to capturing phase. If not provided, it **defaults to false.**
         * @public
         * @memberof ax/af/Component#
         */
        removeEventListener: function (type, handler, useCapture) {
            var domEvtType = this.__getMappedDOMEvent(type),
                wrappedHandler;

            if (!domEvtType) {
                return this._super(type, handler, useCapture);
            }

            useCapture = !!useCapture;

            if (!this.__wrappedListeners || !this.__wrappedListeners[domEvtType] || !this.__wrappedListeners[domEvtType][handler.$$guid] || !this.__wrappedListeners[domEvtType][handler.$$guid][useCapture] || !this.__wrappedListeners[domEvtType][handler.$$guid][useCapture].length) {
                // this event handler is not registered before
                return this._super(type, handler, useCapture);
            }
            wrappedHandler = this.__wrappedListeners[domEvtType][handler.$$guid][useCapture].pop();

            this._root.removeEventListener(domEvtType, wrappedHandler, useCapture);

            return this._super(type, handler, useCapture);
        },
        /**
         * Overrides parent's method {@link ax/af/evt/DualEventDispatcher#removeAllListeners|removeAllListeners}.
         * @method removeAllListeners
         * @public
         * @memberof ax/af/Component#
         */
        removeAllListeners: function () {
            this.__wrappedListeners = {};
            this._root.removeAllListeners();
            return this._super();
        },
        /**
         * Set the visibility of the component. The latest setting will override previous setting.
         * @method setVisibility
         * @param {Boolean|ax/af/mvc/ModelRef} visible state(True to show, false to hide) or ModelRef to modify its visibility with a model
         * @return {ax/af/Component} The component itself, for chaining
         * @fires module:ax/af/evt/type.HIDDEN
         * @fires module:ax/af/evt/type.SHOWN
         * @public
         * @memberof ax/af/Component#
         */
        setVisibility: function (visible) {

            //remove the previous model ref
            if (this.__visibilityModelRef && this.__visibilityListener) {
                this.__visibilityModelRef.getModel().off("change:" + this.__visibilityModelRef.getAttr(), this.__visibilityListener);
                this.__visibilityModelRef = null;
            }

            //add new model ref
            if (visible instanceof ModelRef) {

                if (!this.__visibilityListener) {
                    this.__visibilityListener = util.bind(function (evt) {
                        this.__setVisible(evt.newVal);
                    }, this);
                }

                visible.getModel().on("change:" + visible.getAttr(), this.__visibilityListener);
                this.__visibilityModelRef = visible;
                visible = this.__visibilityModelRef.getVal();
            }

            return this.__setVisible(visible);
        },
        /**
         * Perfom action to show or hide this component
         * @method __setVisible
         * @private
         * @param {Boolean} state True to show, false to hide
         * @return {ax/af/Component} The component itself, for chaining
         * @memberof ax/af/Component#
         * @fires module:ax/af/evt/type.SHOWN
         * @fires module:ax/af/evt/type.HIDDEN
         */
        __setVisible: function (visible) {
            var eventName;

            if (typeof visible !== "boolean") {
                var id = this.getOption("id", undefined);
                console.warn("[Component] Expecting boolean value for visibility" + (id ? " on component " + id : "") + "; actual: " + visible);
            }

            this._hidden = !visible;

            if (this._hidden) {
                this._root.hide();
                eventName = evtType.HIDDEN;
            } else {
                this._root.show();
                eventName = evtType.SHOWN;

            }

            this._updateAncestorHidden();

            //Dispatches an event
            evtMgr.trigger(eventName, this, {
                hidden: this._hidden
            });

            return this;
        }
    });
});
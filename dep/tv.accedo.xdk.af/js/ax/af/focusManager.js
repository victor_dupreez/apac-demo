/**
 *
 * Handling the focus automation here. Based on options defined in components:
 *
 * * nextRight      : Target component's id / abstract component / callback function
 * * nextLeft       : Target component's id / abstract component / callback function
 * * nextUp         : Target component's id / abstract component / callback function
 * * nextDown       : Target component's id / abstract component / callback function
 *
 * If the nextUp/nextDown/nextRight/nextLeft is a callback function, it will run the callback in focus direction change and check the return value.
 *
 * When return value is false/undefined, it will keep going to find its parent navigation option until the toppest (which has no navigation Handle) or
 * the parent navigation handling.
 *
 * When return value is true, it will stop checking the parent. So when writing callback of nextUp/nextDown/nextRight/nextTop,
 * please make sure if it is necessary to continue the navigation handling after the callback.
 *
 *  For example, the nextUp/nextDown option in list/grid are callback functions. If you want to just do the internal nextUp/nextDown without
 *  find the parent one,you need to return true
 *
 *  If you reach the border,it will return the original set value when initial.It may not defined and need to look up for the parent handling,so you may need to return false to keep looping the parent.
 *
 * __forwardFocus__  : Taget descendant component's id / Boolean(true) / Boolean(false)
 * When forwardFocus is set to true, automated forward focus mechanism is enabled.
 * This mechanism forwards the focus to the last focused descendant component.
 * If there's no last focus, it will try to breath first search for the first 'focusable' child.
 * However, whenever the search reachs a Container with 'forwardFocus:false', the search of it's descendents is be terminated.
 *
 * __focus root__ : A container, which is the root of the focusable component sub tree.
 * When focus root is not null, any focus request beyond this container will be rejected.
 * This setting will eventually set up a boundary on where the focus can go.
 * Please note that the direction check for focus automation will not exceed this boundary as well.
 * (ie. if the __next*__ of a component is not set, the recusive search in the parent will only be effective up to the focus root)
 * To clear the boundary, set it to null.
 *
 * @module ax/af/focusManager
 * @author Andy Hui <andy.hui@accedo.tv>
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/focusManager", [
    "ax/af/Component",
    "ax/util",
    "ax/af/evt/eventManager",
    "ax/af/evt/type",
    "ax/device/vKey",
    "ax/console",
    "ax/config",
    "ax/af/it/BFSCompIt",
    "ax/af/it/AFSCompIt",
    "ax/af/Container",
    "ax/core",
    "ax/exception"
], function (
    Component,
    util,
    evtMgr,
    evtType,
    vKey,
    console,
    config,
    BFSCompIt,
    AFSCompIt,
    Container,
    core,
    exception
) {

    "use strict";

    /**
     * Get the inherited forwardFocus option. It will loop to find the available value until the root or available option
     * @param {ax/af/Component} component Component to start with and look for its inherited forwardfocus option
     * @return {Boolean | ax/af/Component | String} The option of the inherited forwardFocus
     * @memberof module:ax/af/focusManager
     * @method inheritForwardFocusOption
     * @private
     */
    function inheritForwardFocusOption(comp) {
        var option;

        while (!option) {
            comp = comp.getParent();
            if (!comp) {
                break;
            }
            option = comp.getOption("forwardFocus");
        }

        return option;
    }

    /**
     * To obtain the next Level first focusable component
     * @param {ax/af/Component} component Component to start with and look for its parent forwardfocus option
     * @return {ax/af/Component} The focusbale components. If it is unable to find, it will return null
     * @memberof module:ax/af/focusManager
     * @method getNextFocusableChild
     * @private
     */
    function getNextFocusableChild(comp) {
        var child, i,
            target = null,
            children;

        if (comp.isLeaf()) {
            return null;
        }

        children = comp.getChildren();

        for (i = 0; i < children.length; i++) {

            child = children[i];

            if (isCompFocusable(child)) {
                return child;
            }

        }
        return target;
    }

    /**
     * To get the next forward focus.
     * @param {ax/af/Component} component Component to start with and look for its parent forwardfocus option
     * @param {Boolean} [parentForwardFocusOption] true if there is parentForwardFocusOption and then it will find the next level forward focus. if it is undefined, it won't loop to the leaf and find the next forwardFocus
     * @return {ax/af/Component} The focusbale components. If it is unable to find, it will return null
     * @memberof module:ax/af/focusManager
     * @method findNextLevelFirstFocusableChild
     * @private
     */
    function getNextForwardFocus(comp, parentForwardFocusOption) {
        var child, i,
            target = null,
            children;

        if (comp.isLeaf()) {
            return null;
        }

        children = comp.getChildren();
        for (i = 0; i < children.length; i++) {

            child = children[i];
            target = getForwardFocus(child, parentForwardFocusOption);

            if (target) {
                return target;
            }

        }

        return target;

    }

    /**
     * To find and handle the forwardFocus of the component. It will find if there is lastFocusChild, if not, it will find the next Level Focusbale children
     * @param {ax/af/Component} component the target component
     * @return {ax/af/Component} the target focusable component
     * @memberof module:ax/af/focusManager
     * @method forwardFocusHandling
     * @private
     */
    function forwardFocusHandling(comp) {
        var lastFocus = comp._lastFocusChild;

        if ((lastFocus instanceof Component) && comp.isAncestorOf(lastFocus) && isCompFocusable(lastFocus)) {

            console.info("Using last focus for forward focus");
            return lastFocus;
        } else {
            console.info("Find the next level focusable item");
            return getNextFocusableChild(comp);
        }
    }

    var focusMgrModule = null,
        rootController = null,
        lastFocus = null,
        // last focused component
        curFocus = null,
        // current focuesed component
        getForwardFocus, getLastActiveFocus, handleFocusDetached, onBlurAction, onFocusAction, isCompFocusable, isCompChildFocused, isCompFocused, getCompLastFocusedTime;


    /**
     * Get the forwarded focus of a specified component.
     * @param {ax/af/Component} component Component to be focused
     * @param {Boolean} parentForwardFocusOption used to determine if need to search for focusable children. Default is false. If it is not set, it will get the inherit forward focus option from the parent.
     * @memberof module:ax/af/focusManager
     * @method
     * @private
     */
    getForwardFocus = function (component, parentForwardFocusOption) {
        if (!component) {
            return null;
        }

        if (isCompFocusable(component)) {
            return component;
        }

        var forwardFocusOption, forwardFocusComp;

        //Step 1. Get current comp forwardFocus
        forwardFocusOption = component.getOption("forwardFocus");

        //Step 2. Handle the forward Focus

        if (forwardFocusOption === true) {
            //Step 2a. Handle current comp forwardFocus true based on focusable
            forwardFocusComp = forwardFocusHandling(component);

            //Step 2ai go deeper to the children and find the forwardFocus when it is unable to find the forward focus
            if (!forwardFocusComp) {
                forwardFocusComp = getNextForwardFocus(component, true);
            }

        } else if (forwardFocusOption === false) {
            //Step 2b. do nothing
            return null;
        } else if (util.isUndefined(forwardFocusOption)) {
            //Step 2c. Handle when no setting on forward Focus 

            if (parentForwardFocusOption) {
                //Step 2ci go deeper to the children and find the forwardFocus if it is capture
                forwardFocusComp = getNextForwardFocus(component, parentForwardFocusOption);
            } else {
                //Step 2cii Get Parent forward Focus Option (inherit from the parent)
                forwardFocusOption = inheritForwardFocusOption(component);

                //Step 2ciii if parent forwardFocus is true, then need to find the forwardFocus start from the current level
                if (forwardFocusOption === true) {

                    forwardFocusComp = forwardFocusHandling(component);

                    if (!forwardFocusComp) {
                        forwardFocusComp = getNextForwardFocus(component, true);
                    }

                }
            }
        } else {
            //to convert the option into comp
            forwardFocusComp = forwardFocusOption;
        }


        //Step 3. Get the focus if id/String
        // fallback case: a valid "forwardFocus" option is specified (in string)
        if (forwardFocusComp && util.isString(forwardFocusComp)) {
            //only have default focus when it is mouseoff
            forwardFocusComp = component.find(forwardFocusComp);
        }

        // fallback case: a valid "forwardFocus" option is specified (as a component)
        if (forwardFocusComp && (forwardFocusComp instanceof Component)) {
            if (isCompFocusable(forwardFocusComp)) {
                return forwardFocusComp;
            } else {
                return getForwardFocus(forwardFocusComp);
            }
        }

        return null;
    };

    /**
     * Checks if component is focused.
     * @method isCompFocused
     * @public
     * @param {ax/af/Component} comp component
     * @return {Boolean} true if focused, false otherwise.
     * @memberof module:ax/af/focusManager
     */
    isCompFocused = function (comp) {
        return comp._focused || false;
    };
    /**
     * Returns whether any of it's child is focused
     * @name isCompChildFocused
     * @function
     * @param {ax/af/Component} comp component
     * @memberof module:ax/af/focusManager
     * @public
     */
    isCompChildFocused = function (comp) {
        return comp._root.hasClass("focus-trail") || (util.isUndefined(comp._focusTrail) ? false : comp._focusTrail);
    };
    /**
     * Returns true if component is focusable, false otherwise.
     * @method
     * @public
     * @param {ax/af/Component} comp component
     * @return {Boolean}
     * @memberof module:ax/af/focusManager
     */
    isCompFocusable = function (comp) {
        //not focusable if it's disabled
        if (comp._disabled || comp.isHidden() || comp.isAncestorHidden()) {
            return false;
        }

        //return default focusability
        return comp.getOption("focusable", false);
    };
    /**
     * Get the timesptamp when component last focused
     * @method
     * @param {ax/af/Component} comp component
     * @return {Integer|null} The timestamp when this component is focused
     * @memberof module:ax/af/focusManager
     * @public
     */
    getCompLastFocusedTime = function (comp) {
        return comp._lastFocused;
    };

    /**
     * Find the latest focused component from most active trail.
     * @method
     * @param {ax/af/Component} [from] - The component node to start
     * at.
     * @return {ax/af/Component|null} the found component, null if entire trail has not
     * been focused before.
     * @memberof module:ax/af/focusManager
     * @public
     */
    getLastActiveFocus = function (from) {

        from = from || rootController.getView();
        var component,
            focusedTime = 0,
            lastActiveFocus = null,
            afsCompIt = new AFSCompIt(from);

        while (afsCompIt.hasNext()) {

            component = afsCompIt.next();

            if (!isCompFocusable(component)) {
                component = component.getParent();
                continue;
            }

            if (focusedTime < getCompLastFocusedTime(component)) {
                lastActiveFocus = component;
                focusedTime = getCompLastFocusedTime(component);
            }
        }

        if (!focusedTime) {
            return null;
        }

        return lastActiveFocus;
    };
    /**
     * handler for focused component detached from DOM
     * @method
     * @private
     */
    handleFocusDetached = function () {
        focusMgrModule.focus(getLastActiveFocus(), {
            noActive: true
        });
    };
    /**
     * Remove focus from a component.
     * @method
     * @param {ax/af/Component} toBlur - The component to blur
     * @memberof module:ax/af/focusManager
     * @private
     */
    onBlurAction = function (toBlur, related) {
        var tempComp = curFocus.getParent(),
            evtOpts;

        // udpate component to be blured
        toBlur.removeClass("focused");
        toBlur._focused = false;
        // update focus trail
        while (tempComp) {
            if (focusMgrModule.isCompFocusTrailOn(tempComp)) {
                tempComp.removeClass("focus-trail");
            } else {
                tempComp._focusTrail = false;
            }

            tempComp = tempComp.getParent();
        }

        // fire events
        if (related) {
            evtOpts = {
                relatedTarget: related
            };
        }
        evtMgr.trigger(evtType.BLUR, curFocus, evtOpts);

        lastFocus = curFocus;
        curFocus = null;
        // commented out in case we need this again
        //lastFocus.removeEventListener(evtType.DETACHED_FROM_DOM, handleFocusDetached);
    };
    /**
     * Focus on a component.
     * @method
     * @param {ax/af/Component} toFocus - The component to focus.
     * @param {Object} [options] focus options
     * @param {Object} [options.noActive] do not set active for the focused component
     * @memberof module:ax/af/focusManager
     * @private
     */
    onFocusAction = function (toFocus, related, options) {
        var tempComp = toFocus.getParent(),
            evtOpts;

        curFocus = toFocus;
        // update component to be focused
        toFocus.getRoot().addClass("focused");
        toFocus._focused = true;
        toFocus._lastFocused = (new Date()).getTime();
        // update focus trail
        while (tempComp) {
            if (focusMgrModule.isCompFocusTrailOn(tempComp)) {
                tempComp.getRoot().addClass("focus-trail");
            } else {
                tempComp._focusTrail = true;
            }

            if (tempComp.getOption("forwardFocus") === true) {
                tempComp._lastFocusChild = toFocus;
            }
            tempComp = tempComp.getParent();
        }

        //Set focus to be active
        if (!options || !options.noActive) {
            curFocus.setActive();
        }

        console.info("[FocusMgr] focused on component " + curFocus + " with id'" + curFocus.getId() + "'");

        // fire events
        if (related) {
            evtOpts = {
                relatedTarget: related
            };
        }
        evtMgr.trigger(evtType.FOCUS, curFocus, evtOpts);


        // commented out in case we need this again
        //curFocus.addEventListener(evtType.DETACHED_FROM_DOM, handleFocusDetached);
    };
    // the actual module
    focusMgrModule = {
        /**
         * Focus direction: UP.
         * @memberof module:ax/af/focusManager
         */
        FOCUS_UP: 0x01,
        /**
         * Focus direction: DOWN.
         * @memberof module:ax/af/focusManager
         */
        FOCUS_DOWN: 0x02,
        /**
         * Focus direction: LEFT.
         * @memberof module:ax/af/focusManager
         */
        FOCUS_LEFT: 0x04,
        /**
         * Focus direction: RIGHT.
         * @memberof module:ax/af/focusManager
         */
        FOCUS_RIGHT: 0x08,
        /* Sets the root controller for this focus manager,
         * so it knows where to start when searching for a component
         * @method
         * @param {ax/af/mvc/Controller} controller
         *     the root controller where component searching starts from
         * @memberof module:ax/af/focusManager
         */
        _setRootController: function (controller) {
            rootController = controller;
        },

        /**
         * Set the root of the focusable tree that bound the focus.
         * @method
         * @param {ax/af/Container} container The root container of the focusable tree. Set to null to clear the boundary.
         * @throws {Exception.<exception.ILLEGAL_ARGUMENT>} If the root is not valid
         * @memberof module:ax/af/focusManager
         */
        setFocusRoot: function (container) {
            if (container && !(container instanceof Container)) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "Focus root should be an instance of ax/af/Container");
            }

            this._focusRoot = container;
            console.info("[focusManager] focus root is set to [" + (container ? container.getId() : container) + "]");
        },

        /**
         * Get the current focus root.
         * @method
         * @returns {ax/af/Container} The current focus root
         * @memberof module:ax/af/focusManager
         */
        getFocusRoot: function () {
            return this._focusRoot;
        },

        /**
         * Check if the component is outside of the current focus root.
         * @method
         * @protected
         * @param {ax/af/Component} component A component
         * @returns {Boolean} True if the component is outside the focus root, false otherwise
         * @memberof module:ax/af/focusManager
         */
        _isComponentOutsideFocusRoot: function (component) {
            return this._focusRoot && !(component.isDescendantOf(this._focusRoot) || component === this._focusRoot);
        },

        /**
         * Explicitly focus a given component, if possible.
         * @param {ax/af/Component} component Component to focus
         * @param {Object} [options] focus options
         * @param {Boolean} [options.noActive] whether to update the focused component's activeness
         * @param {Boolean} [options.skipForwardFocus] whether to skip focus forwarding
         * @method
         * @memberof module:ax/af/focusManager
         * @return {Boolean} true if successfully focused a component or removed focus (if so is requested), false if otherwise
         */
        focus: function (component, options) {
            options = options || {};

            if (!component) {
                if (curFocus) {
                    onBlurAction(curFocus);
                }

                return true;
            }

            var skipForwardFocus = !!options.skipForwardFocus,
                forward = null,
                catchedFocus = curFocus,
                ret;

            // recursive logics to sort out the target component

            if (component.getOption("focusable")) {
                //checks whether a component wanna handle the focus itself
                if (util.isFunction(component.onRequestFocus)) {
                    //focus handler can return a component to forward the focus to
                    ret = component.onRequestFocus();
                }

                //If onRequestFocus return a string / abstract component, it will return the focus item
                if (util.isString(ret)) {
                    ret = component.find(ret);
                }

                if (ret instanceof Component) {
                    // found a component to restart the focus
                    return focusMgrModule.focus(ret);
                }

                // return true by onRequestFocus prevent default focus handling
                if (ret) {
                    return true;
                }
            }
            // find forward focus if it's not focusable
            else if (!skipForwardFocus) {
                forward = getForwardFocus(component);

                if (!(forward instanceof Component)) {
                    console.warn("[focusManger] no valid forward focus, abort focus process.");
                    return false;
                }

                if (forward === component) {
                    console.warn("[focusManager] forward focus is the same as the original component, abort focus process.");
                    return false;
                }

                // call focusManager.focus(forward) so that the forwarded component get chance to handle onRequestFocus()
                return focusMgrModule.focus(forward);
            }

            // at this point, target component is located

            if (this._isComponentOutsideFocusRoot(component)) {
                console.warn("[focusManager] the requested focus target [" + component.getId() + "] is out of the focus root [" + this._focusRoot.getId() + "]");
                return false;
            }

            if (!isCompFocusable(component)) {
                console.warn("Delegated component is not Focusable! Component ID: " + component.getId());
                return false;
            }

            // do actual switch focus handling, blur before focus

            // only blur if there is a current focus, and it is not the target component
            if (curFocus && !isCompFocused(component)) {
                onBlurAction(curFocus, component);
            }

            onFocusAction(component, catchedFocus, options);

            return true;
        },
        /**
         * return the current focus
         * @return {ax/af/Component} current focused component
         * @method
         * @memberof module:ax/af/focusManager
         */
        getCurFocus: function () {
            return curFocus || null;
        },
        /**
         * ** Deprecated: see directionChangeByKey instead **
         * Directionally change focus using an arrow key
         * @param {ax/device/vKey} key on the direction
         * @param  {ax/af/Component} [container] to check the directional focus forward upto this container.(excluding the checking on this container)
         * @return {Boolean} true if directional navigation is successfully made
         * @method
         * @deprecated
         * @memberof module:ax/af/focusManager
         */
        directionalFocusChangeByKey: function (key, container) {
            var ret = false;
            switch (key.id) {
            case vKey.UP.id:
                ret = this.directionalFocusChange(focusMgrModule.FOCUS_UP, container);
                break;
            case vKey.DOWN.id:
                ret = this.directionalFocusChange(focusMgrModule.FOCUS_DOWN, container);
                break;
            case vKey.LEFT.id:
                ret = this.directionalFocusChange(focusMgrModule.FOCUS_LEFT, container);
                break;
            case vKey.RIGHT.id:
                ret = this.directionalFocusChange(focusMgrModule.FOCUS_RIGHT, container);
                break;
            }
            return ret;
        },

        /**
         * Directionally change focus using an arrow key
         * @param {ax/device/vKey} key on the direction
         * @param  {ax/af/Component} [container] to check the directional focus forward upto this container.(excluding the checking on this container)
         * @return {ax/af/Component|Boolean} component if directional navigation is successfully made , direction key consumed.
         * true if direction key consumed but navigation not successfully made.
         * @method
         * @memberof module:ax/af/focusManager
         */
        directionChangeByKey: function (key, container) {
            var ret = false;
            switch (key.id) {
            case vKey.UP.id:
                ret = this.directionChange(focusMgrModule.FOCUS_UP, container);
                break;
            case vKey.DOWN.id:
                ret = this.directionChange(focusMgrModule.FOCUS_DOWN, container);
                break;
            case vKey.LEFT.id:
                ret = this.directionChange(focusMgrModule.FOCUS_LEFT, container);
                break;
            case vKey.RIGHT.id:
                ret = this.directionChange(focusMgrModule.FOCUS_RIGHT, container);
                break;
            }
            return ret;
        },
        /**
         *
         * **This function is deprecated, see directionChange ***
         * Change focus in given direction. It will check the option of that direction.
         * Firstly, it will check if the direction handling of current Element exist.
         * If it is undefined, it will check upwards along the component tree.
         *
         * When a directional focus handling option is found, it will go through the following sequence:
         *
         * *  'null' value will stop the focus direction change.
         * *  callback function will be run first and get back the return value. Boolean false return value will stop the focus direction change, other value will continued to be used for the below checking
         * *  String, the Component with this String as ID will be focused
         * *  Component, it will be focused.
         * *  Otherwise, check parent's directional focus handling option, and start over
         * @param {module:ax/af/focusManager.FOCUS_UP|module:ax/af/focusManager.FOCUS_DOWN
         * |module:ax/af/focusManager.FOCUS_LEFT|module:ax/af/focusManager.FOCUS_RIGHT}
         * direction focus direction
         * @param  {ax/af/Component} [container] to check the directional focus forward upto this container.
         * @return {Boolean} true if directional navigation is successfully made
         * @memberof module:ax/af/focusManager
         * @deprecated
         * @method
         */
        directionalFocusChange: function (direction, container) {

            var ret = this.directionChange(direction, container);

            if (ret) {
                return true;
            }

            return false;
        },

        /**
         *
         * Change focus in given direction. It will check the option of that direction.
         * Firstly, it will check if the direction handling of current Element exist.
         * If it is undefined, it will check upwards along the component tree.
         *
         * When a directional focus handling option is found, it will go through the following sequence:
         *
         * *  'null' value will stop the focus direction change.
         * *  callback function will be run first and get back the return value. Boolean false return value will stop the focus direction change, other value will continued to be used for the below checking
         * *  String, the Component with this String as ID will be focused
         * *  Component, it will be focused.
         * *  Otherwise, check parent's directional focus handling option, and start over
         * @param {module:ax/af/focusManager.FOCUS_UP|module:ax/af/focusManager.FOCUS_DOWN
         * |module:ax/af/focusManager.FOCUS_LEFT|module:ax/af/focusManager.FOCUS_RIGHT}
         * direction focus direction
         * @param  {ax/af/Component} [container] to check the directional focus forward upto this container.
         * @return {Component|Boolean} componenet if directional navigation is successfully made , direction key consumed.
         * true if direction key consumed but navigation not successfully made.
         * @memberof module:ax/af/focusManager
         * @method
         */

        directionChange: function (direction, container) {
            if (container && !this.isCompChildFocused(container)) {
                // container does not have child focused, so nothing could be done
                console.warn("Directional focus change is not possible!");
                return false;
            }

            container = container || rootController.getView();
            var toFocus, opt, component;
            if (!curFocus) {
                toFocus = getLastActiveFocus();

                if (!toFocus) {
                    return false;
                }

                if (this.focus(toFocus)) {
                    return toFocus;
                }

            }

            //Block checking predefines
            component = curFocus;
            if (direction === focusMgrModule.FOCUS_UP) {
                direction = "nextUp";
            } else if (direction === focusMgrModule.FOCUS_DOWN) {
                direction = "nextDown";
            } else if (direction === focusMgrModule.FOCUS_LEFT) {
                direction = "nextLeft";
            } else if (direction === focusMgrModule.FOCUS_RIGHT) {
                direction = "nextRight";
            }


            //check for parent's direction recursively until it's defined or
            //up to root
            while (component && component !== container) {

                if (this._isComponentOutsideFocusRoot(component)) {
                    console.warn("[focusManager] abort direction check: the parent component [" + component.getId() + "] is out of the focus root [" + this._focusRoot.getId() + "]");
                    return false;
                }

                opt = component.getOption(direction);

                /*additional checking to ensure the return value of the callback function
                 if return value is true, it will stop the recursion and won't do anything.
                 If it is false/ undefined, it will keep running the recursion to check the parent*/

                if (util.isFunction(opt)) {
                    opt = opt.call(curFocus);
                    console.debug("directional focus callback return value: " + opt);
                }

                if (opt === true) {
                    return opt;
                }

                if (opt === false || util.isUndefined(opt)) {
                    // no directional option is defined
                    component = component.getParent();
                    continue;
                }

                if (opt === null) {
                    //Explicit empty difinition found - block navigation
                    return true;
                }

                //If opt is a component ID / a abstract component, it will stop recursion
                if (util.isString(opt)) {
                    toFocus = container.find(opt);
                    if (!toFocus && container !== rootController.getView()) {
                        toFocus = rootController.getView().find(opt);
                    }
                    break;
                }
                if (opt instanceof Component) {
                    toFocus = opt;
                    break;
                }

                component = component.getParent();
            }

            if (!toFocus) {
                return false;
            }

            if (this.focus(toFocus)) {
                return toFocus;
            }

        },

        /**
         * Ensure the focus is not null
         * @memberof module:ax/af/focusManager
         * @method
         */
        ensureFocus: function () {
            if (curFocus) {
                return true;
            }
            if (lastFocus) {
                return this.focus(lastFocus);
            }
            return false;
        },
        /**
         * Set the flag for the use of css focus-trail
         * @method
         * @name setCompFocusTrail
         * @param {ax/af/Component} component to set
         * @param {Boolean} flag yes or no
         * @return {Boolean | undefined} current css focus-trail flag
         * @memberof module:ax/af/focusManager
         * @public
         */
        setCompFocusTrail: function (comp, flag) {
            if (util.isBoolean(flag)) {
                comp._enableTrail = flag;
            }
            return comp._enableTrail;
        },
        /**
         * Reset the flag for the use of css focus-trail
         * @method
         * @name resetCompFocusTrail
         * @param {ax/af/Component} component to reset
         * @return {Boolean | undefined} current css focus-trail flag
         * @memberof module:ax/af/focusManager
         * @public
         */
        resetCompFocusTrail: function (comp) {
            comp._enableTrail = undefined;
            return comp._enableTrail;
        },
        /**
         * Get the flag for the use of css focus-trail
         * @method
         * @name isCompFocusTrailOn
         * @param {ax/af/Component} component to reset
         * @return {Boolean} current css focus-trail flag
         * @memberof module:ax/af/focusManager
         * @protected
         */
        isCompFocusTrailOn: function (comp) {

            if (comp && util.isUndefined(comp._enableTrail)) {
                return config.get("ui.focus.enableTrail", true);
            }

            if (!comp._enableTrail) {
                return false;
            }

            return true;
        },
        getLastActiveFocus: getLastActiveFocus,
        isCompFocused: isCompFocused,
        isCompChildFocused: isCompChildFocused,
        isCompFocusable: isCompFocusable,
        getCompLastFocusedTime: getCompLastFocusedTime
    };

    return focusMgrModule;
});
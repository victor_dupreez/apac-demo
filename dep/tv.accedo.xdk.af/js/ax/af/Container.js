/**
 * Basic container component to accomplish parent-child relationship for the view's tree structure.
 * @class ax/af/Container
 * @extends ax/af/Component
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/Container", ["ax/class", "ax/af/Component", "ax/core", "ax/af/evt/eventManager", "ax/util", "ax/af/evt/type", "ax/console"], function (klass, Component, core, evtMgr, util, evtType, console) {
    "use strict";
    var Container = klass.create(Component, {}, {
        /**
         * This array holds pointers to all child components in DOM order.
         * @protected
         * @memberof ax/af/Container#
         */
        _children: [],
        /**
         * This array holds pointers to all child components in activeness order.
         * @private
         * @memberof ax/af/Container#
         */
        __activeness: [],
        /**
         * Overrides the init() method inherits from Component
         * @method
         * @memberof ax/af/Container#
         * @public
         * @protected
         */
        init: function (opts) {
            this._super(opts);
        },
        /**
         * Deinit remove event listeners for itself and also ask its children to remove the event listers
         * @method
         * @memberof ax/af/Container#
         * @protected
         */
        deinit: function () {
            var i, children = this._children.slice(0),
                len = children.length;

            for (i = 0; i < len; i++) {
                children[i].deinit();
            }

            this._super();
        },
        /**
         * To do after the initialization, when all children are readily included
         * @method
         * @memberof ax/af/Container#
         * @protected
         */
        postInit: function () {
            this._super();

            if (!this._opts.children || !util.isArray(this._opts.children)) {
                return;
            }
            //do when there are children
            var i = 0,
                len = this._opts.children.length,
                child;
            if (len <= 0) {
                return;
            }
            for (; i < len; i++) {
                child = this._opts.children[i];
                this.attach(child);
            }
        },
        /**
         * This function retrieves a child object by its ID. It is worth
         * noting that this is a recursive function, so it will search
         * child containers as well for the give ID.
         * @method
         * @param {String} id The component id
         * @returns {ax/af/Component|null} The component with the given id, null if not found
         * @memberof ax/af/Container#
         * @public
         */
        find: function (id) {
            if (this.getId() === id) {
                return this;
            }

            var i, len = this._children.length,
                obj;
            for (i = 0; i < len; i++) {
                obj = this._children[i].find(id);
                if (obj) {
                    return obj;
                }
            }
            return null;
        },
        /**
         * This function retrives the current set of children of this container.
         * @method
         * @return {Array} The children array
         * @memberof ax/af/Container#
         * @public
         */
        getChildren: function () {
            return this._children.slice(0);
        },
        /**
         * Attach a child component.
         * @method
         * @param {ax/af/Component} child - The child component to be attached
         * @param {ax/af/Component.PLACE_BEFORE | ax/af/Component.PLACE_AFTER | ax/af/Component.PLACE_APPEND | ax/af/Component.PLACE_PREPEND}
         * [placement] - position the child should be placed. By default {@link ax/af/Component.PLACE_APPEND}.
         * @param {ax/af/Component} [marker] - must be a child of current container,
         * the placement of child will be in relative to this marker.
         * @memberof ax/af/Container#
         * @fires module:ax/af/evt/type.ATTACH
         * @fires module:ax/af/evt/type.ATTACHED_TO_DOM
         * @fires module:ax/af/evt/type.ATTACHED_TO_CONTROLLER
         * @public
         */
        attach: function (child, placement, marker) {
            if (util.indexOf(this._children, child) >= 0) {
                console.warn("Child is already attached to container");
                return this;
            }

            if (!placement) {
                placement = Container.PLACE_APPEND;
            }


            var idx = marker ? util.indexOf(this._children, marker) : this._children.length - 1;

            switch (placement) {
            case Container.PLACE_APPEND:
                this._children.push(child);
                break;
            case Container.PLACE_PREPEND:
                this._children.unshift(child);
                break;
            case Container.PLACE_BEFORE:
                if (idx < 0) {
                    throw core.createException("IncorrectParam", "The insert place marker component must be a child of this container!");
                }
                this._children.splice(idx, 0, child);
                break;
            case Container.PLACE_AFTER:
                if (idx < 0) {
                    throw core.createException("IncorrectParam", "The insert place marker component must be a child of this container!");
                }
                this._children.splice(idx + 1, 0, child);
                break;
            }

            // do the real work
            this._doAttach(child, placement, marker);

            this.__activeness.push(child);

            child._parent = this;

            // update members
            child._updateAncestorHidden();
            if (this._root.isInDOMTree()) {
                child._updateInDOMTree(true);
            }

            // fire events
            evtMgr.trigger(evtType.ATTACH, child);
            if (this._root.isInDOMTree()) {
                evtMgr.trigger(evtType.ATTACHED_TO_DOM, child);
            }

            return this;
        },
        /**
         * Internal function to really attach a child component.
         * @method
         * @param {ax/af/Component} child - The child component to be attached
         * @param {ax/af/Component.PLACE_BEFORE | ax/af/Component.PLACE_AFTER | ax/af/Component.PLACE_APPEND | ax/af/Component.PLACE_PREPEND}
         * [placement] - position the child should be placed. By default {@link ax/af/Component.PLACE_APPEND}.
         * @param {ax/af/Component} [marker] - must be a child of current container,
         * the placement of child will be in relative to this marker.
         * @memberof ax/af/Container#
         * @protected
         */
        _doAttach: function (child, placement, marker) {
            switch (placement) {
            case Container.PLACE_APPEND:
                this._root.append(child.getRoot());
                break;
            case Container.PLACE_PREPEND:
                this._root.prepend(child.getRoot());
                break;
            case Container.PLACE_BEFORE:
                this._root.insertBefore(marker.getRoot(), child.getRoot());
                break;
            case Container.PLACE_AFTER:
                this._root.insertAfter(marker.getRoot(), child.getRoot());
                break;
            }
        },
        /**
         * This function replace an existing child component with a new child component.
         * @method
         * @param {ax/af/Component} targetChild - Target child to replace.
         * @param {ax/af/Component} replacement - The replacement child component
         * @memberof ax/af/Container#
         * @public
         */
        replace: function (targetChild, replacement) {
            if (replacement === targetChild) {
                return;
            }

            var targetIdx = util.indexOf(this._children, targetChild),
                activeIdx = util.indexOf(this.__activeness, targetChild),
                inDOM = this._root.isInDOMTree();

            if (targetIdx < 0) {
                throw core.createException("IncorrectParam", "The replace target component must be a child of this container!");
            }

            this._children.splice(targetIdx, 1, replacement);
            this.__activeness.splice(activeIdx, 1, replacement);

            // do the real work
            this._doReplace(targetChild, replacement);

            replacement._parent = this;
            targetChild._parent = null;

            // update members
            replacement._updateAncestorHidden();
            if (inDOM) {
                replacement._updateInDOMTree(true);
            }
            // fire events
            evtMgr.trigger(evtType.ATTACH, replacement);
            if (inDOM) {
                evtMgr.trigger(evtType.ATTACHED_TO_DOM, replacement);
            }

            // update members
            targetChild._updateAncestorHidden();
            if (inDOM) {
                targetChild._updateInDOMTree(false);
            }
            // fire events
            evtMgr.trigger(evtType.DETACH, targetChild);
            if (inDOM) {
                evtMgr.trigger(evtType.DETACHED_FROM_DOM, targetChild);
            }
        },
        /**
         * The internal function that really replace an existing child component with a new child component.
         * @method
         * @param {ax/af/Component} targetChild - Target child to replace.
         * @param {ax/af/Component} replacement - The replacement child component
         * @memberof ax/af/Container#
         * @protected
         */
        _doReplace: function (targetChild, replacement) {
            this._root.replaceChild(targetChild.getRoot(), replacement.getRoot());
        },
        /**
         * This function detaches a child object. If no child provided, it will remove container itself.
         * @method
         * @param {ax/af/Component} child The child object to detach
         * @return {ax/af/Component} The component itself, for chaining
         * @memberof ax/af/Container#
         * @public
         */
        detach: function (child) {
            if (!child) {
                //if no child provided, it will remove itself via component detach function
                return this._super();
            }

            var idx = util.indexOf(this._children, child),
                activeIdx;

            if (idx < 0) {
                throw core.createException("IncorrectParam", "The detach target component must be a child of this container!");
            }


            this._children.splice(idx, 1);
            activeIdx = util.indexOf(this.__activeness, child);
            this.__activeness.splice(activeIdx, 1);

            child.getRoot().detach();

            child._parent = null;

            // update members
            child._updateAncestorHidden();
            if (this._root.isInDOMTree()) {
                child._updateInDOMTree(false);
            }

            // fire events
            evtMgr.trigger(evtType.DETACH, child);
            if (this._root.isInDOMTree()) {
                evtMgr.trigger(evtType.DETACHED_FROM_DOM, child);
            }

            return this;
        },
        /**
         * This function detaches all child objects.
         * @method
         * @memberof ax/af/Container#
         * @public
         */
        detachAll: function () {
            var i = this._children.length;
            while (i--) {
                this._children[i].detach();
            }
            this._children = [];
        },
        /**
         * Set the child as most active
         * @method
         * @param {String | ax/af/Component} child - id or child instance
         * @memberof ax/af/Container#
         * container
         */
        setActiveChild: function (child) {
            if (!(child instanceof Component)) {
                child = this.find(child);
            }

            var idx = util.indexOf(this.__activeness, child);

            if (idx < 0) {
                throw core.createException("IncorrectParam", "The target component must be a child of this container!");
            }

            if (idx === 0) {
                return;
            }

            this.__activeness.splice(idx, 1);

            //add to the front
            this.__activeness.unshift(child);

            //dispatch inactive event
            evtMgr.trigger(evtType.INACTIVED, this.__activeness[1]);

            //dispatch active event
            evtMgr.trigger(evtType.ACTIVED, this.__activeness[0]);

            return;
        },

        /**
         * Get the children by activeness
         * @method getActiveChildren
         * @return {ax/af/Component[]} return children array sort by activeness
         * @memberof ax/af/Container#
         */
        getActiveChildren: function () {
            return this.__activeness.slice(0);
        },

        /**
         * Get the most active child
         * @method
         * @param {Number} [activeIdx] - Get the child with specified activeness rank.
         * 0 by default. 0 is most active.
         * @return {ax/af/Component | null} return null if there's not found
         * @memberof ax/af/Container#
         */
        getActiveChild: function (activeIdx) {

            if (util.isUndefined(activeIdx)) {
                activeIdx = 0;
            }

            //out of scope
            if (activeIdx < 0 || activeIdx > this.__activeness.length - 1) {
                return null;
            }

            return this.__activeness[activeIdx];
        },
        /**
         * Get the most active leaf component from this node downwards
         * @method
         * @return {ax/af/Component} the most the most active leaf component
         * @memberof ax/af/Container#
         * @public
         */
        getActiveLeaf: function () {
            var leaf = this;
            while (!leaf.isLeaf()) {
                leaf = leaf.getActiveChild();
            }

            return leaf;
        },
        /**
         * Returns true if the child is active
         * @param {ax/af/Component} child child component to check
         * @return {Boolean} True if the child is active
         * @memberof ax/af/Container#
         * @method
         * @public
         */
        isChildActive: function (child) {
            return child === this.__activeness[0];
        },
        /**
         * Updates the internal _ancestorHidden field for current and child components
         * @memberof ax/af/Container#
         * @method
         * @protected
         */
        _updateAncestorHidden: function () {
            this._super();

            util.each(this._children, function (child) {
                // update all children recursively
                child._updateAncestorHidden();
            });
        },
        /**
         * Updates the internal _inDOMTree field for current and child components
         * @memberof ax/af/Container#
         * @method
         * @protected
         */
        _updateInDOMTree: function (isAttached) {
            this._super(isAttached);

            util.each(this._children, function (child) {
                child._updateInDOMTree(isAttached);
            });
        },
        /**
         * Returns true if itself is leaf component
         * @method
         * @return {Boolean} True if itself is leaf component
         * @memberof ax/af/Container#
         * @public
         */
        isLeaf: function () {
            return this._children.length === 0;
        }
    });
    return Container;
});
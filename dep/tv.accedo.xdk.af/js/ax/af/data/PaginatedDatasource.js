/**
 * PaginatedDatasource is a subclass of {@link ax/af/data/Datasource}, which encapsulate the data retrieval on a paginated data API.
 * Developer must provide a paginated data loader and set the page size before getting any data.
 * A page loader is a function that loads the data from a data provider based on page number.
 * This class is assuming the page number always start with 1.
 *
 * @class ax/af/data/PaginatedDatasource
 * @augments ax/af/data/Datasource
 * @author Marco Fan <marco.fan@accedo.tv>
 * @since 2.2.0
 * @example
 * // example for setting up a workable data source for grid
 * var ds = new PaginatedDatasource(),
 *     pageLoader = function (page) {
 *         // assume we have a module that fetch the data and return a promise
 *         // or
 *         // return ajax.request("http://back-end.api/movies?page=" + page).then....
 *         return RemoteApi.fetch(page)
 *             .then(function (response) {
 *                 // transform the response to the standard format
 *                 return promise.resolve({
 *                     data: response.dataEntries,
 *                     total: response.totalCount
 *                 });
 *             });
 *     };
 *
 * ds.setPageLoader(pageLoader);
 *
 * // the remote API return 50 items per page
 * ds.setPageSize(50);
 *
 * // setup finished, give it to the grid
 * this.grid.setDatasource(ds);
 */
define("ax/af/data/PaginatedDatasource", [
    "ax/class", "ax/af/data/Datasource", "ax/core", "ax/exception", "ax/util", "ax/promise"
], function (klass, Ds, core, exception, util, promise) {

    "use strict";

    var concatPages, dataLoader;

    /**
     * Concatenate the data loaded from different page loader to a single array.
     * This function assumes the `pages` are consecutive and follow the same sequence as the pages.
     * @method
     * @private
     * @param {LoadedData[]} pages The data loaded from the page loader
     * @returns {LoadedData} The combined loaded data object
     * @memberof ax/af/data/PaginatedDatasource#
     */
    concatPages = function (pages) {
        var data = [];

        // concat the data from different pages to a single array
        for (var i = 0, len = pages.length; i < len; i++) {
            data = data.concat(pages[i].data);
        }

        return {
            data: data,
            total: pages[0].total
        };
    };

    /**
     * The default data loader.
     * @method
     * @private
     * @param {Number} from The from index
     * @param {Number} size The number of items to load
     * @returns {LoadedData} The loaded data object
     * @memberof ax/af/data/PaginatedDatasource#
     */
    dataLoader = function (from, size) {
        var startPage = Math.floor(from / this._pageSize),
            endPage = Math.floor((from + size) / this._pageSize),
            loadPromises;

        // all required data sit in the same page
        if (startPage === endPage) {
            return this._pageLoader(startPage);
        }

        // for data that spread over pages
        loadPromises = [];
        for (var page = startPage; page <= endPage; page++) {
            loadPromises.push(this._pageLoader(page));
        }

        return promise.all(loadPromises)
            .then(concatPages);
    };

    return klass.create(Ds, {}, {

        /**
         * Page size of the remote API, which is the number of items returned per page.
         * @protected
         * @memberof ax/af/data/PaginatedDatasource#
         */
        _pageSize: -1,

        /**
         * Page loader, see {@link ax/af/data/PaginatedDatasource#setPageLoader|setPageLoader} for detail.
         * @protected
         * @memberof ax/af/data/PaginatedDatasource#
         */
        _pageLoader: null,

        /**
         * Overriding the inherited data loader.
         * This data loader convert the (from, size) pair to page number, and invoke the page loader for the data.
         * @protected
         * @memberof ax/af/data/PaginatedDatasource#
         */
        _dataLoader: dataLoader,

        /**
         * The ordinary data loader is not supported.
         * Use setPageLoader instead.
         * @method
         * @param {Function} loaderFn The data loader function
         * @throws {Exception} UNSUPPORTED_OPERATION
         * @memberof ax/af/data/PaginatedDatasource#
         */
        setDataLoader: function (loaderFn) {
            throw core.createException(exception.UNSUPPORTED_OPERATION, "PaginatedDatasource is dedicated to paginated data API, use setPageLoader instead.");
        },

        /**
         * Set the page loader for this DS.
         * A valid page loader should be a function which accepts a number (page), and return an array.
         * @method
         * @param {Function} loaderFn The page loader function
         * @memberof ax/af/data/PaginatedDatasource#
         * @example
         * var pageLoader = function (page) {
         *         // assume we have a module that fetch the data and return a promise
         *         return RemoteApi.fetch(page)
         *             .then(function (response) {
         *                 // transform the response to the standard format
         *                 return promise.resolve({
         *                     data: response.dataEntries,
         *                     total: response.totalCount
         *                 });
         *             });
         *     };
         *
         * ds.setPageLoader(pageLoader);
         */
        setPageLoader: function (loaderFn) {
            this._pageLoader = loaderFn;
        },

        /**
         * Set the page size.
         * This usually corresponds to the lower layer's configuration (eg. remote data API).
         * @method
         * @param {Number} size The page size
         * @memberof ax/af/data/PaginatedDatasource#
         */
        setPageSize: function (size) {
            this._pageSize = size;
        },

        /**
         * Get the page size set for this DS.
         * Return -1 if not set.
         * @method
         * @returns {Number} The page size, or -1 if not set.
         * @memberof ax/af/data/PaginatedDatasource#
         */
        getPageSize: function () {
            return this._pageSize;
        },

        /**
         * Get the `from` index of a page.
         * The `from` index is the data index of the first element in the particular page.
         * Total count has to be set before getting the index, or exception will be thrown.
         * If the page querying does not exist (number too large/small), exception will be thrown.
         * @method
         * @param {Number} page The page number, starting from 1
         * @returns {Number} The `from` index of the page
         * @throws {module:ax/exception.ILLEGAL_STATE} if page size is not set or total count is not ready
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} if page number is not valid
         * @memberof ax/af/data/PaginatedDatasource#
         */
        getFromIndexByPage: function (page) {
            this.__checkPageValidity(page);

            return this._pageSize * (page - 1);
        },

        /**
         * Get the `to` index of a page.
         * The `to` index is the data index of the last element in the particular page.
         * Total count has to be set before getting the index, or exception will be thrown.
         * If the page querying does not exist (number too large/small), exception will be thrown.
         * @method
         * @param {Number} page The page number, starting from 1
         * @returns {Number} The `to` index of the page
         * @throws {module:ax/exception.ILLEGAL_STATE} if page size is not set or total count is not ready
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} if page number is not valid
         * @memberof ax/af/data/PaginatedDatasource#
         */
        getToIndexByPage: function (page) {
            this.__checkPageValidity(page);

            return Math.min(this._pageSize * page, this._totalCount) - 1;
        },

        /**
         * Override the super class's method to load data using the internal data loader.
         * The `from`, `size` parameter will be modified based on the page size.
         * (The depracted minFetchSize takes no effect in this function.)
         * @method
         * @protected
         * @param {Number} from The start index of the to-be-loaded data
         * @param {Number} size The number of item to load
         * @returns {Promise.<Object[]>} The newly loaded data
         * @throws {Promise.<Exception>} ILLEGAL_STATE if the page loader and/or page size is not set
         * @throws {Promise.<Exception>} The reason why the dataLoader fails. The actual type is defined in the user-defined pageLoader.
         * @memberof ax/af/data/PaginatedDatasource#
         */
        _load: function (from, size) {
            if (!util.isNumber(this._pageSize) || this._pageSize <= 0) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "Page size is not set"));
            }

            if (!util.isFunction(this._pageLoader)) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "Page loader is not set"));
            }

            var remainder,
                fetchOpts;

            from = from || 0;
            size = size || 0;

            // align to the page start index
            remainder = from % this._pageSize;
            from -= remainder;
            size += remainder;

            fetchOpts = {
                from: from,
                size: size
            };

            return this._dataLoader(from, size)
                .then(util.bind(this._processFetchedData, this, fetchOpts));
        },

        /**
         * Check if the page number is valid.
         * @method
         * @private
         * @param {Number} page The page that going to access
         * @returns {Boolean} True if the page exist
         * @throws {module:ax/exception.ILLEGAL_STATE} if page size is not set or total count is not ready
         * @throws {module:ax/exception.ILLEGAL_ARGUMENT} if page number is not valid
         * @memberof ax/af/data/PaginatedDatasource#
         */
        __checkPageValidity: function (page) {
            if (!util.isNumber(this._pageSize) || this._pageSize <= 0) {
                throw core.createException(exception.ILLEGAL_STATE, "Page size is not set");
            }

            if (page <= 0) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "Page number should be positive");
            }

            var maxPage = this.__getMaxPageNumber();

            if (page > maxPage) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "Page number exceeded the total number of pages");
            }

            return true;
        },

        /**
         * Get the maximum number of pages.
         * Exception will be thrown if the total count is unknown.
         * @method
         * @private
         * @returns {Number} The number of pages available
         * @thorws {module:ax/exception.ILLEGAL_STATE} if total count is not set
         * @memberof ax/af/data/PaginatedDatasource#
         */
        __getMaxPageNumber: function () {
            if (this._totalCount < 0) {
                throw core.createException(exception.ILLEGAL_STATE, "Total count is unknown");
            }

            return Math.ceil(this._totalCount / this._pageSize);
        }
    });

});
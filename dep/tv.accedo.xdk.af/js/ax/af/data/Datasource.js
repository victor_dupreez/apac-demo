/**
 * Datasouce class, acts as an abstraction of back-end data.
 *
 * __Data loader__
 * To fetch the data, a data loader should be set in prior.
 * Data loader is basically a function that converts the response from the back-end API to a set of application data that will be used by the front-end.
 * A valid data loader accepts 2 numeric arguments (from, size), and returns a promise, which will return a {@link LoadedData} object when resolved.
 * The data loader is designed to fit the API with interfaces like (using RESTful API as an example) /movies?startIndex=0&endIndex=19 or /movies?from=0&to=19 or equivalent, where the range can be determined by _from_ and _size_ using simple arithmetic.
 * For API that applies pagination (e.g. /movies?page=1&pageSize=50), you are suggested to use {@link ax/af/data/PaginatedDatasource|PaginatedDatasource}.
 *
 *
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @class ax/af/data/Datasource
 * @param {Object} [opts] Options object
 * @example
 * var ds = new Datasource(),
 *     dataLoader = function(from, size) { // a data loader example
 *         // send ajax request to the back-end API
 *         return ajax.request("http://back-end.api/data?startIndex=" + from + "&endIndex=" + (from + size))
 *             .then(function (transport) {
 *                 // parse the response object, return the LoadedData
 *                 var response = transport.responseJSON,
 *                     dataArr = response.entries,
 *                     totalCount = response.totalCount;
 *
 *                 return {
 *                     data: dataArr,
 *                     total: totalCount
 *                 };
 *             });
 *     };
 *
 * ds.setDataLoader(dataLoader);
 *
 * // fetch data
 * ds.getRange(0, 19)
 *     .then(function (data) {
 *         console.log(data);
 *     }).done();
 */
/**
 * The return type of the data loader.
 * @typedef {Object} LoadedData
 * @property {Object[]} data The actual data entries
 * @property {Number} total The total number of items available in the DS
 */
define("ax/af/data/Datasource", ["ax/class", "ax/EventDispatcher", "ax/core", "ax/util", "ax/promise", "ax/console", "ax/exception", "ax/af/data/interface/Datasource"], function (klass, EvtDisp, core, util, promise, console, exception, IDatasource) {
    "use strict";
    return klass.create(EvtDisp, [IDatasource], {
        /**
         * Dispatched when datasource update
         * @event
         * @type {Object}
         * @property {String} action which dispatch
         * @memberof ax/af/data/Datasource
         */
        EVT_UPDATED: "ui:ds:updated"
    }, {
        /**
         * Minimun size of each data fetch
         * @memberof ax/af/data/Datasource#
         * @private
         * @deprecated since 2.2
         */
        __minFetchSize: 1,
        /**
         * Total number of items available.
         * @memberof ax/af/data/Datasource#
         * @protected
         */
        _totalCount: -1,
        /**
         * Data loader function provided by developer
         * @param {Number} from the from index that want to be laoded
         * @param {Number} size the length of the data to be loaded from the start index
         * @returns {Promise} Return a promise object. Return resolved promise with the data when the data is ready.
         * @memberof ax/af/data/Datasource#
         * @protected
         */
        _dataLoader: null,
        /**
         * Data updater function provided by developer
         * @param {String} action Action:  ax/af/data/interface/Datasource.ACTION.INSERT | ax/af/data/interface/Datasource.ACTION.REMOVE
         * @param {Object} [opts] Optional data
         * @param {Number} [opts.from] retrieve data from this index
         * @param {Number} [opts.to] retrieve data to this index
         * @param {Object[]} [opts.newData] new Data object array
         * @param {Number} [opts.size] data size to retrieve.
         *    It is ok to provide data amount less than this number.
         * @returns {Promise} Return a promise object
         *    takes parameters of reteived data in array, and an optional total data count
         * @memberof ax/af/data/Datasource#
         * @protected
         * @example
         * //action remove should return removedData and optional totalCount
         *      deferred.resolve({
         *            removedData: removedData,
         *             totalCount: self._totalCount - removedData.length
         *      });
         *
         * //action insert should return optional totalCount
         *      deferred.resolve({
         *           totalCount: self._totalCount + newData.length
         *      });
         */
        _dataUpdater: null,

        /**
         * Already fetched data
         * @memberof ax/af/data/Datasource#
         * @protected
         */
        _data: [],
        /**
         * Keep tracking on the current data inside the _data. It will store the start and end index (range) that current obtained.
         * @memberof ax/af/data/Datasource#
         * @example
         * //a sample with size 100 and 3 fragments of data
         * //each fragment will have four properties
         * //start - the start dsIndex in the this._data array of the current fragment
         * //end   - the end dsIndex in the this._data array of the current fragment inclusively
         * //prev  - the previous fragment end Index, -1 when it is the first one
         * //next  - the next fragment start index when it is last one
         * [
         *      {
         *          start:10,
         *           end: 20,
         *           prev:-1,
         *           next:35
         *       },{
         *          start:35,
         *          end:50,
         *          prev:20,
         *          next:80
         *       },{
         *          start:80,
         *          end:99,
         *          prev:50,
         *          next:-1
         *       }
         * ]
         * @protected
         */
        __dataObjArray: [],
        /**
         * Return the total count of the back-end data. It will be -1 before setTotalCount manually.
         * @method
         * @returns {Number} total count of the data
         * @public
         * @memberof ax/af/data/Datasource#
         */
        getTotalCount: function () {
            return this._totalCount;
        },
        /**
         * Set the total count of the back-end data.
         * @method
         * @param {Number} count total count of the data
         * @public
         * @memberof ax/af/data/Datasource#
         */
        setTotalCount: function (count) {

            var totalCount = this._totalCount;

            this._totalCount = count;

            //to create when the count is not initialized
            if (totalCount === -1) {
                this._data = new Array(count);
                return;
            }

            //XDK-1953 change total count
            //to add more data if there is not enough space in data array
            if (totalCount < count) {
                //to add new array item at the end
                this._data = this._data.concat(new Array(count - totalCount));
                return;
            }

            //to splice the extra data
            if (this._data.length > count) {
                this._data.splice(count, this._data.length - count);
                return;
            }


        },
        /**
         * Set minimun fetch size. This size is the minimun amount of data any load will get.
         * @method
         * @param {Number} size the minimun fetch size
         * @public
         * @deprecated since 2.2
         * @memberof ax/af/data/Datasource#
         */
        setMinFetchSize: function (size) {
            this.__minFetchSize = size;
        },
        /**
         * Get minimun fetch size. This size is the minimun amount of data any fetch will get.
         * @method
         * @returns {Number} the minimun fetch size
         * @public
         * @deprecated since 2.2
         * @memberof ax/af/data/Datasource#
         */
        getMinFetchSize: function () {
            return this.__minFetchSize;
        },
        /**
         * Checks if data of specific index exists
         * @method
         * @param {Number} from index of the data to check
         * @param {Number} [to] the end index of the data to check exclusively
         * @returns {Boolean} if it exists
         * @public
         * @memberof ax/af/data/Datasource#
         */
        hasData: function (from, to) {
            var ret = true;

            if (from < 0 || from >= this.getTotalCount() || to < 0 || to > this.getTotalCount()) {
                ret = false;
            }

            ret = this.__hasFetchedData(from, to);

            return ret;
        },
        /**
         * Get a range of data.
         * @method
         * @param {Number} from from index
         * @param {Number} to to index exclusively. If it is undefined and total count is set, all the data from index will be obtained.
         * @returns {Promise.<Object[]>} the data in range
         * @throws {Promise.<exception>} Incorrect Param from/to . From index should be less than the to index. And when total count is not set, only from index 0 is allowed.
         * @throws {Promise.<exception>} Incorrect State when total count is not set.
         * @public
         * @memberof ax/af/data/Datasource#
         */
        getRange: function (from, to) {
            var totalCount = this.getTotalCount(),
                self = this,
                newRange = [],
                i, getRangePromise;

            if (util.isUndefined(to)) {
                to = this.getTotalCount();
            }

            //if the from is larger than 0, the total count should set at the beginning.
            if (from < 0 || from >= to || (from > 0 && to === -1)) {
                return promise.reject(core.createException("IncorrectParam", "Incorrect \"from\" or \"to\" parameter!"));
            }

            //if the from is larger than the total count where no more data is available
            if (totalCount !== -1 && from >= totalCount) {
                return promise.reject(core.createException("IncorrectState", "No more data available!"));
            }

            //the first checking

            newRange = this.__checkFetchedData(from, to);

            //if all the data are existed then return directly
            if (newRange.length === 0) {
                return promise.resolve(this.__getFetchedData(from, to));
            }

            //create a promise which store for each range, it will fetch range by range
            getRangePromise = promise.resolve();

            for (i = 0; i < newRange.length; i++) {

                getRangePromise = getRangePromise.then(util.bind(function (fromIndex, toIndex) {
                    var actualFrom, fetchSize;

                    //to prevent the data is already included into previous load
                    if (self.hasData(fromIndex, toIndex)) {
                        return promise.resolve();
                    }

                    actualFrom = fromIndex;
                    fetchSize = toIndex - actualFrom;

                    //to load the data
                    return self._load(actualFrom, fetchSize);

                }, null, newRange[i].from, newRange[i].to));

            }

            return getRangePromise.then(function () {
                //after loaded all data, get back all the data
                return self.__getFetchedData(from, to);
            });

        },
        /**
         * Sets the data loader function. Must be called before any data can retrieved.
         * @method
         * @see {@link ax/af/data/Datasource#_dataLoader}
         * @param {Function} loaderFn the data loader function.
         * @public
         * @memberof ax/af/data/Datasource#
         */
        setDataLoader: function (loaderFn) {
            this._dataLoader = loaderFn;
        },

        /**
         * Sets the data loader function. Must be called before any data can retrieved.
         * @method
         * @see {@link ax/af/data/Datasource#_dataLoader}
         * @param {Function} loaderFn the data loader function.
         * @public
         * @memberof ax/af/data/Datasource#
         */
        setDataUpdater: function (updaterFn) {
            this._dataUpdater = updaterFn;
        },
        /**
         * Fetch a number of data into datasource, regardless whether data is loaded before
         * Emits an update event with "action" property equals to ax/af/data/interface/Datasource.ACTION.FETCH
         * @method
         * @param {Number} [from] the index to start fetching
         * @param {Number} [size] the size to fetch
         * @public
         * @memberof ax/af/data/Datasource#
         */
        fetch: function (from, size) {

            return this._load(from, size).then(util.bind(function (dataArray) {
                //dispatches updated assuming data is updated from source
                this.dispatchEvent(this.constructor.EVT_UPDATED, {
                    action: IDatasource.ACTION.FETCH,
                    newData: dataArray,
                    index: from,
                    length: dataArray.length
                });
            }, this));

        },
        /**
         * Internal method to load data into datasource using data loader.
         * @method
         * @param {Number} startIndex the start index of fetching
         * @param {Number} requestedSize the original size to requested fetch
         * @param {Array} dataArray the fecthed data in an array
         * @param {Number} totalCount total count of the back-end data
         * @returns {Promise.<Object[]>} the data array
         * @throws {Promise.<Mixed>} the reason why the dataLoader fails. The actual type is defined in the user-defined dataLoader.
         * @protected
         * @memberof ax/af/data/Datasource#
         */
        _load: function (from, size) {
            var originalSize = size,
                totalCount = this.getTotalCount(),
                fetchOpts;

            from = from || 0;

            size = size || 0;

            //update the size
            if (size < this.__minFetchSize) {

                size = this.__minFetchSize;

                //to prevent the size is over the total count
                if (totalCount !== -1 && from + size > totalCount) {
                    size = totalCount - from;
                }


                //to prevent the additional data is already loaded after updated the size, so keep the original size
                if (this.hasData(from + originalSize, from + size)) {
                    size = originalSize;
                }
            }


            fetchOpts = {
                from: from,
                size: size
            };

            return this._dataLoader(from, size)
                .then(util.bind(this._processFetchedData, this, fetchOpts));
        },
        /**
         * Process the data fetched by the data loader.
         * This includes setting the DS total count, setting the data into the internal data array.
         * Finally the required data will be returned.
         * @method
         * @protected
         * @param {Object} fetchOpts The options for the fetch operation
         * @param {Number} fetchOpts.from The from index of the data fetched
         * @param {Number} fetchOpts.size The expected number of items fetched
         * @param {Object} loadedObj The object consists of the loaded data, total count, and the optional from index
         * @returns {Object[]} The required data
         * @memberof ax/af/data/Datasource#
         */
        _processFetchedData: function (fetchOpts, loadedObj) {
            var dataArray = loadedObj.data,
                totalCount = loadedObj.total,
                from = fetchOpts.from,
                newFrom = from;

            // set the total count if available
            if (util.isNumber(totalCount)) {
                this.setTotalCount(totalCount);
            }

            // set the new from index
            if (util.isNumber(loadedObj.from)) {
                newFrom = loadedObj.from;
            }

            // ensure we have an array as argument
            if (!dataArray) {
                throw core.createException(exception.ILLEGAL_ARGUMENT, "Data is missing");
            }

            if (!util.isArray(dataArray)) {
                dataArray = [dataArray];
            }

            // save the data
            this.__setFetchedData(newFrom, dataArray);

            // return the dataArray
            return this.__getFetchedData(from, from + fetchOpts.size);
        },

        /**
         * Remove a range of items from the current data fetched. If the totalcount is not set, it is unable to remove.
         * Emits an update event with "action" property equals to ax/af/data/interface/Datasource.ACTION.REMOVE
         * @method
         * @param {Number} from starting index of items to be removed (0-th indexed)
         * @param {Number} [to] end index of items to be removed, removes only one item is not provided
         * @memberof ax/af/data/Datasource#
         * @returns {Promise.<Undefined>} no actual return value
         * @throws {Promise.<Object>} XDK custom exception
         * @public
         */
        remove: function (from, to) {

            if (!util.isNumber(from)) {
                return promise.reject(core.createException("Invalid param"));
            }

            if (!util.isNumber(to)) {
                to = from + 1;
            }

            if (!util.isFunction(this._dataUpdater)) {
                return promise.reject(core.createException("Datasource: invalid data updater function."));
            }

            return this._dataUpdater(IDatasource.ACTION.REMOVE, {
                from: from,
                to: to
            }).then(util.bind(function (removedObj) {

                if (!removedObj) {
                    return promise.reject(core.createException("Datasource: invalid data update via remove: no removed data received"));
                }

                var updatedFrom = removedObj.from,
                    updatedTo = removedObj.to,
                    removedData = [];


                if (util.isNumber(updatedFrom)) {
                    removedData = this.__removeFetchedData(updatedFrom, updatedTo);
                }

                //emit event
                this.dispatchEvent(this.constructor.EVT_UPDATED, {
                    action: IDatasource.ACTION.REMOVE,
                    index: updatedFrom,
                    length: removedData.length,
                    removedData: removedData
                });

            }, this));

        },
        /**
         * Insert an array of data into currently fetch data. If the totalcount is not set, it is unable to insert.
         * Emits an update event with "action" property equals to ax/af/data/interface/Datasource.ACTION.INSERT
         * @method
         * @param {Array} newData An array of items to be inserted
         * @param {Number} [index] position of the insertion
         * @returns {Promise.<Undefined>} no actual return value
         * @throws {Promise.<Object>} XDK custom exception
         * @memberof ax/af/data/Datasource#
         * @public
         */
        insert: function (newData, index) {

            if (!util.isArray(newData)) {
                newData = [newData];
            }

            if (!util.isNumber(index)) {
                index = this.getTotalCount();
            }

            if (!util.isFunction(this._dataUpdater)) {
                return promise.reject(core.createException("Datasource: invalid data updater function."));
            }

            return this._dataUpdater(IDatasource.ACTION.INSERT, {
                newData: newData,
                from: index
            }).then(util.bind(function (insertedObj) {

                if (!insertedObj) {
                    throw core.createException("Datasource: invalid data update via insert: no new data received");
                }

                var updatedData = insertedObj.newData,
                    updatedIndex = insertedObj.index;

                if (util.isArray(updatedData)) {
                    this.__insertFetchedData(updatedIndex, updatedData);
                }

                // emit event
                this.dispatchEvent(this.constructor.EVT_UPDATED, {
                    action: IDatasource.ACTION.INSERT,
                    index: updatedIndex,
                    length: updatedData.length,
                    newData: updatedData
                });

            }, this));

        },
        /**
         * Return the number of elements currently in the datasource
         * @method
         * @returns {Number} Current fetched size
         * @memberof ax/af/data/Datasource#
         * @depracted The latest version support fragments in datasource which may contain fragments (undefined items)
         * so it will return an array with respect to the datasource total count.
         * If developer want to keep track on the fetched count in datasource with fragments,
         * developers are suggested to count the size in the dataloader on the number of fetched items.
         */
        getFetchedCount: function () {
            return this._data.length;
        },
        /**
         * Return object of fetched data in the datasource
         * @method getFetchedData
         * @param {Number} index the index of data
         * @returns {Object} required data object
         * @memberof ax/af/data/Datasource#
         * @depracted The latest version support fragments in datasource which may contain fragments (undefined items)
         * so it may return an array that data between fragments with undefined value. Developer are suggested to use the getRange to obtain
         * the data. It will return the fetched Data directly if they are existed and send message to dataLoader if the current data is not enough.
         * @public
         */
        getFetchedData: function (index) {
            if (index < 0 || index >= this._data.length) {
                return null;
            }

            return this._data[index];
        },
        /**
         * Return array of fetched data in the datasource
         * @method getAllFetchedData
         * @returns {Array} object array of the data
         * @memberof ax/af/data/Datasource#
         * @deprecated since developers are suggested to use the getRange function to obtain the data.
         * It will return the fetched Data directly and send message to dataLoader if the specific data is not enough.
         * Now it will return the whole array that may contain undefined if items are between the fragments.
         * @public
         */
        getAllFetchedData: function () {
            return this._data;
        },
        /**
         * Removes all data and resets this datasource.
         * Emits an update event with "action" property equals to ax/af/data/interface/Datasource.ACTION.RESET
         * @method
         * @memberof ax/af/data/Datasource#
         */
        reset: function () {
            this.__minFetchSize = 1;

            this._data.length = 0; //Array of loaded data objects

            this.__dataObjArray.length = 0; //Array of dataset objects 

            this._totalCount = -1; //to reset the total Count

            //to notify 3rd party the ds is emptied,

            this.dispatchEvent(this.constructor.EVT_UPDATED, {
                action: IDatasource.ACTION.RESET
            });
        },
        /**
         * remove the from into the datasource
         * @private
         * @param {Number} from the startIndex to insert or update to the datasource
         * @param {Number} [to] the endIndex exclusively. Default will be the index of (from + 1) and assume one data is removed.
         * @returns {Array} the removed item
         * @method
         * @memberof ax/af/data/Datasource#
         */
        __removeFetchedData: function (from, to) {

            var removeList = [], //to store the dataset which will be delete soon
                removeStart = from,
                removeEnd = from, //inclusively,that it may equal with start
                removeSize,
                updatedNextDataObj = false,
                removedData = false,
                totalDataSet = this.__dataObjArray.length;

            //to set back to be from  and to should be larger than 0
            if (util.isNumber(removeEnd) && to > 0) {
                removeEnd = to - 1;
            }

            //removeSize 
            removeSize = removeEnd - removeStart + 1;

            util.each(this.__dataObjArray, util.bind(function (curDataSet, curArrIndex) {

                //just update the offset for the data behind
                //remove range is completely before the current range
                if ((removedData && removeEnd < curDataSet.start) || (removeEnd < curDataSet.start && removeStart < curDataSet.start)) {

                    curDataSet.start -= removeSize;
                    curDataSet.end -= removeSize;

                    if (curArrIndex < totalDataSet - 1) {
                        curDataSet.next -= removeSize;
                    } else {
                        curDataSet.next = -1;
                    }

                    //no need to update the prev index since there is no change
                    if (!updatedNextDataObj) {
                        updatedNextDataObj = true;
                        return;
                    }

                    if (curArrIndex > 0) {
                        curDataSet.prev -= removeSize;
                    } else {
                        curDataSet.prev = -1;
                    }

                    return;
                }

                //do nothing since not in the range and then continue with the next one
                if (removeStart > curDataSet.end) {

                    //if it is inside the gap between the fragments, the current dataset next is needed to update.
                    if (curDataSet.next > removeEnd) {
                        curDataSet.next -= removeSize;
                    }
                    return;
                }

                //the remove range is completely inside the cur data set 
                if (removeStart >= curDataSet.start && removeStart <= curDataSet.end && removeEnd >= curDataSet.start && removeEnd <= curDataSet.end) {
                    curDataSet.end -= removeSize;
                    removedData = true;
                    console.debug("remove dataset completely inside the cur data set " + removeStart);
                    return;
                }

                //the whole range is inside the remove range and then those data are put in the removeList
                if (removeStart <= curDataSet.start && curDataSet.end <= removeEnd) {

                    removeList.push(curArrIndex);

                    //update the relationship
                    if (curArrIndex > 0) {
                        this.__dataObjArray[curArrIndex - 1].next = curDataSet.next - removeSize;
                    } else {
                        this.__dataObjArray[curArrIndex].next = -1;
                    }

                    if (curArrIndex < totalDataSet - 1) {
                        this.__dataObjArray[curArrIndex + 1].prev = curDataSet.prev;
                    } else {
                        this.__dataObjArray[curArrIndex].prev = -1;
                    }

                    removedData = true;
                    console.debug("remove dataset completely cover the cur data set " + removeStart);
                    return;
                }

                //if only startIndex inside the range, then update the size of start
                if (removeStart < curDataSet.start && curDataSet.start <= removeEnd) {

                    curDataSet.start = removeStart;
                    curDataSet.end -= removeSize;

                    if (curArrIndex > 0) {
                        this.__dataObjArray[curArrIndex - 1].next = curDataSet.start;
                    } else {
                        curDataSet.prev = -1;
                    }

                    if (curArrIndex < totalDataSet - 1) {
                        this.__dataObjArray[curArrIndex + 1].prev = curDataSet.end;
                    } else {
                        curDataSet.next = -1;
                    }

                    removedData = true;

                    //check if need to merge them together
                    if (curArrIndex > 0 && curDataSet.prev !== -1 && curDataSet.prev + 1 === curDataSet.start) {

                        this.__dataObjArray[curArrIndex - 1].end = curDataSet.end;
                        this.__dataObjArray[curArrIndex - 1].next = curDataSet.next;

                        removeList.push(curArrIndex);
                    }

                    console.debug("remove dataset overlap with the cur data set startIndex " + removeStart);
                    return;
                }

                //if only endIndex inside the range, then update the size of end to "from"
                if (removeStart > curDataSet.start && curDataSet.end <= removeEnd) {

                    curDataSet.end = removeStart - 1;

                    if (curArrIndex < totalDataSet - 1) {
                        this.__dataObjArray[curArrIndex + 1].prev = curDataSet.end;
                    } else {
                        this.__dataObjArray[curArrIndex].prev = -1;
                    }

                    removedData = true;

                    console.debug("remove dataset overlap with the cur data set endIndex" + removeStart);
                    return;
                }

            }, this));

            //remove the item from list
            util.each(removeList, util.bind(function (targetIndex, removeArrIndex) {
                this.__dataObjArray.splice(targetIndex - removeArrIndex, 1);
            }, this));

            //udpate the totalcount by remove directly
            this._totalCount -= removeSize;

            //remove the data from the array
            return this._data.splice(removeStart, removeSize);
        },
        /**
         * insert the data into the datasource
         * @private
         * @param {Number} index the startIndex to insert or update to the datasource
         * @param {Array} data the data needed to insert
         * @returns {ax/af/data/Datasource} datasource itself
         * @method
         * @memberof ax/af/data/Datasource#
         */
        __insertFetchedData: function (index, data) {

            var insertPos = -1, //to store the dataset which will be delete soon
                insertStart = index,
                insertSize = data.length,
                insertEnd = insertStart + insertSize - 1, //inclusively,that it may equal with start
                updatedNextDataObj = false,
                insertedData = false,
                totalDataSet = this.__dataObjArray.length,
                newDataSet;

            //first case
            if (this.__dataObjArray.length === 0) {
                newDataSet = {
                    prev: -1,
                    next: -1,
                    start: insertStart,
                    end: insertEnd
                };

                this.__dataObjArray.push(newDataSet);
                this._totalCount += insertSize;

                this._data.splice.apply(this._data, [newDataSet.start, 0].concat(data));

                return this;
            }

            util.each(this.__dataObjArray, util.bind(function (curDataSet, curArrIndex) {

                //just update the offset for the data behind

                if (insertedData) {

                    curDataSet.start += insertSize;
                    curDataSet.end += insertSize;

                    if (curArrIndex < totalDataSet - 1) {
                        curDataSet.next += insertSize;
                    } else {
                        curDataSet.next = -1;
                    }

                    //no need to update the prev index since there is no change
                    if (!updatedNextDataObj) {
                        updatedNextDataObj = true;
                        return;
                    }

                    if (curArrIndex > 0) {
                        curDataSet.prev += insertSize;
                    } else {
                        curDataSet.prev = -1;
                    }

                    return;
                }


                //the insert range is before the cur data set 
                if (insertStart < curDataSet.start && insertStart > curDataSet.prev) {
                    insertPos = curArrIndex;

                    insertedData = true;

                    newDataSet = {
                        prev: curDataSet.prev,
                        next: curDataSet.start + insertSize,
                        start: insertStart,
                        end: insertEnd
                    };

                    curDataSet.prev = insertEnd;
                    curDataSet.end += insertSize;
                    curDataSet.start += insertSize;

                    return;
                }

                if (insertStart > curDataSet.end && (curDataSet.next > insertEnd || curDataSet.next === -1)) {

                    //if it is inside the gap between the fragments, the current dataset next is needed to update.
                    insertPos = curArrIndex + 1;

                    insertedData = true;

                    newDataSet = {
                        prev: curDataSet.end,
                        next: (curDataSet.next !== -1) ? (curDataSet.next + insertSize) : -1,
                        start: insertStart,
                        end: insertEnd
                    };

                    curDataSet.next = insertStart;

                    if (curArrIndex < this.__dataObjArray.length - 1) {
                        this.__dataObjArray[curArrIndex + 1].prev = insertEnd;
                    }

                    return;
                }

                //the insert range is inside the cur data set 
                if (insertStart >= curDataSet.start && insertStart <= curDataSet.end) {

                    curDataSet.end += insertSize;

                    insertedData = true;

                    console.debug("insert dataset completely inside the cur data set " + insertStart);
                    return;
                }

            }, this));

            if (insertPos > -1 && newDataSet) {
                this.__dataObjArray.splice(insertPos, 0, newDataSet);
            }

            //udpate the totalcount by add the size directly
            this._totalCount += insertSize;

            //insert the data into the array
            this._data.splice.apply(this._data, [insertStart, 0].concat(data));

            return this;
        },
        /**
         * Set the data into the datasource
         * @private
         * @param {Number} index the startIndex to insert or update to the datasource
         * @param {Array} data the data array
         * @returns {ax/af/data/Datasource} the datasource instance itself
         * @method
         * @memberof ax/af/data/Datasource#
         */
        __setFetchedData: function (index, data) {
            //default value of data set

            var newDataSet = {
                    start: index,
                    prev: -1,
                    next: -1,
                    end: index + data.length - 1
                },
                addNewDataSet = false,
                dstArrIndex = 0,
                targetDataSet, totalDataSet;

            if (this.getTotalCount() !== -1 && newDataSet.end >= this.getTotalCount()) {
                console.warn(index + data.length + "the size is over the total available size" + this.getTotalCount());
                return this;
            }

            //first case
            if (this.__dataObjArray.length === 0) {
                this.__dataObjArray.push(newDataSet);
                this._data.splice.apply(this._data, [newDataSet.start, data.length].concat(data));
                return this;
            }

            util.each(this.__dataObjArray, util.bind(function (curDataSet, curArrIndex) {

                //the ongoing new data set information
                totalDataSet = this.__dataObjArray.length;

                //search for the next item if this is not the last item which should be added at the end
                //and it should be looked for the next one and check whether add later
                if (curArrIndex !== totalDataSet - 1 && newDataSet.start > curDataSet.end + 1) { //+1 is to prevent overlap case
                    return;
                }

                //completely before the data set and add directly
                if ((curDataSet.prev === -1 || curDataSet.prev < newDataSet.start) && newDataSet.end + 1 < curDataSet.start) {

                    dstArrIndex = curArrIndex;

                    newDataSet.next = curDataSet.start;
                    newDataSet.prev = curDataSet.prev;

                    addNewDataSet = true;

                    console.debug("complete before the data set with startIndex " + curDataSet.start);

                    return util.breaker;
                }

                //completely after the data set and add directly.
                if ((curDataSet.next === -1 || curDataSet.next > newDataSet.end) && newDataSet.start > curDataSet.end + 1) {

                    dstArrIndex = curArrIndex + 1;

                    newDataSet.prev = curDataSet.end;
                    newDataSet.next = curDataSet.next;

                    addNewDataSet = true;

                    console.debug("complete after the data set with startIndex " + curDataSet.start);

                    return util.breaker;
                }

                //only overlap with one range change the current data set and no need to add a new dataset
                if (newDataSet.start > curDataSet.prev && (curDataSet.next === -1 || newDataSet.end < curDataSet.next)) {

                    if (newDataSet.start < curDataSet.start) {

                        curDataSet.start = newDataSet.start;

                        //change the relationship
                        if (curArrIndex > 0) {
                            this.__dataObjArray[curArrIndex - 1].next = curDataSet.start;
                        }
                    }

                    if (newDataSet.end > curDataSet.end) {

                        curDataSet.end = newDataSet.end;

                        //change the relationship
                        if (curArrIndex < totalDataSet - 1) {

                            //if they are connected, remove the next one
                            if (curDataSet.end + 1 === this.__dataObjArray[curArrIndex + 1].start) {

                                curDataSet.end = this.__dataObjArray[curArrIndex + 1].end;
                                curDataSet.next = this.__dataObjArray[curArrIndex + 1].next;

                                //remove the next one
                                this.__dataObjArray.splice(curArrIndex + 1, 1);
                            } else {
                                this.__dataObjArray[curArrIndex + 1].prev = curDataSet.end;
                            }

                        }

                    }

                    addNewDataSet = false;

                    console.debug("overlapped and update the data set" + curDataSet.start);

                    this._data.splice.apply(this._data, [newDataSet.start, data.length].concat(data));

                    return util.breaker;
                }

                //overlap with multiple ranges
                //remove the old dataset that cover by the new dataset
                targetDataSet = curDataSet;

                dstArrIndex = curArrIndex;

                while (curArrIndex < totalDataSet && newDataSet.end > targetDataSet.start) {

                    newDataSet.next = targetDataSet.next;
                    newDataSet.prev = targetDataSet.prev;

                    //update the front start
                    if (targetDataSet.start < newDataSet.start) {
                        newDataSet.start = targetDataSet.start;
                    }

                    //update the end
                    if (targetDataSet.end > newDataSet.end) {
                        newDataSet.end = targetDataSet.end;
                    }

                    //remove the current one
                    this.__dataObjArray.splice(curArrIndex, 1);

                    //update the total length 
                    totalDataSet = this.__dataObjArray.length;

                    //obtain the next one to be the current one
                    targetDataSet = this.__dataObjArray[curArrIndex];
                }

                addNewDataSet = true;
                console.debug("overlap with multiple range of dataset");

                return util.breaker;

            }, this));



            if (addNewDataSet) {

                this._data.splice.apply(this._data, [index, data.length].concat(data));

                this.__dataObjArray.splice(dstArrIndex, 0, newDataSet);

                totalDataSet = this.__dataObjArray.length;

                //update the relationship
                if (dstArrIndex < totalDataSet - 1) {
                    this.__dataObjArray[dstArrIndex + 1].prev = newDataSet.end;
                } else {
                    newDataSet.next = -1;
                }

                if (dstArrIndex > 0) {
                    this.__dataObjArray[dstArrIndex - 1].next = newDataSet.start;
                } else {
                    newDataSet.prev = -1;
                }

            }

            return this;

        },
        /**
         * Get the data from the datasource
         * @private
         * @param {Number} from the startIndex
         * @param {Number} [to] the end Index exclusively. default will be the index that from+1.
         * @returns {Array} the requested data
         * @method
         * @memberof ax/af/data/Datasource#
         */
        __getFetchedData: function (from, to) {
            if (util.isUndefined(to)) {
                to = from + 1;
            }
            //no checking on the data array list if the data is already exist
            return this._data.slice(from, to);
        },
        /**
         * To check if currently data is(are) included. If it doesn't include, it will return the new range
         * @private
         * @param {Number} from the start index
         * @param {Number} [to] the end index exclusively. default will be the index that from+1
         * @returns {Object[]} Object Array which is the missing data inside the range. If the length is 0, no missing data.
         * In each object, from and to will indicate the missing range.
         * @method
         * @memberof ax/af/data/Datasource#
         */
        __checkFetchedData: function (from, to) {
            var result = [],
                targetFrom = from,
                targetTo = to,
                hasAllData = false;

            //to ensure all the data has fetched
            util.each(this.__dataObjArray, util.bind(function (curDataSet) {

                //no contain the data
                if (targetTo < curDataSet.start) {
                    return util.breaker;
                }

                //only contain later end data and need to fetch the previos one
                if (targetFrom < curDataSet.start) {

                    result.push({
                        from: targetFrom,
                        to: curDataSet.start
                    });

                    //more than the current range and then update the from
                    if (targetTo > curDataSet.end + 1) {
                        targetFrom = curDataSet.end + 1;
                    } else {
                        hasAllData = true;
                        return util.breaker;
                    }

                    return;
                }

                if (targetFrom <= curDataSet.end) {

                    //contain front part of the data
                    if (targetTo > curDataSet.end + 1) {
                        targetFrom = curDataSet.end + 1;
                    } else {
                        hasAllData = true;
                        return util.breaker;
                    }

                    return;
                }

            }, this));

            if (!hasAllData) {
                result.push({
                    from: targetFrom,
                    to: targetTo
                });
            }

            return result;
        },
        /**
         * To check if currently data is(are) included.
         * @private
         * @param {Number} from the start index
         * @param {Number} [to] the end index exclusively. default will be the index that from+1
         * @returns {Boolean} true if it contains all the requested data. false if it doesn't consist all the desired data.
         * @method
         * @memberof ax/af/data/Datasource#
         */
        __hasFetchedData: function (from, to) {
            var result = false;

            //to ensure all the data has fetched
            util.each(this.__dataObjArray, util.bind(function (curDataSet) {

                if (from >= curDataSet.start && from <= curDataSet.end) {

                    if (util.isUndefined(to) || (to - 1 <= curDataSet.end)) {
                        result = true;
                    }

                    console.info("[XDK] Datasource contains part of the target range of data.");
                    return util.breaker;
                }

            }, this));

            return result;
        }

    });
});
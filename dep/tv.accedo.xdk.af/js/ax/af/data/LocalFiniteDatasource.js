/**
 * Local Finite DataSource which allow developer to update the datasource locally.
 * It provides the default updater and setDataUpdater when initialized.
 * when the data is inserted or removed from externally, it can amend or change the data before setting into the datasource.
 * @class ax/af/data/LocalFiniteDatasource
 *
 */
define("ax/af/data/LocalFiniteDatasource", ["ax/class", "ax/core", "ax/promise", "ax/af/data/FiniteDatasource"], function (klass, core, promise, FiniteDatasource) {
    "use strict";
    return klass.create(FiniteDatasource, {}, {

        init: function () {

            var self = this;

            this.setDataUpdater(function (action, opts) {

                var deferred = promise.defer();

                switch (action) {
                case "remove":
                    removeLocalData(opts.from, opts.to);
                    break;

                case "insert":
                    insertLocalData(opts.newData, opts.from);
                    break;
                }

                return deferred.promise;
                /**
                 * Internal method to remove local data
                 * @method
                 * @param {Number} from the startIndex to remove
                 * @param {Number} to the end index
                 * @returns {Promise.<Object[]>} from and to properties
                 * @private
                 * @memberof ax/af/data/LocalFiniteDatasource#
                 */
                function removeLocalData(from, to) {

                    var len = self.getTotalCount();

                    to = Math.min(to, len);

                    if (from >= to) {
                        deferred.reject(core.createException("Trying to remove self more local data then locally have"));
                        return;
                    }

                    //since at the beginning, the total count is -1 and need to update it when first insert
                    if (len === -1) {
                        deferred.reject(core.createException("ds incorrect total count", "Total Item should be set before insert the data"));
                        return;
                    }

                    deferred.resolve({
                        from: from,
                        to: to
                    });

                }
                /**
                 * Internal method to insert local data
                 * @method
                 * @param {Array} newData array object
                 * @param {Number} index the position to insert
                 * @returns {Promise.<Object[]>} newData which is the updated newData while index is the updated index
                 * @private
                 * @memberof ax/af/data/LocalFiniteDatasource#
                 */
                function insertLocalData(newData, index) {

                    //since at the beginning, the total count is -1 and need to update it when first insert
                    if (self.getTotalCount() === -1) {
                        deferred.reject(core.createException("ds incorrect total count", "Total Item should be set before insert the data"));
                        return;
                    }

                    deferred.resolve({
                        newData: newData,
                        index: index
                    });

                }
            });

        }

    });


});
/**
 * Finite Datasource has implemented datasource interface and inherit from the {@link ax/af/data/interface/Datasource} which provides extend feature for datasource with overflow.
 * It can be used in the {@link ax/ext/ui/grid/LoopedGrid} which support overflow data.
 * Since it supports overflow data, developers need to setTotalCount first before any implementation or usage on the datasource.
 *
 * For example, the totalCount is 100.
 * Developer can getRange(50,140) which will return the data with dsIndex [50,51...98,99,0,1,...39].
 * When getRange with overflow data, it will separate into 2 segments (which are 0 to 39 and 50 to 99) and then load them via the dataLoader.
 * After that, it will concatenate the data and return back the data to developers.
 *
 * Apart from the previous example,
 * Developer can also getRange(98,5) which will return the data with dsIndex [98,99,0,1,2,3,4].
 * When getRange with the above case, it will separate into 2 segments (which are 0 to 4 and 98 to 99) and then load them via the dataLoader.
 * After that, it will concatenate the data and return back the data to developers.
 *
 *
 * Meanwhile, fetch, remove, insert function will also support the overflow data.
 * Since it may involve more than one range, finite datasource will dispatch another event with segments as properties providing each segments information.
 * For example, when fetch(98,5). It will load 2 segments of data like 98 to 99 and 0 to 4. And then it will dispatch the event for the handler such as loopedGrid.
 * The handler will receive one event instead of 2 events in the original  {@link ax/af/data/interface/Datasource}.
 *
 * @class ax/af/data/FiniteDatasource
 */
define("ax/af/data/FiniteDatasource", ["ax/class", "ax/core", "ax/util", "ax/promise", "ax/console", "ax/exception", "ax/af/data/Datasource", "ax/af/data/interface/Datasource"],
    function (klass, core, util, promise, console, exception, Ds, IDatasource) {
        "use strict";

        function mod(number, divisor) { // modulo operation
            //in wiiU 1%0 is 0, so we try to standardize the %0 which should be NaN in all the platform
            if (divisor === 0) {
                return NaN;
            }
            return ((number % divisor) + divisor) % divisor;
        }

        return klass.create(Ds, {
            /**
             * Dispatched when datasource update {@see ax/af/data/Datasource.EVT_UPDATED}
             * @event
             * @type {Object}
             * @property {String} action which dispatch
             * @memberof ax/af/data/FiniteDatasource
             */
            EVT_UPDATED: Ds.EVT_UPDATED,
            /**
             * Dispatched when datasource update for the finiteds
             * @event EVT_FINITEDS_UPDATED
             * @type {Object}
             * @property {IDatasource.ACTION} action which dispatch like FETCH, REMOVE, INSERT, RESET
             * @property {ax/af/data/FiniteDatasource.NewSegment[] | ax/af/data/FiniteDatasource.RemoveSegment[]} [segments] the possible segments for the remove, fetch, insert
             * @memberof ax/af/data/FiniteDatasource
             * @example
             * ds.addEventListener(ds.EVT_FINITEDS_UPDATED, function(evt){
             *     switch(evt.action){
             *     case IDatasource.ACTION.FETCH:
             *     case IDatasource.ACTION.INSERT:
             *        //NewSegment
             *        var i, segments = evt.segments;
             *        for(i = 0 ; i < segments.length ; i++){
             *           //do sth or checking
             *           //segments.newData
             *           //segments.index
             *           //segments.length
             *        }
             *     break;
             *     case IDatasource.ACTION.REMOVE:
             *         //RemoveSegment
             *        for(i = 0 ; i < segments.length ; i++){
             *            //do sth or checking
             *           //segments.removedData
             *           //segments.index
             *           //segments.length
             *        }
             *     break;
             *     case IDatasource.ACTION.RESET:
             *         //do sth
             *     break;
             *     }
             * });
             *
             */
            /**
             * The insert/fetch segment
             * @typedef {Object} NewSegment
             * @property {Number} index The from index of the segment
             * @property {Object[]} newData the data array
             * @property {Number} length
             * @memberof ax/af/data/FiniteDatasource
             */
            /**
             * The remove segment
             * @typedef {Object} RemoveSegment
             * @property {Number} index The from index of the segment
             * @property {Object[]} removedData the data array
             * @property {Number} length
             * @memberof ax/af/data/FiniteDatasource
             */
            EVT_FINITEDS_UPDATED: "ui:finiteds:updated"
        }, {
            /**
             * Override the original to load data. It is different from the original which automtically set the fetched data into datasource directly.
             * Developer need to set the data into finite datasource after this function.
             * @method
             * @param {Number} from the start index of fetching
             * @param {Number} size the original size to requested fetch
             * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} when dataloader is not set
             * @protected
             * @memberof ax/af/data/FiniteDatasource#
             */
            _load: function (from, size) {

                if (!util.isFunction(this._dataLoader)) {
                    return promise.reject(core.createException(exception.ILLEGAL_STATE, "Datasource: invalid data loader function."));
                }

                from = from || 0;

                size = size || 0;

                return this._dataLoader(from, size);
            },
            /**
             * Get a range of data.
             * @method
             * @param {Number} from from index
             * @param {Number} to to index exclusively. If it is undefined, all the data from index will be obtained.
             * @returns {Promise.<Object[]>} the data in range
             * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} When the total count is not set or the totalcount is 0.
             * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} when dataloader is not set
             * @public
             * @memberof ax/af/data/FiniteDatasource#
             */
            getRange: function (from, to) {
                var totalCount = this.getTotalCount(),
                    self = this,
                    newRange = [],
                    notFetchedRange = [],
                    i, getRangePromise, pendingProcessedData = [];

                if (util.isUndefined(to)) {
                    to = totalCount;
                }

                //handle data part

                try {
                    //update the range, it will separate into 2 ranges when it consists exceeds the range
                    newRange = this.__updateRange(from, to);
                } catch (ex) {
                    return promise.reject(ex);
                }

                //the first checking
                util.each(newRange, function (range) {

                    notFetchedRange = notFetchedRange.concat(self.__checkFetchedData(range.from, range.to));

                });


                //create a promise which store for each range, it will fetch range by range
                getRangePromise = promise.resolve();

                for (i = 0; i < notFetchedRange.length; i++) {

                    getRangePromise = getRangePromise.then(util.bind(function (fromIndex, toIndex) {
                        var actualFrom, fetchSize;

                        //to prevent the data is already included into previous load
                        if (self.hasData(fromIndex, toIndex)) {
                            return promise.resolve();
                        }

                        actualFrom = fromIndex;
                        fetchSize = toIndex - actualFrom;

                        //to load the data
                        return self._load(actualFrom, fetchSize).then(util.bind(function (from, size, loadedObj) {

                            //which is for setFetch data after all the _load
                            pendingProcessedData.push(

                                {
                                    "fetchOpts": {
                                        "from": from,
                                        "size": size
                                    },
                                    "loadedObj": loadedObj
                                }

                            );
                        }, this, actualFrom, fetchSize));

                    }, null, notFetchedRange[i].from, notFetchedRange[i].to));

                }

                return getRangePromise.then(function () {

                    //set the fetched data at the end of the getRange
                    util.each(pendingProcessedData, function (eachRange) {

                        self._processFetchedData(eachRange.fetchOpts, eachRange.loadedObj);

                    });

                }).then(util.bind(this.__getOverflowFetchedData, this, from, to));

            },
            /**
             * Obtain the overflowed data array from the overflowed from and to
             * @method
             * @param {Number} from index of the data to check
             * @param {Number} to the end index of the data to check exclusively
             * @returns {Object[]} The data array
             * @private
             * @memberof ax/af/data/FiniteDatasource#
             */
            __getOverflowFetchedData: function (from, to) {
                //handle return data part which may concat the data based on the getRange
                var total = this.getTotalCount(),
                    resultArr = [],
                    tempFrom = from,
                    tempTo = to,
                    modFrom = mod(tempFrom, total),
                    modTo = mod(tempTo, total);

                //convert the `to` into the bigger number
                if (tempFrom > tempTo) {
                    tempTo = total + tempTo;
                }

                //after loaded all data, get back all the data
                if (tempTo < total) {
                    resultArr = resultArr.concat(this.__getFetchedData(modFrom, tempTo));
                    return resultArr;
                }

                resultArr = resultArr.concat(this.__getFetchedData(modFrom, total));
                tempFrom = tempFrom + total - modFrom;

                while (tempTo - tempFrom >= total) {
                    modFrom = mod(tempFrom, total);
                    resultArr = resultArr.concat(this.__getFetchedData(0, total));
                    tempFrom += total;
                }

                if (tempTo > tempFrom) {
                    modFrom = mod(tempFrom, total);
                    resultArr = resultArr.concat(this.__getFetchedData(modFrom, modTo));
                }

                return resultArr;
            },
            /**
             * Checks if data of specific index exists
             * @method
             * @param {Number} from index of the data to check
             * @param {Number} [to] the end index of the data to check exclusively
             * @returns {Boolean} if it exists
             * @public
             * @memberof ax/af/data/FiniteDatasource#
             */
            hasData: function (from, to) {
                var ret = true,
                    newRange = [];

                //check the range if available
                if (!this.__checkRange(from, to)) {
                    return false;
                }

                //update the range, it will separate into 2 ranges when it exceeds the range e.g 98 - 2.
                try {
                    newRange = this.__updateRange(from, to);
                } catch (ex) {
                    return false;
                }

                //check for every newRange to find if data exists and return.
                util.each(newRange, util.bind(function (curRange) {

                    ret = this.__hasFetchedData(curRange.from, curRange.to);

                    if (!ret) {
                        return util.breaker;
                    }

                }, this));

                return ret;
            },
            /**
             * Fetch a number of data into datasource, regardless whether data is loaded before
             * Emits a {@link ax/af/data/FiniteDatasource.EVT_FINITEDS_UPDATED} event with "action" property equals to IDatasource.ACTION.FETCH
             * @method
             * @param {Number} from the index to start fetching
             * @param {Number} [size=1] the size to fetch
             * @returns {Promise.<Undefined>}
             * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>}  When the toal count or the dataloader are not set
             * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>}  When the range is not valid
             * @public
             * @memberof ax/af/data/FiniteDatasource#
             */
            fetch: function (from, size) {
                var to, fetchPromise = [],
                    newRange = [],
                    i, curFrom, curSize, pendingProcessedData = [],
                    fetchedSegments = [],
                    self = this,
                    totalCount = this.getTotalCount();

                //if  the range is invalid, throw exception
                if (totalCount === -1) {
                    return promise.reject(core.createException(exception.ILLEGAL_STATE, "Datasource: Total count is not set."));
                }

                if (!util.isNumber(size)) {
                    size = 1;
                }

                if (from >= totalCount || size > totalCount || from < 0) {
                    return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Datasource: invalid range."));
                }

                to = from + size;

                try {
                    //update the range, it will separate into 2 ranges when it consists the range
                    newRange = this.__updateRange(from, to, true);
                } catch (ex) {
                    return promise.reject(ex);
                }

                for (i = 0; i < newRange.length; i++) {
                    curFrom = newRange[i].from;
                    curSize = newRange[i].to - curFrom;

                    fetchPromise.push(this._load(curFrom, curSize).then(util.bind(function (from, size, loadedObj) {
                        //which is the for setFetchdata after load
                        pendingProcessedData.push({
                            "fetchOpts": {
                                "from": from,
                                "size": size
                            },
                            "loadedObj": loadedObj
                        });

                        //which is for event dispatcher
                        fetchedSegments.push({
                            index: from,
                            newData: loadedObj.data,
                            length: loadedObj.data.length
                        });

                    }, this, curFrom, curSize)));

                }

                return promise.all(fetchPromise).then(function () {

                    //set the fetched data at the end of the getRange
                    util.each(pendingProcessedData, function (eachRange) {

                        self._processFetchedData(eachRange.fetchOpts, eachRange.loadedObj);

                    });

                    //dispatch the first segment
                    if (fetchedSegments.length > 0 && fetchedSegments[0]) {

                        self.dispatchEvent(self.constructor.EVT_UPDATED, util.extend({
                            action: IDatasource.ACTION.FETCH
                        }, fetchedSegments[0]));

                    }

                    //dispatches updated assuming data is updated from source
                    self.dispatchEvent(self.constructor.EVT_FINITEDS_UPDATED, {
                        action: IDatasource.ACTION.FETCH,
                        segments: fetchedSegments
                    });

                });

            },
            /**
             * Remove a range of items from the current data fetched. If the totalcount is not set, it is unable to remove.
             * Emits a {@link ax/af/data/FiniteDatasource.EVT_FINITEDS_UPDATED} event with "action" property equals to IDatasource.ACTION.REMOVE
             * @method
             * @param {Number} from starting index of items to be removed (0-th indexed)
             * @param {Number} [to] end index of items to be removed, removes only one item is not provided
             * @returns {Promise.<Undefined>}
             * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>}  When the range is not valid
             * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>}  When the toal count or the dataupdater are not set
             * @memberof ax/af/data/FiniteDatasource#
             * @public
             */
            remove: function (from, to) {
                var newRange = [],
                    removeRangePromise,
                    removedData = [],
                    removedSegments = [],
                    self = this,
                    i, ret;


                if (!util.isFunction(this._dataUpdater)) {
                    return promise.reject(core.createException(exception.ILLEGAL_STATE, "Datasource: invalid data updater function."));
                }

                if (!util.isNumber(from)) {
                    return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Datasource: invalid from index"));
                }

                if (!util.isNumber(to)) {
                    to = from + 1;
                }

                try {
                    ret = this.__checkRange(from, to);
                } catch (ex) {
                    return promise.reject(ex);
                }

                if (!ret) {
                    return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Datasource: invalid range."));
                }

                try {
                    //update the range, it will separate into 2 ranges when it exceeds the range
                    newRange = this.__updateRange(from, to);
                } catch (ex) {
                    return promise.reject(ex);
                }

                //create a promise which store for each range, it will fetch range by range
                removeRangePromise = promise.resolve();

                //to start with descending order
                for (i = newRange.length - 1; i > -1; i--) {

                    removeRangePromise = removeRangePromise.then(util.bind(this._dataUpdater, this, IDatasource.ACTION.REMOVE, {
                        from: newRange[i].from,
                        to: newRange[i].to
                    })).then(function (removedObj) {
                        if (!removedObj) {
                            throw core.createException("Datasource: invalid data update via remove: no removed data received");
                        }

                        var updatedFrom = removedObj.from,
                            updatedTo = removedObj.to;

                        if (util.isNumber(updatedFrom)) {
                            removedData = self.__removeFetchedData(updatedFrom, updatedTo);
                        }

                        removedSegments.push({
                            index: updatedFrom,
                            removedData: removedData,
                            length: removedData.length
                        });

                    });

                }

                if (newRange.length > 0) {
                    return removeRangePromise.then(function () {

                        //emit event
                        self.dispatchEvent(self.constructor.EVT_UPDATED, util.extend({
                            action: IDatasource.ACTION.REMOVE
                        }, removedSegments[0]));


                        //emit event when finished remove
                        self.dispatchEvent(self.constructor.EVT_FINITEDS_UPDATED, {
                            action: IDatasource.ACTION.REMOVE,
                            segments: removedSegments
                        });
                    });
                }

                return promise.resolve();
            },
            /**
             * Insert an array of data into currently fetch data. If the totalcount is not set, it is unable to insert.
             * Emits a {@link ax/af/data/FiniteDatasource.EVT_FINITEDS_UPDATED} event with "action" property equals to IDatasource.ACTION.INSERT
             * @method
             * @param {Array} newData An array of items to be inserted
             * @param {Number} [index] position of the insertion
             * @returns {Promise.<Undefined>} no actual return value
             * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>}  When the range is not valid
             * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>}  When the toal count or the dataupdater are not set
             * @memberof ax/af/data/FiniteDatasource#
             * @public
             */
            insert: function (newData, index) {
                var totalCount = this.getTotalCount();

                if (!util.isFunction(this._dataUpdater)) {
                    return promise.reject(core.createException(exception.ILLEGAL_STATE, "Datasource: invalid data updater function."));
                }

                if (totalCount === -1) {
                    return promise.reject(core.createException(exception.ILLEGAL_STATE, "Datasource: Total count is not set."));
                }

                if (index > totalCount || index < 0) {
                    return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Datasource: invalid index."));
                }

                if (!util.isArray(newData)) {
                    newData = [newData];
                }

                if (!util.isNumber(index)) {
                    index = totalCount;
                }

                return this._dataUpdater(IDatasource.ACTION.INSERT, {
                    newData: newData,
                    from: index
                }).then(util.bind(function (insertedObj) {

                    if (!insertedObj) {
                        throw core.createException("Datasource: invalid data update via insert: no new data received");
                    }

                    var updatedData = insertedObj.newData,
                        updatedIndex = insertedObj.index;

                    if (util.isArray(updatedData)) {
                        this.__insertFetchedData(updatedIndex, updatedData);
                    }

                    // emit event
                    this.dispatchEvent(this.constructor.EVT_UPDATED, {
                        action: IDatasource.ACTION.INSERT,
                        index: updatedIndex,
                        length: updatedData.length,
                        newData: updatedData
                    });

                    this.dispatchEvent(this.constructor.EVT_FINITEDS_UPDATED, {
                        action: IDatasource.ACTION.INSERT,
                        index: updatedIndex,
                        length: updatedData.length,
                        newData: updatedData
                    });


                }, this));

            },
            /**
             * Removes all data and resets this datasource.
             * @method
             * @memberof ax/af/data/FiniteDatasource#
             */
            reset: function () {
                this._super();
                this.dispatchEvent(this.constructor.EVT_FINITEDS_UPDATED, {
                    action: IDatasource.ACTION.RESET
                });
            },
            /**
             * The return type of the data loader.
             * @typedef {Object} Segment
             * @property {Number} from The from index of the segment
             * @property {Number} to the end index of the segment
             * @memberof ax/af/data/FiniteDatasource
             */
            /**
             * To find the related range involved
             * @private
             * @param {Number} fromRange the start index
             * @param {Number} toRange the end index exclusively.
             * @param {Boolean} [originalOrder=false] If original order is true, it will give [{from:98,to:99}, {from:0,to:2}] instead of [{from:0,to:2}, {from:98,to:99}]
             * @returns {ax/af/data/FiniteDatasource.Segment[]} Array of the segment of related item in ascending order.
             * @method
             * @memberof ax/af/data/FiniteDatasource#
             */
            __updateRange: function (fromRange, toRange, originalOrder) {
                var totalCount = this.getTotalCount(),
                    resultArr = [],
                    newFromRange, newToRange;

                //if  the range is invalid, throw exception
                if (totalCount === -1) {
                    throw core.createException(exception.ILLEGAL_STATE, "Datasource: Total count is not set.");
                }

                if (totalCount === 0) {
                    throw core.createException(exception.ILLEGAL_STATE, "Datasource: No more data.");
                }

                //if toRange smaller, it will be the case when size is 100, from:99, to: 2.. Then convert to become 102 which will be easier to map with and find the desired range.
                if (toRange < fromRange) {
                    toRange += totalCount;
                }

                //since it is over the total count size, it need to fetch all the item
                if (toRange - fromRange >= totalCount) {

                    resultArr.push({
                        from: 0,
                        to: totalCount
                    });

                    return resultArr;
                }

                newFromRange = mod(fromRange, totalCount);
                newToRange = mod(toRange, totalCount);

                //if the toRange exceed the limit, it will automatically mod the toRange
                if (toRange > totalCount) {

                    //separate into 2 ranges with ascending order
                    if (originalOrder) {

                        resultArr.push({
                            from: fromRange,
                            to: totalCount
                        });

                        resultArr.push({
                            from: 0,
                            to: newToRange
                        });

                        return resultArr;

                    }


                    resultArr.push({
                        from: 0,
                        to: newToRange
                    });

                    resultArr.push({
                        from: fromRange,
                        to: totalCount
                    });

                    return resultArr;
                }

                //when there is no change, return the originally range via array of range.

                resultArr.push({
                    from: fromRange,
                    to: toRange
                });

                return resultArr;

            },
            /**
             * To check if the range is available. It only allows parameter within the the totalcount size. E.g totalCount is 100. from and to can only be within 0-99.
             * from is 99, to is 3 / from is 3, to is 99 are possible cases
             * @private
             * @param {Number} from the start index
             * @param {Number} to the end index exclusively
             * @returns {Boolean} true if it is an available range.
             * @method
             * @memberof ax/af/data/FiniteDatasource#
             */
            __checkRange: function (from, to) {

                var total = this.getTotalCount();

                //fail to check range when the total count is -1
                if (total === -1) {
                    throw core.createException(exception.ILLEGAL_STATE, "Datasource: Total count is not set.");
                }

                //since to is exclusively, minus-1 is the last dsIndex
                if (!this.__checkIndex(from) || !this.__checkIndex(to - 1)) {
                    return false;
                }

                return true;

            },
            /**
             * To check if the index is within the range. Total count must be set before any action.
             * @private
             * @param {Number} no the index
             * @returns {Boolean} true if it is an valid index
             * @method
             * @memberof ax/af/data/FiniteDatasource#
             */
            __checkIndex: function (no) {
                var total = this.getTotalCount();

                if (total === -1) {
                    return false;
                }

                if (no < 0 || no >= total) {
                    return false;
                }

                return true;
            },
            /**
             * Return object of fetched data in the datasource
             * @method getFetchedData
             * @param {Number} index the index of data
             * @returns {Object|null} required data object or null when it is not valid index like negative index or datasource total count is less than 1.
             * @memberof ax/af/data/FiniteDatasource#
             * @public
             */
            getFetchedData: function (index) {
                var total = this.getTotalCount();
                if (index < 0 || total < 1) {
                    return null;
                }

                index = mod(index, total);

                return this._data[index];
            }
        });
    });
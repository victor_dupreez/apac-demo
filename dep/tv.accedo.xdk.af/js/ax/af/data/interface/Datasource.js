/**
 * Datasouce class, acts as an abstraction of back-end data.
/**
 * @class ax/af/data/interface/Datasource
 */
define("ax/af/data/interface/Datasource", ["ax/Interface"], function (Interface) {
    "use strict";
    var IDatasource = Interface.create("Datasource", {
        /**
         * Return the total count of the back-end data. It will be -1 before setTotalCount.
         * @method
         * @returns {Number} total count of the data
         * @public
         * @memberof ax/af/data/interface/Datasource
         */
        getTotalCount: [],
        /**
         * Set the total count of the back-end data.
         * @method
         * @param {Number} count total count of the data
         * @public
         * @memberof ax/af/data/interface/Datasource
         */
        setTotalCount: ["count"],
        /**
         * Checks if data of specific index exists
         * @method
         * @param {Number} from index of the data to check
         * @param {Number} [to] the end index of the data to check exclusively
         * @returns {Boolean} if it exists
         * @public
         * @memberof ax/af/data/interface/Datasource
         */
        hasData: ["from", "to"],
        /**
         * Get a range of data.
         * @method
         * @param {Number} from from index
         * @param {Number} to toindex exclusively.
         * @returns {Promise.<Object[]>} the data in range
         * @public
         * @memberof ax/af/data/interface/Datasource
         */
        getRange: ["from", "to"],
        /**
         * Data loader function provided by developer
         * @method loaderFn
         * @param {Number} from the from index that want to be laoded
         * @param {Number} size the length of the data to be loaded from the start index
         * @returns {Promise} Return a promise object. Return resolved promise with the data when the data is ready.
         * @memberof ax/af/data/interface/Datasource
         * @protected
         */
        /**
         * Sets the data loader function. Must be called before any data can retrieved.
         * @method setDataLoader
         * @see {@link ax/af/data/interface/Datasource.loaderFn}
         * @param {Function} loaderFn the data loader function.
         * @public
         * @memberof ax/af/data/interface/Datasource
         */
        setDataLoader: ["loaderFn"],
        /**
         * Data updater function provided by developer
         * @method updaterFn
         * @param {String} action Action: insert|remove
         * @param {Object} [opts] Optional data
         * @param {Number} [opts.from] retrieve data from this index
         * @param {Number} [opts.to] retrieve data to this index
         * @param {Object[]} [opts.newData] new Data object array
         * @param {Number} [opts.size] data size to retrieve.
         *    It is ok to provide data amount less than this number.
         * @returns {Promise} Return a promise object
         *    takes parameters of reteived data in array, and an optional total data count
         * @memberof ax/af/data/interface/Datasource
         */
        /**
         * Sets the data loader function. Must be called before any data can retrieved.
         * @method setDataUpdater
         * @see {@link ax/af/data/interface/Datasource.updaterFn}
         * @param {Function} updaterFn the data loader function.
         * @public
         * @memberof ax/af/data/interface/Datasource
         */
        setDataUpdater: ["updaterFn"],
        /**
         * Fetch a number of data into datasource, regardless whether data is loaded before
         * Emits an update event with "action" property equals to IDatasource.ACTION.FETCH
         * @method
         * @param {Number} [from] the index to start fetching
         * @param {Number} [size] the size to fetch
         * @public
         * @memberof ax/af/data/interface/Datasource
         */
        fetch: ["from", "size"],
        /**
         * Remove a range of items from the current data fetched. If the totalcount is not set, it is unable to remove.
         * Emits an update event with "action" property equals to IDatasource.ACTION.REMOVE
         * @method
         * @param {Number} from starting index of items to be removed (0-th indexed)
         * @param {Number} [to] end index of items to be removed, removes only one item is not provided
         * @memberof ax/af/data/interface/Datasource
         * @returns {Promise.<Undefined>} no actual return value
         * @throws {Promise.<Object>} XDK custom exception
         * @public
         */
        remove: ["from", "to"],
        /**
         * Insert an array of data into currently fetch data. If the totalcount is not set, it is unable to insert.
         * Emits an update event with "action" property equals to IDatasource.ACTION.INSERT
         * @method
         * @param {Array} newData An array of items to be inserted
         * @param {Number} [index] position of the insertion
         * @returns {Promise.<Undefined>} no actual return value
         * @throws {Promise.<Object>} XDK custom exception
         * @memberof ax/af/data/interface/Datasource
         * @public
         */
        insert: ["newData", "index"],
        /**
         * Removes all data and resets this datasource.
         * Emits an update event with "action" property equals to IDatasource.ACTION.RESET
         * @method
         * @memberof ax/af/data/interface/Datasource
         */
        reset: []
    });

    /**
     * Collection of the datasource update action
     * To provide the possible action for the datasource event udpate
     * @enum {String}
     * @memberof ax/af/data/interface/Datasource
     */
    IDatasource.ACTION = {
        /** Data insert */
        INSERT: "insert",
        /** Data remove */
        REMOVE: "remove",
        /** Data fetch */
        FETCH: "fetch",
        /** Data reset */
        RESET: "reset"
    };

    return IDatasource;
});
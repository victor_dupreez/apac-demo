/**
 * A reference pointing to a model's attribute.
 * This class is normally consumed by widgets, so widgets can data-bind display elements with model data.
 * @class ax/af/mvc/ModelRef
 * @param {ax/af/mvc/Model} model the model instance
 * @param {String} attr the attribute name
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/mvc/ModelRef", ["ax/class", "ax/af/mvc/Model", "ax/core"], function (klass, Model, core) {
    "use strict";
    return klass.create({}, {
        /**
         * Model instance
         * @protected
         * @memberof ax/af/mvc/ModelRef#
         */
        _model: null,
        /**
         * Attribute name
         * @protected
         * @memberof ax/af/mvc/ModelRef#
         */
        _attr: null,
        /**
         * overrides parent init() method
         * @protected
         * @method
         * @memberof ax/af/mvc/ModelRef#
         */
        init: function (model, attr) {
            if (!(model instanceof Model) || !attr) {
                throw core.createException("IncorrectParam", "Incorrect parameter when trying to create a ModelRef instance!");
            }
            this._model = model;
            this._attr = attr;
        },
        /**
         * Gets the model instance we are referring
         * @public
         * @return {ax/af/mvc/Model} the model instance
         * @method
         * @memberof ax/af/mvc/ModelRef#
         */
        getModel: function () {
            return this._model;
        },
        /**
         * Gets the attribute name we are referring
         * @public
         * @return {String} attribute name
         * @method
         * @memberof ax/af/mvc/ModelRef#
         */
        getAttr: function () {
            return this._attr;
        },
        /**
         * Gets the attribute value we are referring
         * @public
         * @return {Mixed} attribute value of the model
         * @method
         * @memberof ax/af/mvc/ModelRef#
         */
        getVal: function () {
            return this._model.get(this._attr);
        }
    });
});
/**
 * Application root controller, the root for the controller hierarchy.
 * Responsible for App level logic such as default key actions.
 *
 * This class is designed as a singleton, developer should use {@link ax/af/mvc/AppRoot.singleton|singleton} to obtain the instance.
 * Creating instance using the _new_ keyword is prohibited.
 *
 * @class ax/af/mvc/AppRoot
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/af/mvc/AppRoot", [
    "ax/class",
    "ax/af/mvc/Controller",
    "ax/core",
    "ax/Env",
    "ax/af/Container",
    "ax/promise",
    "ax/util",
    "ax/device/vKey",
    "ax/af/evt/type",
    "ax/af/evt/eventManager",
    "ax/device",
    "ax/af/mvc/ControllerManager",
    "ax/af/focusManager",
    "ax/exception",
    "require",
    "css",
    "css!./css/AppRoot"
], function (klass,
    Controller,
    core,
    Env,
    Container,
    promise,
    util,
    vKey,
    evtType,
    evtMgr,
    device,
    ControllerManager,
    focusMgr,
    exception,
    require) {
    "use strict";
    var AppRoot,
        instance,
        sEnv = Env.singleton(),
        sCtrlMgr = ControllerManager.singleton();

    AppRoot = klass.create(Controller, {
        /**
         * Get the singleton instance of this class.
         * @method
         * @static
         * @returns {ax/af/mvc/AppRoot} The singleton
         * @memberOf ax/af/mvc/AppRoot
         */
        singleton: function () {
            if (!instance) {
                instance = new AppRoot();
            }

            return instance;
        }
    }, {
        /**
         * whether environment is loaded
         * @protected
         * @memberof ax/af/mvc/AppRoot#
         */
        _envLoaded: false,
        /**
         * mapping of key actions
         * @protected
         * @memberof ax/af/mvc/AppRoot#
         */
        _keyActions: {},
        /**
         * Overrides the init method
         * @method
         * @protected
         * @memberof ax/af/mvc/AppRoot#
         */
        init: function () {
            this._super();
            var self = this;
            sEnv.addEventListener(sEnv.EVT_ONLOAD, function () {

                self._onEnvLoad();

            });

        },
        /**
         * An env onload listener
         * @method
         * @protected
         * @memberof ax/af/mvc/AppRoot#
         */
        _onEnvLoad: function () {
            this._envLoaded = true;

            var self = this,
                view = new Container({
                    id: "#TVArea"
                }),
                resolution = device.system.getDisplayResolution(),
                defaultKeyAction;

            view.getRoot().appendTo(core.root.document.body);
            view.addClass(device.platform);

            view.addClass("_" + resolution.width + "x" + resolution.height);

            this.setView(view);

            focusMgr._setRootController(this);

            // default key behaviors
            defaultKeyAction = util.bind(function (evt) {
                var handled;

                // handle external key actions in priority
                if (this._keyActions[evt.id]) {
                    handled = this._keyActions[evt.id](evt);
                    if (handled) {
                        return;
                    }
                }

                switch (evt.id) {
                case vKey.EXIT.id:
                    device.system.exit();
                    break;
                case vKey.UP.id:
                case vKey.DOWN.id:
                case vKey.LEFT.id:
                case vKey.RIGHT.id:
                    focusMgr.directionChangeByKey(evt, this.getView());
                    break;


                case vKey.OK.id:
                    // translate enter key into click
                    if (evt.target.isClickable()) {
                        evtMgr.trigger(evtType.CLICK, evt.target);
                    }
                    break;
                default:
                    return;
                }
            }, this);
            // fire key event into MVC structure
            sEnv.addEventListener(sEnv.EVT_ONKEY, function (evt) {
                var curFocus = require("ax/af/focusManager").getCurFocus();

                // if not focus, at least have default action
                curFocus = curFocus || self.getView();

                evtMgr.trigger(evtType.KEY, curFocus, util.extend({
                    source: evt.source,
                    defaultAction: defaultKeyAction
                }, evt));
            });


            /**
             * @TODO loading controller stuff
             */
        },
        /**
         * Register default key action handlin. Default action will be called when
         * key events reached the AppRoot during event propagation. Each
         * key can only be registered once.
         *
         * @method
         * @public
         * @name setDefaultKeyAction
         * @param {Array} keyIds Array of vKeys to be registered. e.g. "device:vKey:back".
         * @param {Function} actionHandler Action handler to be called
         * @memberof ax/af/mvc/AppRoot#
         */
        setDefaultKeyAction: function (keyIds, actionHandler) {

            if (!util.isArray(keyIds)) {
                keyIds = [keyIds];
            }

            util.each(keyIds, function (key) {

                if (this._keyActions[key]) {
                    throw core.createException("KeyactionAlreadyRegisterd", "Key (" + key + ") is already set");
                }

                this._keyActions[key] = actionHandler;

            }, this);

        },
        /**
         * Sets a controller to be the main controller of the application.
         * @method
         * @public
         * @param {ax/af/mvc/Controller} controller The controller instance or class to use.
         * @param {Object} [options] Options for this operation.
         * @param {Object} [options.context] Context object to be passed to the new controller
         *     Refer to {@link ax/af/mvc/ControllerManager#replaceController} for details.
         * @return {Promise.<ax/af/mvc/Controller>} the controller instance
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} When the existing controller is not attached to parent
         * @memberof ax/af/mvc/AppRoot#
         */
        setMainController: function (controller, options) {
            if (!this._envLoaded) {
                return promise.reject(core.createException(exception.ILLEGAL_STATE, "AppRoot is waiting for environment to load, therefore it is not ready!"));
            }
            options = options || {};
            var activeChild, activeCtrl;

            activeChild = this.getView().getActiveChild();

            if (activeChild) {
                activeCtrl = activeChild.getController();
                return sCtrlMgr.replace(activeCtrl, controller, options);
            }

            // flag event on first main controller
            options.appRootFirstController = true;

            return sCtrlMgr.open(controller, this, options);
        },
        /**
         * Overrides the {@link ax/af/mvc/Controller#getParentController|getParentController} method
         * @return {null} returns null as this is the application root
         * @method
         * @public
         * @memberof ax/af/mvc/AppRoot#
         */
        getParentController: function () {
            return null;
        }
    });

    // enforce the instance creation before returning, preventing 2 instances being created
    // (2 executions may go into the creation at the same time...)
    AppRoot.singleton();

    return AppRoot;

});
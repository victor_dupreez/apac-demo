/**
 * A base controller class.
 * @class ax/af/mvc/Controller
 * @author Daniel Deng <daniel.deng@accedo.tv>
 * @example
 *  var myController = klass.create(Controller, {}, {
 *       init: function() {
 *           this.setView(view.render(mainTmpl, {}));
 *           this._super();
 *       },
 *       setup: function(context) {
 *           ....
 *       }
 *  });
 */
define("ax/af/mvc/Controller", ["ax/class", "ax/core", "ax/util", "ax/af/mvc/view", "ax/console", "ax/promise"], function(klass, core, util, view, console, promise) {
    "use strict";
    view.hookTemplateRenderer("controller", function(template) {
        var Ctrl = template.controller;

        if (!Ctrl.prototype instanceof Controller) {
            throw core.createException("IncorrectAttribute", "Controller attribute should be a Sub-Class of Controller");
        }

        view = (new Ctrl()).getView();

        return view;
    });

    var Controller = klass.create({}, {
        /**
         * The view this controller attached to.
         * @memberof ax/af/mvc/Controller#
         * @protected
         */
        _view: null,
        /**
         * The sub-controllers array.
         * @memberof ax/af/mvc/Controller#
         * @protected
         */
        _subControllers: [],
        /**
         * The parent controller.
         * @memberof ax/af/mvc/Controller#
         * @protected
         */
        _parentController: null,
        /**
         * The controller"s ID, please use {@link ax/af/mvc/Controller#getId()|getId()} to get it.
         * @memberof ax/af/mvc/Controller#
         * @private
         */
        __id: null,
        /**
         * Overriding the deinit function
         * @protected
         * @method
         * @memberof ax/af/mvc/Controller#
         */
        deinit: function() {
            this._view.deinit();

            this._view = null;
        },
        /**
         * Set view for the current controller.
         * @param {ax/af/Componenet} component View object
         * @public
         * @method
         * @memberof ax/af/mvc/Controller#
         */
        setView: function(component) {
            if (this._view) {
                throw core.createException("ViewAlreadySet", "This controller already has a view set, it is not supposed to be overwritten!");
            }
            this._view = component;
            this._view.attachController(this);

            // start tracking the sub-controllers in the view
            var subControllers = this._extraSubController(),
                self = this;
            util.each(subControllers, function(subCtrl) {
                self._addSubController(subCtrl);
            });
        },
        /**
         * Extra sub-controllers from a view, so that when a view is set,
         *     all the sub-controllers is being kept track of.
         * @param {ax/af/Componenet} [view] component object to set as view
         *     default value: view of cucrent controller
         * @param {Array} [subControllers] an array to contain all the discovered sub-controllers
         * @return {Array} the discovered sub-controllers
         * @protected
         * @method
         * @memberof ax/af/mvc/Controller#
         */
        _extraSubController: function(view, subControllers) {
            // check parameters
            if (!view && this._view) {
                view = this._view;
            }
            if (!subControllers) {
                subControllers = [];
            }

            var controller = view.getAttachedController(),
                children, curChild, i, len;
            if (view !== this._view && controller) {
                // found controller already
                subControllers.push(controller);
                return subControllers;
            }

            if (view.isLeaf()) {
                // no more children, just return
                return subControllers;
            }

            // search all children
            children = view.getChildren();
            len = children.length;
            for (i = 0; i < len; i++) {
                curChild = children[i];
                subControllers = this._extraSubController(curChild, subControllers);
            }
            return subControllers;
        },
        /**
         * Get view from the current controller.
         * @public
         * @return {ax/af/Componenet} The attached view object
         * @method
         * @memberof ax/af/mvc/Controller#
         */
        getView: function() {
            return this._view;
        },
        /**
         * Get the current controller"s ID.
         * @public
         * @return {String} the current controller's ID
         * @method
         * @memberof ax/af/mvc/Controller#
         */
        getId: function() {
            if (!this.__id) {
                this.__id = "" + core.getGuid();
            }
            return this.__id;
        },
        /**
         * Find a controller instance by its class.
         * @public
         * @param {ax/af/mvc/Controller} Controller The controller class
         * @return {ax/af/mvc/Controller} the controller instance
         * @method
         * @memberof ax/af/mvc/Controller#
         */
        getControllerByClass: function(Controller) {
            if (this instanceof Controller) {
                return this;
            }
            var i, len = this._subControllers.length,
                result;
            for (i = 0; i < len; i++) {
                result = this._subControllers[i].getControllerByClass(Controller);
                if (result) {
                    return result;
                }
            }
            return false;
        },
        /**
         * Setup the controller as well as the sub-controllers if context data for them exists.
         * @memberof ax/af/mvc/Controller#
         * @method
         * @param {Object} context - Data for controller to update
         * @returns {Promise.<Undefined>} when all the subcontrollers(optionals) and postSetup(optional) are run.
         * @protected
         */
        _setupSubtree: function(context) {
            var i = 0,
                len = this._subControllers.length,
                subController,
                subCtrlrId,
                subContext,
                pendingSubcontrollers = [],
                pending,
                setupPending,
                isSetupPendingPromise = false;


            setupPending = this.setup(context);

            if (promise.isPromise(setupPending)) {
                isSetupPendingPromise = true;
            }

            //setup sub controllers if context.subContexts[controllerId] exists
            for (; i < len; i++) {
                subController = this._subControllers[i];
                subCtrlrId = subController.getId();

                if (context && context.subContexts && context.subContexts[subCtrlrId]) {
                    subContext = context.subContexts[subCtrlrId];
                } else {
                    subContext = {};
                }

                if (isSetupPendingPromise) {

                    //wait for parent controller to finish setup
                    pendingSubcontrollers.push(setupPending.then(function() {
                        return subController._setupSubtree(subContext);
                    }));

                } else {
                    pendingSubcontrollers.push(
                        subController._setupSubtree(subContext)
                    );
                }
            }

            pending = pendingSubcontrollers.length > 0 ? promise.all(pendingSubcontrollers) : setupPending;

            if (this.postSetup) {

                if (pending && isSetupPendingPromise) {
                    return pending.then(util.bind(function() {
                        this.postSetup(context);
                    }, this));
                }

                return promise.resolve(this.postSetup(context));
            }

            if (promise.isPromise(pending)) {
                return pending;
            }

            return promise.resolve();

        },
        /**
         * Setup the controller with context data. This function will be called
         * before the controller is being show.
         * Developer should hanlde controller's context data here.
         * @memberof ax/af/mvc/Controller#
         * @protected
         * @param {Object} [context] - Data for controller to update from
         */
        setup: function(context) {
            console.debug("controller set up" + context);
        },
        /**
         * Reset the subcontrollers, then calls this controller's reset.
         * @protected
         * @memberof ax/af/mvc/Controller#
         * @method
         */
        _resetSubtree: function() {

            var i = 0,
                len = this._subControllers.length;

            //setup sub controllers if context.subContexts[controllerDef] exists
            for (; i < len; i++) {
                this._subControllers[i]._resetSubtree();
            }

            this.reset();
        },
        /**
         * Reset controller's current data. This function will be called before
         * a controller is hidden. Developer should handle resetting the
         * controllers context data here.
         * @memberof ax/af/mvc/Controller#
         * @method
         * @protected
         */
        reset: function() {},
        /**
         * Track one more sub-controller that is to add to this controller
         * @protected
         * @method
         * @param {ax/af/mvc/Controller} controller instance
         * @return {Boolean} successful or not
         * @memberof ax/af/mvc/Controller#
         */
        _addSubController: function(controller) {

            if (this._subControllers.indexOf(controller) > 0) {
                console.warn(controller.getId() + " is already a sub-controller of " + this.getId());
                return false;
            }

            this._subControllers.push(controller);
            controller._parentController = this;

            return true;
        },
        /**
         * Track one less sub-controller that was added to this controller
         * @protected
         * @method
         * @param {ax/af/mvc/Controller} controller instance
         * @return {Boolean} successful or not
         * @memberof ax/af/mvc/Controller#
         */
        _removeSubController: function(controller) {

            var idx = this._subControllers.indexOf(controller);

            if (idx < 0) {
                return false;
            }
            this._subControllers.splice(idx, 1);

            return true;
        },
        /**
         * Retreive the sub-controllers array
         * @method
         * @return {Array} Controllers
         * @public
         * @memberof ax/af/mvc/Controller#
         */
        getSubControllers: function() {
            return this._subControllers;
        },
        /**
         * Retreive the a sub controller attached to this controller by a controller class or an index
         * @param {Number|ax/af/mvc/Controller} klass class or the array index for the sub-controller
         * @method
         * @return {ax/af/mvc/Controller|null} The specified controller or null if not found
         * @public
         * @memberof ax/af/mvc/Controller#
         */
        getSubController: function(klass) {
            if (util.isNumber(klass)) {
                return this._subControllers[klass];
            }

            var subCtrlr, i, len = this._subControllers.length;

            for (i = 0; i < len; i++) {
                subCtrlr = this._subControllers[i];
                if (subCtrlr instanceof klass) {
                    return subCtrlr;
                }
            }

            for (i = 0; i < len; i++) {
                subCtrlr = this._subControllers[i].getSubController();
                if (subCtrlr) {
                    return subCtrlr;
                }
            }

            return null;
        },
        /**
         * Gets the parent controller
         * @method
         * @return {ax/af/mvc/Controller} The parent controller
         * @public
         * @memberof ax/af/mvc/Controller#
         */
        getParentController: function() {
            return this._parentController;
        },
        /**
         * Retreives the context object, along with the sub-controllers' contexts
         * @method
         * @return {Object} The context object, also, all the sub-controller contexts will be stored under key "subContexts"
         * @public
         * @memberof ax/af/mvc/Controller#
         */
        getContextTree: function() {

            var subCtrlr, context = this.getContext(),
                subContext = null,
                i = 0,
                len = this._subControllers.length;

            for (; i < len; i++) {
                subCtrlr = this._subControllers[i];
                subContext = subCtrlr.getContextTree();
                if (subContext) {
                    context = context || {};
                    context.subContexts = context.subContexts || {};
                    context.subContexts[subCtrlr.getId()] = subContext;
                }
            }
            return context;
        },
        /**
         * Retreives the context object, suppose to be override by extending controller
         * @method
         * @return {Object} The context object (which would be null for this base class)
         * @public
         * @memberof ax/af/mvc/Controller#
         */
        getContext: function() {
            return null;
        }
    });

    return Controller;
});
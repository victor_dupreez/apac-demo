/**
 * A basic model impelementation class.
 * For any attribute changes, events will be fired with name of: "change" or "change:[attr name]".
 *
 * The handler function will receive data with:
 *
 *  Attribute | Value
 *  ----------|------
 *  data.attr | the attribute name
 *  data.type | the event type
 *  data.oldValue | the old value
 *  data.newValue | the new value
 * @class ax/af/mvc/Model
 * @example
 * var User, alice, bob;
 * User = klass.create(Model,
 *    defaults:{
 *       gender: "m", // default value
 *       name: "Unnamed" // default value
 *   }, {});
 * alice = new User({
 *       name: "Alice",
 *       gender: "f"
 *   });
 * bob = new User({
 *       name: "Bob",
 *       gender: "m"
 *   })
 */
define("ax/af/mvc/Model", ["ax/class", "ax/EventDispatcher", "ax/util"], function (klass, EventDispatcher, util) {
    "use strict";
    var Model = klass.create(EventDispatcher, {
        /**
         * Default attribute value to start with for a new instance
         * @public
         * @static
         * @memberof ax/af/mvc/Model#
         */
        defaults: {}
    }, {
        /**
         * Internal attribute storage
         * @protected
         * @memberof ax/af/mvc/Model#
         */
        _attrs: {},
        /**
         * Internal changed attributes marker
         * @protected
         * @memberof ax/af/mvc/Model#
         */
        _changed: {},
        /**
         * overrides init function
         * @param {type} attrs attributes to assign into a model instance
         * @param {type} opts options
         * @protected
         * @method
         * @memberof ax/af/mvc/Model#
         */
        /*jshint unused:false*/
        init: function (attrs, opts) {
            this._attrs = util.clone(this.constructor.defaults, true);
            util.extend(this._attrs, attrs);
            this._super();
        },
        /*jshint unused:true*/
        /**
         * Get all the changed attributes since this model has been created.
         * @return {Object} the changed attributes with their values
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        changedAttrs: function () {
            var res = {},
                self = this;
            util.each(this._changed, function (pair) {
                res[pair.key] = self._attrs[pair.key];
            });
            return res;
        },
        /**
         * Get a attribute for this model.
         * @param {String} attr the attribute name
         * @return {Mixed} the attribute value
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        get: function (attr) {
            return this._attrs[attr];
        },
        /**
         * Set one or multiple attributes for this model.
         * @param {String|Object} attr the attribute name,
         *     or the multiple attributes stored in an object
         * @param {Mixed} [value] the attribute value to set to
         * @return {ax/af/mvc/model} the current model for chaining
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        set: function (attr, value) {
            var self = this;
            if (util.isPlainObject(attr)) {
                util.each(attr, function (pair) {
                    self.set(pair.key, pair.value);
                });
                return this;
            }

            this.__updateAttr(attr, value);
            return this;
        },
        /**
         * Unset one an attribute for this model.
         * @param {String} attr the attribute name
         * @return {ax/af/mvc/model} the current model for chaining
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        unset: function (attr) {
            if (!this.has(attr)) {
                return this;
            }
            this.set(attr); // set value to undefined
            return this;
        },
        /**
         * Check if an attribute exists for this model.
         * @param {String} attr the attribute name
         * @param {Mixed} [value] the attribute value to set to
         * @return {Boolean} if the attribute value exist
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        has: function (attr) {
            return this._attrs.hasOwnProperty(attr);
        },
        /**
         * Clears all attributes exists for this model.
         * @return ax/af/mvc/model} the current model for chaining
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        clear: function () {
            var self = this;
            util.each(this._attrs, function (pair) {
                self.unset(pair.key, pair.value);
            });
            return this;
        },
        /**
         * Get the internal attributes storate from this model.
         * Please do not modify the return of this function, you can use set() to do so.
         * @return {Object} the attributes of this model
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        attributes: function () {
            return this._attrs;
        },
        /**
         * Alias to {@link ax/EventDispatcher#addEventListener}.
         * Refer to class description for details.
         * @param {String} evtType event type
         * @param {Function} handler event listener function
         * @returns {ax/af/mvc/model} current model, for easier chaining
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        on: function (evtType, handler) {
            this.addEventListener(evtType, handler, false);
        },
        /**
         * Alias to {@link ax/EventDispatcher#addEventListener}, with once set to true.
         * Refer to class description for details.
         * @param {String} evtType event type
         * @param {Function} handler event listener function
         * @returns {ax/af/mvc/model} current model, for easier chaining
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        once: function (evtType, handler) {
            this.addEventListener(evtType, handler, true);
        },
        /**
         * Alias to {@link ax/EventDispatcher#removeEventListener}, with once set to true
         * @param {String} evtType Event type
         * @param {Function} [handler] event listener function.  If not provided, all listeners will be removed for the specific type.
         * @returns {ax/af/mvc/model} current model, for easier chaining
         * @public
         * @method
         * @memberof ax/af/mvc/Model#
         */
        off: function (evtType, handler) {
            this.removeEventListener(evtType, handler);
        },
        /**
         * Internal method to update an attribute
         * @param {String} attr the attribute name
         * @param {Mixed} value the attribute value to set to
         * @private
         * @method
         * @memberof ax/af/mvc/Model#
         */
        __updateAttr: function (attr, value) {
            var oldValue = this._attrs[attr],
                evtType, evtData;
            if (value === oldValue) {
                return;
            }

            if (util.isUndefined(value)) {
                delete this._attrs[attr];
            } else {
                this._attrs[attr] = value;
            }


            this._changed[attr] = true;
            evtType = "change:" + attr;
            evtData = {
                attr: attr,
                type: "change",
                oldVal: oldValue,
                newVal: value
            };

            this.dispatchEvent(evtType, evtData);
            this.dispatchEvent("change", evtData);
        }
    });

    return Model;
});
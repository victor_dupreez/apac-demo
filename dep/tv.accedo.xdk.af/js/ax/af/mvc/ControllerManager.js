/**
 * Controller Manager centralizes the manipulation of controllers: open, replace and close controller(s).
 *
 * This class is designed as a singleton, developer should use {@link ax/af/mvc/ControllerManager.singleton|singleton} to obtain the instance.
 * Creating instance using the _new_ keyword is prohibited.
 *
 * @class ax/af/mvc/ControllerManager
 * @author Andy Hui <andy.hui@accedo.tv>
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/mvc/ControllerManager", [
    "ax/core",
    "ax/class",
    "ax/EventDispatcher",
    "ax/af/mvc/Controller",
    "ax/promise",
    "ax/util",
    "ax/exception",
    "require"
], function (core,
    klass,
    EventDispatcher,
    Controller,
    promise,
    util,
    exception,
    require) {
    "use strict";
    var ControllerManager,
        instance;

    ControllerManager = klass.create(EventDispatcher, {
        /**
         * Event fired when replace controller complete
         * @event
         * @type {Object}
         * @property {ax/af/mvc/Controller} exist The original controller instance that is being replaced
         * @property {ax/af/mvc/Controller} replacement The new controller instance that replaces with
         * @property {Object} [options] Option value which will pass from the replace function
         * @memberof ax/af/mvc/ControllerManager#
         */
        EVT_REPLACED: "ui:ctrlMgr:replaced",
        /**
         * Event fired when open controller complete
         * @event
         * @type {Object}
         * @property {ax/af/mvc/Controller} exist The controller instance that is being opened
         * @property {ax/af/Container} container The parent container of the controller where it is attached
         * @property {Object} [options] Option value which will pass from the open function
         * @memberof ax/af/mvc/ControllerManager#
         */
        EVT_OPENED: "ui:ctrlMgr:opened",
        /**
         * Event fired when close controller complete
         * @event
         * @type {Object}
         * @property {ax/af/mvc/Controller} controller The original controller instance that is being closed
         * @property {ax/af/Container} container The parent container of the controller where it is detached from
         * @property {Object} [options] Option value which will pass from the close function
         * @memberof ax/af/mvc/ControllerManager#
         */
        EVT_CLOSED: "ui:ctrlMgr:closed",

        /**
         * Get the singleton instance of this class.
         * @method
         * @static
         * @returns {ax/af/mvc/ControllerManager} The singleton
         * @memberOf ax/af/mvc/ControllerManager
         */
        singleton: function () {
            if (!instance) {
                instance = new ControllerManager();
            }

            return instance;
        }
    }, {
        /**
         * Replace a controller with a new one.
         * @param {ax/af/mvc/Controller} exist The existing controller instance or class.
         * @param {ax/af/mvc/Controller} replacement The replacement controller class or instance.
         * @param {Object} [options] Options for this operation
         * @param {Object} [options.context] Context object to be passed to the new controller
         * @return {Promise.<ax/af/mvc/Controller>} the replacement controller instance
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} When the existing controller (exist) is not a Controller instance or class or replacement controller (replacement) is not a Controller instance or class
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} When the existing controller or replacement controller don't have a view
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} When the existing controller is not attached to parent
         * @memberof ax/af/mvc/ControllerManager#
         * @fires ax/af/mvc/ControllerManager.EVT_REPLACED
         * @public
         */
        replace: function (exist, replacement, options) {
            var sAppRoot, existView, replaceView, existParent, existViewParent, context;

            sAppRoot = require("ax/af/mvc/AppRoot").singleton();

            if (!(exist instanceof Controller)) {
                if (exist.prototype instanceof Controller) {
                    exist = sAppRoot.getControllerByClass(exist);
                } else {
                    return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Fail to get the current controller instance."));
                }
            }

            if (!(replacement instanceof Controller)) {
                if (replacement.prototype instanceof Controller) {
                    /*jshint newcap: false */
                    replacement = new replacement();
                    /*jshint newcap: true*/
                } else {
                    return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Replacement controller is not a Controller class!"));
                }
            }

            existView = exist.getView();
            replaceView = replacement.getView();
            existParent = exist.getParentController();

            if (!existView || !replaceView) {
                return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "One of the controllers does not have a view!"));
            }

            existViewParent = existView.getParent();
            if (!existViewParent) {
                return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Controller to be replaced is not attached to a parent!"));
            }

            options = options || {};
            context = options.context || {};

            // extract controller data from commit function
            existViewParent.replace(existView, replaceView);

            if (existParent) {
                existParent._removeSubController(exist);
                existParent._addSubController(replacement);
            }

            //reset the last controller conext data
            exist._resetSubtree();

            //setup the controller with context data
            return replacement._setupSubtree(context).then(util.bind(function () {

                this.dispatchEvent(this.constructor.EVT_REPLACED, {
                    exist: exist,
                    replacement: replacement,
                    options: options
                });

                return replacement;
            }, this));

        },
        /**
         * Open a controller in a container.
         * @param {ax/af/mvc/Controller} controller The controller instance or class to open.
         * @param {ax/af/Container|ax/af/mvc/Controller} container Container or controller
         *     under which the controller should be opened under.
         * @param {Object} [context] Context object to be passed to the new controller
         * @param {Object} [options] Options for this operation
         * @param {Object} [options.context] Options for this operation
         * @return {Promise.<ax/af/mvc/Controller>} the opened controller instance
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} when fail to get the container
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} when fail to create the new controller
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} when the target new controller has no view
         * @memberof ax/af/mvc/ControllerManager#
         * @fires ax/af/mvc/ControllerManager.EVT_OPENED
         * @public
         */
        open: function (controller, container, options) {
            var context, view, parent;
            options = options || {};
            context = options.context || {};


            if (container instanceof Controller) {
                container = container.getView();
            }

            if (!container) {
                return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Fail to find the container to open a new controller."));
            }


            if (!(controller instanceof Controller)) {
                if (controller.prototype instanceof Controller) {
                    /*jshint newcap: false */
                    controller = new controller();
                    /*jshint newcap: true */
                } else {
                    return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Fail to get the controller instance when opening controller."));
                }
            }

            view = controller.getView();
            parent = container.getController();

            if (!view) {
                return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Fail to get the view from the controller instance."));
            }

            container.attach(view);

            if (parent) {
                parent._addSubController(controller);
            }

            //setup the controller with context data
            return controller._setupSubtree(context).then(util.bind(function () {
                this.dispatchEvent(this.constructor.EVT_OPENED, {
                    controller: controller,
                    container: container,
                    options: options
                });
                return controller;
            }, this));
        },
        /**
         * Close a controller.
         * @param {ax/af/mvc/Controller} controller The controller instance or class to close.
         * @param {Object} [options] Options for this operation
         * @return {Promise.<Undefined>}
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} the controller does not exist
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} when the controller has no view
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} when the controller isn't attached to parent
         * @memberof ax/af/mvc/ControllerManager#
         * @fires ax/af/mvc/ControllerManager.EVT_CLOSED
         * @public
         */
        close: function (controller, options) {
            var sAppRoot, view, parent, viewParent;
            options = options || {};
            sAppRoot = require("ax/af/mvc/AppRoot").singleton();

            if (!(controller instanceof Controller)) {
                if (controller.prototype instanceof Controller) {
                    controller = sAppRoot.getControllerByClass(controller);
                } else {
                    return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Fail to get the controller instance when closing controller."));
                }
            }

            view = controller.getView();
            parent = controller.getParentController();

            if (!view) {
                return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "Fail to get the view from the controller instance."));
            }

            viewParent = view.getParent();
            if (!viewParent) {
                return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "The controller instance to be closed is not attached to a parent!"));
            }

            if (parent) {
                parent._removeSubController(controller);
            }

            // detach the view
            view.detach();
            // reset the controller
            controller._resetSubtree();


            this.dispatchEvent(this.constructor.EVT_CLOSED, {
                controller: controller,
                container: viewParent,
                options: options
            });

            return promise.resolve();
        }

    });

    // enforce the instance creation before returning, preventing 2 instances being created
    // (2 executions may go into the creation at the same time...)
    ControllerManager.singleton();

    return ControllerManager;

});
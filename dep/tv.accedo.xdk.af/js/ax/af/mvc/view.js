/**
 * Renders view templates. Views are built with trees of {@link ax/af/Component|Component}.
 * @module ax/af/mvc/view
 */
define("ax/af/mvc/view", ["ax/core", "ax/af/Component", "ax/util"], function (core, Component, util) {
    "use strict";
    return {
        _templateRenderHooks: {},
        /**
         * Hook template rendering by recognizing a **key** in the template object.
         * The hook handler function will be called if the **key** exists.
         * @name hookTemplateRenderer
         * @param {String} hook The object key to hook to
         * @param {Function} handler Handler function to be called on hook. Expected to
         * return render view (i.e. Component). Template object will be passed as
         * the first argument.
         * @example
         *      view.hookTemplateRenderer("controller", function(viewTemplate){
         *          ...
         *          return component;
         *      })
         * @method
         * @memberof module:ax/af/mvc/view
         * @public
         */
        hookTemplateRenderer: function (hook, handler) {
            if (!util.isString(hook)) {
                throw core.createException("IncorrectParam", "Template hook attribute (hookAttr) should be a string : " + hook);
            }

            if (!util.isFunction(handler)) {
                throw core.createException("IncorrectParam", "Template hook helper (helperFn) should be a function : " + handler);
            }

            if (this._templateRenderHooks[hook]) {
                throw core.createException("TemplateRenderHelperTaken", "Template helper for hook (" + hook + ") is already taken.");
            }

            this._templateRenderHooks[hook] = handler;
        },
        /**
         *
         * Renders a view template.
         * Note: The root of view template must be a component, while its children can contain controllers.
         * @param {Object|Function} template View template object or a function that produces the template
         * @param {Object} modelObj the object containing models and information needed for the template
         * @return {ax/af/Component} the reference to the root of the component tree i.e. view
         * @method
         * @memberof module:ax/af/mvc/view
         * @public
         * @example
         * var template = {
         *   klass: Container,
         *   id: "#my-container",
         *   children: [
         *     {
         *       klass: ButtonWidget,
         *       css: "my-button-class"
         *     },
         *     {
         *       controller: MyController
         *     }
         *   ]
         * },
         * myView = view.render(template);
         */
        render: function (template, modelObj) {

            // view is a function that takes models
            if (util.isFunction(template)) {
                template = template(modelObj);
            }

            if (!util.isPlainObject(template)) {
                throw core.createException("ViewTemplateInvalid", "View template is incorrect: " + util.stringify(template));

            }

            if (!util.isFunction(template.klass)) {
                throw core.createException("ViewTemplateInvalid", "Trying to create an unknown type. Template: " + util.stringify(template));

            }

            if (!(template.klass.prototype instanceof Component)) {
                throw core.createException("ViewTemplateInvalid", "View template root must be a component! Template:" + util.stringify(template));

            }

            var view, opts, i, len, childTemp, childComp;

            opts = util.clone(template);
            delete opts.klass;
            delete opts.children;

            // create the root component first, in case need to open controller under it
            view = new template.klass(opts);

            // renders any children that need rendering
            if (util.isArray(template.children) && template.children.length) {
                len = template.children.length;
                for (i = 0; i < len; i++) {
                    childComp = null;
                    childTemp = template.children[i];

                    if (!util.isPlainObject(childTemp)) {
                        // incorrect child template
                        throw core.createException("ViewTemplateInvalid", "Sub-view template rendering has failed for: " + template.children[i]);

                    }

                    util.each(this._templateRenderHooks, function (pair) {

                        if (util.isUndefined(childTemp[pair.key])) {
                            return;
                        }

                        //if helper function exists, delegate template creation to helper
                        childComp = this._templateRenderHooks[pair.key](childTemp);

                        if (!(childComp instanceof Component)) {
                            throw core.createException("UnexpectedViewTemplateHelperResponse", "View template helper did not return a Component: " + childComp);
                        }

                        view.attach(childComp);

                        return util.breaker;
                    }, this);

                    if (childComp) {
                        // hooked
                        continue;
                    }

                    if (childTemp.klass.prototype instanceof Component) {
                        // renders the child if it is a template
                        childComp = this.render(childTemp, true);
                        view.attach(childComp);
                        continue;
                    }

                    // incorrect child template
                    throw core.createException("ViewTemplateInvalid", "Sub-view template contains unknown class! Sub-view template: " + util.stringify(childTemp));
                }
            }

            return view;
        }
    };
});
/**
 * The event type constants.
 * @module ax/af/evt/type
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/evt/type", {
    /**
     * Dispatched when component shown
     * @event
     * @type {Object}
     * @property {Boolean} hidden True if component is hidden.
     * @memberof module:ax/af/evt/type
     */
    SHOWN: "ui:evt:shown",
    /**
     * Dispatched when component hidden
     * @event
     * @type {Object}
     * @property {Boolean} hidden True if component is hidden.
     * @memberof module:ax/af/evt/type
     */
    HIDDEN: "ui:evt:hidden",
    /**
     * Dispatched when component detach
     * @event
     * @memberof module:ax/af/evt/type
     */
    DETACH: "ui:evt:detach",
    /**
     * Dispatched when component detached from DOM
     * @event
     * @memberof module:ax/af/evt/type
     */
    DETACHED_FROM_DOM: "ui:evt:detachDOM",
    /**
     * Dispatched when component attach
     * @event
     * @memberof module:ax/af/evt/type
     */
    ATTACH: "ui:evt:attach",
    /**
     * Dispatched when component attacted to DOM
     * @event
     * @memberof module:ax/af/evt/type
     */
    ATTACHED_TO_DOM: "ui:evt:attachDOM",
    /**
     * Dispatched when component attached to Controller
     * @event
     * @memberof module:ax/af/evt/type
     */
    ATTACHED_TO_CONTROLLER: "ui:evt:attach-controller",
    /**
     * Dispatched when component detached from Controller
     * @event
     * @memberof module:ax/af/evt/type
     */
    DETACHED_FROM_CONTROLLER: "ui:evt:detach-controller",
    /**
     * Dispatched when component focus
     * @event
     * @type {object}
     * @property {ax/af/Component} relatedTarget The component who just lost focus due to this event.
     * @memberof module:ax/af/evt/type
     */
    FOCUS: "ui:evt:focus",
    /**
     * Dispatched when component blur
     * @event
     * @type {object}
     * @property {ax/af/Component} relatedTarget The component who just gain focus due to this event.
     * @memberof module:ax/af/evt/type
     */
    BLUR: "ui:evt:blur",
    /**
     * Dispatched when component receive key
     * @event
     * @type {object}
     * @property {String} source from software or hardware keyboard
     * @property {String} id key ID
     * @property {String} text key text, if any
     * @memberof module:ax/af/evt/type
     */
    KEY: "ui:evt:key",
    /**
     * Dispatched when widget open
     * @event
     * @memberof module:ax/af/evt/type
     */
    OPEN: "ui:evt:open",
    /**
     * Dispatched when widget close
     * @event
     * @memberof module:ax/af/evt/type
     */
    CLOSE: "ui:evt:close",
    /**
     * Dispatched when component receive click
     * @event
     * @memberof module:ax/af/evt/type
     */
    CLICK: "ui:evt:click",
    /**
     * Dispatched when component receive mouseover
     * @event
     * @memberof module:ax/af/evt/type
     */
    MOUSEOVER: "ui:evt:mouseover",
    /**
     * Dispatched when widget cancel
     * @event
     * @memberof module:ax/af/evt/type
     */
    CANCEL: "ui:evt:cancel",
    /**
     * Dispatched when widget confirm
     * @event
     * @memberof module:ax/af/evt/type
     */
    CONFIRM: "ui:evt:confirm",
    /**
     * Dispatched when widget scroll
     * @event
     * @memberof module:ax/af/evt/type
     */
    SCROLL: "ui:evt:scroll",
    /**
     * Dispatched when widget selection changed
     * @event
     * @memberof module:ax/af/evt/type
     */
    SELECTION_CHANGED: "ui:evt:selection-changed",
    /**
     * Dispatched when widget text changed
     * @event
     * @memberof module:ax/af/evt/type
     */
    TEXT_CHANGED: "ui:evt:text-changed",
    /**
     * Dispatched when value changed
     * @event
     * @memberof module:ax/af/evt/type
     */
    VALUE_CHANGED: "ui:evt:value-changed",
    /**
     * Dispatched when widget moved
     * @event
     * @memberof module:ax/af/evt/type
     */
    MOVED: "ui:evt:moved",
    /**
     * Dispatched when scrollable label scroll to the top ,previously was SCROLL_TO_TOP
     * @event
     * @memberof module:ax/af/evt/type
     */
    SCROLLED_TO_TOP: "ui:evt:scroll-to-top",
    /**
     * Dispatched when scrollable label scroll to the bottom,previously was SCROLL_TO_BOTTOM
     * @event
     * @memberof module:ax/af/evt/type
     */
    SCROLLED_TO_BOTTOM: "ui:evt:scroll-to-bottom",
    /**
     * Dispatched when a component becomes active
     * @event
     * @memberof module:ax/af/evt/type
     */
    ACTIVED: "ui:evt:actived",
    /**
     * Dispatched: when a component becomes inactive
     * @event
     * @memberof module:ax/af/evt/type
     */
    INACTIVED: "ui:evt:inactived",
    /**
     * Dispatched when a draggable object is moving
     * @event
     * @memberof module:ax/af/evt/type
     */
    DRAG_MOVING: "ui:evt:drag-moving",
    /**
     * Dispatched when a draggable object is released
     * @event
     * @memberof module:ax/af/evt/type
     */
    DRAG_RELEASE: "ui:evt:drag-release",
    /**
     * Dispatched when a indicator is updated and send signal to the mediator and then to the scrollable
     * @deprecated since the indicator now only receive event from scrollable, it won't dispatch update event to the scrollable.
     * @event
     * @memberof module:ax/af/evt/type
     */
    INDICATOR_UPDATED: "ui:evt:indicator-updated",
    /**
     * Dispatched when a scrollable object is scrolled by indicator info (start, length, total), it will send signal to the mediator and then to the indicator
     * @event
     * @memberof module:ax/af/evt/type
     */
    SCROLLED_INDICATOR: "ui:evt:scrolled-indicator",
    /**
     * Dispatched when a scrollable object is scrolled by percentage, it will send signal to the mediator and then to the indicator
     * @event
     * @memberof module:ax/af/evt/type
     */
    SCROLLED_PERCENTAGE: "ui:evt:scrolled-percentage",
    /**
     * DEPRECATED. Dispatched when the tv channel changed
     * @deprecated
     * @event
     * @memberof module:ax/af/evt/type
     */
    CHANNEL_CHANGED: "device:evt:channel-changed",
    /**
     * Dispatched when animated label iteration end
     * @event
     * @memberof module:ax/af/evt/type
     */
    ANIM_ITERATION_ENDED: "ui:evt:anim-iteration-ended"
});

/**
 * An event dispatcher, that can dispatch events in capturing and bubbling phases.
 * @extends ax/EventDispatcher
 * @class ax/af/evt/DualEventDispatcher
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/evt/DualEventDispatcher", ["ax/class", "ax/EventDispatcher"], function (klass, EventDispatcher) {
    "use strict";
    return klass.create(EventDispatcher, {}, {
        /**
         * Event callbacks for capturing phase
         * @memberof ax/af/evt/DualEventDispatcher#
         * @private
         */
        __capturingCallbacks: {},
        /**
         * Event callbacks for bubbling phase
         * @memberof ax/af/evt/DualEventDispatcher#
         * @private
         */
        __bubblingCallbacks: {},
        /**
         * Switch between capturing and bubbling mode.
         * @method __switchMode
         * @param {Booleab} isCapture is for capturing phase
         * @public
         * @memberof ax/af/evt/DualEventDispatcher#
         */
        __switchMode: function (isCapture) {
            if (isCapture) {
                this.__eventCallbacks = this.__capturingCallbacks;
            } else {
                this.__eventCallbacks = this.__bubblingCallbacks;
            }
        },
        /**
         * Dispatches an event with additional data.
         * @method dispatchEvent
         * @param {ax/af/evt|String} type event type to dispatch
         * @param {Object} data the event data object
         * @param {Boolean} isCapture whether is capturing phase currently
         * @public
         * @memberof ax/af/evt/DualEventDispatcher#
         */
        dispatchEvent: function (type, data, isCapture) {
            isCapture = !! isCapture;
            this.__switchMode(isCapture);
            return this._super(type, data);
        },
        /**
         * Registers an event listener for a certain type.
         * @method addEventListener
         * @param {ax/af/evt|String} type event type
         * @param {Function} handler event listener function
         * @param {Boolean} [useCapture] whether listener is registered to capturing phase, false by default
         * @param {Boolean} [once] set to true if listener will be called only once
         * @returns {ax/af/evt/DualEventDispatcher} current event dispatcher, for easier chaining
         * @public
         * @memberof ax/af/evt/DualEventDispatcher#
         */
        addEventListener: function (type, handler, useCapture, once) {
            useCapture = !! useCapture;
            this.__switchMode(useCapture);
            return this._super(type, handler, once);
        },
        /**
         * Removes one or more event listeners.
         * @method removeEventListener
         * @param {module:ax/af/evt/type|String} type Event type
         * @param {Function} [handler] event listener function.  If not provided, all listeners will be removed for the specific type.
         * @param {Boolean} [useCapture] whether listener is registered to capturing phase. If not provided, it **defaults to false.**
         * @public
         * @memberof ax/af/evt/DualEventDispatcher#
         */
        removeEventListener: function (type, handler, useCapture) {
            useCapture = !! useCapture;
            this.__switchMode(useCapture);
            return this._super(type, handler);
        },
        /**
         * Removes all event listeners.
         * @method removeAllListeners
         * @public
         * @memberof ax/af/evt/DualEventDispatcher#
         */
        removeAllListeners: function () {
            this.__switchMode(false);
            this._super();
            this.__switchMode(true);
            this._super();
            return this;
        }
    });
});
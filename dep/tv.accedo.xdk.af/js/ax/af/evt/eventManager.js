/**
 * Triggers events on a tree of {@link ax/af/Component|Component}, and make sure it propagates on that tree.
 * @module ax/af/evt/eventManager
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/evt/eventManager", ["ax/af/evt/Event", "ax/af/evt/type", "ax/af/it/BubblingCompIt", "ax/af/it/CapturingCompIt", "ax/af/it/DFSCompIt"], function (Event, evtType, BubblingCompIt, CapturingCompIt, DFSCompIt) {
    "use strict";
    var evtMgrModule, itConfig = {};

    // iterator config for special events
    /**
     * @TODO include other event types
     */

    itConfig[evtType.ATTACH] = {
        bubblingIt: DFSCompIt
    };
    itConfig[evtType.ATTACHED_TO_DOM] = {
        bubblingIt: DFSCompIt
    };
    itConfig[evtType.DETACH] = {
        bubblingIt: DFSCompIt
    };
    itConfig[evtType.DETACHED_FROM_DOM] = {
        bubblingIt: DFSCompIt
    };

    evtMgrModule = {
        /**
         * Triggers an event on a specified target.
         * @method
         * @param {module:ax/af/evt/type} eventType event type
         * @param {ax/af/Component} target the target component
         * @param {Object} [data] a plain ojbect data to be carried along with event object when dispatching
         * @param {Object} [data.defaultAction] the default action to be carried out when event finished propagation
         * @public
         * @memberof module:ax/af/evt/eventManager
         */
        trigger: function (eventType, target, data) {
            data = data || {};

            var capturingIt, bubblingIt, evt, curComp;

            // check if has special iterator config
            if (itConfig[eventType]) {
                capturingIt = itConfig[eventType].capturingIt;
                bubblingIt = itConfig[eventType].bubblingIt;
                /* jshint newcap:false*/
                capturingIt = capturingIt ? new capturingIt(target) : null;
                bubblingIt = bubblingIt ? new bubblingIt(target) : null;
                /* jshint newcap:true*/
            } else { // use default iterators
                capturingIt = new CapturingCompIt(target);
                bubblingIt = new BubblingCompIt(target);
            }

            evt = new Event(eventType, target, data, data.defaultAction);

            // event capturing phase
            if (capturingIt) {
                while (!evt.isPropagationStopped() && capturingIt.hasNext()) {
                    curComp = capturingIt.next();
                    if (false === curComp.dispatchEvent(eventType, evt, true)) {
                        evt.stopPropagation();
                        evt.preventDefault();
                    }
                }
            }

            // event bubbling phase
            if (bubblingIt) {
                while (!evt.isPropagationStopped() && bubblingIt.hasNext()) {
                    curComp = bubblingIt.next();
                    if (false === curComp.dispatchEvent(eventType, evt, false)) {
                        evt.stopPropagation();
                        evt.preventDefault();
                    }
                }
            }

            if (!evt.isDefaultPrevented()) {
                evt.performDefaultAction();
            }
        }
    };

    return evtMgrModule;
});
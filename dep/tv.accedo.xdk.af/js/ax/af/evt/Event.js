/**
 * The event object, holding information of an event.
 * @class ax/af/evt/Event
 * @param {module:ax/af/evt/type} type event type
 * @param {ax/af/Component} target the target component
 * @param {Object} [data] a plain ojbect data to be carried along with event object when dispatching
 * @param {Function} defaultAction the default action to be carried out, if no event listener raises objection. Takes current event object as parameter.
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/evt/Event", ["ax/class", "ax/util", "ax/console"], function (klass, util, console) {
    "use strict";
    return klass.create({}, {
        _isPropagationStopped: false,
        _isDefaultPrevented: false,
        _defaultAction: null,
        target: null,
        type: "",
        init: function (type, target, data, defaultAction) {
            var i;
            this.type = type;
            this.target = target;
            this._defaultAction = defaultAction;

            // inject data stuff into current event object
            // e.g. keyCode
            for (i in data) {
                if (!util.isUndefined(this[i])) {
                    console.warn("Data parameter have collision with some event class members! They will be ignored!");
                }
                this[i] = data[i];
            }
        },
        /**
         * Sets and overrides the default action.
         * @method
         * @param {Function} defaultAction the default action function
         * @protected
         * @memberof ax/af/evt/eventManager#
         */
        _setDefaultAction: function (defaultAction) {
            this._defaultAction = defaultAction;
        },
        /**
         * Prevents default action to be carried out.
         * @method
         * @public
         * @memberof ax/af/evt/eventManager#
         */
        preventDefault: function () {
            this._isDefaultPrevented = true;
        },
        /**
         * If default action is prevented to be carried out.
         * @method
         * @return {Boolean} If default action is prevented to be carried out.
         * @public
         * @memberof ax/af/evt/eventManager#
         */
        isDefaultPrevented: function () {
            return this._isDefaultPrevented;
        },
        /**
         * Prevents the current event from futher propagating.
         * @method
         * @public
         * @memberof ax/af/evt/eventManager#
         */
        stopPropagation: function () {
            this._isPropagationStopped = true;
        },
        /**
         * If the current event is prevented from futher propagating.
         * @method
         * @return {Boolean} If the current event is prevented from futher propagating.
         * @public
         * @memberof ax/af/evt/eventManager#
         */
        isPropagationStopped: function () {
            return this._isPropagationStopped;
        },
        /**
         * Performs the default action. Will be call upon propagation complete and no event listener has objection.
         * @method
         * @public
         * @memberof ax/af/evt/eventManager#
         */
        performDefaultAction: function () {
            if (this._defaultAction) {
                this._defaultAction(this);
            }
        }
    });

});
/**
 * depth first search iterator.
 * @class ax/af/it/DFSCompIt
 * @extends ax/af/it/interface/Iterator
 * @param {ax/af/Component} targetComp capturing target component
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/it/DFSCompIt", ["ax/class", "ax/af/it/interface/Iterator", "ax/core"], function (klass, IIterator, core) {
    "use strict";
    return klass.create([IIterator], {}, {
        /**
         * internal component array
         * @private
         */
        __compArr: [],
        init: function (targetComp) {
            this._include(targetComp);
        },
        _include: function (comp) {
            if (comp.isLeaf()) {
                this.__compArr.unshift(comp);
                return;
            }
            var i, children = comp.getChildren();
            for (i in children) {
                this._include(children[i]);
            }

            this.__compArr.unshift(comp);
        },
        next: function () {
            if (!this.__compArr.length) {
                core.createException("NoSuchElement", "No more element is available for current iteraion!");
            }
            return this.__compArr.pop();
        },
        hasNext: function () {
            return !!this.__compArr.length;
        }
    });
});
/**
 * Interface for iterators.
 * @class ax/af/it/IIterator
 * @deprecated ax/af/it/interface/Iterator replace with this abstract interface
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/it/IIterator", ["ax/class"], function (klass) {
    "use strict";
    return klass.createAbstract({}, {
        /**
         * Returns the next element in the iteration.
         * @method
         * @return {Mixed} the next element, throws exception if iteration has no more elements.
         * @memberof ax/af/it/IIterator#
         * @public
         */
        next: klass.abstractFn,
        /**
         * Returns true if the iteration has more elements.
         * @method
         * @return {Boolean} true if the iterator has more elements.
         * @memberof ax/af/it/IIterator#
         * @public
         */
        hasNext: klass.abstractFn
    });
});
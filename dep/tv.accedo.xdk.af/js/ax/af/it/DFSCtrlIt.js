/**
 * @author Andy Hui <andy.hui@accedo.tv>
 * @ignore
 */
define("ax/af/it/DFSCtrlIt", ["ax/class", "ax/af/it/interface/Iterator", "ax/core"], function (klass, IIterator, core) {
    "use strict";
    /**
     * @class ax/af/it/DFSCtrlIt
     * @extends ax/af/it/interface/Iterator
     * @classdesc Depth first search controller iterator.
     */

    return klass.create([IIterator], {}, {
        /**
         * internal component array
         * @private
         */
        __ctrlArr: [],
        /**
         * @name init
         * @param {ax/af/mvc/Controller} targetCtrl capturing target controller
         * @method
         * @memberOf ax/af/iu/DFSCtrlIt#
         */
        init: function (targetCtrl) {
            this._include(targetCtrl);
        },
        _include: function (ctrl) {

            var i, children = ctrl.getSubControllers();

            this.__ctrlArr.unshift(ctrl);

            for (i in children) {
                this._include(children[i]);
            }

        },
        next: function () {
            if (!this.__ctrlArr.length) {
                core.createException("NoSuchElement", "No more element is available for current iteraion!");
            }
            return this.__ctrlArr.pop();
        },
        hasNext: function () {
            return !!this.__ctrlArr.length;
        }
    });
});
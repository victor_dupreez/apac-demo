/**
 * {@link ax/af/Component|Component} bubbling iterator.
 * @class ax/af/it/BubblingCompIt
 * @extends ax/af/it/interface/Iterator
 * @param {ax/af/Component} targetComp bubbling starting component
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/it/BubblingCompIt", [
    "ax/class",
    "ax/af/it/interface/Iterator",
    "ax/core"
], function (
    klass,
    IIterator,
    core
) {
    "use strict";

    return klass.create([IIterator], {}, {
        /**
         * current component reference
         * @private
         */
        __curComp: null,
        init: function (targetComp) {
            this.__curComp = targetComp;
        },
        next: function () {
            if (!this.__curComp) {
                core.createException("NoSuchElement", "No more element is available for current iteraion!");
            }
            var result = this.__curComp;
            this.__curComp = this.__curComp.getParent();
            return result;
        },
        hasNext: function () {
            return !!this.__curComp;
        }
    });
});
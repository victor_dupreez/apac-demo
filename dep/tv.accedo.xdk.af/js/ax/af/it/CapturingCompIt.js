/**
 * {@link ax/af/Component|Component} capturing iterator.
 * @class ax/af/it/CapturingCompIt
 * @extends ax/af/it/interface/Iterator
 * @param {ax/af/Component} targetComp capturing target component
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/af/it/CapturingCompIt", [
    "ax/class",
    "ax/af/it/interface/Iterator",
    "ax/core"
], function (
    klass,
    IIterator,
    core
) {
    "use strict";

    return klass.create([IIterator], {}, {
        /**
         * internal component array
         * @private
         */
        __compArr: [],
        init: function (targetComp) {
            var curComp = targetComp;
            while (curComp) {
                this.__compArr.push(curComp);
                curComp = curComp.getParent();
            }
        },
        next: function () {
            if (!this.__compArr.length) {
                core.createException("NoSuchElement", "No more element is available for current iteraion!");
            }
            return this.__compArr.pop();
        },
        hasNext: function () {
            return !!this.__compArr.length;
        }
    });
});
/**
 * Active first search iterator.
 * @class ax/af/it/AFSCompIt
 * @extends ax/af/it/interface/Iterator
 * @param {ax/af/Component} targetComp capturing target component
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/af/it/AFSCompIt", ["ax/class", "ax/af/it/interface/Iterator", "ax/core"], function (klass, IIterator, core) {
    "use strict";
    return klass.create([IIterator],{}, {
        /**
         * internal component array
         * @private
         **/
        __compArr: [],
        init: function (targetComp) {

            this._include(targetComp);

        },
        _include: function (comp) {

            if (comp.isLeaf()) {
                this.__compArr.unshift(comp);
                return;
            }

            var i, children = comp.getActiveChildren();
            for (i = 0; i < children.length; i++) {
                this._include(children[i]);
            }

            this.__compArr.unshift(comp);
        },
        next: function () {
            if (!this.__compArr.length) {
                core.createException("NoSuchElement", "No more element is available for current iteraion!");
            }
            return this.__compArr.pop();
        },
        hasNext: function () {
            return !!this.__compArr.length;
        }
    });
});
/**
 * {@link ax/af/Component|Component} Breath first search iterator.
 * @class ax/af/it/BFSCompIt
 * @extends ax/af/it/interface/Iterator
 * @param {ax/af/Component} targetComp capturing target component
 * @author Andy Hui <andy.hui@accedo.tv>
 */
define("ax/af/it/BFSCompIt", ["ax/class", "ax/af/it/interface/Iterator", "ax/core"], function (klass, IIterator, core) {
    "use strict";
    return klass.create([IIterator], {}, {
        /**
         * internal component array
         * @private
         */
        __compArr: [],
        init: function (targetComp) {
            this._include(targetComp);
        },
        _include: function (comp) {

            if (comp.isLeaf()) {
                return;
            }

            var i, children = comp.getChildren();

            for (i in children) {
                this.__compArr.unshift(children[i]);
            }

            for (i in children) {
                this._include(children[i]);
            }
        },
        next: function () {
            if (!this.__compArr.length) {
                core.createException("NoSuchElement", "No more element is available for current iteraion!");
            }
            return this.__compArr.pop();
        },
        hasNext: function () {
            return !!this.__compArr.length;
        }
    });
});
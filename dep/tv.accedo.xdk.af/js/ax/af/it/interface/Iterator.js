/**
 * Interface for iterators.
 * @class ax/af/it/interface/Iterator
 */
define("ax/af/it/interface/Iterator", ["ax/Interface"], function (Interface) {
    "use strict";
    return Interface.create("Iterator", {
        /**
         * Returns the next element in the iteration.
         * @method
         * @return {Mixed} the next element, throws exception if iteration has no more elements.
         * @memberof ax/af/it/interface/Iterator
         * @public
         */
        next: [],
        /**
         * Returns true if the iteration has more elements.
         * @method
         * @return {Boolean} true if the iterator has more elements.
         * @memberof ax/af/it/interface/Iterator
         * @public
         */
        hasNext: []
    });
});
/**
 * DevicePack class to handle the packaging for the Samsung
 * Samsung firmware year larger than 2011 will have mouse support
 * Storage will make use of file to store that data is only removed when delete the app.
 *
 * ###Support Models
 *
 * * 2010
 * * 2011
 * * 2012
 * * 2013
 * * 2014
 *
 * ###Media Player
 *
 * * {@link ax/ext/device/samsung/InfolinkPlayer} Infolink Player
 * Support format: MP4 ASF HLS HAS MP3 WIDEVINE WMDRM PLAYREADY
 * * {@link ax/ext/device/samsung/SefPlayer} Sefplayer (Supported in 2011+)
 * Support format: MP4 ASF HLS HAS MP3 WIDEVINE WMDRM PLAYREADY VERIMATRIX
 * Support multiple audio tracks
 * * {@link ax/ext/device/samsung/webAPI/AVPlayer} AVPlayer
 * Support format: MP4 ASF HLS HAS MP3 WIDEVINE WMDRM PLAYREADY (Supported in 2012+ and those device with webapis)
 * * {@link ax/device/shared/FlashPlayer} Flash player (Supported in 2012+)
 * Support format: RTMP (MP4 is removed from the supported list due to device limitation)
 * * {@link ax/device/shared/Html5Player} HTML5 player (Supported in 2012+)
 * Support format: MP4
 *
 *
 * If there is no setting available player in the config, the following default player will be applied.
 * Default: ax/ext/device/samsung/InfolinkPlayer
 *
 * ###Strategy
 * * {@link ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy} SefPlayerMultiAudioTrackStgy
 * Support only on the SEF Player. And it allows users to switch the audio channels of the videos.
 * * {@link ax/ext/device/samsung/webAPI/AVPlayerMultiAudioTracksStgy} AVPlayerMultiAudioTrackStgy
 * Support only on the AVPlayer. And it allows users to switch the audio channels of the videos.
 * ###Resources
 *
 * * <https://accedobroadband.jira.com/wiki/display/ATEC/Samsung+Smart+TV>
 * * <http://www.samsungdforum.com/>
 *
 * ###Remark
 * {@link ax/ext/device/samsung/globals} To get the global object of samsung like tvplugin, infolink player or system..etc.
 * _sswidget_ is a reserved module path for samsung plugins/widgets, please do not define any module under this path.
 *
 * ###Configuration Parameters
 *
 *  Attribute | Value
 * --------- | ---------
 * Key:    | samsung.initDelay
 * Desc    | Time (s) to delay the init in sec for some special handling like setBannerState
 * Type    | Number
 * Default  | 0.5
 * Usage | {@link module:ax/config}
 * @class ax/ext/device/samsung/DevicePackage
 * @extends ax/device/AbstractDevicePackage
 */
define("ax/ext/device/samsung/DevicePackage", [
    "ax/ext/device/samsung/loader/nativeAPI",
    "ax/device/AbstractDevicePackage",
    "ax/ext/device/samsung/FileStorage",
    "ax/ext/device/samsung/TvKey",
    "ax/ext/device/samsung/Id",
    "ax/ext/device/samsung/System",
    "ax/device/Media",
    "ax/class",
    "ax/console",
    "ax/util",
    "ax/config",
    "ax/ext/device/samsung/globals",
    "ax/ext/device/samsung/webAPI/Id",
    "ax/ext/device/samsung/webAPI/System",
    "require"
], function (
    nativeAPI,
    AbstractDevicePackage,
    FileStorage,
    TvKey,
    Id,
    System,
    Media,
    klass,
    console,
    util,
    config,
    globals,
    WebAPIId,
    WebAPISystem,
    require
) {
    "use strict";
    var sMedia = Media.singleton(),
        id,
        system;

    return klass.create(AbstractDevicePackage, {}, {
        /**
         * To indicate if it is ready
         * @protected
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        initedPlayList: false,
        /*
         * To indicate if it is initied
         * @name inited
         * @memberof ax/device/samsung/DevicePackage#
         * @protected
         */
        inited: false,
        /**
         * To inited Device
         * @protected
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        initedDevice: false,
        /**
         * [Backward Compatible only] To be true when device package version >= 2.3, determine whether to run setup method in AbstractDevicePackage
         * @property {Boolean} _shouldSetup
         * @memberof ax/ext/device/samsung/DevicePackage#
         * @protected
         */
        _shouldSetup: true,
        /**
         * To return platform id of this abstraction
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        getId: function () {
            return "samsung";
        },
        /**
         * setup the device package
         * @param {Function} onDeviceLoaded callback when the device is loaded.
         * @returns {ax/ext/device/samsung/DevicePackage} abstract device package itself
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        setup: function (onDeviceLoaded) {
            this._super(onDeviceLoaded);

            //assume to use the old id to find the firmware year 
            id = new Id();

            //according to guideline, it supports since 2012
            if (id.getFirmwareYear() >= 2012) {
                console.info("[XDK] Try to load the webapi on the device with firmware year >= 2012");
                require(["ax/ext/device/samsung/loader/nativeAPI!locateAPI=false|$MANAGER_WIDGET/Common/webapi/1.0/webapis"], util.bind(function () {
                    //to assign back the value to the globals
                    globals.webapis = window.webapis;
                    console.info("[XDK] load the web api successfully");
                    this.__loadPackage();
                }, this), util.bind(function () {
                    console.info("[XDK] fail to load the device webapi");
                    this.__loadPackage();
                }, this));

                return;
            }

            this.__loadPackage();
            console.info("[XDK] Didn't try to load the webapis");
        },
        /**
         * To load the package
         * @method __loadPackage
         * @private
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        __loadPackage: function () {

            var interfaceTypes, ready;

            //init Interface
            interfaceTypes = this.getInterfaceTypes();

            util.each(interfaceTypes, util.bind(function (pair) {
                this.addInterfaceType(pair.key, pair.value.handle, pair.value.interfaceObj);
            }, this));

            //inti samsung step
            this.samsungInit();

            ready = util.bind(function () {
                this.initedPlayList = true;

                if (this.inited) {
                    return;
                }

                if (this.initedPlayList && this.initedDevice) {
                    console.info("[XDK]Ready to load after the play list ready");
                    this.inited = true;
                    this.ready();
                }
            }, this);

            this._preparePlayerList(ready);
        },
        /**
         * To set the default player to be {@link ax/ext/device/samsung/InfolinkPlayer}
         * @method getDefaultPlayer
         * @returns {String[]} Array of the player list
         * @public
         * @memberof ax/ext/device/samsung/DevicePackagel#
         */
        getDefaultPlayer: function () {
            return ["ax/ext/device/samsung/InfolinkPlayer"];
        },
        /**
         * To init each modules like id, media, system and tvkey
         * @method getInterfaceTypes
         * @protected
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        getInterfaceTypes: function () {
            console.info("[XDK] Get the interfaceType" + globals.webapis);
            //to create the system and id when no webapis
            if (!globals.webapis) {
                system = new System();
            } else {
                console.info("[XDK] Using the webAPI samsung set");

                console.info("[XDK] Run the System");
                system = new WebAPISystem();

                console.info("[XDK] Run the id");
                //to override with the one used in the init
                id = new WebAPIId();
            }
            console.info("[XDK] Start to load other parts");
            var ret = {};
            ret[AbstractDevicePackage.STORAGE] = {
                handle: AbstractDevicePackage.STORAGE_HANDLE,
                interfaceObj: new FileStorage()
            };

            ret[AbstractDevicePackage.TV_KEY] = {
                handle: AbstractDevicePackage.TV_KEY_HANDLE,
                interfaceObj: new TvKey()
            };

            ret[AbstractDevicePackage.MEDIA_PLAYER] = {
                handle: AbstractDevicePackage.MEDIA_PLAYER_HANDLE,
                interfaceObj: sMedia
            };

            ret[AbstractDevicePackage.ID] = {
                handle: AbstractDevicePackage.ID_HANDLE,
                interfaceObj: id
            };

            ret[AbstractDevicePackage.SYSTEM] = {
                handle: AbstractDevicePackage.SYSTEM_HANDLE,
                interfaceObj: system
            };

            //@todo 3D
            return ret;
        },
        /**
         * To set when the samsung device package is loaded.
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        samsungOnLoad: function () {
            console.info("[XDK] Samsung onload fired!");
            console.info("[XDK] Samsung onload: window.location.href: " + window.location.href);

            if (id.getFirmwareYear() < 2012) {
                document.body.style.position = "fixed";
            }

            //@workaround to remove handling on the starting Parameter
            /*
             try {
             accedo.device.samsung.DevicePackage._startingParameters = accedo.QueryString.getValue("data");
             accedo.device.samsung.DevicePackage._startingParameters = eval("(" + decodeURIComponent(accedo.device.samsung.DevicePackage._startingParameters) + ")");
             alert("starting Parameters acquired: " + accedo.Object.toJSON(accedo.device.samsung.DevicePackage._startingParameters));
             } catch (e) {
             alert("error acquiring starting parameters:" + e);
             }
             */

            //@workaround To remove the set screen saver as according to the requirement, screensaver is only turn off when playing the video/
            //So it shouldn't be turned off at the begining.
            //special handle for putting off screensaver all the time
            /*accedo.Fn.delay(function() {
             system.setScreenSaver(false);
             alert("[ON SHOW] turn off screen saver");
             }, 10);*/

            //Delay is implemented to allow time for plugin to initialize
            //(otherwise code below may get ignored by the device)
            util.delay(config.get("samsung.initDelay", 0.5)).then(util.bind(function () {
                //According to guideline, normal app should set to be 1
                globals.NNaviPlugin.SetBannerState(1);
                console.info("[XDK] Samsung onload: setBannerState");

                globals.pluginAPI.setOffIdleEvent();
                console.info("[XDK] Samsung onload: setOffIdleEvent");

                this.initedDevice = true;

                if (this.inited) {
                    return;
                }

                if (this.initedPlayList && this.initedDevice) {
                    console.info("[XDK]Ready to load after the setBanner State");
                    this.inited = true;
                    this.ready();
                }
            }, this)).done();

            //register part widget key
            globals.pluginAPI.registPartWidgetKey();
            console.info("[XDK] Samsung onload: registPartWidgetKey");

            //register full widget key
            globals.pluginAPI.registFullWidgetKey();
            console.info("[XDK] Samsung onload: registFullWidgetKey");
        },
        /**
         * To unset when the samsung device package is unloaded.
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        samsungOnUnload: function () {
            console.info("[XDK] Samsung onunload fired!");
            sMedia.stop();
            sMedia.setFullscreen();

            //@workaround Not sure what to do with the mPlugin and setFull screen
            //@todo: widevine connection stage crashing issue
        },
        /**
         * To initialize the samsung device
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/DevicePackage#
         */
        samsungInit: function () {
            console.info("[XDK] Samsung Device package initializing...");

            // samsung onLoad Handling instead of using window.onShow
            domReady(util.bind(this.samsungOnLoad, this));

            var sEnv = require("ax/Env").singleton();

            // set onunload function
            sEnv.addEventListener(sEnv.EVT_ONUNLOAD, util.bind(this.samsungOnUnload, this));
            console.info("[XDK] Assigned onunload function");

            // samsung package relies on Env.EVT_ONUNLOAD event
            // however, devices > 2012 use window.onHide for onunload, which Env does not aware of
            // therefore we dispatch EVT_ONUNLOAD through Env here
            if (id.getFirmwareYear() > 2011) {
                window.onHide = util.bind(sEnv.dispatchEvent, sEnv, sEnv.EVT_ONUNLOAD);
            }

            // call the samsung device to show the widget
            globals.widgetAPI.sendReadyEvent();
            console.info("[XDK] Sent ready event");
        }
    });
});
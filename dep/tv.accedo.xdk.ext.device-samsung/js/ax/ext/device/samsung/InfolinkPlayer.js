/**
 * Samsung Infolink player.
 * @class ax/ext/device/samsung/InfolinkPlayer
 * @augments ax/device/interface/Player
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/ext/device/samsung/InfolinkPlayer", [
    "ax/class",
    "ax/device/Media",
    "ax/console",
    "ax/Element",
    "ax/core",
    "ax/util",
    "ax/ajax",
    "ax/ext/device/samsung/globals",
    "ax/device/AbstractPlayer",
    "ax/device/interface/Player",
    "ax/device/interface/Id",
    "require"
], function (
    klass,
    Media,
    console,
    Element,
    core,
    util,
    ajax,
    globals,
    AbstractPlayer,
    IPlayer,
    IId,
    require) {
    "use strict";
    var sMedia = Media.singleton();
    var PLAYBACK_ERRORS = IPlayer.PLAYBACK_ERRORS;

    function _getDeviceAPI(api) {
        var device = require("ax/device");
        return device[api];
    }

    function _getResolution() {
        return _getDeviceAPI("system").getDisplayResolution();
    }


    return klass.create(AbstractPlayer, {}, {
        /**
         * current 3D mode
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _current3dType: 0,
        /**
         * previous 3D mode
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _previous3dTypeType: 0,
        /**
         * connection time limit
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _connectionTimeLimit: 90,
        /**
         * the media plugin object
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _plugin: null,
        /**
         * In case the player plugin object needs to be moved, this is the plugin object container
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _pluginContainer: null,
        /**
         * JSON containing current window size
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _currentWindowSize: null,
        /**
         * playback current time
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _curTime: 0,
        /**
         * media duration
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _duration: 0,
        /**
         * is the player ready for playback
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _prepared: false,
        /**
         * playback Speed
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _playbackSpeed: 1,
        /**
         * is there any stop function withholding
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _withHoldingStop: false,
        /**
         * media container format
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _type: null,
        /**
         * DRM technology being used
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _drm: null,
        /**
         * Cookie information intended for DRM server
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _cookie: null,
        /**
         * cookie url for authentication
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _authCookiesUrl: null,
        /**
         * custom data for playready
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _playReadyCustomData: null,
        /**
         * DRM server location
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _DRMServer: null,
        /**
         * is currently using 3D
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _is3d: false,
        /**
         * current 3D type
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _3dType: "off",
        /**
         * will be set to true whenever current playback time has been reported by plugin
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _curTimeReported: false,
        /**
         * @method
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         * @return {ax/device/interface/Player~PlayerCapabilites}
         */
        getCapabilities: function () {
            return {
                type: ["mp4", "asf", "hls", "has", "mp3"],
                drms: ["wmdrm", "playready", "widevine"]
            };
        },
        /**
         * To initialize the plugin player
         * @method
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         * @private
         */
        init: function () {
            this._plugin = globals.infolinkPlayer;

            if (!this._plugin) {
                throw core.createException("InfolinkPluginNotFound", "Infolink plugin is not found, player cannot be init");
            }

            //@TODO z-index hacks
            /*
             accedo.ui.AppSystem.onLoad = util.bind(function(appsystem){
             if (accedo.device.id.getFirmwareYear() > 2011) {
             return;
             }
             appsystem.addClass("samsung-buggy-zindex");
             this.placeVideoObject(appsystem);
             }, this);
             */

            /* Save current TV Source */

            //Sometimes we need to set to media source to remove the TV playback for 3-4s at the beginning of the app.
            //Before changing, we need to save the ORIGINAL source of the current device.
            //So add a check here, if we have already saved the TV source before loading js files, we should use that one.

            var self = this;
            this._plugin.deinit = function () {
                self._stopPlayback();
            };
        },
        /**
         * To prepare the plugin player for video playback
         * @method
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         * @public
         */
        prepare: function (opts) {
            if (this._prepared) {
                return;
            }
            this._prepared = true;
            console.debug("opts" + opts);
            // set up event callback function
            // 2010 and 2011 devices require setting strings as callbacks
            window.__SSMediaCallbacks = {};
            window.__SSMediaCallbacks.onCurrentPlayTime = util.bind(this._onCurrentPlayTime, this);
            window.__SSMediaCallbacks.setDuration = util.bind(this._setDuration, this);
            window.__SSMediaCallbacks.onStreamNotFound = util.bind(this._onStreamNotFound, this);
            window.__SSMediaCallbacks.onRenderError = util.bind(this._onRenderError, this);
            window.__SSMediaCallbacks.onCustomEvent = util.bind(this._onCustomEvent, this);
            window.__SSMediaCallbacks.onConnectionFailed = util.bind(this._onConnectionFailed, this);
            window.__SSMediaCallbacks.onRenderingComplete = util.bind(this._onRenderingComplete, this);
            window.__SSMediaCallbacks.onBufferingProgress = util.bind(this._onBufferingProgress, this);
            window.__SSMediaCallbacks.onAuthenticationFailed = util.bind(this._onAuthenticationFailed, this);
            window.__SSMediaCallbacks.onBufferingStart = util.bind(this._onBufferingStart, this);
            window.__SSMediaCallbacks.onBufferingComplete = util.bind(this._onBufferingComplete, this);
            window.__SSMediaCallbacks.onNetworkDisconnected = util.bind(this._onNetworkDisconnected, this);

            this._plugin.OnCurrentPlayTime = "__SSMediaCallbacks.onCurrentPlayTime";
            this._plugin.OnStreamInfoReady = "__SSMediaCallbacks.setDuration";
            this._plugin.OnStreamNotFound = "__SSMediaCallbacks.onStreamNotFound";
            this._plugin.OnRenderError = "__SSMediaCallbacks.onRenderError";
            this._plugin.OnCustomEvent = "__SSMediaCallbacks.onCustomEvent";
            this._plugin.OnConnectionFailed = "__SSMediaCallbacks.onConnectionFailed";
            this._plugin.OnRenderingComplete = "__SSMediaCallbacks.onRenderingComplete";
            this._plugin.OnBufferingProgress = "__SSMediaCallbacks.onBufferingProgress";
            this._plugin.OnAuthenticationFailed = "__SSMediaCallbacks.onAuthenticationFailed";
            this._plugin.OnBufferingStart = "__SSMediaCallbacks.onBufferingStart";
            this._plugin.OnBufferingComplete = "__SSMediaCallbacks.onBufferingComplete";
            this._plugin.OnNetworkDisconnected = "__SSMediaCallbacks.onNetworkDisconnected";

        },
        /**
         * To reset the plugin player when it is not currently used
         * @method
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         * @public
         */
        reset: function () {
            if (!this._prepared) {
                return;
            }
            this._prepared = false;

            delete this._plugin.OnCurrentPlayTime;
            delete this._plugin.OnStreamInfoReady;
            delete this._plugin.OnStreamNotFound;
            delete this._plugin.OnRenderError;
            delete this._plugin.OnCustomEvent;
            delete this._plugin.OnConnectionFailed;
            delete this._plugin.OnRenderingComplete;
            delete this._plugin.OnBufferingProgress;
            delete this._plugin.OnAuthenticationFailed;
            delete this._plugin.OnBufferingStart;
            delete this._plugin.OnBufferingComplete;
            delete this._plugin.OnNetworkDisconnected;

            //enable screen saver when there is no player
            _getDeviceAPI("system").setScreenSaver(true);
        },
        /**
         * Safely de-init and remove the mediapPyer
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         * @ignore
         */
        deinit: function () {
            console.info("samsung media deinit");
            if (!this._prepared) {
                return;
            }
            if (!this._inited) {
                return;
            }

            if (_getDeviceAPI("id").getHardwareType() !== IId.HARDWARE_TYPE.TV) {
                this._current3dType = 0;
                this._previous3dType = 0;
                if (globals.pluginObject3D && globals.pluginObject3D.Check3DEffectMode(0) === 1) {
                    globals.pluginObject3D.Set3DEffectMode(0);
                }
            }

            this.stop();



            this.reset();
            delete window.__SSMediaCallbacks.onCurrentPlayTime;
            delete window.__SSMediaCallbacks.setDuration;
            delete window.__SSMediaCallbacks.onStreamNotFound;
            delete window.__SSMediaCallbacks.onRenderError;
            delete window.__SSMediaCallbacks.onCustomEvent;
            delete window.__SSMediaCallbacks.onConnectionFailed;
            delete window.__SSMediaCallbacks.onRenderingComplete;
            delete window.__SSMediaCallbacks.onBufferingProgress;
            delete window.__SSMediaCallbacks.onAuthenticationFailed;
            delete window.__SSMediaCallbacks.onBufferingStart;
            delete window.__SSMediaCallbacks.onBufferingComplete;
            delete window.__SSMediaCallbacks.onNetworkDisconnected;

            delete window.__SSMediaCallbacks;

            this._inited = false;

            console.info("samsung media deinit success");
            return;
        },
        /**
         * Place the video plugin object as child, to fight the z-index problem on old devices below 2012
         * @method
         * @param {ax/Element|Element|ax/af/AbstractComponent} parent The parent element to contain the video plugin object
         * @param {Boolean} [forceExecute] If true, execute this even on new devices that do not need this workaround
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        placeVideoObject: function (parent, forceExecute) {
            var container, containerEle;
            if (!forceExecute && _getDeviceAPI("id").getFirmwareYear() > 2011) {
                console.warn("placeVideoObject() is a workaround method for 2011 devices or older, to fight the z-index problem!");
                return;
            }

            if (!parent) {
                console.error("Parent element of the video plugin object must be specified!");
                return;
            }

            console.info("Moving samsung player plugin object...");

            if (parent.getRoot) { // this is component
                parent = parent.getRoot();
            } else if (parent.nodeName) {
                parent = new Element(parent);
            }

            this._plugin.parentNode.removeChild(this._plugin);
            delete this._plugin.OnCurrentPlayTime;
            delete this._plugin.OnStreamInfoReady;
            delete this._plugin.OnStreamNotFound;
            delete this._plugin.OnRenderError;
            delete this._plugin.OnCustomEvent;
            delete this._plugin.OnConnectionFailed;
            delete this._plugin.OnRenderingComplete;
            delete this._plugin.OnBufferingProgress;
            delete this._plugin.OnAuthenticationFailed;
            delete this._plugin.OnBufferingStart;
            delete this._plugin.OnBufferingComplete;
            delete this._plugin.OnNetworkDisconnected;

            if (this._pluginContainer) {
                this._pluginContainer.detach();
                this._pluginContainer = null;
            }

            //to ensure the container is removed from dom
            containerEle = document.getElementById("InfolinkContainer");
            if (containerEle) {
                containerEle.parentNode.removeChild(containerEle);
            }

            this._pluginContainer = new Element("div", {
                id: "InfolinkContainer",
                css: "playerContainer"
            }, parent);
            container = this._pluginContainer.getHTMLElement();
            // have to use innerHTML for it to work
            container.innerHTML = "<object id=\"infolinkPlayer\" border=0 classid=\"clsid:SAMSUNG-INFOLINK-PLAYER\" style=\"position:absolute;\"></object>";
            this._plugin = document.getElementById("infolinkPlayer");

            this._plugin.OnCurrentPlayTime = "__SSMediaCallbacks.onCurrentPlayTime";
            this._plugin.OnStreamInfoReady = "__SSMediaCallbacks.setDuration";
            this._plugin.OnStreamNotFound = "__SSMediaCallbacks.onStreamNotFound";
            this._plugin.OnRenderError = "__SSMediaCallbacks.onRenderError";
            this._plugin.OnCustomEvent = "__SSMediaCallbacks.onCustomEvent";
            this._plugin.OnConnectionFailed = "__SSMediaCallbacks.onConnectionFailed";
            this._plugin.OnRenderingComplete = "__SSMediaCallbacks.onRenderingComplete";
            this._plugin.OnBufferingProgress = "__SSMediaCallbacks.onBufferingProgress";
            this._plugin.OnAuthenticationFailed = "__SSMediaCallbacks.onAuthenticationFailed";
            this._plugin.OnBufferingStart = "__SSMediaCallbacks.onBufferingStart";
            this._plugin.OnBufferingComplete = "__SSMediaCallbacks.onBufferingComplete";
            this._plugin.OnNetworkDisconnected = "__SSMediaCallbacks.onNetworkDisconnected";
            globals.infolinkPlayer = this._plugin;
            this._plugin.deinit = util.bind(function () {
                this._stopPlayback();
            }, this);
        },
        /**
         * set the window size
         * @param {Object} param window size parameter
         * @param {Boolean} param.relativeToParent True if relative to the parent position and then use the css position:relative. \
         *                   Default will be false and use position:fixed
         * @param {Integer} param.top window top
         * @param {Integer} param.left window left
         * @param {Integer} param.width window width
         * @param {Integer} param.height window height
         * @method
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        setWindowSize: function (obj) {
            if (!this._plugin) {
                console.warn("[InfoPlayer] Fail to set the size due to no player Object");
                return;
            }

            var objClassName = "playerObject";

            //determine whether it is relative to parent which add a relative class with "position:relative"
            if (obj.relativeToParent) {
                objClassName += " relative";
            }

            //determine whether it is fullscreen and add a fullscreen selector
            if (obj.width === _getResolution().width && obj.height === _getResolution().height) {
                objClassName += " fullscreen";
                if (this._pluginContainer) {
                    this._pluginContainer.addClass("fullscreen");
                }
            } else {
                if (this._pluginContainer) {
                    this._pluginContainer.removeClass("fullscreen");
                }
            }

            this._plugin.className = objClassName;

            this._plugin.style.top = obj.top + "px";
            this._plugin.style.left = obj.left + "px";
            this._plugin.style.width = obj.width + "px";
            this._plugin.style.height = obj.height + "px";

            this.show();

            this._setPlayerSize(obj);
            this._currentWindowSize = obj;

            if (sMedia.isState(sMedia.PAUSED)) {
                // to prevent the player from resuming on its own. Samsung is not to be trusted.
                this._plugin.Pause();
            }
        },
        /**
         * Show the player
         * @method
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        show: function () {
            if (this._plugin) {
                this._plugin.style.visibility = "visible";
            }
        },
        /**
         * Hide the player
         * @method
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        hide: function () {
            if (this._plugin) {
                this._plugin.style.visibility = "hidden";
            }
        },
        /**
         * set the window size to be fullscrenn
         * @method
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        setFullscreen: function () {
            this.setWindowSize({
                top: 0,
                left: 0,
                height: _getResolution().height,
                width: _getResolution().width
            });
        },
        /**
         * set the player plugin size
         * @method
         * @protected
         * @param {object} obj the object of the dimension.
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _setPlayerSize: function (obj) {
            var pos, offset, coef, x, y, w, h;
            pos = {
                x: obj.left,
                y: obj.top
            };
            //if it is inside the container, the position will be updated with respect to its parent container when parent node is set.
            if (this._pluginContainer && (obj.width !== _getResolution().width || obj.height !== _getResolution().height)) {
                offset = this._pluginContainer.cumulativeOffset();
                if (offset.x) {
                    pos.x = offset.x;
                }
                if (offset.y) {
                    pos.y = offset.y;
                }
            }
            if (this._plugin !== null) {
                coef = 1;
                x = pos.x;
                y = pos.y;
                w = obj.width;
                h = obj.height;
                if (_getDeviceAPI("id").getFirmwareYear() !== 2010) { // 2010 device should only use 1
                    coef = (_getResolution().width === 960 ? 1 : 960 / 1280);
                }
                console.info("Math.round(x * coef), Math.round(y * coef)" + Math.round(x * coef) + "," + Math.round(y * coef));
                this._plugin.SetDisplayArea(Math.round(x * coef), Math.round(y * coef), Math.round(w * coef), Math.round(h * coef));
            }
        },
        /**
         * Loads a media from a URL and prepares the media player before playing
         * @method
         * @param {String} mediaUrl  url string of the media
         * @param {Object} [opts] Optional - extra prarameter needed
         * @param {String} [opts.drm] DRM technology to use
         * @param {String} [opts.type] media container format to use
         * @param {String} [opts.drmUrl] (Widevine || Playready || Verimatrix) Set the DRM license url
         * @param {String} [opts.userData] (Widevine) Set the user data
         * @param {String} [opts.deviceId] (Widevine)  the device id
         * @param {String} [opts.deviceTypeId] (Widevine)the device type id when using widevine
         * @param {String} [opts.drmCurTime] (Widevine) cur time param when using widevine
         * @param {String} [opts.iSeek] (Widevine) i-seek param when using widevine
         * @param {String} [opts.extraString] (Widevine) extra string param when using widevine
         * @param {String} [opts.portal] (Widevine)portal param when using widevine
         * @param {String} [opts.authCookiesUrl] (WMDRM) Set the url to get the authentication cookies
         * @param {String} [opts.customData] (PlayerReady) to set custom data
         * @param {String} [opts.startBitrate] (PlayerReady) To set the initial bitrate for PlayReady e.g ("AVERAGE"/"LOWEST"/"HIGHEST"/"CHECK"/"532000")
         * @param {String} [opts.bitrate] (PlayerReady) Set bitrate range for PlayReady e.g ("305000~937000")
         * @param {String} [opts.skipBitrate] (PlayerReady) Set the bitrate value after skip for PlayReady e.g ("CHECK"/"LOWEST"/"AVERAGE"/"HIGHEST")
         * @param {String} [opts.cookie] (PlayerReady) Set the COOKIE information
         * @param {Number} [opts.timeOut] (PlayerReady) Set the Network timeout value(in seconds)
         * @param {Number} [opts.startFragment] (PlayerReady) Set the fragment in live streaming (e.g 5) startFragment
         * @param {String} [opts.startBitrate] (HLS || HAS) To set the initial bitrate e.g 2600000
         * @param {Number} [opts.bitratesFrom] (HLS || HAS) the lowest bitrate setting
         * @param {Number} [opts.bitratesTo] (HLS || HAS) the highest bitrate setting
         * @param {Boolean} [opts.use3d] true if to use 3D
         * @param {String} [opts.type3d] the type of 3D
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        load: function (mediaUrl, opts) {
            opts = opts || {};
            this._drm = opts.drm;
            this._type = opts.type;

            var component = "",
                deviceId, iSeek, drmCurTime, drmUrl, extraString, userData, deviceType, bitrates, portal, tempStr;

            if (opts.parentNode) {
                this.placeVideoObject(opts.parentNode, true);
            }

            if (opts.drm === "widevine") {
                component = "WV";
                deviceId = "60";
                iSeek = "TIME";
                drmCurTime = "PTS";
                drmUrl = "";
                extraString = "";
                userData = "";
                deviceType = "";
                portal = "";

                if (!util.isUndefined(opts.drmUrl)) {
                    drmUrl = opts.drmUrl;
                }
                if (!util.isUndefined(opts.deviceId)) {
                    deviceId = opts.deviceId;
                }
                if (!util.isUndefined(opts.iSeek)) {
                    iSeek = opts.iSeek;
                }
                if (!util.isUndefined(opts.drmCurTime)) {
                    drmCurTime = opts.drmCurTime;
                }
                if (!util.isUndefined(opts.extraString)) {
                    extraString = opts.extraString;
                }
                if (!util.isUndefined(opts.deviceType)) {
                    deviceType = "|DEVICE_TYPE_ID=" + opts.deviceType;
                }
                if (!util.isUndefined(opts.userData)) {
                    userData = "|USER_DATA=" + opts.userData;
                }
                if (!util.isUndefined(opts.portal)) {
                    portal = "|PORTAL=" + opts.portal;
                }

                mediaUrl += "|DRM_URL=" + drmUrl + "|DEVICE_ID=" + deviceId + "|I_SEEK=" + iSeek + "|CUR_TIME=" + drmCurTime + userData + portal + extraString + deviceType + "|COMPONENT=" + component;

                console.info("Infolink Player: widevine turned on");

            } else if (opts.drm === "wmdrm") {
                component = "WMDRM";
                if (opts.authCookiesUrl && this._authCookiesUrl !== opts.authCookiesUrl) {
                    this._authCookiesUrl = opts.authCookiesUrl;
                    console.info("Infolink Player: send a request to get the cookies");
                    ajax.request(opts.authCookiesUrl, {
                        method: "GET",
                        async: false
                    }).then(util.bind(function (transport) {
                        if (transport.getResponseHeader("Set-Cookie")) {
                            this._cookie = transport.getResponseHeader("Set-Cookie");
                            _getDeviceAPI("storage").set("accedoDRMCookie", this._cookie);
                            _getDeviceAPI("storage").save();
                            console.info("Infolink Player: get the set-cookies in response:" + this._cookie);
                        } else {
                            _getDeviceAPI("storage").load();
                            if (_getDeviceAPI("storage").get("accedoDRMCookie")) {
                                this._cookie = _getDeviceAPI("storage").get("accedoDRMCookie");
                            }
                            console.info("Infolink Player: no set cookie in response and try to get it from storage:" + this._cookie);
                        }

                        console.info("Infolink Player: got cookie:" + this._cookie);
                    }, this), function (rejection) {

                        //unable to get the response from the request and suppose to be wrong,throw error
                        console.error("Infolink Player: fail to get the cookie with the reason" + rejection);

                    }).done();
                } else {
                    //either already have no url or same url as before and then try to get the cookie from the storage
                    if (_getDeviceAPI("storage").get("accedoDRMCookie")) {
                        this._cookie = _getDeviceAPI("storage").get("accedoDRMCookie");
                    }

                    console.info("Infolink Player: got cookie from storage:" + this._cookie);
                }

                mediaUrl += "|COMPONENT=" + component;

                console.info("Infolink Player: WMDRM turned on");

            } else if (opts.drm === "playready") {
                tempStr = "";

                if (opts.customData) {
                    this._playReadyCustomData = opts.customData;
                }

                if (opts.drmUrl) {
                    this._DRMServer = opts.drmUrl;
                }

                if (!util.isUndefined(opts.startBitrate)) {
                    tempStr += "|STARTBITRATE=" + opts.startBitrate;
                }

                if (!util.isUndefined(opts.bitrate)) {
                    tempStr += "|BITRATE=" + opts.bitrate;
                }

                if (!util.isUndefined(opts.skipBitrate)) {
                    tempStr += "|SKIPBITRATE=" + opts.skipBitrate;
                }

                if (!util.isUndefined(opts.cookie)) {
                    tempStr += "|COOKIE=" + opts.cookie;
                }

                if (!util.isUndefined(opts.timeOut)) {
                    tempStr += "|NETWORKTIMEOUT=" + opts.timeOut;
                }

                if (!util.isUndefined(opts.startFragment)) {
                    tempStr += "|STARTFRAGMENT=" + opts.startFragment;
                }

                mediaUrl += tempStr;

                console.info("Infolink Player: Infolink Player: playReady turned on");
            }

            if (opts.type === "hls") {
                component = "HLS";

                bitrates = "";

                if (opts.bitratesFrom && opts.bitratesTo && (opts.bitratesFrom) < (opts.bitratesTo)) {
                    bitrates += "|BITRATES=" + opts.bitratesFrom + "~" + opts.bitratesTo;
                }

                if (!util.isUndefined(opts.startBitrate)) {
                    bitrates += "|STARTBITRATE=" + opts.startBitrate;
                }

                mediaUrl += bitrates + "|COMPONENT=" + component;

            } else if (opts.type === "has") {
                component = "HAS";

                bitrates = "";


                if (opts.bitratesFrom && opts.bitratesTo && (opts.bitratesFrom) < (opts.bitratesTo)) {
                    bitrates += "|BITRATES=" + opts.bitratesFrom + "~" + opts.bitratesTo;
                }

                if (!util.isUndefined(opts.startBitrate)) {
                    bitrates += "|STARTBITRATE=" + opts.startBitrate;
                }

                mediaUrl += bitrates + "|COMPONENT=" + component;
            }

            if (opts.use3d) {
                this._is3d = true;
                this._3dType = "sideBySide";

                if (opts.type3d) {
                    this._3dType = opts.type3d;
                }
            } else {
                this._is3d = false;
                this._3dType = "off";
            }

            //if it is audio type,no matter whether it set the window size before,it will also change back to 0,0,0,0
            if (!util.isUndefined(opts.type) && opts.type === "mp3") {
                this._currentWindowSize = this.AUDIO_SIZE;
                this.setWindowSize(this._currentWindowSize);
            }

            this.url = mediaUrl;
            return mediaUrl;
        },
        /**
         * play the media
         * @method
         * @param {Object} opts object containing the options
         * @param {Number} opts.sec Play the video at the specified second
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        play: function (opts) {
            console.info("Infolink Player: play requested with url " + this.url);

            _getDeviceAPI("system").setScreenSaver(false);
            var playResult = this.__attemptPlay(opts);

            if (playResult === -1 || playResult === "-1" || playResult === false) {
                sMedia._onError(PLAYBACK_ERRORS.GENERIC.LOAD);
                return false;
            }

            //only change state when the video is stopped
            if (sMedia.isState(sMedia.STOPPED)) {
                sMedia._onConnecting();

                this.hide();

                this.__createConnectionTimeout();
            }

            //Carry out 3D handling for 2011 devices
            if (this._is3d && _getDeviceAPI("id").getFirmwareYear() >= 2011) {
                console.info("Infolink Player: turning on 3D mode");
                if (this._current3dType !== this._3dType) {
                    this._previous3dType = this._current3dType;
                }
                this._current3dType = this._3dType;

                if ((globals.pluginObject3D.Flag3DEffectSupport && globals.pluginObject3D.Flag3DEffectSupport() === 1) || (globals.pluginObject3D.Flag3DTVConnect && globals.pluginObject3D.Flag3DTVConnect() === 1)) {

                    if (this._3dType === "sideBySide") {
                        this._plugin.SetPlayerProperty(2, "3", 1);
                    }

                }
            }
            return playResult;
        },
        /**
         * Really calls plugin object to play the media
         * @method
         * @param {Object} opts object containing the options
         * @param {Number} opts.sec Play the video at the specified second
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        __attemptPlay: function (opts) {
            opts = opts || {};
            var sec = opts.sec || 0;

            switch (sMedia.getState()) {

            case sMedia.PLAYING:
            case sMedia.SPEEDING:

                if (!util.isUndefined(opts.sec)) {
                    this.seek(opts.sec);
                }

                console.info("[XDK] AVPlayer set the playback speed to 1 when pressing play");

                if (this._playbackSpeed !== 1) {
                    this.speed(1);
                }

                return;

            case sMedia.PAUSED:

                if (!util.isUndefined(opts.sec)) {
                    this.seek(opts.sec);
                }
                this.resume();

                return;
            }

            // some devices will make the video fullscreen when finished playback
            // so reset the window size to counter that glitch
            if (this._currentWindowSize) {
                console.info("[XDK] setting the window size!!");
                this.setWindowSize(this._currentWindowSize);
            }

            if (sec && "wmdrm" !== this._drm && "playready" !== this._drm) {
                console.info("Infolink Player: playback at certain position");
                return this._plugin.ResumePlay(this.url, sec);
            }

            // playback from stopped
            this._withHoldingStop = false;
            this._playbackSpeed = 1;
            this._curTime = 0;
            this._duration = 0;


            console.info("Infolink Player: will play from the beginning");

            if ("wmdrm" === this._drm) {
                this._plugin.InitPlayer(this.url);

                if (this._cookie) {
                    this._plugin.SetPlayerProperty(1, this._cookie, this._cookie.length);
                    console.info("Infolink Player: set the cookies to the player" + this._cookie);
                }

                try {
                    return this._plugin.ResumePlay(this.url, sec);
                } catch (e) {
                    console.error("WMDRM Error: " + e.message);
                    return false;
                }

            }

            if ("playready" === this._drm) {
                console.info("Infolink Player: using PlayReady: " + this.url);
                this._plugin.InitPlayer(this.url);

                if (this._playReadyCustomData) {
                    this._plugin.SetPlayerProperty(3, this._playReadyCustomData, this._playReadyCustomData.length);
                    console.info("Infolink Player: set the custom data" + this._playReadyCustomData);
                }

                if (this._DRMServer) {
                    this._plugin.SetPlayerProperty(4, this._DRMServer, this._DRMServer.length);
                    console.info("Infolink Player: set the DRMServer " + this._DRMServer);
                }

                try {
                    return this._plugin.ResumePlay(this.url, sec);
                } catch (e) {
                    console.error("Play Ready Error: " + e.message);
                    return false;
                }
            }

            return this._plugin.Play(this.url);
        },
        /**
         * pause the video item
         * @method
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        pause: function () {
            this._plugin.Pause();

            sMedia._onPause();

            //enable the screen saver when it is paused
            _getDeviceAPI("system").setScreenSaver(true);
        },
        /**
         * stop the video item
         * @method
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        stop: function () {
            console.info("Infolink player: stop requested");

            this.hide();

            this._stopPlayback();
            sMedia._onStopped();
            return true;
        },
        /**
         * stop the video playback for real
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _stopPlayback: function () {
            console.info("Infolink Player: stop action enforced");
            if (this._plugin !== null) {

                if (this._is3d && _getDeviceAPI("id").getFirmwareYear() >= 2011) {
                    console.info("Infolink Player: turn on 3D mode");

                    if ((globals.pluginObject3D.Flag3DEffectSupport && globals.pluginObject3D.Flag3DEffectSupport() === 1) || (globals.pluginObject3D.Flag3DTVConnect && globals.pluginObject3D.Flag3DTVConnect() === 1)) {

                        this._plugin.SetPlayerProperty(2, "3", 0);

                    }
                }

                this._plugin.Stop();
                this.hide();

            }

            //remove the connection time out when stop
            this.__removeConnectionTimeout();

            this._curTimeReported = false; // reset as no current playback time reported by plugin
            _getDeviceAPI("system").setScreenSaver(true);
        },
        /**
         * resume the video item
         * @method
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        resume: function () {
            this._plugin.Resume();

            // disable the screen saver when resume playing
            _getDeviceAPI("system").setScreenSaver(false);

            sMedia._onPlaying();

            if (this._playbackSpeed !== 1) {
                this._playbackSpeed = 1;
                this._plugin.SetPlaybackSpeed(this._playbackSpeed);
            }
        },
        /**
         * seek to a specific time
         * @method
         * @param {Number} sec the number of second you want to seek to (in sec)
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         * @public
         */
        seek: function (sec) {
            //workaround to seek when speeding.
            if (sMedia.isState(sMedia.SPEEDING)) {
                sec = Math.max(sec, 1);
            }
            //get current time in sec
            var curTime = this.getCurTime();

            this.skip(sec - curTime);
        },
        /**
         * skip the video for a few second
         * @method
         * @param {Number} sec number of second to skip (10 by default)
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        skip: function (sec) {
            //Note: haven"t been test for non progressive usage.
            var boundaryTime = 1,
                curTime = this.getCurTime(),
                duration = this.getDuration();

            if (!sec) {
                return;
            }

            //3000 because some video(http://av.vimeo.com/44206/716/14097677.mp4?aksessionid=5ac5cb7b639a84db8c1fd46388ca4b20&token=1361333279_7b2482145462029a47655992e4df9289) is unable to jumpForward properly on 2012BDP/2012TV.
            //If less than 3000, some videos will not jump but dispatch onRenderComplete event
            //To summarize, here is my testing result 1:04 video and 1000s
            //on 2010 TV will go to 0:58 and play so it doesn"t matter on the boundary case
            //on 2011 TV will go properly and go to 1:03s and then play
            //on 2012 TV/BDP will go back to the previous skip time and then play a few second then suddenly jump to 1:04 then stop. So if set to 3s,it can jump successfully to the desired time and play.
            if (_getDeviceAPI("id").getFirmwareYear() === 2012) {
                boundaryTime = 3;
            }


            if (curTime + sec > duration - boundaryTime) { // make sure do not skip outside of boundary
                sec = duration - boundaryTime - curTime;
            } else if (curTime + sec < 0) {
                sec = -curTime;
            }
            if (_getDeviceAPI("id").getFirmwareYear() <= 2011 && this._type !== "hls") {
                sec = Math.ceil(sec * 0.95);
                console.info("Infolink Player: Trying to correct skip gap... gap is now:" + sec);
            }
            console.debug("Infolink Player: Skip with gap: " + sec);
            if (!sec) { // request a 0 second jump to device does work well!
                return;
            }
            if (sec < 0) {
                this._plugin.JumpBackward(Math.round(sec) * -1);
            } else {
                this._plugin.JumpForward(Math.round(sec));
            }
        },
        /**
         * set the playback speed of the media
         * @method
         * @param {Number} speed speed number, better to be multiple of 2 and within -16 to 16
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        speed: function (speed) {
            // does not work on paused state as well
            if (sMedia.isState(sMedia.PAUSED)) {
                this._plugin.Resume();
            }

            // speeding in HLS is not possible due to device limitation
            if (this._type === "hls") {
                console.warn("Infolink Player:Speeding during HLS playback is not supported by device!");
                //since it resume and change back to the state for playing
                sMedia._onPlaying();
                return;
            }

            speed = Math.floor(speed);

            if (speed > 16) {
                speed = 16;
            }
            if (speed < -16) {
                speed = -16;
            }

            this._playbackSpeed = speed;
            //Warning: not all device and media type would support this function
            if (speed === 1) {
                this._plugin.SetPlaybackSpeed(1);
                sMedia._onPlaying();
                return;
            }

            this._plugin.SetPlaybackSpeed(speed);
            sMedia._onSpeeding();
        },
        /**
         * get bitrate and available bitrates
         * @method
         * @return {ax/device/interface/Player~MediaBitrates}
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        getBitrates: function () {
            if (this._currentBitrate === null || this._availableBitrates === null) {
                return false;
            }

            return {
                currentBitrate: this._currentBitrate,
                availableBitrates: this._availableBitrates
            };
        },
        /**
         * get and return playbackSpeed
         * @method
         * @return {Number} playback speed
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        getPlaybackSpeed: function () {
            return this._playbackSpeed;
        },
        /**
         * set the current time
         * @method
         * @param {Number} time current time in milliseconds
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _setCurTime: function (time) {
            var curBitrate, availBitrateString, sortNumeric, availBitrate, i;

            console.info("Infolink Player: set current time to:" + time);
            this._curTime = time;
            sMedia._onTimeUpdate(time / 1000);

            //3D handling for 2010 devices
            //@TODO
            /*
             if (id.getFirmwareYear() < 2011) {
             if (time > 0 && time < 5000 && this._is3d) {
             console.info("Infolink Player: turn on 3D mode");
             
             //set 3d type
             if (this._current3dType !== this._3dType) {
             //check whether it is 2d to 3d or off currently
             current3dEffect = globals.pluginObject3D.Get3DEffectMode();
             if (current3dEffect === 7) {
             this._current3dType = "2dTo3d";
             }else{
             this._current3dType = "off";
             }
             this._previous3dType = this._current3dType;
             }
             this._current3dType = this._3dType;
             
             accedo.device.threeD.switch3DmodeFor2010Device();
             
             } else if(time > 0 && time < 5000  && !this._is3d && this._current3dType === 2) {
             console.info("Infolink Player: turn off 3D mode");
             console.info("Infolink Player: previous3D: " + this._previous3dType + " currentMode: " + this._current3dType);
             if (this._previous3dType === "2dTo3d") {
             this._previous3dType =  this._current3dType;
             this._current3dType = "2dTo3d";
             } else {
             this._previous3dType = this._current3dType;
             this._current3dType = "off";
             }
             
             accedo.device.threeD.switch3DmodeFor2010Device();
             console.info("Infolink Player: after >> previous3D: " + this._previous3dType + " currentMode: " + this._current3dType);
             }
             }*/

            if (sMedia.isState(sMedia.BUFFERING) || sMedia.isState(sMedia.STOPPED) || sMedia.isState(sMedia.PAUSED)) {
                return;
            }

            //upagte bitrates
            curBitrate = this._plugin.GetCurrentBitrates();

            if (typeof curBitrate === "number") {
                this._currentBitrate = parseInt(curBitrate, 10);
            }
            availBitrateString = this._plugin.GetAvailableBitrates();

            sortNumeric = function (a, b) {
                return Number(a) - Number(b);
            };

            if (typeof availBitrateString === "string") {
                availBitrate = availBitrateString.split("|");
                for (i = 0; i < availBitrate.length - 1; i++) {
                    availBitrate[i] = parseInt(availBitrate[i], 10);
                }
                this._availableBitrates = availBitrate.sort(sortNumeric);
            }
        },
        /**
         * Returns the current time (0 if never played)
         * @method
         * @param {boolean} seconds When true, the return value is in seconds
         * @returns {Integer|false} current time in milliseconds (or seconds according to the "seconds" param)
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        getCurTime: function () {
            return Math.floor(this._curTime / 1000);
        },
        /**
         * set the total time by reading the media's duration. It will be called when the media is loaded
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _setDuration: function () {
            this._duration = this._plugin.GetDuration();
            //remove the connection time out since the meta data is ready
            this.__removeConnectionTimeout();
        },
        /**
         * get total time of the media
         * @method
         * @public
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        getDuration: function () {
            //XDK-2266 Under some conditions the HLS Infolink player don't get the correct duration. 
            //checking if the duration is equal to 0, asking the plugin for the correct one.
            if (!this._duration) {
                this._duration = this._plugin.GetDuration();
            }

            return Math.floor(this._duration / 1000);
        },
        /**
         * function to be triggered by the plugin player when the current playback time is progressing
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onCurrentPlayTime: function (time) {
            this._setCurTime(time);
            if (this._curTimeReported) {
                return;
            }
            console.info("Infolink Player: plugin reported current playback time for the first time.");
            this._curTimeReported = true; // indicate current playback time has been reported by plugin
        },
        /**
         * function to be triggered by the plugin player when the stream is not found
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onStreamNotFound: function () {
            console.info("Infolink Player: onStreamNotFound");
            this._stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.NETWORK.FILE);
        },
        /**
         * function to be triggered when the connection is timed out
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onConnectionTimeout: function () {
            console.info("Infolink Player: onConnectionTimeout");
            // this._stopPlayback(); //Ho: yet to decide use _stopPlayback or not

            //enable the screen saver since it is not playing any video
            _getDeviceAPI("system").setScreenSaver(true);

            sMedia._onError(PLAYBACK_ERRORS.NETWORK.TIMEOUT);
        },
        /**
         * function to be triggered by the plugin player when the connection is failed
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onConnectionFailed: function () {
            console.info("Infolink Player: onConnectionFailed");
            this._stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.NETWORK.FAILED);
        },
        /**
         * function to be triggered by the plugin player when there is rendering error
         * @method
         * @param {Number} error code ID
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onRenderError: function (param) {
            console.info("Infolink Player: onRenderError " + param);
            this._stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.RENDER.FAILED, null, param);
        },
        /**
         * function to be triggered by the plugin player when there is custom error from external plugin like widevine
         * @method
         * @param {Integer} error code ID
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onCustomEvent: function (param) {
            console.info("Infolink Player: onCustomEvent " + param);
            this._stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.RENDER.FAILED, null, param);
        },
        /**
         * function to be triggered by the plugin player when there is authentication failure
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onAuthenticationFailed: function () {
            console.info("Infolink Player: [xdk Samsung] onAuthenticationFailed ");
            this._stopPlayback();
            sMedia._onError("onAuthenticationFailed", "Authentication failed!");
            sMedia._onError(PLAYBACK_ERRORS.DRM.FAILED);
        },
        /**
         * function to be triggered by the plugin player the network is disconnected
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onNetworkDisconnected: function () {
            console.info("Infolink Player: [xdk Samsung] nNetworkDisconnected ");
            this._stopPlayback();
            sMedia._onError(PLAYBACK_ERRORS.NETWORK.DISCONNECTED);
        },
        /**
         * function to be triggered by the plugin player when the media rendering is complete
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onRenderingComplete: function () {
            this._stopPlayback();
            sMedia._onFinish();
        },
        /**
         * function to be triggered by the plugin player when the buffering is in progress
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onBufferingProgress: function () {
            if (this._withHoldingStop === true) {
                console.info("Infolink Player: carry out withholding stop action");
                this.stop();
                this._withHoldingStop = false;
                return;
            }

            sMedia._onBufferingProgress();
        },
        /**
         * function to be triggered by the plugin player when the buffering is started
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onBufferingStart: function () {
            if (this._withHoldingStop === true) {
                console.info("Infolink Player: carry out withholding stop action");
                this.stop();
                this._withHoldingStop = false;
                return;
            }

            sMedia._onBufferingStart();
        },
        /**
         * function to be triggered by the plugin player when the buffering is completed
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        _onBufferingComplete: function () {
            sMedia._onBufferingFinish();

            if (sMedia.isState(sMedia.STOPPED)) {
                return;
            }

            var self = this;

            // hackish workaround for 2010 BDP refuses to start playback (bug XDK-322)
            if (_getDeviceAPI("id").getHardwareType() === IId.HARDWARE_TYPE.BD && _getDeviceAPI("id").getFirmwareYear() === 2010 && !this._curTimeReported) {
                console.info("Infolink Player: 2010 BDP playback failure detection in effect!");
                util.delay(1.5).then(function () {
                    if (!self._curTimeReported) { // still not start playback at device side
                        console.info("Infolink Player: 2010 BDP playback failure detected! Work-around action taken!");
                        self.pause();
                        self.resume();
                    } else {
                        console.info("Infolink Player: 2010 BDP playback success detected! Work-around action not taken.");
                    }
                }).done();
            }
        },
        /**
         * create the connection time out
         * @method  __createConnectionTimeout
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        __createConnectionTimeout: function () {
            console.info("Infolink Player: create connection timer");
            this.connectionTimer = core.getGuid();
            util.delay(this._connectionTimeLimit, this.connectionTimer).then(util.bind(function () {
                console.info("Infolink Player: connection timeout");
                this._onConnectionTimeout();
            }, this)).done();
        },
        /**
         * remove connection timeout
         * @method __removeConnectionTimeout
         * @private
         * @memberof ax/ext/device/samsung/InfolinkPlayer#
         */
        __removeConnectionTimeout: function () {
            //remove the connection time out when stop
            if (this.connectionTimer) {
                util.clearDelay(this.connectionTimer);
                console.info("Infolink Player: remove connection time out");
                this.connectionTimer = null;
            }
        }
    });
});
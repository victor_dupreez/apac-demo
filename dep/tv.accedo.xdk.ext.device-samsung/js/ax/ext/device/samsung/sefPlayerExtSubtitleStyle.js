/**
 * To handle the style from the tv subtitle profile
 *
 * @module ax/ext/device/samsung/sefPlayerExtSubtitleStyle
 */
define("ax/ext/device/samsung/sefPlayerExtSubtitleStyle", [
    "ax/console",
    "ax/ext/device/samsung/globals",
    "css!./css/SefPlayerExtSubtitleStyle"
], function (
    console,
    globals
) {
    "use strict";
    var profileSetting, subtitleContainer, getProfileSetting,
        updateColour, updateOpacity, updateFontSize, updateEdgeStyle, updateFontFamily, updateFontLang,
        currentStyle = {},
        updateClass;

    if (globals.webapis && globals.webapis.tv) {
        profileSetting = globals.webapis.tv.closedcaption;
    }

    /**
     * Get the profile setting from the device
     * @private
     * @method
     * @param {String} setting the id of the specific setting
     * @returns {String} the value of the setting.
     * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
     */
    getProfileSetting = function (setting) {
        var profile;

        profile = profileSetting["PROFILE_CC" + (setting.indexOf("EDGE") >= 0 ? "_FCC" : "")];

        return profileSetting.getClosedCaptionOption(profile, profileSetting["CAPTION_" + setting]);
    };
    /**
     * update the css class of subtitle container
     * @private
     * @method
     * @param {String} type the type of color, it can be FG (font color) or BG (background color)
     * @param {String} targetValue the target css value for the type
     * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
     */
    updateClass = function (type, targetValue) {
        var originalValue = currentStyle[type];

        if (originalValue === targetValue) {
            //do nothing when they are the same
            return;
        }

        //remove the current color
        if (originalValue) {
            subtitleContainer.removeClass(originalValue);
        }

        currentStyle[type] = targetValue;

        subtitleContainer.addClass(targetValue);
    };
    /**
     * get the caption color
     * @private
     * @method
     * @param {String} type the type of color, it can be FG (font color) or BG (background color)
     * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
     */
    updateColour = function (type) {
        var colour = getProfileSetting(type + "_COLOR"),
            colourStr = "";

        switch (colour) {
        case profileSetting.CAPTION_COLOR_WHITE:
            colourStr = "WHITE";
            break;
        case profileSetting.CAPTION_COLOR_BLACK:
            colourStr = "BLACK";
            break;
        case profileSetting.CAPTION_COLOR_RED:
            colourStr = "RED";
            break;
        case profileSetting.CAPTION_COLOR_GREEN:
            colourStr = "GREEN";
            break;
        case profileSetting.CAPTION_COLOR_BLUE:
            colourStr = "BLUE";
            break;
        case profileSetting.CAPTION_COLOR_YELLOW:
            colourStr = "YELLOW";
            break;
        case profileSetting.CAPTION_COLOR_MAGENTA:
            colourStr = "MAGENTA";
            break;
        case profileSetting.CAPTION_COLOR_CYAN:
            colourStr = "CYAN";
            break;
        default:
            colourStr = "DEFAULT";
            break;
        }
        updateClass(type + "_COLOR", type + "_COLOR_" + colourStr);
    };
    /**
     * get the caption opacity
     * @private
     * @method
     * @param {String} type the type of color, it can be FG (font color) or BG (background color)
     * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
     */
    updateOpacity = function (type) {
        var opacity = getProfileSetting(type + "_OPACITY"),
            opacityStr = "";

        switch (opacity) {
        case profileSetting.CAPTION_OPACITY_FLASH:
            opacityStr = "FLASH";
            break;
        case profileSetting.CAPTION_OPACITY_SOLID:
            opacityStr = "SOLID";
            break;
        case profileSetting.CAPTION_OPACITY_TRANSLUCENT:
            opacityStr = "TRANSLUCENT";
            break;
        case profileSetting.CAPTION_OPACITY_TRANSPARENT:
            opacityStr = "TRANSPARENT";
            break;
        default:
            opacityStr = "DEFAULT";
            break;
        }

        updateClass(type + "_OPACITY", type + "_OPACITY_" + opacityStr);
    };
    /**
     * get the caption color
     * @private
     * @method
     * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
     */
    updateFontSize = function () {
        var size = getProfileSetting("FONT_SIZE"),
            sizeStr = "";

        switch (size) {
        case profileSetting.CAPTION_SIZE_SMALL:
            sizeStr = "SMALL";
            break;
        case profileSetting.CAPTION_SIZE_LARGE:
            sizeStr = "LARGE";
            break;
        case profileSetting.CAPTION_SIZE_EXTRA_LARGE:
            sizeStr = "EXTRA_LARGE";
            break;
        case profileSetting.CAPTION_SIZE_STANDARD:
            sizeStr = "STANDARD";
            break;
        default:
            sizeStr = "DEFAULT";
            break;
        }

        updateClass("FONT_SIZE", "SIZE_" + sizeStr);
    };
    /**
     * update the edge style and set the css to the text
     * @private
     * @method
     * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
     */
    updateEdgeStyle = function () {
        var style = getProfileSetting("EDGE_TYPE"),
            colour = getProfileSetting("EDGE_COLOR"),
            styleStr = "",
            cssArr,
            currentEdge = currentStyle.EDGE_STYLE;

        switch (colour) {
        case profileSetting.CAPTION_COLOR_WHITE:
            colour = "white";
            break;

        case profileSetting.CAPTION_COLOR_BLACK:
            colour = "black";
            break;

        case profileSetting.CAPTION_COLOR_RED:
            colour = "red";
            break;

        case profileSetting.CAPTION_COLOR_GREEN:
            colour = "green";
            break;

        case profileSetting.CAPTION_COLOR_BLUE:
            colour = "blue";
            break;

        case profileSetting.CAPTION_COLOR_YELLOW:
            colour = "yellow";
            break;

        case profileSetting.CAPTION_COLOR_MAGENTA:
            colour = "magenta";
            break;

        case profileSetting.CAPTION_COLOR_CYAN:
            colour = "cyan";
            break;

        default:
            colour = "transparent";
            break;
        }

        switch (style) {
        case profileSetting.CAPTION_EDGE_RAISED:
            styleStr = "-1px -1px " + colour;
            break;
        case profileSetting.CAPTION_EDGE_DEPRESSED:
            styleStr = "1px 1px " + colour;
            break;
        case profileSetting.CAPTION_EDGE_DROP_SHADOWED:
            styleStr = "2px 2px " + colour;
            break;
        case profileSetting.CAPTION_EDGE_UNIFORM:
            cssArr = [];

            for (var x = -1; x <= 1; x++) {
                for (var y = -1; y <= 1; y++) {
                    cssArr.push(x + "px " + y + "px " + colour);
                }
            }

            styleStr = cssArr.join(",");
            break;
        }

        if (currentEdge === styleStr) {
            return;
        }

        currentStyle.EDGE_STYLE = styleStr;

        //since it is not possible to set via class, so set directly
        //it will set directly to the text div which is the first children in the container
        subtitleContainer.getChildren()[0].css("textShadow", styleStr);
    };
    /**
     * get the caption color
     * @private
     * @method
     * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
     */
    updateFontFamily = function () {
        var style = getProfileSetting("FONT_STYLE"),
            styleStr = "";

        switch (style) {
        case profileSetting.CAPTION_FONT_STYLE1:
            styleStr = "STYLE1";
            break;
        case profileSetting.CAPTION_FONT_STYLE2:
            styleStr = "STYLE2";
            break;
        case profileSetting.CAPTION_FONT_STYLE3:
            styleStr = "STYLE3";
            break;
        case profileSetting.CAPTION_FONT_STYLE4:
            styleStr = "STYLE4";
            break;
        case profileSetting.CAPTION_FONT_STYLE5:
            styleStr = "STYLE5";
            break;
        case profileSetting.CAPTION_FONT_STYLE6:
            styleStr = "STYLE6";
            break;
        case profileSetting.CAPTION_FONT_STYLE7:
            styleStr = "STYLE7";
            break;
        default:
            styleStr = "UNDEFINED";
            break;
        }

        updateClass("FONT_STYLE", "FONT_" + style);
    };
    /**
     * get the caption language
     * @private
     * @method
     * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
     */
    updateFontLang = function () {
        var lang = getProfileSetting("LANGUAGE_TYPE"),
            langStr = "";

        switch (lang) {
        case profileSetting.CAPTION_LANGUAGE_AUTO:
            langStr = "AUTO";
            break;
        case profileSetting.CAPTION_LANGUAGE_JPN:
            langStr = "JPN";
            break;
        case profileSetting.CAPTION_LANGUAGE_ENG:
            langStr = "ENG";
            break;
        case profileSetting.CAPTION_LANGUAGE_GER:
            langStr = "GER";
            break;
        case profileSetting.CAPTION_LANGUAGE_FRE:
            langStr = "FRE";
            break;
        case profileSetting.CAPTION_LANGUAGE_ITA:
            langStr = "ITA";
            break;
        case profileSetting.CAPTION_LANGUAGE_RUS:
            langStr = "RUS";
            break;
        case profileSetting.CAPTION_LANGUAGE_CHS:
            langStr = "CHS";
            break;
        case profileSetting.CAPTION_LANGUAGE_KOR:
            langStr = "KOR";
            break;
        case profileSetting.CAPTION_LANGUAGE_SPA:
            langStr = "SPA";
            break;
        default:
            langStr = "UNDEFINED";
            break;
        }

        updateClass("LANGUAGE_TYPE", "LANGUAGE_" + langStr);
    };

    return {
        /**
         * Set the container to decorate
         * @public
         * @method
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
         */
        setContainer: function (container) {
            if (!container) {
                console.info("invalid container and fail to update the style");
                return;
            }
            subtitleContainer = container;
        },
        /**
         * update the subtitle style
         * @public
         * @method
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
         */
        update: function () {
            if (!profileSetting) {
                console.info("uncessary to handle the profile setting");
                return;
            }


            updateFontFamily();
            updateFontSize();
            updateFontLang();

            updateEdgeStyle();

            updateColour("FG"); //font color
            updateColour("BG"); //background color

            updateOpacity("FG"); //font opacity
            updateOpacity("BG"); //background opacity
        },

        /**
         * Update the screen size
         * @public
         * @method
         * @param {Object} obj the new screen size object
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStyle
         */
        updateScreenSize: function (obj) {
            if (!subtitleContainer) {
                console.info("invalid container and fail to set the screen size");
                return;
            }

            if (obj.relativeToParent) {
                subtitleContainer.css("position", "relative");
                subtitleContainer.css("top", "0px");
                subtitleContainer.css("left", "0px");
            } else {
                subtitleContainer.css("position", "fixed");
                subtitleContainer.css("top", obj.top + "px");
                subtitleContainer.css("left", obj.left + "px");
            }

            subtitleContainer.removeClass("fullscreen");

            if (obj.isFullscreen) {
                subtitleContainer.addClass("fullscreen");
            }

            subtitleContainer.css("width", obj.width + "px");
            subtitleContainer.css("height", obj.height + "px");
        }
    };
});
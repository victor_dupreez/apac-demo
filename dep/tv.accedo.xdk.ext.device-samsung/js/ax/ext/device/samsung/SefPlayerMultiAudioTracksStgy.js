/**
 * Id class to handle the device information like firmware version
 * @class ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy
 * @augments ax/device/interface/MultipleAudioTrackStgy
 */
define("ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy", ["ax/class", "ax/console", "ax/device/interface/MultipleAudioTrackStgy", "ax/promise", "ax/core", "ax/util", "ax/exception"], function(klass, console, MultipleAudioTrackStgy, promise, core, util, exception) {
    "use strict";
    var SefMultiAudioTracks = klass.create([MultipleAudioTrackStgy], {}, {
        /**
         * player instance
         * @name __player
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy#
         */
        __player: null,
        /**
         * store the current audio track id
         * @name __currentAudioTrackId
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy#
         */
        __currentAudioTrackId: null,
        /**
         *
         * @method
         * @param {Object} player the player instance that will use this multi audio track strategy
         * @memberof ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy#
         */
        init: function(player) {
            //since it need the player state and do some checking and thus linking them.
            this.__player = player;
        },
        /**
         * Get ids of Audio Tracks. It will delay 10s when the info is not ready yet.
         * Tested on Samsung 2013 devices only which support playready.
         * @method
         * @public
         * @returns {Promise.<String[]>} A list of audio track object with id
         * @memberof ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy#
         */
        getAudioTracks: function() {
            console.info("[XDK SEF] getting the audio track information and current state of metadata: " + this.__player.__infoReady);

            if (!this.__player.__infoReady) {
                //since the data is not ready yet, then delay 10s to check again
                console.info("[XDK SEF] Delay 10s to get the audio track info again");
                return util.delay(10).then(util.bind(this.getAudioTracks, this));

            }

            var noOfAudioStream, audioTracksArr, i;

            console.info("[XDK SEF] Start getting the info for audio track");

            noOfAudioStream = this.__player._plugin.Execute("GetTotalNumOfStreamID", 1);

            console.info("[XDK SEF] " + noOfAudioStream + " audio track(s) is(are) found.");

            audioTracksArr = [];

            for (i = 0; i < noOfAudioStream; i++) {
                //it depends on the video itself.
                //convert the id into string to ensure the standard of string type id
                audioTracksArr.push(i.toString());

                console.info("[XDK SEF] Audio Track " + i);
            }

            console.info("[XDK SEF]Audio Tracks: " + audioTracksArr);

            return promise.resolve(audioTracksArr);

        },
        /**
         * Set audio track by id
         * @method
         * @public
         * @param {String} id The id of the audio track
         * @returns {Promise.<Boolean>} Return the result after call in a promise
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} wrong formated parameter.
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} player is not ready yet
         * @memberof ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy#
         */
        setAudioTrack: function(id) {
            if (!this.__player.__infoReady) {
                return promise.reject(core.createException(exception.UNSUPPORTED_OPERATION, "Sef player is not ready yet."));
            }

            this.__currentAudioTrackId = id;

            if (util.isString(id)) {
                id = parseInt(id, 10);
            }

            console.info("[XDK SEF]Set stream ID to: " + id);

            this.__player._plugin.Execute("SetStreamID", 1, id);
            return promise.resolve(true);
        },
        /**
         * Set audio track by id
         * @method
         * @public
         * @param {String} id The id of the audio track
         * @param {String} attr The media audio track attribute {@link ax/device/interface/MultipleAudioTrackStgy#LANGCODE}
         * @returns {Promise.<String>} Return the result of the property
         * @throws {Promise.<module:ax/exception.ILLEGAL_ARGUMENT>} wrong formated parameter.
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} player is not ready yet
         * @memberof ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy#
         */
        getAudioTrackAttr: function(id, attr, defaultValue) {
            if (!this.__player.__infoReady) {
                return promise.reject(core.createException(exception.UNSUPPORTED_OPERATION, "Sef player is not ready yet."));
            }

            if (util.isString(id)) {
                id = parseInt(id, 10);
            }

            switch (attr) {
                case MultipleAudioTrackStgy.LANGCODE:
                    //sample "eng|audio_eng"
                    return promise.resolve(this.__player._plugin.Execute("GetStreamLanguageCode", "1", id).split("|")[0]);
                default:
                    if (!util.isUndefined(defaultValue)) {
                        return promise.resolve(defaultValue);
                    }
            }
            //return reject if no available value for attr and no default Value
            return promise.reject(core.createException(exception.ILLEGAL_ARGUMENT, "No available attribute"));
        },
        /* Get current audio track id
         * @method getCurrentAudioTrackId
         * @public
         * @returns {Promise.<String|null>} The id of the audio track. Some audio track is unable to obtain the current track id, it will return null.
         * @throws {Promise.<module:ax/exception.UNSUPPORTED_OPERATION>} device is not ready yet
         * @memberof ax/ext/device/samsung/SefPlayerMultiAudioTracksStgy#
         */
        getCurrentAudioTrackId: function() {
            return promise.resolve(this.__currentAudioTrackId);
        }
    });
    return SefMultiAudioTracks;
});
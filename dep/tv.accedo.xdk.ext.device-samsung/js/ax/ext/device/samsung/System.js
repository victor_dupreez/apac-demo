/**
 * System class to handle the system api and connection checking
 *
 * @class ax/ext/device/samsung/System
 * @augments ax/device/AbstractSystem
 */
define("ax/ext/device/samsung/System", [
    "ax/class",
    "ax/device/AbstractSystem",
    "ax/device/shared/ConnectionPoller",
    "ax/ext/device/samsung/globals",
    "ax/console",
    "ax/util",
    "ax/promise",
    "ax/device/interface/System",
    "require"
], function (
    klass,
    abstrSystem,
    ConnectionPoller,
    global,
    console,
    util,
    promise,
    ISystem,
    require
) {
    "use strict";

    return klass.create(abstrSystem, {}, {
        /**
         * To save the status of the screen saver
         * @private
         * @name __screenSaverOn
         * @memberof ax/ext/device/samsung/System#
         */
        __screenSaverOn: true,
        /**
         *To check if it exiting to smart hub or tv source. If it is exiting to smart hub,it won't set TVsource to fullscreen
         * @private
         * @name __exitingToSmartHub
         * @memberof ax/ext/device/samsung/System#
         */
        __exitingToSmartHub: false,
        /**
         * To handle the exit issue
         * @private
         * @name __handleExitIssue
         * @memberof ax/ext/device/samsung/System#
         */
        __handleExitIssue: function (callback) {

            if (!callback) {
                callback = function () {
                    global.widgetAPI.sendReturnEvent();
                };
            }

            var okToExit = true;
            // var device = require("ax/device");
            //handle video player, make sure it is deinited
            //@todo
            /*
             if (!device.video.deinit()) {
             console.debug("[XDK] mediaPlayer not yet deinited");
             okToExit = false;
             }*/

            if (okToExit) {
                console.debug("[XDK] exit callback");
                util.delay(1).then(callback).done(); //Ho: allow time for it to deinit...
            } else {
                console.debug("[XDK] >>> not all exit issue get handle, waiting...");
                util.delay(1).then(util.bind(this.__handleExitIssue, this, callback)).done();
            }

        },
        /**
         * To set the screen saver
         * @public
         * @method setScreenSaver
         * @param {Boolean} turnOn True if turn on the screen saver, false when turn off.
         * @memberof ax/ext/device/samsung/System#
         */
        setScreenSaver: function (turnOn) {
            if (this.__screenSaverOn !== turnOn) {
                if (turnOn) {
                    console.debug("[XDK] >> Turn On Screen Saver");
                    global.pluginAPI.setOnScreenSaver();
                } else {
                    console.debug("[XDK] >> Turn Off Screen Saver");
                    global.pluginAPI.setOffScreenSaver();
                }
                this.__screenSaverOn = turnOn;
            } else {
                console.debug("[XDK] >> Screensaver status unchanged as " + (this.__screenSaverOn ? "ON" : "OFF"));
            }
        },
        /**
         * To set the system mute
         * @public
         * @method setSystemMute
         * @param {Boolean} turnOn True if turn on the mute, false when turn off.
         * @memberof ax/ext/device/samsung/System#
         */
        setSystemMute: function (turnOn) {
            if (global.audioPlugin.GetSystemMute() !== turnOn) {
                //Mute
                if (turnOn) {
                    global.audioPlugin.SetSystemMute(1);
                    console.debug("[XDK] >> Mute the TV");
                } else {
                    global.audioPlugin.SetSystemMute(0);
                    console.debug("[XDK] >> UnMute the TV");
                }
            } else {
                console.debug("[XDK] >> Screensaver status unchanged as " + (this.__screenSaverOn ? "ON" : "OFF"));
            }
        },
        /**
         * To power off the device
         * @public
         * @method powerOff
         * @memberof ax/ext/device/samsung/System#
         */
        powerOff: function () {
            this.__handleExitIssue(function () {
                // turn off the tv
                console.debug("[XDK] >> TURN OFF TV tvKey.KEY_POWER: " + global.tvKey.KEY_POWER);
                global.appCommon.SendKeyToTVViewer(global.tvKey.KEY_POWER);
            });
        },
        /**
         * To exit
         * @public
         * @param {Boolean} [param.toTV] Set to true if explicitly return to tv channel, false will return to where the app is launched from. Default: true
         * @method exit
         * @memberof ax/ext/device/samsung/System#
         */
        exit: function (param) {
            var toTV = true;
            if (param && typeof param.toTV === "boolean") {
                toTV = param.toTV;
            }

            this.__handleExitIssue(util.bind(function () {
                if (!toTV) {
                    this.__exitingToSmartHub = true;
                    console.debug(">> exit to where the app is launched from");
                    //only press smarthub btn will set the TV source to small screen
                    global.widgetAPI.sendReturnEvent();
                } else {
                    console.debug(">> exit to TV and channel");
                    global.widgetAPI.sendExitEvent();
                }
            }, this));
        },
        /**
         * Has mouse on samsung when the firmware year is larger than 2011. Some usb keyboard will have a mousepad or use usb.
         * @public
         * @method hasMouse
         * @memberof ax/ext/device/samsung/System#
         */
        hasMouse: function () {
            var device = require("ax/device");
            if (device && device.id) {
                if (device.id.getFirmwareYear() > 2011) {
                    return true;
                }
            }
            return false;
        },
        /**
         * To get the display resolution
         * @public
         * @method getDisplayResolution
         * @return {Object} object {width:1280,height:720}
         * @memberof ax/ext/device/samsung/System#
         */
        getDisplayResolution: function () {

            var innerWidth = window.innerWidth !== null ? window.innerWidth : document.body !== null ? document.body.clientWidth : null;


            if (innerWidth < 1270) {
                return {
                    width: 960,
                    height: 540
                };
            }

            if (innerWidth > 1290) {
                return {
                    width: 1920,
                    height: 1080
                };
            }

            return {
                width: 1280,
                height: 720
            };

        },
        /**
         * Samsung supports SSL
         * @public
         * @method supportSSL
         * @return {Boolean} True
         * @memberof ax/ext/device/samsung/System#
         */
        supportSSL: function () {
            return true;
        },
        /**
         * Samsung doesnt have same-origin policy (cross domain ajax restriction)
         * @public
         * @name hasSOP
         * @return {Boolean} False
         * @memberof ax/ext/device/samsung/System#
         */
        hasSOP: function () {
            return false;
        },
        /**
         * Get the current network status from the device API
         * @method getNetworkStatus
         * @return {Promise.<Boolean>}  Return true if it is connected to internet.
         * @public
         * @memberof ax/ext/device/samsung/System#
         */
        getNetworkStatus: function () {
            return promise.resolve(this._isInternetConnected());
        },
        /**
         * override the abstract system network status checking and use the connect poller for checking the status via isInternetConnected function
         * @method _postInit
         * @protected
         * @memberof ax/ext/device/samsung/System#
         */
        _postInit: function () {
            var checkStatus, onStatusChange;

            checkStatus = util.bind(function (cbDone) {
                if (this._isInternetConnected()) {
                    cbDone(true);
                } else {
                    cbDone(false);
                }
            }, this);

            //the checking callback
            onStatusChange = util.bind(function (status) {
                this.dispatchEvent(ISystem.EVT_NETWORK_STATUS_CHANGED, status);
            }, this);

            this._internetPoller = new ConnectionPoller(checkStatus, onStatusChange, {
                networkStatus: this._isInternetConnected()
            });
        },
        /**
         * Check if the the device is connected to internet
         *
         * @method
         * @return {Boolean} true if the device is connected to lan and both internet by the device api
         * @protected
         * @memberof ax/ext/device/samsung/System#
         */
        _isInternetConnected: function () {
            if (this.__isEmulator()) {
                //emulator as unable to determine the network status on emulator
                return true;
            }

            var connection = false;
            var type = global.networkPlugin.GetActiveType();

            if (type === -1) {
                return false;
            }

            // if the network is not connected physically, internet shouldn't be connected
            if (!this._isLANConnected()) {
                return false;
            }

            try {
                connection = global.networkPlugin.CheckGateway(type) > 0;
                console.debug("[XDK] isInternetConencted:" + connection);
            } catch (e) {
                console.warn("unable to detect the internet Connection" + e);
            }

            return connection;
        },
        /**
         * is physical Lan connected
         * @method _isLANConnected
         * @return {Boolean} true if it is connected
         * @protected
         * @memberof ax/ext/device/samsung/System#
         */
        _isLANConnected: function () {
            if (this.__isEmulator()) {
                //emulator as unable to determine the network status on emulator
                return true;
            }

            var connection = false;
            var type = global.networkPlugin.GetActiveType();

            if (type === -1) {
                return false;
            }

            try {
                connection = global.networkPlugin.CheckPhysicalConnection(type) > 0;
            } catch (e) {
                console.warn("unable to detect the lan Connection" + e);
            }

            return connection;
        },

        /**
         * Checks if the running device is an emulator.
         *
         * @method
         * @private
         * @returns {Boolean} True if it is an emulator, false otherwise
         * @memberof ax/ext/device/samsung/System#
         */
        __isEmulator: function () {
            return window.location.search.indexOf("product=") < 0;
        }
    });
});
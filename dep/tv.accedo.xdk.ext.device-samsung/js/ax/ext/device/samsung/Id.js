/**
 * Id class to handle the device information like firmware version
 * @class ax/ext/device/samsung/Id
 * @extends ax/device/AbstractId
 */
define("ax/ext/device/samsung/Id", [
    "ax/class",
    "ax/device/AbstractId",
    "ax/device/interface/Id",
    "ax/util",
    "ax/console",
    "ax/ext/device/samsung/globals",
    "require"
], function (
    klass,
    abstrId,
    IId,
    util,
    console,
    globals,
    require) {
    "use strict";
    return klass.create(abstrId, [IId], {}, {
        /**
         * To store the mac address
         * @private
         * @memberof ax/ext/device/samsung/Id#
         */
        __mac: null,
        /**
         * To store the firmware
         * @private
         * @memberof ax/ext/device/samsung/Id#
         */
        __firmware: null,
        /**
         * To store widevine SDI ID
         * @private
         * @memberof ax/ext/device/samsung/Id#
         */
        __widevineSDIID: null,
        /**
         * To save widevine ESN
         * @private
         * @memberof ax/ext/device/samsung/Id#
         */
        __widevineESN: null,
        /**
         * To save device type
         * @private
         * @deprecated replaced with __hardwareType
         * @memberof ax/ext/device/samsung/Id#
         */
        __deviceType: null,
        /**
         * To save hardware type
         * @private
         * @memberof ax/ext/device/samsung/Id#
         */
        __hardwareType: null,
        init: function () {
            require(["ax/ext/device/samsung/PrivilegedAPI"], util.bind(function (api) {
                //default value
                this.__widevineESN = "SSBDPE000793631B";
                this.__widevineSDIID = api._getSDIID();

                var firmwareStr = parseInt(this.getFirmware().substr(this.getFirmware().lastIndexOf("-") + 1), 10);
                if (this.getFirmwareYear() > 2010 || (firmwareStr >= 1003 && this.getHardwareType() !== IId.HARDWARE_TYPE.TV)) {
                    this.__widevineESN = api._getESN();
                } else {
                    console.debug("[XDK] no value widevineESN fine. Mac address would be used");
                    this.__widevineESN = this.getMac();
                }

                if (!util.isString(this.__widevineESN) || this.__widevineESN.length > 16) {
                    this.__widevineESN = this.getMac();
                }


                console.debug("[XDK] widevine SDIID " + this.__widevineSDIID);
                console.debug("[XDK] widevine ESN " + this.__widevineESN);
            }, this), function (err) {
                console.error("[XDK] Unable to load the privillegedAPI file" + err);
            });
        },
        /**
         * To return the device type as "samsung"
         * @method getDeviceType
         * @public
         * @deprecated replaced with getHardwareType
         * @return {String} type of the device e.g Bluray, TV
         * @memberof ax/ext/device/samsung/Id#
         */
        getDeviceType: function () {

            if (this.__deviceType) {
                return this.__deviceType;
            }

            var deviceType = "";

            console.debug("[XDK] get the Device Type" + window.location);
            if (window.location.search.indexOf("product=") === -1) {
                deviceType = "emulator";
            } else {
                try {
                    var str = window.location.search[window.location.search.indexOf("product=") + 8],
                        productType;
                    switch (str) {
                    case "2":
                        try {
                            productType = globals.tvPlugin.GetBDProductType();
                            /*
                                 0 - PL_TV_BDPRODUCT_TYPE_UNKNOWN
                                 1 - PL_TV_BDPRODUCT_TYPE_SINGLE
                                 2 - PL_TV_BDPRODUCT_TYPE_HTS
                                 3 - PL_TV_BDPRODUCT_TYPE_HDD
                                 */
                            if (productType === 2) {
                                deviceType = "Home Theatre";
                            } else {
                                deviceType = "Bluray";
                            }
                        } catch (err) {
                            console.warn("Unable to get the product type error: " + err);
                            deviceType = "Bluray"; //if error, regard the device as bd
                        }
                        break;
                    case "1":
                        deviceType = "Monitor";
                        break;
                    default:
                        deviceType = "TV";
                        break;
                    }
                } catch (e) {
                    console.warn("Unable to get the product type error: " + e);
                    return false;
                }
            }

            this.__deviceType = deviceType;
            return this.__deviceType;
        },
        /**
         * To return the hardware type
         * @method getHardwareType
         * @public
         * @return {ax/device/interface/Id.HARDWARE_TYPE} type of the hardware {@link ax/device/interface/Id.HARDWARE_TYPE.BD} or
          {@link ax/device/interface/Id.HARDWARE_TYPE.HOME_THEATRE} or 
          {@link ax/device/interface/Id.HARDWARE_TYPE.TV} or 
          {@link ax/device/interface/Id.HARDWARE_TYPE.MONITOR} or 
          {@link ax/device/interface/Id.HARDWARE_TYPE.EMULATOR}
         * @memberof ax/ext/device/samsung/Id#
         */
        getHardwareType: function () {

            if (this.__hardwareType) {
                return this.__hardwareType;
            }

            var hardwareType = "";

            console.debug("[XDK] get the Device Type" + window.location);
            if (window.location.search.indexOf("product=") === -1) {
                hardwareType = IId.HARDWARE_TYPE.EMULATOR;
            } else {
                try {
                    var str = window.location.search[window.location.search.indexOf("product=") + 8],
                        productType;
                    switch (str) {
                    case "2":
                        try {
                            productType = globals.tvPlugin.GetBDProductType();
                            /*
                                 0 - PL_TV_BDPRODUCT_TYPE_UNKNOWN
                                 1 - PL_TV_BDPRODUCT_TYPE_SINGLE
                                 2 - PL_TV_BDPRODUCT_TYPE_HTS
                                 3 - PL_TV_BDPRODUCT_TYPE_HDD
                                 */
                            if (productType === 2) {
                                hardwareType = IId.HARDWARE_TYPE.HOME_THEATRE;
                            } else {
                                hardwareType = IId.HARDWARE_TYPE.BD;
                            }
                        } catch (err) {
                            console.warn("Unable to get the product type error: " + err);
                            hardwareType = IId.HARDWARE_TYPE.BD; //if error, regard the device as bd
                        }
                        break;
                    case "1":
                        hardwareType = IId.HARDWARE_TYPE.MONITOR;
                        break;
                    default:
                        hardwareType = IId.HARDWARE_TYPE.TV;
                        break;
                    }
                } catch (e) {
                    console.warn("Unable to get the product type error: " + e);
                    return false;
                }
            }

            this.__hardwareType = hardwareType;

            return this.__hardwareType;
        },
        /**
         * return the mac address
         * @method getMac
         * @return {String} mac address
         * @memberof ax/ext/device/samsung/Id#
         * @public
         */
        getMac: function () {
            if (this.__mac) {
                return this.__mac;
            }

            //no mac address
            if (window.location.toString().indexOf("product=") === -1) {
                return null;
            } else {
                globals.networkPlugin.CreatePlugin();
                var type = globals.networkPlugin.GetActiveType();
                //1 if active interface is WIRED,
                //0 if active interface is WIRELESS,
                //-1 if there is no active connection
                if (type > -1) {
                    this.__mac = globals.networkPlugin.GetMAC(type);
                    return this.__mac;
                }
            }
            return null;
        },
        /**
         * To get the firmware version
         * @method getFirmware
         * @public
         * @return {String} the firmware of the devices
         * @memberof ax/ext/device/samsung/Id#
         */
        getFirmware: function () {
            if (this.__firmware) {
                return this.__firmware;
            }
            var firmwareStr;
            try {
                firmwareStr = globals.NNaviPlugin.GetFirmware();
                firmwareStr = util.strip(firmwareStr);
            } catch (e) {
                console.warn("Unable to get the firmware error" + e);
            }

            if (!util.isUndefined(firmwareStr) && firmwareStr.length > 0) {
                this.__firmware = firmwareStr;
            } else {
                this.__firmware = "dummyFirmware";
            }
            return this.__firmware;
        },
        /**
         * return the firmware year
         * @method getFirmwareYear
         * @returns {Number|Boolean} the firmware year, otherwise it will return false
         * @memberof ax/ext/device/samsung/Id#
         * @public
         */
        getFirmwareYear: function () {
            var str = this.getFirmware(),
                year;

            if (!util.isUndefined(str)) {
                year = parseInt(str.substr(10, 4), 10);
                return isNaN(year) ? 0 : year;
            }
            return false;
        },
        /**
         * To get the unique ID. If no unique id.if will generate a UUID by XDK
         * @method getUniqueID
         * @public
         * @returns {String} the the duid from the samsung, otherwise it will return the uuid
         * @memberof ax/ext/device/samsung/Id#
         */
        getUniqueID: function () {

            if (this.getHardwareType() === IId.HARDWARE_TYPE.EMULATOR) {
                return "dummyUniqueId";
            }

            var mac = this.getMac();
            console.debug("[XDK] get Mac" + mac);
            try {
                return globals.NNaviPlugin.GetDUID(mac);
            } catch (e) {
                console.debug("[XDK] fail to get DUID from the device and use UUID");
                return this._super();
            }
        },
        /**
         * To get the model of the device. If no related information. "dummySamsungmodel" will be returned
         * @method getModel
         * @public
         * @returns {String} the model numebr
         * @memberof ax/ext/device/samsung/Id#
         */
        getModel: function () {
            var model = globals.tvPlugin.GetProductCode(0);
            if (model) {
                return model;
            }
            return "dummySamsungmodel";
        },
        /**
         * To get IP address
         * @method getIP
         * @public
         * @return {String} the IP address
         * @memberof ax/ext/device/samsung/Id#
         */
        getIP: function () {
            if (this.getFirmwareYear() > 2010) {
                var ip = globals.networkPlugin.GetHostAddr();
                if (ip) {
                    return ip;
                }
            } else {
                console.warn("[XDK] IP function is not supported in 2010 devices");
            }
            return "0.0.0.0";
        },
        /**
         * To get the widevine SDI ID
         * @method getWidevineSDIID
         * @public
         * @return {String} the widevine sdi id
         * @memberof ax/ext/device/samsung/Id#
         */
        getWidevineSDIID: function () {
            return this.__widevineSDIID;
        },
        /**
         * To get the widevine ESN
         * @method getWidevineESN
         * @public
         * @return {String} the widevine ESN
         * @memberof ax/ext/device/samsung/Id#
         */
        getWidevineESN: function () {
            return this.__widevineESN;
        }
    });
});
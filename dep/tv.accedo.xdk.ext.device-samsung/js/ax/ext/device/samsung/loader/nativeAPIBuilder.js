/**
 * A dummy builder of samsung API loader plugin that will be used instead of the original plugin during optimization.  
 * @module ax/ext/device/samsung/nativeApiBuilder
 * @ignore
 */
define(function () {
    "use strict";
    return {
        load: function (resourceId, require, load) {
            load(true);
        }
    };
});

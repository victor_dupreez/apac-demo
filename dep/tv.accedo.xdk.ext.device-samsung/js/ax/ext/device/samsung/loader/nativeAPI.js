/**
 * A native API loader plugin that is responsible to load Samsung API objects.
 * Parameters can be specified in the definition separated using pipe character (|) to control the load.
 * Configurable behavior include locateAPI where it won't locate the API directly by accessing the window.
 *
 * e.g `$MANAGER_WIDGET/Common/API/Widget` will be automatically loaded and return the plugin window.Common.API.Widget.
 *
 * However, some samsung native apis don't follow the same practice.
 * e.g `$MANAGER_WIDGET/Common/webapi/1.0/webapis` is not following the rule, it will load as window.webapis instead of `window.Common.webapis.1.0.0.webapis`.
 * So empty object(indicate the library is loaded) will be returned and developers need to map themselves the module.
 *
 * To disable locateAPI, specify locateAPI=false. [default will be true]
 * @example
 * // load a samsung native api
 * require(["ax/ext/device/samsung/loader/nativeAPI!locateAPI=false|$MANAGER_WIDGET/Common/webapi/1.0/webapis"], function () {
 *     // map the webapis apis
 *     globals.webapis = window.webapis;
 * });
 *
 * @module ax/ext/device/samsung/loader/nativeAPI
 */
define("ax/ext/device/samsung/loader/nativeAPI", [
    "ax/ext/device/samsung/detection",
    "ax/QueryString",
    "ax/util"
], function (
    detection,
    QueryString,
    util
) {
    "use strict";
    var exports = {
        /**
         * If the plugin has a dynamic property set to true, then it means
         * the loader MUST NOT cache the value of a normalized plugin dependency,
         * instead call the plugin's load method for each instance of a plugin dependency.
         * @property {Boolean} dynamic
         * @memberof module:ax/ext/device/samsung/loader/nativeAPI
         */
        dynamic: false,

        /**
         * pluginBuilder is a string that points to another module to use instead of the current plugin when the plugin is used as part of an optimizer build.
         * Without this property, the optimization WILL fail.
         * @see {@link http://requirejs.org/docs/plugins.html#apipluginbuilder}
         * @property {String} pluginBuilder
         * @memberof module:ax/ext/device/samsung/loader/nativeAPI
         */
        pluginBuilder: "./nativeAPIBuilder"

    };

    var modules = {},
        // delimiter for loader parameter
        PARAM_DELIMITER = "|";

    /**
     * Instantiate the API object.
     * @private
     * @method
     * @param {String} name The API name
     * @returns {Object} The newly created API object
     * @memberof module:ax/ext/device/samsung/loader/nativeAPI
     */

    function locateAPI(name) {
        var parts,
            obj = window;

        // remove the place holder of the API name
        name = name.replace("$MANAGER_WIDGET/", "");

        parts = name.split("/");
        while (parts.length > 0 && obj) {
            obj = obj[parts.shift()];
        }

        // obj could be the actual API object or undefined
        return obj;
    }

    /**
     * Load a resource.
     * Assuming the resource IDs do not need special ID normalization.
     * @method
     * @param {String} resourceId The resource ID that the plugin should load. This ID MUST be normalized.
     * @param {Function} require A local require function. Will not be used.
     * @param {Function} load A function to call once the value of the resource ID has been determined.
     * @param {Object} [opts] Option
     * @param {Function} [opts.onFailure] failure callback
     * @memberof module:ax/ext/device/samsung/loader/nativeAPI
     */
    exports.load = function (resourceId, require, load, opts) {
        var loadOption = parseResourceId(resourceId);
        resourceId = loadOption.id;

        //Avoiding running this loading modules in optimized(concat) version
        if (!detection) {
            load({});
            return;
        }

        if (modules[resourceId]) {
            load(modules[resourceId]);
            return;
        }

        var path = resourceId;
        if (resourceId.lastIndexOf(".js") < (resourceId.length - 3)) {
            path = resourceId + ".js";
        }

        // require a *.js, no path resolving should be done
        // expecting nothing from the callback as the native API is not a module but a script with global object(s)
        require([path], function () {
            var apiObject = {};
            //locate the api object when setting true.
            if (loadOption.locateAPI) {
                apiObject = locateAPI(resourceId);
            }
            modules[resourceId] = apiObject;
            load(modules[resourceId]);
        }, opts.onFailure);
    };

    /**
     * Parse the "resource id", get the locateAPI config and the real resource id from it.
     * @method
     * @private
     * @param {String} resourceId The resource id
     * @returns {Object} The parsed object, containing the resource id and the parameters
     * @memberof module:ax/ext/device/samsung/loader/nativeAPI
     */
    function parseResourceId(resourceId) {
        var tokens = resourceId.split(PARAM_DELIMITER),
            param;

        // no delimiter in the resource id, no more process
        if (tokens.length === 1) {
            return {
                id: resourceId,
                locateAPI: true
            };
        }

        param = parseLoadingOption(tokens[0]);

        return util.extend({
            id: tokens[1]
        }, param);
    }

    /**
     * Parse the loading option.
     * @method
     * @private
     * @param {String} queryString The parameter list in the loader dependency
     * @returns {Object} The parsed object, containing 1 property: locateAPI (Boolean)
     * @memberof module:ax/ext/device/samsung/loader/nativeAPI
     */
    function parseLoadingOption(queryString) {
        var param = QueryString.parse(queryString);

        param.locateAPI = param.locateAPI === "true";

        return param;
    }


    return exports;
});
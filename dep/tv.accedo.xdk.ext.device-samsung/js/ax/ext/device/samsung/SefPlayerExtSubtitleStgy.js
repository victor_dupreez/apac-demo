/**
 * SefPlayer external subtitle strategy
 *
 * Supported type: "sami" and "smpte-tt"
 *
 * SefPlayer external subtitle will create a div which display the subtitle and show on the screen. Developer
 * can customize the style of the div themselves.
 * Class name will be "sefplayer external-subtitle" and it will appended under the sefPlayer container("SefPlayerContainer")
 *
 *###Dom Structure
 *      <div class="sefplayer external-subtitle">
 *          <div class="text">
 *              Sample Subtitle
 *          </div>
 *      </div>
 *
 * @class ax/ext/device/samsung/SefPlayerExtSubtitleStgy
 */
define("ax/ext/device/samsung/SefPlayerExtSubtitleStgy", [
    "ax/class",
    "ax/device/interface/ExtSubtitleStgy",
    "ax/core",
    "ax/exception",
    "ax/console",
    "ax/promise",
    "ax/util",
    "ax/Element",
    "ax/ext/device/samsung/globals",
    "ax/device/Media",
    "ax/ext/device/samsung/sefPlayerExtSubtitleStyle"
], function (
    klass,
    IExtSubtitleStgy,
    core,
    exception,
    console,
    promise,
    util,
    Element,
    globals,
    Media,
    subtitleStyle
) {
    "use strict";
    var sMedia = Media.singleton();
    
    return klass.create([IExtSubtitleStgy], {}, {
        /**
         * The player instance
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __player: null,
        /**
         * Array to hold the subtitle information
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __subtitleArr: [],
        /**
         * Whether prepared or not for the subtitle. True when it is downloaded
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __preparedSubtitle: false,
        /**
         * The current Id of subtitle track
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __currentId: -1,
        /**
         * The file path of the subtitle
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __file: [],
        /**
         * the subtitle html
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __subtitleDiv: null,
        /**
         * the subtitle div status
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __showingSubtitle: false,
        /**
         * the download plugin container
         * @private
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __downloadPluginContainer: null,
        /**
         * Prepare for the subtitle, like download the external source or embedded the external subtitle into the player
         * @throws {module:ax/exception.ILLEGAL_STATE} when there is preloabable but not exist method to get the player object
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         * @method
         * @public
         */
        prepare: function (subtitleOption) {
            //embedded the subtitle into the video object
            this.__subtitleArr = [];

            //only need to prepare for exist subtitleOption
            if (!subtitleOption) {
                return;
            }

            this.__playerObj = globals.sefPlugin;

            this.__downloadObj = globals.download;
            this.__downloadObj.Open("Download", "1.000", "Download");

            //append the subtitle div same level with the sefplayer
            this.__subtitleContainer = new Element("div", null, this.__playerObj.parentNode);
            this.__subtitleContainer.addClass("sefplayer");
            this.__subtitleContainer.addClass("external-subtitle");
            this.__subtitleContainer.hide();

            this.__subtitleText = new Element("div", null, this.__subtitleContainer);
            this.__subtitleText.addClass("text");

            subtitleStyle.setContainer(this.__subtitleContainer);

            this.__onSubtitleHandler = util.bind(this.__onSubtitle, this);

            this.__player.addEventListener(this.__player.constructor.EVT_SUBTITLE, this.__onSubtitleHandler);

            this.__onScreenSizeChangeHandler = util.bind(this.__onScreenSizeChange, this);
            this.__player.addEventListener(this.__player.constructor.EVT_SCREEN_SIZE_CHANGED, this.__onScreenSizeChangeHandler);

            this.__onStateChangeHandler = util.bind(this.__onStateChange, this);

            sMedia.addEventListener(sMedia.EVT_STATE_CHANGED, this.__onStateChangeHandler);

            var capacity = this._getCapability();
            util.each(subtitleOption, util.bind(function (opts, i) {

                var file, downloadObj, downloadPlugin;

                if (capacity.indexOf(opts.type) === -1) {
                    console.info("SEFPlayer doesn't support " + opts.type + " subtitle format");
                    return;
                }

                //download the file to the $TEMP
                file = "$TEMP/" + opts.url.replace(/^.*[\\\/]/, "");

                //since 2013/2014 TV no idea fail to download the second file,
                //so create a new plugin each time to obtain the file

                //create a dummy container for the 
                if (!this.__downloadPluginContainer) {
                    this.__downloadPluginContainer = document.createElement("div");
                    document.body.appendChild(this.__downloadPluginContainer);
                }

                downloadObj = document.createElement("div");
                this.__downloadPluginContainer.appendChild(downloadObj);

                downloadObj.innerHTML = "<object border=0 classid='clsid:SAMSUNG-INFOLINK-SEF' style='position:absolute;width:0px;height:0px'></object>";

                downloadPlugin = downloadObj.childNodes[0];
                downloadPlugin.Open("Download", "1.000", "Download");
                downloadPlugin.Execute("StartDownFile", opts.url, file, 10, 5);


                console.info("Downloading the subtitle" + opts.url);

                this.__file.push(file);

                //prepare for the list
                this.__subtitleArr.push({
                    id: this.__subtitleArr.length,
                    type: IExtSubtitleStgy.TYPE,
                    lang: opts.lang,
                    tag: opts.tag
                });

            }, this));

            this.__preparedSubtitle = true;
        },
        /**
         * Listen to player screen size change
         * @private
         * @method
         * @param {Object} obj the new screen size object
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __onScreenSizeChange: function (obj) {
            subtitleStyle.updateScreenSize(obj);
        },
        /**
         * Handle the subtitle when state change
         * @private
         * @method
         * @param {Object} stateObj the state change
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __onStateChange: function (stateObj) {
            if (!this.__showingSubtitle) {
                return;
            }

            //clean up the subtitle text
            if (stateObj.toState === sMedia.STOPPED) {
                this.__subtitleText.setInnerHTML("");

                //hide the subtitle and reset the id
                this.__showDiv(false);
                this.__currentId = -1;
            }
        },

        /**
         * Listen to player onSubtitle event and set on the div
         * @private
         * @method
         * @param {String} param the subtitle text from the player
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __onSubtitle: function (param) {
            console.info("Sefplayer external subtitle receive subtitle info " + param);
            if (!this.__showingSubtitle) {
                //do nothing
                return;
            }

            subtitleStyle.update();

            this.__subtitleText.setInnerHTML(param);
        },
        /**
         * Show or hide the subtitle div
         * @param {Boolean} enable True when show div.
         * @private
         * @method
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        __showDiv: function (enable) {
            if (enable) {
                this.__showingSubtitle = true;
                this.__subtitleContainer.show();
                return;
            }

            this.__showingSubtitle = false;
            this.__subtitleContainer.hide();
        },
        /**
         * The capable type for the external subtitle
         * @protected
         * @method
         * @returns {String[]} the format of possible subtitle.
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        _getCapability: function () {
            return ["sami", "smpte-tt"];
        },

        /**
         * Set the player and preloadable information
         * @method
         * @param {ax/device/interface/Player} player the player
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        setPlayer: function (player) {
            this.__player = player;
        },
        /**
         * show the substitle.
         * @param {String} id the subtitle id
         * @returns {Promise.<Boolean>} the result of showing substitle. True when successfully loaded.
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} when fail to show non-existing subtitle
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         * @method
         * @public
         */
        showSubtitle: function (id) {
            //just show the div
            this.__showDiv(true);

            if (this.__currentId === id) {
                return promise.resolve(false);
            }

            this.__currentId = id;

            this.__playerObj.Execute("StartSubtitle", this.__file[id]);
            this.__playerObj.Execute("SetStreamID", 5, 0);

            return promise.resolve(true);
        },
        /**
         * hide the subtitle
         * @method
         * @returns {Promise.<Boolean> the result of hiding subtitle. True when succesfully hided.
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} No available subtitle
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         * @public
         */
        hideSubtitle: function () {
            this.__showDiv(false);
            return promise.resolve(true);
        },
        /**
         * get the current subtitle
         * @return {Promise.<ax/device/Subtitle.Subtitle>} The current substitle information
         * @throws {Promise.<module:ax/exception.ILLEGAL_STATE>} No available subtitle
         * @method
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         * @public
         */
        getCurrentSubtitle: function () {
            return promise.resolve(this.__subtitleArr[this.__currentId]);

        },
        /**
         * get all the available subtitle
         * @return {Promise.<ax/device/Subtitle.subsitle[]>} the available substitle array
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         * @method
         * @public
         */
        getSubtitles: function () {
            return promise.resolve(this.__subtitleArr);
        },
        /**
         * deinit the external subtitle
         * @method
         * @protected
         * @memberof ax/ext/device/samsung/SefPlayerExtSubtitleStgy#
         */
        deinit: function () {
            if (!this.__preparedSubtitle) {
                return;
            }

            //remove the subtitle files
            var i, filesNo = this.__file.length;

            for (i = 0; i < filesNo; i++) {
                this.__downloadObj.Execute("Delete", filesNo[i]);
            }

            //remove back the download plugin container used on 2013.
            if (this.__downloadPluginContainer) {
                this.__downloadPluginContainer.parentNode.removeChild(this.__downloadPluginContainer);
                this.__downloadPluginContainer = null;
            }

            //remove the subtitle container
            if (this.__subtitleContainer) {
                this.__subtitleContainer.discard();
                this.__subtitleContainer = null;
            }

            sMedia.removeEventListener(sMedia.EVT_STATE_CHANGED, this.__onStateChangeHandler);

            this.__player.removeEventListener(this.__player.constructor.EVT_SUBTITLE, this.__onSubtitleHandler);
            this.__player.removeEventListener(this.__player.constructor.EVT_SCREEN_SIZE_CHANGED, this.__onScreenSizeChangeHandler);

            this.__preparedSubtitle = false;
        }
    });
});
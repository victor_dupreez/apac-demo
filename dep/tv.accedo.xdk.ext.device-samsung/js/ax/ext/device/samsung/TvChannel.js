/**
 * To provide the tv channel usage in samsung device
 * @class ax/ext/device/samsung/TvChannel
 * @fires CHANNEL_CHANGED tv channel changed
 */
define("ax/ext/device/samsung/TvChannel", [
    "ax/console",
    "ax/ext/device/samsung/globals",
    "ax/util",
    "ax/Env",
    "ax/class",
    "ax/EventDispatcher",
    "require"
], function (
    console,
    globals,
    util,
    Env,
    klass,
    EventDispatcher,
    require) {
    "use strict";

    function _getDeviceAPI(api) {
        var device = require("ax/device");
        return device[api];
    }

    function _getFirmwareYear() {
        return _getDeviceAPI("id").getFirmwareYear();
    }
    var sEnv = Env.singleton(),
        PL_WINDOW_CHANNEL_TYPE_DTV = 3,
        PL_WINDOW_TV_MODE_AIR = 1,
        // Air TV
        PL_WINDOW_TV_MODE_CABLE = 2,
        // Cable TV
        PL_TV_EVENT_PROGRAM_CHANGED = 204,
        PL_TV_EVENT_CHANNEL_CHANGED = 113,
        PL_TV_EVENT_TUNE_SUCCESS = 103,
        PL_TV_LANGUAGE_HKG = 28,
        PL_TV_LANGUAGE_TPE = 29,
        TvChannel = klass.create(EventDispatcher, {
            /**
             * Dispatched when the tv channel changed
             * @event
             * @memberof ax/ext/device/samsung/TvChannel
             */
            CHANNEL_CHANGED: "device:evt:channel-changed",
            /**
             * Static reference of singleton instance
             * @private
             * @function
             * @memberof ax/ext/device/samsung/TvChannel
             */
            __instance: null,
            /**
             * Dispatched when the tv channel changed
             * @function
             * @memberof ax/ext/device/samsung/TvChannel
             */
            singleton: function () {

                if (this.__instance === null) {
                    this.__instance = new TvChannel();
                }

                return this.__instance;
            }

        }, {
            /**
             * save the status of the tv
             * @function __tvOn
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __tvOn: false,
            /**
             * save the current channel info
             * @function __currentChannelInfo
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __currentChannelInfo: {},
            /**
             * save the channel list info
             * @function __channelList
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __channelList: [],
            /**
             * save the channel order by number major eg [11,12,13,14,81,82,83]
             * @function __channelOrder
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __channelOrder: [],
            /**
             * store the current channel index which will be used to get the related info inside the channelList
             * @function __currentChannelIndex
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __currentChannelIndex: -1,
            /**
             * to store the original source
             * @function __originalSource
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __originalSource: null,
            init: function () {
                console.info("tv channel init");
                this.__timePlugin = globals.timePlugin;
                this.__windowPluginNew = globals.mwPlugin2;
                this.__windowPluginDeprecated = globals.mwPlugin;
                this.__windowPlugin = this.__windowPluginNew;

                this.getChannelList();

                globals.tvPlugin.SetEvent(PL_TV_EVENT_CHANNEL_CHANGED);
                globals.tvPlugin.SetEvent(PL_TV_EVENT_TUNE_SUCCESS);
                globals.tvPlugin.SetEvent(PL_TV_EVENT_PROGRAM_CHANGED);

                window.tvChannelOnEvent = util.bind(this.__tvChannelOnEvent, this);
                globals.tvPlugin.OnEvent = "window.tvChannelOnEvent";

                sEnv.addEventListener(sEnv.EVT_ONUNLOAD, util.bind(function () {

                    var system = require("ax/device").system;
                    console.info("[XDK] receive unload and checking the system info" + system.__exitingToSmartHub);
                    if (!system.__exitingToSmartHub) {
                        console.info("[XDK] samsung onunload: back to tv source");
                        //tvChannel set to fullscreen
                        this.setFullscreen();
                    }
                }, this));

            },
            /**
             * Handle the event from the tv
             * @function  __tvChannelOnEvent
             * @param {Object} id the id of the event
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __tvChannelOnEvent: function (id) {
                console.info("samsung tv _onEvent: " + id);
                switch (parseInt(id, 10)) {
                case PL_TV_EVENT_TUNE_SUCCESS:
                case PL_TV_EVENT_PROGRAM_CHANGED:
                case PL_TV_EVENT_CHANNEL_CHANGED:
                    this.getChannelInfo();
                    this.dispatchEvent(this.constructor.CHANNEL_CHANGED, this.__currentChannelInfo);
                    break;
                }
            },
            /**
             * To handle chanel up
             * @function chUp
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            chUp: function () {
                var currentCh, targetCh;
                if (!this.__tvOn) {
                    return;
                }
                //in analog mode, no channel will be found, so try to change the major only
                if (this.__channelList.length === 0) {
                    currentCh = this.getChannelInfo();
                    if (currentCh && currentCh.major) {
                        //according to the tutorial , analog signal is from 1 to 135
                        targetCh = {
                            major: (currentCh.major === 135) ? 1 : currentCh.major + 1,
                            minor: currentCh.minor
                        };
                    }
                } else {
                    if (this.__currentChannelIndex < this.__channelList.length - 1) {
                        this.__currentChannelIndex++;
                    } else {
                        this.__currentChannelIndex = 0;
                    }
                    console.info("switching into the next channel");
                    targetCh = this.__channelList[this.__currentChannelIndex];
                }
                if (targetCh) {
                    this.goToChannel(targetCh);
                    return;
                }
                console.debug("no available channels");
            },
            /**
             * To handle chanel down
             * @function chDown
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            chDown: function () {
                var currentCh, targetCh;
                if (!this.__tvOn) {
                    return;
                }
                if (this.__channelList.length === 0) {
                    currentCh = this.getChannelInfo();
                    if (currentCh && currentCh.major) {
                        targetCh = {
                            major: (currentCh.major === 1) ? 135 : currentCh.major - 1,
                            minor: currentCh.minor
                        };
                    }
                } else {
                    if (this.__currentChannelIndex > 0) {
                        this.__currentChannelIndex--;
                    } else {
                        this.__currentChannelIndex = this.__channelList.length - 1;
                    }
                    console.info("switching into the prev channel");
                    targetCh = this.__channelList[this.__currentChannelIndex];
                }
                if (targetCh) {
                    this.goToChannel(targetCh);
                    return;
                }
                console.debug("no available channels");
            },
            /**
             * Handle the channel switch by channel up and channel down
             * @function __handleSwitchChannel
             * @param {Object} key from the sEnv
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __handleSwitchChannel: function (key) {
                var device = require("ax/device");
                console.info("received channel up and down key");
                if (key.id === device.tvkey.constructor.CH_UP.id) {
                    this.chUp();
                }

                if (key.id === device.tvkey.constructor.CH_DOWN.id) {
                    this.chDown();
                }
            },
            /**
             * checks the function exist in window plugin
             * Samsung created a mess on this window plugin thing, so try to resolve it in this way
             * @function __checkFunction
             * @param {String} name the function name
             * @memberof ax/ext/device/samsung/TvChannel#
             * @private
             */
            __checkFunction: function (name) {
                if (this.__windowPluginNew[name]) {
                    this.__windowPlugin = this.__windowPluginNew;
                } else {
                    this.__windowPlugin = this.__windowPluginDeprecated;
                }
            },
            /**
             * get and return the currentChannelInfo
             * @function getChannelInfo
             * @return {Object} channel object
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            getChannelInfo: function () {
                console.info("getChannelInfo");
                this.__checkFunction("GetCurrentChannel_Major");

                var title = globals.tvPlugin.GetPresentProgram_Title(),
                    startTime = globals.tvPlugin.GetPresentProgram_StartTime(),
                    endTime = globals.tvPlugin.GetPresentProgram_EndTime(),
                    duration = globals.tvPlugin.GetPresentProgram_Duration(),
                    currentChannelMajor = this.__windowPlugin.GetCurrentChannel_Major(),
                    currentChannelMinor = this.__windowPlugin.GetCurrentChannel_Minor(),
                    currentChannelName = this.__windowPlugin.GetCurrentChannel_Name(),
                    currentChannelOriginNetID = this.__windowPlugin.GetCurrentChannel_OriginNetID(),
                    currentChannelProgramNumber = this.__windowPlugin.GetCurrentChannel_ProgramNumber(),
                    currentChannelPTC = this.__windowPlugin.GetCurrentChannel_PTC(),
                    currentChannelServiceName = this.__windowPlugin.GetCurrentChannel_ServiceName(),
                    currentChannelType = this.__windowPlugin.GetCurrentChannel_Type(),
                    currentTransportStreamID = this.__windowPlugin.GetCurrentChannel_TransportStreamID(),
                    str = "",
                    key;

                this.__currentChannelInfo = {
                    program: {
                        title: title,
                        startTime: startTime,
                        endTime: endTime,
                        duration: duration
                    },
                    major: currentChannelMajor,
                    minor: currentChannelMinor,
                    name: currentChannelName,
                    originNetID: currentChannelOriginNetID,
                    // tirplet system
                    programNumber: currentChannelProgramNumber,
                    // i.e. service ID in triplet system
                    ptc: currentChannelPTC,
                    serviceName: currentChannelServiceName,
                    type: currentChannelType,
                    transportStreamID: currentTransportStreamID // tirplet system
                };

                for (key in this.__currentChannelInfo) {
                    str += key + " :" + this.__currentChannelInfo[key];
                }
                console.debug(str);

                return this.__currentChannelInfo;
            },
            /**
             * get and return the channelinfo from the channel list
             * @function getChannelInfoFromList
             * @param {Number} id of the channel on list
             * @return {Object} channel object
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            getChannelInfoFromList: function (id) {
                this.__checkFunction("GetChannel_Major");

                var channelMajor = this.__windowPlugin.GetChannel_Major(id),
                    channelMinor = this.__windowPlugin.GetChannel_Minor(id),
                    channelName = this.__windowPlugin.GetChannel_Name(id),
                    channelOriginNetID = this.__windowPlugin.GetChannel_OriginNetID(id),
                    channelProgramNumber = this.__windowPlugin.GetChannel_ProgramNumber(id),
                    channelPTC = this.__windowPlugin.GetChannel_PTC(id),
                    channelServiceName = this.__windowPlugin.GetChannel_ServiceName(id),
                    channelType = this.__windowPlugin.GetChannel_Type(id),
                    channeltransportStreamID = this.__windowPlugin.GetChannel_TransportStreamID(id);

                console.info("channelMajor" + channelMajor + ":channelName:" + channelName + "channelMinor" + channelMinor + "channelOriginNetID" + channelOriginNetID + "channelProgramNumber" + channelProgramNumber + "channelPTC" + channelPTC + "channelServiceName" + channelServiceName + "channelType" + channelType + "channeltransportStreamID" + channeltransportStreamID);

                return {
                    major: channelMajor,
                    minor: channelMinor,
                    name: channelName,
                    originNetID: channelOriginNetID,
                    programNumber: channelProgramNumber,
                    ptc: channelPTC,
                    serviceName: channelServiceName,
                    type: channelType,
                    transportStreamID: channeltransportStreamID
                };

            },
            /**
             * get and return the channelList
             * @function getChannelList
             * @returns {Array} array of the available channel objects
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            getChannelList: function () {
                console.info("getChannelList");

                this.__checkFunction("FindChannel");

                var newChannelList = [],
                    numOfChannel, n, current;

                if (this.__windowPlugin.FindChannel && this.__windowPlugin.FindChannel(PL_WINDOW_CHANNEL_TYPE_DTV, PL_WINDOW_TV_MODE_AIR)) {
                    numOfChannel = this.__windowPlugin.GetChannel_Size();

                    console.info("For AIR Mode : " + numOfChannel);

                    for (n = 0; n < numOfChannel; n++) {
                        current = this.getChannelInfoFromList(n);
                        this.__channelOrder.push(current.major);
                        newChannelList.push(current);
                    }
                }

                if (this.__windowPlugin.FindChannel && this.__windowPlugin.FindChannel(PL_WINDOW_CHANNEL_TYPE_DTV, PL_WINDOW_TV_MODE_CABLE)) {
                    numOfChannel = this.__windowPlugin.GetChannel_Size();
                    console.info("For CABLE Mode :" + numOfChannel);
                    for (n = 0; n < numOfChannel; n++) {
                        current = this.getChannelInfoFromList(n);
                        this.__channelOrder.push(current.major);
                        newChannelList.push(current);
                    }
                }

                this.__channelList = newChannelList;
                return this.__channelList;

            },
            /**
             * change the channel
             * @function goToChannel
             * @param {Object} channelDetails json object containing details required by the plugin
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            goToChannel: function (channelDetails) {
                if (!this.__tvOn) {
                    console.info("the tv channel is not switched on");
                    return;
                }

                if (util.isUndefined(channelDetails.major) || util.isUndefined(channelDetails.minor)) {
                    return false;
                }
                this.__checkFunction("SetChannel");
                console.info("gotoChannel" + channelDetails.major + "  minor:" + channelDetails.minor);
                return this.__windowPlugin.SetChannel(channelDetails.major, channelDetails.minor);
            },
            /**
             * set the window size of FTA channel
             * @param {Object} obj JSON object containing width, height, left, top of the screen
             * @function setWindowSize
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            setWindowSize: function (obj) {
                this.__checkFunction("SetScreenRect");

                if (obj && !util.isUndefined(obj.top) && !util.isUndefined(obj.left) && !util.isUndefined(obj.width) && !util.isUndefined(obj.height)) {
                    this.__windowPlugin.SetScreenRect(obj.left, obj.top, obj.width, obj.height);
                    console.info("top: " + obj.top + " left: " + obj.left + " width: " + obj.width + " height: " + obj.height);
                    return true;
                }
                return false;
            },
            /**
             * set the window size of FTA channel to fullscreen
             * @function setFullscreen
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            setFullscreen: function () {
                this.__checkFunction("SetScreenRect_PosSizeMode");
                if (this.__windowPlugin.SetScreenRect_PosSizeMode) {
                    this.__windowPlugin.SetScreenRect_PosSizeMode(8, 5); //>>>>>> Ho's mod 11-06-2012
                }
            },
            /**
             * get the language information, return the locale used to be used by Accedo
             * @function getLanguage
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            getLanguage: function () {
                var language = globals.tvPlugin.GetLanguage();
                if (language === PL_TV_LANGUAGE_HKG || language === PL_TV_LANGUAGE_TPE) {
                    return "zh-TW";
                }
                return "en";
            },
            /**
             * Turn off the TV channel (Ho's mod 11-06-2012)
             * @function turnOff
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            turnOff: function () {
                console.info("Turning off TV channel...");
                //this.windowPlugin.SetMediaSource();
                try {
                    this.__checkFunction("SetMediaSource");
                    this.__windowPlugin.SetMediaSource();
                    this.__tvOn = false;
                } catch (e) { // Samsung can't be trusted!
                    console.error(e.message);
                }

                sEnv.removeEventListener(sEnv.EVT_ONKEY, this.__switchChannlerHandler);
            },
            /**
             * Turn on the TV channel (Ho's mod 11-06-2012)
             * @function turnOn
             * @memberof ax/ext/device/samsung/TvChannel#
             * @public
             */
            turnOn: function () {
                console.info("Turning on TV channel...");
                try {
                    if (_getFirmwareYear() === 2011) {
                        //for 2011, the other API exists but does nothing, so force to use this deprecated API
                        this.__windowPluginDeprecated.SetSource(0);
                    } else if (this.__windowPluginNew && util.isFunction(this.__windowPluginNew.SetSource)) {
                        this.__windowPluginNew.SetSource(0);
                    } else { // should be an old device
                        this.__windowPluginDeprecated.SetSource(0);
                        this.__windowPluginDeprecated.style.zIndex = 10000;
                    }

                    //check the current channel Order
                    if (this.__channelList.length === 0) {
                        this.getChannelList();
                    }
                    var channel, major;
                    channel = this.getChannelInfo();
                    if (channel) {
                        major = channel.major;
                        this.__currentChannelIndex = util.indexOf(this.__channelOrder, major);
                    }
                    this.__tvOn = true;
                    console.info("dispatch channel changed");
                    this.dispatchEvent(this.constructor.CHANNEL_CHANGED);
                } catch (e) { // Samsung can't be trusted!
                    console.error(e.message);
                }

                this.__switchChannlerHandler = util.bind(this.__handleSwitchChannel, this);
                sEnv.addEventListener(sEnv.EVT_ONKEY, this.__switchChannlerHandler);
            }
        });
    return TvChannel;
});
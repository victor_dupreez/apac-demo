/**
 * To return the global objects used in the platform
 * @module ax/ext/device/samsung/globals
 * @property {Object} widgetAPI Widget Object <http://www.samsungdforum.com/Guide/ref00006/common_module_widget_object.html>
 * @property {Object} tvKey an object that defines the TV key code <http://www.samsungdforum.com/Guide/ref00006/common_module_tvkeyvalue_object.html>
 * @property {Object} pluginAPI Plugin Objects <http://www.samsungdforum.com/Guide/ref00006/common_module_plugin_object.html>
 * @property {Object} appCommon AppCommon plugin is for the basic TV functions <http://www.samsungdforum.com/Guide/ref00014/sef_plugin_appcommon.html>
 * @property {Object} NNaviPlugin Samsung Smart TV-specific commands on the DTV platform <http://www.samsungdforum.com/Guide/ref00011/deviceapi_nnavi.html>
 * @property {Object} mwPlugin TVMW plugin handles the functions for basic application <http://www.samsungdforum.com/Guide/ref00014/sef_plugin_tvmw.html>
 * @property {Object} mwPlugin2  deals with basic functions of TV <http://www.samsungdforum.com/Guide/ref00011/deviceapi_window.html>
 * @property {Object} tvPlugin handle basic tv function <http://www.samsungdforum.com/Guide/ref00011/deviceapi_tv.html>
 * @property {Object} networkPlugin controls and gets network-related information <http://www.samsungdforum.com/Guide/ref00011/deviceapi_network.html>
 * @property {Object} taskManagerPlugin  TV inter-task actions <http://www.samsungdforum.com/Guide/ref00011/deviceapi_taskmanager.html>
 * @property {Object} audioPlugin audio-related functions <http://www.samsungdforum.com/Guide/ref00011/deviceapi_audio.html>
 * @property {Object} pluginObject3D handles the TV screen commands <http://www.samsungdforum.com/Guide/ref00011/deviceapi_screen.html>
 * @property {Object} widevinePlugin external widget <http://www.samsungdforum.com/Guide/ref00014/sef_plugin_externalwidgetinterface.html>
 * @property {Object} timePlugin handles the TV time <http://www.samsungdforum.com/Guide/ref00011/deviceapi_time.html>
 * @property {Object} sefPlugin sef plugin <http://www.samsungdforum.com/Guide/ref00014/index.html>
 * @property {Object} infolinkPlayer infolink player plugin <http://www.samsungdforum.com/Guide/ref00011/deviceapi_player.html>
 * @property {Object} webapis web device api <http://www.samsungdforum.com/Guide/ref00008/index.html> Only available on 2012+
 */
define("ax/ext/device/samsung/globals", ["./loader/nativeAPI!$MANAGER_WIDGET/Common/API/Widget", "./loader/nativeAPI!$MANAGER_WIDGET/Common/API/Plugin", "./loader/nativeAPI!$MANAGER_WIDGET/Common/API/TVKeyValue"], function (Widget, Plugin, TVKeyValue) {
    "use strict";
    var samsungObj, obj;
    samsungObj = document.createElement("div");
    samsungObj.id = "samsungDeviceObject";
    document.body.appendChild(samsungObj);
    /*jshint multistr: true */
    samsungObj.innerHTML = "<object id='pluginNetwork' border=0 classid='clsid:SAMSUNG-INFOLINK-NETWORK'></object>\
    <object id='pluginObjectAudio' border=0 classid='clsid:SAMSUNG-INFOLINK-AUDIO' style='opacity:0.0;background-color:#000000;width:0px;height:0px;'></object>\
    <object id='pluginObjectTVMW' border=0 classid='clsid:SAMSUNG-INFOLINK-TVMW'></object>\
    <object id='pluginObjectNNavi' border=0 classid='clsid:SAMSUNG-INFOLINK-NNAVI'></object>\
    <object id='pluginObjectAppCommon' border=0 classid='clsid:SAMSUNG-INFOLINK-APPCOMMON'></object>\
    <div id='InfolinkContainer' class='playerContainer'><object id='infolinkPlayer' border=0 classid='clsid:SAMSUNG-INFOLINK-PLAYER' style='position:absolute;'></object></div>\
    <object id='pluginObjectTV' border=0 classid='clsid:SAMSUNG-INFOLINK-TV'></object>\
    <object id='pluginObjectExternal' border=0 classid='clsid:SAMSUNG-INFOLINK-EXTERNALWIDGETINTERFACE'></object>\
    <object id='TimePlugin' border=0 classid='clsid:SAMSUNG-INFOLINK-TIME'></object>\
    <object id='pluginObjectWindow' border=0 classid='clsid:SAMSUNG-INFOLINK-WINDOW'></object>\
    <object id='pluginObjectTaskManager' border=0 classid='clsid:SAMSUNG-INFOLINK-TASKMANAGER'></object>\
    <object id='pluginObjectScreen3D' border=0 classid='clsid:SAMSUNG-INFOLINK-SCREEN'></object>\
    <div id='SefPlayerContainer' class='playerContainer'><object id='pluginSef' border=0 classid='clsid:SAMSUNG-INFOLINK-SEF' style='position:fixed;width:0px;height:0px'></object></div>\
    <object id='pluginSefDownload' border=0 classid='clsid:SAMSUNG-INFOLINK-SEF' style='position:absolute;width:0px;height:0px'></object>";

    obj = {
        widgetAPI: new Widget(),
        tvKey: new TVKeyValue(),
        pluginAPI: new Plugin(),
        appCommon: document.getElementById("pluginObjectAppCommon"),
        NNaviPlugin: document.getElementById("pluginObjectNNavi"),
        mwPlugin: document.getElementById("pluginObjectTVMW"),
        mwPlugin2: document.getElementById("pluginObjectWindow"),
        tvPlugin: document.getElementById("pluginObjectTV"),
        networkPlugin: document.getElementById("pluginNetwork"),
        taskManagerPlugin: document.getElementById("pluginObjectTaskManager"),
        audioPlugin: document.getElementById("pluginObjectAudio"),
        pluginObject3D: document.getElementById("pluginObjectScreen3D"),
        widevinePlugin: document.getElementById("pluginObjectExternal"),
        //widevine ID
        timePlugin: document.getElementById("TimePlugin"),
        //TV channel
        sefPlugin: document.getElementById("pluginSef"),
        infolinkPlayer: document.getElementById("infolinkPlayer"),
        webapis: null,
        //download
        download: document.getElementById("pluginSefDownload")
    };
    
    return obj;
});
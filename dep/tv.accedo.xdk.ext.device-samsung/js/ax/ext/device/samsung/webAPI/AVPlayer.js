/**
 * Samsung AVPlayer which only available from 2012 samsung devices and those devices with webapis.
 * <http://www.samsungdforum.com/Guide/ref00008/avplay/dtv_avplay_module.html>
 * @class ax/ext/device/samsung/webAPI/AVPlayer
 * @augments ax/device/interface/Player
 */
define("ax/ext/device/samsung/webAPI/AVPlayer", [
    "ax/class",
    "ax/device/Media",
    "ax/console",
    "ax/Element",
    "ax/core",
    "ax/util",
    "ax/ext/device/samsung/globals",
    "ax/ajax",
    "ax/device/AbstractPlayer",
    "ax/device/interface/Player",
    "ax/exception",
    "require"
], function (
    klass,
    Media,
    console,
    Element,
    core,
    util,
    globals,
    ajax,
    AbstractPlayer,
    IPlayer,
    Exception,
    require
) {
    "use strict";

    function _getDeviceAPI(api) {
        var device = require("ax/device");
        return device[api];
    }

    function _getResolution() {
        return _getDeviceAPI("system").getDisplayResolution();
    }

    var PLAYBACK_ERRORS = IPlayer.PLAYBACK_ERRORS;

    var PLAYREADY = "playready";
    var WMDRM = "wmdrm";
    var WIDEVINE = "widevine";

    var sMedia = Media.singleton(),

        AVClass = klass.create(AbstractPlayer, {}, {
            /**
             * connection time limit
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _connectionTimeLimit: 90,
            /**
             * the media plugin object
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _plugin: null,
            /**
             * JSON containing current window size
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _currentWindowSize: null,
            /**
             * playback current time
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _curTime: 0,
            /**
             * media duration
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _duration: 0,
            /**
             * is the player ready for playback
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _prepared: false,
            /**
             * playback Speed
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _playbackSpeed: 1,

            /**
             * cookie url for authentication
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _authCookiesUrl: null,
            /**
             * To store the parent container in form of dom element
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            __parentContainerNode: null,
            /**
             * To store the dom element of the container
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            __containerNode: null,
            /**
             * To see if set any play option.
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            __setPlayOption: false,
            /**
             * To see if set any play option.
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _ready: false,
            /**
             * To see the type
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            __type: null,

            /**
             * The current DRM data. These data are overwrite after load().
             *
             * @private
             * @property {String} drm The DRM type
             * @property {String} laUrl The license acquisition server URL
             * @property {Object} playreadyCustomData The custom data for playready
             * @property {String} cookie The cookie for wmdrm
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            __currentDrmData: {},

            /**
             * @method
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             * @return {ax/device/interface/Player~PlayerCapabilites}
             */
            getCapabilities: function () {
                return {
                    type: ["mp4", "asf", "hls", "has", "mp3"],
                    drms: ["wmdrm", "playready", "widevine"]
                };
            },
            /**
             * To initialize the plugin player
             * @method
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             * @private
             */
            init: function (devicePackage) {

                if (!globals.webapis || !globals.webapis.avplay) {
                    throw core.createException(Exception.UNSUPPORTED_OPERATION, "AV Player is not ready in the webapis.");
                }

                var ret = globals.webapis.avplay.getAVPlay(util.bind(function (avplayer) {
                    console.info("[XDK] AVPlayer is responsed" + avplayer);
                    this._plugin = avplayer;
                }, this), function (ex) {
                    console.error(ex);
                    throw core.createException(Exception.UNSUPPORTED_OPERATION, "Could not create AV Play instance.");
                });


                if (!this._plugin || (this._plugin && !this._plugin.init)) {
                    throw core.createException(Exception.UNSUPPORTED_OPERATION, "AV plugin is not found, player cannot be init");
                }

                console.info("[XDK] AVPlayer is obtained and ready to use" + ret);

            },
            /**
             * Safely de-init and remove the mediapPyer
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             * @ignore
             */
            deinit: function () {
                this.stop();
                this.reset();

                this._inited = false;

                console.info("[XDK] AVPlayer samsung media deinit success");
                return;
            },
            /**
             * set the window size
             * @param {Object} obj containing width, height, left, top of the screen
             * @method
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            setWindowSize: function (obj) {
                //to set the position
                if (this.__containerNode) {
                    this.__containerNode.style.position = (obj.relativeToParent ? "relative" : "fixed");
                }

                this._plugin.setDisplayRect(obj);
                this._currentWindowSize = obj;

                if (sMedia.isState(sMedia.PAUSED)) {
                    // to prevent the player from resuming on its own. Samsung is not to be trusted.
                    //this._plugin.pause();
                }
            },
            /**
             * set the window size to be fullscrenn
             * @method
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            setFullscreen: function () {
                console.info("[XDK] AVPlayer set to be fullscreen with height: " + _getResolution().height + "  width: " + _getResolution().width);
                this.setWindowSize({
                    top: 0,
                    left: 0,
                    height: _getResolution().height,
                    width: _getResolution().width
                });
            },
            /**
             * To prepare the plugin player for video playback
             * @method
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             * @public
             */
            prepare: function (opts) {
                var ret;

                if (this._prepared) {
                    return;
                }

                var setting = {
                    bufferingCallback: {
                        onbufferingstart: util.bind(function () {
                            console.info("[XDK] AVPlayer buffering started");
                            this._onBufferingStart();
                        }, this),
                        onbufferingprogress: util.bind(function (percent) {
                            console.info("[XDK] AVPlayer on buffering : " + percent);
                            this._onBufferingProgress(percent);
                        }, this),
                        onbufferingcomplete: util.bind(function () {
                            console.info("[XDK] AVPlayer buffering completely");
                            this._onBufferingComplete();
                        }, this)
                    },
                    playCallback: {
                        oncurrentplaytime: util.bind(function (time) {
                            console.info("[XDK] AVPlayer recevied playing time : " + time);
                            //time.timeString => string in form of "00:00:01"
                            //time.millisecond = > millisecond
                            this._onCurrentPlayTime(time.millisecond);
                        }, this),
                        onresolutionchanged: util.bind(function (width, height) {
                            console.info("[XDK] AVPlayer resolution changed : " + width + ", " + height);
                        }, this),
                        onstreamcompleted: util.bind(function () {
                            console.info("[XDK] AVPlayer streaming completed");
                            this._onRenderingComplete();
                        }, this),
                        onerror: util.bind(function (error) {
                            console.info("[XDK] AVPlayer onError" + error.name);
                            this._onRenderError(error);
                        }, this)
                    },
                    autoratio: true
                };

                if (opts.parentNode) {

                    //since the avplayer will rely on the node id to find the parent container so set one for it.
                    if (util.isUndefined(opts.parentNode.id)) {
                        opts.parentNode.id = "___xdkAVPlayer" + core.getGuid();
                    }

                    setting.containerID = opts.parentNode.id;

                    this.__parentContainerNode = opts.parentNode;
                } else {
                    this.__parentContainerNode = null;
                }

                this.__containerNode = null;

                //init the player object
                ret = this._plugin.init(setting);

                if (ret) {
                    this._prepared = true;
                    console.info("[XDK] AVPlayer is prepare and inited properly");
                    return;
                }

                console.info("[XDK] AVPlayer is not inited properly.");
            },
            /**
             * To reset the plugin player for video playback
             * @method
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             * @public
             */
            reset: function () {
                if (!this._prepared) {
                    return;
                }

                if (this.__parentContainerNode && this.__containerNode) {
                    this.__parentContainerNode.removeChild(this.__containerNode);
                }

                this.__containerNode = null;
                this.__parentContainerNode = null;

                this._prepared = false;
            },
            /**
             * Loads a media from a URL and prepares the media player before playing
             * please refer <http://www.samsungdforum.com/Guide/ref00008/avplay/dtv_avplay_module.html> for more information
             * @method
             * @param {String}  mediaUrlurl string of the media
             * @param {Object} [opts] Optional - extra prarameter needed
             * @param {String} [opts.drm] DRM technology to use
             * @param {String} [opts.type] media container format to use
             * @param {String} [opts.drmUrl] (Widevine || Playready || Verimatrix) Set the DRM license url
             * @param {String} [opts.deviceId] the device id
             * @param {String} [opts.deviceTypeId] the device type id when using widevine
             * @param {String} [opts.portal] portal param when
             * @param {String} [opts.authCookiesUrl] Set the url to get the authentication cookies
             * @param {String} [opts.startBitrate] To set the initial bitrate
             * @param {String} [opts.bitrates] Set bitrate range for PlayReady e.g ("305000~937000")
             * @param {String} [opts.upTimer]
             * @param {String} [opts.userData]  Set the user data
             * @param {String} [opts.customData]  Set the user data
             * @param {String} [opts.companyId]
             * @param {String} [opts.streamId]
             * @param {String} [opts.heartbeatPeriod]
             * @param {String} [otps.ackUrl]
             * @param {String} [opts.admode]
             * @param {String} [opts.cookie]  Set the COOKIE information
             * @param {Number} [opts.totalBufferSize] total beffer size
             * @param {Number} [opts.pendingBufferSize] pending Buffer Size
             * @param {Number} [opts.initialBufferSize] initial Buffer Size
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            load: function (mediaUrl, opts) {

                this.__setPlayOption = false;
                this.__type = opts.type;
                var adaptive = {},
                    setAdaptive = false;
                opts = opts || {};
                this.opts = {};

                //handle the buffer size
                if (!util.isUndefined(opts.totalBufferSize)) {
                    this.opts.totalBufferSize = opts.totalBufferSize;
                    this.__setPlayOption = true;
                }

                if (!util.isUndefined(opts.pendingBufferSize)) {
                    this.opts.pendingBufferSize = opts.pendingBufferSize;
                    this.__setPlayOption = true;
                }

                if (!util.isUndefined(opts.initialBufferSize)) {
                    this.opts.initialBufferSize = opts.initialBufferSize;
                    this.__setPlayOption = true;
                }

                //Setting the adaptive streaming option
                //http://www.samsungdforum.com/Guide/art00098/index.html

                //should be same as component
                if (opts.drm === WMDRM) {
                    adaptive.type = WMDRM.toUpperCase();
                    setAdaptive = true;
                }

                if (opts.type === "hls") {
                    adaptive.type = "HLS";
                    setAdaptive = true;
                }

                if (opts.type === "has") {
                    adaptive.type = "HAS";
                    setAdaptive = true;
                }

                if (!util.isUndefined(opts.startBitrate)) {
                    adaptive.startBitrate = opts.startBitrate;
                    setAdaptive = true;
                }

                if (!util.isUndefined(opts.upTimer)) {
                    adaptive.upTimer = opts.upTimer;
                    setAdaptive = true;
                }

                if (!util.isUndefined(opts.bitrates)) {
                    adaptive.bitrates = opts.bitrates;
                    setAdaptive = true;
                }

                if (!util.isUndefined(opts.admode)) {
                    adaptive.admode = opts.admode;
                    setAdaptive = true;
                }

                if (setAdaptive) {
                    if (util.isUndefined(adaptive.type)) {
                        adaptive.type = "";
                    }
                    this.opts.adaptive = adaptive;
                    this.__setPlayOption = true;
                }

                if (opts.drm === PLAYREADY || opts.drm === WMDRM || opts.drm === WIDEVINE) {
                    var cookie;

                    this.__currentDrmData.drm = opts.drm;
                    this.opts.drm = {};

                    //Converting the type to samsung widevine type short form
                    if (opts.drm === "widevine") {
                        this.opts.drm.type = "WV";

                        //XDK-2413 @workaround to set the default value for those parameters.otherwise, it will become undefined
                        opts.deviceId = opts.deviceId || "";
                        opts.deviceTypeId = opts.deviceTypeId || "";
                        opts.streamId = opts.streamId || "";
                        opts.drmUrl = opts.drmUrl || "";
                        opts.ackUrl = opts.ackUrl || "";
                        opts.heartbeatPeriod = opts.heartbeatPeriod || "";
                        opts.heartbeatURL = opts.heartbeatURL || "";
                        opts.userData = opts.userData || "";
                        opts.portal = opts.portal || "";

                    } else {
                        this.opts.drm.type = opts.drm;
                    }

                    if (opts.authCookiesUrl || this._authCookiesUrl) {

                        if (this._authCookiesUrl !== opts.authCookiesUrl) {
                            this._authCookiesUrl = opts.authCookiesUrl;

                            console.info("[AVPlayer]: send a request to get the cookies");
                            ajax.request(opts.authCookiesUrl, {
                                method: "GET",
                                async: false,
                                onSuccess: util.bind(function (transport) {
                                    if (transport.getResponseHeader("Set-Cookie")) {
                                        cookie = transport.getResponseHeader("Set-Cookie");

                                        _getDeviceAPI("storage").set("accedoDRMCookie", opts.cookie);
                                        _getDeviceAPI("storage").save();

                                        console.info("[AVPlayer] get the set-cookies in response:" + opts.cookie);
                                    } else {
                                        _getDeviceAPI("storage").load();

                                        if (_getDeviceAPI("storage").get("accedoDRMCookie")) {
                                            cookie = _getDeviceAPI("storage").get("accedoDRMCookie");
                                        }

                                        console.info("[XDK] AVPlayer: no set cookie in response and try to get it from storage:" + opts.cookie);
                                    }

                                    console.info("[XDK] AVPlayer: got cookie:" + opts.cookie);
                                    this.__currentDrmData.cookie = cookie;
                                }, this),
                                onFailure: util.bind(function (transport) {
                                    console.error("[XDK] AVPlayer: fail to get the cookie" + transport);
                                    this.__currentDrmData.cookie = null;

                                    //unable to get the response from the request and suppose to be wrong,throw error                                    
                                    sMedia._onError(PLAYBACK_ERRORS.DRM.INVALID, "Failed to get cookie " + transport);
                                }, this)
                            });
                        } else { //either already have no url or same url as before and then try to get the cookie from the storage

                            if (_getDeviceAPI("storage").get("accedoDRMCookie")) {
                                cookie = _getDeviceAPI("storage").get("accedoDRMCookie");
                                this.__currentDrmData.cookie = cookie;
                            }

                            console.info("[XDK]  AVPlayer: got cookie from storage:" + cookie);
                        }

                    } else {
                        this.__currentDrmData.laUrl = null;
                        this.__currentDrmData.cookie = null;
                    }

                    if (opts.drm === PLAYREADY) {
                        if (opts.customData) {
                            this.__currentDrmData.playreadyCustomData = opts.customData;
                        }

                        if (opts.drmUrl) {
                            this.__currentDrmData.laUrl = opts.drmUrl;
                        }
                    } else {
                        this.__currentDrmData.playreadyCustomData = null;
                    }

                    if (!util.isUndefined(opts.customData)) {
                        this.opts.drm.userData = opts.customData;
                    }

                    if (!util.isUndefined(opts.userData)) {
                        this.opts.drm.userData = opts.userData;
                    }

                    if (!util.isUndefined(opts.drmUrl)) {
                        this.opts.drm.drmURL = opts.drmUrl;
                    }

                    if (!util.isUndefined(opts.cookie) || cookie) {
                        this.opts.drm.cookie = opts.cookie;
                    }

                    if (!util.isUndefined(opts.portal)) {
                        this.opts.drm.portal = opts.portal;
                    }

                    if (!util.isUndefined(opts.company)) {
                        this.opts.drm.company = opts.company;
                    }

                    if (!util.isUndefined(opts.deviceId)) {
                        this.opts.drm.deviceID = opts.deviceId;
                    } else {
                        this.opts.drm.deviceID = "60";
                    }

                    if (!util.isUndefined(opts.deviceTypeId)) {
                        this.opts.drm.deviceType = opts.deviceTypeId;
                    }

                    if (!util.isUndefined(opts.streamId)) {
                        this.opts.drm.streamID = opts.streamId;
                    }

                    if (!util.isUndefined(opts.ackUrl)) {
                        this.opts.drm.ackURL = opts.ackUrl;
                    }

                    if (!util.isUndefined(opts.heartbeatPeriod)) {
                        this.opts.drm.heartbeatPeriod = opts.heartbeatPeriod;
                    }

                    this.__setPlayOption = true;

                    console.info("[XDK] AVPlayer load the playready with parameter");
                } else {
                    this.__currentDrmData.drm = null;
                }


                //if it is audio type,no matter whether it set the window size before,it will also change back to 0,0,0,0
                if (!util.isUndefined(opts.type) && opts.type === "mp3") {
                    this._currentWindowSize = this.AUDIO_SIZE;
                    this.setWindowSize(this._currentWindowSize);
                }

                this.url = mediaUrl;

                this._plugin.hide();
                return mediaUrl;
            },
            /**
             * play the media
             * @method
             * @param {Object} opts object containing the options
             * @param {Number} opts.sec Play the video at the specified second
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            play: function (opts) {
                console.info("[XDK] AVPlayer: play requested with url " + this.url);

                var ret = this.__attemptPlay(opts);

                if (ret === false) {
                    console.info("[XDK] AVPlayer Unable to play the video");
                    sMedia._onError(PLAYBACK_ERRORS.RENDER.FAILED);
                    return false;
                }

                //only change state when the video is stopped
                if (sMedia.isState(sMedia.STOPPED)) {
                    sMedia._onConnecting();
                }
            },
            /**
             * the success callback when play
             * @method
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            __playSuccessCB: function () {
                var i, len, childNodes;

                this._ready = true;
                console.info("[XDK] AVPlayer play successfully.");

                this._plugin.show();

                console.info("[XDK] AVPlayer get the duration");

                this._setDuration();

                //default will be position fixed
                //to find the container
                if (this.__parentContainerNode) {
                    for (i = 0, len = this.__parentContainerNode.childNodes.length; i < len; i++) {

                        childNodes = this.__parentContainerNode.childNodes[i];

                        if (childNodes.childNodes.length === 1 && childNodes.childNodes[0].id !== "pluginSef" && childNodes.childNodes[0].getAttribute("classid") === "clsid:SAMSUNG-INFOLINK-SEF") {
                            this.__containerNode = childNodes;
                            this.__containerNode.style.position = "fixed";
                            break;
                        }

                    }
                }

                //to create a connection timeout
                console.info("[XDK] AVPlayer set the connection timeout");

                this.connectionTimer = core.getGuid();
                util.delay(this._connectionTimeLimit, this.connectionTimer).then(util.bind(function () {
                    console.log("[XDK] AVPlayer: connection timeout");
                    this._onConnectionTimeout();
                }, this)).done();

            },
            /**
             * the fail callback when play
             * @method
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            __playErrorCB: function (error) {
                /*
            "AVPlayNetworkDisconnectedError": If network is disconnected
            "AVPlayUnsupportedVideoFormatError": If the video codec is not supported
            "AVPlayUnsupportedVideoResolutionError": If the video resolution is not supported
            "AVPlayUnsupportedVideoFrameRateError": If the video frame rate is not supported
            "AVPlayCorruptedStreamError": If the video container is corrupt
            "InvalidValuesError": If the input attributes do not contain valid values.
            "UnknownError": In the case of any other error.
            */
                switch (error.name) {
                case "AVPlayNetworkDisconnectedError":
                    this._onError(PLAYBACK_ERRORS.NETWORK.DISCONNECTED, error);
                    break;
                case "InvalidValuesError":
                case "AVPlayUnsupportedVideoFormatError":
                case "AVPlayUnsupportedVideoResolutionError":
                case "AVPlayUnsupportedVideoFrameRateError":
                    this._onError(PLAYBACK_ERRORS.RENDER.UNSUPPORTED, error);
                    break;
                case "AVPlayCorruptedStreamError":
                    this._onError(PLAYBACK_ERRORS.RENDER.CORRUPTED, error);
                    break;
                case "UnknownError":
                    this._onError(PLAYBACK_ERRORS.GENERIC.UNKNOWN, error);
                    break;
                }

                console.info("[XDK] AVPlayer play error " + error.name + "  message" + error.message);
            },
            /**
             * Really calls plugin object to play the media
             * @method
             * @param {Object} opts object containing the options
             * @param {Number} [opts.sec] Play the video at the specified second
             * @private
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            __attemptPlay: function (opts) {
                var retOpen, retPlay, retLaUrl, retCookie, retCustomData, sec;
                opts = opts || {};
                sec = opts.sec || 0;

                //attempt play from paused, seek the time

                switch (sMedia.getState()) {

                case sMedia.PLAYING:
                case sMedia.SPEEDING:

                    if (!util.isUndefined(opts.sec)) {
                        this.seek(opts.sec);
                    }

                    console.info("[XDK] AVPlayer set the playback speed to 1 when pressing play");

                    if (this._playbackSpeed !== 1) {
                        this.speed(1);
                    }

                    return;

                case sMedia.PAUSED:

                    if (!util.isUndefined(opts.sec)) {
                        this.seek(opts.sec);
                    }
                    this.resume();

                    return;
                }

                this._ready = false;
                this._playbackSpeed = 1;
                this._curTime = 0;
                this._duration = 0;

                // some devices will make the video fullscreen when finished playback
                // so reset the window size to counter that glitch
                if (this._currentWindowSize) {
                    console.info("[XDK] setting the window size!!");
                    this.setWindowSize(this._currentWindowSize);
                }


                //to set the option if there any item in the option
                if (this.__setPlayOption) {
                    retOpen = this._plugin.open(this.url, this.opts);
                } else {
                    retOpen = this._plugin.open(this.url);
                }

                switch (this.__currentDrmData.drm) {
                case PLAYREADY:

                    if (this.__currentDrmData.laUrl) {
                        console.info("[XDK] set the licenseUrl");
                        retLaUrl = this._plugin.setPlayerProperty(4, this.__currentDrmData.laUrl, this.__currentDrmData.laUrl.length);
                        console.info("[XDK] set license result:" + retLaUrl);
                    }

                    if (this.__currentDrmData.playreadyCustomData) {
                        retCustomData = this._plugin.setPlayerProperty(3, this.__currentDrmData.playreadyCustomData, this.__currentDrmData.playreadyCustomData.length);
                        console.info("[XDK] set custom data result:" + retCustomData);
                    }
                    break;
                case WMDRM:
                    if (this.__currentDrmData.cookie) {
                        retCookie = this._plugin.setPlayerProperty(1, this.__currentDrmData.cookie, this.__currentDrmData.cookie.length);
                        console.info("[XDK] set the cookie result:" + retCookie);
                    }
                    break;
                }

                if (sec > 0) {
                    retPlay = this._plugin.play(util.bind(this.__playSuccessCB, this), util.bind(this.__playErrorCB, this), sec);
                } else {
                    retPlay = this._plugin.play(util.bind(this.__playSuccessCB, this), util.bind(this.__playErrorCB, this));
                }

                console.info("[XDK] AVPlayer attempt play open" + retOpen + "retPlay  " + retPlay);
            },
            /**
             * pause the video item
             * @method
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            pause: function () {
                this._plugin.pause();
                sMedia._onPause();
            },
            /**
             * stop the video item
             * @method
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            stop: function () {
                console.info("[XDK] AVPlayer: stop requested");

                this._stopPlayback();
                sMedia._onStopped();
                return true;
            },
            /**
             * stop the video playback for real
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _stopPlayback: function () {
                console.log("[XDK] AVPlayer stop action enforced");
                if (this._plugin) {
                    this._plugin.stop();
                }

                this._plugin.hide();

            },
            /**
             * resume the video item
             * @method
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            resume: function () {
                this._plugin.resume();
                sMedia._onPlaying();

                if (this._playbackSpeed !== 1) {
                    this._playbackSpeed = 1;
                    this._plugin.setSpeed(1);
                }
            },
            /**
             * seek to a specific time
             * @method
             * @param {Number} sec the number of second you want to seek to (in sec)
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             * @public
             */
            seek: function (sec) {
                //workaround to seek when speeding.
                if (sMedia.isState(sMedia.SPEEDING)) {
                    sec = Math.max(sec, 1);
                }
                //get current time in sec
                var curTime = this.getCurTime();
                this.skip(sec - curTime);
            },
            /**
             * skip the video for a few second
             * @method
             * @param {Number} sec number of second to skip (10 by default)
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            skip: function (sec) {
                //Note: haven't been test for non progressive usage.
                var curTime = this.getCurTime(),
                    duration = this.getDuration();

                if (!sec) {
                    return;
                }


                if (curTime + sec > duration) { // make sure do not skip outside of boundary
                    sec = duration - curTime;
                } else if (curTime + sec < 0) {
                    sec = -curTime;
                }

                console.info("[XDK] AVPlayer skip the player with " + sec);

                if (sec < 0) {
                    this._plugin.jumpBackward(Math.round(sec) * -1);
                } else {
                    this._plugin.jumpForward(Math.round(sec));
                }
            },
            /**
             * set the playback speed of the media
             * @method
             * @param {Number} speed speed number, better to be multiple of 2 and within -16 to 16
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            speed: function (speed) {
                // does not work on paused state as well
                if (sMedia.isState(sMedia.PAUSED)) {
                    this._plugin.resume();
                }

                if (this.__type === "hls") {
                    console.warn("AV Player:Speeding during HLS playback is not supported by device!");
                    //since it resume and change back to the state for playing
                    sMedia._onPlaying();
                    return;
                }

                speed = Math.floor(speed);

                if (speed > 16) {
                    speed = 16;
                }
                if (speed < -16) {
                    speed = -16;
                }

                this._playbackSpeed = speed;
                //Warning: not all device and media type would support this function
                if (speed === 1) {
                    this._plugin.setSpeed(speed);
                    sMedia._onPlaying();
                    return;
                }

                this._plugin.setSpeed(speed);
                sMedia._onSpeeding();
            },
            /**
             * get bitrate and available bitrates
             * @method
             * @return {ax/device/interface/Player~MediaBitrates}
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            getBitrates: function () {
                if (this._currentBitrate === null || this._availableBitrates === null) {
                    return false;
                }

                return {
                    currentBitrate: this._currentBitrate,
                    availableBitrates: this._availableBitrates
                };
            },
            /**
             * get and return playbackSpeed
             * @method
             * @return {Number} playback speed
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            getPlaybackSpeed: function () {
                return this._playbackSpeed;
            },
            /**
             * set the current time
             * @method
             * @param {Number} time current time in milliseconds
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _setCurTime: function (time) {
                var curBitrate, availBitrateString, sortNumeric, availBitrate, i;

                console.info("[XDK] AVPlayer: set current time to:" + time);
                this._curTime = time;
                sMedia._onTimeUpdate(time / 1000);

                if (sMedia.isState(sMedia.BUFFERING) || sMedia.isState(sMedia.STOPPED) || sMedia.isState(sMedia.PAUSED)) {
                    return;
                }

                //upagte bitrates
                curBitrate = this._plugin.getCurrentBitrate();

                console.info("[XDK] AVPlayer current Bitrate" + curBitrate);

                if (typeof curBitrate === "number") {
                    this._currentBitrate = parseInt(curBitrate, 10);
                }

                availBitrateString = this._plugin.getAvailableBitrates();

                sortNumeric = function (a, b) {
                    return Number(a) - Number(b);
                };

                if (typeof availBitrateString === "string") {
                    availBitrate = availBitrateString.split("|");
                    for (i = 0; i < availBitrate.length - 1; i++) {
                        availBitrate[i] = parseInt(availBitrate[i], 10);
                    }
                    this._availableBitrates = availBitrate.sort(sortNumeric);
                }

                console.info("[XDK] AVPlayer available Bitrate" + this._availableBitrates);
            },
            /**
             * Returns the current time (0 if never played)
             * @method
             * @param {boolean} seconds When true, the return value is in seconds
             * @returns {Integer|false} current time in milliseconds (or seconds according to the 'seconds' param)
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            getCurTime: function () {
                return Math.floor(this._curTime / 1000);
            },
            /**
             * set the total time by reading the media's duration. It will be called when the media is loaded
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _setDuration: function () {
                console.info("[XDK] AVPlayer get the duration");
                this._duration = this._plugin.duration;
            },
            /**
             * get total time of the media
             * @method
             * @public
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            getDuration: function () {
                return Math.floor(this._duration / 1000);
            },
            /**
             * function to be triggered by the plugin player when the current playback time is progressing
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onCurrentPlayTime: function (time) {
                this._setCurTime(time);
                console.info("[XDK] AVPlayer on current time " + time);
            },
            /**
             * function to be triggered when the connection is timed out
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onConnectionTimeout: function () {
                console.info("[XDK] AVPlayer onConnectionTimeout");
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.NETWORK.TIMEOUT, "Connection timeout!");
            },
            /**
             * function to be triggered by the plugin player when there is rendering error
             * @method
             * @param {Number} param the parameter
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onRenderError: function (param) {
                console.info("[XDK] AVPlayer onRenderError " + param.name + " with " + param.message);
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.RENDER.FAILED, {
                    code: param.name,
                    msg: param.message
                }, param);
            },
            /**
             * function to be triggered by the plugin player when there is a generic error
             * @method
             * @param {Object} param the parameter
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onError: function (internalError, param) {
                console.info("[XDK] AVPlayer onRenderError " + param.name + " with " + param.message);
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.RENDER.FAILED, {
                    code: param.name,
                    msg: param.message
                }, param);
            },
            /**
             * function to be triggered by the plugin player when there is custom error from external plugin like widevine
             * @method
             * @param {Integer} error code ID
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onUnknownEvent: function (event) {
                console.info("[XDK] AVPlayer unknownEvent " + event.name + " with " + event.message);
                sMedia._onError(PLAYBACK_ERRORS.GENERIC.UNKNOWN);
            },
            /**
             * function to be triggered by the plugin player the network is disconnected
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onNetworkDisconnected: function () {
                console.info("[XDK] AVPlayer NetworkDisconnected ");
                sMedia._onError(PLAYBACK_ERRORS.NETWORK.DISCONNECTED);
            },
            /**
             * function to be triggered by the plugin player when the media rendering is complete
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onRenderingComplete: function () {
                this._stopPlayback();
                sMedia._onFinish();
            },
            /**
             * function to be triggered by the plugin player when the buffering is in progress
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onBufferingProgress: function () {
                sMedia._onBufferingProgress();
            },
            /**
             * function to be triggered by the plugin player when the buffering is started
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onBufferingStart: function () {
                console.info("[XDK] AVPlayer onBuffering Start handling");
                //remove the connection timeout
                if (this.connectionTimer) {
                    util.clearDelay(this.connectionTimer);
                    this.connectionTimer = null;
                }

                sMedia._onBufferingStart();
            },
            /**
             * function to be triggered by the plugin player when the buffering is completed
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/webAPI/AVPlayer#
             */
            _onBufferingComplete: function () {
                sMedia._onBufferingFinish();
            }
        });
    return AVClass;
});
/**
 * Id class to handle the device information like firmware version
 * @class ax/ext/device/samsung/webAPI/Id
 * @augments ax/ext/device/samsung/Id
 */
define("ax/ext/device/samsung/webAPI/Id", ["ax/class", "ax/ext/device/samsung/Id", "ax/util", "ax/console", "ax/ext/device/samsung/globals", "ax/device/interface/Id"], function (klass, samsungId, util, console, globals, IId) {
    "use strict";
    return klass.create(samsungId, [IId], {}, {
        /**
         * The mac address
         * @private
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        __mac: null,
        /**
         * To ip address
         * @private
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        __ip: "0.0.0.0",
        /**
         * To firmware version
         * @private
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        __firmware: null,
        /**
         * To save the device type
         * @private
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        __deviceType: null,
        /**
         * To save the device hardware type
         * @private
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        __hardwareType: null,
        init: function () {

            var successCB, errorCB;

            successCB = util.bind(function (networks) {

                var i, len;

                console.info("[XDK] receive " + networks.length + " network information");

                for (i = 0, len = networks.length; i < len; i++) {

                    console.info("[XDK] Available Network interface is " + networks[i].interfaceType);

                    //save the network info
                    if (networks[i].isActive()) {
                        this.__ip = networks[i].ip;
                        this.__mac = networks[i].mac;
                        console.info("[XDK] get the ip and mac address ip " + this.__ip + "  mac : " + this.__mac);
                        return;
                    }
                }

            }, this);

            errorCB = function (error) {
                console.info("[XDK] fail to get the available networks");
            };

            if (globals.webapis) {
                //get the mac
                try {
                    globals.webapis.network.getAvailableNetworks(successCB, errorCB);
                } catch (error) {
                    console.info("[XDK] Fail to get the available network" + error.name);
                }
            } else {
                console.info("[XDK] no available webapi and fail to get the id information.");
            }

            console.info("[XDK] loaded the webAPI Id");

            this._super();
        },
        /**
         * To return the device type
         * @method getDeviceType
         * @public
         * @return {String} type of the device e.g Bluray, TV
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        getDeviceType: function () {

            if (this.__deviceType) {
                return this.__deviceType;
            }

            var deviceType = "emulator",
                str, position = window.location.search.indexOf("product=");

            //use window.location.search first 
            //http://www.samsungdforum.com/Guide/d60/index.html
            if (position !== -1) {

                str = window.location.search[position + 8];

                switch (str) {
                case "2":
                    deviceType = "Bluray";
                    break;
                case "1":
                    deviceType = "Monitor";
                    break;
                case "0":
                    deviceType = "TV";
                    break;
                }

            } else {

                //get the product type
                try {
                    str = globals.webapis.tv.info.getProduct();
                    switch (str) {
                    case globals.webapis.tv.info.PRODUCT_TYPE_BD:
                        deviceType = "Bluray";
                        break;
                    case globals.webapis.tv.info.PRODUCT_TYPE_MONITOR:
                        deviceType = "Monitor";
                        break;
                    case globals.webapis.tv.info.PRODUCT_TYPE_TV:
                        deviceType = "TV";
                        break;
                    }
                } catch (ex) {

                    //http://www.samsungdforum.com/Guide/ref00008/tvinformation/dtv_tvinformation_module.html#ref00008-tvi-getproduct
                    //with error type "UnknownError" in any other error case.
                    //with error type "NotSupportedError" if the feature is not supported.
                    //with error type "SecurityError" if this functionality is not allowed.

                    console.info("[XDK] WEB API Error Unable to get the device type " + ex.name + " msg : " + ex.message);

                }

            }

            this.__deviceType = deviceType;

            return this.__deviceType;

        },
        /**
         * To return the hardware type
         * @method getHardwareType
         * @public
         * @return {ax/device/interface/Id.HARDWARE_TYPE} type of the device hardware as {@link ax/device/interface/Id.HARDWARE_TYPE.BD} 
           or {@link ax/device/interface/Id.HARDWARE_TYPE.MONITOR}
           or {@link ax/device/interface/Id.HARDWARE_TYPE.TV}
           or {@link ax/device/interface/Id.HARDWARE_TYPE.EMULATOR}  
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        getHardwareType: function () {

            if (this.__hardwareType) {
                return this.__hardwareType;
            }

            var hardwareType = IId.HARDWARE_TYPE.EMULATOR,
                str, position = window.location.search.indexOf("product=");

            //use window.location.search first 
            //http://www.samsungdforum.com/Guide/d60/index.html
            if (position !== -1) {

                str = window.location.search[position + 8];

                switch (str) {
                case "2":
                    hardwareType = IId.HARDWARE_TYPE.BD;
                    break;
                case "1":
                    hardwareType = IId.HARDWARE_TYPE.MONITOR;
                    break;
                case "0":
                    hardwareType = IId.HARDWARE_TYPE.TV;
                    break;
                }

            } else {

                //get the product type
                try {
                    str = globals.webapis.tv.info.getProduct();
                    switch (str) {
                    case globals.webapis.tv.info.PRODUCT_TYPE_BD:
                        hardwareType = IId.HARDWARE_TYPE.BD;
                        break;
                    case globals.webapis.tv.info.PRODUCT_TYPE_MONITOR:
                        hardwareType = IId.HARDWARE_TYPE.MONITOR;
                        break;
                    case globals.webapis.tv.info.PRODUCT_TYPE_TV:
                        hardwareType = IId.HARDWARE_TYPE.TV;
                        break;
                    }
                } catch (ex) {

                    //http://www.samsungdforum.com/Guide/ref00008/tvinformation/dtv_tvinformation_module.html#ref00008-tvi-getproduct
                    //with error type "UnknownError" in any other error case.
                    //with error type "NotSupportedError" if the feature is not supported.
                    //with error type "SecurityError" if this functionality is not allowed.

                    console.info("[XDK] WEB API Error Unable to get the device type " + ex.name + " msg : " + ex.message);

                }

            }

            this.__hardwareType = hardwareType;

            return this.__hardwareType;

        },
        /**
         * return the mac address
         * @method getMac
         * @return {String} mac address
         * @memberof ax/ext/device/samsung/webAPI/Id#
         * @public
         */
        getMac: function () {
            return this.__mac;
        },
        /**
         * To get the firmware version
         * @method getFirmware
         * @public
         * @return {String} the firmware of the devices
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        getFirmware: function () {

            if (this.__firmware) {
                return this.__firmware;
            }

            var firmwareStr = globals.webapis.tv.info.getFirmware();

            if (!util.isUndefined(firmwareStr) && firmwareStr.length > 0) {
                this.__firmware = firmwareStr;
            } else {
                this.__firmware = "dummyFirmware";
            }

            return this.__firmware;
        },
        /**
         * return the firmware year
         * @method getFirmwareYear
         * @returns {String} the firmware year, otherwise it will return false
         * @memberof ax/ext/device/samsung/webAPI/Id#
         * @public
         */
        getFirmwareYear: function () {
            var str = this.getFirmware(),
                year;

            if (!util.isUndefined(str)) {
                year = parseInt(str.substr(10, 4), 10);
                return isNaN(year) ? 0 : year;
            }
            return false;
        },
        /**
         * To get the unique ID. If no unique id.if will generate a UUID by XDK
         * @method getUniqueID
         * @public
         * @returns {String} the the duid from the samsung, otherwise it will return the uuid
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        getUniqueID: function () {

            var uniqueId;

            try {
                uniqueId = globals.webapis.tv.info.getDeviceID();
            } catch (ex) {
                console.info("[XDK] WEB API Error Unable to get the device id " + ex.name + " msg : " + ex.message);
            }

            if (uniqueId) {
                return uniqueId;
            }

            console.info("[XDK] fail to get DUID from the device and use UUID");
            return this._super();
        },
        /**
         * To get the model of the device. If no related information. "dummySamsungmodel" will be returned
         * @method getModel
         * @public
         * @returns {String} the model numebr
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        getModel: function () {
            //webapis.tv.info.getModel() is different from the original model number.
            //e.g "13_X12_2D" which is not expected model number
            var model = globals.webapis._plugin("TV", "GetProductCode");

            if (model) {
                return model;
            }

            return "dummySamsungmodel";
        },
        /**
         * To get IP address
         * @method getIP
         * @public
         * @return {String} the IP address
         * @memberof ax/ext/device/samsung/webAPI/Id#
         */
        getIP: function () {
            return this.__ip;
        }
    });
});
/**
 * System class to handle the system api and connection checking
 * @class ax/ext/device/samsung/webAPI/System
 * @augments ax/ext/device/samsung/System
 */
define("ax/ext/device/samsung/webAPI/System", ["ax/class", "ax/ext/device/samsung/System", "ax/ext/device/samsung/globals", "ax/console", "ax/util", "ax/EventDispatcher", "ax/device/interface/System"], function(klass, samsungSystem, globals, console, util, EventDispatcher, ISystem) {
    "use strict";
    return klass.create(samsungSystem, {}, {
        /**
         * To set the system mute
         * @public
         * @method setSystemMute
         * @param {Boolean} turnOn True if turn on the mute, false when turn off.
         * @memberof ax/ext/device/samsung/webAPI/System#
         */
        setSystemMute: function(turnOn) {
            if (globals.webapis.audiocontrol.getMute() !== turnOn) {
                //Mute
                globals.webapis.audiocontrol.setMute(turnOn);
                console.debug("[XDK] >>" + (turnOn ? " Mute" : "Unmute") + " the TV");
            } else {
                console.debug("[XDK] >> Screensaver status unchanged as mute is " + turnOn);
            }
        } 
    });
});
/* global curWidget:false,FileSystem:false */
/**
 * To provide the localstorage class based
 *
 * ###Configuration Parameters
 *
 *  Attribute | Value
 * --------- | ---------
 * Key:    | samsung.storage.uniquePrefix
 * Desc    | The prefix in the samsung
 * Type    | String
 * Default  |  {@link ax/device/AbstractStorage#_uniquePrefix}
 * Usage | {@link module:ax/config}
 * ----------------------------------
 *  Attribute | Value
 * --------- | ---------
 * Key:    | samsung.storage.fileName
 * Desc    | The file name of text file
 * Type    | String
 * Default  | "AccedoXDK.txt"
 * Usage | {@link module:ax/config}
 *
 * @class ax/ext/device/samsung/FileStorage
 * @augments ax/device/AbstractStorage
 */
define("ax/ext/device/samsung/FileStorage", ["ax/class", "ax/device/AbstractStorage", "ax/config", "ax/console", "ax/util"], function (klass, AbstractStorage, config, console, util) {
    "use strict";
    return klass.create(AbstractStorage, {}, {
        /**
         * To save the data, Only access to the file system to get the data. And then the data will be stored.
         * @private
         * @memberof ax/ext/device/samsung/FileStorage#
         */
        __tempData: null,
        /**
         * To open the samsung new system
         * @private
         * @memberof ax/ext/device/samsung/FileStorage#
         */
        __fileSystemObj: null,
        /**
         * To get the curWidget id which is the app folder and then store the file inside that folder
         * which will be removed when the apps is deleted
         * @private
         * @memberof ax/ext/device/samsung/FileStorage#
         */
        __appId: null,
        /**
         * The file Name which save the data
         * @private
         * @memberof ax/ext/device/samsung/FileStorage#
         */
        __fileName: null,

        init: function () {
            this.__appId = curWidget.id;
            this._uniquePrefix = config.get("samsung.storage.uniquePrefix", this._uniquePrefix);
            this.__fileName = this.__appId + "/" + config.get("samsung.storage.fileName", "AccedoXDK.txt");
            this.__fileSystemObj = new FileSystem();
        },
        /**
         * to set the variable
         * @public
         * @method set
         * @param {String} k keys
         * @param {String} v values
         * @memberof ax/ext/device/samsung/FileStorage#
         */
        set: function (k, v) {
            console.log("this one is using file Storage0");
            //to load the cookies when the cookies is not loaded yet
            if (!this.__tempData) {
                this.__getFile();
            }

            this.__tempData[k] = v;
            this.__setFile();
        },
        /**
         * to get the variable
         * @public
         * @method set
         * @param {String} k keys
         * @return {String} v values
         * @memberof ax/ext/device/samsung/FileStorage#
         */
        get: function (k) {
            //to load the cookies when the cookies is not loaded yet
            if (!this.__tempData) {
                this.__getFile();
            }

            var value;

            if (this.__tempData && this.__tempData[k]) {
                value = this.__tempData[k];

                if (!util.isUndefined(value)) {
                    return value;
                }
            }

            return null;
        },
        /**
         * to unset the variable
         * @public
         * @method unset
         * @param {String} k keys
         * @memberof ax/ext/device/samsung/FileStorage#
         */
        unset: function (k) {
            //to load the cookies when the cookies is not loaded yet
            if (!this.__tempData) {
                this.__getFile();
            }

            if (this.__tempData && !util.isUndefined(this.__tempData[k])) {
                this.__tempData[k] = null;
                delete this.__tempData[k];
                this.__setFile();
            }
        },
        /**
         * Direct overrideing clear as samsung has it"s own Unique Id API
         * @public
         * @method clear
         * @memberof ax/ext/device/samsung/FileStorage#
         */
        clear: function () {
            this.__tempData = {};
            this.__setFile();
        },
        /**
         * to get the file from the device
         * @private
         * @method __getFile
         * @memberof ax/device/shared/CookiesStorage
         */
        __getFile: function () {
            var jsFileObj, stringResult = "",
                i, l, cookies, cookieString;

            if (this.__fileSystemObj.isValidCommonPath(this.__fileName)) {
                console.debug("[XDK] File found: " + this.__fileName);
                jsFileObj = this.__fileSystemObj.openCommonFile(this.__fileName, "r");
                stringResult += jsFileObj.readLine();
                this.__fileSystemObj.closeCommonFile(jsFileObj);
                cookies = stringResult.split(";");

                for (i = 0, l = cookies.length; i < l; i++) {
                    cookieString = util.strip(cookies[i]);
                    if (util.startsWith(cookieString, this._uniquePrefix + "=")) {
                        cookieString = cookieString.substring((this._uniquePrefix + "=").length);
                        if (cookieString) {
                            try {
                                this.__tempData = util.parse(decodeURIComponent(cookieString));
                                return;
                            } catch (e) {
                                console.warn("[XDK] unable to load the file");
                            }
                        }
                    }
                }
            } else {
                console.debug("no file: " + this.__fileName);
            }
            this.__tempData = {};
        },
        /**
         * to save the string into the file
         * @private
         * @method __setFile
         * @memberof ax/device/shared/CookiesStorage
         */
        __setFile: function () {
            if (!this.__tempData) {
                console.warn("Object is undefined and unable to set into the file");
                return false;
            }

            var days = 365,
                date, expires, dataString, jsFileObj;

            if (!util.isEmpty(this.__tempData)) {
                date = (new Date());
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "; expires= -1";
            }

            dataString = this._uniquePrefix + "=" + encodeURIComponent(util.stringify(this.__tempData)) + expires + "; path=/";
            console.debug("[XDK] File storage save the String" + dataString);

            if (!this.__fileSystemObj.isValidCommonPath(this.__appId)) {
                console.debug("Going to create common dir: " + this.__appId);
                this.__fileSystemObj.createCommonDir(this.__appId);
            } else {
                console.debug("it has a new dir");
            }

            jsFileObj = this.__fileSystemObj.openCommonFile(this.__fileName, "w");
            jsFileObj.writeLine(dataString);
            this.__fileSystemObj.closeCommonFile(jsFileObj);
        }
    });
});
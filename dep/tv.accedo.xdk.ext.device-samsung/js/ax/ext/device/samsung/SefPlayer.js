/**
 * Samsung Sef player which only available from 2012 samsung devices.
 * @class ax/ext/device/samsung/SefPlayer
 * @augments ax/device/interface/Player
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/ext/device/samsung/SefPlayer", [
    "ax/class",
    "ax/device/Media",
    "ax/console",
    "ax/Element",
    "ax/core",
    "ax/util",
    "ax/ext/device/samsung/globals",
    "ax/ajax",
    "ax/EventDispatcher",
    "ax/device/interface/Id",
    "ax/device/interface/Player",
    "require"
], function (
    klass,
    Media,
    console,
    Element,
    core,
    util,
    globals,
    ajax,
    EventDispatcher,
    IId,
    IPlayer,
    require
) {
    "use strict";

    var PLAYBACK_ERRORS = IPlayer.PLAYBACK_ERRORS;

    function _getDeviceAPI(api) {
        var device = require("ax/device");
        return device[api];
    }

    function _getResolution() {
        return _getDeviceAPI("system").getDisplayResolution();
    }


    var sMedia = Media.singleton(),

        sefClass = klass.create(EventDispatcher, [IPlayer], {
            /**
             * Event: subtitle
             *
             * @event EVT_SUBTITLE
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer
             */
            EVT_SUBTITLE: "sefplayer:subtitle",
            /**
             * Event: screen-size-change
             *
             * @event EVT_SCREEN_SIZE_CHANGED
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer
             */
            EVT_SCREEN_SIZE_CHANGED: "sefplayer:screen-size-change"
        }, {
            /**
             * The id of the player
             * @name __id
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            __id: null,
            /**
             * current 3D mode
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _current3dType: 0,
            /**
             * previous 3D mode
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _previous3dType: 0,
            /**
             * connection time limit
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _connectionTimeLimit: 90,
            /**
             * the media plugin object
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _plugin: null,
            /**
             * In case the player plugin object needs to be moved, this is the plugin object container
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _pluginContainer: null,
            /**
             * JSON containing current window size
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _currentWindowSize: null,
            /**
             * playback current time
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _curTime: 0,
            /**
             * media duration
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _duration: 0,
            /**
             * is the player ready for playback
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _prepared: false,
            /**
             * playback Speed
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _playbackSpeed: 1,
            /**
             * is there any stop function withholding
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _withHoldingStop: false,
            /**
             * media container format
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _type: null,
            /**
             * DRM technology being used
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _drm: null,
            /**
             * Cookie information intended for DRM server
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _cookie: null,
            /**
             * cookie url for authentication
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _authCookiesUrl: null,
            /**
             * custom data for playready
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _playReadyCustomData: null,
            /**
             * DRM server location
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _DRMServer: null,
            /**
             * is currently using 3D
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _is3d: false,
            /**
             * current 3D type
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _3dType: "off",
            /**
             * will be set to true whenever current playback time has been reported by plugin
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _curTimeReported: false,
            /**
             * To indicate the meta data info is ready or not
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _infoReady: false,
            /**
             * To handle the multi audio track strategy
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            __multiAudioStgy: null,
            /**
             * @method
             * @memberof ax/ext/device/samsung/SefPlayer#
             * @return {ax/device/interface/Player~PlayerCapabilites}
             */
            getCapabilities: function () {
                return {
                    type: ["mp4", "asf", "hls", "has", "mp3"],
                    drms: ["wmdrm", "playready", "widevine", "verimatrix"]
                };
            },
            /**
             * To initialize the plugin player
             * @method
             * @memberof ax/ext/device/samsung/SefPlayer#
             * @private
             */
            init: function (devicePackage) {
                var Id = devicePackage.getInterface(devicePackage.constructor.ID),
                    self = this;
                //SEF player doesn't support in 2010 so filter it
                if (Id.getFirmwareYear() < 2011) {
                    throw core.createException("SefPluginNotSupported", "Sef plugin is not supported when less than 2011.");
                }

                this._plugin = globals.sefPlugin;

                if (!this._plugin || (this._plugin && !this._plugin.Open)) {
                    throw core.createException("SefPluginNotFound", "Sef plugin is not found, player cannot be init");
                }

                this._plugin.Open("Player", "0001", "InitPlayer");


                //it is unable to bind the callback function into plugin onEvent, so assign it by the global.
                window.__sefOnEvent = function __sefOnEvent(eventType, param1, param2) {
                    console.info("[Samsung SEF] OnEvent() - eventType : " + eventType + ", param1 : " + param1 + ", param2 : " + param2);

                    switch (eventType) {
                    case 1:
                        self._onConnectionFailed();
                        break;
                    case 2:
                        self._onAuthenticationFailed();
                        break;
                    case 3:
                        self._onStreamNotFound();
                        break;
                    case 4:
                        self._onNetworkDisconnected();
                        break;
                    case 6:
                        self._onRenderError(param1);
                        break;
                    case 8:
                        self._onRenderingComplete();
                        break;
                    case 9:
                        // stream info ready event
                        self._setDuration();
                        self.__infoReady = true;

                        //remove the connection time out since the meta data is ready
                        self.__removeConnectionTimeout();

                        break;
                    case 11:
                        self._onBufferingStart();
                        break;
                    case 12:
                        self._onBufferingComplete();
                        break;
                    case 13:
                        self._onBufferingProgress();
                        break;
                    case 14:
                        // current playback time
                        self._onCurrentPlayTime(param1);
                        break;
                    case 19:
                        // subtitle received
                        console.info("dispatch the subtitle event");
                        self.dispatchEvent(self.constructor.EVT_SUBTITLE, param1);
                        break;
                    default:
                        self._onUnknownEvent(eventType, param1, param2);
                        break;
                    }
                };
            },
            /**
             * To prepare the plugin player for video playback
             * @method
             * @memberof ax/ext/device/samsung/SefPlayer#
             * @public
             */
            prepare: function (opts) {
                if (this._prepared) {
                    return;
                }
                console.debug("opts" + opts);
                this._prepared = true;
                this._plugin.OnEvent = "window.__sefOnEvent";
            },
            /**
             * To reset the plugin player when it is not currently used
             * @method
             * @memberof ax/ext/device/samsung/SefPlayer#
             * @public
             */
            reset: function () {
                if (!this._prepared) {
                    return;
                }
                this._prepared = false;

                delete this._plugin.OnEvent;

                //enable screen saver when there is no player
                _getDeviceAPI("system").setScreenSaver(true);
            },
            /**
             * Safely de-init and remove the mediapPyer
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             * @ignore
             */
            deinit: function () {
                console.info("samsung media deinit");
                if (!this._prepared) {
                    return;
                }
                if (!this._inited) {
                    return;
                }

                if (_getDeviceAPI("id").getHardwareType() !== IId.HARDWARE_TYPE.TV) {
                    this._current3dType = 0;
                    this._previous3dType = 0;
                    if (globals.pluginObject3D && globals.pluginObject3D.Check3DEffectMode(0) === 1) {
                        globals.pluginObject3D.Set3DEffectMode(0);
                    }
                }

                this.stop();

                this.reset();

                this._inited = false;

                console.info("samsung media deinit success");
                delete window.__sefOnEvent;
                return;
            },
            /**
             * Place the video plugin object as child, to fight the z-index problem on old devices below 2012
             * @method
             * @param {ax/Element|Element|ax/af/AbstractComponent} parent The parent element to contain the video plugin object
             * @param {Boolean} [forecExecute] If true, execute this even on new devices that do not need this workaround
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            placeVideoObject: function (parent, forceExecute) {
                if (!forceExecute && _getDeviceAPI("id").getFirmwareYear() > 2011) {
                    console.warn("placeVideoObject() is a workaround method for 2011 devices or older, to fight the z-index problem!");
                    return;
                }
                if (!parent) {
                    console.error("Parent element of the video plugin object must be specified!");
                    return;
                }

                console.info("Moving samsung player plugin object..." + parent);

                if (parent.getRoot) { // this is component
                    parent = parent.getRoot();
                } else if (parent.nodeName) {
                    parent = new Element(parent);
                }

                this._plugin.parentNode.removeChild(this._plugin);

                if (this._pluginContainer) {
                    this._pluginContainer.detach();
                    this._pluginContainer = null;
                }

                //to ensure the container is removed from dom
                var containerEle = document.getElementById("SefPlayerContainer");
                if (containerEle) {
                    containerEle.parentNode.removeChild(containerEle);
                }

                this._pluginContainer = new Element("div", {
                    id: "SefPlayerContainer",
                    css: "playerContainer"
                }, parent);

                this._pluginContainer.getHTMLElement().innerHTML = "<object id='pluginSef' border=0 classid='clsid:SAMSUNG-INFOLINK-SEF' style='position:fixed;'></object>";
                this._plugin = this._pluginContainer.getHTMLElement().childNodes[0];
                this._plugin.Open("Player", "0001", "InitPlayer");
                globals.sefPlugin = this._plugin;
                this._plugin.OnEvent = "window.__sefOnEvent";
            },
            /**
             * set the window size
             * @param {Object} obj containing width, height, left, top of the screen
             * @method
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            setWindowSize: function (obj) {

                if (!this._plugin) {
                    console.warn("[SefPlayer] Fail to set the size due to no player Object");
                    return;
                }

                var resolution = _getResolution(),
                    objClassName = "playerObject";

                //determine whether it is relative to parent which add a relative class with "position:relative"
                if (obj.relativeToParent) {
                    objClassName += " relative";
                }

                //determine whether it is fullscreen and add a fullscreen selector
                if (obj.width === resolution.width && obj.height === resolution.height) {
                    objClassName += " fullscreen";
                    if (this._pluginContainer) {
                        this._pluginContainer.addClass("fullscreen");
                    }
                } else {
                    if (this._pluginContainer) {
                        this._pluginContainer.removeClass("fullscreen");
                    }
                }

                this._plugin.className = objClassName;

                this._plugin.style.top = obj.top + "px";
                this._plugin.style.left = obj.left + "px";
                this._plugin.style.width = obj.width + "px";
                this._plugin.style.height = obj.height + "px";

                this.show();

                this._setPlayerSize(obj);
                this._currentWindowSize = obj;

                if (sMedia.isState(sMedia.PAUSED)) {
                    // to prevent the player from resuming on its own. Samsung is not to be trusted.
                    this._plugin.Execute("Pause");
                }
                console.info("Dispatch the size change event");
                this.dispatchEvent(this.constructor.EVT_SCREEN_SIZE_CHANGED, obj);
            },
            /**
             * Show the player
             * @method
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            show: function () {
                if (this._plugin) {
                    this._plugin.style.visibility = "visible";
                }
            },
            /**
             * Hide the player
             * @method
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            hide: function () {
                if (this._plugin) {
                    this._plugin.style.visibility = "hidden";
                }
            },
            /**
             * set the window size to be fullscrenn
             * @method
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            setFullscreen: function () {
                var resolution = _getResolution();
                this.setWindowSize({
                    top: 0,
                    left: 0,
                    height: resolution.height,
                    width: resolution.width
                });
            },
            /**
             * set the player plugin size
             * @method
             * @protected
             * @param {object} obj the object of the dimension.
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _setPlayerSize: function (obj) {
                var pos, offset, coef, x, y, w, h, resolution = _getResolution();
                pos = {
                    x: obj.left,
                    y: obj.top
                };
                //if it is inside the container, the position will be updated with respect to its parent container when parent node is set.
                if (this._pluginContainer && (obj.width !== resolution.width || obj.height !== resolution.height)) {
                    offset = this._pluginContainer.cumulativeOffset();
                    if (offset.x) {
                        pos.x = offset.x;
                    }
                    if (offset.y) {
                        pos.y = offset.y;
                    }
                }
                if (this._plugin !== null) {
                    coef = 1;
                    x = pos.x;
                    y = pos.y;
                    w = obj.width;
                    h = obj.height;
                    if (_getDeviceAPI("id").getFirmwareYear() !== 2010) { // 2010 device should only use 1
                        coef = (resolution.width === 960 ? 1 : 960 / 1280);
                    }

                    this._plugin.Execute("SetDisplayArea", Math.round(x * coef), Math.round(y * coef), Math.round(w * coef), Math.round(h * coef));
                }
            },
            /**
             * Loads a media from a URL and prepares the media player before playing
             * @method
             * @param {String}  mediaUrlurl string of the media
             * @param {Object} [opts] Optional - extra prarameter needed
             * @param {String} [opts.drm] DRM technology to use
             * @param {String} [opts.type] media container format to use
             * @param {String} [opts.drmUrl] (Widevine || Playready || Verimatrix) Set the DRM license url
             * @param {String} [opts.userData] (Widevine) Set the user data
             * @param {String} [opts.deviceId] (Widevine)  the device id
             * @param {String} [opts.deviceTypeId] (Widevine)the device type id when using widevine
             * @param {String} [opts.drmCurTime] (Widevine) cur time param when using widevine
             * @param {String} [opts.iSeek] (Widevine) i-seek param when using widevine
             * @param {String} [opts.extraString] (Widevine) extra string param when using widevine
             * @param {String} [opts.portal] (Widevine)portal param when using widevine
             * @param {String} [opts.authCookiesUrl] (WMDRM) Set the url to get the authentication cookies
             * @param {String} [opts.customData] (PlayerReady) to set custom data
             * @param {String} [opts.startBitrate] (PlayerReady) To set the initial bitrate for PlayReady e.g ("AVERAGE"/"LOWEST"/"HIGHEST"/"CHECK"/"532000")
             * @param {String} [opts.bitrate] (PlayerReady) Set bitrate range for PlayReady e.g ("305000~937000")
             * @param {String} [opts.skipBitrate] (PlayerReady) Set the bitrate value after skip for PlayReady e.g ("CHECK"/"LOWEST"/"AVERAGE"/"HIGHEST")
             * @param {String} [opts.cookie] (PlayerReady) Set the COOKIE information for PlayReady
             * @param {Number} [opts.timeOut] (PlayerReady) Set the Network timeout value(in seconds) for PlayReady
             * @param {Number} [opts.startFragment] (PlayerReady) Set the fragment in live streaming (e.g 5) startFragment
             * @param {String} [opts.startBitrate] (HLS || HAS) To set the initial bitrate e.g 2600000
             * @param {Number} [opts.bitratesFrom] (HLS || HAS) the lowest bitrate setting
             * @param {Number} [opts.bitratesTo] (HLS || HAS) the highest bitrate setting
             * @param {Boolean} [opts.use3d] true if to use 3D
             * @param {String} [opts.type3d] the type of 3D
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            load: function (mediaUrl, opts) {
                opts = opts || {};
                this._drm = opts.drm;
                this._type = opts.type;

                var component = "",
                    deviceId, iSeek, drmCurTime, drmUrl, extraString, userData, deviceType, bitrates, portal, tempStr;

                if (opts.parentNode) {
                    this.placeVideoObject(opts.parentNode, true);
                }

                if (opts.drm === "widevine") {
                    component = "WV";
                    deviceId = "60";
                    iSeek = "TIME";
                    drmCurTime = "PTS";
                    drmUrl = "";
                    extraString = "";
                    userData = "";
                    deviceType = "";
                    portal = "";

                    if (!util.isUndefined(opts.drmUrl)) {
                        drmUrl = opts.drmUrl;
                    }
                    if (!util.isUndefined(opts.deviceId)) {
                        deviceId = opts.deviceId;
                    }
                    if (!util.isUndefined(opts.iSeek)) {
                        iSeek = opts.iSeek;
                    }
                    if (!util.isUndefined(opts.drmCurTime)) {
                        drmCurTime = opts.drmCurTime;
                    }
                    if (!util.isUndefined(opts.extraString)) {
                        extraString = opts.extraString;
                    }
                    if (!util.isUndefined(opts.deviceTypeId)) {
                        deviceType = "|DEVICE_TYPE_ID=" + opts.deviceTypeId;
                    }
                    if (!util.isUndefined(opts.userData)) {
                        userData = "|USER_DATA=" + opts.userData;
                    }
                    if (!util.isUndefined(opts.portal)) {
                        portal = "|PORTAL=" + opts.portal;
                    }

                    mediaUrl += "|DRM_URL=" + drmUrl + "|DEVICE_ID=" + deviceId + "|I_SEEK=" + iSeek + "|CUR_TIME=" + drmCurTime + userData + portal + extraString + deviceType + "|COMPONENT=" + component;

                    console.info("Sef Player: widevine turned on");

                } else if (opts.drm === "wmdrm") {
                    component = "WMDRM";
                    if (opts.authCookiesUrl && this._authCookiesUrl !== opts.authCookiesUrl) {
                        this._authCookiesUrl = opts.authCookiesUrl;
                        console.info("Sef Player: send a request to get the cookies");
                        ajax.request(opts.authCookiesUrl, {
                            method: "GET",
                            async: false,
                            onSuccess: util.bind(function (transport) {
                                if (transport.getResponseHeader("Set-Cookie")) {
                                    this._cookie = transport.getResponseHeader("Set-Cookie");
                                    _getDeviceAPI("storage").set("accedoDRMCookie", this._cookie);
                                    _getDeviceAPI("storage").save();
                                    console.info("Sef Player: get the set-cookies in response:" + this._cookie);
                                } else {
                                    _getDeviceAPI("storage").load();
                                    if (_getDeviceAPI("storage").get("accedoDRMCookie")) {
                                        this._cookie = _getDeviceAPI("storage").get("accedoDRMCookie");
                                    }
                                    console.info("Sef Player: no set cookie in response and try to get it from storage:" + this._cookie);
                                }

                                console.info("Sef Player: got cookie:" + this._cookie);
                            }, this),
                            onFailure: function (transport) {
                                //unable to get the response from the request and suppose to be wrong,throw error
                                console.error("Sef Player: fail to get the cookie" + transport);
                            }
                        });
                    } else {
                        //either already have no url or same url as before and then try to get the cookie from the storage
                        if (_getDeviceAPI("storage").get("accedoDRMCookie")) {
                            this._cookie = _getDeviceAPI("storage").get("accedoDRMCookie");
                        }

                        console.info("Sef Player: got cookie from storage:" + this._cookie);
                    }

                    mediaUrl += "|COMPONENT=" + component;

                    console.info("Sef Player: WMDRM turned on");

                } else if (opts.drm === "playready") {
                    tempStr = "";

                    if (opts.customData) {
                        this._playReadyCustomData = opts.customData;
                    }

                    if (opts.drmUrl) {
                        this._DRMServer = opts.drmUrl;
                    }

                    if (!util.isUndefined(opts.startBitrate)) {
                        tempStr += "|STARTBITRATE=" + opts.startBitrate;
                    }

                    if (!util.isUndefined(opts.bitrate)) {
                        tempStr += "|BITRATE=" + opts.bitrate;
                    }

                    if (!util.isUndefined(opts.skipBitrate)) {
                        tempStr += "|SKIPBITRATE=" + opts.skipBitrate;
                    }

                    if (!util.isUndefined(opts.cookie)) {
                        tempStr += "|COOKIE=" + opts.cookie;
                    }

                    if (!util.isUndefined(opts.timeOut)) {
                        tempStr += "|NETWORKTIMEOUT=" + opts.timeOut;
                    }

                    if (!util.isUndefined(opts.startFragment)) {
                        tempStr += "|STARTFRAGMENT=" + opts.startFragment;
                    }

                    mediaUrl += tempStr;

                    console.info("Sef Player: Sef Player: playReady turned on");

                }

                if (opts.type === "hls") {
                    component = "HLS";

                    bitrates = "";

                    if (opts.bitratesFrom && opts.bitratesTo && (opts.bitratesFrom) < (opts.bitratesTo)) {
                        bitrates += "|BITRATES=" + opts.bitratesFrom + "~" + opts.bitratesTo;
                    }

                    if (!util.isUndefined(opts.startBitrate)) {
                        bitrates += "|STARTBITRATE=" + opts.startBitrate;
                    }

                    mediaUrl += bitrates + "|COMPONENT=" + component;

                } else if (opts.type === "has") {
                    component = "HAS";

                    bitrates = "";

                    if (opts.bitratesFrom && opts.bitratesTo && (opts.bitratesFrom) < (opts.bitratesTo)) {
                        bitrates += "|BITRATES=" + opts.bitratesFrom + "~" + opts.bitratesTo;
                    }

                    if (!util.isUndefined(opts.startBitrate)) {
                        bitrates += "|STARTBITRATE=" + opts.startBitrate;
                    }


                    mediaUrl += bitrates + "|COMPONENT=" + component;
                }

                if (opts.drm === "verimatrix" && opts.type === "hls") {

                    mediaUrl += "|DRM_TYPE=VERIMATRIX";


                    console.info("__attemptPlay VERIMATRIX");

                    var serviceProvider = "";
                    var verimatrixProperties = "";

                    if (!util.isUndefined(opts.serviceProvider)) {
                        console.info("opts.serviceProvider is required by Verimatrix DRM");
                        serviceProvider = opts.serviceProvider;
                    }

                    if (!util.isUndefined(opts.iptvServerIp)) {
                        console.info("setting iptvServerIp " + opts.iptvServerIp);
                        verimatrixProperties += "i=" + opts.iptvServerIp;
                    }

                    if (!util.isUndefined(opts.webServerIp)) {
                        console.info("setting webServerIp " + opts.webServerIp);
                        verimatrixProperties += "|w=" + opts.webServerIp;
                    }

                    console.info("Sef Player: Initializing Verimatrix with data: serviceProvider:" + serviceProvider + " properties:" + verimatrixProperties);

                    this._plugin.Open("DRM", "1.000", "DRM");

                    var response = this._plugin.Execute("Initialize", 3, serviceProvider, verimatrixProperties);

                    console.info("DRM plugin initialization response: " + response);

                }

                if (opts.use3d) {
                    this._is3d = true;
                    this._3dType = "sideBySide";

                    if (opts.type3d) {
                        this._3dType = opts.type3d;
                    }
                } else {
                    this._is3d = false;
                    this._3dType = "off";
                }

                //if it is audio type,no matter whether it set the window size before,it will also change back to 0,0,0,0
                if (!util.isUndefined(opts.type) && opts.type === "mp3") {
                    this._currentWindowSize = this.AUDIO_SIZE;
                    this.setWindowSize(this._currentWindowSize);
                }

                this.url = mediaUrl;
                return mediaUrl;
            },
            /**
             * play the media
             * @method
             * @param {Object} opts object containing the options
             * @param {Number} opts.sec Play the video at the specified second
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            play: function (opts) {
                //disable the screen saver when playing
                _getDeviceAPI("system").setScreenSaver(false);
                console.info("Sef Player: play requested with url " + this.url);
                var playResult = this.__attemptPlay(opts);
                if (playResult === -1 || playResult === "-1" || playResult === false) {
                    sMedia._onError(PLAYBACK_ERRORS.GENERIC.LOAD);
                    return false;
                }

                //only change state when the video is stopped
                if (sMedia.isState(sMedia.STOPPED)) {
                    sMedia._onConnecting();

                    //create the connection timeout
                    this.show();
                    this.__createConnectionTimeout();
                }

                //Carry out 3D handling for 2011 devices
                if (this._is3d && _getDeviceAPI("id").getFirmwareYear() >= 2011) {
                    console.info("Sef Player: turning on 3D mode");
                    if (this._current3dType !== this._3dType) {
                        this._previous3dType = this._current3dType;
                    }
                    this._current3dType = this._3dType;

                    if ((globals.pluginObject3D.Flag3DEffectSupport && globals.pluginObject3D.Flag3DEffectSupport() === 1) || (globals.pluginObject3D.Flag3DTVConnect && globals.pluginObject3D.Flag3DTVConnect() === 1)) {

                        if (this._3dType === "sideBySide") {
                            this._plugin.SetPlayerProperty(2, "3", 1);
                        }

                    }
                }
                return playResult;
            },
            /**
             * Really calls plugin object to play the media
             * @method
             * @param {Object} opts object containing the options
             * @param {Number} [opts.sec] Play the video at the specified second
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            __attemptPlay: function (opts) {
                var ret, sec;
                opts = opts || {};
                sec = opts.sec || 0;

                // if the media is already playing, don't have to start playback
                // (reset playback speed to 1 if it is not)
                switch (sMedia.getState()) {

                case sMedia.PLAYING:
                case sMedia.SPEEDING:

                    if (!util.isUndefined(opts.sec)) {
                        this.seek(opts.sec);
                    }

                    console.info("[XDK] AVPlayer set the playback speed to 1 when pressing play");

                    if (this._playbackSpeed !== 1) {
                        this.speed(1);
                    }

                    return;

                case sMedia.PAUSED:

                    if (!util.isUndefined(opts.sec)) {
                        this.seek(opts.sec);
                    }
                    this.resume();

                    return;
                }

                // need to init player before setting screen size
                ret = this._plugin.Execute("InitPlayer", this.url);

                // some devices will make the video fullscreen when finished playback
                // so reset the window size to counter that glitch
                if (this._currentWindowSize) {
                    console.info("Sef Player: set the window size to " + this._currentWindowSize.width + "x" + this._currentWindowSize.height);
                    this.setWindowSize(this._currentWindowSize);
                }

                // playback from stopped
                this._withHoldingStop = false;
                this._playbackSpeed = 1;
                this._curTime = 0;
                this._duration = 0;
                this.__infoReady = false;


                console.info("Sef Player: will play from the beginning");

                if ("wmdrm" === this._drm) {
                    if (this._cookie) {
                        this._plugin.Execute("SetPlayerProperty", 1, this._cookie, this._cookie.length);
                        console.info("Sef Player: set the cookies to the player" + this._cookie);
                    }
                } else if ("playready" === this._drm) {

                    console.info("Sef Player: play PlayReady: " + this.url);

                    if (this._playReadyCustomData) {
                        this._plugin.Execute("SetPlayerProperty", 3, this._playReadyCustomData, this._playReadyCustomData.length);
                        console.info("Sef Player: set the custom data" + this._playReadyCustomData);
                    }

                    if (this._DRMServer) {
                        this._plugin.Execute("SetPlayerProperty", 4, this._DRMServer, this._DRMServer.length);
                        console.info("Sef Player: set the DRMServer " + this._DRMServer);
                    }

                }

                return this.__startPlayback(sec);
            },

            /**
             * Starts playback on the native player plugin.
             *
             * @method
             * @private
             * @param {Number} [sec]  Starting position of the playback. If not specified, the plugin would determine the position.
             * @returns {Number} The playback result code, -1 if failed.
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            __startPlayback: function (sec) {
                try {
                    if (util.isNumber(sec)) {
                        return this._plugin.Execute("StartPlayback", sec);
                    } else {
                        return this._plugin.Execute("StartPlayback");
                    }
                } catch (e) {
                    console.error("Play Ready Error: " + e.message);
                    return -1;
                }
            },
            /**
             * pause the video item
             * @method
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            pause: function () {
                this._plugin.Execute("Pause");

                sMedia._onPause();

                //enable the screen saver when it is paused
                _getDeviceAPI("system").setScreenSaver(true);
            },
            /**
             * stop the video item
             * @method
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            stop: function () {
                console.info("Sef player: stop requested");

                this.hide();
                this._stopPlayback();
                sMedia._onStopped();
                return true;
            },
            /**
             * stop the video playback for real
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _stopPlayback: function () {
                console.info("Sef Player: stop action enforced");
                if (this._plugin !== null) {

                    if (this._is3d && _getDeviceAPI("id").getFirmwareYear() >= 2011) {
                        console.info("Sef Player: turn on 3D mode");

                        if ((globals.pluginObject3D.Flag3DEffectSupport && globals.pluginObject3D.Flag3DEffectSupport() === 1) || (globals.pluginObject3D.Flag3DTVConnect && globals.pluginObject3D.Flag3DTVConnect() === 1)) {

                            this._plugin.Execute("SetPlayerProperty", 2, "3", 0);

                        }
                    }

                    this._plugin.Execute("Stop");
                    this.hide();
                }

                this.__removeConnectionTimeout();

                this._curTimeReported = false; // reset as no current playback time reported by plugin
                //enable the screen saver when it is stopped
                _getDeviceAPI("system").setScreenSaver(true);
            },
            /**
             * create the connection time out
             * @method  __createConnectionTimeout
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            __createConnectionTimeout: function () {
                this.connectionTimer = core.getGuid();
                console.info("Sef Player: create connection timeout");
                util.delay(this._connectionTimeLimit, this.connectionTimer).then(util.bind(function () {
                    console.info("Sef Player: connection timeout");
                    this._onConnectionTimeout();
                }, this)).done();
            },
            /**
             * remove connection timeout
             * @method __removeConnectionTimeout
             * @private
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            __removeConnectionTimeout: function () {
                //remove the connection time out when stop
                if (this.connectionTimer) {
                    util.clearDelay(this.connectionTimer);
                    console.info("Sef Player: remove connection timeout");
                    this.connectionTimer = null;
                }
            },
            /**
             * resume the video item
             * @method
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            resume: function () {
                this._plugin.Execute("Resume");

                // disable the screen saver when resume playing
                _getDeviceAPI("system").setScreenSaver(false);

                sMedia._onPlaying();

                if (this._playbackSpeed !== 1) {
                    this._playbackSpeed = 1;
                    this._plugin.Execute("SetPlaybackSpeed", this._playbackSpeed);
                }
            },
            /**
             * seek to a specific time
             * @method
             * @param {Number} sec the number of second you want to seek to (in sec)
             * @memberof ax/ext/device/samsung/SefPlayer#
             * @public
             */
            seek: function (sec) {
                //workaround to seek when speeding.
                if (sMedia.isState(sMedia.SPEEDING)) {
                    sec = Math.max(sec, 1);
                }
                //get current time in sec
                var curTime = this.getCurTime();
                this.skip(sec - curTime);
            },
            /**
             * skip the video for a few second
             * @method
             * @param {Number} sec number of second to skip (10 by default)
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            skip: function (sec) {
                //Note: haven't been test for non progressive usage.
                var boundaryTime = 1,
                    curTime = this.getCurTime(),
                    duration = this.getDuration();

                if (!sec) {
                    return;
                }

                //3000 because some video(http://av.vimeo.com/44206/716/14097677.mp4?aksessionid=5ac5cb7b639a84db8c1fd46388ca4b20&token=1361333279_7b2482145462029a47655992e4df9289) is unable to jumpForward properly on 2012BDP/2012TV.
                //If less than 3000, some videos will not jump but dispatch onRenderComplete event
                //To summarize, here is my testing result 1:04 video and 1000s
                //on 2010 TV will go to 0:58 and play so it doesn't matter on the boundary case
                //on 2011 TV will go properly and go to 1:03s and then play
                //on 2012 TV/BDP will go back to the previous skip time and then play a few second then suddenly jump to 1:04 then stop. So if set to 3s,it can jump successfully to the desired time and play.
                if (_getDeviceAPI("id").getFirmwareYear() === 2012) {
                    boundaryTime = 3;
                }


                if (curTime + sec > duration - boundaryTime) { // make sure do not skip outside of boundary
                    sec = duration - boundaryTime - curTime;
                } else if (curTime + sec < 0) {
                    sec = -curTime;
                }
                if (_getDeviceAPI("id").getFirmwareYear() <= 2011 && this._type !== "hls") {
                    sec = Math.ceil(sec * 0.95);
                    console.info("Sef Player: Trying to correct skip gap... gap is now:" + sec);
                }
                console.debug("Sef Player: Skip with gap: " + sec);
                if (!sec) { // request a 0 second jump to device does work well!
                    return;
                }
                if (sec < 0) {
                    this._plugin.Execute("JumpBackward", Math.round(sec) * -1);
                } else {
                    this._plugin.Execute("JumpForward", Math.round(sec));
                }
            },
            /**
             * set the playback speed of the media
             * @method
             * @param {Number} speed speed number, better to be multiple of 2 and within -16 to 16
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            speed: function (speed) {
                // does not work on paused state as well
                if (sMedia.isState(sMedia.PAUSED)) {
                    this._plugin.Execute("Resume");
                }

                // speeding in HLS is not possible due to device limitation
                if (this._type === "hls") {
                    console.warn("Sef Player:Speeding during HLS playback is not supported by device!");
                    //since it resume and change back to the state for playing
                    sMedia._onPlaying();
                    return;
                }

                speed = Math.floor(speed);

                if (speed > 16) {
                    speed = 16;
                }
                if (speed < -16) {
                    speed = -16;
                }

                this._playbackSpeed = speed;
                //Warning: not all device and media type would support this function
                if (speed === 1) {
                    this._plugin.Execute("SetPlaybackSpeed", 1);
                    sMedia._onPlaying();
                    return;
                }

                this._plugin.Execute("SetPlaybackSpeed", speed);
                sMedia._onSpeeding();
            },
            /**
             * get bitrate and available bitrates
             * @method
             * @return {ax/device/interface/Player~MediaBitrates}
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            getBitrates: function () {
                if (this._currentBitrate === null || this._availableBitrates === null) {
                    return false;
                }

                return {
                    currentBitrate: this._currentBitrate,
                    availableBitrates: this._availableBitrates
                };
            },
            /**
             * get and return playbackSpeed
             * @method
             * @return {Number} playback speed
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            getPlaybackSpeed: function () {
                return this._playbackSpeed;
            },
            /**
             * set the current time
             * @method
             * @param {Number} time current time in milliseconds
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _setCurTime: function (time) {
                var curBitrate, availBitrateString, sortNumeric, availBitrate, i;

                console.info("Sef Player: set current time to:" + time);
                this._curTime = time;
                sMedia._onTimeUpdate(time / 1000);

                //3D handling for 2010 devices
                //@TODO
                /*
                     if (id.getFirmwareYear() < 2011) {
                     if (time > 0 && time < 5000 && this._is3d) {
                     console.info("Sef Player: turn on 3D mode");
                     
                     //set 3d type
                     if (this._current3dType !== this._3dType) {
                     //check whether it is 2d to 3d or off currently
                     current3dEffect = globals.pluginObject3D.Get3DEffectMode();
                     if (current3dEffect === 7) {
                     this._current3dType = "2dTo3d";
                     }else{
                     this._current3dType = "off";
                     }
                     this._previous3dType = this._current3dType;
                     }
                     this._current3dType = this._3dType;
                     
                     accedo.device.threeD.switch3DmodeFor2010Device();
                     
                     } else if(time > 0 && time < 5000  && !this._is3d && this._current3dType === 2) {
                     console.info("Sef Player: turn off 3D mode");
                     console.info("Sef Player: previous3D: " + this._previous3dType + " currentMode: " + this._current3dType);
                     if (this._previous3dType === "2dTo3d") {
                     this._previous3dType =  this._current3dType;
                     this._current3dType = "2dTo3d";
                     } else {
                     this._previous3dType = this._current3dType;
                     this._current3dType = "off";
                     }
                     
                     accedo.device.threeD.switch3DmodeFor2010Device();
                     console.info("Sef Player: after >> previous3D: " + this._previous3dType + " currentMode: " + this._current3dType);
                     }
                     }*/

                if (sMedia.isState(sMedia.BUFFERING) || sMedia.isState(sMedia.STOPPED) || sMedia.isState(sMedia.PAUSED)) {
                    return;
                }

                //upagte bitrates
                curBitrate = this._plugin.Execute("GetCurrentBitrates");

                if (typeof curBitrate === "number") {
                    this._currentBitrate = parseInt(curBitrate, 10);
                }
                availBitrateString = this._plugin.Execute("GetAvailableBitrates");

                sortNumeric = function (a, b) {
                    return Number(a) - Number(b);
                };

                if (typeof availBitrateString === "string") {
                    availBitrate = availBitrateString.split("|");
                    for (i = 0; i < availBitrate.length - 1; i++) {
                        availBitrate[i] = parseInt(availBitrate[i], 10);
                    }
                    this._availableBitrates = availBitrate.sort(sortNumeric);
                }
            },
            /**
             * Returns the current time (0 if never played)
             * @method
             * @param {boolean} seconds When true, the return value is in seconds
             * @returns {Integer|false} current time in milliseconds (or seconds according to the 'seconds' param)
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            getCurTime: function () {
                return Math.floor(this._curTime / 1000);
            },
            /**
             * set the total time by reading the media's duration. It will be called when the media is loaded
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _setDuration: function () {
                this._duration = this._plugin.Execute("GetDuration");
            },
            /**
             * get total time of the media
             * @method
             * @public
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            getDuration: function () {
                return Math.floor(this._duration / 1000);
            },
            /**
             * function to be triggered by the plugin player when the current playback time is progressing
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onCurrentPlayTime: function (time) {
                this._setCurTime(time);
                if (this._curTimeReported) {
                    return;
                }
                console.info("Sef Player: plugin reported current playback time for the first time.");
                this._curTimeReported = true; // indicate current playback time has been reported by plugin
            },
            /**
             * function to be triggered by the plugin player when the stream is not found
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onStreamNotFound: function () {
                console.info("Sef Player: onStreamNotFound");
                //enable the screen saver since it is not playing any video
                _getDeviceAPI("system").setScreenSaver(true);
                sMedia._onError(PLAYBACK_ERRORS.NETWORK.FILE);
            },
            /**
             * function to be triggered when the connection is timed out
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onConnectionTimeout: function () {
                console.info("Sef Player: onConnectionTimeout");
                // this._stopPlayback(); //Ho: yet to decide use _stopPlayback or not

                //enable the screen saver since it is not playing any video
                _getDeviceAPI("system").setScreenSaver(true);
                sMedia._onError(PLAYBACK_ERRORS.NETWORK.TIMEOUT);
            },
            /**
             * function to be triggered by the plugin player when the connection is failed
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onConnectionFailed: function () {
                console.info("Sef Player: onConnectionFailed");
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.NETWORK.FAILED);
            },
            /**
             * function to be triggered by the plugin player when there is rendering error
             * @method
             * @param {Number} param the parameter
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onRenderError: function (param) {
                console.info("Sef Player: onRenderError " + param);
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.RENDER.FAILED, null, param);
            },
            /**
             * function to be triggered by the plugin player when there is custom error from external plugin like widevine
             * @method
             * @param {Integer} error code ID
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onUnknownEvent: function (eventType, param1, param2) {
                console.info("Sef Player: onUnknownEvent " + eventType + "with the following parameters" + param1 + "," + param2);
            },
            /**
             * function to be triggered by the plugin player when there is authentication failure
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onAuthenticationFailed: function () {
                console.info("Sef Player: [xdk Samsung] onAuthenticationFailed ");
                this._stopPlayback();
                sMedia._onError(PLAYBACK_ERRORS.DRM.FAILED);
            },
            /**
             * function to be triggered by the plugin player the network is disconnected
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onNetworkDisconnected: function () {
                console.info("Sef Player: [xdk Samsung] nNetworkDisconnected ");

                //as video playback cannot be resumed/recovered. Stop the playback.
                this._stopPlayback();

                sMedia._onError(PLAYBACK_ERRORS.NETWORK.DISCONNECTED);
            },
            /**
             * function to be triggered by the plugin player when the media rendering is complete
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onRenderingComplete: function () {
                this._stopPlayback();
                sMedia._onFinish();
            },
            /**
             * function to be triggered by the plugin player when the buffering is in progress
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onBufferingProgress: function () {
                if (this._withHoldingStop === true) {
                    console.info("Sef Player: carry out withholding stop action");
                    this.stop();
                    this._withHoldingStop = false;
                    return;
                }

                sMedia._onBufferingProgress();
            },
            /**
             * function to be triggered by the plugin player when the buffering is started
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onBufferingStart: function () {
                if (this._withHoldingStop === true) {
                    console.info("Sef Player: carry out withholding stop action");
                    this.stop();
                    this._withHoldingStop = false;
                    return;
                }

                sMedia._onBufferingStart();
            },
            /**
             * function to be triggered by the plugin player when the buffering is completed
             * @method
             * @protected
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            _onBufferingComplete: function () {
                sMedia._onBufferingFinish();

                if (sMedia.isState(sMedia.STOPPED)) {
                    return;
                }

                var self = this;

                // hackish workaround for 2010 BDP refuses to start playback (bug XDK-322)
                if (_getDeviceAPI("id").getHardwareType() === IId.HARDWARE_TYPE.BD && _getDeviceAPI("id").getFirmwareYear() === 2010 && !this._curTimeReported) {
                    console.info("Sef Player: 2010 BDP playback failure detection in effect!");
                    util.delay(function () {
                        if (!self._curTimeReported) { // still not start playback at device side
                            console.info("Sef Player: 2010 BDP playback failure detected! Work-around action taken!");
                            self.pause();
                            self.resume();
                        } else {
                            console.info("Sef Player: 2010 BDP playback success detected! Work-around action not taken.");
                        }
                    }, 1.5);
                }
            },
            /**
             * Get back the path
             * @method
             * @public
             * @return {String} the path of the player
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            getId: function () {
                return this.__id;
            },
            /**
             * set the id of the player
             * @method
             * @public
             * @param {String} id the path of the player
             * @memberof ax/ext/device/samsung/SefPlayer#
             */
            setId: function (id) {
                this.__id = id;
            }
        });
    return sefClass;
});
/**
 * Detection of Samsung by checking the userAgent for "maple" Return true if it is samsung
 * @module ax/ext/device/samsung/detection
 */
define("ax/ext/device/samsung/detection", [], function () {
    "use strict";
    var detection = (function () {
        var agent = navigator.userAgent.toLowerCase();
        if (agent.indexOf("maple") !== -1) {
            return true;
        }
        return false;
    })();
    return detection;
});
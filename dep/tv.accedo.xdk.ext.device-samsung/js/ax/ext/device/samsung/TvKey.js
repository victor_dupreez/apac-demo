/**
 * TvKey class to handle the key
 * @class ax/ext/device/samsung/TvKey
 * @extends ax/device/AbstractTvKey
 */
define("ax/ext/device/samsung/TvKey", [
    "ax/device/shared/browser/TvKey",
    "ax/class",
    "ax/console",
    "ax/device/vKey",
    "ax/util",
    "ax/ext/device/samsung/globals",
    "ax/device/interface/Id",
    "require"
], function (
    browserTvKey,
    klass,
    console,
    VKey,
    util,
    global,
    IId,
    require) {
    "use strict";
    return klass.create(browserTvKey, {
        /**
         * @name CH_UP
         * @property {String} id Virtual Key ID. "samsung:vkey:chup"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        CH_UP: {
            id: "samsung:vkey:chup"
        },
        /**
         * @name CH_DOWN
         * @property {String} id Virtual Key ID. "samsung:vkey:chdown"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        CH_DOWN: {
            id: "samsung:vkey:chdown"
        },
        /**
         * @name TOOLS
         * @property {String} id Virtual Key ID. "samsung:vkey:tools"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        TOOLS: {
            id: "samsung:vkey:tools"
        },
        /**
         * @name INFO
         * @property {String} id Virtual Key ID. "samsung:vkey:info"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        INFO: {
            id: "samsung:vkey:info"
        },
        /**
         * @name EMODE
         * @property {String} id Virtual Key ID."samsung:vkey:emode"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        EMODE: {
            id: "samsung:vkey:emode"
        },
        /**
         * @name DMA
         * @property {String} id Virtual Key ID. "samsung:vkey:dma"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        DMA: {
            id: "samsung:vkey:dma"
        },
        /**
         * @name PRECH
         * @property {String} id Virtual Key ID."samsung:vkey:prech"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        PRECH: {
            id: "samsung:vkey:prech"
        },
        /**
         * @name FAVCH
         * @property {String} id Virtual Key ID. "samsung:vkey:favch"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        FAVCH: {
            id: "samsung:vkey:favch"
        },
        /**
         * @name CHLIST
         * @property {String} id Virtual Key ID. "samsung:vkey:chlist"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        CHLIST: {
            id: "samsung:vkey:chlist"
        },
        /**
         * @name TTX_MIX
         * @property {String} id Virtual Key ID. "samsung:vkey:ttxmix"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        TTX_MIX: {
            id: "samsung:vkey:ttxmix"
        },
        /**
         * @name GUIDE
         * @property {String} id Virtual Key ID. "samsung:vkey:guide"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        GUIDE: {
            id: "samsung:vkey:guide"
        },
        /**
         * @name SUBTITLE
         * @property {String} id Virtual Key ID.  "samsung:vkey:subtitle"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        SUBTITLE: {
            id: "samsung:vkey:subtitle"
        },
        /**
         * @name ASPECT
         * @property {String} id Virtual Key ID. "samsung:vkey:aspect"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        ASPECT: {
            id: "samsung:vkey:aspect"
        },
        /**
         * @name  DOLBY_SRR
         * @property {String} id Virtual Key ID. "samsung:vkey:dolbysrr"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        DOLBY_SRR: {
            id: "samsung:vkey:dolbysrr"
        },
        /**
         * @name MTS
         * @property {String} id Virtual Key ID. "samsung:vkey:mts"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        MTS: {
            id: "samsung:vkey:mts"
        },
        /**
         * @name WHEELDOWN
         * @property {String} id Virtual Key ID. "samsung:vkey:wheeldown"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        WHEELDOWN: {
            id: "samsung:vkey:wheeldown"
        },
        /**
         * @name WHEELUP
         * @property {String} id Virtual Key ID. "samsung:vkey:wheelup"
         * @memberof ax/ext/device/samsung/TvKey#
         */
        WHEELUP: {
            id: "samsung:vkey:wheelup"
        }
    }, {
        _keyMapping: null,
        init: function () {
            this._keyMapping = {
                VKey: {}
            };
            this._keyMapping.VKey[global.tvKey.KEY_UP] = VKey.UP; //"up";
            this._keyMapping.VKey[global.tvKey.KEY_DOWN] = VKey.DOWN; //"down";
            this._keyMapping.VKey[global.tvKey.KEY_LEFT] = VKey.LEFT; //"left";
            this._keyMapping.VKey[global.tvKey.KEY_RIGHT] = VKey.RIGHT; //"right";
            this._keyMapping.VKey[global.tvKey.KEY_ENTER] = VKey.OK; //"ok";
            this._keyMapping.VKey[global.tvKey.KEY_RED] = VKey.RED; //"red";
            this._keyMapping.VKey[global.tvKey.KEY_GREEN] = VKey.GREEN; //"green";
            this._keyMapping.VKey[global.tvKey.KEY_YELLOW] = VKey.YELLOW; //"yellow";
            this._keyMapping.VKey[global.tvKey.KEY_BLUE] = VKey.BLUE; //"blue";
            this._keyMapping.VKey[global.tvKey.KEY_RETURN] = VKey.BACK; //"back";
            this._keyMapping.VKey[global.tvKey.KEY_EXIT] = VKey.EXIT; //"exit";
            this._keyMapping.VKey[global.tvKey.KEY_PLAY] = VKey.PLAY; //"play";
            this._keyMapping.VKey[global.tvKey.KEY_PAUSE] = VKey.PAUSE; //"pause";
            this._keyMapping.VKey[global.tvKey.KEY_STOP] = VKey.STOP; //"stop";
            this._keyMapping.VKey[global.tvKey.KEY_FF] = VKey.FF; //"ff";
            this._keyMapping.VKey[global.tvKey.KEY_RW] = VKey.RW; //"rw";

            this._keyMapping.VKey[global.tvKey.KEY_FF_] = VKey.NEXT; //"next";
            this._keyMapping.VKey[global.tvKey.KEY_RW_] = VKey.PREV; //"prev";
            this._keyMapping.VKey[global.tvKey.KEY_REWIND_] = VKey.PREV; //"prev";

            this._keyMapping.VKey[global.tvKey.KEY_0] = VKey.KEY_0; //"0";
            this._keyMapping.VKey[global.tvKey.KEY_1] = VKey.KEY_1; //"1";
            this._keyMapping.VKey[global.tvKey.KEY_2] = VKey.KEY_2; //"2";
            this._keyMapping.VKey[global.tvKey.KEY_3] = VKey.KEY_3; //"3";
            this._keyMapping.VKey[global.tvKey.KEY_4] = VKey.KEY_4; //"4";
            this._keyMapping.VKey[global.tvKey.KEY_5] = VKey.KEY_5; //"5";
            this._keyMapping.VKey[global.tvKey.KEY_6] = VKey.KEY_6; //"6";
            this._keyMapping.VKey[global.tvKey.KEY_7] = VKey.KEY_7; //"7";
            this._keyMapping.VKey[global.tvKey.KEY_8] = VKey.KEY_8; //"8";
            this._keyMapping.VKey[global.tvKey.KEY_9] = VKey.KEY_9; //"9";
            this._keyMapping.VKey[global.tvKey.KEY_CH_UP] = this.constructor.CH_UP; //channel up
            this._keyMapping.VKey[global.tvKey.KEY_CH_DOWN] = this.constructor.CH_DOWN; //channel down

            // Key support for TV panel
            this._keyMapping.VKey[global.tvKey.KEY_PANEL_CH_UP] = this._keyMapping.VKey[global.tvKey.KEY_UP]; // same as KEY_UP
            this._keyMapping.VKey[global.tvKey.KEY_PANEL_CH_DOWN] = this._keyMapping.VKey[global.tvKey.KEY_DOWN]; // same as KEY_DOWN
            this._keyMapping.VKey[global.tvKey.KEY_PANEL_VOL_UP] = this._keyMapping.VKey[global.tvKey.KEY_RIGHT]; // same as KEY_RIGHT
            this._keyMapping.VKey[global.tvKey.KEY_PANEL_VOL_DOWN] = this._keyMapping.VKey[global.tvKey.KEY_LEFT]; // same as KEY_LEFT
            this._keyMapping.VKey[global.tvKey.KEY_PANEL_SOURCE] = this._keyMapping.VKey[global.tvKey.KEY_ENTER]; // same as KEY_OK
            this._keyMapping.VKey[global.tvKey.KEY_PANEL_MENU] = this._keyMapping.VKey[global.tvKey.KEY_RETURN]; // same as KEY_BACK

            //According to http://www.samsungdforum.com/Guide/art00046/index.html, we will register full widget key and those keys are able to be captured.
            //Some keys are excluded, KEY_VOL_UP, KEY_VOL_DOWN, KEY_MUTE, KEY_SOURCE, KEY_MENU, KEY_POWER, KEY_INFOLINK, KEY_CONTENT, KEY_WLINK are not registered 
            //since it should be preserved as default behaviour.
            this._keyMapping.VKey[global.tvKey.KEY_TOOLS] = this.constructor.TOOLS;
            this._keyMapping.VKey[global.tvKey.KEY_INFO] = this.constructor.INFO;
            this._keyMapping.VKey[global.tvKey.KEY_EMODE] = this.constructor.EMODE;
            this._keyMapping.VKey[global.tvKey.KEY_DMA] = this.constructor.DMA;
            this._keyMapping.VKey[global.tvKey.KEY_PRECH] = this.constructor.PRECH;
            this._keyMapping.VKey[global.tvKey.KEY_FAVCH] = this.constructor.FAVCH;
            this._keyMapping.VKey[global.tvKey.KEY_CHLIST] = this.constructor.CHLIST;
            this._keyMapping.VKey[global.tvKey.KEY_TTX_MIX] = this.constructor.TTX_MIX;
            this._keyMapping.VKey[global.tvKey.KEY_GUIDE] = this.constructor.GUIDE;
            this._keyMapping.VKey[global.tvKey.KEY_SUBTITLE] = this.constructor.SUBTITLE;
            this._keyMapping.VKey[global.tvKey.KEY_ASPECT] = this.constructor.ASPECT;
            this._keyMapping.VKey[global.tvKey.KEY_DOLBY_SRR] = this.constructor.DOLBY_SRR;
            this._keyMapping.VKey[global.tvKey.KEY_MTS] = this.constructor.MTS;
            this._keyMapping.VKey[global.tvKey.KEY_WHEELDOWN] = this.constructor.WHEEL_DOWN;
            this._keyMapping.VKey[global.tvKey.KEY_WHEELUP] = this.constructor.WHEEL_UP;

            //XDK-2075 Although it is not included in the global tvkey, it is the same for various TV.(it will dispatch when long press ff/rw)
            this._keyMapping.VKey[1080] = VKey.PREV;
            this._keyMapping.VKey[1078] = VKey.NEXT;

            this.initKeyMapping(this._keyMapping);
        },
        getVirtualKey: function (evt) {
            if (this._keyMapping.VKey[evt.keyCode]) {
                switch (evt.keyCode) {
                case global.tvKey.KEY_POWER:
                case global.tvKey.KEY_EXIT:
                case global.tvKey.KEY_PANEL_VOL_DOWN:
                case global.tvKey.KEY_PANEL_VOL_UP:
                case global.tvKey.KEY_PANEL_CH_UP:
                case global.tvKey.KEY_PANEL_CH_DOWN:
                case global.tvKey.KEY_PANEL_MENU:
                case global.tvKey.KEY_PANEL_SOURCE:
                case global.tvKey.KEY_RETURN:
                    global.widgetAPI.blockNavigation(evt);
                    break;
                }
            }
            return this._super(evt);
        },
        /**
         * To handle the initial key handling
         * @protected
         * @method initKeyHandling
         * @memberof ax/ext/device/samsung/TvKey#
         */
        initKeyHandling: function () {
            var device = require("ax/device"),
                year = device.id.getFirmwareYear(),
                type = device.id.getHardwareType();

            //only handle keydown in ss
            document.onkeydown = util.bind(function (evt) {
                return this.keyHandler(evt);
            }, this);

            // hack for 2011 device"s smart-hub key exits to TV channel
            if (year === 2011) {
                // for 2011 device, doing below assignment will actually add another event listener
                document.onkeydown = function (evt) {
                    console.log("samsung on key down");
                    // 2011's smart-hub key is bugged, it exits to channel, so need to handle ourselves
                    if (evt.keyCode === global.tvKey.KEY_INFOLINK || evt.keyCode === global.tvKey.KEY_CONTENT) {
                        console.debug("[XDK]Smart-hub key receiced! Exiting application...");
                        device.system.exit({
                            toTV: false
                        });
                        evt.preventDefault();
                        return false;
                    }
                    return true;
                };
            }

            //Ensure those keys are registered and unregistered
            util.delay(1).then(function () {
                // bring global variable into local scope for performance
                var ac = global.appCommon;

                //we will use samsung registFullWidgetKey to register all the common keys.
                // commons keys other that will be registered here.
                ac.RegisterKey(global.tvKey.KEY_PANEL_CH_UP);
                ac.RegisterKey(global.tvKey.KEY_PANEL_CH_DOWN);
                ac.RegisterKey(global.tvKey.KEY_PANEL_VOL_UP);
                ac.RegisterKey(global.tvKey.KEY_PANEL_VOL_DOWN);
                ac.RegisterKey(global.tvKey.KEY_PANEL_ENTER);
                ac.RegisterKey(global.tvKey.KEY_PANEL_SOURCE);
                ac.RegisterKey(global.tvKey.KEY_PANEL_MENU);
                ac.RegisterKey(global.tvKey.KEY_EXIT);

                // 2013
                global.pluginAPI.registKey(global.tvKey.KEY_TOOLS);

                //register the channel up and down keys, which doesn't work on samsung 2012
                //and some 2013 TV, so register for all devices

                ac.RegisterKey(global.tvKey.KEY_CH_DOWN);
                ac.RegisterKey(global.tvKey.KEY_CH_UP);

                if (year <= 2012) {
                    if (type === IId.HARDWARE_TYPE.TV && year <= 2010) {
                        ac.UnregisterKey(global.tvKey.KEY_WLINK); // Smart-hub key another version
                    } else {
                        ac.RegisterKey(global.tvKey.KEY_WLINK); // Smart-hub key another version
                    }
                    ac.RegisterKey(global.tvKey.KEY_CONTENT); // Smart-hub key
                    ac.RegisterKey(global.tvKey.KEY_INFOLINK); // Smart-hub key another version
                } else {
                    ac.UnregisterKey(global.tvKey.KEY_INFOLINK);
                    ac.UnregisterKey(global.tvKey.KEY_CONTENT);
                    //ac.UnregisterKey(global.tvKey.KEY_WLINK); //2013 will unregister tools key
                }

                //To Ensure the following keys are not registered to use the default behaviour.
                ac.UnregisterKey(global.tvKey.KEY_VOL_UP);
                ac.UnregisterKey(global.tvKey.KEY_VOL_DOWN);
                ac.UnregisterKey(global.tvKey.KEY_MUTE);
                ac.UnregisterKey(global.tvKey.KEY_POWER);
                ac.UnregisterKey(global.tvKey.KEY_SOURCE);
                ac.UnregisterKey(global.tvKey.KEY_MENU);
            }).done();
        }
    });
});
/**
 * PrivilegedAPI will store those functions that are unable to pass the security check
 * @class ax/ext/device/samsung/PrivilegedAPI
 */
define("ax/ext/device/samsung/PrivilegedAPI", ["ax/class", "ax/ext/device/samsung/globals", "ax/console"], function (klass, Global, console) {
    "use strict";
    return klass.create({
        /**
         * get SDI_ID is unable to pass the security check on 2P012,
         * @private
         * @memberof ax/ext/device/samsung/PrivilegedAPI#
         */
        _getSDIID: function () {
            if (parseInt(Global.NNaviPlugin.GetFirmware().substr(10, 4), 10) >= 2011) {
                try {
                    var result = Global.widevinePlugin.GetSDI_ID();
                    if (result !== "ERROR") {
                        console.debug("[XDK] widevine sdi_id: " + result);
                        return result;
                    }
                } catch (e) {
                    console.warn("[XDK] error to get widevine sdi_id: ");
                }
            }
            return false;
        },
        /**
         * get widevine ESN is unable to pass the security check on 2012,
         * @private
         * @memberof ax/ext/device/samsung/PrivilegedAPI#
         */
        _getESN: function () {
            try {
                var result = Global.widevinePlugin.GetESN("WIDEVINE");
                console.debug("[XDK] Get the widevine ESN " + result);
                return result;

            } catch (e) {
                console.warn("[XDK] error to get widevine ESN " + this.__widevineESN);
            }
            return false;
        }
    }, {});
});
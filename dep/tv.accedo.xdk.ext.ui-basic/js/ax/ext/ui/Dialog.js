/**
 * A dialog widget for displaying content in a popup.
 *
 * ###Dom Structure
 *      <div class="wgt-dialog wgt-dialog-modal">
 *          <div class="wgt-dialog-overlay"></div>
 *          <div class="wgt-dialog-container">
 *              <div class="wgt-dialog-header"></div>
 *              <div class="wgt-dialog-content">
 *                  <div class="wgt-label">Dialog content...</div>
 *              </div>
 *              <div class="wgt-dialog-footer">
 *                  <div class="wgt-layout align-h">
 *                       <div class="wgt-button row-begin row-end" style="clear: left; float: left;">
 *                           <div class="wgt-label">Close</div>
 *                       </div>
 *                 </div>
 *             </div>
 *          </div>
 *      </div>
 *
 * ###CSS
 * ####Structural CSS
 *      //To set the overlay
 *     .wgt-dialog, .wgt-dialog-overlay, .wgt-dialog-container {
 *         position: absolute;
 *      }
 *
 *      //To set the overlay and modal to be 100% and cover the whole page
 *      .wgt-dialog.wgt-dialog-modal, .wgt-dialog-overlay {
 *          top: 0;
 *          left: 0;
 *          width: 100%;
 *          height: 100%;
 *       }
 *
 *       //Default background with gray color
 *       .wgt-dialog-overlay{
 *            background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAAlwSFlzAAALEwAACxMBAJqcGAAAAA1JREFUCNdjYGBgMAYAADgANNF/knAAAAAASUVORK5CYII=);
 *        }
 * #####Customizable CSS
 * Developer may need to set the following css to make it works properly.
 *
 * * .wgt-dialog-container - set the size of the container
 * * .wgt-dialog-header - set the size of header of dialog
 * * .wgt-dialog-content - set the size the content of dialog
 * * .wgt-dialog-footer - set the size the footer of dialog
 * @class ax/ext/ui/Dialog
 * @extends ax/af/Container
 * @param {Object} opts The options object
 * @param {Boolean} [opts.modal] Whether this is a modal dialog, blocking all other sibling contents, default value is true
 * @param {Boolean} [opts.autoOpen] Whether the dialog is auto-opened upon init, default value is true
 * @param {String} [opts.title] The title text
 * @param {ax/af/Component} [opts.header] The header component to set. If this option is set, opts.title will be ignored
 * @param {String|ax/af/Component} [opts.content] The content component or text to set.
 * @param {Array} [opts.buttons] The buttons to go into footer. See example for detailed usage
 * @param {String} opts.buttons.text The button text
 * @param {Function} opts.buttons.click The button click event callback. "this" variable in the function will be pointing to the dialog instance.
 * @param {String} [opts.closeText] If set, a close button with this text will appear in the footer
 * @param {ax/af/Component} [opts.footer] The footer component to set. If this option is set, opts.buttons and opts.closeText will be ignored
 * @param {Boolean} [opts.isPureText] True if the text options are all pure text, default value is false
 * @param {String} [otps.dialogCss] The CSS class to add for the dialog container
 * @param {Boolean} [otps.horizontalFooter] If false, the buttons in the footer will be aligned vertically. Default value is true.
 * @example
 * var myDialog = new Dialog({
 *   title: "Dialog Title",
 *   closeText: "Close me", // we add another button, which is a close button
 *   content: "some content text goes here...",
 *   buttons: [
 *      {
 *          text: "Cancel",
 *          click: function(){
 *              this.close();
 *              //do something more...
 *          }
 *      },{
 *          text: "Do something else",
 *          click: function(){
 *              // do something else
 *          }
 *      }
 *   ]
 * });
 */
define("ax/ext/ui/Dialog", [
	"ax/class",
	"ax/af/Container",
	"ax/af/focusManager",
	"ax/ext/ui/Layout",
	"ax/ext/ui/Button",
	"ax/ext/ui/Label",
	"ax/util",
	"ax/af/evt/type",
	"ax/console",
	"ax/device/vKey",
	"require",
	"css!./css/Dialog"
], function (
	klass,
	Container,
	fm,
	Layout,
	Button,
	Label,
	util,
	evtType,
	console,
	vKey,
	require) {
	"use strict";
	return klass.create(Container, {}, {
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_headerComp: null,
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_contentComp: null,
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_footerComp: null,
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_overlay: null,
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_view: null,
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_container: null,
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_isOpen: false,
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_buttons: null,
		/**
		 * @private
		 * @memberof ax/ext/ui/Dialog#
		 */
		_lastFocus: null,
		/**
		 * Initiates the dialog
		 * @memberof ax/ext/ui/Dialog#
		 * @private
		 */
		init: function (opts) {
			opts = opts || {};

			opts.focusable = false;

			if (util.isUndefined(opts.forwardFocus)) {
				opts.forwardFocus = true;
			}

			opts.dialogCss = opts.dialogCss || "";
			this._super(opts);
			this._root.addClass("wgt-dialog");


			if (opts.modal !== false) {
				this._root.addClass("wgt-dialog-modal");
				this._overlay = new Container({
					parent: this,
					css: "wgt-dialog-overlay"
				});
			}

			if (opts.horizontalFooter !== false) {
				opts.horizontalFooter = true;
			}

			this._headerComp = new Container({
				css: "wgt-dialog-header",
				nextDown: util.bind(function () {
					return this._contentComp;
				}, this)
			});
			this._contentComp = new Container({
				css: "wgt-dialog-content",
				nextUp: util.bind(function () {
					return this._headerComp;
				}, this),
				nextDown: util.bind(function () {
					return this._footerComp;
				}, this)
			});
			this._footerComp = new Container({
				css: "wgt-dialog-footer",
				nextUp: util.bind(function () {
					return this._contentComp;
				}, this)
			});
			this._container = new Container({
				type: Container,
				parent: this,
				css: "wgt-dialog-container " + opts.dialogCss,
				children: [this._headerComp, this._contentComp, this._footerComp]
			});


			var self = this,
				header = opts.header,
				content = opts.content,
				footer = opts.footer,
				buttons, button;

			// build header
			if (util.isString(opts.title)) {
				header = new Label({
					text: opts.title,
					isPureText: opts.isPureText
				});
			}

			if (header) {
				this._headerComp.attach(header);
			}

			//  build content
			if (util.isString(content)) {
				content = new Label({
					text: opts.content,
					isPureText: opts.isPureText
				});
			}
			if (content) {
				this._contentComp.attach(content);
				this._contentComp.setOption("forwardFocus", content);
				this.setOption("forwardFocus", this._contentComp);
			} else {
				console.info("Dialog widget: content is set to empty!");
			}

			// build footer
			if (opts.closeText) {
				opts.buttons = opts.buttons || [];
				opts.buttons.push({
					text: opts.closeText,
					click: function () {
						this.close();
					}
				});
			}
			if (!footer && opts.buttons && opts.buttons.length) {
				buttons = [];
				util.each(opts.buttons, function (buttonOpt) {
					button = new Button({
						text: buttonOpt.text,
						isPureText: opts.isPureText
					});
					button.addEventListener(evtType.CLICK, util.bind(buttonOpt.click, self));
					buttons.push(button);
				});

				footer = new Layout({
					alignment: opts.horizontalFooter ? Layout.HORIZONTAL : Layout.VERTICAL,
					fowardFocus: true,
					children: buttons
				});

				footer.setOption("forwardFocus", buttons[0]);

				this._buttons = buttons;
			}
			if (footer) {
				this._footerComp.attach(footer);
				this._footerComp.setOption("forwardFocus", footer);
				this.setOption("forwardFocus", this._footerComp); // favor focus on footer than content
			}

			if (opts.autoOpen !== false) {
				this.open();
			}

			this.addEventListener(evtType.KEY, this._keyHandler);
		},
		/**
		 * Adds an item to the header of the dialog
		 * @method
		 * @memberof ax/ext/ui/Dialog#
		 * @param {ax/af/Component} child - The child component to be attached
		 * @param {ax/af/Component.PLACE_BEFORE | ax/af/Component.PLACE_AFTER | ax/af/Component.PLACE_APPEND | ax/af/Component.PLACE_PREPEND}
		 * [placement] - position the child should be placed. By default {@link ax/af/Container.PLACE_APPEND}.
		 * @param {ax/af/Component} [marker] - must be a child of current container,
		 * the placement of child will be in relative to this marker.
		 * @public
		 */
		attachToHeader: function (child, placement, marker) {
			this._headerComp.attach(child, placement, marker);
		},
		/**
		 * Adds an item to the content container of the dialog
		 * @method
		 * @memberof ax/ext/ui/Dialog#
		 * @param {ax/af/Component} child - The child component to be attached
		 * @param {ax/af/Component.PLACE_BEFORE | ax/af/Component.PLACE_AFTER | ax/af/Component.PLACE_APPEND | ax/af/Component.PLACE_PREPEND}
		 * [placement] - position the child should be placed. By default {@link ax/af/Container.PLACE_APPEND}.
		 * @param {ax/af/Component} [marker] - must be a child of current container,
		 * the placement of child will be in relative to this marker.
		 * @public
		 */
		attachToContent: function (child, placement, marker) {
			this._contentComp.attach(child, placement, marker);
		},
		/**
		 * adds an item to the footer of the dialog
		 * @method
		 * @memberof ax/ext/ui/Dialog#
		 * @param {ax/af/Component} child - The child component to be attached
		 * @param {ax/af/Component.PLACE_BEFORE | ax/af/Component.PLACE_AFTER | ax/af/Component.PLACE_APPEND | ax/af/Component.PLACE_PREPEND}
		 * [placement] - position the child should be placed. By default {@link ax/af/Container.PLACE_APPEND}.
		 * @param {ax/af/Component} [marker] - must be a child of current container,
		 * the placement of child will be in relative to this marker.
		 * @public
		 */
		attachToFooter: function (child, placement, marker) {
			this._footerComp.attach(child, placement, marker);
		},
		/**
		 * Open the dialog
		 * @method
		 * @memberof ax/ext/ui/Dialog#
		 * @public
		 */
		open: function () {
			var parent = this._opts.parent;
			parent = parent || (this.getRootController() || require("ax/af/mvc/AppRoot").singleton()).getView();
			parent.attach(this);

			this._isOpen = true;

			this.dispatchEvent(evtType.OPEN);
			if (this._buttons && this._buttons.length) {
				this._lastFocus = fm.getCurFocus();
				fm.focus(this);
			} else {
				this.setActive();
			}
		},
		/**
		 * Close the dialog
		 * @method
		 * @memberof ax/ext/ui/Dialog#
		 * @public
		 */
		close: function () {
			this.detach();
			this._isOpen = false;
			this.dispatchEvent(evtType.CLOSE);
			if (this._lastFocus) {
				fm.focus(this._lastFocus);
				this._lastFocus = null;
			}
		},
		/**
		 * Key handler for the whole dialog
		 * @method
		 * @memberof ax/ext/ui/Dialog#
		 * @private
		 */
		_keyHandler: function (evt) {
			if (this._opts.closeOnBack === false) {
				return true;
			}

			var focusRet, evtKey = evt.id;

			switch (evtKey) {
			case vKey.UP.id:
			case vKey.DOWN.id:
			case vKey.LEFT.id:
			case vKey.RIGHT.id:
				focusRet = fm.directionalFocusChangeByKey(evt, this);
				return false;
			case vKey.BACK.id:
				this.close();
				return false;
			default:
				return;
			}
		}

	});
});
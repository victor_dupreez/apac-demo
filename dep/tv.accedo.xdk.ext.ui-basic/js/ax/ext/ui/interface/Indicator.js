/**
 * @class ax/ext/ui/interface/Indicator
 * @desc To handle the indicator display.
 * 
 * Example: {@link ax/ext/ui/ProgressBar} , {@link ax/ext/ui/Scrollbar}
 * @author Mike Leung <mike.leung@accedo.tv>
 **/
define("ax/ext/ui/interface/Indicator", ["ax/Interface"], function (Interface) {
    "use strict";
    return Interface.create("Indicator", {
        /**
         * To update the when there are event indicator updated is received
         * @method update
         * @public
         * @param {Number} percent the percentage of the each drag
         * @memberof ax/ext/ui/interface/Indicator
         */
        update: ["percent"],
        /**
         * To set the indicator by the start, length and total
         * @method set
         * @public
         * @param {Number} start the start
         * @param {Number} length the length of a scroll
         * @param {Number} total the total length of the scrollable
         * @memberof ax/ext/ui/interface/Indicator
         */
        set: ["start", "length", "total"]
    });
});
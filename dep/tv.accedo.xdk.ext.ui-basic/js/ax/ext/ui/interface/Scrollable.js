/**
 * @class ax/ext/ui/interface/Scrollable
 * @desc To handle the scrollable items like scrollable container, grid
 * 
 * Scrollable Item are mainly used to update the indicator the current scrolling process.  
 * Example: {@link ax/ext/ui/ScrollableContainer} 
 *
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("ax/ext/ui/interface/Scrollable", ["ax/Interface"], function (Interface) {
    "use strict";
    return Interface.create("Scrollable", {
        /**
         * To register the ScrollingMediator
         * @public
         * @abstract
         * @method registerMediator
         * @param {ax/ext/ui/ScrollingMediator} mediator scrollingMediator object 
         * @memberof ax/ext/ui/interface/Scrollable
         */
        registerMediator: ["mediator"],
        /**
         * To unregister the ScrollingMediator
         * @public
         * @abstract
         * @method unregisterMediator
         * @param {ax/ext/ui/ScrollingMediator} mediator scrollingMediator object 
         * @memberof ax/ext/ui/interface/Scrollable
         */
        unregisterMediator: ["mediator"],
        /**
         * To unregister all the ScrollingMediator
         * @public
         * @abstract
         * @method unregisterAllMediator
         * @memberof ax/ext/ui/interface/Scrollable
         */
        unregisterAllMediator: []
    });
});
/**
 * @class ax/ext/ui/AbstractIndicator
 * @deprecated
 * @extends ax/af/Component
 * @augments ax/ext/ui/interface/Indicator
 * @desc To handle the indicator display.
 *
 * There are two features.
 *
 * 1. Indicator will receive update callback with percentage and then update the indicator UI.
 * (Deprecated)2. Indicator will dispatch event to send the signal when the value of indicator changes.
 *
 *
 * E.g Scroll Bar,
 * when the scrollable container scroll, it will keep update the scroll bar by the percentage.
 * when the user click a point in the scroll bar, the scroll bar will dispatch the event and scrollable container will receive and then update.
 *
 * Example: {@link ax/ext/ui/ProgressBar} , {@link ax/ext/ui/Scrollbar}
 *
 * @author Mike Leung <mike.leung@accedo.tv>
 **/
/*jshint unused:false*/
define("ax/ext/ui/AbstractIndicator", ["ax/ext/ui/interface/Indicator", "ax/class", "ax/af/Component", "ax/af/evt/type", "ax/console"], function (IIndicator, klass, Component, evtType, console) {
	"use strict";
	return klass.createAbstract(Component, [IIndicator], {}, {
		/**
		 * To update the when there are event indicator updated is received
		 * @method update
		 * @public
		 * @param {Number} percent the percentage of the each drag
		 * @memberof ax/ext/ui/AbstractIndicator
		 */
		update: function (percent) {
			console.warn("update is not implemented in Indicator Object");
		},
		/**
		 * To set the indicator by the start, length and total
		 * @method set
		 * @public
		 * @param {Number} start the start
		 * @param {Number} length the length of a scroll
		 * @param {Number} total the total length of the scrollable
		 * @memberof ax/ext/ui/AbstractIndicator
		 */
		set: function (start, length, total) {
			console.warn("set is not implemented in Indicator Object");
		},
		/**
		 * To update the mediator that the indicator is updated
		 * @method dispatchIndicatorUpdate
		 * @deprecated since the indicator should only receive the event from the scrollable and set/update the information. It won't update scrollable.
		 * @public
		 * @param {Boolean} reverse the percent of the each update
		 * @param {Number} percent the percentage of the each update
		 * @memberof ax/ext/ui/AbstractIndicator
		 */
		dispatchIndicatorUpdate: function (reverse, percent) {
			var obj = {};
			obj.reverse = reverse || false;
			obj.percent = percent || 0;
			this.dispatchEvent(evtType.INDICATOR_UPDATED, obj);
		}
	});
});
/*jshint unused:true*/
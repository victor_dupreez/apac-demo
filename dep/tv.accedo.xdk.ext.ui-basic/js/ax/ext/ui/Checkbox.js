/**
 * 
 * A basic checkbox widget. It will add checked class to the span to indicate. 
 * 
 * ###Dom Structure
 *      <div class="wgt-checkbox">
 *          <span class="checked">
 *          </span>
 *      </div>
 * 
 * ###CSS
 * ####Structural CSS
 *      //To display span according to the size of .wgt-checkbox
 *      .wgt-checkbox span {
 *           display: block;
 *           width: 100%;
 *           height: 100%;
 *       }
 *       
 * ####Customizable CSS
 * Developer may need to set the following css to make it works properly.  
 * 
 * * .wgt-checkbox - to set its width and height so that the span will fit its size.
 * * .wgt-checkbox span - the original css style
 * * .wgt-checkbox span.checked - the checked css style
 * @class ax/ext/ui/Checkbox
 * @extends ax/af/Component
 * @param {Object} opts The options object
 * @param {Boolean} opts.checked The default setting of the checkbox whether is checked or not.
 * @author Daniel Deng <daniel.deng@accedo.tv>
 **/
define("ax/ext/ui/Checkbox", ["ax/class", "ax/af/Component", "ax/Element", "ax/af/evt/type", "ax/util", "css!./css/Checkbox"], function (klass, Component, Element, evtType, util) {
    "use strict";
	return klass.create(Component, {}, {
		/**
		 * @private
		 * @name _value
		 * @memberof ax/ext/ui/Checkbox#
		 */
		_value: false,
		/**
		 * @private
		 * @name _image
		 * @memberof ax/ext/ui/Checkbox#
		 */
		_image: null,
		/**
		 *
		 * @memberof ax/ext/ui/Checkbox#
		 * @param {Object} opts The options object
		 * @protected
		 */
		init: function (opts) {

			opts.focusable = true;
			opts.clickable = true;

			this._super(opts);

			this.getRoot().addClass("wgt-checkbox");

			this._value = !! opts.checked;
			this._image = new Element("span");

			if ( !! this._value) {
				this._image.addClass("checked");
			}

			this.getRoot().append(this._image);

			//to listen the event and toggle the check
			this.addEventListener(evtType.CLICK, util.bind(function () {
				this.toggle();
			}, this));
		},
		/**
		 * Toggles the checkbox status
		 * @method toggle
		 * @memberof ax/ext/ui/Checkbox#
		 * @public
		 */
		toggle: function () {
			this.check(!this._value);
		},
		/**
		 * Checks the checkbox if no param, or sets the checkbox to a specified status with param
		 * @name check
		 * @function
		 * @memberof ax/ext/ui/Checkbox#
		 * @param {Boolean} isChecked (optional) set the checkbox to this status
		 * @public
		 */
		check: function (isChecked) {
			var toCheck = util.isUndefined(isChecked) || !! isChecked;

			if (toCheck && !this._value) {
				this._image.addClass("checked");
			} else if (!toCheck && this._value) {
				this._image.removeClass("checked");
			}
			this._value = toCheck;
			this.dispatchEvent(evtType.VALUE_CHANGED, this._value);
		},
		/**
		 * Unchecks the checkbox.
		 * @name uncheck
		 * @function
		 * @memberof ax/ext/ui/Checkbox#
		 * @public
		 */
		uncheck: function () {
			this.check(false);
		},
		/**
		 * Gets the checkbox checked status
		 * @name getValue
		 * @function
		 * @returns {Boolean} whether this checkbox is checked
		 * @memberof ax/ext/ui/Checkbox#
		 * @public
		 */
		getValue: function () {
			return this._value;
		}
	});
});
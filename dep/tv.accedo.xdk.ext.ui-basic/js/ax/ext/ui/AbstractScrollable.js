/**
 * @class ax/ext/ui/AbstractScrollable
 * @deprecated
 * @extends ax/af/Container
 * @augments ax/ext/ui/interface/Scrollable
 * @desc To handle the scrollable items like scrollable container, grid
 *
 * [Backward Compatible only] Scrollable Item are mainly used to update the indicator the current scrolling process.
 *
 * There are two ways to notify the indicator.
 * 1. Inform the indicator by pecentage
 * 2. Inform the indicator by scrolling info ( start position, the scroll length and scrollable component length)
 *
 * (Deprecated) Another feature is to receive update from the indicator
 * 1. Recive the update information from the indicator.
 *
 * Example: {@link ax/ext/ui/ScrollableContainer}
 *
 * @author Mike Leung <mike.leung@accedo.tv>
 */
 /*jshint unused:false*/
define("ax/ext/ui/AbstractScrollable", ["ax/ext/ui/interface/Scrollable", "ax/class", "ax/af/Container", "ax/af/evt/type", "ax/console", "ax/util", "ax/core"], function(IScrollable, klass, Container, evtType, console, util, core) {
    "use strict";
    return klass.createAbstract(Container, [IScrollable], {}, {
        /**
         * To store the mediator
         * @name __mediator
         * @private
         * @memberof ax/ext/ui/interface/Scrollable#
         */
        __mediator: [],
        /**
         * To register the ScrollingMediator
         * @public
         * @method registerMediator
         * @param {ax/ext/ui/ScrollingMediator} mediator scrollingMediator object
         * @memberof ax/ext/ui/interface/Scrollable#
         */
        registerMediator: function(mediator) {
            if (!mediator.setScrollable) {
                throw core.createException("IncorrectParam", "Incorrect mediator implementation or not a proper mediator!");
            }

            // pass current instance to mediator to set
            mediator.setScrollable(this);
        },
        /**
         * To unregister the ScrollingMediator
         * @public
         * @method unregisterMediator
         * @param {ax/ext/ui/ScrollingMediator} mediator scrollingMediator object
         * @memberof ax/ext/ui/interface/Scrollable#
         */
        unregisterMediator: function(mediator) {
            if (!mediator.setScrollable) {
                throw core.createException("IncorrectParam", "Incorrect mediator implementation or not a proper mediator!");
            }
            var index = util.indexOf(this.__mediator, mediator);
            if (index > -1) {
                this.__mediator.splice(index, 1);
                // pass current instance to mediator to unset
                mediator.unsetScrollable(this);
            }
        },
        /**
         * To unregister all the ScrollingMediator
         * @public
         * @method unregisterAllMediator
         * @memberof ax/ext/ui/interface/Scrollable#
         */
        unregisterAllMediator: function() {
            util.each(this.__mediator, util.bind(function(obj) {
                this.unregisterMediator(obj);
            }, this));
        },
        /**
         * To update the indicator the info through the start, length of each scroll and total.
         * e.g there are ten items(total), each scroll will be 1 (length of scroll) item and start from item0(start).
         * So this will be depends on the process and its length
         * @method dispatchScrolledInfo
         * @public
         * @param {Number} start the start
         * @param {Number} length the length of a scroll
         * @param {Number} total the total length of the scrollable
         * @memberof ax/ext/ui/AbstractScrollable#
         */
        dispatchScrolledInfo: function(start, length, total) {
            var opt = {
                start: start || 0,
                length: length || 1,
                total: total || 1
            };
            console.info(opt);
            this.dispatchEvent(evtType.SCROLLED_INDICATOR, opt);
        },
        /**
         * To update the indicator update by the percentage like how much viewed which is independent of the length(size) of the indicator
         * @method dispatchScrolledUpdate
         * @public
         * @param {Number} percent the percentage of the process
         * @memberof ax/ext/ui/AbstractScrollable#
         */
        dispatchScrolledUpdate: function(percent) {
            this.dispatchEvent(evtType.SCROLLED_PERCENTAGE, percent);
        },
        /**
         * To update the when there are event indicator updated is received
         * @method update
         * @public
         * @deprecated
         * @param {Boolean} reverse the direction. Reverse is true when scroll up or left
         * @param {Number} percent the percentage of the each drag
         * @memberof ax/ext/ui/AbstractScrollable#
         */
       
        update: function(reverse, percent) {
            console.warn("update is not implemented in Scrollable Object");
        }

    });
});
/*jshint unused:true*/
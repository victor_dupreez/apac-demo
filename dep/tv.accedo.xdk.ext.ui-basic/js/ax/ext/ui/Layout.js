/**
 * The is a widget to layout components into grid like formation. In addition, it enables
 * automatic key navigation handling upon developer demand.
 *
 * ### Dom Structure
 *      <div id="layout1" class="wgt-layout align-v">
 *          <div id="buttonA" class="wgt-button layout-child row-begin" style="clear: left; float: left;">
 *              <div class="wgt-label">buttonA</div>
 *          </div>
 *          <div id="buttonD" class="wgt-button layout-child" style="clear: none; float: left;">
 *              <div class="wgt-label">buttonD</div>
 *          </div>
 *          <div id="buttonG" class="wgt-button layout-child row-end" style="clear: none; float: left;">
 *              <div class="wgt-label">buttonG</div>
 *          </div>
 *          <div id="buttonB" class="wgt-button layout-child row-begin" style="clear: left; float: left;">
 *              <div class="wgt-label">buttonB</div>
 *          </div>
 *          <div id="buttonE" class="wgt-button layout-child" style="clear: none; float: left;">
 *              <div class="wgt-label">buttonE</div>
 *          </div>
 *          <div id="buttonC" class="wgt-button layout-child row-end" style="clear: left; float: left;">
 *              <div class="wgt-label">buttonC</div>
 *          </div>
 *          <div id="buttonF" class="wgt-button row-begin layout-child" style="clear: none; float: left;">
 *              <div class="wgt-label">buttonF</div>
 *          </div>
 *      </div>
 * ### CSS
 * #### Structural CSS
 *      //to make the overflow element hidden
 *      .wgt-layout {
 *          overflow: hidden;
 *      }
 *      //to make each element float left
 *      .wgt-layout > * {
 *          float: left;
 *       }
 *       //to open a new row
 *       .wgt-layout > .row-begin{
 *           clear: left;
 *        }
 * @class ax/ext/ui/Layout
 * @extends ax/af/Container
 * @param {Object} opts The options object
 * @param {Integer} [opts.width] the layout width along the child alignment direction. Default is the initial number of children, or 10 if no children are specified.
 * @param {ax/ext/ui/Layout.VERTICAL|ax/ext/ui/Layout.HORIZONTAL} [opts.alignment=ax/ext/ui/Layout.HORIZONTAL] the alignment of the child object
 * @param {boolean} [opts.autoNavigation=true] wether to handle arrow key navigation automatically
 * @param {boolean} [opts.connectRows=false] treats all rows are connected one after another. Used only when auto-navigation is on.
 *   Handles the LEFT/RIGHT keys to navigate from the last component on the current row to the first component on the next row, and vice versa.
 * @example
 * // Generate a layout as shown in the *DOM Structure* section
 *  {
 *    klass : layout,
 *    alignment : layout.VERTICAL,
 *    id: "#layout1",
 *    width: 3,
 *    children : [
 *        {
 *            klass: Button,
 *            id: "buttonA",
 *            text: "buttonA"
 *        },
 *        {
 *            klass: Button,
 *            id: "buttonB",
 *            text: "buttonB"
 *        },
 *        {
 *            klass: Button,
 *            id: "buttonC",
 *            text: "buttonC"
 *        },
 *        {
 *            klass: Button,
 *            id: "buttonD",
 *            text: "buttonD"
 *        },
 *        {
 *            klass: Button,
 *            id: "buttonE",
 *            text: "buttonE"
 *        },
 *        {
 *            klass: Button,
 *            id: "buttonF",
 *            text: "buttonF"
 *        },
 *        {
 *            klass: Button,
 *            id: "buttonG",
 *            text: "buttonG"
 *        }
 *  ]}
 *
 */
define("ax/ext/ui/Layout", [
    "ax/class",
    "ax/af/Container",
    "ax/af/focusManager",
    "ax/af/evt/type",
    "ax/device/vKey",
    "ax/util",
    "css!./css/Layout"
], function (
    klass,
    Container,
    focusMgr,
    evtType,
    vKey,
    util
) {
    "use strict";

    return klass.create(Container, {
        /**
         * This is used to indicate a vertical alignment.
         * @memberof ax/ext/ui/Layout
         * @constant
         */
        VERTICAL: 0x01,
        /**
         * This is used to indicate a horizontal alignment.
         * @constant
         * @memberof ax/ext/ui/Layout
         */
        HORIZONTAL: 0x02
    }, {
        /**
         * To store the rows in the layout
         * @memberof ax/ext/ui/Layout#
         * @name _rows
         * @private
         */
        _rows: [],
        /**
         * Initialize the layout, overrides parent"s init()
         * @method
         * @memberof ax/ext/ui/Layout#
         */
        init: function (opts) {
            //default autoNavigation will be true
            if (!util.isBoolean(opts.autoNavigation)) {
                opts.autoNavigation = true;
            }

            //Set default alignment
            if (!opts.alignment) {
                opts.alignment = this.constructor.HORIZONTAL;
            }

            if (util.isUndefined(opts.width)) {
                opts.width = opts.children ? opts.children.length : 10;
            }

            this._super(opts);

            this._root.addClass("wgt-layout");
            if (opts.alignment === this.constructor.HORIZONTAL) {
                this._root.addClass("align-h");
            } else {
                this._root.addClass("align-v");
            }
        },
        /**
         * This function overrides parent"s postInit() method.
         * @method
         * @memberof ax/ext/ui/Layout#
         */
        postInit: function () {
            this._super();

            if (this._opts.autoNavigation) {
                this.addEventListener(evtType.KEY, this._keyHandler);
            }
        },
        /**
         * This function redraw all the items on dom
         * @method __redraw
         * @private
         * @memberof ax/ext/ui/Layout#
         */
        __redraw: function () {
            var curCol, curRow, i, children, width, targetPrevMarker;

            children = this._children;
            width = this._opts.width;

            this._rows.length = 0;

            //remove from dom and update the css
            this.getRoot().removeAll();
            this.__refresh();

            //to redraw everythings since the dom and css order are changed
            for (i = 0; i < children.length; i++) {
                curCol = Math.floor(i / width);
                curRow = i % width;

                if (curCol > 0) {
                    targetPrevMarker = this._rows[curRow][curCol - 1];
                    this._root.insertAfter(targetPrevMarker.getRoot(), children[i].getRoot());
                } else {
                    this._root.append(children[i].getRoot());
                }
            }
        },
        /**
         * This function refreshes children css of the layout
         * @method __refresh
         * @private
         * @param {Number} index the start index that css want to update
         * @memberof ax/ext/ui/Layout#
         */
        __refresh: function (index) {

            //if undefined, refresh all of them
            if (util.isUndefined(index)) {
                index = 0;
            }

            var curRow, curCol, child, i,
                width = this._opts.width,
                align = this._opts.alignment,
                children = this._children.slice(0);



            for (i = index; i < children.length; i++) {

                child = children[i];

                if (align === this.constructor.HORIZONTAL) {
                    curRow = Math.floor(i / width);
                    curCol = i % width;
                } else {
                    curCol = Math.floor(i / width);
                    curRow = i % width;
                }

                this.__updateChild(child, curRow, curCol);
            }

        },
        /**
         * Update the child css and map according to the row and col
         * @method __updateChild
         * @private
         * @param {ax/af/Component} child The child object to be updated
         * @param {Number} curRow the current row of the child
         * @param {Number} curCol the current col of the child
         * @returns {ax/af/Component} the child
         * @memberof ax/ext/ui/Layout#
         */
        __updateChild: function (child, curRow, curCol) {
            var rowEnd;

            if (this._opts.alignment === this.constructor.HORIZONTAL) {
                rowEnd = this._opts.width - 1;
            } else {
                //if next col has no stuff.then rowEnd will be itself
                if ((curCol + 1) * this._opts.width + curRow >= this._children.length) {
                    rowEnd = curCol;
                } else {
                    rowEnd = Math.floor((this._children.length - 1) / this._opts.width);
                }

            }
            //clear previous css class
            child.removeClass("row-begin");
            child.removeClass("row-end");

            if (!this._rows[curRow]) {
                this._rows[curRow] = [];
            }

            //update the ref to the row
            this._rows[curRow][curCol] = child;

            //it is unable to update the style by css, so use inner in the 2011 and 2010 samsung tv
            child.getRoot().css({
                "float": "left"
            });
            child.getRoot().css({
                "clear": "none"
            });

            if (curCol === 0) {
                child.addClass("row-begin");
                child.getRoot().css({
                    "clear": "left"
                });
            }
            if (curCol === rowEnd) {
                child.addClass("row-end");
            }

            return child;
        },
        /**
         * Attach a child component.
         * The DOM operation is abstracted, and be realized according to the component alignment. For instance, the order of the children in the Layout and the order of the corresponding DOM elements will be different when the alignment is {@link ax/ext/ui/Layout.VERTICAL}.
         *
         * @method attach
         * @param {ax/af/Component} child - The child component to be attached
         * @param {ax/af/Component.PLACE_BEFORE | ax/af/Component.PLACE_AFTER | ax/af/Component.PLACE_APPEND | ax/af/Component.PLACE_PREPEND}
         *   [placement=ax/af/Component.PLACE_APPEND] - Target position of the new child.
         * @param {ax/af/Component} [anchor] - The anchor component, effective only when __placement__ equals {@link ax/af/Component.PLACE_BEFORE} or {@link ax/af/Component.PLACE_AFTER}.
         *   This must be a child of the current container, the placement of the new child will be relative to this anchor.
         * @fires module:ax/af/evt/type.ATTACH
         * @fires module:ax/af/evt/type.ATTACHED_TO_DOM
         * @fires module:ax/af/evt/type.ATTACHED_TO_CONTROLLER
         * @memberof ax/ext/ui/Layout#
         */
        /**
         * Internal function to really attach a child component. Since the attach child compoent in layout is not the same as the children order.
         * Handling on the position on the attachment is handled here.
         * @method
         * @param {ax/af/Component} child - The child component to be attached
         * @param {ax/af/Component.PLACE_BEFORE | ax/af/Component.PLACE_AFTER | ax/af/Component.PLACE_APPEND | ax/af/Component.PLACE_PREPEND}
         * [placement] - position the child should be placed. By default {@link ax/af/Component.PLACE_APPEND}.
         * @param {ax/af/Component} [marker] - must be a child of current container,
         * the placement of child will be in relative to this marker.
         * @memberof ax/ext/ui/Layout#
         * @protected
         */
        _doAttach: function (child, placement, marker) {

            var targetPrevMarker, index, curCol, curRow, parentNode, nextSibling, children,
                width = this._opts.width,
                align = this._opts.alignment;

            //search for the position
            index = util.indexOf(this._children, child);

            //get the current position in the map
            if (align === this.constructor.HORIZONTAL) {
                curRow = Math.floor(index / width);
                curCol = index % width;
            } else {
                curCol = Math.floor(index / width);
                curRow = index % width;
            }

            //update the map and css
            this.__updateChild(child, curRow, curCol);

            children = this._children;

            //detach all the layout since it involves the change in dom or css
            //the purpose is to render once instead of rendering many times
            if (this.getRoot().isInDOMTree()) {
                nextSibling = this.getRoot().getHTMLElement().nextSibling;
                parentNode = this.getRoot().getParent();
                parentNode.removeChild(this.getRoot());
            }

            //attach the new item for the new child

            //for horizontal, the order of children in layout is same as dom
            if (align === this.constructor.HORIZONTAL) {

                switch (placement) {
                case Container.PLACE_APPEND:
                    this._root.append(child.getRoot());
                    break;
                case Container.PLACE_PREPEND:
                    this._root.prepend(child.getRoot());
                    break;
                case Container.PLACE_BEFORE:
                    this._root.insertBefore(marker.getRoot(), child.getRoot());
                    break;
                case Container.PLACE_AFTER:
                    this._root.insertAfter(marker.getRoot(), child.getRoot());
                    break;
                }

                //refresh the other layout children css start from the current index + 1
                if (index + 1 < children.length) {
                    this.__refresh(index + 1);
                }

            } else {

                //special dom handling

                //refresh the css start from 0 since whenever there are change. The css will be changed.
                this.__refresh();

                //for vertical alignment, need to change the dom order accroding to the index
                if (placement === Container.PLACE_APPEND) {

                    if (curCol > 0) {
                        //append the col next to the previous col
                        targetPrevMarker = this._rows[curRow][curCol - 1];
                        this._root.insertAfter(targetPrevMarker.getRoot(), child.getRoot());
                    } else {
                        //append the first col items
                        this._root.append(child.getRoot());
                    }

                } else {
                    //other cases, redraw everything since the dom and css order are changed.
                    this.__redraw();
                }

            }

            //attach the layout back to the DOM
            if (parentNode) {

                if (nextSibling) {
                    parentNode.insertBefore(nextSibling, this.getRoot());
                } else {
                    parentNode.append(this.getRoot());
                }

            }

        },
        /**
         * It will detach the child provided.
         * If there is no child parameter, it will detach the layout itself via the component's detach function.
         * @method
         * @public
         * @param {ax/af/Component} [child] The taret child to be removed. No Child provided will remove the layout itself.
         * @memberof ax/ext/ui/Layout#
         */
        detach: function (child) {
            if (!child) {
                //if no child provided, it will remove itself via component detach function
                return this._super();
            }

            var index = util.indexOf(this._children, child),
                ret = this._super(child),
                parentNode, prevSibling;

            //for horizontal, just update the css
            if (this._opts.alignment === this.constructor.HORIZONTAL) {
                this.__refresh(index);
                //remove the last map since a child is removed
                this._rows[this._rows.length - 1].splice([this._rows[this._rows.length - 1].length - 1], 1);
                if (this._rows[this._rows.length - 1].length === 0) {
                    //remove the last row array when it is empty
                    this._rows.splice(this._rows.length - 1, 1);
                }
                return ret;
            }

            //detach all the layout since it involves the change in dom or css
            //the purpose is to render once instead of render many times
            if (this.getRoot().isInDOMTree()) {
                prevSibling = this.getRoot().getHTMLElement().previousSibling;
                parentNode = this.getRoot().getParent();
                parentNode.removeChild(this.getRoot());
            }

            //for vertical, need to redraw everything, so remove all and update the css
            this.__redraw();

            //attach back to the DOM
            if (parentNode) {

                if (prevSibling) {
                    parentNode.insertAfter(prevSibling, this.getRoot());
                } else {
                    parentNode.append(this.getRoot());
                }

            }

            return ret;
        },
        /**
         * This function returns the child alignment of this layout.
         * @method
         * @see ax/ext/ui/Layout.VERTICAL
         * @see ax/ext/ui/Layout.HORIZONTAL
         * @public
         * @memberof ax/ext/ui/Layout#
         * @return {ax/ext/ui/Layout.VERTICAL|ax/ext/ui/Layout.HORIZONTAL} The child alignment
         */
        getAlignment: function () {
            return this._opts.alignment;
        },
        /**
         * The navigation key handler.
         * @method
         * @protected
         * @param {Event} evt the key event
         * @memberof ax/ext/ui/Layout#
         */
        _keyHandler: function (evt) {
            if (!(this._opts.autoNavigation)) {
                return;
            }

            var focusRet, evtKey = evt.id,
                curFocus, parent, child, curIdx, width = this._opts.width,
                align = this._opts.alignment,
                curRow, curCol;

            switch (evtKey) {
            case vKey.UP.id:
            case vKey.DOWN.id:
            case vKey.LEFT.id:
            case vKey.RIGHT.id:
                focusRet = focusMgr.directionChangeByKey(evt, this);
                break;
            default:
                return;

            }

            if (focusRet) {
                return false;
            }

            curFocus = focusMgr.getCurFocus();
            child = curFocus;
            parent = child.getParent();
            while (parent && parent !== this) {
                child = parent;
                parent = child.getParent();
            }

            if (!parent) {
                return;
            }

            curIdx = util.indexOf(this._children, child);

            while (curIdx >= 0) {

                if (align === this.constructor.HORIZONTAL) {
                    curRow = Math.floor(curIdx / width);
                    curCol = curIdx % width;
                } else {
                    curCol = Math.floor(curIdx / width);
                    curRow = curIdx % width;
                }

                switch (evt.id) {
                case vKey.UP.id:
                    curRow--;
                    break;
                case vKey.DOWN.id:
                    curRow++;
                    break;
                case vKey.LEFT.id:
                    curCol--;
                    break;
                case vKey.RIGHT.id:
                    curCol++;
                    break;
                }


                if (this._opts.connectRows) {
                    if (align === this.constructor.HORIZONTAL && (curCol >= width || curCol < 0)) {
                        curRow += Math.floor(curCol / width);
                        curCol = ((curCol % width) + width) % width;
                    } else if (curRow >= width || curRow < 0) {
                        curCol += Math.floor(curRow / width);
                        curRow = ((curRow % width) + width) % width;
                    }
                }

                if (this._rows[curRow] && this._rows[curRow][curCol]) {

                    //consumed the event
                    if (focusMgr.focus(this._rows[curRow][curCol])) {
                        return false;
                    }

                    //skip when focus failed
                    curIdx = util.indexOf(this._children, this._rows[curRow][curCol]);
                } else {
                    return;
                }
            }
        }
    });
});
/**
 * 
 * Scroll bar widget. Indicates scrolling progress.
 * ####Dom Structure
 *      <div id="scroll" class="wgt-scrollbar wgt-scrollbar-v">
 *          <table cellpadding="0" cellspacing="0" width="100%" class="track">
 *              <tr>
 *                   <td class="trackTop"></td>
 *              </tr>
 *              <tr>
 *                  <td class="trackMiddle"></td>
 *              </tr>
 *              <tr>
 *                  <td class="trackBottom"></td>
 *              </tr>
 *          </table>
 *          <table cellpadding="0" cellspacing="0" class="nodeTable" style="table-layout: fixed; height: 63px; top: 0px;">
 *              <tr class="nodeTopTr">
 *                  <td class="nodeTop"></td>
 *              </tr>
 *              <tr class="nodeMiddleTr">
 *                  <td class="nodeMiddle"></td>
 *              </tr>
 *              <tr class="nodeBottomTr">
 *                  <td class="nodeBottom"></td>
 *              </tr>
 *          </table>
 *      </div>
 * The above scroll bar is vertical one and it consists of two part, node and track.  
 * Node is the one which move up and down.  
 * Track is the background that node can move along.  
 * Each node/ track has three parts. They are top, middle and bottom which can handle different styles of scrollbar.  
 * 
 * ###CSS
 * ####Structural CSS
 *      .wgt-scrollbar{
 *          position: absolute;
 *          padding: 0px;
 *          margin: 0px;
 *      }
 *      .wgt-scrollbar .track{
 *          position: absolute;
 *          top: 0px;
 *          left: 0px;
 *          height: 100%;
 *          width:100%;
 *          table-layout: fixed;
 *      }
 *      .wgt-scrollbar .nodeTable{
 *          border-collapse: collapse;
 *          position: absolute;
 *      }
 *      .wgt-scrollbar .nodeTopTr{
 *          border-collapse:collapse;
 *      }
 *      .wgt-scrollbar-h .trackTop{
 *          height:100%;
 *      }
 *      .wgt-scrollbar-h .trackMiddle{
 *          height:100%;
 *      }
 *      .wgt-scrollbar-h .trackBottom{
 *          height:100%;
 *      }
 *      .wgt-scrollbar-h .nodeTable{
 *          height:100%;
 *      }
 *      .wgt-scrollbar-v .trackTop{
 *          width:100%;
 *      }
 *      .wgt-scrollbar-v .trackMiddle{
 *          width:100%;
 *      }
 *      .wgt-scrollbar-v .trackBottom{
 *          width:100%;
 *      }
 *      .wgt-scrollbar-v .nodeTable{
 *          width:100%;
 *      }
 *  ####Customizable CSS
 * Developer may need to set the following css to make it works properly.
 * 
 * * .wgt-scrollbar -to set the size of the scrollbar
 * * .wgt-scrollbar .track .trackTop - to set the size/style of the top element of the track
 * * .wgt-scrollbar .track .trackMiddle - to set the style of the middle element of the track
 * * .wgt-scrollbar .track .trackBottom - to set the style of the bottom element of the track
 * * .wgt-scrollbar .nodeTable .nodeTop - to set the style/size of the top element of the node
 * * .wgt-scrollbar .nodeTable .nodeMiddle - to set the style/size of the middle element of the node
 * * .wgt-scrollbar .nodeTable .nodeBottom - to set the style/size of the bottom element of the node
 * @class ax/ext/ui/Scrollbar
 * @extends ax/ext/ui/AbstractIndicator
 * @param {Object} opts The options object
 * @param {ax/ext/ui/Scrollbar.VERTICAL|ax/ext/ui/Scrollbar.HORIZONTAL} [opts.orientation] the scroll orientation
 */
define("ax/ext/ui/Scrollbar", ["ax/class", "ax/util", "ax/Element", "ax/console", "ax/ext/ui/AbstractIndicator", "css!./css/Scrollbar"], function (klass, util, Element, console, Indicator) {
    "use strict";
	return klass.create(Indicator, {
		/**
		 * This is used to indicate a vertical layout.
		 * @memberof ax/ext/ui/Scrollbar
		 * @constant
		 */
		VERTICAL: 0x01,
		/**
		 * This is used to indicate a horizontal layout.
		 * @constant
		 * @memberof ax/ext/ui/Scrollbar
		 */
		HORIZONTAL: 0x02
	}, {
		/**
		 * @private
		 * @name _track
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		_track: null,
		/**
		 * @private
		 * @name _nodeTable
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		_nodeTable: null,
		/**
		 * @private
		 * @name _vertical
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		_vertical: true,
		/**
		 * @private
		 * @name _trackTopTd
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		_trackTopTd: null,
		/**
		 * @private
		 * @name _nodeTopTd
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		_nodeTopTd: null,
		/**
		 * @private
		 * @name _nodeMiddleTd
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		_nodeMiddleTd: null,
		/**
		 * @private
		 * @name _nodeBottomTd
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		_nodeBottomTd: null,
		/**
		 * @private
		 * @name _trackTopping
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		_trackTopping: true,
		/**
		 * @private
		 * @name __previousPosition
		 * @memberof ax/ext/ui/Scrollbar#
		 */
		__previousPosition: 0,
		/**
		 * @constructor
		 * @memberof ax/ext/ui/Scrollbar#
		 * @param {String | Object} opts String or an object
		 * @private
		 */
		init: function (opts) {
			if (!opts.orientation) {
				opts.orientation = this.constructor.VERTICAL;
			}

			if (!opts.trackTopping) {
				this._trackTopping = opts.trackTopping;
			}

			if (!opts.trackTopping) {
				this._trackTopping = opts.trackTopping;
			}

			this._super(opts);
			var trackTopTr, trackMiddleTd, trackBottomTd, trackBottomTr, trackMiddleTr, nodeMiddleTd, nodeTopTr, nodeMiddleTr, nodeBottomTr;
			this.getRoot().addClass("wgt-scrollbar");
			if (opts.orientation === this.constructor.HORIZONTAL) {
				this._vertical = false;
				this.getRoot().addClass("wgt-scrollbar-h");
			} else {
				this.getRoot().addClass("wgt-scrollbar-v");
			}

			this._trackTable = new Element("table", {
				cellpadding: "0",
				cellspacing: "0",
				width: "100%"
			});

			this._trackTable.addClass("track");
			trackTopTr = new Element("tr");
			this._trackTopTd = new Element("td");
			this._trackTopTd.addClass("trackTop");
			if (this._vertical) {
				trackMiddleTr = new Element("tr");
				trackBottomTr = new Element("tr");
			}

			trackMiddleTd = new Element("td");
			trackMiddleTd.addClass("trackMiddle");
			trackBottomTd = new Element("td");
			trackBottomTd.addClass("trackBottom");
			this._nodeTable = new Element("table", {
				cellpadding: "0",
				cellspacing: "0"
			});
			this._nodeTable.addClass("nodeTable");

			nodeTopTr = new Element("tr");
			if (this._vertical) {
				nodeTopTr.addClass("nodeTopTr");
			}

			this._nodeTopTd = new Element("td");
			this._nodeTopTd.addClass("nodeTop");
			if (this._vertical) {
				nodeMiddleTr = new Element("tr");
				nodeMiddleTr.addClass("nodeMiddleTr");
				nodeBottomTr = new Element("tr");
				nodeBottomTr.addClass("nodeBottomTr");
			}

			nodeMiddleTd = new Element("td");
			nodeMiddleTd.addClass("nodeMiddle");
			this._nodeBottomTd = new Element("td");
			this._nodeBottomTd.addClass("nodeBottom");
			trackTopTr.append(this._trackTopTd);
			if (this._vertical) {
				trackMiddleTr.append(trackMiddleTd);
				trackBottomTr.append(trackBottomTd);
			} else {
				trackTopTr.append(trackMiddleTd);
				trackTopTr.append(trackBottomTd);
			}

			this._trackTable.append(trackTopTr);
			if (this._vertical) {
				this._trackTable.append(trackMiddleTr);
				this._trackTable.append(trackBottomTr);
			}

			nodeTopTr.append(this._nodeTopTd);
			if (this._vertical) {
				nodeMiddleTr.append(nodeMiddleTd);
				nodeBottomTr.append(this._nodeBottomTd);
			} else {
				nodeTopTr.append(nodeMiddleTd);
				nodeTopTr.append(this._nodeBottomTd);
			}

			this._nodeTable.append(nodeTopTr);
			if (this._vertical) {
				this._nodeTable.append(nodeMiddleTr);
				this._nodeTable.append(nodeBottomTr);
			}

			this.getRoot().append(this._trackTable);
			this.getRoot().append(this._nodeTable);
		},
		/**
		 * Set the size and length of the scroll bar
		 * @method set
		 * @param {Integer} start Starting position of the data being shown
		 * @param {Integer} length Length of the data being shown
		 * @param {Integer} total Total number of data in the data
		 * @memberof ax/ext/ui/Scrollbar#
		 * @public
		 */
		set: function (start, length, total) {
			if (start + length > total) {
				length = total - start;
			}
			var deferFunc = function () {
					var trackE = this._trackTable.getHTMLElement(),
						nodeStyle = this._nodeTable.getHTMLElement().style,
						trackTopTdE = this._trackTopTd.getHTMLElement(),
						nodeTopTdE = this._nodeTopTd.getHTMLElement(),
						nodeBottomTdE = this._nodeBottomTd.getHTMLElement(),
						trackHeight, trackY, nodeHeight, nodeY, trackWidth, trackX, nodeWidth, nodeX;
					if (this._vertical) {
						trackHeight = trackE.scrollHeight - (this._trackTopping ? trackTopTdE.scrollHeight * 2 : 0);
						trackY = trackE.scrollTop;
						nodeHeight = Math.ceil(trackHeight * length / total) + (this._trackTopping ? trackTopTdE.scrollHeight * 2 : 0);
						nodeHeight = Math.max(nodeTopTdE.scrollHeight + nodeBottomTdE.scrollHeight, nodeHeight);
						if (total === length) {
							nodeY = 0;
						} else {
							nodeY = Math.ceil(trackY + (this._trackTopping ? trackTopTdE.scrollHeight : 0) + start / (total - length) * (trackHeight - nodeHeight));
						}
						console.info("trackHeight: " + trackHeight + " trackY: " + trackY + " nodeHeight: " + nodeHeight + " nodeY: " + nodeY);
						nodeStyle.height = nodeHeight + "px";
						nodeStyle.top = nodeY + "px";
						this.__nodeHeight = nodeHeight;
						this.__trackHeight = trackHeight;
					} else {
						trackWidth = trackE.scrollWidth - (this._trackTopping ? trackTopTdE.scrollWidth * 2 : 0);
						trackX = trackE.scrollLeft;
						nodeWidth = Math.ceil(trackWidth * length / total) + (this._trackTopping ? trackTopTdE.scrollWidth * 2 : 0);
						nodeWidth = Math.max(nodeTopTdE.scrollWidth + nodeBottomTdE.scrollWidth, nodeWidth);
						if (total === length) {
							nodeX = 0;
						} else {
							nodeX = Math.ceil(trackX + (this._trackTopping ? trackTopTdE.scrollWidth : 0) + start / (total - length) * (trackWidth - nodeWidth));
						}

						nodeStyle.left = nodeX + "px";
						nodeStyle.width = nodeWidth + "px";

						this.__nodeWidth = nodeWidth;
						this.__trackWidth = trackWidth;
					}
					//need to add it here to make it work properly    
					this._nodeTable.css("tableLayout", "fixed");
					if (this.getRoot().css("display") !== "none") {
						this.show();
					}

				};
			util.defer().then(util.bind(deferFunc, this)).done();
		},
		/**
		 * To update the scroll bar display to the specific percentage and it will just edit the top of the node instead of the size or length
		 * @method update
		 * @param {Integer} percent the percentage of the scroll bar
		 * @memberof ax/ext/ui/Scrollbar#
		 * @public
		 */
		update: function (percent) {
			if (percent < 0 || percent > 1) {
				console.warn("percentage should be within 0 to 1");
				return;
			}
			if (this._vertical) {
				this._nodeTable.css("top", (this.__trackHeight - this.__nodeHeight) * percent + "px");
			} else {
				this._nodeTable.css("left", (this.__trackWidth - this.__nodeWidth) * percent + "px");
			}
		}
	});
});
/**
 * 
 * A basic button widget. Button is a focuable and clickable container with a label widget.  
 * 
 * ###Dom Structure  
 *      <div class="wgt-button">  
 *          <div class="wgt-label">  
 *              Sample Button  
 *          </div>  
 *      </div>
 * 
 * ###CSS
 * ####Structural CSS
 *      //To display button block
 *      .wgt-button{
 *          display: block;
 *       }
 *      .wgt-label{
 *         display: inline;
 *       }
 * @class ax/ext/ui/Button
 * @extends ax/af/Container
 * @param {Object} opts The options object
 * @param {String|ax/af/mvc/ModelRef} opts.text The text displayed on this button
 * @param {String} [opts.isPureText] whether the button text to set is pure text or html
 * @example
 * var button = new Button({
 *       text:"Button A",
 *       isPureText:true
 * })
 * button.addEventListener(evtType.CLICK, function() {
 *      console.log("enter");
 * });
 * 
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
/**
 * 
 * @method attach
 * @memberof ax/ext/ui/Button
 */
define("ax/ext/ui/Button", ["ax/class", "ax/af/Container", "ax/ext/ui/Label", "ax/Element", "css!./css/Button"], function (klass, Container, Label, Element) {
    "use strict";
	return klass.create(Container, {}, {
		/**
		 * The internal label widget to show button text
		 * @protected
		 * @name _label
		 * @memberof ax/ext/ui/Button#
		 */
		_label: null,
		/**
		 * override parent's init() method
		 * @method
		 * @memberof ax/ext/ui/Button#
		 * @protected
		 */
		init: function (opts) {

			opts.focusable = true;
			opts.clickable = true;
			opts.text = opts.text || "";
			opts.root = opts.root || new Element("div");

			this._super(opts);

			this.getRoot().addClass("wgt-button");

			this._label = new Label({
				text: opts.text,
				isPureText: !! opts.isPureText,
				parent: this
			});
		},
		/**
		 * This function sets the button text.
		 * @method
		 * @param {String|ax/af/mvc/ModelRef} text The text to set
		 * @param {Boolean} [isPureText] whether the text to set is pure text or html
		 * @memberof ax/ext/ui/Button#
		 * @public
		 */
		setText: function (text, isPureText) {
			this._label.setText(text || "", !! isPureText);
		},
		/**
		 * Get the button text.
		 * @return {String} text of the button
		 * @public
		 * @memberof ax/ext/ui/Button#
		 */
		getText: function () {
			return this._label.getText();
		}
	});
});
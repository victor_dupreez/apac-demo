/**
 *
 * Scrolling Mediator acts as the middle part between the scrollable component and indicator components.
 * After registering the indicator(s) and set scrollable, they will then be linked and communicate with each other.
 * @class ax/ext/ui/ScrollingMediator
 * @extends ax/af/evt/DualEventDispatcher
 * @example
 *   var medi = new mediator();
 *   medi.registerIndicator(this.getView().find("scrollBar"));
 *   medi.setScrollable(this.getView().find("scrollableLabel"));
 *   [Deprecated] medi.registerScrollable(this.getView().find("scrollableLabel"));
 *
 * @author Mike Leung <mike.leung@accedo.tv>
 */
define("ax/ext/ui/ScrollingMediator", ["ax/class", "ax/af/evt/DualEventDispatcher", "./interface/Scrollable", "ax/console", "ax/af/evt/type", "ax/util", "./interface/Indicator"], function (klass, EventDispatcher, IScrollable, console, evtType, util, IIdicator) {
    "use strict";
    var mediator = klass.create(EventDispatcher, {}, {
        /**
         * To store the indcator
         * @name __indicator
         * @private
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __indicator: [],
        /**
         * To store the scrollable object
         * @name __scrollableComp
         * @private
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __scrollableComp: null,
        /**
         * [Backward Compatible only] To store the scrollable object
         * @name __scrollable
         * @private
         * @deprecated
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __scrollable: [],
        /**
         * Handler for the listening to the scrollable and then emitting to the indicator about scrolled percentage
         * @name  __emitScrolledPercentHandler
         * @private
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __emitScrolledPercentHandler: null,
        /**
         * Handler for listening to the scrollable info and then emiiting to the indicator about the info
         * @name  __emitIndicatorInfoHandler
         * @private
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __emitIndicatorInfoHandler: null,
        /**
         * Handler for the listening to the indicator and then emitting updated info to the scrollable
         * @name  __emitUpdatedHandler
         * @deprecated since indicator won't dispatch update anymore.
         * @private
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __emitUpdatedHandler: null,
        init: function (opts) {
            this._super(opts);
            this.__emitScrolledPercentHandler = util.bind(this.__emitScrolledPercent, this);
            this.__emitIndicatorInfoHandler = util.bind(this.__emitIndicatorInfo, this);
            this.__emitUpdatedHandler = util.bind(this.__emitUpdated, this);
        },
        /**
         * To register the indicator
         * @public
         * @method registerIndicator
         * @param {ax/ext/ui/interface/Indicator | ax/ext/ui/IIndicator} indicator indicator object like scrollbar {@link ax/ext/ui/IIndicator} which is deprecated.
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        registerIndicator: function (indicator) {

            //@deprecated since it won't listen to indicator update anymore.
            indicator.addEventListener(evtType.INDICATOR_UPDATED, this.__emitUpdatedHandler);

            this.__indicator.push(indicator);
        },
        /**
         * To set the scrollable, scrollable would be replaced if it is already exist
         * @public
         * @method setScrollable
         * @param {ax/ext/ui/interface/Scrollable} scrollable the scrollable object
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        setScrollable: function (scrollable) {
            if (!klass.hasImpl(scrollable.constructor, IScrollable)) {
                console.warn("scrollable does not implement scrollable interface");
                return;
            }

            if (this.__scrollableComp) {
                console.warn("scrollable exists, replace with new scrollable");
                this.unsetScrollable();
            }


            scrollable.addEventListener(evtType.SCROLLED_PERCENTAGE, this.__emitScrolledPercentHandler);
            scrollable.addEventListener(evtType.SCROLLED_INDICATOR, this.__emitIndicatorInfoHandler);

            this.__scrollableComp = scrollable;
        },
        /**
         *  To handle when scrollable object send out signal to update the indicator info with start, length and total
         * @private
         * @method __emitIndicatorInfo
         * @param {Object} evt
         * @param {Number} evt.start the start
         * @param {Number} evt.length the length of a scroll
         * @param {Number} evt.total the total length of the scrollable
         * @param {ax/ext/ui/IScrollable} scrollable the scrollable object
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __emitIndicatorInfo: function (evt) {

            var start, length, total;
            start = evt.start || 0;
            length = evt.length || 1;
            total = evt.total || 1;

            util.each(this.__indicator, function (obj) {
                if (obj.set) {
                    obj.set(start, length, total);
                }
            });

        },
        /**
         *  To handle when scrollable object send out signal to update the indicator info by the percentage
         * @private
         * @method  __emitScrolledPercent
         * @param {Number} percent the percent to send to indicator
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __emitScrolledPercent: function (percent) {

            util.each(this.__indicator, function (obj) {
                if (obj.update) {
                    obj.update(percent);
                }
            });
        },
        /**
         * To unregister the indicator
         * @public
         * @method unregisterIndicator
         * @param {ax/ext/ui/interface/Indicator} indicator indicator object like scrollbar
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        unregisterIndicator: function (indicator) {
            var index;

            index = util.indexOf(this.__indicator, indicator);

            if (index > -1) {

                 //@deprecated since it won't listen to indicator update anymore.
                indicator.removeEventListener(evtType.INDICATOR_UPDATED, this.__emitUpdatedHandler);
                
                this.__indicator.splice(index, 1);

            }

        },
        /**
         * To unset the scrollable
         * @public
         * @method unsetScrollable
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        unsetScrollable: function () {

            var scrollable = this.__scrollableComp;

            if (!scrollable) {
                return;
            }

            scrollable.removeEventListener(evtType.SCROLLED_PERCENTAGE, this.__emitScrolledPercentHandler);
            scrollable.removeEventListener(evtType.SCROLLED_INDICATOR, this.__emitIndicatorInfoHandler);

            this.__scrollableComp = null;
        },

        /**
         * To unregister all the indicators
         * @public
         * @method unregisterAllIndicators
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        unregisterAllIndicators: function () {

            util.each(this.__indicator, util.bind(function (obj) {
                this.unregisterIndicator(obj);
            }, this));

        },
        /**
         * To unregister all the indicators and scrollables
         * @public
         * @method unregisterAll
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        unregisterAll: function () {

            this.unregisterAllIndicators();
            this.unsetScrollable();

            // keep for Backward Compatible
            this.unregisterScrollables();
        },

        /**
         * To register the scrollable
         * @public
         * @deprecated since it only allows one scrollable and nth indicator, register scrollable is deprecated and developers should use ax/ext/ui/ScrollingMediator#setScrollable
         * @method registerScrollable
         * @param {ax/ext/ui/IScrollable} scrollable the scrollable object
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        registerScrollable: function (scrollable) {

            scrollable.addEventListener(evtType.SCROLLED_PERCENTAGE, this.__emitScrolledPercentHandler);
            scrollable.addEventListener(evtType.SCROLLED_INDICATOR, this.__emitIndicatorInfoHandler);

            this.__scrollable.push(scrollable);
        },
        /**
         * To unregister the scrollable
         * @public
         * @deprecated since it only allows one scrollable and nth indicator, register scrollable is deprecated and developers should use ax/ext/ui/ScrollingMediator#unsetScrollable
         * @method unregisterScrollable
         * @param {ax/ext/ui/IScrollable} scrollable the scrollable object
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        unregisterScrollable: function (scrollable) {
            var index;

            index = util.indexOf(this.__scrollable, scrollable);

            if (index > -1) {

                scrollable.removeEventListener(evtType.SCROLLED_PERCENTAGE, this.__emitScrolledPercentHandler);
                scrollable.removeEventListener(evtType.SCROLLED_INDICATOR, this.__emitIndicatorInfoHandler);

                this.__scrollable.splice(index, 1);
            }
        },
        /**
         * To handle when indicator object send out signal to update the scrollable by the direction and percentage
         * @private
         * @method __emitUpdated
         * @deprecated since the scrollable component won't listen to the indicator anymore.  
         *             And it only allow one scrollable and so this __emitUpdated should not be used.
         * @param {Object} obj the option object
         * @param {Boolean} obj.reverse direction from the indicator change. True is reverse direction
         * @param {Number} obj.percent the percentage
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        __emitUpdated: function (obj) {

            var reverse, percent;
            reverse = obj.reverse || false;
            percent = obj.percent || 0;

            //notify each scrollable comp about the change
            util.each(this.__scrollable, function (item) {
                if (item.update) {
                    item.update(reverse, percent);
                }
            });

            //notify the scrollable comp about the update which is received from indicator.
            if (this.__scrollableComp && this.__scrollableComp.update) {
                this.__scrollableComp.update(reverse, percent);
            }

        },
        /**
         * To unregister all the scrollables
         * @public
         * @deprecated since it only allows one scrollable and nth indicator, register scrollable is deprecated and developers should use ax/ext/ui/ScrollingMediator#unsetScrollable
         * @method unregisterScrollables
         * @memberof ax/ext/ui/ScrollingMediator#
         */
        unregisterScrollables: function () {

            util.each(this.__scrollable, util.bind(function (obj) {
                this.unregisterScrollable(obj);
            }, this));

        }
    });

    return mediator;
});
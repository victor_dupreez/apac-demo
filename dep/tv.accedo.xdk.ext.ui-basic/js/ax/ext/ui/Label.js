/**
 *
 * A simple label widget.
 * ###Dom Structure
 *      <div class="wgt-label">Label</div>
 * @class ax/ext/ui/Label
 * @extends ax/af/Component
 * @param {String|ax/af/mvc/ModelRef} [opts.text] the text to set initially
 * @param {String} [opts.isPureText] whether the text to set is pure text or html
 * @author Daniel Deng <daniel.deng@accedo.tv>
 */
define("ax/ext/ui/Label", [
    "ax/class",
    "ax/af/Component",
    "ax/Element",
    "ax/util",
    "ax/af/mvc/ModelRef",
    "ax/console"
], function (
    klass,
    Component,
    Element,
    util,
    ModelRef,
    console
) {
    "use strict";

    return klass.create(Component, {}, {
        /**
         * the last model reference used
         * @memberof ax/ext/ui/Label#
         * @name _lastModelRef
         * @private
         */
        _lastModelRef: null,
        /**
         * the listener that is attached onto model reference
         * @memberof ax/ext/ui/Label#
         * @name _modelListener
         * @private
         */
        _modelListener: null,
        /**
         * override's parent's init() method
         * @memberof ax/ext/ui/Label#
         * @method
         * @protected
         */
        init: function (opts) {
            opts.text = opts.text || "";

            opts.root = new Element("div");
            opts.root.addClass("wgt-label");

            this._super(opts);

            this.setText(opts.text, !!opts.isPureText);
        },
        /**
         * override's parent's deinit() method
         * @memberof ax/ext/ui/Label#
         * @method
         * @protected
         */
        deinit: function () {
            // remove listener from last model reference
            if (this._lastModelRef && this._modelListener) {
                this._lastModelRef.getModel().off("change:" + this._lastModelRef.getAttr(), this._modelListener);
                this._lastModelRef = null;
            }
            this._modelListener = null;

            this._super();
        },
        /**
         * Whether the text set is pure text or html
         * @memberof ax/ext/ui/Label#
         * @private
         * @name _isPureText
         */
        _isPureText: false,
        /**
         * Sets the text of the label.
         * @param {String|ax/af/mvc/ModelRef} text The text to set
         * @param {Boolean} [isPureText] whether the text to set is pure text or html
         * @method
         * @memberof ax/ext/ui/Label#
         * @public
         */
        setText: function (text, isPureText) {
            // remove listener from last model reference
            if (this._lastModelRef && this._modelListener) {
                this._lastModelRef.getModel().off("change:" + this._lastModelRef.getAttr(), this._modelListener);
                this._lastModelRef = null;
            }

            isPureText = !!isPureText;
            this._isPureText = isPureText;
            var setTextFn = util.bind(function (text) {
                if (this._isPureText) {
                    this.getRoot().setInnerText(text);
                } else {
                    this.getRoot().setInnerHTML(text);
                }
            }, this);

            if (text instanceof ModelRef) {
                if (!this._modelListener) {
                    this._modelListener = util.bind(function (labelId, evt) {
                        var newValue = evt.newVal;

                        if (newValue === undefined || newValue === null) {
                            console.warn("[Label] Text" + (labelId ? " of label " + labelId : "") + " binded to the model value is changing to an undesired data type/value: " + newValue);
                            newValue = "";
                        }

                        setTextFn(newValue);
                    }, null, this.getOption("id", undefined));
                }
                text.getModel().on("change:" + text.getAttr(), this._modelListener);

                // save the model object
                this._lastModelRef = text;

                text = text.getVal();
            }
            setTextFn(text);
        },
        /**
         * Gets the text of the label.
         * @memberof ax/ext/ui/Label#
         * @method
         * @public
         * @return {String} text of the label
         */
        getText: function () {
            if (this._isPureText) {
                return this.getRoot().getInnerText();
            }
            return this.getRoot().getInnerHTML();
        }
    });
});
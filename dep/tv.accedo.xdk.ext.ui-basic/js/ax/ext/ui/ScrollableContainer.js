/**
 * Scrollable Container is mainly for scrolling the page of long information.
 *
 * It makes of the container and scrollable element inside, it will update the scrollable element top position and show part of the content.
 *
 * There are two ways to use the scrollable container
 *
 * 1. Scroll by focus and component
 * 2. Scroll by move
 *
 * The first one will make use of the current focus and then update the position of the page according to the position of the component. So developer needs to focus
 * on the scrollable container itself. upThreshold and bottomThreshold could be used for handling the padding to the border when focus on them.
 *
 * The second method is to scroll by move. Each move may be 50px, users can scroll up and down by updaing the top position of the page.
 *
 * PS. Linking to the scrollbar can be easily done by using the mediator and registering the indicator. The scrollbar will then be updated by the scrollable container
 * Another common problem is about the dynamic updating or image loading, since the scrollable container initialize at the begining and create the scrollbar with
 * specific length and start position, so updateInterval will be used to keep tracking on the scrollable element size and update the scrollbar more accurately.
 *
 * ###Dom Structure
 *      <div class="wgt-scrollableContainer">
 *          <div class="scrollable">Content</div>
 *      </div>
 *
 * ###CSS
 * ####Structural CSS
 *      //To make the extra text/component hidden when overflow
 *      .wgt-scrollableContainer{
 *           overflow:hidden;
 *          position:relative;
 *       }
 *
 *      //Scrollable is the container that changing the top position to make it scroll, so  need to be absolute
 *      .wgt-scrollableContainer .scrollable {
 *           position:absolute;
 *           width:100%;
 *       }
 *
 * ####Customizable CSS
 *
 * * .wgt-scrollableContainer - to set the size of scrollable container
 *
 * @class ax/ext/ui/ScrollableContainer
 * @extends ax/ext/ui/AbstractScrollable
 * @param {Object} [opts] Optional properties to be set in scrollable Container
 * @param {Number} [opts.scrollMove=50] the move of each scroll
 * @param {Number} [opts.updateInterval=0] the interval to update the scrollable container and info in sec
 * Since sometimes children are created dynamically, it needs to update the container info
 * @param {Number} [opts.upThreshold=0] threshold of the upper part when scrolling by element. It is similar to padding and indicates the location to be shown on the top.
 * @param {Number} [opts.bottomThreshold=0] threshold of the bottom part when scrolling by element.It is similar to padding and indicates the location to be shown on the top.
 */
define("ax/ext/ui/ScrollableContainer", [
    "ax/class",
    "ax/ext/ui/AbstractScrollable",
    "ax/Element",
    "ax/af/Container",
    "ax/af/evt/type",
    "ax/util",
    "ax/af/focusManager",
    "ax/device",
    "ax/console",
    "css!./css/ScrollableContainer"
], function (
    klass,
    Scrollable,
    Element,
    Container,
    evtType,
    util,
    focusManager,
    device,
    console,
    css
) {
    "use strict";

    return klass.create(Scrollable, {}, {
        /**
         *height of scrollable
         * @private
         * @name __scrollableHeight
         * @memberof ax/ext/ui/ScrollableContainer
         */
        __scrollableHeight: 0,
        /**
         *height of container
         * @private
         * @name __containerHeight
         * @memberof ax/ext/ui/ScrollableContainer
         */
        __containerHeight: 0,
        /**
         *top of scrollable
         * @private
         * @name  __scrollableTop
         * @memberof ax/ext/ui/ScrollableContainer
         */
        __scrollableTop: 0,
        /**
         *height of scrollable
         * @private
         * @name __scrollEle
         * @memberof ax/ext/ui/ScrollableContainer
         */
        __scrollEle: null,
        /**
         *height of scrollable
         * @private
         * @name __interval
         * @memberof ax/ext/ui/ScrollableContainer
         */
        __interval: null,
        /**
         * override's parent's init() method
         * @memberof ax/ext/ui/ScrollableContainer
         * @method
         * @protected
         */
        init: function (opts) {
            opts.scrollMove = util.isNumber(opts.scrollMove) ? opts.scrollMove : 50;
            opts.updateInterval = opts.updateInterval || 0;
            opts.upThreshold = opts.upThreshold || 0;
            opts.bottomThreshold = opts.bottomThreshold || 0;

            opts.root = new Element("div");
            this._super(opts);

            opts.root.addClass("wgt-scrollableContainer");

            //the element to scroll
            this.__scrollEle = new Element("div").addClass("scrollable");
            this.getRoot().append(this.__scrollEle);

            this.addEventListener(evtType.KEY, util.bind(this.__keyHandler, this), true);
            this.addEventListener(evtType.FOCUS, util.bind(this.__onFocus, this));

            this.addEventListener(evtType.ATTACHED_TO_DOM, util.bind(this.__onAttachedDOM, this));
            this.addEventListener(evtType.DETACHED_FROM_DOM, util.bind(this.__onDetachedDOM, this));
        },
        /**
         * Handling the checking when attached to the dom
         * @memberof ax/ext/ui/ScrollableContainer
         * @method
         * @private
         */
        __onAttachedDOM: function () {
            if (this.getOption("updateInterval") > 0) {
                this.__interval = setInterval(util.bind(this.update, this), this.getOption("updateInterval") * 1000);
            }
        },
        /**
         * Remove the handling when detached from the dom
         * @memberof ax/ext/ui/ScrollableContainer
         * @method
         * @private
         */
        __onDetachedDOM: function () {
            if (this.__interval) {
                clearInterval(this.__interval);
                this.__interval = null;
            }
        },
        /**
         * Handling the child focus and upate the position
         * @memberof ax/ext/ui/ScrollableContainer
         * @method
         * @private
         */
        __onFocus: function (evt) {
            var currentFocus = evt.target;
            if (currentFocus !== this) {
                this.scrollTo(currentFocus);
            }
        },
        /**
         * Handling the key inside the scrollable container
         * @memberof ax/ext/ui/ScrollableContainer
         * @method
         * @private
         */
        __keyHandler: function (key) {
            var evtKey = key.id,
                curFocus, ret = false;
            curFocus = focusManager.getCurFocus();

            //do Nothing when there is no focus
            if (!curFocus) {
                return false;
            }

            //only allow to handle onkey scroll when it is focusable and current focus is scrollable container itself.
            if (focusManager.isCompFocusable(this) && curFocus === this) {
                ret = this.__UpDownKeyHandling(evtKey);
                return !ret;
            }

            //focus and scroll handling for the children
            ret = focusManager.directionalFocusChangeByKey(key, this);
            //Get the new focus and then scrollTo that component
            curFocus = focusManager.getCurFocus();

            if (curFocus.isDescendantOf(this)) {
                this.scrollTo(curFocus);
            }

            if (ret) {
                return false;
            }

            return !this.__UpDownKeyHandling(evtKey);
        },
        /**
         * Handling the up down key
         * @memberof ax/ext/ui/ScrollableContainer
         * @return {Boolean} true if it is scrolled
         * @method
         * @private
         */
        __UpDownKeyHandling: function (evtKey) {
            var vKey = device.vKey;
            switch (evtKey) {
            case vKey.DOWN.id:
                return this.onScrollDown(this._scrollMove);
            case vKey.UP.id:
                return this.onScrollUp(this._scrollMove);
            }
        },
        /**
         * Override the original do Attach and add a layer when children are appending
         * @memberof ax/ext/ui/ScrollableContainer
         * @method
         */
        _doAttach: function (child, placement, marker) {
            switch (placement) {
            case Container.PLACE_APPEND:
                this.__scrollEle.append(child.getRoot());
                break;
            case Container.PLACE_PREPEND:
                this.__scrollEle.prepend(child.getRoot());
                break;
            case Container.PLACE_BEFORE:
                this.__scrollEle.insertBefore(marker.getRoot(), child.getRoot());
                break;
            case Container.PLACE_AFTER:
                this.__scrollEle.insertAfter(marker.getRoot(), child.getRoot());
                break;
            }
        },
        /**
         * To scroll up the container
         * @public
         * @param {Number} [scrollMove] the number of pixel to move up. Default will be the option of the scrollable container
         * @return {Boolean} true if it is scrolled
         * @method
         * @memberof ax/ext/ui/ScrollableContainer
         */
        onScrollUp: function (scrollMove) {
            var move = scrollMove || this.getOption("scrollMove");
            this.__scrollableTop = this.__scrollEle.getHTMLElement().offsetTop;

            if (this.__scrollableTop + move < 0) {
                return this.scroll(this.__scrollableTop + move);
            } else {
                return this.scroll(0);
            }
        },
        /**
         * To scroll down the container
         * @public
         * @param {Number} [scrollMove] the number of pixel to move up. Default will be the option of the scrollable container
         * @return {Boolean} true if it is scrolled
         * @method
         * @memberof ax/ext/ui/ScrollableContainer
         */
        onScrollDown: function (scrollMove) {
            var move = scrollMove || this.getOption("scrollMove");
            this.__containerHeight = this.getRoot().getHTMLElement().offsetHeight;
            this.__scrollableHeight = this.__scrollEle.getHTMLElement().scrollHeight;
            this.__scrollableTop = this.__scrollEle.getHTMLElement().offsetTop;

            if (this.__scrollableTop - move > -(this.__scrollableHeight - this.__containerHeight)) {
                return this.scroll(this.__scrollableTop - move);
            } else {
                return this.scroll(-(this.__scrollableHeight - this.__containerHeight));
            }
        },
        /**
         * To handle the scroll to specific position with the position number
         * @public
         * @param {Number} position the position to be set in the container
         * @return {Boolean} true if it is scrolled
         * @method
         * @memberof ax/ext/ui/ScrollableContainer
         */
        scroll: function (position) {
            if (!this.isScrollable()) {
                console.info("Container size is not scrollable");
                return false;
            }

            if (this.__scrollableTop === position) {
                console.info("The target position is same as the current position");
                return false;
            }

            if (position < -(this.__scrollableHeight - this.__containerHeight) || position > 0) {
                console.info("Out of the range and thus no scroll is performed");
                return false;
            }

            if (position === 0) {
                console.info("Dispatch Event evtType.SCROLLED_TO_TOP");
                this.dispatchEvent(evtType.SCROLLED_TO_TOP);
            }

            if (position === -(this.__scrollableHeight - this.__containerHeight)) {
                console.info("Dispatch Event evtType.SCROLLED_TO_BOTTOM");
                this.dispatchEvent(evtType.SCROLLED_TO_BOTTOM);
            }

            this.__scrollEle.getHTMLElement().style.top = position + "px";
            this.__scrollableTop = position;
            this.dispatchScrolledUpdate(-position / (this.__scrollableHeight - this.__containerHeight));
            return true;
        },
        /**
         * To check if it is scrollable.
         * @public
         * @method
         * @return {Boolean} true if it can further scroll
         * @memberof ax/ext/ui/ScrollableContainer
         */
        isScrollable: function () {
            this.__scrollableHeight = this.__scrollEle.getHTMLElement().offsetHeight;
            this.__containerHeight = this.getRoot().getHTMLElement().offsetHeight;
            return this.__scrollableHeight > this.__containerHeight;
        },
        /**
         * To check if it reaches the top
         * @public
         * @method
         * @return {Boolean} true if it reaches the top
         * @memberof ax/ext/ui/ScrollableContainer
         */
        isTop: function () {
            this.__scrollableTop = this.__scrollEle.getHTMLElement().offsetTop;
            if (!this.isScrollable() || this.__scrollableTop === 0) {
                return true;
            }
            return false;
        },
        /**
         * To check if it reaches the bottom
         * @public
         * @method
         * @return {Boolean} true if it reaches the bottom
         * @memberof ax/ext/ui/ScrollableContainer
         */
        isBottom: function () {
            this.__scrollableTop = this.__scrollEle.getHTMLElement().offsetTop;
            this.__containerHeight = this.getRoot().getHTMLElement().offsetHeight;
            this.__scrollableHeight = this.__scrollEle.getHTMLElement().scrollHeight;
            if (!this.isScrollable() || this.__scrollableTop === -(this.__scrollableHeight - this.__containerHeight)) {
                return true;
            }
            return false;
        },
        /**
         * Scroll the container to the top
         * @public
         * @method
         * @return {Boolean} true if it is scrolled
         * @memberof ax/ext/ui/ScrollableContainer
         */
        scrollToTop: function () {
            return this.scroll(0);
        },
        /**
         * Scroll the container to the bottom
         * @public
         * @method
         * @return {Boolean} true if it is scrolled
         * @memberof ax/ext/ui/ScrollableContainer
         */
        scrollToBottom: function () {
            this.__containerHeight = this.getRoot().getHTMLElement().offsetHeight;
            this.__scrollableHeight = this.__scrollEle.getHTMLElement().scrollHeight;
            return this.scroll(-(this.__scrollableHeight - this.__containerHeight));
        },
        /**
         * Scroll the container to the bottom
         * @public
         * @method
         * @param {ax/af/Component} component the component to be scrolled at.
         * @return {Boolean} true if it is scrolled
         * @memberof ax/ext/ui/ScrollableContainer
         */
        scrollTo: function (component) {
            if (!component) {
                console.info("no component available and thus unable to scroll to");
                return false;
            }
            var objectTop = 0,
                objectHeight, newScrollTop, maxScrollTop, calScrollTop,
                curElement = component.getRoot().getHTMLElement(),
                scrollableElement = this.__scrollEle.getHTMLElement();

            objectHeight = curElement.offsetHeight;

            while (curElement && curElement !== scrollableElement) {
                objectTop += curElement.offsetTop;
                curElement = curElement.offsetParent;
            }

            this.__scrollableTop = scrollableElement.offsetTop;
            this.__containerHeight = this.getRoot().getHTMLElement().offsetHeight;
            this.__scrollableHeight = scrollableElement.scrollHeight;

            newScrollTop = 0;
            if (objectTop + this.__scrollableTop + objectHeight > this.__containerHeight - this.getOption("bottomThreshold")) {
                //out of the bottom threshold
                maxScrollTop = -(this.__scrollableHeight - this.__containerHeight);
                calScrollTop = this.__containerHeight - this.getOption("bottomThreshold") - objectTop - objectHeight;
                newScrollTop = Math.max(maxScrollTop, calScrollTop);

                return this.scroll(newScrollTop);
            } else if (objectTop + this.__scrollableTop < this.getOption("upThreshold")) {
                //out of the upper threshold
                newScrollTop = Math.min(0, this.getOption("upThreshold") - objectTop);
                return this.scroll(newScrollTop);
            }
        },
        /**
         * Override the addEventListener and set the size to the indicator
         * @method
         * @protected
         * @memberof ax/ext/ui/ScrollableContainer
         */
        addEventListener: function (type, fn) {
            this.__scrollableTop = this.__scrollEle.getHTMLElement().offsetTop;
            this.__containerHeight = this.getRoot().getHTMLElement().offsetHeight;
            this.__scrollableHeight = this.__scrollEle.getHTMLElement().scrollHeight;
            var ret = this._super(type, fn);
            if (type === evtType.SCROLLED_INDICATOR) {
                this.dispatchScrolledInfo(this.__scrollableTop, this.__containerHeight, this.__scrollableHeight);
            }
            return ret;
        },
        /**
         * [Backward Compatible only] Update the scrollable container
         * @method
         * @deprecated
         * @public
         * @memberof ax/ext/ui/ScrollableContainer
         */
        update: function () {
            var newScrollableHeight = this.__scrollEle.getHTMLElement().offsetHeight,
                curFocus;
            if (this.__scrollableHeight !== newScrollableHeight) {
                this.__scrollableHeight = newScrollableHeight;
                if (this.__scrollableTop !== 0) {
                    this.scrollToBottom();
                }
                curFocus = focusManager.getCurFocus();
                if (curFocus !== this) {
                    this.scrollTo(curFocus);
                }
                this.__scrollableTop = this.__scrollEle.getHTMLElement().offsetTop;
                this.__containerHeight = this.getRoot().getHTMLElement().offsetHeight;
                this.dispatchScrolledInfo(this.__scrollableTop, this.__containerHeight, this.__scrollableHeight);
            }
        }
    });
});
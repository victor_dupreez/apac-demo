/**
 * Progress bar is the control and status bar when playing the video. It will indicate the percentage of the current video status.  
 *
 * 1. Normal Progress Bar
 * 2. Progress Bar with pointer
 *
 * In the progress bar widget(opts.progressBarCSS), there are firstly two separate parts.  
 * One is background(otps.bgCSS) and another one is the progress bar ( which like filling color of the bacgkround)  
 *
 * In the progress bar, it will further divide into three sections which are front (opts.frontCSS), middle (opts.playBarCSS) and end (opts.endCSS).  
 * The purpose is to be easy customised like setting as round corner at the front and end part.  
 * When it is very long, only the middle part will be repeated filled into the progress bar.  
 *
 * If there are pointer, user can set opts.pointer true and opts.pointerCSS.  
 * __Note__
 * PS. Since the progress bar involve the calculation of the progress and update the UI layout by the percentage.  
 * The requirement of css setting is very high. Progress bar widget will obtain the progress bar width and also the progress pointer (if necessary) and playbar width.  
 * Progress bar will undergo the calculation of the pointer and width and then update the css width of the playbar.  
 *
 * ###Dom Structure
 *      <div id="timeline" class="wgt-progressBar">
 *          <div class="wgt-progressBar-background"></div>
 *          <div class="wgt-progressBar-playBarDiv">
 *              <table cellpadding="0" cellspacing="0" border="0" class="wgt-progressBar-playBar">
 *                  <tr>
 *                      <td class="wgt-progressBar-playBar-front" style="width: 6.5px;"></td>
 *                      <td class="wgt-progressBar-playBar-middle" style="display: none;"></td>
 *                      <td class="wgt-progressBar-playBar-end" style="width: 6.5px;"></td>
 *                  </tr>
 *              </table>
 *          <div class="wgt-progressBar-pointer"></div>
 *      </div>
 *      
 *      
 * The above progress bar may contain 3 parts which are background of the progress bar, table for the progress bar and div for the pointer  
 * The progress bar table also consists of  3 parts which are front, middle and end. So developer will be able to customize the style of the progress bar.  
 * 
 * ###CSS
 * ####Structural CSS
 *      //To make the background same size as the progress bar
 *      .wgt-progressBar-background {
 *          width: 100%;
 *          height: 100%;
 *      }
 *      //To make the playbar has the same height of the progress bar
 *       .wgt-progressBar-playBarDiv{
 *          padding: 0px;
 *          margin: 0px;
 *          position: absolute;
 *          top: 0px;
 *          height: 100%;
 *       }
 *       //To make the playbar has the same height of the progress bar
 *      .wgt-progressBar-playBar-front, .wgt-progressBar-playBar-middle, .wgt-progressBar-playBar-end{
 *          height:100%
 *      }
 *      //To make the table to be correctly displayed
 *      .wgt-progressBar-playBar{
 *          border-collapse: collapse;
 *          table-layout: fixed;
 *          height: 100%;
 *      }
 *       //To set the position properly
 *      .wgt-progressBar-pointer{
 *          right: 0px;
 *          top: 0px;
 *          position: absolute;
 *      }
 *  ####Customizable CSS
 * Developer may need to set the following css to make it works properly.  
 * 
 * * .wgt-progressBar - to set the size of the progress bar like the width and height
 * * .wgt-progressBar-background -to set the background of the progress bar
 * * .wgt-progressBar-playBar-front - to set the front size of the play bar (most likely for the playbar with round corner)
 * * .wgt-progressBar-playBar-middle - to set the middle part which will be repeated filled when the progress is going on.
 * * .wgt-progressBar-playBar-end - to set the end size of the play bar
 * * .wgt-progressBar-pointer - to set the style of the pointer if necessary.
 *
 * @class ax/ext/ui/ProgressBar
 * @extends ax/ext/ui/AbstractIndicator
 * @param {Object} opts The options object
 * @param {Boolean} opts.pointer True if it has pointer in the progress bar
 * @param {String} opts.progressBarCSS The progressBar css. The default will be "wgt-progressBar"
 * @param {String} opts.bgCSS The bg css of the progress bar css name. The default will be "wgt-progressBar-background"
 * @param {String} opts.frontCSS The front end of the progress bar css. The default will be"wgt-progressBar-playBar-front"
 * @param {String} opts.playBarCSS The middle part of the progress bar css. The default will be"wgt-progressBar-playBar-middle"
 * @param {String} opts.endCSS The end/tip part of the progress bar css. The default will be "wgt-progressBar-playBar-end"
 * @param {String} opts.pointerCSS The pointer of the progress bar css. The default will be "wgt-progressBar-pointer"
 */
define("ax/ext/ui/ProgressBar", ["ax/class", "ax/util", "ax/ext/ui/AbstractIndicator", "ax/Element", "ax/af/evt/type", "css!./css/ProgressBar"], function (klass, util, Indicator, Element, evtType) {
    "use strict";
	return klass.create(Indicator, {}, {
		/**
		 * pointer of the progress bar
		 * @private
		 * @name _pointer
		 * @memberof ax/ext/ui/ProgressBar#
		 */
		_pointer: null,
		/**
		 * width of the front round corner of the progress bar
		 * @private
		 * @name _frontWidth
		 * @memberof ax/ext/ui/ProgressBar#
		 */
		_frontWidth: 0,
		/**
		 * width of the end round corner of the progress bar
		 * @private
		 * @name _endWidth
		 * @memberof ax/ext/ui/ProgressBar#
		 */
		_endWidth: 0,
		/**
		 * width of the current pointer of the progress bar which suppose to be smaller than the endWidth
		 * @private
		 * @name _pointerWidth
		 * @memberof ax/ext/ui/ProgressBar#
		 */
		_pointerWidth: 0,
		/**
		 * to indicate whether init or not
		 * @private
		 * @name _init
		 * @memberof ax/ext/ui/ProgressBar#
		 */
		_init: false,
		init: function (opts) {
			if (util.isUndefined(opts)) {
				opts = {};
			}

			opts.focusable = false;

			opts.root = new Element("div");

			opts.progressBarCSS = opts.progressBarCSS || "wgt-progressBar";
			opts.bgCSS = opts.bgCSS || "wgt-progressBar-background";
			opts.frontCSS = opts.frontCSS || "wgt-progressBar-playBar-front";
			opts.playBarCSS = opts.playBarCSS || "wgt-progressBar-playBar-middle";
			opts.endCSS = opts.endCSS || "wgt-progressBar-playBar-end";

			this._super(opts);

			this.progressBar = this.getRoot();
			this.progressBar.addClass(opts.progressBarCSS);

			this.progressBg = new Element("div");
			this.progressBg.addClass(opts.bgCSS);
			this.progressBar.append(this.progressBg);

			this.playBarDiv = new Element("div");
			this.playBarDiv.addClass("wgt-progressBar-playBarDiv");
			this.progressBar.append(this.playBarDiv);

			this.playBar = new Element("table", {
				cellpadding: "0",
				cellspacing: "0",
				border: "0"
			});
			this.playBar.addClass("wgt-progressBar-playBar");
			this.playBarDiv.append(this.playBar);

			this.playBartr = new Element("tr");
			this.playBar.append(this.playBartr);

			this.frontBar = new Element("td");
			this.frontBar.addClass(opts.frontCSS);
			this.playBartr.append(this.frontBar);

			this.currentPlay = new Element("td");
			this.currentPlay.addClass(opts.playBarCSS);
			this.playBartr.append(this.currentPlay);
			this.currentPlay.css("display", "none");

			this.endBar = new Element("td");
			this.endBar.addClass(opts.endCSS);
			this.playBartr.append(this.endBar);
			if (opts.pointer) {
				opts.pointerCSS = opts.pointerCSS || "wgt-progressBar-pointer";
				this._pointer = new Element("div");
				this._pointer.addClass(opts.pointerCSS);
				this.playBarDiv.append(this._pointer);
			}

			this.addEventListener(evtType.ATTACHED_TO_DOM, util.bind(this._onAttachedDOM, this));
		},
		/**
		 * get the information when the dom element is attached to HTML
		 * @private
		 * @function
		 * @memberof ax/ext/ui/ProgressBar#
		 * @name _onAttachedDOM
		 */
		_onAttachedDOM: function () {
			if (this._init) {
				return;
			}
			this.frontBar.css("width", null);

			this.endBar.css("width", null);

			//to get the front and end width at the first time
			this.progressLength = this.getRoot().getHTMLElement().scrollWidth;
			this._frontWidth = this.frontBar.getHTMLElement().scrollWidth;
			this._endWidth = this.endBar.getHTMLElement().scrollWidth;

			if (this.getOption("pointer")) {
				this._pointerWidth = this._pointer.getHTMLElement().scrollWidth;
				this.progressLength = this.progressLength - this._pointerWidth;
				/**
				 * @workaround it doesn't work on 2011 devices at the beginning, so set directly.
				 */

				this.frontBar.getHTMLElement().style.width = this._pointerWidth / 2 + "px";
				this.endBar.getHTMLElement().style.width = this._pointerWidth / 2 + "px";

			} else {
				this._pointerWidth = 0;
				/**
				 * @workaround it doesn't work on 2011 devices at the beginning, so set directly.
				 */
				this.frontBar.getHTMLElement().style.width = "0px";
				this.endBar.getHTMLElement().style.width = "0px";
			}


			//only get the width data when first attach to DOM
			this._init = true;
		},
		/**
		 * update the layout of the progress bar
		 * @public
		 * @param {Number} percent the percentage of the current position of the whole video
		 * @memberof ax/ext/ui/ProgressBar#
		 * @method update
		 */
		update: function (percent) {
			//the total length of the bar
			var playLength = this.progressLength * percent / 100,
				b4 = this._frontWidth + this._endWidth,
				//the (b4)length of play bar that need to handle when there is round corner image at the beginning and the end
				frontEndBar = 0,
				midLength = 0;

			if (this.getOption("pointer")) {
				playLength = playLength + this._pointerWidth;
				b4 = b4 - this._pointerWidth;
			}

			//the whole play bar
			this.playBar.css("width", playLength + "px");

			//handle case that the total of front bar and end bar is less than the current playBar at the beginning
			if (this.progressLength * percent / 100 < b4) {
				frontEndBar = this.progressLength * percent / 200;
				if (this.getOption("pointer")) {
					frontEndBar += this._pointerWidth / 2;
				}
				this.frontBar.css("width", frontEndBar + 1 + "px");
				this.endBar.css("width", frontEndBar + "px");
				this.currentPlay.css({
					width: "0px",
					// WARNING: CANNOT USE DISPLAY=NONE HERE!
					// Using that will exclude the tabel cell from the rendering flow.
					// That would crash Samsung 2011 TV!!!
					visibility: "hidden"
				});
			} else {
				frontEndBar = this._frontWidth + this._endWidth;
				this.frontBar.css("width", this._frontWidth + "px");
				this.endBar.css("width", this._endWidth + "px");

				midLength = playLength - frontEndBar;
				//in case the pointer must less than the endBar
				this.currentPlay.css({
					width: midLength + "px",
					visibility: "visible",
					display: "" // in case hidden from the start
				});
			}
		},
		/**
		 * get the progressBarBg which can use to change the image when necessary
		 * @public
		 * @memberof ax/ext/ui/ProgressBar#
		 * @method getProgressBg
		 * @returns {ax/Element} progressBarbg progressBarbg
		 */
		getProgressBg: function () {
			return this.progressBg;
		}
	});
});
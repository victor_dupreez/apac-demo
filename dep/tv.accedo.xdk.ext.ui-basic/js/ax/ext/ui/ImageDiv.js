/**
 * A basic image div widget using css setting the background. (not backgroud Image).  
 * So the background attribute (like background color / background position) should not be set to the imageDiv widget.  
 *
 * Note:2011 and 2012 Samsung devices don't support background image css, background-image css won't be used.  
 * {@link ax/ext/ui/Image}  
 * The behaviour is the same as the image widget. The difference is image widget use the image tag.  
 * 
 * ###Dom Structure
 *      <div  id="imageDiv" style="background-image: url(http://www.accedo.tv/wp-content/themes/accedo/images/customer_logos/viaplay.png); ">
 *      </div>
 * 
 * ###CSS
 * ####Customizable CSS
 * Developers need to set the size(width, height) of the imageDiv through css
 * 
 * @class ax/ext/ui/ImageDiv
 * @extends ax/af/Component
 * @param {Object} opts The options object
 * @param {String|ax/af/mvc/ModelRef} opts.src The image src
 */
define("ax/ext/ui/ImageDiv", ["ax/class", "ax/af/Component", "ax/Element", "ax/util", "ax/af/mvc/ModelRef"], function (klass, Component, Element, util, ModelRef) {
    "use strict";
	return klass.create(Component, {}, {
		/**
		 * To store the url of the image
		 * @name __url
		 * @memberof ax/ext/ui/ImageDiv#
		 * @private
		 */
		__url: null,
		/**
		 * @memberof ax/ext/ui/ImageDiv#
		 * @param {Object} opts The options object
		 * @protected
		 */
		init: function (opts) {
			opts.src = opts.src || "data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
			opts.root = new Element("div");
			this._super(opts);
			this.setSrc(opts.src);
		},
		/**
		 * Sets the source of an image object.
		 * @method setSrc
		 * @param {String|ax/af/mvc/ModelRef} value The new image source
		 * @memberof ax/ext/ui/ImageDiv#
		 * @public
		 */
		setSrc: function (value) {
			// remove listener from last model reference
			if (this.__lastModelRef && this.__modelListener) {
				this.__lastModelRef.getModel().off("change:" + this.__lastModelRef.getAttr(), this.__modelListener);
				this.__lastModelRef = null;
			}

			if (value instanceof ModelRef) {
				this.__lastModelRef = value;
				if (!this.__modelListener) {
					this.__modelListener = util.bind(function (evt) {
						this.__url = evt.newVal;
						this.getRoot().css("background", "url(" + evt.newVal + ")");
					}, this);
				}
				value.getModel().on("change:" + value.getAttr(), this.__modelListener);
				value = value.getVal();
			}
			this.__url = value;
			this.getRoot().css("background", "url(" + value + ")");
		},
		/**
		 * Gets the source
		 * @method getSrc
		 * @return {String} src The new image source
		 * @memberof ax/ext/ui/ImageDiv#
		 * @public
		 */
		getSrc: function () {
			return this.__url;
		}
	});
});
/**
 * @class ax/ext/ui/IScrollable
 * @deprecated as it is changed to another implementation using ax/ext/ui/interface/Scrollable.
 * @extends ax/af/Container
 * @desc To handle the scrollable items like scrollable container, grid
 * 
 * Scrollable Item are mainly used to update the indicator the current scrolling process.  
 * 
 * There are two ways to notify the indicator.  
 * 1. Inform the indicator by pecentage
 * 2. Inform the indicator by scrolling info ( start position, the scroll length and scrollable component length)
 * 
 * (Deprecated) Another feature is to receive update from the indicator  
 * 1. Recive the update information from the indicator.  
 * 
 * Example: {@link ax/ext/ui/ScrollableContainer} 
 */
define("ax/ext/ui/IScrollable", ["ax/class", "ax/af/Container", "ax/af/evt/type", "ax/console"], function (klass, Container, evtType, console) {
    "use strict";
	return klass.create(Container, {}, {
		/**
		 * To update the when there are event indicator updated is received
		 * @method update
		 * @public
		 * @deprecated since scrollable component is now only as scrollable item and won't be an indicator or receive any update from indicator.
		 * @param {Boolean} reverse the direction. Reverse is true when scroll up or left
		 * @param {Number} percent the percentage of the each drag
		 * @memberof ax/ext/ui/IScrollable#
		 */
		update: klass.abstractFn,
		/**
		 * To update the indicator the info through the start, length of each scroll and total.
		 * e.g there are ten items(total), each scroll will be 1 (length of scroll) item and start from item0(start).
		 * So this will be depends on the process and its length
		 * @method dispatchScrolledInfo
		 * @public        
		 * @param {Number} start the start
		 * @param {Number} length the length of a scroll
		 * @param {Number} total the total length of the scrollable
		 * @memberof ax/ext/ui/IScrollable#
		 */
		dispatchScrolledInfo: function (start, length, total) {
			var opt = {
				start: start || 0,
				length: length || 1,
				total: total || 1
			};
			console.info(opt);
			this.dispatchEvent(evtType.SCROLLED_INDICATOR, opt);
		},
		/**
		 * To update the indicator update by the percentage like how much viewed which is independent of the length(size) of the indicator
		 * @method dispatchScrolledUpdate
		 * @public
		 * @param {Number} percent the percentage of the process
		 * @memberof ax/ext/ui/IScrollable#
		 */
		dispatchScrolledUpdate: function (percent) {
			this.dispatchEvent(evtType.SCROLLED_PERCENTAGE, percent);
		}
	});
});
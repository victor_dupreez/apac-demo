/**
 * 
 * A basic image widget using image tag.  
 *
 * Developer can set the src as a string or model which you can update the image src when the model changes.  
 *
 *  {@link ax/ext/ui/ImageDiv}  
 *  The behaviour is the same as the imageDiv widget. The difference is ImageDiv using the background css attribute  
 *  
 *  ###Dom Structure
 *       <img src="http://www.accedo.tv/wp-content/themes/accedo/images/customer_logos/viaplay.png"> 
 * @class ax/ext/ui/Image
 * @extends ax/af/Component
 * @param {Object} opts The options object
 * @param {String|ax/af/mvc/ModelRef} opts.src The image src
 * @param {String|ax/af/mvc/ModelRef} opts.errorImg The error image
 * @param {String|ax/af/mvc/ModelRef} otps.alt The alt of image
 * @returns {Promise.<Object>} fulfilled with Object message and onload evt object
 * @throws {Promise.<Object>} rejected with Object message and onerror evt object
 */
define("ax/ext/ui/Image", ["ax/class", "ax/af/Component", "ax/Element", "ax/util", "ax/af/mvc/ModelRef", "ax/promise"], function (klass, Component, Element, util, ModelRef, promise) {
    "use strict";
	return klass.create(Component, {}, {
		/**
		 * To store the url of the image
		 * @name __url
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__url: null,
		/**
		 * To store the url of the image
		 * @name __alt
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__alt: null,
		/**
		 * To store the url of the error Img.
		 * @name __errorImg
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__errorImg: null,
		/**
		 * To save the error callback
		 * @name __errorCallback
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__errorCallback: null,
		/**
		 * To save the number of error called after setting a new source
		 * @name __counter
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__counter: 0,
		/**
		 * To save the model Ref for src, alt and errorImg
		 * @name __lastModelRef
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__lastModelRef: {
			src: null,
			alt: null,
			errorImg: null
		},
		/**
		 * To save the model listener for src, alt and errorImg
		 * @name __modelListener
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__modelListener: {
			src: null,
			alt: null,
			errorImg: null
		},
		/**
		 *
		 * @memberof ax/ext/ui/Image#
		 * @protected
		 */
		init: function (opts) {
			opts.src = opts.src || "data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
			opts.alt = opts.alt || "";
			opts.errorImg = opts.errorImg || "";

			opts.root = new Element("img");

			this._super(opts);

			this.setSrc(opts.src);
			this.setAlt(opts.alt);
			this.setErrorImg(opts.errorImg);
		},
		/**
		 * Sets the source of an image object.
		 * @method setSrc
		 * @param {String|ax/af/mvc/ModelRef} src The new image source
		 * @memberof ax/ext/ui/Image#
		 * @public
		 * @since 2.1: add promise capability
		 * @returns {Promise.<Object>} fulfilled with Object message and onload evt object
		 * @throws {Promise.<Object>} rejected with Object message and onerror evt object
		 */
		setSrc: function (src) {
			var imgNode = this.getRoot().getHTMLElement(), 
				deferred = promise.defer(),

				errorHandler = function(evt){
					deferred.reject({
						message: "Error in image loading", 
						evt: evt
					});
					clearHandlers();
				},
				loadedHandler = function(evt){
					deferred.resolve({
						message: "image loaded", 
						evt: evt
					});
					clearHandlers();
				}, 
				clearHandlers = function() {
					// these eventlisteners are used once
					imgNode.removeEventListener("error", errorHandler);
					imgNode.removeEventListener("load", loadedHandler);
				};
			
			imgNode.addEventListener("error", errorHandler);
			imgNode.addEventListener("load", loadedHandler);

			this.__modelHandling("src", util.bind(this.__setSrcFunc, this), src);

			return deferred.promise;
		},
		/**
		 * To set the src
		 * @method __setSrcFunc
		 * @param {String} src the url source
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__setSrcFunc: function (src) {
			this.__url = src;
			this.getRoot().attr("src", src);
			this.__counter = 0;
		},
		/**
		 * gets the source of an image object.
		 * @method getSrc
		 * @retrun {String}  The image src
		 * @memberof ax/ext/ui/Image#
		 * @public
		 */
		getSrc: function () {
			return this.__url;
		},
		/**
		 * Sets alternative text for an image object.
		 * @method setAlt
		 * @param {String|ax/af/mvc/ModelRef} alt New Alternative text
		 * @memberof ax/ext/ui/Image#
		 * @public
		 */
		setAlt: function (alt) {
			this.__modelHandling("alt", util.bind(this.__setAltFunc, this), alt);
		},
		/**
		 * To set the alt
		 * @method __setAltFunc
		 * @param {String} alt the alt
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__setAltFunc: function (alt) {
			this.__alt = alt;
			this.getRoot().attr("alt", alt);
		},
		/**
		 * gets the alt of an image object.
		 * @method getAlt
		 * @retrun {String}  The image alt
		 * @memberof ax/ext/ui/Image#
		 * @public
		 */
		getAlt: function () {
			return this.__alt;
		},
		/**
		 * To set the error callback when the image is unable to load
		 * @method setErrorCallback
		 * @param {Function} cb the error callback
		 * @memberof ax/ext/ui/Image#
		 * @public
		 */
		setErrorCallback: function (cb) {
			if (cb && util.isFunction(cb)) {
				this.__errorCallback = cb;
				this.getRoot().getHTMLElement().onerror = this.__errorCallback;
				this.__errorImg = null;
				this.__counter = 0;
			}
		},
		/**
		 * To reset/remove the error callback
		 * @method resetErrorCallback
		 * @memberof ax/ext/ui/Image#
		 * @public
		 */
		resetErrorCallback: function () {
			//to rest the counter to 0
			this.__counter = 0;
			this.__errorCallback = null;
			if (this.__errorImg) {
				this.getRoot().getHTMLElement().onerror = util.bind(function () {
					//to avoid setting errorImg and error again which cause deadlock, so just set once when failure.
					if (!this.__counter) {
						this.getRoot().attr("src", this.__errorImg);
						this.__counter++;
					}
				}, this);
			} else {
				this.getRoot().getHTMLElement().onerror = null;
			}
		},
		/**
		 * To handle the dynamic set error image
		 * @method setErrorImg
		 * @param {String|ax/af/mvc/ModelRef} value the error Image
		 * @memberof ax/ext/ui/Image#
		 * @public
		 */
		setErrorImg: function (value) {
			this.__modelHandling("errorImg", util.bind(this.__setErrorImgFunc, this), value);
		},
		/**
		 * To set the errorImg
		 * @method __setErrorImgFunc
		 * @param {String} value error image url
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__setErrorImgFunc: function (value) {
			if (value) {
				this.__errorImg = value;
			}
			this.resetErrorCallback();
		},
		/**
		 * To set the model
		 * @method __modelHandling
		 * @param {String} field type of the model e.g src/alt/errorImg
		 * @param {Function} func the callback function
		 * @param {Object|String} value the string or model reference
		 * @memberof ax/ext/ui/Image#
		 * @private
		 */
		__modelHandling: function (field, func, value) {
			// remove listener from last model reference
			if (this.__lastModelRef[field] && this.__modelListener[field]) {
				this.__lastModelRef[field].getModel().off("change:" + this.__lastModelRef[field].getAttr(), this.__modelListener[field]);
				this.__lastModelRef[field] = null;
			}

			if (!util.isString(value) && value instanceof ModelRef) {
				this.__lastModelRef[field] = value;
				if (!this.__modelListener[field]) {
					this.__modelListener[field] = function (evt) {
						func(evt.newVal);
					};
				}
				value.getModel().on("change:" + value.getAttr(), this.__modelListener[field]);
				value = value.getVal();
			}
			func(value);
		}
	});
});